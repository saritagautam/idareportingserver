/*
 * XML Type:  ProductSelectionType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.ProductSelectionType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML ProductSelectionType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public class ProductSelectionTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.core.api.ProductSelectionType
{
    private static final long serialVersionUID = 1L;
    
    public ProductSelectionTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName PRODUCT$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "Product");
    
    
    /**
     * Gets array of all "Product" elements
     */
    public com.idanalytics.core.api.ProductType[] getProductArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(PRODUCT$0, targetList);
            com.idanalytics.core.api.ProductType[] result = new com.idanalytics.core.api.ProductType[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "Product" element
     */
    public com.idanalytics.core.api.ProductType getProductArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.ProductType target = null;
            target = (com.idanalytics.core.api.ProductType)get_store().find_element_user(PRODUCT$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "Product" element
     */
    public int sizeOfProductArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PRODUCT$0);
        }
    }
    
    /**
     * Sets array of all "Product" element  WARNING: This method is not atomicaly synchronized.
     */
    public void setProductArray(com.idanalytics.core.api.ProductType[] productArray)
    {
        check_orphaned();
        arraySetterHelper(productArray, PRODUCT$0);
    }
    
    /**
     * Sets ith "Product" element
     */
    public void setProductArray(int i, com.idanalytics.core.api.ProductType product)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.ProductType target = null;
            target = (com.idanalytics.core.api.ProductType)get_store().find_element_user(PRODUCT$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(product);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "Product" element
     */
    public com.idanalytics.core.api.ProductType insertNewProduct(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.ProductType target = null;
            target = (com.idanalytics.core.api.ProductType)get_store().insert_element_user(PRODUCT$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "Product" element
     */
    public com.idanalytics.core.api.ProductType addNewProduct()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.ProductType target = null;
            target = (com.idanalytics.core.api.ProductType)get_store().add_element_user(PRODUCT$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "Product" element
     */
    public void removeProduct(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PRODUCT$0, i);
        }
    }
}
