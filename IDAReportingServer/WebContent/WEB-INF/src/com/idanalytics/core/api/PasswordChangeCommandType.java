/*
 * XML Type:  PasswordChangeCommandType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.PasswordChangeCommandType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api;


/**
 * An XML PasswordChangeCommandType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public interface PasswordChangeCommandType extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(PasswordChangeCommandType.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("passwordchangecommandtypea01etype");
    
    /**
     * Gets the "Credentials" element
     */
    com.idanalytics.core.api.CredentialsType getCredentials();
    
    /**
     * Sets the "Credentials" element
     */
    void setCredentials(com.idanalytics.core.api.CredentialsType credentials);
    
    /**
     * Appends and returns a new empty "Credentials" element
     */
    com.idanalytics.core.api.CredentialsType addNewCredentials();
    
    /**
     * Gets the "NewPassword" element
     */
    java.lang.String getNewPassword();
    
    /**
     * Gets (as xml) the "NewPassword" element
     */
    com.idanalytics.core.api.PasswordType xgetNewPassword();
    
    /**
     * Sets the "NewPassword" element
     */
    void setNewPassword(java.lang.String newPassword);
    
    /**
     * Sets (as xml) the "NewPassword" element
     */
    void xsetNewPassword(com.idanalytics.core.api.PasswordType newPassword);
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.core.api.PasswordChangeCommandType newInstance() {
          return (com.idanalytics.core.api.PasswordChangeCommandType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.core.api.PasswordChangeCommandType newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.core.api.PasswordChangeCommandType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.core.api.PasswordChangeCommandType parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.core.api.PasswordChangeCommandType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.core.api.PasswordChangeCommandType parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.core.api.PasswordChangeCommandType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.core.api.PasswordChangeCommandType parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.PasswordChangeCommandType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.core.api.PasswordChangeCommandType parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.PasswordChangeCommandType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.core.api.PasswordChangeCommandType parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.PasswordChangeCommandType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.core.api.PasswordChangeCommandType parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.PasswordChangeCommandType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.core.api.PasswordChangeCommandType parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.PasswordChangeCommandType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.core.api.PasswordChangeCommandType parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.PasswordChangeCommandType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.core.api.PasswordChangeCommandType parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.PasswordChangeCommandType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.core.api.PasswordChangeCommandType parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.PasswordChangeCommandType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.core.api.PasswordChangeCommandType parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.core.api.PasswordChangeCommandType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.core.api.PasswordChangeCommandType parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.core.api.PasswordChangeCommandType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.core.api.PasswordChangeCommandType parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.core.api.PasswordChangeCommandType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.core.api.PasswordChangeCommandType parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.core.api.PasswordChangeCommandType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.core.api.PasswordChangeCommandType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.core.api.PasswordChangeCommandType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.core.api.PasswordChangeCommandType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.core.api.PasswordChangeCommandType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
