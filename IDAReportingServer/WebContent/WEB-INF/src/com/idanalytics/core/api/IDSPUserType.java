/*
 * XML Type:  IDSPUserType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.IDSPUserType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api;


/**
 * An XML IDSPUserType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public interface IDSPUserType extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(IDSPUserType.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("idspusertype2ff3type");
    
    /**
     * Gets the "UserName" element
     */
    java.lang.String getUserName();
    
    /**
     * Gets (as xml) the "UserName" element
     */
    com.idanalytics.core.api.UsernameType xgetUserName();
    
    /**
     * Sets the "UserName" element
     */
    void setUserName(java.lang.String userName);
    
    /**
     * Sets (as xml) the "UserName" element
     */
    void xsetUserName(com.idanalytics.core.api.UsernameType userName);
    
    /**
     * Gets the "Password" element
     */
    java.lang.String getPassword();
    
    /**
     * Gets (as xml) the "Password" element
     */
    org.apache.xmlbeans.XmlString xgetPassword();
    
    /**
     * Tests for nil "Password" element
     */
    boolean isNilPassword();
    
    /**
     * True if has "Password" element
     */
    boolean isSetPassword();
    
    /**
     * Sets the "Password" element
     */
    void setPassword(java.lang.String password);
    
    /**
     * Sets (as xml) the "Password" element
     */
    void xsetPassword(org.apache.xmlbeans.XmlString password);
    
    /**
     * Nils the "Password" element
     */
    void setNilPassword();
    
    /**
     * Unsets the "Password" element
     */
    void unsetPassword();
    
    /**
     * Gets the "PasswordExpirationDate" element
     */
    java.lang.String getPasswordExpirationDate();
    
    /**
     * Gets (as xml) the "PasswordExpirationDate" element
     */
    org.apache.xmlbeans.XmlString xgetPasswordExpirationDate();
    
    /**
     * Tests for nil "PasswordExpirationDate" element
     */
    boolean isNilPasswordExpirationDate();
    
    /**
     * True if has "PasswordExpirationDate" element
     */
    boolean isSetPasswordExpirationDate();
    
    /**
     * Sets the "PasswordExpirationDate" element
     */
    void setPasswordExpirationDate(java.lang.String passwordExpirationDate);
    
    /**
     * Sets (as xml) the "PasswordExpirationDate" element
     */
    void xsetPasswordExpirationDate(org.apache.xmlbeans.XmlString passwordExpirationDate);
    
    /**
     * Nils the "PasswordExpirationDate" element
     */
    void setNilPasswordExpirationDate();
    
    /**
     * Unsets the "PasswordExpirationDate" element
     */
    void unsetPasswordExpirationDate();
    
    /**
     * Gets the "MaximumSessions" element
     */
    short getMaximumSessions();
    
    /**
     * Gets (as xml) the "MaximumSessions" element
     */
    org.apache.xmlbeans.XmlShort xgetMaximumSessions();
    
    /**
     * Tests for nil "MaximumSessions" element
     */
    boolean isNilMaximumSessions();
    
    /**
     * True if has "MaximumSessions" element
     */
    boolean isSetMaximumSessions();
    
    /**
     * Sets the "MaximumSessions" element
     */
    void setMaximumSessions(short maximumSessions);
    
    /**
     * Sets (as xml) the "MaximumSessions" element
     */
    void xsetMaximumSessions(org.apache.xmlbeans.XmlShort maximumSessions);
    
    /**
     * Nils the "MaximumSessions" element
     */
    void setNilMaximumSessions();
    
    /**
     * Unsets the "MaximumSessions" element
     */
    void unsetMaximumSessions();
    
    /**
     * Gets the "AccountLocked" element
     */
    boolean getAccountLocked();
    
    /**
     * Gets (as xml) the "AccountLocked" element
     */
    org.apache.xmlbeans.XmlBoolean xgetAccountLocked();
    
    /**
     * Sets the "AccountLocked" element
     */
    void setAccountLocked(boolean accountLocked);
    
    /**
     * Sets (as xml) the "AccountLocked" element
     */
    void xsetAccountLocked(org.apache.xmlbeans.XmlBoolean accountLocked);
    
    /**
     * Gets the "UserType" element
     */
    java.lang.String getUserType();
    
    /**
     * Gets (as xml) the "UserType" element
     */
    org.apache.xmlbeans.XmlString xgetUserType();
    
    /**
     * Sets the "UserType" element
     */
    void setUserType(java.lang.String userType);
    
    /**
     * Sets (as xml) the "UserType" element
     */
    void xsetUserType(org.apache.xmlbeans.XmlString userType);
    
    /**
     * Gets the "ClientId" element
     */
    java.lang.String getClientId();
    
    /**
     * Gets (as xml) the "ClientId" element
     */
    org.apache.xmlbeans.XmlString xgetClientId();
    
    /**
     * Tests for nil "ClientId" element
     */
    boolean isNilClientId();
    
    /**
     * True if has "ClientId" element
     */
    boolean isSetClientId();
    
    /**
     * Sets the "ClientId" element
     */
    void setClientId(java.lang.String clientId);
    
    /**
     * Sets (as xml) the "ClientId" element
     */
    void xsetClientId(org.apache.xmlbeans.XmlString clientId);
    
    /**
     * Nils the "ClientId" element
     */
    void setNilClientId();
    
    /**
     * Unsets the "ClientId" element
     */
    void unsetClientId();
    
    /**
     * Gets the "FirstName" element
     */
    java.lang.String getFirstName();
    
    /**
     * Gets (as xml) the "FirstName" element
     */
    org.apache.xmlbeans.XmlString xgetFirstName();
    
    /**
     * Sets the "FirstName" element
     */
    void setFirstName(java.lang.String firstName);
    
    /**
     * Sets (as xml) the "FirstName" element
     */
    void xsetFirstName(org.apache.xmlbeans.XmlString firstName);
    
    /**
     * Gets the "LastName" element
     */
    java.lang.String getLastName();
    
    /**
     * Gets (as xml) the "LastName" element
     */
    org.apache.xmlbeans.XmlString xgetLastName();
    
    /**
     * Sets the "LastName" element
     */
    void setLastName(java.lang.String lastName);
    
    /**
     * Sets (as xml) the "LastName" element
     */
    void xsetLastName(org.apache.xmlbeans.XmlString lastName);
    
    /**
     * Gets the "DisplayName" element
     */
    java.lang.String getDisplayName();
    
    /**
     * Gets (as xml) the "DisplayName" element
     */
    org.apache.xmlbeans.XmlString xgetDisplayName();
    
    /**
     * Tests for nil "DisplayName" element
     */
    boolean isNilDisplayName();
    
    /**
     * True if has "DisplayName" element
     */
    boolean isSetDisplayName();
    
    /**
     * Sets the "DisplayName" element
     */
    void setDisplayName(java.lang.String displayName);
    
    /**
     * Sets (as xml) the "DisplayName" element
     */
    void xsetDisplayName(org.apache.xmlbeans.XmlString displayName);
    
    /**
     * Nils the "DisplayName" element
     */
    void setNilDisplayName();
    
    /**
     * Unsets the "DisplayName" element
     */
    void unsetDisplayName();
    
    /**
     * Gets the "Email" element
     */
    java.lang.String getEmail();
    
    /**
     * Gets (as xml) the "Email" element
     */
    org.apache.xmlbeans.XmlString xgetEmail();
    
    /**
     * Sets the "Email" element
     */
    void setEmail(java.lang.String email);
    
    /**
     * Sets (as xml) the "Email" element
     */
    void xsetEmail(org.apache.xmlbeans.XmlString email);
    
    /**
     * Gets the "Affiliate" element
     */
    java.lang.String getAffiliate();
    
    /**
     * Gets (as xml) the "Affiliate" element
     */
    org.apache.xmlbeans.XmlString xgetAffiliate();
    
    /**
     * Tests for nil "Affiliate" element
     */
    boolean isNilAffiliate();
    
    /**
     * True if has "Affiliate" element
     */
    boolean isSetAffiliate();
    
    /**
     * Sets the "Affiliate" element
     */
    void setAffiliate(java.lang.String affiliate);
    
    /**
     * Sets (as xml) the "Affiliate" element
     */
    void xsetAffiliate(org.apache.xmlbeans.XmlString affiliate);
    
    /**
     * Nils the "Affiliate" element
     */
    void setNilAffiliate();
    
    /**
     * Unsets the "Affiliate" element
     */
    void unsetAffiliate();
    
    /**
     * Gets the "GroupName" element
     */
    java.lang.String getGroupName();
    
    /**
     * Gets (as xml) the "GroupName" element
     */
    org.apache.xmlbeans.XmlString xgetGroupName();
    
    /**
     * Sets the "GroupName" element
     */
    void setGroupName(java.lang.String groupName);
    
    /**
     * Sets (as xml) the "GroupName" element
     */
    void xsetGroupName(org.apache.xmlbeans.XmlString groupName);
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.core.api.IDSPUserType newInstance() {
          return (com.idanalytics.core.api.IDSPUserType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.core.api.IDSPUserType newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.core.api.IDSPUserType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.core.api.IDSPUserType parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.core.api.IDSPUserType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.core.api.IDSPUserType parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.core.api.IDSPUserType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.core.api.IDSPUserType parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.IDSPUserType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.core.api.IDSPUserType parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.IDSPUserType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.core.api.IDSPUserType parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.IDSPUserType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.core.api.IDSPUserType parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.IDSPUserType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.core.api.IDSPUserType parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.IDSPUserType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.core.api.IDSPUserType parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.IDSPUserType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.core.api.IDSPUserType parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.IDSPUserType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.core.api.IDSPUserType parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.IDSPUserType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.core.api.IDSPUserType parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.core.api.IDSPUserType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.core.api.IDSPUserType parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.core.api.IDSPUserType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.core.api.IDSPUserType parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.core.api.IDSPUserType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.core.api.IDSPUserType parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.core.api.IDSPUserType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.core.api.IDSPUserType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.core.api.IDSPUserType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.core.api.IDSPUserType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.core.api.IDSPUserType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
