/*
 * XML Type:  GetClientProfileSolutionKeysResponseType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.GetClientProfileSolutionKeysResponseType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML GetClientProfileSolutionKeysResponseType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public class GetClientProfileSolutionKeysResponseTypeImpl extends com.idanalytics.core.api.impl.IDSPResponseTypeImpl implements com.idanalytics.core.api.GetClientProfileSolutionKeysResponseType
{
    private static final long serialVersionUID = 1L;
    
    public GetClientProfileSolutionKeysResponseTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName KEYS$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "keys");
    
    
    /**
     * Gets the "keys" element
     */
    public com.idanalytics.core.api.MapType getKeys()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.MapType target = null;
            target = (com.idanalytics.core.api.MapType)get_store().find_element_user(KEYS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "keys" element
     */
    public void setKeys(com.idanalytics.core.api.MapType keys)
    {
        generatedSetterHelperImpl(keys, KEYS$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "keys" element
     */
    public com.idanalytics.core.api.MapType addNewKeys()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.MapType target = null;
            target = (com.idanalytics.core.api.MapType)get_store().add_element_user(KEYS$0);
            return target;
        }
    }
}
