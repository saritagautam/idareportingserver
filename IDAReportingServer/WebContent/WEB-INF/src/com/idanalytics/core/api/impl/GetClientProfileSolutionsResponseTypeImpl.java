/*
 * XML Type:  GetClientProfileSolutionsResponseType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.GetClientProfileSolutionsResponseType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML GetClientProfileSolutionsResponseType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public class GetClientProfileSolutionsResponseTypeImpl extends com.idanalytics.core.api.impl.IDSPResponseTypeImpl implements com.idanalytics.core.api.GetClientProfileSolutionsResponseType
{
    private static final long serialVersionUID = 1L;
    
    public GetClientProfileSolutionsResponseTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SOLUTIONS$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "Solutions");
    
    
    /**
     * Gets the "Solutions" element
     */
    public com.idanalytics.core.api.SolutionListType getSolutions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.SolutionListType target = null;
            target = (com.idanalytics.core.api.SolutionListType)get_store().find_element_user(SOLUTIONS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Tests for nil "Solutions" element
     */
    public boolean isNilSolutions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.SolutionListType target = null;
            target = (com.idanalytics.core.api.SolutionListType)get_store().find_element_user(SOLUTIONS$0, 0);
            if (target == null) return false;
            return target.isNil();
        }
    }
    
    /**
     * True if has "Solutions" element
     */
    public boolean isSetSolutions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SOLUTIONS$0) != 0;
        }
    }
    
    /**
     * Sets the "Solutions" element
     */
    public void setSolutions(com.idanalytics.core.api.SolutionListType solutions)
    {
        generatedSetterHelperImpl(solutions, SOLUTIONS$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "Solutions" element
     */
    public com.idanalytics.core.api.SolutionListType addNewSolutions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.SolutionListType target = null;
            target = (com.idanalytics.core.api.SolutionListType)get_store().add_element_user(SOLUTIONS$0);
            return target;
        }
    }
    
    /**
     * Nils the "Solutions" element
     */
    public void setNilSolutions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.SolutionListType target = null;
            target = (com.idanalytics.core.api.SolutionListType)get_store().find_element_user(SOLUTIONS$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.core.api.SolutionListType)get_store().add_element_user(SOLUTIONS$0);
            }
            target.setNil();
        }
    }
    
    /**
     * Unsets the "Solutions" element
     */
    public void unsetSolutions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SOLUTIONS$0, 0);
        }
    }
}
