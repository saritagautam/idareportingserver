/*
 * XML Type:  ChallengeType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.ChallengeType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api;


/**
 * An XML ChallengeType(@http://idanalytics.com/core/api).
 *
 * This is an atomic type that is a restriction of com.idanalytics.core.api.ChallengeType.
 */
public interface ChallengeType extends org.apache.xmlbeans.XmlString
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ChallengeType.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("challengetype7ae1type");
    
    org.apache.xmlbeans.StringEnumAbstractBase enumValue();
    void set(org.apache.xmlbeans.StringEnumAbstractBase e);
    
    static final Enum CREDENTIALS_OR_TOKEN_NEEDED = Enum.forString("CREDENTIALS_OR_TOKEN_NEEDED");
    static final Enum TOKEN_EXPIRED = Enum.forString("TOKEN_EXPIRED");
    static final Enum CREDENTIALS_DENIED = Enum.forString("CREDENTIALS_DENIED");
    static final Enum PASSWORD_CHANGE_NEEDED = Enum.forString("PASSWORD_CHANGE_NEEDED");
    static final Enum MAX_SESSIONS_EXCEEDED = Enum.forString("MAX_SESSIONS_EXCEEDED");
    
    static final int INT_CREDENTIALS_OR_TOKEN_NEEDED = Enum.INT_CREDENTIALS_OR_TOKEN_NEEDED;
    static final int INT_TOKEN_EXPIRED = Enum.INT_TOKEN_EXPIRED;
    static final int INT_CREDENTIALS_DENIED = Enum.INT_CREDENTIALS_DENIED;
    static final int INT_PASSWORD_CHANGE_NEEDED = Enum.INT_PASSWORD_CHANGE_NEEDED;
    static final int INT_MAX_SESSIONS_EXCEEDED = Enum.INT_MAX_SESSIONS_EXCEEDED;
    
    /**
     * Enumeration value class for com.idanalytics.core.api.ChallengeType.
     * These enum values can be used as follows:
     * <pre>
     * enum.toString(); // returns the string value of the enum
     * enum.intValue(); // returns an int value, useful for switches
     * // e.g., case Enum.INT_CREDENTIALS_OR_TOKEN_NEEDED
     * Enum.forString(s); // returns the enum value for a string
     * Enum.forInt(i); // returns the enum value for an int
     * </pre>
     * Enumeration objects are immutable singleton objects that
     * can be compared using == object equality. They have no
     * public constructor. See the constants defined within this
     * class for all the valid values.
     */
    static final class Enum extends org.apache.xmlbeans.StringEnumAbstractBase
    {
        /**
         * Returns the enum value for a string, or null if none.
         */
        public static Enum forString(java.lang.String s)
            { return (Enum)table.forString(s); }
        /**
         * Returns the enum value corresponding to an int, or null if none.
         */
        public static Enum forInt(int i)
            { return (Enum)table.forInt(i); }
        
        private Enum(java.lang.String s, int i)
            { super(s, i); }
        
        static final int INT_CREDENTIALS_OR_TOKEN_NEEDED = 1;
        static final int INT_TOKEN_EXPIRED = 2;
        static final int INT_CREDENTIALS_DENIED = 3;
        static final int INT_PASSWORD_CHANGE_NEEDED = 4;
        static final int INT_MAX_SESSIONS_EXCEEDED = 5;
        
        public static final org.apache.xmlbeans.StringEnumAbstractBase.Table table =
            new org.apache.xmlbeans.StringEnumAbstractBase.Table
        (
            new Enum[]
            {
                new Enum("CREDENTIALS_OR_TOKEN_NEEDED", INT_CREDENTIALS_OR_TOKEN_NEEDED),
                new Enum("TOKEN_EXPIRED", INT_TOKEN_EXPIRED),
                new Enum("CREDENTIALS_DENIED", INT_CREDENTIALS_DENIED),
                new Enum("PASSWORD_CHANGE_NEEDED", INT_PASSWORD_CHANGE_NEEDED),
                new Enum("MAX_SESSIONS_EXCEEDED", INT_MAX_SESSIONS_EXCEEDED),
            }
        );
        private static final long serialVersionUID = 1L;
        private java.lang.Object readResolve() { return forInt(intValue()); } 
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.core.api.ChallengeType newValue(java.lang.Object obj) {
          return (com.idanalytics.core.api.ChallengeType) type.newValue( obj ); }
        
        public static com.idanalytics.core.api.ChallengeType newInstance() {
          return (com.idanalytics.core.api.ChallengeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.core.api.ChallengeType newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.core.api.ChallengeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.core.api.ChallengeType parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.core.api.ChallengeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.core.api.ChallengeType parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.core.api.ChallengeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.core.api.ChallengeType parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.ChallengeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.core.api.ChallengeType parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.ChallengeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.core.api.ChallengeType parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.ChallengeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.core.api.ChallengeType parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.ChallengeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.core.api.ChallengeType parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.ChallengeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.core.api.ChallengeType parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.ChallengeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.core.api.ChallengeType parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.ChallengeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.core.api.ChallengeType parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.ChallengeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.core.api.ChallengeType parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.core.api.ChallengeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.core.api.ChallengeType parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.core.api.ChallengeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.core.api.ChallengeType parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.core.api.ChallengeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.core.api.ChallengeType parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.core.api.ChallengeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.core.api.ChallengeType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.core.api.ChallengeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.core.api.ChallengeType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.core.api.ChallengeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
