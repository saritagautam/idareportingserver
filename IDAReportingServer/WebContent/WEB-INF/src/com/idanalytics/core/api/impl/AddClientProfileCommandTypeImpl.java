/*
 * XML Type:  AddClientProfileCommandType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.AddClientProfileCommandType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML AddClientProfileCommandType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public class AddClientProfileCommandTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.core.api.AddClientProfileCommandType
{
    private static final long serialVersionUID = 1L;
    
    public AddClientProfileCommandTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName TOKEN$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "Token");
    private static final javax.xml.namespace.QName CLIENTPROFILE$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "ClientProfile");
    
    
    /**
     * Gets the "Token" element
     */
    public java.lang.String getToken()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TOKEN$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Token" element
     */
    public com.idanalytics.core.api.TokenType xgetToken()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.TokenType target = null;
            target = (com.idanalytics.core.api.TokenType)get_store().find_element_user(TOKEN$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Token" element
     */
    public void setToken(java.lang.String token)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TOKEN$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TOKEN$0);
            }
            target.setStringValue(token);
        }
    }
    
    /**
     * Sets (as xml) the "Token" element
     */
    public void xsetToken(com.idanalytics.core.api.TokenType token)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.TokenType target = null;
            target = (com.idanalytics.core.api.TokenType)get_store().find_element_user(TOKEN$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.core.api.TokenType)get_store().add_element_user(TOKEN$0);
            }
            target.set(token);
        }
    }
    
    /**
     * Gets the "ClientProfile" element
     */
    public com.idanalytics.core.api.ClientProfileType getClientProfile()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.ClientProfileType target = null;
            target = (com.idanalytics.core.api.ClientProfileType)get_store().find_element_user(CLIENTPROFILE$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "ClientProfile" element
     */
    public void setClientProfile(com.idanalytics.core.api.ClientProfileType clientProfile)
    {
        generatedSetterHelperImpl(clientProfile, CLIENTPROFILE$2, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "ClientProfile" element
     */
    public com.idanalytics.core.api.ClientProfileType addNewClientProfile()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.ClientProfileType target = null;
            target = (com.idanalytics.core.api.ClientProfileType)get_store().add_element_user(CLIENTPROFILE$2);
            return target;
        }
    }
}
