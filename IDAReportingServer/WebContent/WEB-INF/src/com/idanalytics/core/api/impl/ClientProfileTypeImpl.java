/*
 * XML Type:  ClientProfileType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.ClientProfileType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML ClientProfileType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public class ClientProfileTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.core.api.ClientProfileType
{
    private static final long serialVersionUID = 1L;
    
    public ClientProfileTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CLIENTID$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "ClientId");
    private static final javax.xml.namespace.QName REQUIRESAFFILIATE$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "RequiresAffiliate");
    private static final javax.xml.namespace.QName TOKEN$4 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "Token");
    
    
    /**
     * Gets the "ClientId" element
     */
    public java.lang.String getClientId()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CLIENTID$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "ClientId" element
     */
    public org.apache.xmlbeans.XmlString xgetClientId()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CLIENTID$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "ClientId" element
     */
    public void setClientId(java.lang.String clientId)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CLIENTID$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CLIENTID$0);
            }
            target.setStringValue(clientId);
        }
    }
    
    /**
     * Sets (as xml) the "ClientId" element
     */
    public void xsetClientId(org.apache.xmlbeans.XmlString clientId)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CLIENTID$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(CLIENTID$0);
            }
            target.set(clientId);
        }
    }
    
    /**
     * Gets the "RequiresAffiliate" element
     */
    public boolean getRequiresAffiliate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(REQUIRESAFFILIATE$2, 0);
            if (target == null)
            {
                return false;
            }
            return target.getBooleanValue();
        }
    }
    
    /**
     * Gets (as xml) the "RequiresAffiliate" element
     */
    public org.apache.xmlbeans.XmlBoolean xgetRequiresAffiliate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(REQUIRESAFFILIATE$2, 0);
            return target;
        }
    }
    
    /**
     * Tests for nil "RequiresAffiliate" element
     */
    public boolean isNilRequiresAffiliate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(REQUIRESAFFILIATE$2, 0);
            if (target == null) return false;
            return target.isNil();
        }
    }
    
    /**
     * True if has "RequiresAffiliate" element
     */
    public boolean isSetRequiresAffiliate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(REQUIRESAFFILIATE$2) != 0;
        }
    }
    
    /**
     * Sets the "RequiresAffiliate" element
     */
    public void setRequiresAffiliate(boolean requiresAffiliate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(REQUIRESAFFILIATE$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(REQUIRESAFFILIATE$2);
            }
            target.setBooleanValue(requiresAffiliate);
        }
    }
    
    /**
     * Sets (as xml) the "RequiresAffiliate" element
     */
    public void xsetRequiresAffiliate(org.apache.xmlbeans.XmlBoolean requiresAffiliate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(REQUIRESAFFILIATE$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlBoolean)get_store().add_element_user(REQUIRESAFFILIATE$2);
            }
            target.set(requiresAffiliate);
        }
    }
    
    /**
     * Nils the "RequiresAffiliate" element
     */
    public void setNilRequiresAffiliate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(REQUIRESAFFILIATE$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlBoolean)get_store().add_element_user(REQUIRESAFFILIATE$2);
            }
            target.setNil();
        }
    }
    
    /**
     * Unsets the "RequiresAffiliate" element
     */
    public void unsetRequiresAffiliate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(REQUIRESAFFILIATE$2, 0);
        }
    }
    
    /**
     * Gets the "Token" element
     */
    public java.lang.String getToken()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TOKEN$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Token" element
     */
    public org.apache.xmlbeans.XmlString xgetToken()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TOKEN$4, 0);
            return target;
        }
    }
    
    /**
     * Tests for nil "Token" element
     */
    public boolean isNilToken()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TOKEN$4, 0);
            if (target == null) return false;
            return target.isNil();
        }
    }
    
    /**
     * True if has "Token" element
     */
    public boolean isSetToken()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(TOKEN$4) != 0;
        }
    }
    
    /**
     * Sets the "Token" element
     */
    public void setToken(java.lang.String token)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TOKEN$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TOKEN$4);
            }
            target.setStringValue(token);
        }
    }
    
    /**
     * Sets (as xml) the "Token" element
     */
    public void xsetToken(org.apache.xmlbeans.XmlString token)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TOKEN$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(TOKEN$4);
            }
            target.set(token);
        }
    }
    
    /**
     * Nils the "Token" element
     */
    public void setNilToken()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TOKEN$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(TOKEN$4);
            }
            target.setNil();
        }
    }
    
    /**
     * Unsets the "Token" element
     */
    public void unsetToken()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(TOKEN$4, 0);
        }
    }
}
