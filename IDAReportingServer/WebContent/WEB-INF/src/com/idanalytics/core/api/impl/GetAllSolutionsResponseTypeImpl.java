/*
 * XML Type:  GetAllSolutionsResponseType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.GetAllSolutionsResponseType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML GetAllSolutionsResponseType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public class GetAllSolutionsResponseTypeImpl extends com.idanalytics.core.api.impl.IDSPResponseTypeImpl implements com.idanalytics.core.api.GetAllSolutionsResponseType
{
    private static final long serialVersionUID = 1L;
    
    public GetAllSolutionsResponseTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SOLUTIONS$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "Solutions");
    
    
    /**
     * Gets the "Solutions" element
     */
    public com.idanalytics.core.api.SolutionListType getSolutions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.SolutionListType target = null;
            target = (com.idanalytics.core.api.SolutionListType)get_store().find_element_user(SOLUTIONS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "Solutions" element
     */
    public void setSolutions(com.idanalytics.core.api.SolutionListType solutions)
    {
        generatedSetterHelperImpl(solutions, SOLUTIONS$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "Solutions" element
     */
    public com.idanalytics.core.api.SolutionListType addNewSolutions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.SolutionListType target = null;
            target = (com.idanalytics.core.api.SolutionListType)get_store().add_element_user(SOLUTIONS$0);
            return target;
        }
    }
}
