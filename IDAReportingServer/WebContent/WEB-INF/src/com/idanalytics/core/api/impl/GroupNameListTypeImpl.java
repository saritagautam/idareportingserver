/*
 * XML Type:  GroupNameListType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.GroupNameListType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML GroupNameListType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public class GroupNameListTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.core.api.GroupNameListType
{
    private static final long serialVersionUID = 1L;
    
    public GroupNameListTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GROUPNAME$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "GroupName");
    
    
    /**
     * Gets array of all "GroupName" elements
     */
    public java.lang.String[] getGroupNameArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(GROUPNAME$0, targetList);
            java.lang.String[] result = new java.lang.String[targetList.size()];
            for (int i = 0, len = targetList.size() ; i < len ; i++)
                result[i] = ((org.apache.xmlbeans.SimpleValue)targetList.get(i)).getStringValue();
            return result;
        }
    }
    
    /**
     * Gets ith "GroupName" element
     */
    public java.lang.String getGroupNameArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(GROUPNAME$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) array of all "GroupName" elements
     */
    public com.idanalytics.core.api.PathType[] xgetGroupNameArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(GROUPNAME$0, targetList);
            com.idanalytics.core.api.PathType[] result = new com.idanalytics.core.api.PathType[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets (as xml) ith "GroupName" element
     */
    public com.idanalytics.core.api.PathType xgetGroupNameArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.PathType target = null;
            target = (com.idanalytics.core.api.PathType)get_store().find_element_user(GROUPNAME$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "GroupName" element
     */
    public int sizeOfGroupNameArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(GROUPNAME$0);
        }
    }
    
    /**
     * Sets array of all "GroupName" element
     */
    public void setGroupNameArray(java.lang.String[] groupNameArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(groupNameArray, GROUPNAME$0);
        }
    }
    
    /**
     * Sets ith "GroupName" element
     */
    public void setGroupNameArray(int i, java.lang.String groupName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(GROUPNAME$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.setStringValue(groupName);
        }
    }
    
    /**
     * Sets (as xml) array of all "GroupName" element
     */
    public void xsetGroupNameArray(com.idanalytics.core.api.PathType[]groupNameArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(groupNameArray, GROUPNAME$0);
        }
    }
    
    /**
     * Sets (as xml) ith "GroupName" element
     */
    public void xsetGroupNameArray(int i, com.idanalytics.core.api.PathType groupName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.PathType target = null;
            target = (com.idanalytics.core.api.PathType)get_store().find_element_user(GROUPNAME$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(groupName);
        }
    }
    
    /**
     * Inserts the value as the ith "GroupName" element
     */
    public void insertGroupName(int i, java.lang.String groupName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = 
                (org.apache.xmlbeans.SimpleValue)get_store().insert_element_user(GROUPNAME$0, i);
            target.setStringValue(groupName);
        }
    }
    
    /**
     * Appends the value as the last "GroupName" element
     */
    public void addGroupName(java.lang.String groupName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(GROUPNAME$0);
            target.setStringValue(groupName);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "GroupName" element
     */
    public com.idanalytics.core.api.PathType insertNewGroupName(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.PathType target = null;
            target = (com.idanalytics.core.api.PathType)get_store().insert_element_user(GROUPNAME$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "GroupName" element
     */
    public com.idanalytics.core.api.PathType addNewGroupName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.PathType target = null;
            target = (com.idanalytics.core.api.PathType)get_store().add_element_user(GROUPNAME$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "GroupName" element
     */
    public void removeGroupName(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(GROUPNAME$0, i);
        }
    }
}
