/*
 * XML Type:  SolutionListType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.SolutionListType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML SolutionListType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public class SolutionListTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.core.api.SolutionListType
{
    private static final long serialVersionUID = 1L;
    
    public SolutionListTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SOLUTION$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "Solution");
    
    
    /**
     * Gets array of all "Solution" elements
     */
    public java.lang.String[] getSolutionArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(SOLUTION$0, targetList);
            java.lang.String[] result = new java.lang.String[targetList.size()];
            for (int i = 0, len = targetList.size() ; i < len ; i++)
                result[i] = ((org.apache.xmlbeans.SimpleValue)targetList.get(i)).getStringValue();
            return result;
        }
    }
    
    /**
     * Gets ith "Solution" element
     */
    public java.lang.String getSolutionArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SOLUTION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) array of all "Solution" elements
     */
    public com.idanalytics.core.api.PathType[] xgetSolutionArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(SOLUTION$0, targetList);
            com.idanalytics.core.api.PathType[] result = new com.idanalytics.core.api.PathType[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets (as xml) ith "Solution" element
     */
    public com.idanalytics.core.api.PathType xgetSolutionArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.PathType target = null;
            target = (com.idanalytics.core.api.PathType)get_store().find_element_user(SOLUTION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "Solution" element
     */
    public int sizeOfSolutionArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SOLUTION$0);
        }
    }
    
    /**
     * Sets array of all "Solution" element
     */
    public void setSolutionArray(java.lang.String[] solutionArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(solutionArray, SOLUTION$0);
        }
    }
    
    /**
     * Sets ith "Solution" element
     */
    public void setSolutionArray(int i, java.lang.String solution)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SOLUTION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.setStringValue(solution);
        }
    }
    
    /**
     * Sets (as xml) array of all "Solution" element
     */
    public void xsetSolutionArray(com.idanalytics.core.api.PathType[]solutionArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(solutionArray, SOLUTION$0);
        }
    }
    
    /**
     * Sets (as xml) ith "Solution" element
     */
    public void xsetSolutionArray(int i, com.idanalytics.core.api.PathType solution)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.PathType target = null;
            target = (com.idanalytics.core.api.PathType)get_store().find_element_user(SOLUTION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(solution);
        }
    }
    
    /**
     * Inserts the value as the ith "Solution" element
     */
    public void insertSolution(int i, java.lang.String solution)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = 
                (org.apache.xmlbeans.SimpleValue)get_store().insert_element_user(SOLUTION$0, i);
            target.setStringValue(solution);
        }
    }
    
    /**
     * Appends the value as the last "Solution" element
     */
    public void addSolution(java.lang.String solution)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SOLUTION$0);
            target.setStringValue(solution);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "Solution" element
     */
    public com.idanalytics.core.api.PathType insertNewSolution(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.PathType target = null;
            target = (com.idanalytics.core.api.PathType)get_store().insert_element_user(SOLUTION$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "Solution" element
     */
    public com.idanalytics.core.api.PathType addNewSolution()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.PathType target = null;
            target = (com.idanalytics.core.api.PathType)get_store().add_element_user(SOLUTION$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "Solution" element
     */
    public void removeSolution(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SOLUTION$0, i);
        }
    }
}
