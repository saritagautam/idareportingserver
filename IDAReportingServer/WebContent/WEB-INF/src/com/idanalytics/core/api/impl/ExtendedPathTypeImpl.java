/*
 * XML Type:  ExtendedPathType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.ExtendedPathType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML ExtendedPathType(@http://idanalytics.com/core/api).
 *
 * This is an atomic type that is a restriction of com.idanalytics.core.api.ExtendedPathType.
 */
public class ExtendedPathTypeImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.core.api.ExtendedPathType
{
    private static final long serialVersionUID = 1L;
    
    public ExtendedPathTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected ExtendedPathTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
