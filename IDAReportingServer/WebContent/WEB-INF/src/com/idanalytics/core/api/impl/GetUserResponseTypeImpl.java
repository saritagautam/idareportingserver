/*
 * XML Type:  GetUserResponseType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.GetUserResponseType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML GetUserResponseType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public class GetUserResponseTypeImpl extends com.idanalytics.core.api.impl.IDSPResponseTypeImpl implements com.idanalytics.core.api.GetUserResponseType
{
    private static final long serialVersionUID = 1L;
    
    public GetUserResponseTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName USER$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "User");
    
    
    /**
     * Gets the "User" element
     */
    public com.idanalytics.core.api.IDSPUserType getUser()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.IDSPUserType target = null;
            target = (com.idanalytics.core.api.IDSPUserType)get_store().find_element_user(USER$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "User" element
     */
    public void setUser(com.idanalytics.core.api.IDSPUserType user)
    {
        generatedSetterHelperImpl(user, USER$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "User" element
     */
    public com.idanalytics.core.api.IDSPUserType addNewUser()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.IDSPUserType target = null;
            target = (com.idanalytics.core.api.IDSPUserType)get_store().add_element_user(USER$0);
            return target;
        }
    }
}
