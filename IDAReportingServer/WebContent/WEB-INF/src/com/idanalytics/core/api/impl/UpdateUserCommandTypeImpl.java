/*
 * XML Type:  UpdateUserCommandType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.UpdateUserCommandType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML UpdateUserCommandType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public class UpdateUserCommandTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.core.api.UpdateUserCommandType
{
    private static final long serialVersionUID = 1L;
    
    public UpdateUserCommandTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName USER$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "User");
    private static final javax.xml.namespace.QName TOKEN$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "Token");
    
    
    /**
     * Gets the "User" element
     */
    public com.idanalytics.core.api.IDSPUserType getUser()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.IDSPUserType target = null;
            target = (com.idanalytics.core.api.IDSPUserType)get_store().find_element_user(USER$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "User" element
     */
    public void setUser(com.idanalytics.core.api.IDSPUserType user)
    {
        generatedSetterHelperImpl(user, USER$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "User" element
     */
    public com.idanalytics.core.api.IDSPUserType addNewUser()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.IDSPUserType target = null;
            target = (com.idanalytics.core.api.IDSPUserType)get_store().add_element_user(USER$0);
            return target;
        }
    }
    
    /**
     * Gets the "Token" element
     */
    public java.lang.String getToken()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TOKEN$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Token" element
     */
    public com.idanalytics.core.api.TokenType xgetToken()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.TokenType target = null;
            target = (com.idanalytics.core.api.TokenType)get_store().find_element_user(TOKEN$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Token" element
     */
    public void setToken(java.lang.String token)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TOKEN$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TOKEN$2);
            }
            target.setStringValue(token);
        }
    }
    
    /**
     * Sets (as xml) the "Token" element
     */
    public void xsetToken(com.idanalytics.core.api.TokenType token)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.TokenType target = null;
            target = (com.idanalytics.core.api.TokenType)get_store().find_element_user(TOKEN$2, 0);
            if (target == null)
            {
                target = (com.idanalytics.core.api.TokenType)get_store().add_element_user(TOKEN$2);
            }
            target.set(token);
        }
    }
}
