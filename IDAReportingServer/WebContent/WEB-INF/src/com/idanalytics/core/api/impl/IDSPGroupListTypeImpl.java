/*
 * XML Type:  IDSPGroupListType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.IDSPGroupListType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML IDSPGroupListType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public class IDSPGroupListTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.core.api.IDSPGroupListType
{
    private static final long serialVersionUID = 1L;
    
    public IDSPGroupListTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GROUP$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "Group");
    
    
    /**
     * Gets array of all "Group" elements
     */
    public com.idanalytics.core.api.IDSPGroupType[] getGroupArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(GROUP$0, targetList);
            com.idanalytics.core.api.IDSPGroupType[] result = new com.idanalytics.core.api.IDSPGroupType[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "Group" element
     */
    public com.idanalytics.core.api.IDSPGroupType getGroupArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.IDSPGroupType target = null;
            target = (com.idanalytics.core.api.IDSPGroupType)get_store().find_element_user(GROUP$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "Group" element
     */
    public int sizeOfGroupArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(GROUP$0);
        }
    }
    
    /**
     * Sets array of all "Group" element  WARNING: This method is not atomicaly synchronized.
     */
    public void setGroupArray(com.idanalytics.core.api.IDSPGroupType[] groupArray)
    {
        check_orphaned();
        arraySetterHelper(groupArray, GROUP$0);
    }
    
    /**
     * Sets ith "Group" element
     */
    public void setGroupArray(int i, com.idanalytics.core.api.IDSPGroupType group)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.IDSPGroupType target = null;
            target = (com.idanalytics.core.api.IDSPGroupType)get_store().find_element_user(GROUP$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(group);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "Group" element
     */
    public com.idanalytics.core.api.IDSPGroupType insertNewGroup(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.IDSPGroupType target = null;
            target = (com.idanalytics.core.api.IDSPGroupType)get_store().insert_element_user(GROUP$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "Group" element
     */
    public com.idanalytics.core.api.IDSPGroupType addNewGroup()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.IDSPGroupType target = null;
            target = (com.idanalytics.core.api.IDSPGroupType)get_store().add_element_user(GROUP$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "Group" element
     */
    public void removeGroup(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(GROUP$0, i);
        }
    }
}
