/*
 * XML Type:  GetClientProfileMetadataCommandType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.GetClientProfileMetadataCommandType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML GetClientProfileMetadataCommandType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public class GetClientProfileMetadataCommandTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.core.api.GetClientProfileMetadataCommandType
{
    private static final long serialVersionUID = 1L;
    
    public GetClientProfileMetadataCommandTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SOLUTION$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "Solution");
    private static final javax.xml.namespace.QName TOKEN$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "Token");
    
    
    /**
     * Gets the "Solution" element
     */
    public java.lang.String getSolution()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SOLUTION$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Solution" element
     */
    public com.idanalytics.core.api.PathType xgetSolution()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.PathType target = null;
            target = (com.idanalytics.core.api.PathType)get_store().find_element_user(SOLUTION$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Solution" element
     */
    public void setSolution(java.lang.String solution)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SOLUTION$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SOLUTION$0);
            }
            target.setStringValue(solution);
        }
    }
    
    /**
     * Sets (as xml) the "Solution" element
     */
    public void xsetSolution(com.idanalytics.core.api.PathType solution)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.PathType target = null;
            target = (com.idanalytics.core.api.PathType)get_store().find_element_user(SOLUTION$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.core.api.PathType)get_store().add_element_user(SOLUTION$0);
            }
            target.set(solution);
        }
    }
    
    /**
     * Gets the "Token" element
     */
    public java.lang.String getToken()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TOKEN$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Token" element
     */
    public com.idanalytics.core.api.TokenType xgetToken()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.TokenType target = null;
            target = (com.idanalytics.core.api.TokenType)get_store().find_element_user(TOKEN$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Token" element
     */
    public void setToken(java.lang.String token)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TOKEN$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TOKEN$2);
            }
            target.setStringValue(token);
        }
    }
    
    /**
     * Sets (as xml) the "Token" element
     */
    public void xsetToken(com.idanalytics.core.api.TokenType token)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.TokenType target = null;
            target = (com.idanalytics.core.api.TokenType)get_store().find_element_user(TOKEN$2, 0);
            if (target == null)
            {
                target = (com.idanalytics.core.api.TokenType)get_store().add_element_user(TOKEN$2);
            }
            target.set(token);
        }
    }
}
