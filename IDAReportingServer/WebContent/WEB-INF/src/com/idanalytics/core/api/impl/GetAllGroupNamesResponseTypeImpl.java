/*
 * XML Type:  GetAllGroupNamesResponseType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.GetAllGroupNamesResponseType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML GetAllGroupNamesResponseType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public class GetAllGroupNamesResponseTypeImpl extends com.idanalytics.core.api.impl.IDSPResponseTypeImpl implements com.idanalytics.core.api.GetAllGroupNamesResponseType
{
    private static final long serialVersionUID = 1L;
    
    public GetAllGroupNamesResponseTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GROUPNAMES$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "GroupNames");
    
    
    /**
     * Gets the "GroupNames" element
     */
    public com.idanalytics.core.api.GroupNameListType getGroupNames()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.GroupNameListType target = null;
            target = (com.idanalytics.core.api.GroupNameListType)get_store().find_element_user(GROUPNAMES$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "GroupNames" element
     */
    public void setGroupNames(com.idanalytics.core.api.GroupNameListType groupNames)
    {
        generatedSetterHelperImpl(groupNames, GROUPNAMES$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "GroupNames" element
     */
    public com.idanalytics.core.api.GroupNameListType addNewGroupNames()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.GroupNameListType target = null;
            target = (com.idanalytics.core.api.GroupNameListType)get_store().add_element_user(GROUPNAMES$0);
            return target;
        }
    }
}
