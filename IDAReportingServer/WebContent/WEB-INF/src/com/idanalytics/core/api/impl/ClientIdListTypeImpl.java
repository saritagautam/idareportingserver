/*
 * XML Type:  ClientIdListType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.ClientIdListType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML ClientIdListType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public class ClientIdListTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.core.api.ClientIdListType
{
    private static final long serialVersionUID = 1L;
    
    public ClientIdListTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CLIENTID$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "ClientID");
    
    
    /**
     * Gets array of all "ClientID" elements
     */
    public java.lang.String[] getClientIDArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(CLIENTID$0, targetList);
            java.lang.String[] result = new java.lang.String[targetList.size()];
            for (int i = 0, len = targetList.size() ; i < len ; i++)
                result[i] = ((org.apache.xmlbeans.SimpleValue)targetList.get(i)).getStringValue();
            return result;
        }
    }
    
    /**
     * Gets ith "ClientID" element
     */
    public java.lang.String getClientIDArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CLIENTID$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) array of all "ClientID" elements
     */
    public com.idanalytics.core.api.PathType[] xgetClientIDArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(CLIENTID$0, targetList);
            com.idanalytics.core.api.PathType[] result = new com.idanalytics.core.api.PathType[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets (as xml) ith "ClientID" element
     */
    public com.idanalytics.core.api.PathType xgetClientIDArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.PathType target = null;
            target = (com.idanalytics.core.api.PathType)get_store().find_element_user(CLIENTID$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "ClientID" element
     */
    public int sizeOfClientIDArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CLIENTID$0);
        }
    }
    
    /**
     * Sets array of all "ClientID" element
     */
    public void setClientIDArray(java.lang.String[] clientIDArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(clientIDArray, CLIENTID$0);
        }
    }
    
    /**
     * Sets ith "ClientID" element
     */
    public void setClientIDArray(int i, java.lang.String clientID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CLIENTID$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.setStringValue(clientID);
        }
    }
    
    /**
     * Sets (as xml) array of all "ClientID" element
     */
    public void xsetClientIDArray(com.idanalytics.core.api.PathType[]clientIDArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(clientIDArray, CLIENTID$0);
        }
    }
    
    /**
     * Sets (as xml) ith "ClientID" element
     */
    public void xsetClientIDArray(int i, com.idanalytics.core.api.PathType clientID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.PathType target = null;
            target = (com.idanalytics.core.api.PathType)get_store().find_element_user(CLIENTID$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(clientID);
        }
    }
    
    /**
     * Inserts the value as the ith "ClientID" element
     */
    public void insertClientID(int i, java.lang.String clientID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = 
                (org.apache.xmlbeans.SimpleValue)get_store().insert_element_user(CLIENTID$0, i);
            target.setStringValue(clientID);
        }
    }
    
    /**
     * Appends the value as the last "ClientID" element
     */
    public void addClientID(java.lang.String clientID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CLIENTID$0);
            target.setStringValue(clientID);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "ClientID" element
     */
    public com.idanalytics.core.api.PathType insertNewClientID(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.PathType target = null;
            target = (com.idanalytics.core.api.PathType)get_store().insert_element_user(CLIENTID$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "ClientID" element
     */
    public com.idanalytics.core.api.PathType addNewClientID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.PathType target = null;
            target = (com.idanalytics.core.api.PathType)get_store().add_element_user(CLIENTID$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "ClientID" element
     */
    public void removeClientID(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CLIENTID$0, i);
        }
    }
}
