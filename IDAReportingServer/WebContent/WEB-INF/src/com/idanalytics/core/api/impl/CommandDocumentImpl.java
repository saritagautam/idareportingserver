/*
 * An XML document type.
 * Localname: Command
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.CommandDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * A document containing one Command(@http://idanalytics.com/core/api) element.
 *
 * This is a complex type.
 */
public class CommandDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.core.api.CommandDocument
{
    private static final long serialVersionUID = 1L;
    
    public CommandDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName COMMAND$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "Command");
    
    
    /**
     * Gets the "Command" element
     */
    public com.idanalytics.core.api.CommandType getCommand()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.CommandType target = null;
            target = (com.idanalytics.core.api.CommandType)get_store().find_element_user(COMMAND$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "Command" element
     */
    public void setCommand(com.idanalytics.core.api.CommandType command)
    {
        generatedSetterHelperImpl(command, COMMAND$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "Command" element
     */
    public com.idanalytics.core.api.CommandType addNewCommand()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.CommandType target = null;
            target = (com.idanalytics.core.api.CommandType)get_store().add_element_user(COMMAND$0);
            return target;
        }
    }
}
