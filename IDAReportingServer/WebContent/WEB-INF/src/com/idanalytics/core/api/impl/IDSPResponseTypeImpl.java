/*
 * XML Type:  IDSPResponseType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.IDSPResponseType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML IDSPResponseType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public class IDSPResponseTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.core.api.IDSPResponseType
{
    private static final long serialVersionUID = 1L;
    
    public IDSPResponseTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CHALLENGE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "Challenge");
    private static final javax.xml.namespace.QName ERROR$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "Error");
    
    
    /**
     * Gets the "Challenge" element
     */
    public com.idanalytics.core.api.ChallengeType.Enum getChallenge()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CHALLENGE$0, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.core.api.ChallengeType.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "Challenge" element
     */
    public com.idanalytics.core.api.ChallengeType xgetChallenge()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.ChallengeType target = null;
            target = (com.idanalytics.core.api.ChallengeType)get_store().find_element_user(CHALLENGE$0, 0);
            return target;
        }
    }
    
    /**
     * True if has "Challenge" element
     */
    public boolean isSetChallenge()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CHALLENGE$0) != 0;
        }
    }
    
    /**
     * Sets the "Challenge" element
     */
    public void setChallenge(com.idanalytics.core.api.ChallengeType.Enum challenge)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CHALLENGE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CHALLENGE$0);
            }
            target.setEnumValue(challenge);
        }
    }
    
    /**
     * Sets (as xml) the "Challenge" element
     */
    public void xsetChallenge(com.idanalytics.core.api.ChallengeType challenge)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.ChallengeType target = null;
            target = (com.idanalytics.core.api.ChallengeType)get_store().find_element_user(CHALLENGE$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.core.api.ChallengeType)get_store().add_element_user(CHALLENGE$0);
            }
            target.set(challenge);
        }
    }
    
    /**
     * Unsets the "Challenge" element
     */
    public void unsetChallenge()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CHALLENGE$0, 0);
        }
    }
    
    /**
     * Gets the "Error" element
     */
    public com.idanalytics.error.api.ErrorType getError()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.error.api.ErrorType target = null;
            target = (com.idanalytics.error.api.ErrorType)get_store().find_element_user(ERROR$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "Error" element
     */
    public boolean isSetError()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ERROR$2) != 0;
        }
    }
    
    /**
     * Sets the "Error" element
     */
    public void setError(com.idanalytics.error.api.ErrorType error)
    {
        generatedSetterHelperImpl(error, ERROR$2, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "Error" element
     */
    public com.idanalytics.error.api.ErrorType addNewError()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.error.api.ErrorType target = null;
            target = (com.idanalytics.error.api.ErrorType)get_store().add_element_user(ERROR$2);
            return target;
        }
    }
    
    /**
     * Unsets the "Error" element
     */
    public void unsetError()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ERROR$2, 0);
        }
    }
}
