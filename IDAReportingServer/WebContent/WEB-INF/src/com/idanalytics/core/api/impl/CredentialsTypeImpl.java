/*
 * XML Type:  CredentialsType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.CredentialsType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML CredentialsType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public class CredentialsTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.core.api.CredentialsType
{
    private static final long serialVersionUID = 1L;
    
    public CredentialsTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName USERNAME$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "Username");
    private static final javax.xml.namespace.QName PASSWORD$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "Password");
    
    
    /**
     * Gets the "Username" element
     */
    public java.lang.String getUsername()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(USERNAME$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Username" element
     */
    public com.idanalytics.core.api.UsernameType xgetUsername()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.UsernameType target = null;
            target = (com.idanalytics.core.api.UsernameType)get_store().find_element_user(USERNAME$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Username" element
     */
    public void setUsername(java.lang.String username)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(USERNAME$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(USERNAME$0);
            }
            target.setStringValue(username);
        }
    }
    
    /**
     * Sets (as xml) the "Username" element
     */
    public void xsetUsername(com.idanalytics.core.api.UsernameType username)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.UsernameType target = null;
            target = (com.idanalytics.core.api.UsernameType)get_store().find_element_user(USERNAME$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.core.api.UsernameType)get_store().add_element_user(USERNAME$0);
            }
            target.set(username);
        }
    }
    
    /**
     * Gets the "Password" element
     */
    public java.lang.String getPassword()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSWORD$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Password" element
     */
    public com.idanalytics.core.api.PasswordType xgetPassword()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.PasswordType target = null;
            target = (com.idanalytics.core.api.PasswordType)get_store().find_element_user(PASSWORD$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Password" element
     */
    public void setPassword(java.lang.String password)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSWORD$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSWORD$2);
            }
            target.setStringValue(password);
        }
    }
    
    /**
     * Sets (as xml) the "Password" element
     */
    public void xsetPassword(com.idanalytics.core.api.PasswordType password)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.PasswordType target = null;
            target = (com.idanalytics.core.api.PasswordType)get_store().find_element_user(PASSWORD$2, 0);
            if (target == null)
            {
                target = (com.idanalytics.core.api.PasswordType)get_store().add_element_user(PASSWORD$2);
            }
            target.set(password);
        }
    }
}
