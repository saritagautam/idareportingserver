/*
 * XML Type:  AuthCommandType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.AuthCommandType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML AuthCommandType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public class AuthCommandTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.core.api.AuthCommandType
{
    private static final long serialVersionUID = 1L;
    
    public AuthCommandTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CREDENTIALS$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "Credentials");
    private static final javax.xml.namespace.QName TOKEN$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "Token");
    
    
    /**
     * Gets the "Credentials" element
     */
    public com.idanalytics.core.api.CredentialsType getCredentials()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.CredentialsType target = null;
            target = (com.idanalytics.core.api.CredentialsType)get_store().find_element_user(CREDENTIALS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "Credentials" element
     */
    public void setCredentials(com.idanalytics.core.api.CredentialsType credentials)
    {
        generatedSetterHelperImpl(credentials, CREDENTIALS$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "Credentials" element
     */
    public com.idanalytics.core.api.CredentialsType addNewCredentials()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.CredentialsType target = null;
            target = (com.idanalytics.core.api.CredentialsType)get_store().add_element_user(CREDENTIALS$0);
            return target;
        }
    }
    
    /**
     * Gets the "Token" element
     */
    public java.lang.String getToken()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TOKEN$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Token" element
     */
    public com.idanalytics.core.api.TokenType xgetToken()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.TokenType target = null;
            target = (com.idanalytics.core.api.TokenType)get_store().find_element_user(TOKEN$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Token" element
     */
    public void setToken(java.lang.String token)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TOKEN$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TOKEN$2);
            }
            target.setStringValue(token);
        }
    }
    
    /**
     * Sets (as xml) the "Token" element
     */
    public void xsetToken(com.idanalytics.core.api.TokenType token)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.TokenType target = null;
            target = (com.idanalytics.core.api.TokenType)get_store().find_element_user(TOKEN$2, 0);
            if (target == null)
            {
                target = (com.idanalytics.core.api.TokenType)get_store().add_element_user(TOKEN$2);
            }
            target.set(token);
        }
    }
}
