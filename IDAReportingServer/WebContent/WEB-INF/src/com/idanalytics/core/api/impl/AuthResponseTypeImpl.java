/*
 * XML Type:  AuthResponseType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.AuthResponseType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML AuthResponseType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public class AuthResponseTypeImpl extends com.idanalytics.core.api.impl.IDSPResponseTypeImpl implements com.idanalytics.core.api.AuthResponseType
{
    private static final long serialVersionUID = 1L;
    
    public AuthResponseTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CLIENT$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "Client");
    private static final javax.xml.namespace.QName SOLUTIONLIST$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "SolutionList");
    
    
    /**
     * Gets the "Client" element
     */
    public java.lang.String getClient()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CLIENT$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Client" element
     */
    public com.idanalytics.core.api.PathType xgetClient()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.PathType target = null;
            target = (com.idanalytics.core.api.PathType)get_store().find_element_user(CLIENT$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Client" element
     */
    public void setClient(java.lang.String client)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CLIENT$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CLIENT$0);
            }
            target.setStringValue(client);
        }
    }
    
    /**
     * Sets (as xml) the "Client" element
     */
    public void xsetClient(com.idanalytics.core.api.PathType client)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.PathType target = null;
            target = (com.idanalytics.core.api.PathType)get_store().find_element_user(CLIENT$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.core.api.PathType)get_store().add_element_user(CLIENT$0);
            }
            target.set(client);
        }
    }
    
    /**
     * Gets the "SolutionList" element
     */
    public com.idanalytics.core.api.SolutionListType getSolutionList()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.SolutionListType target = null;
            target = (com.idanalytics.core.api.SolutionListType)get_store().find_element_user(SOLUTIONLIST$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "SolutionList" element
     */
    public void setSolutionList(com.idanalytics.core.api.SolutionListType solutionList)
    {
        generatedSetterHelperImpl(solutionList, SOLUTIONLIST$2, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "SolutionList" element
     */
    public com.idanalytics.core.api.SolutionListType addNewSolutionList()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.SolutionListType target = null;
            target = (com.idanalytics.core.api.SolutionListType)get_store().add_element_user(SOLUTIONLIST$2);
            return target;
        }
    }
}
