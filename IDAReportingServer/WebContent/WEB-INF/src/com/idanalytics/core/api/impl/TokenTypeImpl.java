/*
 * XML Type:  TokenType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.TokenType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML TokenType(@http://idanalytics.com/core/api).
 *
 * This is an atomic type that is a restriction of com.idanalytics.core.api.TokenType.
 */
public class TokenTypeImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.core.api.TokenType
{
    private static final long serialVersionUID = 1L;
    
    public TokenTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected TokenTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
