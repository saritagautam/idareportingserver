/*
 * XML Type:  ResponseType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.ResponseType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML ResponseType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public class ResponseTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.core.api.ResponseType
{
    private static final long serialVersionUID = 1L;
    
    public ResponseTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CHALLENGE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "Challenge");
    private static final javax.xml.namespace.QName SOLUTION$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "Solution");
    private static final javax.xml.namespace.QName REQUESTID$4 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "RequestID");
    private static final javax.xml.namespace.QName VIEW$6 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "View");
    private static final javax.xml.namespace.QName BODY$8 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "Body");
    
    
    /**
     * Gets the "Challenge" element
     */
    public com.idanalytics.core.api.ChallengeType.Enum getChallenge()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CHALLENGE$0, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.core.api.ChallengeType.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "Challenge" element
     */
    public com.idanalytics.core.api.ChallengeType xgetChallenge()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.ChallengeType target = null;
            target = (com.idanalytics.core.api.ChallengeType)get_store().find_element_user(CHALLENGE$0, 0);
            return target;
        }
    }
    
    /**
     * True if has "Challenge" element
     */
    public boolean isSetChallenge()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CHALLENGE$0) != 0;
        }
    }
    
    /**
     * Sets the "Challenge" element
     */
    public void setChallenge(com.idanalytics.core.api.ChallengeType.Enum challenge)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CHALLENGE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CHALLENGE$0);
            }
            target.setEnumValue(challenge);
        }
    }
    
    /**
     * Sets (as xml) the "Challenge" element
     */
    public void xsetChallenge(com.idanalytics.core.api.ChallengeType challenge)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.ChallengeType target = null;
            target = (com.idanalytics.core.api.ChallengeType)get_store().find_element_user(CHALLENGE$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.core.api.ChallengeType)get_store().add_element_user(CHALLENGE$0);
            }
            target.set(challenge);
        }
    }
    
    /**
     * Unsets the "Challenge" element
     */
    public void unsetChallenge()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CHALLENGE$0, 0);
        }
    }
    
    /**
     * Gets the "Solution" element
     */
    public java.lang.String getSolution()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SOLUTION$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Solution" element
     */
    public com.idanalytics.core.api.PathType xgetSolution()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.PathType target = null;
            target = (com.idanalytics.core.api.PathType)get_store().find_element_user(SOLUTION$2, 0);
            return target;
        }
    }
    
    /**
     * True if has "Solution" element
     */
    public boolean isSetSolution()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SOLUTION$2) != 0;
        }
    }
    
    /**
     * Sets the "Solution" element
     */
    public void setSolution(java.lang.String solution)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SOLUTION$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SOLUTION$2);
            }
            target.setStringValue(solution);
        }
    }
    
    /**
     * Sets (as xml) the "Solution" element
     */
    public void xsetSolution(com.idanalytics.core.api.PathType solution)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.PathType target = null;
            target = (com.idanalytics.core.api.PathType)get_store().find_element_user(SOLUTION$2, 0);
            if (target == null)
            {
                target = (com.idanalytics.core.api.PathType)get_store().add_element_user(SOLUTION$2);
            }
            target.set(solution);
        }
    }
    
    /**
     * Unsets the "Solution" element
     */
    public void unsetSolution()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SOLUTION$2, 0);
        }
    }
    
    /**
     * Gets the "RequestID" element
     */
    public java.lang.String getRequestID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(REQUESTID$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "RequestID" element
     */
    public com.idanalytics.core.api.RequestIDType xgetRequestID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.RequestIDType target = null;
            target = (com.idanalytics.core.api.RequestIDType)get_store().find_element_user(REQUESTID$4, 0);
            return target;
        }
    }
    
    /**
     * True if has "RequestID" element
     */
    public boolean isSetRequestID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(REQUESTID$4) != 0;
        }
    }
    
    /**
     * Sets the "RequestID" element
     */
    public void setRequestID(java.lang.String requestID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(REQUESTID$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(REQUESTID$4);
            }
            target.setStringValue(requestID);
        }
    }
    
    /**
     * Sets (as xml) the "RequestID" element
     */
    public void xsetRequestID(com.idanalytics.core.api.RequestIDType requestID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.RequestIDType target = null;
            target = (com.idanalytics.core.api.RequestIDType)get_store().find_element_user(REQUESTID$4, 0);
            if (target == null)
            {
                target = (com.idanalytics.core.api.RequestIDType)get_store().add_element_user(REQUESTID$4);
            }
            target.set(requestID);
        }
    }
    
    /**
     * Unsets the "RequestID" element
     */
    public void unsetRequestID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(REQUESTID$4, 0);
        }
    }
    
    /**
     * Gets the "View" element
     */
    public java.lang.String getView()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(VIEW$6, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "View" element
     */
    public com.idanalytics.core.api.PathType xgetView()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.PathType target = null;
            target = (com.idanalytics.core.api.PathType)get_store().find_element_user(VIEW$6, 0);
            return target;
        }
    }
    
    /**
     * True if has "View" element
     */
    public boolean isSetView()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(VIEW$6) != 0;
        }
    }
    
    /**
     * Sets the "View" element
     */
    public void setView(java.lang.String view)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(VIEW$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(VIEW$6);
            }
            target.setStringValue(view);
        }
    }
    
    /**
     * Sets (as xml) the "View" element
     */
    public void xsetView(com.idanalytics.core.api.PathType view)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.PathType target = null;
            target = (com.idanalytics.core.api.PathType)get_store().find_element_user(VIEW$6, 0);
            if (target == null)
            {
                target = (com.idanalytics.core.api.PathType)get_store().add_element_user(VIEW$6);
            }
            target.set(view);
        }
    }
    
    /**
     * Unsets the "View" element
     */
    public void unsetView()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(VIEW$6, 0);
        }
    }
    
    /**
     * Gets the "Body" element
     */
    public com.idanalytics.core.api.BodyType getBody()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.BodyType target = null;
            target = (com.idanalytics.core.api.BodyType)get_store().find_element_user(BODY$8, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "Body" element
     */
    public boolean isSetBody()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(BODY$8) != 0;
        }
    }
    
    /**
     * Sets the "Body" element
     */
    public void setBody(com.idanalytics.core.api.BodyType body)
    {
        generatedSetterHelperImpl(body, BODY$8, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "Body" element
     */
    public com.idanalytics.core.api.BodyType addNewBody()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.BodyType target = null;
            target = (com.idanalytics.core.api.BodyType)get_store().add_element_user(BODY$8);
            return target;
        }
    }
    
    /**
     * Unsets the "Body" element
     */
    public void unsetBody()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(BODY$8, 0);
        }
    }
}
