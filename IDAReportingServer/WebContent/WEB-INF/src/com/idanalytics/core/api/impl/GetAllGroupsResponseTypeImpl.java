/*
 * XML Type:  GetAllGroupsResponseType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.GetAllGroupsResponseType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML GetAllGroupsResponseType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public class GetAllGroupsResponseTypeImpl extends com.idanalytics.core.api.impl.IDSPResponseTypeImpl implements com.idanalytics.core.api.GetAllGroupsResponseType
{
    private static final long serialVersionUID = 1L;
    
    public GetAllGroupsResponseTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GROUPLIST$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "GroupList");
    
    
    /**
     * Gets the "GroupList" element
     */
    public com.idanalytics.core.api.IDSPGroupListType getGroupList()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.IDSPGroupListType target = null;
            target = (com.idanalytics.core.api.IDSPGroupListType)get_store().find_element_user(GROUPLIST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "GroupList" element
     */
    public void setGroupList(com.idanalytics.core.api.IDSPGroupListType groupList)
    {
        generatedSetterHelperImpl(groupList, GROUPLIST$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "GroupList" element
     */
    public com.idanalytics.core.api.IDSPGroupListType addNewGroupList()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.IDSPGroupListType target = null;
            target = (com.idanalytics.core.api.IDSPGroupListType)get_store().add_element_user(GROUPLIST$0);
            return target;
        }
    }
}
