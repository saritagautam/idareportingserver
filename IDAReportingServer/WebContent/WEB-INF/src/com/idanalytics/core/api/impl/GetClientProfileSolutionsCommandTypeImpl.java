/*
 * XML Type:  GetClientProfileSolutionsCommandType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.GetClientProfileSolutionsCommandType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML GetClientProfileSolutionsCommandType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public class GetClientProfileSolutionsCommandTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.core.api.GetClientProfileSolutionsCommandType
{
    private static final long serialVersionUID = 1L;
    
    public GetClientProfileSolutionsCommandTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName TOKEN$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "Token");
    private static final javax.xml.namespace.QName CLIENTID$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "ClientId");
    
    
    /**
     * Gets the "Token" element
     */
    public java.lang.String getToken()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TOKEN$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Token" element
     */
    public com.idanalytics.core.api.TokenType xgetToken()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.TokenType target = null;
            target = (com.idanalytics.core.api.TokenType)get_store().find_element_user(TOKEN$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Token" element
     */
    public void setToken(java.lang.String token)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TOKEN$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TOKEN$0);
            }
            target.setStringValue(token);
        }
    }
    
    /**
     * Sets (as xml) the "Token" element
     */
    public void xsetToken(com.idanalytics.core.api.TokenType token)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.TokenType target = null;
            target = (com.idanalytics.core.api.TokenType)get_store().find_element_user(TOKEN$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.core.api.TokenType)get_store().add_element_user(TOKEN$0);
            }
            target.set(token);
        }
    }
    
    /**
     * Gets the "ClientId" element
     */
    public java.lang.String getClientId()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CLIENTID$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "ClientId" element
     */
    public com.idanalytics.core.api.PathType xgetClientId()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.PathType target = null;
            target = (com.idanalytics.core.api.PathType)get_store().find_element_user(CLIENTID$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "ClientId" element
     */
    public void setClientId(java.lang.String clientId)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CLIENTID$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CLIENTID$2);
            }
            target.setStringValue(clientId);
        }
    }
    
    /**
     * Sets (as xml) the "ClientId" element
     */
    public void xsetClientId(com.idanalytics.core.api.PathType clientId)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.PathType target = null;
            target = (com.idanalytics.core.api.PathType)get_store().find_element_user(CLIENTID$2, 0);
            if (target == null)
            {
                target = (com.idanalytics.core.api.PathType)get_store().add_element_user(CLIENTID$2);
            }
            target.set(clientId);
        }
    }
}
