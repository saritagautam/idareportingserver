/*
 * XML Type:  ResponseType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.ResponseType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api;


/**
 * An XML ResponseType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public interface ResponseType extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ResponseType.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("responsetypea4b1type");
    
    /**
     * Gets the "Challenge" element
     */
    com.idanalytics.core.api.ChallengeType.Enum getChallenge();
    
    /**
     * Gets (as xml) the "Challenge" element
     */
    com.idanalytics.core.api.ChallengeType xgetChallenge();
    
    /**
     * True if has "Challenge" element
     */
    boolean isSetChallenge();
    
    /**
     * Sets the "Challenge" element
     */
    void setChallenge(com.idanalytics.core.api.ChallengeType.Enum challenge);
    
    /**
     * Sets (as xml) the "Challenge" element
     */
    void xsetChallenge(com.idanalytics.core.api.ChallengeType challenge);
    
    /**
     * Unsets the "Challenge" element
     */
    void unsetChallenge();
    
    /**
     * Gets the "Solution" element
     */
    java.lang.String getSolution();
    
    /**
     * Gets (as xml) the "Solution" element
     */
    com.idanalytics.core.api.PathType xgetSolution();
    
    /**
     * True if has "Solution" element
     */
    boolean isSetSolution();
    
    /**
     * Sets the "Solution" element
     */
    void setSolution(java.lang.String solution);
    
    /**
     * Sets (as xml) the "Solution" element
     */
    void xsetSolution(com.idanalytics.core.api.PathType solution);
    
    /**
     * Unsets the "Solution" element
     */
    void unsetSolution();
    
    /**
     * Gets the "RequestID" element
     */
    java.lang.String getRequestID();
    
    /**
     * Gets (as xml) the "RequestID" element
     */
    com.idanalytics.core.api.RequestIDType xgetRequestID();
    
    /**
     * True if has "RequestID" element
     */
    boolean isSetRequestID();
    
    /**
     * Sets the "RequestID" element
     */
    void setRequestID(java.lang.String requestID);
    
    /**
     * Sets (as xml) the "RequestID" element
     */
    void xsetRequestID(com.idanalytics.core.api.RequestIDType requestID);
    
    /**
     * Unsets the "RequestID" element
     */
    void unsetRequestID();
    
    /**
     * Gets the "View" element
     */
    java.lang.String getView();
    
    /**
     * Gets (as xml) the "View" element
     */
    com.idanalytics.core.api.PathType xgetView();
    
    /**
     * True if has "View" element
     */
    boolean isSetView();
    
    /**
     * Sets the "View" element
     */
    void setView(java.lang.String view);
    
    /**
     * Sets (as xml) the "View" element
     */
    void xsetView(com.idanalytics.core.api.PathType view);
    
    /**
     * Unsets the "View" element
     */
    void unsetView();
    
    /**
     * Gets the "Body" element
     */
    com.idanalytics.core.api.BodyType getBody();
    
    /**
     * True if has "Body" element
     */
    boolean isSetBody();
    
    /**
     * Sets the "Body" element
     */
    void setBody(com.idanalytics.core.api.BodyType body);
    
    /**
     * Appends and returns a new empty "Body" element
     */
    com.idanalytics.core.api.BodyType addNewBody();
    
    /**
     * Unsets the "Body" element
     */
    void unsetBody();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.core.api.ResponseType newInstance() {
          return (com.idanalytics.core.api.ResponseType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.core.api.ResponseType newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.core.api.ResponseType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.core.api.ResponseType parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.core.api.ResponseType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.core.api.ResponseType parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.core.api.ResponseType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.core.api.ResponseType parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.ResponseType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.core.api.ResponseType parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.ResponseType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.core.api.ResponseType parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.ResponseType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.core.api.ResponseType parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.ResponseType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.core.api.ResponseType parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.ResponseType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.core.api.ResponseType parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.ResponseType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.core.api.ResponseType parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.ResponseType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.core.api.ResponseType parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.ResponseType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.core.api.ResponseType parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.core.api.ResponseType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.core.api.ResponseType parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.core.api.ResponseType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.core.api.ResponseType parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.core.api.ResponseType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.core.api.ResponseType parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.core.api.ResponseType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.core.api.ResponseType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.core.api.ResponseType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.core.api.ResponseType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.core.api.ResponseType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
