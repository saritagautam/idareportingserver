/*
 * XML Type:  RequestIDType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.RequestIDType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML RequestIDType(@http://idanalytics.com/core/api).
 *
 * This is an atomic type that is a restriction of com.idanalytics.core.api.RequestIDType.
 */
public class RequestIDTypeImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.core.api.RequestIDType
{
    private static final long serialVersionUID = 1L;
    
    public RequestIDTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected RequestIDTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
