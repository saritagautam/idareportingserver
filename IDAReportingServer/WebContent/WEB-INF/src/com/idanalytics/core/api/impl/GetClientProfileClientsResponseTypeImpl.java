/*
 * XML Type:  GetClientProfileClientsResponseType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.GetClientProfileClientsResponseType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML GetClientProfileClientsResponseType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public class GetClientProfileClientsResponseTypeImpl extends com.idanalytics.core.api.impl.IDSPResponseTypeImpl implements com.idanalytics.core.api.GetClientProfileClientsResponseType
{
    private static final long serialVersionUID = 1L;
    
    public GetClientProfileClientsResponseTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CLIENTIDS$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "ClientIds");
    
    
    /**
     * Gets the "ClientIds" element
     */
    public com.idanalytics.core.api.ClientIdListType getClientIds()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.ClientIdListType target = null;
            target = (com.idanalytics.core.api.ClientIdListType)get_store().find_element_user(CLIENTIDS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "ClientIds" element
     */
    public void setClientIds(com.idanalytics.core.api.ClientIdListType clientIds)
    {
        generatedSetterHelperImpl(clientIds, CLIENTIDS$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "ClientIds" element
     */
    public com.idanalytics.core.api.ClientIdListType addNewClientIds()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.ClientIdListType target = null;
            target = (com.idanalytics.core.api.ClientIdListType)get_store().add_element_user(CLIENTIDS$0);
            return target;
        }
    }
}
