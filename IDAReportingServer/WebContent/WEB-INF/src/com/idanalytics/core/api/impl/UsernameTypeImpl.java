/*
 * XML Type:  UsernameType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.UsernameType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML UsernameType(@http://idanalytics.com/core/api).
 *
 * This is an atomic type that is a restriction of com.idanalytics.core.api.UsernameType.
 */
public class UsernameTypeImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.core.api.UsernameType
{
    private static final long serialVersionUID = 1L;
    
    public UsernameTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected UsernameTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
