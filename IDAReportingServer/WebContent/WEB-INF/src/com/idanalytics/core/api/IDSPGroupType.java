/*
 * XML Type:  IDSPGroupType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.IDSPGroupType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api;


/**
 * An XML IDSPGroupType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public interface IDSPGroupType extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(IDSPGroupType.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("idspgrouptypefd45type");
    
    /**
     * Gets the "GroupName" element
     */
    java.lang.String getGroupName();
    
    /**
     * Gets (as xml) the "GroupName" element
     */
    com.idanalytics.core.api.GroupType xgetGroupName();
    
    /**
     * Sets the "GroupName" element
     */
    void setGroupName(java.lang.String groupName);
    
    /**
     * Sets (as xml) the "GroupName" element
     */
    void xsetGroupName(com.idanalytics.core.api.GroupType groupName);
    
    /**
     * Gets the "Description" element
     */
    java.lang.String getDescription();
    
    /**
     * Gets (as xml) the "Description" element
     */
    org.apache.xmlbeans.XmlString xgetDescription();
    
    /**
     * Sets the "Description" element
     */
    void setDescription(java.lang.String description);
    
    /**
     * Sets (as xml) the "Description" element
     */
    void xsetDescription(org.apache.xmlbeans.XmlString description);
    
    /**
     * Gets the "Users" element
     */
    com.idanalytics.core.api.IDSPUserListType getUsers();
    
    /**
     * Tests for nil "Users" element
     */
    boolean isNilUsers();
    
    /**
     * True if has "Users" element
     */
    boolean isSetUsers();
    
    /**
     * Sets the "Users" element
     */
    void setUsers(com.idanalytics.core.api.IDSPUserListType users);
    
    /**
     * Appends and returns a new empty "Users" element
     */
    com.idanalytics.core.api.IDSPUserListType addNewUsers();
    
    /**
     * Nils the "Users" element
     */
    void setNilUsers();
    
    /**
     * Unsets the "Users" element
     */
    void unsetUsers();
    
    /**
     * Gets the "AssignedSolutions" element
     */
    com.idanalytics.core.api.SolutionListType getAssignedSolutions();
    
    /**
     * Tests for nil "AssignedSolutions" element
     */
    boolean isNilAssignedSolutions();
    
    /**
     * True if has "AssignedSolutions" element
     */
    boolean isSetAssignedSolutions();
    
    /**
     * Sets the "AssignedSolutions" element
     */
    void setAssignedSolutions(com.idanalytics.core.api.SolutionListType assignedSolutions);
    
    /**
     * Appends and returns a new empty "AssignedSolutions" element
     */
    com.idanalytics.core.api.SolutionListType addNewAssignedSolutions();
    
    /**
     * Nils the "AssignedSolutions" element
     */
    void setNilAssignedSolutions();
    
    /**
     * Unsets the "AssignedSolutions" element
     */
    void unsetAssignedSolutions();
    
    /**
     * Gets the "DeletedSolutions" element
     */
    com.idanalytics.core.api.SolutionListType getDeletedSolutions();
    
    /**
     * Tests for nil "DeletedSolutions" element
     */
    boolean isNilDeletedSolutions();
    
    /**
     * True if has "DeletedSolutions" element
     */
    boolean isSetDeletedSolutions();
    
    /**
     * Sets the "DeletedSolutions" element
     */
    void setDeletedSolutions(com.idanalytics.core.api.SolutionListType deletedSolutions);
    
    /**
     * Appends and returns a new empty "DeletedSolutions" element
     */
    com.idanalytics.core.api.SolutionListType addNewDeletedSolutions();
    
    /**
     * Nils the "DeletedSolutions" element
     */
    void setNilDeletedSolutions();
    
    /**
     * Unsets the "DeletedSolutions" element
     */
    void unsetDeletedSolutions();
    
    /**
     * Gets the "AddedSolutions" element
     */
    com.idanalytics.core.api.SolutionListType getAddedSolutions();
    
    /**
     * Tests for nil "AddedSolutions" element
     */
    boolean isNilAddedSolutions();
    
    /**
     * True if has "AddedSolutions" element
     */
    boolean isSetAddedSolutions();
    
    /**
     * Sets the "AddedSolutions" element
     */
    void setAddedSolutions(com.idanalytics.core.api.SolutionListType addedSolutions);
    
    /**
     * Appends and returns a new empty "AddedSolutions" element
     */
    com.idanalytics.core.api.SolutionListType addNewAddedSolutions();
    
    /**
     * Nils the "AddedSolutions" element
     */
    void setNilAddedSolutions();
    
    /**
     * Unsets the "AddedSolutions" element
     */
    void unsetAddedSolutions();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.core.api.IDSPGroupType newInstance() {
          return (com.idanalytics.core.api.IDSPGroupType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.core.api.IDSPGroupType newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.core.api.IDSPGroupType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.core.api.IDSPGroupType parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.core.api.IDSPGroupType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.core.api.IDSPGroupType parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.core.api.IDSPGroupType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.core.api.IDSPGroupType parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.IDSPGroupType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.core.api.IDSPGroupType parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.IDSPGroupType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.core.api.IDSPGroupType parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.IDSPGroupType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.core.api.IDSPGroupType parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.IDSPGroupType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.core.api.IDSPGroupType parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.IDSPGroupType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.core.api.IDSPGroupType parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.IDSPGroupType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.core.api.IDSPGroupType parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.IDSPGroupType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.core.api.IDSPGroupType parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.IDSPGroupType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.core.api.IDSPGroupType parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.core.api.IDSPGroupType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.core.api.IDSPGroupType parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.core.api.IDSPGroupType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.core.api.IDSPGroupType parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.core.api.IDSPGroupType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.core.api.IDSPGroupType parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.core.api.IDSPGroupType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.core.api.IDSPGroupType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.core.api.IDSPGroupType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.core.api.IDSPGroupType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.core.api.IDSPGroupType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
