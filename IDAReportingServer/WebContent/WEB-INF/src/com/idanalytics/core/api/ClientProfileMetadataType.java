/*
 * XML Type:  ClientProfileMetadataType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.ClientProfileMetadataType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api;


/**
 * An XML ClientProfileMetadataType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public interface ClientProfileMetadataType extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ClientProfileMetadataType.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("clientprofilemetadatatypebbabtype");
    
    /**
     * Gets the "Key" element
     */
    java.lang.String getKey();
    
    /**
     * Gets (as xml) the "Key" element
     */
    org.apache.xmlbeans.XmlString xgetKey();
    
    /**
     * Sets the "Key" element
     */
    void setKey(java.lang.String key);
    
    /**
     * Sets (as xml) the "Key" element
     */
    void xsetKey(org.apache.xmlbeans.XmlString key);
    
    /**
     * Gets the "Type" element
     */
    java.lang.String getType();
    
    /**
     * Gets (as xml) the "Type" element
     */
    org.apache.xmlbeans.XmlString xgetType();
    
    /**
     * Sets the "Type" element
     */
    void setType(java.lang.String type);
    
    /**
     * Sets (as xml) the "Type" element
     */
    void xsetType(org.apache.xmlbeans.XmlString type);
    
    /**
     * Gets the "Required" element
     */
    boolean getRequired();
    
    /**
     * Gets (as xml) the "Required" element
     */
    org.apache.xmlbeans.XmlBoolean xgetRequired();
    
    /**
     * Sets the "Required" element
     */
    void setRequired(boolean required);
    
    /**
     * Sets (as xml) the "Required" element
     */
    void xsetRequired(org.apache.xmlbeans.XmlBoolean required);
    
    /**
     * Gets the "DefaultValue" element
     */
    java.lang.String getDefaultValue();
    
    /**
     * Gets (as xml) the "DefaultValue" element
     */
    org.apache.xmlbeans.XmlString xgetDefaultValue();
    
    /**
     * Tests for nil "DefaultValue" element
     */
    boolean isNilDefaultValue();
    
    /**
     * True if has "DefaultValue" element
     */
    boolean isSetDefaultValue();
    
    /**
     * Sets the "DefaultValue" element
     */
    void setDefaultValue(java.lang.String defaultValue);
    
    /**
     * Sets (as xml) the "DefaultValue" element
     */
    void xsetDefaultValue(org.apache.xmlbeans.XmlString defaultValue);
    
    /**
     * Nils the "DefaultValue" element
     */
    void setNilDefaultValue();
    
    /**
     * Unsets the "DefaultValue" element
     */
    void unsetDefaultValue();
    
    /**
     * Gets the "Expression" element
     */
    java.lang.String getExpression();
    
    /**
     * Gets (as xml) the "Expression" element
     */
    org.apache.xmlbeans.XmlString xgetExpression();
    
    /**
     * Tests for nil "Expression" element
     */
    boolean isNilExpression();
    
    /**
     * True if has "Expression" element
     */
    boolean isSetExpression();
    
    /**
     * Sets the "Expression" element
     */
    void setExpression(java.lang.String expression);
    
    /**
     * Sets (as xml) the "Expression" element
     */
    void xsetExpression(org.apache.xmlbeans.XmlString expression);
    
    /**
     * Nils the "Expression" element
     */
    void setNilExpression();
    
    /**
     * Unsets the "Expression" element
     */
    void unsetExpression();
    
    /**
     * Gets array of all "AllowableValues" elements
     */
    java.lang.String[] getAllowableValuesArray();
    
    /**
     * Gets ith "AllowableValues" element
     */
    java.lang.String getAllowableValuesArray(int i);
    
    /**
     * Gets (as xml) array of all "AllowableValues" elements
     */
    org.apache.xmlbeans.XmlString[] xgetAllowableValuesArray();
    
    /**
     * Gets (as xml) ith "AllowableValues" element
     */
    org.apache.xmlbeans.XmlString xgetAllowableValuesArray(int i);
    
    /**
     * Returns number of "AllowableValues" element
     */
    int sizeOfAllowableValuesArray();
    
    /**
     * Sets array of all "AllowableValues" element
     */
    void setAllowableValuesArray(java.lang.String[] allowableValuesArray);
    
    /**
     * Sets ith "AllowableValues" element
     */
    void setAllowableValuesArray(int i, java.lang.String allowableValues);
    
    /**
     * Sets (as xml) array of all "AllowableValues" element
     */
    void xsetAllowableValuesArray(org.apache.xmlbeans.XmlString[] allowableValuesArray);
    
    /**
     * Sets (as xml) ith "AllowableValues" element
     */
    void xsetAllowableValuesArray(int i, org.apache.xmlbeans.XmlString allowableValues);
    
    /**
     * Inserts the value as the ith "AllowableValues" element
     */
    void insertAllowableValues(int i, java.lang.String allowableValues);
    
    /**
     * Appends the value as the last "AllowableValues" element
     */
    void addAllowableValues(java.lang.String allowableValues);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "AllowableValues" element
     */
    org.apache.xmlbeans.XmlString insertNewAllowableValues(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "AllowableValues" element
     */
    org.apache.xmlbeans.XmlString addNewAllowableValues();
    
    /**
     * Removes the ith "AllowableValues" element
     */
    void removeAllowableValues(int i);
    
    /**
     * Gets the "Group" element
     */
    java.lang.String getGroup();
    
    /**
     * Gets (as xml) the "Group" element
     */
    org.apache.xmlbeans.XmlString xgetGroup();
    
    /**
     * Tests for nil "Group" element
     */
    boolean isNilGroup();
    
    /**
     * True if has "Group" element
     */
    boolean isSetGroup();
    
    /**
     * Sets the "Group" element
     */
    void setGroup(java.lang.String group);
    
    /**
     * Sets (as xml) the "Group" element
     */
    void xsetGroup(org.apache.xmlbeans.XmlString group);
    
    /**
     * Nils the "Group" element
     */
    void setNilGroup();
    
    /**
     * Unsets the "Group" element
     */
    void unsetGroup();
    
    /**
     * Gets the "DisplayName" element
     */
    java.lang.String getDisplayName();
    
    /**
     * Gets (as xml) the "DisplayName" element
     */
    org.apache.xmlbeans.XmlString xgetDisplayName();
    
    /**
     * Tests for nil "DisplayName" element
     */
    boolean isNilDisplayName();
    
    /**
     * True if has "DisplayName" element
     */
    boolean isSetDisplayName();
    
    /**
     * Sets the "DisplayName" element
     */
    void setDisplayName(java.lang.String displayName);
    
    /**
     * Sets (as xml) the "DisplayName" element
     */
    void xsetDisplayName(org.apache.xmlbeans.XmlString displayName);
    
    /**
     * Nils the "DisplayName" element
     */
    void setNilDisplayName();
    
    /**
     * Unsets the "DisplayName" element
     */
    void unsetDisplayName();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.core.api.ClientProfileMetadataType newInstance() {
          return (com.idanalytics.core.api.ClientProfileMetadataType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.core.api.ClientProfileMetadataType newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.core.api.ClientProfileMetadataType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.core.api.ClientProfileMetadataType parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.core.api.ClientProfileMetadataType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.core.api.ClientProfileMetadataType parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.core.api.ClientProfileMetadataType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.core.api.ClientProfileMetadataType parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.ClientProfileMetadataType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.core.api.ClientProfileMetadataType parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.ClientProfileMetadataType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.core.api.ClientProfileMetadataType parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.ClientProfileMetadataType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.core.api.ClientProfileMetadataType parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.ClientProfileMetadataType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.core.api.ClientProfileMetadataType parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.ClientProfileMetadataType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.core.api.ClientProfileMetadataType parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.ClientProfileMetadataType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.core.api.ClientProfileMetadataType parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.ClientProfileMetadataType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.core.api.ClientProfileMetadataType parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.core.api.ClientProfileMetadataType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.core.api.ClientProfileMetadataType parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.core.api.ClientProfileMetadataType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.core.api.ClientProfileMetadataType parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.core.api.ClientProfileMetadataType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.core.api.ClientProfileMetadataType parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.core.api.ClientProfileMetadataType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.core.api.ClientProfileMetadataType parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.core.api.ClientProfileMetadataType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.core.api.ClientProfileMetadataType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.core.api.ClientProfileMetadataType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.core.api.ClientProfileMetadataType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.core.api.ClientProfileMetadataType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
