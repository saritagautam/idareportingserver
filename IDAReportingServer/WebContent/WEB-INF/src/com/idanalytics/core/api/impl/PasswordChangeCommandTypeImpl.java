/*
 * XML Type:  PasswordChangeCommandType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.PasswordChangeCommandType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML PasswordChangeCommandType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public class PasswordChangeCommandTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.core.api.PasswordChangeCommandType
{
    private static final long serialVersionUID = 1L;
    
    public PasswordChangeCommandTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CREDENTIALS$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "Credentials");
    private static final javax.xml.namespace.QName NEWPASSWORD$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "NewPassword");
    
    
    /**
     * Gets the "Credentials" element
     */
    public com.idanalytics.core.api.CredentialsType getCredentials()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.CredentialsType target = null;
            target = (com.idanalytics.core.api.CredentialsType)get_store().find_element_user(CREDENTIALS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "Credentials" element
     */
    public void setCredentials(com.idanalytics.core.api.CredentialsType credentials)
    {
        generatedSetterHelperImpl(credentials, CREDENTIALS$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "Credentials" element
     */
    public com.idanalytics.core.api.CredentialsType addNewCredentials()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.CredentialsType target = null;
            target = (com.idanalytics.core.api.CredentialsType)get_store().add_element_user(CREDENTIALS$0);
            return target;
        }
    }
    
    /**
     * Gets the "NewPassword" element
     */
    public java.lang.String getNewPassword()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NEWPASSWORD$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "NewPassword" element
     */
    public com.idanalytics.core.api.PasswordType xgetNewPassword()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.PasswordType target = null;
            target = (com.idanalytics.core.api.PasswordType)get_store().find_element_user(NEWPASSWORD$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "NewPassword" element
     */
    public void setNewPassword(java.lang.String newPassword)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NEWPASSWORD$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(NEWPASSWORD$2);
            }
            target.setStringValue(newPassword);
        }
    }
    
    /**
     * Sets (as xml) the "NewPassword" element
     */
    public void xsetNewPassword(com.idanalytics.core.api.PasswordType newPassword)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.PasswordType target = null;
            target = (com.idanalytics.core.api.PasswordType)get_store().find_element_user(NEWPASSWORD$2, 0);
            if (target == null)
            {
                target = (com.idanalytics.core.api.PasswordType)get_store().add_element_user(NEWPASSWORD$2);
            }
            target.set(newPassword);
        }
    }
}
