/*
 * XML Type:  IDSPGroupType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.IDSPGroupType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML IDSPGroupType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public class IDSPGroupTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.core.api.IDSPGroupType
{
    private static final long serialVersionUID = 1L;
    
    public IDSPGroupTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GROUPNAME$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "GroupName");
    private static final javax.xml.namespace.QName DESCRIPTION$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "Description");
    private static final javax.xml.namespace.QName USERS$4 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "Users");
    private static final javax.xml.namespace.QName ASSIGNEDSOLUTIONS$6 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "AssignedSolutions");
    private static final javax.xml.namespace.QName DELETEDSOLUTIONS$8 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "DeletedSolutions");
    private static final javax.xml.namespace.QName ADDEDSOLUTIONS$10 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "AddedSolutions");
    
    
    /**
     * Gets the "GroupName" element
     */
    public java.lang.String getGroupName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(GROUPNAME$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "GroupName" element
     */
    public com.idanalytics.core.api.GroupType xgetGroupName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.GroupType target = null;
            target = (com.idanalytics.core.api.GroupType)get_store().find_element_user(GROUPNAME$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "GroupName" element
     */
    public void setGroupName(java.lang.String groupName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(GROUPNAME$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(GROUPNAME$0);
            }
            target.setStringValue(groupName);
        }
    }
    
    /**
     * Sets (as xml) the "GroupName" element
     */
    public void xsetGroupName(com.idanalytics.core.api.GroupType groupName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.GroupType target = null;
            target = (com.idanalytics.core.api.GroupType)get_store().find_element_user(GROUPNAME$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.core.api.GroupType)get_store().add_element_user(GROUPNAME$0);
            }
            target.set(groupName);
        }
    }
    
    /**
     * Gets the "Description" element
     */
    public java.lang.String getDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESCRIPTION$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Description" element
     */
    public org.apache.xmlbeans.XmlString xgetDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DESCRIPTION$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Description" element
     */
    public void setDescription(java.lang.String description)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESCRIPTION$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DESCRIPTION$2);
            }
            target.setStringValue(description);
        }
    }
    
    /**
     * Sets (as xml) the "Description" element
     */
    public void xsetDescription(org.apache.xmlbeans.XmlString description)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DESCRIPTION$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(DESCRIPTION$2);
            }
            target.set(description);
        }
    }
    
    /**
     * Gets the "Users" element
     */
    public com.idanalytics.core.api.IDSPUserListType getUsers()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.IDSPUserListType target = null;
            target = (com.idanalytics.core.api.IDSPUserListType)get_store().find_element_user(USERS$4, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Tests for nil "Users" element
     */
    public boolean isNilUsers()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.IDSPUserListType target = null;
            target = (com.idanalytics.core.api.IDSPUserListType)get_store().find_element_user(USERS$4, 0);
            if (target == null) return false;
            return target.isNil();
        }
    }
    
    /**
     * True if has "Users" element
     */
    public boolean isSetUsers()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(USERS$4) != 0;
        }
    }
    
    /**
     * Sets the "Users" element
     */
    public void setUsers(com.idanalytics.core.api.IDSPUserListType users)
    {
        generatedSetterHelperImpl(users, USERS$4, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "Users" element
     */
    public com.idanalytics.core.api.IDSPUserListType addNewUsers()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.IDSPUserListType target = null;
            target = (com.idanalytics.core.api.IDSPUserListType)get_store().add_element_user(USERS$4);
            return target;
        }
    }
    
    /**
     * Nils the "Users" element
     */
    public void setNilUsers()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.IDSPUserListType target = null;
            target = (com.idanalytics.core.api.IDSPUserListType)get_store().find_element_user(USERS$4, 0);
            if (target == null)
            {
                target = (com.idanalytics.core.api.IDSPUserListType)get_store().add_element_user(USERS$4);
            }
            target.setNil();
        }
    }
    
    /**
     * Unsets the "Users" element
     */
    public void unsetUsers()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(USERS$4, 0);
        }
    }
    
    /**
     * Gets the "AssignedSolutions" element
     */
    public com.idanalytics.core.api.SolutionListType getAssignedSolutions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.SolutionListType target = null;
            target = (com.idanalytics.core.api.SolutionListType)get_store().find_element_user(ASSIGNEDSOLUTIONS$6, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Tests for nil "AssignedSolutions" element
     */
    public boolean isNilAssignedSolutions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.SolutionListType target = null;
            target = (com.idanalytics.core.api.SolutionListType)get_store().find_element_user(ASSIGNEDSOLUTIONS$6, 0);
            if (target == null) return false;
            return target.isNil();
        }
    }
    
    /**
     * True if has "AssignedSolutions" element
     */
    public boolean isSetAssignedSolutions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ASSIGNEDSOLUTIONS$6) != 0;
        }
    }
    
    /**
     * Sets the "AssignedSolutions" element
     */
    public void setAssignedSolutions(com.idanalytics.core.api.SolutionListType assignedSolutions)
    {
        generatedSetterHelperImpl(assignedSolutions, ASSIGNEDSOLUTIONS$6, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "AssignedSolutions" element
     */
    public com.idanalytics.core.api.SolutionListType addNewAssignedSolutions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.SolutionListType target = null;
            target = (com.idanalytics.core.api.SolutionListType)get_store().add_element_user(ASSIGNEDSOLUTIONS$6);
            return target;
        }
    }
    
    /**
     * Nils the "AssignedSolutions" element
     */
    public void setNilAssignedSolutions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.SolutionListType target = null;
            target = (com.idanalytics.core.api.SolutionListType)get_store().find_element_user(ASSIGNEDSOLUTIONS$6, 0);
            if (target == null)
            {
                target = (com.idanalytics.core.api.SolutionListType)get_store().add_element_user(ASSIGNEDSOLUTIONS$6);
            }
            target.setNil();
        }
    }
    
    /**
     * Unsets the "AssignedSolutions" element
     */
    public void unsetAssignedSolutions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ASSIGNEDSOLUTIONS$6, 0);
        }
    }
    
    /**
     * Gets the "DeletedSolutions" element
     */
    public com.idanalytics.core.api.SolutionListType getDeletedSolutions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.SolutionListType target = null;
            target = (com.idanalytics.core.api.SolutionListType)get_store().find_element_user(DELETEDSOLUTIONS$8, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Tests for nil "DeletedSolutions" element
     */
    public boolean isNilDeletedSolutions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.SolutionListType target = null;
            target = (com.idanalytics.core.api.SolutionListType)get_store().find_element_user(DELETEDSOLUTIONS$8, 0);
            if (target == null) return false;
            return target.isNil();
        }
    }
    
    /**
     * True if has "DeletedSolutions" element
     */
    public boolean isSetDeletedSolutions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(DELETEDSOLUTIONS$8) != 0;
        }
    }
    
    /**
     * Sets the "DeletedSolutions" element
     */
    public void setDeletedSolutions(com.idanalytics.core.api.SolutionListType deletedSolutions)
    {
        generatedSetterHelperImpl(deletedSolutions, DELETEDSOLUTIONS$8, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "DeletedSolutions" element
     */
    public com.idanalytics.core.api.SolutionListType addNewDeletedSolutions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.SolutionListType target = null;
            target = (com.idanalytics.core.api.SolutionListType)get_store().add_element_user(DELETEDSOLUTIONS$8);
            return target;
        }
    }
    
    /**
     * Nils the "DeletedSolutions" element
     */
    public void setNilDeletedSolutions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.SolutionListType target = null;
            target = (com.idanalytics.core.api.SolutionListType)get_store().find_element_user(DELETEDSOLUTIONS$8, 0);
            if (target == null)
            {
                target = (com.idanalytics.core.api.SolutionListType)get_store().add_element_user(DELETEDSOLUTIONS$8);
            }
            target.setNil();
        }
    }
    
    /**
     * Unsets the "DeletedSolutions" element
     */
    public void unsetDeletedSolutions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(DELETEDSOLUTIONS$8, 0);
        }
    }
    
    /**
     * Gets the "AddedSolutions" element
     */
    public com.idanalytics.core.api.SolutionListType getAddedSolutions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.SolutionListType target = null;
            target = (com.idanalytics.core.api.SolutionListType)get_store().find_element_user(ADDEDSOLUTIONS$10, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Tests for nil "AddedSolutions" element
     */
    public boolean isNilAddedSolutions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.SolutionListType target = null;
            target = (com.idanalytics.core.api.SolutionListType)get_store().find_element_user(ADDEDSOLUTIONS$10, 0);
            if (target == null) return false;
            return target.isNil();
        }
    }
    
    /**
     * True if has "AddedSolutions" element
     */
    public boolean isSetAddedSolutions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ADDEDSOLUTIONS$10) != 0;
        }
    }
    
    /**
     * Sets the "AddedSolutions" element
     */
    public void setAddedSolutions(com.idanalytics.core.api.SolutionListType addedSolutions)
    {
        generatedSetterHelperImpl(addedSolutions, ADDEDSOLUTIONS$10, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "AddedSolutions" element
     */
    public com.idanalytics.core.api.SolutionListType addNewAddedSolutions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.SolutionListType target = null;
            target = (com.idanalytics.core.api.SolutionListType)get_store().add_element_user(ADDEDSOLUTIONS$10);
            return target;
        }
    }
    
    /**
     * Nils the "AddedSolutions" element
     */
    public void setNilAddedSolutions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.SolutionListType target = null;
            target = (com.idanalytics.core.api.SolutionListType)get_store().find_element_user(ADDEDSOLUTIONS$10, 0);
            if (target == null)
            {
                target = (com.idanalytics.core.api.SolutionListType)get_store().add_element_user(ADDEDSOLUTIONS$10);
            }
            target.setNil();
        }
    }
    
    /**
     * Unsets the "AddedSolutions" element
     */
    public void unsetAddedSolutions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ADDEDSOLUTIONS$10, 0);
        }
    }
}
