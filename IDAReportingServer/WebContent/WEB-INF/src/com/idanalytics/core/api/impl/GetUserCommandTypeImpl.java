/*
 * XML Type:  GetUserCommandType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.GetUserCommandType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML GetUserCommandType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public class GetUserCommandTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.core.api.GetUserCommandType
{
    private static final long serialVersionUID = 1L;
    
    public GetUserCommandTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName USERNAME$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "UserName");
    private static final javax.xml.namespace.QName TOKEN$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "Token");
    
    
    /**
     * Gets the "UserName" element
     */
    public java.lang.String getUserName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(USERNAME$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "UserName" element
     */
    public com.idanalytics.core.api.UsernameType xgetUserName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.UsernameType target = null;
            target = (com.idanalytics.core.api.UsernameType)get_store().find_element_user(USERNAME$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "UserName" element
     */
    public void setUserName(java.lang.String userName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(USERNAME$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(USERNAME$0);
            }
            target.setStringValue(userName);
        }
    }
    
    /**
     * Sets (as xml) the "UserName" element
     */
    public void xsetUserName(com.idanalytics.core.api.UsernameType userName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.UsernameType target = null;
            target = (com.idanalytics.core.api.UsernameType)get_store().find_element_user(USERNAME$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.core.api.UsernameType)get_store().add_element_user(USERNAME$0);
            }
            target.set(userName);
        }
    }
    
    /**
     * Gets the "Token" element
     */
    public java.lang.String getToken()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TOKEN$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Token" element
     */
    public com.idanalytics.core.api.TokenType xgetToken()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.TokenType target = null;
            target = (com.idanalytics.core.api.TokenType)get_store().find_element_user(TOKEN$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Token" element
     */
    public void setToken(java.lang.String token)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TOKEN$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TOKEN$2);
            }
            target.setStringValue(token);
        }
    }
    
    /**
     * Sets (as xml) the "Token" element
     */
    public void xsetToken(com.idanalytics.core.api.TokenType token)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.TokenType target = null;
            target = (com.idanalytics.core.api.TokenType)get_store().find_element_user(TOKEN$2, 0);
            if (target == null)
            {
                target = (com.idanalytics.core.api.TokenType)get_store().add_element_user(TOKEN$2);
            }
            target.set(token);
        }
    }
}
