/*
 * XML Type:  MapType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.MapType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML MapType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public class MapTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.core.api.MapType
{
    private static final long serialVersionUID = 1L;
    
    public MapTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName MAPENTRY$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "MapEntry");
    
    
    /**
     * Gets array of all "MapEntry" elements
     */
    public com.idanalytics.core.api.MapEntryType[] getMapEntryArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(MAPENTRY$0, targetList);
            com.idanalytics.core.api.MapEntryType[] result = new com.idanalytics.core.api.MapEntryType[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "MapEntry" element
     */
    public com.idanalytics.core.api.MapEntryType getMapEntryArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.MapEntryType target = null;
            target = (com.idanalytics.core.api.MapEntryType)get_store().find_element_user(MAPENTRY$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "MapEntry" element
     */
    public int sizeOfMapEntryArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(MAPENTRY$0);
        }
    }
    
    /**
     * Sets array of all "MapEntry" element  WARNING: This method is not atomicaly synchronized.
     */
    public void setMapEntryArray(com.idanalytics.core.api.MapEntryType[] mapEntryArray)
    {
        check_orphaned();
        arraySetterHelper(mapEntryArray, MAPENTRY$0);
    }
    
    /**
     * Sets ith "MapEntry" element
     */
    public void setMapEntryArray(int i, com.idanalytics.core.api.MapEntryType mapEntry)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.MapEntryType target = null;
            target = (com.idanalytics.core.api.MapEntryType)get_store().find_element_user(MAPENTRY$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(mapEntry);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "MapEntry" element
     */
    public com.idanalytics.core.api.MapEntryType insertNewMapEntry(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.MapEntryType target = null;
            target = (com.idanalytics.core.api.MapEntryType)get_store().insert_element_user(MAPENTRY$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "MapEntry" element
     */
    public com.idanalytics.core.api.MapEntryType addNewMapEntry()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.MapEntryType target = null;
            target = (com.idanalytics.core.api.MapEntryType)get_store().add_element_user(MAPENTRY$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "MapEntry" element
     */
    public void removeMapEntry(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(MAPENTRY$0, i);
        }
    }
}
