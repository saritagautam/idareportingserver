/*
 * XML Type:  PathType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.PathType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML PathType(@http://idanalytics.com/core/api).
 *
 * This is an atomic type that is a restriction of com.idanalytics.core.api.PathType.
 */
public class PathTypeImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.core.api.PathType
{
    private static final long serialVersionUID = 1L;
    
    public PathTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected PathTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
