/*
 * XML Type:  PasswordType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.PasswordType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML PasswordType(@http://idanalytics.com/core/api).
 *
 * This is an atomic type that is a restriction of com.idanalytics.core.api.PasswordType.
 */
public class PasswordTypeImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.core.api.PasswordType
{
    private static final long serialVersionUID = 1L;
    
    public PasswordTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected PasswordTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
