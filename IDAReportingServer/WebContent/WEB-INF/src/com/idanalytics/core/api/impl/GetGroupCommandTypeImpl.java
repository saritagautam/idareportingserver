/*
 * XML Type:  GetGroupCommandType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.GetGroupCommandType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML GetGroupCommandType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public class GetGroupCommandTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.core.api.GetGroupCommandType
{
    private static final long serialVersionUID = 1L;
    
    public GetGroupCommandTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GROUPNAME$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "GroupName");
    private static final javax.xml.namespace.QName TOKEN$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "Token");
    
    
    /**
     * Gets the "GroupName" element
     */
    public java.lang.String getGroupName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(GROUPNAME$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "GroupName" element
     */
    public com.idanalytics.core.api.GroupType xgetGroupName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.GroupType target = null;
            target = (com.idanalytics.core.api.GroupType)get_store().find_element_user(GROUPNAME$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "GroupName" element
     */
    public void setGroupName(java.lang.String groupName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(GROUPNAME$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(GROUPNAME$0);
            }
            target.setStringValue(groupName);
        }
    }
    
    /**
     * Sets (as xml) the "GroupName" element
     */
    public void xsetGroupName(com.idanalytics.core.api.GroupType groupName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.GroupType target = null;
            target = (com.idanalytics.core.api.GroupType)get_store().find_element_user(GROUPNAME$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.core.api.GroupType)get_store().add_element_user(GROUPNAME$0);
            }
            target.set(groupName);
        }
    }
    
    /**
     * Gets the "Token" element
     */
    public java.lang.String getToken()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TOKEN$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Token" element
     */
    public com.idanalytics.core.api.TokenType xgetToken()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.TokenType target = null;
            target = (com.idanalytics.core.api.TokenType)get_store().find_element_user(TOKEN$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Token" element
     */
    public void setToken(java.lang.String token)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TOKEN$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TOKEN$2);
            }
            target.setStringValue(token);
        }
    }
    
    /**
     * Sets (as xml) the "Token" element
     */
    public void xsetToken(com.idanalytics.core.api.TokenType token)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.TokenType target = null;
            target = (com.idanalytics.core.api.TokenType)get_store().find_element_user(TOKEN$2, 0);
            if (target == null)
            {
                target = (com.idanalytics.core.api.TokenType)get_store().add_element_user(TOKEN$2);
            }
            target.set(token);
        }
    }
}
