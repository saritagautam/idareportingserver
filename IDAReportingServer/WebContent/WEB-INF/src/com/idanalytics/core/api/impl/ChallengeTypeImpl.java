/*
 * XML Type:  ChallengeType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.ChallengeType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML ChallengeType(@http://idanalytics.com/core/api).
 *
 * This is an atomic type that is a restriction of com.idanalytics.core.api.ChallengeType.
 */
public class ChallengeTypeImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.idanalytics.core.api.ChallengeType
{
    private static final long serialVersionUID = 1L;
    
    public ChallengeTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected ChallengeTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
