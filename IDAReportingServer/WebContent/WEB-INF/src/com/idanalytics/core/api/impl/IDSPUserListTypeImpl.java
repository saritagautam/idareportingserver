/*
 * XML Type:  IDSPUserListType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.IDSPUserListType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML IDSPUserListType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public class IDSPUserListTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.core.api.IDSPUserListType
{
    private static final long serialVersionUID = 1L;
    
    public IDSPUserListTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName USER$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "User");
    
    
    /**
     * Gets array of all "User" elements
     */
    public com.idanalytics.core.api.IDSPUserType[] getUserArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(USER$0, targetList);
            com.idanalytics.core.api.IDSPUserType[] result = new com.idanalytics.core.api.IDSPUserType[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "User" element
     */
    public com.idanalytics.core.api.IDSPUserType getUserArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.IDSPUserType target = null;
            target = (com.idanalytics.core.api.IDSPUserType)get_store().find_element_user(USER$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "User" element
     */
    public int sizeOfUserArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(USER$0);
        }
    }
    
    /**
     * Sets array of all "User" element  WARNING: This method is not atomicaly synchronized.
     */
    public void setUserArray(com.idanalytics.core.api.IDSPUserType[] userArray)
    {
        check_orphaned();
        arraySetterHelper(userArray, USER$0);
    }
    
    /**
     * Sets ith "User" element
     */
    public void setUserArray(int i, com.idanalytics.core.api.IDSPUserType user)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.IDSPUserType target = null;
            target = (com.idanalytics.core.api.IDSPUserType)get_store().find_element_user(USER$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(user);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "User" element
     */
    public com.idanalytics.core.api.IDSPUserType insertNewUser(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.IDSPUserType target = null;
            target = (com.idanalytics.core.api.IDSPUserType)get_store().insert_element_user(USER$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "User" element
     */
    public com.idanalytics.core.api.IDSPUserType addNewUser()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.IDSPUserType target = null;
            target = (com.idanalytics.core.api.IDSPUserType)get_store().add_element_user(USER$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "User" element
     */
    public void removeUser(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(USER$0, i);
        }
    }
}
