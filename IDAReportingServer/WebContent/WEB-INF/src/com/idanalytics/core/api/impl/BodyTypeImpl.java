/*
 * XML Type:  BodyType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.BodyType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML BodyType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public class BodyTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.core.api.BodyType
{
    private static final long serialVersionUID = 1L;
    
    public BodyTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ITEM$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "Item");
    
    
    /**
     * Gets array of all "Item" elements
     */
    public com.idanalytics.core.api.ItemType[] getItemArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(ITEM$0, targetList);
            com.idanalytics.core.api.ItemType[] result = new com.idanalytics.core.api.ItemType[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "Item" element
     */
    public com.idanalytics.core.api.ItemType getItemArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.ItemType target = null;
            target = (com.idanalytics.core.api.ItemType)get_store().find_element_user(ITEM$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "Item" element
     */
    public int sizeOfItemArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ITEM$0);
        }
    }
    
    /**
     * Sets array of all "Item" element  WARNING: This method is not atomicaly synchronized.
     */
    public void setItemArray(com.idanalytics.core.api.ItemType[] itemArray)
    {
        check_orphaned();
        arraySetterHelper(itemArray, ITEM$0);
    }
    
    /**
     * Sets ith "Item" element
     */
    public void setItemArray(int i, com.idanalytics.core.api.ItemType item)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.ItemType target = null;
            target = (com.idanalytics.core.api.ItemType)get_store().find_element_user(ITEM$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(item);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "Item" element
     */
    public com.idanalytics.core.api.ItemType insertNewItem(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.ItemType target = null;
            target = (com.idanalytics.core.api.ItemType)get_store().insert_element_user(ITEM$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "Item" element
     */
    public com.idanalytics.core.api.ItemType addNewItem()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.ItemType target = null;
            target = (com.idanalytics.core.api.ItemType)get_store().add_element_user(ITEM$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "Item" element
     */
    public void removeItem(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ITEM$0, i);
        }
    }
}
