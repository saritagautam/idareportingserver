/*
 * XML Type:  AdminResponseType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.AdminResponseType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML AdminResponseType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public class AdminResponseTypeImpl extends com.idanalytics.core.api.impl.IDSPResponseTypeImpl implements com.idanalytics.core.api.AdminResponseType
{
    private static final long serialVersionUID = 1L;
    
    public AdminResponseTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    
}
