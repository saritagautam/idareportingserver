/*
 * XML Type:  GetClientProfileMetadataResponseType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.GetClientProfileMetadataResponseType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML GetClientProfileMetadataResponseType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public class GetClientProfileMetadataResponseTypeImpl extends com.idanalytics.core.api.impl.IDSPResponseTypeImpl implements com.idanalytics.core.api.GetClientProfileMetadataResponseType
{
    private static final long serialVersionUID = 1L;
    
    public GetClientProfileMetadataResponseTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName METADATA$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "Metadata");
    
    
    /**
     * Gets array of all "Metadata" elements
     */
    public com.idanalytics.core.api.ClientProfileMetadataType[] getMetadataArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(METADATA$0, targetList);
            com.idanalytics.core.api.ClientProfileMetadataType[] result = new com.idanalytics.core.api.ClientProfileMetadataType[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "Metadata" element
     */
    public com.idanalytics.core.api.ClientProfileMetadataType getMetadataArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.ClientProfileMetadataType target = null;
            target = (com.idanalytics.core.api.ClientProfileMetadataType)get_store().find_element_user(METADATA$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "Metadata" element
     */
    public int sizeOfMetadataArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(METADATA$0);
        }
    }
    
    /**
     * Sets array of all "Metadata" element  WARNING: This method is not atomicaly synchronized.
     */
    public void setMetadataArray(com.idanalytics.core.api.ClientProfileMetadataType[] metadataArray)
    {
        check_orphaned();
        arraySetterHelper(metadataArray, METADATA$0);
    }
    
    /**
     * Sets ith "Metadata" element
     */
    public void setMetadataArray(int i, com.idanalytics.core.api.ClientProfileMetadataType metadata)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.ClientProfileMetadataType target = null;
            target = (com.idanalytics.core.api.ClientProfileMetadataType)get_store().find_element_user(METADATA$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(metadata);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "Metadata" element
     */
    public com.idanalytics.core.api.ClientProfileMetadataType insertNewMetadata(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.ClientProfileMetadataType target = null;
            target = (com.idanalytics.core.api.ClientProfileMetadataType)get_store().insert_element_user(METADATA$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "Metadata" element
     */
    public com.idanalytics.core.api.ClientProfileMetadataType addNewMetadata()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.ClientProfileMetadataType target = null;
            target = (com.idanalytics.core.api.ClientProfileMetadataType)get_store().add_element_user(METADATA$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "Metadata" element
     */
    public void removeMetadata(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(METADATA$0, i);
        }
    }
}
