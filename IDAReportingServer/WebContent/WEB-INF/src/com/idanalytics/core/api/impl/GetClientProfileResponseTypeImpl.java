/*
 * XML Type:  GetClientProfileResponseType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.GetClientProfileResponseType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML GetClientProfileResponseType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public class GetClientProfileResponseTypeImpl extends com.idanalytics.core.api.impl.IDSPResponseTypeImpl implements com.idanalytics.core.api.GetClientProfileResponseType
{
    private static final long serialVersionUID = 1L;
    
    public GetClientProfileResponseTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CLIENTPROFILE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "ClientProfile");
    
    
    /**
     * Gets the "ClientProfile" element
     */
    public com.idanalytics.core.api.ClientProfileType getClientProfile()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.ClientProfileType target = null;
            target = (com.idanalytics.core.api.ClientProfileType)get_store().find_element_user(CLIENTPROFILE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Tests for nil "ClientProfile" element
     */
    public boolean isNilClientProfile()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.ClientProfileType target = null;
            target = (com.idanalytics.core.api.ClientProfileType)get_store().find_element_user(CLIENTPROFILE$0, 0);
            if (target == null) return false;
            return target.isNil();
        }
    }
    
    /**
     * True if has "ClientProfile" element
     */
    public boolean isSetClientProfile()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CLIENTPROFILE$0) != 0;
        }
    }
    
    /**
     * Sets the "ClientProfile" element
     */
    public void setClientProfile(com.idanalytics.core.api.ClientProfileType clientProfile)
    {
        generatedSetterHelperImpl(clientProfile, CLIENTPROFILE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "ClientProfile" element
     */
    public com.idanalytics.core.api.ClientProfileType addNewClientProfile()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.ClientProfileType target = null;
            target = (com.idanalytics.core.api.ClientProfileType)get_store().add_element_user(CLIENTPROFILE$0);
            return target;
        }
    }
    
    /**
     * Nils the "ClientProfile" element
     */
    public void setNilClientProfile()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.ClientProfileType target = null;
            target = (com.idanalytics.core.api.ClientProfileType)get_store().find_element_user(CLIENTPROFILE$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.core.api.ClientProfileType)get_store().add_element_user(CLIENTPROFILE$0);
            }
            target.setNil();
        }
    }
    
    /**
     * Unsets the "ClientProfile" element
     */
    public void unsetClientProfile()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CLIENTPROFILE$0, 0);
        }
    }
}
