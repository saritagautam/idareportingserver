/*
 * An XML document type.
 * Localname: Response
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.ResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * A document containing one Response(@http://idanalytics.com/core/api) element.
 *
 * This is a complex type.
 */
public class ResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.core.api.ResponseDocument
{
    private static final long serialVersionUID = 1L;
    
    public ResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName RESPONSE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "Response");
    
    
    /**
     * Gets the "Response" element
     */
    public com.idanalytics.core.api.ResponseType getResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.ResponseType target = null;
            target = (com.idanalytics.core.api.ResponseType)get_store().find_element_user(RESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "Response" element
     */
    public void setResponse(com.idanalytics.core.api.ResponseType response)
    {
        generatedSetterHelperImpl(response, RESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "Response" element
     */
    public com.idanalytics.core.api.ResponseType addNewResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.ResponseType target = null;
            target = (com.idanalytics.core.api.ResponseType)get_store().add_element_user(RESPONSE$0);
            return target;
        }
    }
}
