/*
 * XML Type:  SessionCommandType
 * Namespace: http://idanalytics.com/core/api
 * Java type: com.idanalytics.core.api.SessionCommandType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.core.api.impl;
/**
 * An XML SessionCommandType(@http://idanalytics.com/core/api).
 *
 * This is a complex type.
 */
public class SessionCommandTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.core.api.SessionCommandType
{
    private static final long serialVersionUID = 1L;
    
    public SessionCommandTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CLIENT$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "Client");
    private static final javax.xml.namespace.QName AFFILIATE$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "Affiliate");
    private static final javax.xml.namespace.QName SOLUTION$4 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "Solution");
    private static final javax.xml.namespace.QName TOKEN$6 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "Token");
    private static final javax.xml.namespace.QName PRODUCTSELECTION$8 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "ProductSelection");
    private static final javax.xml.namespace.QName REQUESTID$10 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "RequestID");
    private static final javax.xml.namespace.QName BODY$12 = 
        new javax.xml.namespace.QName("http://idanalytics.com/core/api", "Body");
    
    
    /**
     * Gets the "Client" element
     */
    public java.lang.String getClient()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CLIENT$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Client" element
     */
    public com.idanalytics.core.api.PathType xgetClient()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.PathType target = null;
            target = (com.idanalytics.core.api.PathType)get_store().find_element_user(CLIENT$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Client" element
     */
    public void setClient(java.lang.String client)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CLIENT$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CLIENT$0);
            }
            target.setStringValue(client);
        }
    }
    
    /**
     * Sets (as xml) the "Client" element
     */
    public void xsetClient(com.idanalytics.core.api.PathType client)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.PathType target = null;
            target = (com.idanalytics.core.api.PathType)get_store().find_element_user(CLIENT$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.core.api.PathType)get_store().add_element_user(CLIENT$0);
            }
            target.set(client);
        }
    }
    
    /**
     * Gets the "Affiliate" element
     */
    public java.lang.String getAffiliate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(AFFILIATE$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Affiliate" element
     */
    public com.idanalytics.core.api.ExtendedPathType xgetAffiliate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.ExtendedPathType target = null;
            target = (com.idanalytics.core.api.ExtendedPathType)get_store().find_element_user(AFFILIATE$2, 0);
            return target;
        }
    }
    
    /**
     * True if has "Affiliate" element
     */
    public boolean isSetAffiliate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(AFFILIATE$2) != 0;
        }
    }
    
    /**
     * Sets the "Affiliate" element
     */
    public void setAffiliate(java.lang.String affiliate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(AFFILIATE$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(AFFILIATE$2);
            }
            target.setStringValue(affiliate);
        }
    }
    
    /**
     * Sets (as xml) the "Affiliate" element
     */
    public void xsetAffiliate(com.idanalytics.core.api.ExtendedPathType affiliate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.ExtendedPathType target = null;
            target = (com.idanalytics.core.api.ExtendedPathType)get_store().find_element_user(AFFILIATE$2, 0);
            if (target == null)
            {
                target = (com.idanalytics.core.api.ExtendedPathType)get_store().add_element_user(AFFILIATE$2);
            }
            target.set(affiliate);
        }
    }
    
    /**
     * Unsets the "Affiliate" element
     */
    public void unsetAffiliate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(AFFILIATE$2, 0);
        }
    }
    
    /**
     * Gets the "Solution" element
     */
    public java.lang.String getSolution()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SOLUTION$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Solution" element
     */
    public com.idanalytics.core.api.PathType xgetSolution()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.PathType target = null;
            target = (com.idanalytics.core.api.PathType)get_store().find_element_user(SOLUTION$4, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Solution" element
     */
    public void setSolution(java.lang.String solution)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SOLUTION$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SOLUTION$4);
            }
            target.setStringValue(solution);
        }
    }
    
    /**
     * Sets (as xml) the "Solution" element
     */
    public void xsetSolution(com.idanalytics.core.api.PathType solution)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.PathType target = null;
            target = (com.idanalytics.core.api.PathType)get_store().find_element_user(SOLUTION$4, 0);
            if (target == null)
            {
                target = (com.idanalytics.core.api.PathType)get_store().add_element_user(SOLUTION$4);
            }
            target.set(solution);
        }
    }
    
    /**
     * Gets the "Token" element
     */
    public java.lang.String getToken()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TOKEN$6, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Token" element
     */
    public com.idanalytics.core.api.TokenType xgetToken()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.TokenType target = null;
            target = (com.idanalytics.core.api.TokenType)get_store().find_element_user(TOKEN$6, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Token" element
     */
    public void setToken(java.lang.String token)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TOKEN$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TOKEN$6);
            }
            target.setStringValue(token);
        }
    }
    
    /**
     * Sets (as xml) the "Token" element
     */
    public void xsetToken(com.idanalytics.core.api.TokenType token)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.TokenType target = null;
            target = (com.idanalytics.core.api.TokenType)get_store().find_element_user(TOKEN$6, 0);
            if (target == null)
            {
                target = (com.idanalytics.core.api.TokenType)get_store().add_element_user(TOKEN$6);
            }
            target.set(token);
        }
    }
    
    /**
     * Gets the "ProductSelection" element
     */
    public com.idanalytics.core.api.ProductSelectionType getProductSelection()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.ProductSelectionType target = null;
            target = (com.idanalytics.core.api.ProductSelectionType)get_store().find_element_user(PRODUCTSELECTION$8, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "ProductSelection" element
     */
    public boolean isSetProductSelection()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PRODUCTSELECTION$8) != 0;
        }
    }
    
    /**
     * Sets the "ProductSelection" element
     */
    public void setProductSelection(com.idanalytics.core.api.ProductSelectionType productSelection)
    {
        generatedSetterHelperImpl(productSelection, PRODUCTSELECTION$8, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "ProductSelection" element
     */
    public com.idanalytics.core.api.ProductSelectionType addNewProductSelection()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.ProductSelectionType target = null;
            target = (com.idanalytics.core.api.ProductSelectionType)get_store().add_element_user(PRODUCTSELECTION$8);
            return target;
        }
    }
    
    /**
     * Unsets the "ProductSelection" element
     */
    public void unsetProductSelection()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PRODUCTSELECTION$8, 0);
        }
    }
    
    /**
     * Gets the "RequestID" element
     */
    public java.lang.String getRequestID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(REQUESTID$10, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "RequestID" element
     */
    public com.idanalytics.core.api.RequestIDType xgetRequestID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.RequestIDType target = null;
            target = (com.idanalytics.core.api.RequestIDType)get_store().find_element_user(REQUESTID$10, 0);
            return target;
        }
    }
    
    /**
     * True if has "RequestID" element
     */
    public boolean isSetRequestID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(REQUESTID$10) != 0;
        }
    }
    
    /**
     * Sets the "RequestID" element
     */
    public void setRequestID(java.lang.String requestID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(REQUESTID$10, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(REQUESTID$10);
            }
            target.setStringValue(requestID);
        }
    }
    
    /**
     * Sets (as xml) the "RequestID" element
     */
    public void xsetRequestID(com.idanalytics.core.api.RequestIDType requestID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.RequestIDType target = null;
            target = (com.idanalytics.core.api.RequestIDType)get_store().find_element_user(REQUESTID$10, 0);
            if (target == null)
            {
                target = (com.idanalytics.core.api.RequestIDType)get_store().add_element_user(REQUESTID$10);
            }
            target.set(requestID);
        }
    }
    
    /**
     * Unsets the "RequestID" element
     */
    public void unsetRequestID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(REQUESTID$10, 0);
        }
    }
    
    /**
     * Gets the "Body" element
     */
    public com.idanalytics.core.api.BodyType getBody()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.BodyType target = null;
            target = (com.idanalytics.core.api.BodyType)get_store().find_element_user(BODY$12, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "Body" element
     */
    public void setBody(com.idanalytics.core.api.BodyType body)
    {
        generatedSetterHelperImpl(body, BODY$12, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "Body" element
     */
    public com.idanalytics.core.api.BodyType addNewBody()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.core.api.BodyType target = null;
            target = (com.idanalytics.core.api.BodyType)get_store().add_element_user(BODY$12);
            return target;
        }
    }
}
