/*
 * An XML document type.
 * Localname: OutputRecord
 * Namespace: http://idanalytics.com/products/netdown/result
 * Java type: com.idanalytics.products.netdown.result.OutputRecordDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.netdown.result.impl;
/**
 * A document containing one OutputRecord(@http://idanalytics.com/products/netdown/result) element.
 *
 * This is a complex type.
 */
public class OutputRecordDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.netdown.result.OutputRecordDocument
{
    private static final long serialVersionUID = 1L;
    
    public OutputRecordDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName OUTPUTRECORD$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/netdown/result", "OutputRecord");
    
    
    /**
     * Gets the "OutputRecord" element
     */
    public com.idanalytics.products.netdown.result.OutputRecordDocument.OutputRecord getOutputRecord()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.netdown.result.OutputRecordDocument.OutputRecord target = null;
            target = (com.idanalytics.products.netdown.result.OutputRecordDocument.OutputRecord)get_store().find_element_user(OUTPUTRECORD$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "OutputRecord" element
     */
    public void setOutputRecord(com.idanalytics.products.netdown.result.OutputRecordDocument.OutputRecord outputRecord)
    {
        generatedSetterHelperImpl(outputRecord, OUTPUTRECORD$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "OutputRecord" element
     */
    public com.idanalytics.products.netdown.result.OutputRecordDocument.OutputRecord addNewOutputRecord()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.netdown.result.OutputRecordDocument.OutputRecord target = null;
            target = (com.idanalytics.products.netdown.result.OutputRecordDocument.OutputRecord)get_store().add_element_user(OUTPUTRECORD$0);
            return target;
        }
    }
    /**
     * An XML OutputRecord(@http://idanalytics.com/products/netdown/result).
     *
     * This is a complex type.
     */
    public static class OutputRecordImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.netdown.result.OutputRecordDocument.OutputRecord
    {
        private static final long serialVersionUID = 1L;
        
        public OutputRecordImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName IDASTATUS$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/netdown/result", "IDAStatus");
        private static final javax.xml.namespace.QName IDATIMESTAMP$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/netdown/result", "IDATimeStamp");
        private static final javax.xml.namespace.QName IDASEQUENCE$4 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/netdown/result", "IDASequence");
        private static final javax.xml.namespace.QName PRESCREENIDASEQUENCEID$6 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/netdown/result", "PrescreenIDASequenceID");
        private static final javax.xml.namespace.QName PRESCREENDATE$8 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/netdown/result", "PrescreenDate");
        private static final javax.xml.namespace.QName RECORDFOUND$10 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/netdown/result", "RecordFound");
        private static final javax.xml.namespace.QName ERRORDESCRIPTION$12 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/netdown/result", "ErrorDescription");
        private static final javax.xml.namespace.QName PASSTHRU1$14 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/netdown/result", "PassThru1");
        private static final javax.xml.namespace.QName PASSTHRU2$16 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/netdown/result", "PassThru2");
        private static final javax.xml.namespace.QName PASSTHRU3$18 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/netdown/result", "PassThru3");
        private static final javax.xml.namespace.QName PASSTHRU4$20 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/netdown/result", "PassThru4");
        
        
        /**
         * Gets the "IDAStatus" element
         */
        public int getIDAStatus()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASTATUS$0, 0);
                if (target == null)
                {
                    return 0;
                }
                return target.getIntValue();
            }
        }
        
        /**
         * Gets (as xml) the "IDAStatus" element
         */
        public com.idanalytics.products.common_v1.IDAStatus xgetIDAStatus()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDAStatus target = null;
                target = (com.idanalytics.products.common_v1.IDAStatus)get_store().find_element_user(IDASTATUS$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "IDAStatus" element
         */
        public void setIDAStatus(int idaStatus)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASTATUS$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDASTATUS$0);
                }
                target.setIntValue(idaStatus);
            }
        }
        
        /**
         * Sets (as xml) the "IDAStatus" element
         */
        public void xsetIDAStatus(com.idanalytics.products.common_v1.IDAStatus idaStatus)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDAStatus target = null;
                target = (com.idanalytics.products.common_v1.IDAStatus)get_store().find_element_user(IDASTATUS$0, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.IDAStatus)get_store().add_element_user(IDASTATUS$0);
                }
                target.set(idaStatus);
            }
        }
        
        /**
         * Gets the "IDATimeStamp" element
         */
        public java.util.Calendar getIDATimeStamp()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDATIMESTAMP$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getCalendarValue();
            }
        }
        
        /**
         * Gets (as xml) the "IDATimeStamp" element
         */
        public com.idanalytics.products.common_v1.IDATimeStamp xgetIDATimeStamp()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDATimeStamp target = null;
                target = (com.idanalytics.products.common_v1.IDATimeStamp)get_store().find_element_user(IDATIMESTAMP$2, 0);
                return target;
            }
        }
        
        /**
         * Sets the "IDATimeStamp" element
         */
        public void setIDATimeStamp(java.util.Calendar idaTimeStamp)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDATIMESTAMP$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDATIMESTAMP$2);
                }
                target.setCalendarValue(idaTimeStamp);
            }
        }
        
        /**
         * Sets (as xml) the "IDATimeStamp" element
         */
        public void xsetIDATimeStamp(com.idanalytics.products.common_v1.IDATimeStamp idaTimeStamp)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDATimeStamp target = null;
                target = (com.idanalytics.products.common_v1.IDATimeStamp)get_store().find_element_user(IDATIMESTAMP$2, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.IDATimeStamp)get_store().add_element_user(IDATIMESTAMP$2);
                }
                target.set(idaTimeStamp);
            }
        }
        
        /**
         * Gets the "IDASequence" element
         */
        public java.lang.String getIDASequence()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASEQUENCE$4, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "IDASequence" element
         */
        public com.idanalytics.products.common_v1.IDASequence xgetIDASequence()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDASequence target = null;
                target = (com.idanalytics.products.common_v1.IDASequence)get_store().find_element_user(IDASEQUENCE$4, 0);
                return target;
            }
        }
        
        /**
         * Sets the "IDASequence" element
         */
        public void setIDASequence(java.lang.String idaSequence)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASEQUENCE$4, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDASEQUENCE$4);
                }
                target.setStringValue(idaSequence);
            }
        }
        
        /**
         * Sets (as xml) the "IDASequence" element
         */
        public void xsetIDASequence(com.idanalytics.products.common_v1.IDASequence idaSequence)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDASequence target = null;
                target = (com.idanalytics.products.common_v1.IDASequence)get_store().find_element_user(IDASEQUENCE$4, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.IDASequence)get_store().add_element_user(IDASEQUENCE$4);
                }
                target.set(idaSequence);
            }
        }
        
        /**
         * Gets the "PrescreenIDASequenceID" element
         */
        public java.lang.String getPrescreenIDASequenceID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PRESCREENIDASEQUENCEID$6, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PrescreenIDASequenceID" element
         */
        public com.idanalytics.products.common_v1.IDASequence xgetPrescreenIDASequenceID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDASequence target = null;
                target = (com.idanalytics.products.common_v1.IDASequence)get_store().find_element_user(PRESCREENIDASEQUENCEID$6, 0);
                return target;
            }
        }
        
        /**
         * Sets the "PrescreenIDASequenceID" element
         */
        public void setPrescreenIDASequenceID(java.lang.String prescreenIDASequenceID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PRESCREENIDASEQUENCEID$6, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PRESCREENIDASEQUENCEID$6);
                }
                target.setStringValue(prescreenIDASequenceID);
            }
        }
        
        /**
         * Sets (as xml) the "PrescreenIDASequenceID" element
         */
        public void xsetPrescreenIDASequenceID(com.idanalytics.products.common_v1.IDASequence prescreenIDASequenceID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDASequence target = null;
                target = (com.idanalytics.products.common_v1.IDASequence)get_store().find_element_user(PRESCREENIDASEQUENCEID$6, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.IDASequence)get_store().add_element_user(PRESCREENIDASEQUENCEID$6);
                }
                target.set(prescreenIDASequenceID);
            }
        }
        
        /**
         * Gets the "PrescreenDate" element
         */
        public java.lang.String getPrescreenDate()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PRESCREENDATE$8, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PrescreenDate" element
         */
        public com.idanalytics.products.common_v1.CampaignIdentifier xgetPrescreenDate()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.CampaignIdentifier target = null;
                target = (com.idanalytics.products.common_v1.CampaignIdentifier)get_store().find_element_user(PRESCREENDATE$8, 0);
                return target;
            }
        }
        
        /**
         * Sets the "PrescreenDate" element
         */
        public void setPrescreenDate(java.lang.String prescreenDate)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PRESCREENDATE$8, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PRESCREENDATE$8);
                }
                target.setStringValue(prescreenDate);
            }
        }
        
        /**
         * Sets (as xml) the "PrescreenDate" element
         */
        public void xsetPrescreenDate(com.idanalytics.products.common_v1.CampaignIdentifier prescreenDate)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.CampaignIdentifier target = null;
                target = (com.idanalytics.products.common_v1.CampaignIdentifier)get_store().find_element_user(PRESCREENDATE$8, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.CampaignIdentifier)get_store().add_element_user(PRESCREENDATE$8);
                }
                target.set(prescreenDate);
            }
        }
        
        /**
         * Gets the "RecordFound" element
         */
        public boolean getRecordFound()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RECORDFOUND$10, 0);
                if (target == null)
                {
                    return false;
                }
                return target.getBooleanValue();
            }
        }
        
        /**
         * Gets (as xml) the "RecordFound" element
         */
        public org.apache.xmlbeans.XmlBoolean xgetRecordFound()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlBoolean target = null;
                target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(RECORDFOUND$10, 0);
                return target;
            }
        }
        
        /**
         * True if has "RecordFound" element
         */
        public boolean isSetRecordFound()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(RECORDFOUND$10) != 0;
            }
        }
        
        /**
         * Sets the "RecordFound" element
         */
        public void setRecordFound(boolean recordFound)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RECORDFOUND$10, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(RECORDFOUND$10);
                }
                target.setBooleanValue(recordFound);
            }
        }
        
        /**
         * Sets (as xml) the "RecordFound" element
         */
        public void xsetRecordFound(org.apache.xmlbeans.XmlBoolean recordFound)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlBoolean target = null;
                target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(RECORDFOUND$10, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlBoolean)get_store().add_element_user(RECORDFOUND$10);
                }
                target.set(recordFound);
            }
        }
        
        /**
         * Unsets the "RecordFound" element
         */
        public void unsetRecordFound()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(RECORDFOUND$10, 0);
            }
        }
        
        /**
         * Gets the "ErrorDescription" element
         */
        public java.lang.String getErrorDescription()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ERRORDESCRIPTION$12, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "ErrorDescription" element
         */
        public com.idanalytics.products.common_v1.ErrorDescription xgetErrorDescription()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.ErrorDescription target = null;
                target = (com.idanalytics.products.common_v1.ErrorDescription)get_store().find_element_user(ERRORDESCRIPTION$12, 0);
                return target;
            }
        }
        
        /**
         * True if has "ErrorDescription" element
         */
        public boolean isSetErrorDescription()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(ERRORDESCRIPTION$12) != 0;
            }
        }
        
        /**
         * Sets the "ErrorDescription" element
         */
        public void setErrorDescription(java.lang.String errorDescription)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ERRORDESCRIPTION$12, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ERRORDESCRIPTION$12);
                }
                target.setStringValue(errorDescription);
            }
        }
        
        /**
         * Sets (as xml) the "ErrorDescription" element
         */
        public void xsetErrorDescription(com.idanalytics.products.common_v1.ErrorDescription errorDescription)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.ErrorDescription target = null;
                target = (com.idanalytics.products.common_v1.ErrorDescription)get_store().find_element_user(ERRORDESCRIPTION$12, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.ErrorDescription)get_store().add_element_user(ERRORDESCRIPTION$12);
                }
                target.set(errorDescription);
            }
        }
        
        /**
         * Unsets the "ErrorDescription" element
         */
        public void unsetErrorDescription()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(ERRORDESCRIPTION$12, 0);
            }
        }
        
        /**
         * Gets the "PassThru1" element
         */
        public java.lang.String getPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU1$14, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru1" element
         */
        public com.idanalytics.products.common_v1.PassThru xgetPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU1$14, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru1" element
         */
        public boolean isSetPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU1$14) != 0;
            }
        }
        
        /**
         * Sets the "PassThru1" element
         */
        public void setPassThru1(java.lang.String passThru1)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU1$14, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU1$14);
                }
                target.setStringValue(passThru1);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru1" element
         */
        public void xsetPassThru1(com.idanalytics.products.common_v1.PassThru passThru1)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU1$14, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.PassThru)get_store().add_element_user(PASSTHRU1$14);
                }
                target.set(passThru1);
            }
        }
        
        /**
         * Unsets the "PassThru1" element
         */
        public void unsetPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU1$14, 0);
            }
        }
        
        /**
         * Gets the "PassThru2" element
         */
        public java.lang.String getPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU2$16, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru2" element
         */
        public com.idanalytics.products.common_v1.PassThru xgetPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU2$16, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru2" element
         */
        public boolean isSetPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU2$16) != 0;
            }
        }
        
        /**
         * Sets the "PassThru2" element
         */
        public void setPassThru2(java.lang.String passThru2)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU2$16, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU2$16);
                }
                target.setStringValue(passThru2);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru2" element
         */
        public void xsetPassThru2(com.idanalytics.products.common_v1.PassThru passThru2)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU2$16, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.PassThru)get_store().add_element_user(PASSTHRU2$16);
                }
                target.set(passThru2);
            }
        }
        
        /**
         * Unsets the "PassThru2" element
         */
        public void unsetPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU2$16, 0);
            }
        }
        
        /**
         * Gets the "PassThru3" element
         */
        public java.lang.String getPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU3$18, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru3" element
         */
        public com.idanalytics.products.common_v1.PassThru xgetPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU3$18, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru3" element
         */
        public boolean isSetPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU3$18) != 0;
            }
        }
        
        /**
         * Sets the "PassThru3" element
         */
        public void setPassThru3(java.lang.String passThru3)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU3$18, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU3$18);
                }
                target.setStringValue(passThru3);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru3" element
         */
        public void xsetPassThru3(com.idanalytics.products.common_v1.PassThru passThru3)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU3$18, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.PassThru)get_store().add_element_user(PASSTHRU3$18);
                }
                target.set(passThru3);
            }
        }
        
        /**
         * Unsets the "PassThru3" element
         */
        public void unsetPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU3$18, 0);
            }
        }
        
        /**
         * Gets the "PassThru4" element
         */
        public java.lang.String getPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU4$20, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru4" element
         */
        public com.idanalytics.products.common_v1.PassThru xgetPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU4$20, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru4" element
         */
        public boolean isSetPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU4$20) != 0;
            }
        }
        
        /**
         * Sets the "PassThru4" element
         */
        public void setPassThru4(java.lang.String passThru4)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU4$20, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU4$20);
                }
                target.setStringValue(passThru4);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru4" element
         */
        public void xsetPassThru4(com.idanalytics.products.common_v1.PassThru passThru4)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU4$20, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.PassThru)get_store().add_element_user(PASSTHRU4$20);
                }
                target.set(passThru4);
            }
        }
        
        /**
         * Unsets the "PassThru4" element
         */
        public void unsetPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU4$20, 0);
            }
        }
    }
}
