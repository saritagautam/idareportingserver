/*
 * An XML document type.
 * Localname: Group
 * Namespace: http://idanalytics.com/products/netdown/request
 * Java type: com.idanalytics.products.netdown.request.GroupDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.netdown.request.impl;
/**
 * A document containing one Group(@http://idanalytics.com/products/netdown/request) element.
 *
 * This is a complex type.
 */
public class GroupDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.netdown.request.GroupDocument
{
    private static final long serialVersionUID = 1L;
    
    public GroupDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GROUP$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/netdown/request", "Group");
    
    
    /**
     * Gets the "Group" element
     */
    public com.idanalytics.products.netdown.request.GroupDocument.Group getGroup()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.netdown.request.GroupDocument.Group target = null;
            target = (com.idanalytics.products.netdown.request.GroupDocument.Group)get_store().find_element_user(GROUP$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "Group" element
     */
    public void setGroup(com.idanalytics.products.netdown.request.GroupDocument.Group group)
    {
        generatedSetterHelperImpl(group, GROUP$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "Group" element
     */
    public com.idanalytics.products.netdown.request.GroupDocument.Group addNewGroup()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.netdown.request.GroupDocument.Group target = null;
            target = (com.idanalytics.products.netdown.request.GroupDocument.Group)get_store().add_element_user(GROUP$0);
            return target;
        }
    }
    /**
     * An XML Group(@http://idanalytics.com/products/netdown/request).
     *
     * This is a complex type.
     */
    public static class GroupImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.netdown.request.GroupDocument.Group
    {
        private static final long serialVersionUID = 1L;
        
        public GroupImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName ENTRY$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/netdown/request", "Entry");
        private static final javax.xml.namespace.QName NAME$2 = 
            new javax.xml.namespace.QName("", "name");
        
        
        /**
         * Gets array of all "Entry" elements
         */
        public com.idanalytics.products.netdown.request.EntryDocument.Entry[] getEntryArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(ENTRY$0, targetList);
                com.idanalytics.products.netdown.request.EntryDocument.Entry[] result = new com.idanalytics.products.netdown.request.EntryDocument.Entry[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "Entry" element
         */
        public com.idanalytics.products.netdown.request.EntryDocument.Entry getEntryArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.netdown.request.EntryDocument.Entry target = null;
                target = (com.idanalytics.products.netdown.request.EntryDocument.Entry)get_store().find_element_user(ENTRY$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "Entry" element
         */
        public int sizeOfEntryArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(ENTRY$0);
            }
        }
        
        /**
         * Sets array of all "Entry" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setEntryArray(com.idanalytics.products.netdown.request.EntryDocument.Entry[] entryArray)
        {
            check_orphaned();
            arraySetterHelper(entryArray, ENTRY$0);
        }
        
        /**
         * Sets ith "Entry" element
         */
        public void setEntryArray(int i, com.idanalytics.products.netdown.request.EntryDocument.Entry entry)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.netdown.request.EntryDocument.Entry target = null;
                target = (com.idanalytics.products.netdown.request.EntryDocument.Entry)get_store().find_element_user(ENTRY$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(entry);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "Entry" element
         */
        public com.idanalytics.products.netdown.request.EntryDocument.Entry insertNewEntry(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.netdown.request.EntryDocument.Entry target = null;
                target = (com.idanalytics.products.netdown.request.EntryDocument.Entry)get_store().insert_element_user(ENTRY$0, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "Entry" element
         */
        public com.idanalytics.products.netdown.request.EntryDocument.Entry addNewEntry()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.netdown.request.EntryDocument.Entry target = null;
                target = (com.idanalytics.products.netdown.request.EntryDocument.Entry)get_store().add_element_user(ENTRY$0);
                return target;
            }
        }
        
        /**
         * Removes the ith "Entry" element
         */
        public void removeEntry(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(ENTRY$0, i);
            }
        }
        
        /**
         * Gets the "name" attribute
         */
        public java.lang.String getName()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(NAME$2);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "name" attribute
         */
        public org.apache.xmlbeans.XmlString xgetName()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_attribute_user(NAME$2);
                return target;
            }
        }
        
        /**
         * Sets the "name" attribute
         */
        public void setName(java.lang.String name)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(NAME$2);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(NAME$2);
                }
                target.setStringValue(name);
            }
        }
        
        /**
         * Sets (as xml) the "name" attribute
         */
        public void xsetName(org.apache.xmlbeans.XmlString name)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_attribute_user(NAME$2);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlString)get_store().add_attribute_user(NAME$2);
                }
                target.set(name);
            }
        }
    }
}
