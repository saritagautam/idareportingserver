/*
 * An XML document type.
 * Localname: OutputRecord
 * Namespace: http://idanalytics.com/products/netdown/result
 * Java type: com.idanalytics.products.netdown.result.OutputRecordDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.netdown.result;


/**
 * A document containing one OutputRecord(@http://idanalytics.com/products/netdown/result) element.
 *
 * This is a complex type.
 */
public interface OutputRecordDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(OutputRecordDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("outputrecord2a0adoctype");
    
    /**
     * Gets the "OutputRecord" element
     */
    com.idanalytics.products.netdown.result.OutputRecordDocument.OutputRecord getOutputRecord();
    
    /**
     * Sets the "OutputRecord" element
     */
    void setOutputRecord(com.idanalytics.products.netdown.result.OutputRecordDocument.OutputRecord outputRecord);
    
    /**
     * Appends and returns a new empty "OutputRecord" element
     */
    com.idanalytics.products.netdown.result.OutputRecordDocument.OutputRecord addNewOutputRecord();
    
    /**
     * An XML OutputRecord(@http://idanalytics.com/products/netdown/result).
     *
     * This is a complex type.
     */
    public interface OutputRecord extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(OutputRecord.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("outputrecord62f8elemtype");
        
        /**
         * Gets the "IDAStatus" element
         */
        int getIDAStatus();
        
        /**
         * Gets (as xml) the "IDAStatus" element
         */
        com.idanalytics.products.common_v1.IDAStatus xgetIDAStatus();
        
        /**
         * Sets the "IDAStatus" element
         */
        void setIDAStatus(int idaStatus);
        
        /**
         * Sets (as xml) the "IDAStatus" element
         */
        void xsetIDAStatus(com.idanalytics.products.common_v1.IDAStatus idaStatus);
        
        /**
         * Gets the "IDATimeStamp" element
         */
        java.util.Calendar getIDATimeStamp();
        
        /**
         * Gets (as xml) the "IDATimeStamp" element
         */
        com.idanalytics.products.common_v1.IDATimeStamp xgetIDATimeStamp();
        
        /**
         * Sets the "IDATimeStamp" element
         */
        void setIDATimeStamp(java.util.Calendar idaTimeStamp);
        
        /**
         * Sets (as xml) the "IDATimeStamp" element
         */
        void xsetIDATimeStamp(com.idanalytics.products.common_v1.IDATimeStamp idaTimeStamp);
        
        /**
         * Gets the "IDASequence" element
         */
        java.lang.String getIDASequence();
        
        /**
         * Gets (as xml) the "IDASequence" element
         */
        com.idanalytics.products.common_v1.IDASequence xgetIDASequence();
        
        /**
         * Sets the "IDASequence" element
         */
        void setIDASequence(java.lang.String idaSequence);
        
        /**
         * Sets (as xml) the "IDASequence" element
         */
        void xsetIDASequence(com.idanalytics.products.common_v1.IDASequence idaSequence);
        
        /**
         * Gets the "PrescreenIDASequenceID" element
         */
        java.lang.String getPrescreenIDASequenceID();
        
        /**
         * Gets (as xml) the "PrescreenIDASequenceID" element
         */
        com.idanalytics.products.common_v1.IDASequence xgetPrescreenIDASequenceID();
        
        /**
         * Sets the "PrescreenIDASequenceID" element
         */
        void setPrescreenIDASequenceID(java.lang.String prescreenIDASequenceID);
        
        /**
         * Sets (as xml) the "PrescreenIDASequenceID" element
         */
        void xsetPrescreenIDASequenceID(com.idanalytics.products.common_v1.IDASequence prescreenIDASequenceID);
        
        /**
         * Gets the "PrescreenDate" element
         */
        java.lang.String getPrescreenDate();
        
        /**
         * Gets (as xml) the "PrescreenDate" element
         */
        com.idanalytics.products.common_v1.CampaignIdentifier xgetPrescreenDate();
        
        /**
         * Sets the "PrescreenDate" element
         */
        void setPrescreenDate(java.lang.String prescreenDate);
        
        /**
         * Sets (as xml) the "PrescreenDate" element
         */
        void xsetPrescreenDate(com.idanalytics.products.common_v1.CampaignIdentifier prescreenDate);
        
        /**
         * Gets the "RecordFound" element
         */
        boolean getRecordFound();
        
        /**
         * Gets (as xml) the "RecordFound" element
         */
        org.apache.xmlbeans.XmlBoolean xgetRecordFound();
        
        /**
         * True if has "RecordFound" element
         */
        boolean isSetRecordFound();
        
        /**
         * Sets the "RecordFound" element
         */
        void setRecordFound(boolean recordFound);
        
        /**
         * Sets (as xml) the "RecordFound" element
         */
        void xsetRecordFound(org.apache.xmlbeans.XmlBoolean recordFound);
        
        /**
         * Unsets the "RecordFound" element
         */
        void unsetRecordFound();
        
        /**
         * Gets the "ErrorDescription" element
         */
        java.lang.String getErrorDescription();
        
        /**
         * Gets (as xml) the "ErrorDescription" element
         */
        com.idanalytics.products.common_v1.ErrorDescription xgetErrorDescription();
        
        /**
         * True if has "ErrorDescription" element
         */
        boolean isSetErrorDescription();
        
        /**
         * Sets the "ErrorDescription" element
         */
        void setErrorDescription(java.lang.String errorDescription);
        
        /**
         * Sets (as xml) the "ErrorDescription" element
         */
        void xsetErrorDescription(com.idanalytics.products.common_v1.ErrorDescription errorDescription);
        
        /**
         * Unsets the "ErrorDescription" element
         */
        void unsetErrorDescription();
        
        /**
         * Gets the "PassThru1" element
         */
        java.lang.String getPassThru1();
        
        /**
         * Gets (as xml) the "PassThru1" element
         */
        com.idanalytics.products.common_v1.PassThru xgetPassThru1();
        
        /**
         * True if has "PassThru1" element
         */
        boolean isSetPassThru1();
        
        /**
         * Sets the "PassThru1" element
         */
        void setPassThru1(java.lang.String passThru1);
        
        /**
         * Sets (as xml) the "PassThru1" element
         */
        void xsetPassThru1(com.idanalytics.products.common_v1.PassThru passThru1);
        
        /**
         * Unsets the "PassThru1" element
         */
        void unsetPassThru1();
        
        /**
         * Gets the "PassThru2" element
         */
        java.lang.String getPassThru2();
        
        /**
         * Gets (as xml) the "PassThru2" element
         */
        com.idanalytics.products.common_v1.PassThru xgetPassThru2();
        
        /**
         * True if has "PassThru2" element
         */
        boolean isSetPassThru2();
        
        /**
         * Sets the "PassThru2" element
         */
        void setPassThru2(java.lang.String passThru2);
        
        /**
         * Sets (as xml) the "PassThru2" element
         */
        void xsetPassThru2(com.idanalytics.products.common_v1.PassThru passThru2);
        
        /**
         * Unsets the "PassThru2" element
         */
        void unsetPassThru2();
        
        /**
         * Gets the "PassThru3" element
         */
        java.lang.String getPassThru3();
        
        /**
         * Gets (as xml) the "PassThru3" element
         */
        com.idanalytics.products.common_v1.PassThru xgetPassThru3();
        
        /**
         * True if has "PassThru3" element
         */
        boolean isSetPassThru3();
        
        /**
         * Sets the "PassThru3" element
         */
        void setPassThru3(java.lang.String passThru3);
        
        /**
         * Sets (as xml) the "PassThru3" element
         */
        void xsetPassThru3(com.idanalytics.products.common_v1.PassThru passThru3);
        
        /**
         * Unsets the "PassThru3" element
         */
        void unsetPassThru3();
        
        /**
         * Gets the "PassThru4" element
         */
        java.lang.String getPassThru4();
        
        /**
         * Gets (as xml) the "PassThru4" element
         */
        com.idanalytics.products.common_v1.PassThru xgetPassThru4();
        
        /**
         * True if has "PassThru4" element
         */
        boolean isSetPassThru4();
        
        /**
         * Sets the "PassThru4" element
         */
        void setPassThru4(java.lang.String passThru4);
        
        /**
         * Sets (as xml) the "PassThru4" element
         */
        void xsetPassThru4(com.idanalytics.products.common_v1.PassThru passThru4);
        
        /**
         * Unsets the "PassThru4" element
         */
        void unsetPassThru4();
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.netdown.result.OutputRecordDocument.OutputRecord newInstance() {
              return (com.idanalytics.products.netdown.result.OutputRecordDocument.OutputRecord) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.netdown.result.OutputRecordDocument.OutputRecord newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.netdown.result.OutputRecordDocument.OutputRecord) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.netdown.result.OutputRecordDocument newInstance() {
          return (com.idanalytics.products.netdown.result.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.netdown.result.OutputRecordDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.netdown.result.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.netdown.result.OutputRecordDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.netdown.result.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.netdown.result.OutputRecordDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.netdown.result.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.netdown.result.OutputRecordDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.netdown.result.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.netdown.result.OutputRecordDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.netdown.result.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.netdown.result.OutputRecordDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.netdown.result.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.netdown.result.OutputRecordDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.netdown.result.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.netdown.result.OutputRecordDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.netdown.result.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.netdown.result.OutputRecordDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.netdown.result.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.netdown.result.OutputRecordDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.netdown.result.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.netdown.result.OutputRecordDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.netdown.result.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.netdown.result.OutputRecordDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.netdown.result.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.netdown.result.OutputRecordDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.netdown.result.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.netdown.result.OutputRecordDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.netdown.result.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.netdown.result.OutputRecordDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.netdown.result.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.netdown.result.OutputRecordDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.netdown.result.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.netdown.result.OutputRecordDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.netdown.result.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
