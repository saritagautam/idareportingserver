/*
 * An XML document type.
 * Localname: NetDownRequest
 * Namespace: http://idanalytics.com/products/netdown/request
 * Java type: com.idanalytics.products.netdown.request.NetDownRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.netdown.request.impl;
/**
 * A document containing one NetDownRequest(@http://idanalytics.com/products/netdown/request) element.
 *
 * This is a complex type.
 */
public class NetDownRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.netdown.request.NetDownRequestDocument
{
    private static final long serialVersionUID = 1L;
    
    public NetDownRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName NETDOWNREQUEST$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/netdown/request", "NetDownRequest");
    
    
    /**
     * Gets the "NetDownRequest" element
     */
    public com.idanalytics.products.netdown.request.NetDownRequestDocument.NetDownRequest getNetDownRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.netdown.request.NetDownRequestDocument.NetDownRequest target = null;
            target = (com.idanalytics.products.netdown.request.NetDownRequestDocument.NetDownRequest)get_store().find_element_user(NETDOWNREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "NetDownRequest" element
     */
    public void setNetDownRequest(com.idanalytics.products.netdown.request.NetDownRequestDocument.NetDownRequest netDownRequest)
    {
        generatedSetterHelperImpl(netDownRequest, NETDOWNREQUEST$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "NetDownRequest" element
     */
    public com.idanalytics.products.netdown.request.NetDownRequestDocument.NetDownRequest addNewNetDownRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.netdown.request.NetDownRequestDocument.NetDownRequest target = null;
            target = (com.idanalytics.products.netdown.request.NetDownRequestDocument.NetDownRequest)get_store().add_element_user(NETDOWNREQUEST$0);
            return target;
        }
    }
    /**
     * An XML NetDownRequest(@http://idanalytics.com/products/netdown/request).
     *
     * This is a complex type.
     */
    public static class NetDownRequestImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.netdown.request.NetDownRequestDocument.NetDownRequest
    {
        private static final long serialVersionUID = 1L;
        
        public NetDownRequestImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName CPC$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/netdown/request", "CPC");
        private static final javax.xml.namespace.QName PRESCREENIDASEQUENCEID$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/netdown/request", "PrescreenIDASequenceID");
        private static final javax.xml.namespace.QName PRESCREENDATE$4 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/netdown/request", "PrescreenDate");
        private static final javax.xml.namespace.QName PASSTHRU1$6 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/netdown/request", "PassThru1");
        private static final javax.xml.namespace.QName PASSTHRU2$8 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/netdown/request", "PassThru2");
        private static final javax.xml.namespace.QName PASSTHRU3$10 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/netdown/request", "PassThru3");
        private static final javax.xml.namespace.QName PASSTHRU4$12 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/netdown/request", "PassThru4");
        private static final javax.xml.namespace.QName IDAINTERNAL$14 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/netdown/request", "IDAInternal");
        
        
        /**
         * Gets the "CPC" element
         */
        public java.lang.String getCPC()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CPC$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "CPC" element
         */
        public com.idanalytics.products.common_v1.CPCType xgetCPC()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.CPCType target = null;
                target = (com.idanalytics.products.common_v1.CPCType)get_store().find_element_user(CPC$0, 0);
                return target;
            }
        }
        
        /**
         * True if has "CPC" element
         */
        public boolean isSetCPC()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(CPC$0) != 0;
            }
        }
        
        /**
         * Sets the "CPC" element
         */
        public void setCPC(java.lang.String cpc)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CPC$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CPC$0);
                }
                target.setStringValue(cpc);
            }
        }
        
        /**
         * Sets (as xml) the "CPC" element
         */
        public void xsetCPC(com.idanalytics.products.common_v1.CPCType cpc)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.CPCType target = null;
                target = (com.idanalytics.products.common_v1.CPCType)get_store().find_element_user(CPC$0, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.CPCType)get_store().add_element_user(CPC$0);
                }
                target.set(cpc);
            }
        }
        
        /**
         * Unsets the "CPC" element
         */
        public void unsetCPC()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(CPC$0, 0);
            }
        }
        
        /**
         * Gets the "PrescreenIDASequenceID" element
         */
        public java.lang.String getPrescreenIDASequenceID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PRESCREENIDASEQUENCEID$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PrescreenIDASequenceID" element
         */
        public com.idanalytics.products.common_v1.IDASequence xgetPrescreenIDASequenceID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDASequence target = null;
                target = (com.idanalytics.products.common_v1.IDASequence)get_store().find_element_user(PRESCREENIDASEQUENCEID$2, 0);
                return target;
            }
        }
        
        /**
         * Sets the "PrescreenIDASequenceID" element
         */
        public void setPrescreenIDASequenceID(java.lang.String prescreenIDASequenceID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PRESCREENIDASEQUENCEID$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PRESCREENIDASEQUENCEID$2);
                }
                target.setStringValue(prescreenIDASequenceID);
            }
        }
        
        /**
         * Sets (as xml) the "PrescreenIDASequenceID" element
         */
        public void xsetPrescreenIDASequenceID(com.idanalytics.products.common_v1.IDASequence prescreenIDASequenceID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDASequence target = null;
                target = (com.idanalytics.products.common_v1.IDASequence)get_store().find_element_user(PRESCREENIDASEQUENCEID$2, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.IDASequence)get_store().add_element_user(PRESCREENIDASEQUENCEID$2);
                }
                target.set(prescreenIDASequenceID);
            }
        }
        
        /**
         * Gets the "PrescreenDate" element
         */
        public java.lang.String getPrescreenDate()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PRESCREENDATE$4, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PrescreenDate" element
         */
        public com.idanalytics.products.common_v1.CampaignIdentifier xgetPrescreenDate()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.CampaignIdentifier target = null;
                target = (com.idanalytics.products.common_v1.CampaignIdentifier)get_store().find_element_user(PRESCREENDATE$4, 0);
                return target;
            }
        }
        
        /**
         * Sets the "PrescreenDate" element
         */
        public void setPrescreenDate(java.lang.String prescreenDate)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PRESCREENDATE$4, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PRESCREENDATE$4);
                }
                target.setStringValue(prescreenDate);
            }
        }
        
        /**
         * Sets (as xml) the "PrescreenDate" element
         */
        public void xsetPrescreenDate(com.idanalytics.products.common_v1.CampaignIdentifier prescreenDate)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.CampaignIdentifier target = null;
                target = (com.idanalytics.products.common_v1.CampaignIdentifier)get_store().find_element_user(PRESCREENDATE$4, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.CampaignIdentifier)get_store().add_element_user(PRESCREENDATE$4);
                }
                target.set(prescreenDate);
            }
        }
        
        /**
         * Gets the "PassThru1" element
         */
        public java.lang.String getPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU1$6, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru1" element
         */
        public com.idanalytics.products.common_v1.PassThru xgetPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU1$6, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru1" element
         */
        public boolean isSetPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU1$6) != 0;
            }
        }
        
        /**
         * Sets the "PassThru1" element
         */
        public void setPassThru1(java.lang.String passThru1)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU1$6, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU1$6);
                }
                target.setStringValue(passThru1);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru1" element
         */
        public void xsetPassThru1(com.idanalytics.products.common_v1.PassThru passThru1)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU1$6, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.PassThru)get_store().add_element_user(PASSTHRU1$6);
                }
                target.set(passThru1);
            }
        }
        
        /**
         * Unsets the "PassThru1" element
         */
        public void unsetPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU1$6, 0);
            }
        }
        
        /**
         * Gets the "PassThru2" element
         */
        public java.lang.String getPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU2$8, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru2" element
         */
        public com.idanalytics.products.common_v1.PassThru xgetPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU2$8, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru2" element
         */
        public boolean isSetPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU2$8) != 0;
            }
        }
        
        /**
         * Sets the "PassThru2" element
         */
        public void setPassThru2(java.lang.String passThru2)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU2$8, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU2$8);
                }
                target.setStringValue(passThru2);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru2" element
         */
        public void xsetPassThru2(com.idanalytics.products.common_v1.PassThru passThru2)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU2$8, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.PassThru)get_store().add_element_user(PASSTHRU2$8);
                }
                target.set(passThru2);
            }
        }
        
        /**
         * Unsets the "PassThru2" element
         */
        public void unsetPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU2$8, 0);
            }
        }
        
        /**
         * Gets the "PassThru3" element
         */
        public java.lang.String getPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU3$10, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru3" element
         */
        public com.idanalytics.products.common_v1.PassThru xgetPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU3$10, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru3" element
         */
        public boolean isSetPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU3$10) != 0;
            }
        }
        
        /**
         * Sets the "PassThru3" element
         */
        public void setPassThru3(java.lang.String passThru3)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU3$10, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU3$10);
                }
                target.setStringValue(passThru3);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru3" element
         */
        public void xsetPassThru3(com.idanalytics.products.common_v1.PassThru passThru3)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU3$10, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.PassThru)get_store().add_element_user(PASSTHRU3$10);
                }
                target.set(passThru3);
            }
        }
        
        /**
         * Unsets the "PassThru3" element
         */
        public void unsetPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU3$10, 0);
            }
        }
        
        /**
         * Gets the "PassThru4" element
         */
        public java.lang.String getPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU4$12, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru4" element
         */
        public com.idanalytics.products.common_v1.PassThru xgetPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU4$12, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru4" element
         */
        public boolean isSetPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU4$12) != 0;
            }
        }
        
        /**
         * Sets the "PassThru4" element
         */
        public void setPassThru4(java.lang.String passThru4)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU4$12, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU4$12);
                }
                target.setStringValue(passThru4);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru4" element
         */
        public void xsetPassThru4(com.idanalytics.products.common_v1.PassThru passThru4)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU4$12, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.PassThru)get_store().add_element_user(PASSTHRU4$12);
                }
                target.set(passThru4);
            }
        }
        
        /**
         * Unsets the "PassThru4" element
         */
        public void unsetPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU4$12, 0);
            }
        }
        
        /**
         * Gets the "IDAInternal" element
         */
        public com.idanalytics.products.netdown.request.IDAInternalDocument.IDAInternal getIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.netdown.request.IDAInternalDocument.IDAInternal target = null;
                target = (com.idanalytics.products.netdown.request.IDAInternalDocument.IDAInternal)get_store().find_element_user(IDAINTERNAL$14, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "IDAInternal" element
         */
        public boolean isSetIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(IDAINTERNAL$14) != 0;
            }
        }
        
        /**
         * Sets the "IDAInternal" element
         */
        public void setIDAInternal(com.idanalytics.products.netdown.request.IDAInternalDocument.IDAInternal idaInternal)
        {
            generatedSetterHelperImpl(idaInternal, IDAINTERNAL$14, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "IDAInternal" element
         */
        public com.idanalytics.products.netdown.request.IDAInternalDocument.IDAInternal addNewIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.netdown.request.IDAInternalDocument.IDAInternal target = null;
                target = (com.idanalytics.products.netdown.request.IDAInternalDocument.IDAInternal)get_store().add_element_user(IDAINTERNAL$14);
                return target;
            }
        }
        
        /**
         * Unsets the "IDAInternal" element
         */
        public void unsetIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(IDAINTERNAL$14, 0);
            }
        }
    }
}
