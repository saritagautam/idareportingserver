/*
 * An XML document type.
 * Localname: NetDownRequest
 * Namespace: http://idanalytics.com/products/netdown/request
 * Java type: com.idanalytics.products.netdown.request.NetDownRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.netdown.request;


/**
 * A document containing one NetDownRequest(@http://idanalytics.com/products/netdown/request) element.
 *
 * This is a complex type.
 */
public interface NetDownRequestDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(NetDownRequestDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("netdownrequeste100doctype");
    
    /**
     * Gets the "NetDownRequest" element
     */
    com.idanalytics.products.netdown.request.NetDownRequestDocument.NetDownRequest getNetDownRequest();
    
    /**
     * Sets the "NetDownRequest" element
     */
    void setNetDownRequest(com.idanalytics.products.netdown.request.NetDownRequestDocument.NetDownRequest netDownRequest);
    
    /**
     * Appends and returns a new empty "NetDownRequest" element
     */
    com.idanalytics.products.netdown.request.NetDownRequestDocument.NetDownRequest addNewNetDownRequest();
    
    /**
     * An XML NetDownRequest(@http://idanalytics.com/products/netdown/request).
     *
     * This is a complex type.
     */
    public interface NetDownRequest extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(NetDownRequest.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("netdownrequest4e14elemtype");
        
        /**
         * Gets the "CPC" element
         */
        java.lang.String getCPC();
        
        /**
         * Gets (as xml) the "CPC" element
         */
        com.idanalytics.products.common_v1.CPCType xgetCPC();
        
        /**
         * True if has "CPC" element
         */
        boolean isSetCPC();
        
        /**
         * Sets the "CPC" element
         */
        void setCPC(java.lang.String cpc);
        
        /**
         * Sets (as xml) the "CPC" element
         */
        void xsetCPC(com.idanalytics.products.common_v1.CPCType cpc);
        
        /**
         * Unsets the "CPC" element
         */
        void unsetCPC();
        
        /**
         * Gets the "PrescreenIDASequenceID" element
         */
        java.lang.String getPrescreenIDASequenceID();
        
        /**
         * Gets (as xml) the "PrescreenIDASequenceID" element
         */
        com.idanalytics.products.common_v1.IDASequence xgetPrescreenIDASequenceID();
        
        /**
         * Sets the "PrescreenIDASequenceID" element
         */
        void setPrescreenIDASequenceID(java.lang.String prescreenIDASequenceID);
        
        /**
         * Sets (as xml) the "PrescreenIDASequenceID" element
         */
        void xsetPrescreenIDASequenceID(com.idanalytics.products.common_v1.IDASequence prescreenIDASequenceID);
        
        /**
         * Gets the "PrescreenDate" element
         */
        java.lang.String getPrescreenDate();
        
        /**
         * Gets (as xml) the "PrescreenDate" element
         */
        com.idanalytics.products.common_v1.CampaignIdentifier xgetPrescreenDate();
        
        /**
         * Sets the "PrescreenDate" element
         */
        void setPrescreenDate(java.lang.String prescreenDate);
        
        /**
         * Sets (as xml) the "PrescreenDate" element
         */
        void xsetPrescreenDate(com.idanalytics.products.common_v1.CampaignIdentifier prescreenDate);
        
        /**
         * Gets the "PassThru1" element
         */
        java.lang.String getPassThru1();
        
        /**
         * Gets (as xml) the "PassThru1" element
         */
        com.idanalytics.products.common_v1.PassThru xgetPassThru1();
        
        /**
         * True if has "PassThru1" element
         */
        boolean isSetPassThru1();
        
        /**
         * Sets the "PassThru1" element
         */
        void setPassThru1(java.lang.String passThru1);
        
        /**
         * Sets (as xml) the "PassThru1" element
         */
        void xsetPassThru1(com.idanalytics.products.common_v1.PassThru passThru1);
        
        /**
         * Unsets the "PassThru1" element
         */
        void unsetPassThru1();
        
        /**
         * Gets the "PassThru2" element
         */
        java.lang.String getPassThru2();
        
        /**
         * Gets (as xml) the "PassThru2" element
         */
        com.idanalytics.products.common_v1.PassThru xgetPassThru2();
        
        /**
         * True if has "PassThru2" element
         */
        boolean isSetPassThru2();
        
        /**
         * Sets the "PassThru2" element
         */
        void setPassThru2(java.lang.String passThru2);
        
        /**
         * Sets (as xml) the "PassThru2" element
         */
        void xsetPassThru2(com.idanalytics.products.common_v1.PassThru passThru2);
        
        /**
         * Unsets the "PassThru2" element
         */
        void unsetPassThru2();
        
        /**
         * Gets the "PassThru3" element
         */
        java.lang.String getPassThru3();
        
        /**
         * Gets (as xml) the "PassThru3" element
         */
        com.idanalytics.products.common_v1.PassThru xgetPassThru3();
        
        /**
         * True if has "PassThru3" element
         */
        boolean isSetPassThru3();
        
        /**
         * Sets the "PassThru3" element
         */
        void setPassThru3(java.lang.String passThru3);
        
        /**
         * Sets (as xml) the "PassThru3" element
         */
        void xsetPassThru3(com.idanalytics.products.common_v1.PassThru passThru3);
        
        /**
         * Unsets the "PassThru3" element
         */
        void unsetPassThru3();
        
        /**
         * Gets the "PassThru4" element
         */
        java.lang.String getPassThru4();
        
        /**
         * Gets (as xml) the "PassThru4" element
         */
        com.idanalytics.products.common_v1.PassThru xgetPassThru4();
        
        /**
         * True if has "PassThru4" element
         */
        boolean isSetPassThru4();
        
        /**
         * Sets the "PassThru4" element
         */
        void setPassThru4(java.lang.String passThru4);
        
        /**
         * Sets (as xml) the "PassThru4" element
         */
        void xsetPassThru4(com.idanalytics.products.common_v1.PassThru passThru4);
        
        /**
         * Unsets the "PassThru4" element
         */
        void unsetPassThru4();
        
        /**
         * Gets the "IDAInternal" element
         */
        com.idanalytics.products.netdown.request.IDAInternalDocument.IDAInternal getIDAInternal();
        
        /**
         * True if has "IDAInternal" element
         */
        boolean isSetIDAInternal();
        
        /**
         * Sets the "IDAInternal" element
         */
        void setIDAInternal(com.idanalytics.products.netdown.request.IDAInternalDocument.IDAInternal idaInternal);
        
        /**
         * Appends and returns a new empty "IDAInternal" element
         */
        com.idanalytics.products.netdown.request.IDAInternalDocument.IDAInternal addNewIDAInternal();
        
        /**
         * Unsets the "IDAInternal" element
         */
        void unsetIDAInternal();
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.netdown.request.NetDownRequestDocument.NetDownRequest newInstance() {
              return (com.idanalytics.products.netdown.request.NetDownRequestDocument.NetDownRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.netdown.request.NetDownRequestDocument.NetDownRequest newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.netdown.request.NetDownRequestDocument.NetDownRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.netdown.request.NetDownRequestDocument newInstance() {
          return (com.idanalytics.products.netdown.request.NetDownRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.netdown.request.NetDownRequestDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.netdown.request.NetDownRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.netdown.request.NetDownRequestDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.netdown.request.NetDownRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.netdown.request.NetDownRequestDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.netdown.request.NetDownRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.netdown.request.NetDownRequestDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.netdown.request.NetDownRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.netdown.request.NetDownRequestDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.netdown.request.NetDownRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.netdown.request.NetDownRequestDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.netdown.request.NetDownRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.netdown.request.NetDownRequestDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.netdown.request.NetDownRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.netdown.request.NetDownRequestDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.netdown.request.NetDownRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.netdown.request.NetDownRequestDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.netdown.request.NetDownRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.netdown.request.NetDownRequestDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.netdown.request.NetDownRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.netdown.request.NetDownRequestDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.netdown.request.NetDownRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.netdown.request.NetDownRequestDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.netdown.request.NetDownRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.netdown.request.NetDownRequestDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.netdown.request.NetDownRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.netdown.request.NetDownRequestDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.netdown.request.NetDownRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.netdown.request.NetDownRequestDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.netdown.request.NetDownRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.netdown.request.NetDownRequestDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.netdown.request.NetDownRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.netdown.request.NetDownRequestDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.netdown.request.NetDownRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
