/*
 * An XML document type.
 * Localname: Indicators
 * Namespace: http://idanalytics.com/products/txp/result
 * Java type: com.idanalytics.products.txp.result.IndicatorsDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.txp.result.impl;
/**
 * A document containing one Indicators(@http://idanalytics.com/products/txp/result) element.
 *
 * This is a complex type.
 */
public class IndicatorsDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.txp.result.IndicatorsDocument
{
    private static final long serialVersionUID = 1L;
    
    public IndicatorsDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName INDICATORS$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/txp/result", "Indicators");
    
    
    /**
     * Gets the "Indicators" element
     */
    public com.idanalytics.products.txp.result.IndicatorsDocument.Indicators getIndicators()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.txp.result.IndicatorsDocument.Indicators target = null;
            target = (com.idanalytics.products.txp.result.IndicatorsDocument.Indicators)get_store().find_element_user(INDICATORS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "Indicators" element
     */
    public void setIndicators(com.idanalytics.products.txp.result.IndicatorsDocument.Indicators indicators)
    {
        generatedSetterHelperImpl(indicators, INDICATORS$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "Indicators" element
     */
    public com.idanalytics.products.txp.result.IndicatorsDocument.Indicators addNewIndicators()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.txp.result.IndicatorsDocument.Indicators target = null;
            target = (com.idanalytics.products.txp.result.IndicatorsDocument.Indicators)get_store().add_element_user(INDICATORS$0);
            return target;
        }
    }
    /**
     * An XML Indicators(@http://idanalytics.com/products/txp/result).
     *
     * This is a complex type.
     */
    public static class IndicatorsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.txp.result.IndicatorsDocument.Indicators
    {
        private static final long serialVersionUID = 1L;
        
        public IndicatorsImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName GROUP$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/txp/result", "Group");
        
        
        /**
         * Gets array of all "Group" elements
         */
        public com.idanalytics.products.txp.result.GroupDocument.Group[] getGroupArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(GROUP$0, targetList);
                com.idanalytics.products.txp.result.GroupDocument.Group[] result = new com.idanalytics.products.txp.result.GroupDocument.Group[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "Group" element
         */
        public com.idanalytics.products.txp.result.GroupDocument.Group getGroupArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.txp.result.GroupDocument.Group target = null;
                target = (com.idanalytics.products.txp.result.GroupDocument.Group)get_store().find_element_user(GROUP$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "Group" element
         */
        public int sizeOfGroupArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(GROUP$0);
            }
        }
        
        /**
         * Sets array of all "Group" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setGroupArray(com.idanalytics.products.txp.result.GroupDocument.Group[] groupArray)
        {
            check_orphaned();
            arraySetterHelper(groupArray, GROUP$0);
        }
        
        /**
         * Sets ith "Group" element
         */
        public void setGroupArray(int i, com.idanalytics.products.txp.result.GroupDocument.Group group)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.txp.result.GroupDocument.Group target = null;
                target = (com.idanalytics.products.txp.result.GroupDocument.Group)get_store().find_element_user(GROUP$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(group);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "Group" element
         */
        public com.idanalytics.products.txp.result.GroupDocument.Group insertNewGroup(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.txp.result.GroupDocument.Group target = null;
                target = (com.idanalytics.products.txp.result.GroupDocument.Group)get_store().insert_element_user(GROUP$0, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "Group" element
         */
        public com.idanalytics.products.txp.result.GroupDocument.Group addNewGroup()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.txp.result.GroupDocument.Group target = null;
                target = (com.idanalytics.products.txp.result.GroupDocument.Group)get_store().add_element_user(GROUP$0);
                return target;
            }
        }
        
        /**
         * Removes the ith "Group" element
         */
        public void removeGroup(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(GROUP$0, i);
            }
        }
    }
}
