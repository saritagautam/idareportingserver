/*
 * An XML document type.
 * Localname: TXPResponse
 * Namespace: http://idanalytics.com/products/txp/result
 * Java type: com.idanalytics.products.txp.result.TXPResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.txp.result.impl;
/**
 * A document containing one TXPResponse(@http://idanalytics.com/products/txp/result) element.
 *
 * This is a complex type.
 */
public class TXPResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.txp.result.TXPResponseDocument
{
    private static final long serialVersionUID = 1L;
    
    public TXPResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName TXPRESPONSE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/txp/result", "TXPResponse");
    
    
    /**
     * Gets the "TXPResponse" element
     */
    public com.idanalytics.products.txp.result.TXPResponseDocument.TXPResponse getTXPResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.txp.result.TXPResponseDocument.TXPResponse target = null;
            target = (com.idanalytics.products.txp.result.TXPResponseDocument.TXPResponse)get_store().find_element_user(TXPRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "TXPResponse" element
     */
    public void setTXPResponse(com.idanalytics.products.txp.result.TXPResponseDocument.TXPResponse txpResponse)
    {
        generatedSetterHelperImpl(txpResponse, TXPRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "TXPResponse" element
     */
    public com.idanalytics.products.txp.result.TXPResponseDocument.TXPResponse addNewTXPResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.txp.result.TXPResponseDocument.TXPResponse target = null;
            target = (com.idanalytics.products.txp.result.TXPResponseDocument.TXPResponse)get_store().add_element_user(TXPRESPONSE$0);
            return target;
        }
    }
    /**
     * An XML TXPResponse(@http://idanalytics.com/products/txp/result).
     *
     * This is a complex type.
     */
    public static class TXPResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.txp.result.TXPResponseDocument.TXPResponse
    {
        private static final long serialVersionUID = 1L;
        
        public TXPResponseImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName IDASTATUS$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/txp/result", "IDAStatus");
        private static final javax.xml.namespace.QName APPID$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/txp/result", "AppID");
        private static final javax.xml.namespace.QName DESIGNATION$4 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/txp/result", "Designation");
        private static final javax.xml.namespace.QName ACCTLINKKEY$6 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/txp/result", "AcctLinkKey");
        private static final javax.xml.namespace.QName IDASEQUENCE$8 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/txp/result", "IDASequence");
        private static final javax.xml.namespace.QName IDATIMESTAMP$10 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/txp/result", "IDATimeStamp");
        private static final javax.xml.namespace.QName SCORE$12 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/txp/result", "Score");
        private static final javax.xml.namespace.QName RESULTCODE1$14 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/txp/result", "ResultCode1");
        private static final javax.xml.namespace.QName RESULTCODE2$16 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/txp/result", "ResultCode2");
        private static final javax.xml.namespace.QName RESULTCODE3$18 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/txp/result", "ResultCode3");
        private static final javax.xml.namespace.QName PASSTHRU1$20 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/txp/result", "PassThru1");
        private static final javax.xml.namespace.QName PASSTHRU2$22 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/txp/result", "PassThru2");
        private static final javax.xml.namespace.QName PASSTHRU3$24 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/txp/result", "PassThru3");
        private static final javax.xml.namespace.QName PASSTHRU4$26 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/txp/result", "PassThru4");
        private static final javax.xml.namespace.QName INDICATORS$28 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/txp/result", "Indicators");
        private static final javax.xml.namespace.QName IDAINTERNAL$30 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/txp/result", "IDAInternal");
        
        
        /**
         * Gets the "IDAStatus" element
         */
        public int getIDAStatus()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASTATUS$0, 0);
                if (target == null)
                {
                    return 0;
                }
                return target.getIntValue();
            }
        }
        
        /**
         * Gets (as xml) the "IDAStatus" element
         */
        public com.idanalytics.products.common_v1.IDAStatus xgetIDAStatus()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDAStatus target = null;
                target = (com.idanalytics.products.common_v1.IDAStatus)get_store().find_element_user(IDASTATUS$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "IDAStatus" element
         */
        public void setIDAStatus(int idaStatus)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASTATUS$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDASTATUS$0);
                }
                target.setIntValue(idaStatus);
            }
        }
        
        /**
         * Sets (as xml) the "IDAStatus" element
         */
        public void xsetIDAStatus(com.idanalytics.products.common_v1.IDAStatus idaStatus)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDAStatus target = null;
                target = (com.idanalytics.products.common_v1.IDAStatus)get_store().find_element_user(IDASTATUS$0, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.IDAStatus)get_store().add_element_user(IDASTATUS$0);
                }
                target.set(idaStatus);
            }
        }
        
        /**
         * Gets the "AppID" element
         */
        public java.lang.String getAppID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(APPID$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "AppID" element
         */
        public com.idanalytics.products.common_v1.AppID xgetAppID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.AppID target = null;
                target = (com.idanalytics.products.common_v1.AppID)get_store().find_element_user(APPID$2, 0);
                return target;
            }
        }
        
        /**
         * Sets the "AppID" element
         */
        public void setAppID(java.lang.String appID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(APPID$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(APPID$2);
                }
                target.setStringValue(appID);
            }
        }
        
        /**
         * Sets (as xml) the "AppID" element
         */
        public void xsetAppID(com.idanalytics.products.common_v1.AppID appID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.AppID target = null;
                target = (com.idanalytics.products.common_v1.AppID)get_store().find_element_user(APPID$2, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.AppID)get_store().add_element_user(APPID$2);
                }
                target.set(appID);
            }
        }
        
        /**
         * Gets the "Designation" element
         */
        public com.idanalytics.products.common_v1.Designation.Enum getDesignation()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESIGNATION$4, 0);
                if (target == null)
                {
                    return null;
                }
                return (com.idanalytics.products.common_v1.Designation.Enum)target.getEnumValue();
            }
        }
        
        /**
         * Gets (as xml) the "Designation" element
         */
        public com.idanalytics.products.common_v1.Designation xgetDesignation()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.Designation target = null;
                target = (com.idanalytics.products.common_v1.Designation)get_store().find_element_user(DESIGNATION$4, 0);
                return target;
            }
        }
        
        /**
         * True if has "Designation" element
         */
        public boolean isSetDesignation()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(DESIGNATION$4) != 0;
            }
        }
        
        /**
         * Sets the "Designation" element
         */
        public void setDesignation(com.idanalytics.products.common_v1.Designation.Enum designation)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESIGNATION$4, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DESIGNATION$4);
                }
                target.setEnumValue(designation);
            }
        }
        
        /**
         * Sets (as xml) the "Designation" element
         */
        public void xsetDesignation(com.idanalytics.products.common_v1.Designation designation)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.Designation target = null;
                target = (com.idanalytics.products.common_v1.Designation)get_store().find_element_user(DESIGNATION$4, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.Designation)get_store().add_element_user(DESIGNATION$4);
                }
                target.set(designation);
            }
        }
        
        /**
         * Unsets the "Designation" element
         */
        public void unsetDesignation()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(DESIGNATION$4, 0);
            }
        }
        
        /**
         * Gets the "AcctLinkKey" element
         */
        public java.lang.String getAcctLinkKey()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ACCTLINKKEY$6, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "AcctLinkKey" element
         */
        public com.idanalytics.products.txp.result.AcctLinkKeyDocument.AcctLinkKey xgetAcctLinkKey()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.txp.result.AcctLinkKeyDocument.AcctLinkKey target = null;
                target = (com.idanalytics.products.txp.result.AcctLinkKeyDocument.AcctLinkKey)get_store().find_element_user(ACCTLINKKEY$6, 0);
                return target;
            }
        }
        
        /**
         * True if has "AcctLinkKey" element
         */
        public boolean isSetAcctLinkKey()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(ACCTLINKKEY$6) != 0;
            }
        }
        
        /**
         * Sets the "AcctLinkKey" element
         */
        public void setAcctLinkKey(java.lang.String acctLinkKey)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ACCTLINKKEY$6, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ACCTLINKKEY$6);
                }
                target.setStringValue(acctLinkKey);
            }
        }
        
        /**
         * Sets (as xml) the "AcctLinkKey" element
         */
        public void xsetAcctLinkKey(com.idanalytics.products.txp.result.AcctLinkKeyDocument.AcctLinkKey acctLinkKey)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.txp.result.AcctLinkKeyDocument.AcctLinkKey target = null;
                target = (com.idanalytics.products.txp.result.AcctLinkKeyDocument.AcctLinkKey)get_store().find_element_user(ACCTLINKKEY$6, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.txp.result.AcctLinkKeyDocument.AcctLinkKey)get_store().add_element_user(ACCTLINKKEY$6);
                }
                target.set(acctLinkKey);
            }
        }
        
        /**
         * Unsets the "AcctLinkKey" element
         */
        public void unsetAcctLinkKey()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(ACCTLINKKEY$6, 0);
            }
        }
        
        /**
         * Gets the "IDASequence" element
         */
        public java.lang.String getIDASequence()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASEQUENCE$8, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "IDASequence" element
         */
        public com.idanalytics.products.common_v1.IDASequence xgetIDASequence()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDASequence target = null;
                target = (com.idanalytics.products.common_v1.IDASequence)get_store().find_element_user(IDASEQUENCE$8, 0);
                return target;
            }
        }
        
        /**
         * Sets the "IDASequence" element
         */
        public void setIDASequence(java.lang.String idaSequence)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASEQUENCE$8, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDASEQUENCE$8);
                }
                target.setStringValue(idaSequence);
            }
        }
        
        /**
         * Sets (as xml) the "IDASequence" element
         */
        public void xsetIDASequence(com.idanalytics.products.common_v1.IDASequence idaSequence)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDASequence target = null;
                target = (com.idanalytics.products.common_v1.IDASequence)get_store().find_element_user(IDASEQUENCE$8, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.IDASequence)get_store().add_element_user(IDASEQUENCE$8);
                }
                target.set(idaSequence);
            }
        }
        
        /**
         * Gets the "IDATimeStamp" element
         */
        public java.util.Calendar getIDATimeStamp()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDATIMESTAMP$10, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getCalendarValue();
            }
        }
        
        /**
         * Gets (as xml) the "IDATimeStamp" element
         */
        public com.idanalytics.products.common_v1.IDATimeStamp xgetIDATimeStamp()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDATimeStamp target = null;
                target = (com.idanalytics.products.common_v1.IDATimeStamp)get_store().find_element_user(IDATIMESTAMP$10, 0);
                return target;
            }
        }
        
        /**
         * Sets the "IDATimeStamp" element
         */
        public void setIDATimeStamp(java.util.Calendar idaTimeStamp)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDATIMESTAMP$10, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDATIMESTAMP$10);
                }
                target.setCalendarValue(idaTimeStamp);
            }
        }
        
        /**
         * Sets (as xml) the "IDATimeStamp" element
         */
        public void xsetIDATimeStamp(com.idanalytics.products.common_v1.IDATimeStamp idaTimeStamp)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDATimeStamp target = null;
                target = (com.idanalytics.products.common_v1.IDATimeStamp)get_store().find_element_user(IDATIMESTAMP$10, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.IDATimeStamp)get_store().add_element_user(IDATIMESTAMP$10);
                }
                target.set(idaTimeStamp);
            }
        }
        
        /**
         * Gets the "Score" element
         */
        public java.lang.String getScore()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SCORE$12, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "Score" element
         */
        public com.idanalytics.products.txp.result.ScoreDocument.Score xgetScore()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.txp.result.ScoreDocument.Score target = null;
                target = (com.idanalytics.products.txp.result.ScoreDocument.Score)get_store().find_element_user(SCORE$12, 0);
                return target;
            }
        }
        
        /**
         * True if has "Score" element
         */
        public boolean isSetScore()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(SCORE$12) != 0;
            }
        }
        
        /**
         * Sets the "Score" element
         */
        public void setScore(java.lang.String score)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SCORE$12, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SCORE$12);
                }
                target.setStringValue(score);
            }
        }
        
        /**
         * Sets (as xml) the "Score" element
         */
        public void xsetScore(com.idanalytics.products.txp.result.ScoreDocument.Score score)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.txp.result.ScoreDocument.Score target = null;
                target = (com.idanalytics.products.txp.result.ScoreDocument.Score)get_store().find_element_user(SCORE$12, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.txp.result.ScoreDocument.Score)get_store().add_element_user(SCORE$12);
                }
                target.set(score);
            }
        }
        
        /**
         * Unsets the "Score" element
         */
        public void unsetScore()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(SCORE$12, 0);
            }
        }
        
        /**
         * Gets the "ResultCode1" element
         */
        public com.idanalytics.products.txp.result.ResultCode getResultCode1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.txp.result.ResultCode target = null;
                target = (com.idanalytics.products.txp.result.ResultCode)get_store().find_element_user(RESULTCODE1$14, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "ResultCode1" element
         */
        public boolean isSetResultCode1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(RESULTCODE1$14) != 0;
            }
        }
        
        /**
         * Sets the "ResultCode1" element
         */
        public void setResultCode1(com.idanalytics.products.txp.result.ResultCode resultCode1)
        {
            generatedSetterHelperImpl(resultCode1, RESULTCODE1$14, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "ResultCode1" element
         */
        public com.idanalytics.products.txp.result.ResultCode addNewResultCode1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.txp.result.ResultCode target = null;
                target = (com.idanalytics.products.txp.result.ResultCode)get_store().add_element_user(RESULTCODE1$14);
                return target;
            }
        }
        
        /**
         * Unsets the "ResultCode1" element
         */
        public void unsetResultCode1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(RESULTCODE1$14, 0);
            }
        }
        
        /**
         * Gets the "ResultCode2" element
         */
        public com.idanalytics.products.txp.result.ResultCode getResultCode2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.txp.result.ResultCode target = null;
                target = (com.idanalytics.products.txp.result.ResultCode)get_store().find_element_user(RESULTCODE2$16, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "ResultCode2" element
         */
        public boolean isSetResultCode2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(RESULTCODE2$16) != 0;
            }
        }
        
        /**
         * Sets the "ResultCode2" element
         */
        public void setResultCode2(com.idanalytics.products.txp.result.ResultCode resultCode2)
        {
            generatedSetterHelperImpl(resultCode2, RESULTCODE2$16, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "ResultCode2" element
         */
        public com.idanalytics.products.txp.result.ResultCode addNewResultCode2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.txp.result.ResultCode target = null;
                target = (com.idanalytics.products.txp.result.ResultCode)get_store().add_element_user(RESULTCODE2$16);
                return target;
            }
        }
        
        /**
         * Unsets the "ResultCode2" element
         */
        public void unsetResultCode2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(RESULTCODE2$16, 0);
            }
        }
        
        /**
         * Gets the "ResultCode3" element
         */
        public com.idanalytics.products.txp.result.ResultCode getResultCode3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.txp.result.ResultCode target = null;
                target = (com.idanalytics.products.txp.result.ResultCode)get_store().find_element_user(RESULTCODE3$18, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "ResultCode3" element
         */
        public boolean isSetResultCode3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(RESULTCODE3$18) != 0;
            }
        }
        
        /**
         * Sets the "ResultCode3" element
         */
        public void setResultCode3(com.idanalytics.products.txp.result.ResultCode resultCode3)
        {
            generatedSetterHelperImpl(resultCode3, RESULTCODE3$18, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "ResultCode3" element
         */
        public com.idanalytics.products.txp.result.ResultCode addNewResultCode3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.txp.result.ResultCode target = null;
                target = (com.idanalytics.products.txp.result.ResultCode)get_store().add_element_user(RESULTCODE3$18);
                return target;
            }
        }
        
        /**
         * Unsets the "ResultCode3" element
         */
        public void unsetResultCode3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(RESULTCODE3$18, 0);
            }
        }
        
        /**
         * Gets the "PassThru1" element
         */
        public java.lang.String getPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU1$20, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru1" element
         */
        public com.idanalytics.products.common_v1.PassThru xgetPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU1$20, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru1" element
         */
        public boolean isSetPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU1$20) != 0;
            }
        }
        
        /**
         * Sets the "PassThru1" element
         */
        public void setPassThru1(java.lang.String passThru1)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU1$20, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU1$20);
                }
                target.setStringValue(passThru1);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru1" element
         */
        public void xsetPassThru1(com.idanalytics.products.common_v1.PassThru passThru1)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU1$20, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.PassThru)get_store().add_element_user(PASSTHRU1$20);
                }
                target.set(passThru1);
            }
        }
        
        /**
         * Unsets the "PassThru1" element
         */
        public void unsetPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU1$20, 0);
            }
        }
        
        /**
         * Gets the "PassThru2" element
         */
        public java.lang.String getPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU2$22, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru2" element
         */
        public com.idanalytics.products.common_v1.PassThru xgetPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU2$22, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru2" element
         */
        public boolean isSetPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU2$22) != 0;
            }
        }
        
        /**
         * Sets the "PassThru2" element
         */
        public void setPassThru2(java.lang.String passThru2)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU2$22, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU2$22);
                }
                target.setStringValue(passThru2);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru2" element
         */
        public void xsetPassThru2(com.idanalytics.products.common_v1.PassThru passThru2)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU2$22, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.PassThru)get_store().add_element_user(PASSTHRU2$22);
                }
                target.set(passThru2);
            }
        }
        
        /**
         * Unsets the "PassThru2" element
         */
        public void unsetPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU2$22, 0);
            }
        }
        
        /**
         * Gets the "PassThru3" element
         */
        public java.lang.String getPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU3$24, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru3" element
         */
        public com.idanalytics.products.common_v1.PassThru xgetPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU3$24, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru3" element
         */
        public boolean isSetPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU3$24) != 0;
            }
        }
        
        /**
         * Sets the "PassThru3" element
         */
        public void setPassThru3(java.lang.String passThru3)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU3$24, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU3$24);
                }
                target.setStringValue(passThru3);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru3" element
         */
        public void xsetPassThru3(com.idanalytics.products.common_v1.PassThru passThru3)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU3$24, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.PassThru)get_store().add_element_user(PASSTHRU3$24);
                }
                target.set(passThru3);
            }
        }
        
        /**
         * Unsets the "PassThru3" element
         */
        public void unsetPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU3$24, 0);
            }
        }
        
        /**
         * Gets the "PassThru4" element
         */
        public java.lang.String getPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU4$26, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru4" element
         */
        public com.idanalytics.products.common_v1.PassThru xgetPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU4$26, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru4" element
         */
        public boolean isSetPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU4$26) != 0;
            }
        }
        
        /**
         * Sets the "PassThru4" element
         */
        public void setPassThru4(java.lang.String passThru4)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU4$26, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU4$26);
                }
                target.setStringValue(passThru4);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru4" element
         */
        public void xsetPassThru4(com.idanalytics.products.common_v1.PassThru passThru4)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU4$26, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.PassThru)get_store().add_element_user(PASSTHRU4$26);
                }
                target.set(passThru4);
            }
        }
        
        /**
         * Unsets the "PassThru4" element
         */
        public void unsetPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU4$26, 0);
            }
        }
        
        /**
         * Gets the "Indicators" element
         */
        public com.idanalytics.products.txp.result.IndicatorsDocument.Indicators getIndicators()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.txp.result.IndicatorsDocument.Indicators target = null;
                target = (com.idanalytics.products.txp.result.IndicatorsDocument.Indicators)get_store().find_element_user(INDICATORS$28, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "Indicators" element
         */
        public boolean isSetIndicators()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(INDICATORS$28) != 0;
            }
        }
        
        /**
         * Sets the "Indicators" element
         */
        public void setIndicators(com.idanalytics.products.txp.result.IndicatorsDocument.Indicators indicators)
        {
            generatedSetterHelperImpl(indicators, INDICATORS$28, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "Indicators" element
         */
        public com.idanalytics.products.txp.result.IndicatorsDocument.Indicators addNewIndicators()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.txp.result.IndicatorsDocument.Indicators target = null;
                target = (com.idanalytics.products.txp.result.IndicatorsDocument.Indicators)get_store().add_element_user(INDICATORS$28);
                return target;
            }
        }
        
        /**
         * Unsets the "Indicators" element
         */
        public void unsetIndicators()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(INDICATORS$28, 0);
            }
        }
        
        /**
         * Gets the "IDAInternal" element
         */
        public com.idanalytics.products.txp.result.IDAInternalDocument.IDAInternal getIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.txp.result.IDAInternalDocument.IDAInternal target = null;
                target = (com.idanalytics.products.txp.result.IDAInternalDocument.IDAInternal)get_store().find_element_user(IDAINTERNAL$30, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "IDAInternal" element
         */
        public boolean isSetIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(IDAINTERNAL$30) != 0;
            }
        }
        
        /**
         * Sets the "IDAInternal" element
         */
        public void setIDAInternal(com.idanalytics.products.txp.result.IDAInternalDocument.IDAInternal idaInternal)
        {
            generatedSetterHelperImpl(idaInternal, IDAINTERNAL$30, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "IDAInternal" element
         */
        public com.idanalytics.products.txp.result.IDAInternalDocument.IDAInternal addNewIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.txp.result.IDAInternalDocument.IDAInternal target = null;
                target = (com.idanalytics.products.txp.result.IDAInternalDocument.IDAInternal)get_store().add_element_user(IDAINTERNAL$30);
                return target;
            }
        }
        
        /**
         * Unsets the "IDAInternal" element
         */
        public void unsetIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(IDAINTERNAL$30, 0);
            }
        }
    }
}
