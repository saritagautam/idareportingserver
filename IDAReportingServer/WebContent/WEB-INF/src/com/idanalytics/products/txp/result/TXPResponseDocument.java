/*
 * An XML document type.
 * Localname: TXPResponse
 * Namespace: http://idanalytics.com/products/txp/result
 * Java type: com.idanalytics.products.txp.result.TXPResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.txp.result;


/**
 * A document containing one TXPResponse(@http://idanalytics.com/products/txp/result) element.
 *
 * This is a complex type.
 */
public interface TXPResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(TXPResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("txpresponseccd4doctype");
    
    /**
     * Gets the "TXPResponse" element
     */
    com.idanalytics.products.txp.result.TXPResponseDocument.TXPResponse getTXPResponse();
    
    /**
     * Sets the "TXPResponse" element
     */
    void setTXPResponse(com.idanalytics.products.txp.result.TXPResponseDocument.TXPResponse txpResponse);
    
    /**
     * Appends and returns a new empty "TXPResponse" element
     */
    com.idanalytics.products.txp.result.TXPResponseDocument.TXPResponse addNewTXPResponse();
    
    /**
     * An XML TXPResponse(@http://idanalytics.com/products/txp/result).
     *
     * This is a complex type.
     */
    public interface TXPResponse extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(TXPResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("txpresponse93dbelemtype");
        
        /**
         * Gets the "IDAStatus" element
         */
        int getIDAStatus();
        
        /**
         * Gets (as xml) the "IDAStatus" element
         */
        com.idanalytics.products.common_v1.IDAStatus xgetIDAStatus();
        
        /**
         * Sets the "IDAStatus" element
         */
        void setIDAStatus(int idaStatus);
        
        /**
         * Sets (as xml) the "IDAStatus" element
         */
        void xsetIDAStatus(com.idanalytics.products.common_v1.IDAStatus idaStatus);
        
        /**
         * Gets the "AppID" element
         */
        java.lang.String getAppID();
        
        /**
         * Gets (as xml) the "AppID" element
         */
        com.idanalytics.products.common_v1.AppID xgetAppID();
        
        /**
         * Sets the "AppID" element
         */
        void setAppID(java.lang.String appID);
        
        /**
         * Sets (as xml) the "AppID" element
         */
        void xsetAppID(com.idanalytics.products.common_v1.AppID appID);
        
        /**
         * Gets the "Designation" element
         */
        com.idanalytics.products.common_v1.Designation.Enum getDesignation();
        
        /**
         * Gets (as xml) the "Designation" element
         */
        com.idanalytics.products.common_v1.Designation xgetDesignation();
        
        /**
         * True if has "Designation" element
         */
        boolean isSetDesignation();
        
        /**
         * Sets the "Designation" element
         */
        void setDesignation(com.idanalytics.products.common_v1.Designation.Enum designation);
        
        /**
         * Sets (as xml) the "Designation" element
         */
        void xsetDesignation(com.idanalytics.products.common_v1.Designation designation);
        
        /**
         * Unsets the "Designation" element
         */
        void unsetDesignation();
        
        /**
         * Gets the "AcctLinkKey" element
         */
        java.lang.String getAcctLinkKey();
        
        /**
         * Gets (as xml) the "AcctLinkKey" element
         */
        com.idanalytics.products.txp.result.AcctLinkKeyDocument.AcctLinkKey xgetAcctLinkKey();
        
        /**
         * True if has "AcctLinkKey" element
         */
        boolean isSetAcctLinkKey();
        
        /**
         * Sets the "AcctLinkKey" element
         */
        void setAcctLinkKey(java.lang.String acctLinkKey);
        
        /**
         * Sets (as xml) the "AcctLinkKey" element
         */
        void xsetAcctLinkKey(com.idanalytics.products.txp.result.AcctLinkKeyDocument.AcctLinkKey acctLinkKey);
        
        /**
         * Unsets the "AcctLinkKey" element
         */
        void unsetAcctLinkKey();
        
        /**
         * Gets the "IDASequence" element
         */
        java.lang.String getIDASequence();
        
        /**
         * Gets (as xml) the "IDASequence" element
         */
        com.idanalytics.products.common_v1.IDASequence xgetIDASequence();
        
        /**
         * Sets the "IDASequence" element
         */
        void setIDASequence(java.lang.String idaSequence);
        
        /**
         * Sets (as xml) the "IDASequence" element
         */
        void xsetIDASequence(com.idanalytics.products.common_v1.IDASequence idaSequence);
        
        /**
         * Gets the "IDATimeStamp" element
         */
        java.util.Calendar getIDATimeStamp();
        
        /**
         * Gets (as xml) the "IDATimeStamp" element
         */
        com.idanalytics.products.common_v1.IDATimeStamp xgetIDATimeStamp();
        
        /**
         * Sets the "IDATimeStamp" element
         */
        void setIDATimeStamp(java.util.Calendar idaTimeStamp);
        
        /**
         * Sets (as xml) the "IDATimeStamp" element
         */
        void xsetIDATimeStamp(com.idanalytics.products.common_v1.IDATimeStamp idaTimeStamp);
        
        /**
         * Gets the "Score" element
         */
        java.lang.String getScore();
        
        /**
         * Gets (as xml) the "Score" element
         */
        com.idanalytics.products.txp.result.ScoreDocument.Score xgetScore();
        
        /**
         * True if has "Score" element
         */
        boolean isSetScore();
        
        /**
         * Sets the "Score" element
         */
        void setScore(java.lang.String score);
        
        /**
         * Sets (as xml) the "Score" element
         */
        void xsetScore(com.idanalytics.products.txp.result.ScoreDocument.Score score);
        
        /**
         * Unsets the "Score" element
         */
        void unsetScore();
        
        /**
         * Gets the "ResultCode1" element
         */
        com.idanalytics.products.txp.result.ResultCode getResultCode1();
        
        /**
         * True if has "ResultCode1" element
         */
        boolean isSetResultCode1();
        
        /**
         * Sets the "ResultCode1" element
         */
        void setResultCode1(com.idanalytics.products.txp.result.ResultCode resultCode1);
        
        /**
         * Appends and returns a new empty "ResultCode1" element
         */
        com.idanalytics.products.txp.result.ResultCode addNewResultCode1();
        
        /**
         * Unsets the "ResultCode1" element
         */
        void unsetResultCode1();
        
        /**
         * Gets the "ResultCode2" element
         */
        com.idanalytics.products.txp.result.ResultCode getResultCode2();
        
        /**
         * True if has "ResultCode2" element
         */
        boolean isSetResultCode2();
        
        /**
         * Sets the "ResultCode2" element
         */
        void setResultCode2(com.idanalytics.products.txp.result.ResultCode resultCode2);
        
        /**
         * Appends and returns a new empty "ResultCode2" element
         */
        com.idanalytics.products.txp.result.ResultCode addNewResultCode2();
        
        /**
         * Unsets the "ResultCode2" element
         */
        void unsetResultCode2();
        
        /**
         * Gets the "ResultCode3" element
         */
        com.idanalytics.products.txp.result.ResultCode getResultCode3();
        
        /**
         * True if has "ResultCode3" element
         */
        boolean isSetResultCode3();
        
        /**
         * Sets the "ResultCode3" element
         */
        void setResultCode3(com.idanalytics.products.txp.result.ResultCode resultCode3);
        
        /**
         * Appends and returns a new empty "ResultCode3" element
         */
        com.idanalytics.products.txp.result.ResultCode addNewResultCode3();
        
        /**
         * Unsets the "ResultCode3" element
         */
        void unsetResultCode3();
        
        /**
         * Gets the "PassThru1" element
         */
        java.lang.String getPassThru1();
        
        /**
         * Gets (as xml) the "PassThru1" element
         */
        com.idanalytics.products.common_v1.PassThru xgetPassThru1();
        
        /**
         * True if has "PassThru1" element
         */
        boolean isSetPassThru1();
        
        /**
         * Sets the "PassThru1" element
         */
        void setPassThru1(java.lang.String passThru1);
        
        /**
         * Sets (as xml) the "PassThru1" element
         */
        void xsetPassThru1(com.idanalytics.products.common_v1.PassThru passThru1);
        
        /**
         * Unsets the "PassThru1" element
         */
        void unsetPassThru1();
        
        /**
         * Gets the "PassThru2" element
         */
        java.lang.String getPassThru2();
        
        /**
         * Gets (as xml) the "PassThru2" element
         */
        com.idanalytics.products.common_v1.PassThru xgetPassThru2();
        
        /**
         * True if has "PassThru2" element
         */
        boolean isSetPassThru2();
        
        /**
         * Sets the "PassThru2" element
         */
        void setPassThru2(java.lang.String passThru2);
        
        /**
         * Sets (as xml) the "PassThru2" element
         */
        void xsetPassThru2(com.idanalytics.products.common_v1.PassThru passThru2);
        
        /**
         * Unsets the "PassThru2" element
         */
        void unsetPassThru2();
        
        /**
         * Gets the "PassThru3" element
         */
        java.lang.String getPassThru3();
        
        /**
         * Gets (as xml) the "PassThru3" element
         */
        com.idanalytics.products.common_v1.PassThru xgetPassThru3();
        
        /**
         * True if has "PassThru3" element
         */
        boolean isSetPassThru3();
        
        /**
         * Sets the "PassThru3" element
         */
        void setPassThru3(java.lang.String passThru3);
        
        /**
         * Sets (as xml) the "PassThru3" element
         */
        void xsetPassThru3(com.idanalytics.products.common_v1.PassThru passThru3);
        
        /**
         * Unsets the "PassThru3" element
         */
        void unsetPassThru3();
        
        /**
         * Gets the "PassThru4" element
         */
        java.lang.String getPassThru4();
        
        /**
         * Gets (as xml) the "PassThru4" element
         */
        com.idanalytics.products.common_v1.PassThru xgetPassThru4();
        
        /**
         * True if has "PassThru4" element
         */
        boolean isSetPassThru4();
        
        /**
         * Sets the "PassThru4" element
         */
        void setPassThru4(java.lang.String passThru4);
        
        /**
         * Sets (as xml) the "PassThru4" element
         */
        void xsetPassThru4(com.idanalytics.products.common_v1.PassThru passThru4);
        
        /**
         * Unsets the "PassThru4" element
         */
        void unsetPassThru4();
        
        /**
         * Gets the "Indicators" element
         */
        com.idanalytics.products.txp.result.IndicatorsDocument.Indicators getIndicators();
        
        /**
         * True if has "Indicators" element
         */
        boolean isSetIndicators();
        
        /**
         * Sets the "Indicators" element
         */
        void setIndicators(com.idanalytics.products.txp.result.IndicatorsDocument.Indicators indicators);
        
        /**
         * Appends and returns a new empty "Indicators" element
         */
        com.idanalytics.products.txp.result.IndicatorsDocument.Indicators addNewIndicators();
        
        /**
         * Unsets the "Indicators" element
         */
        void unsetIndicators();
        
        /**
         * Gets the "IDAInternal" element
         */
        com.idanalytics.products.txp.result.IDAInternalDocument.IDAInternal getIDAInternal();
        
        /**
         * True if has "IDAInternal" element
         */
        boolean isSetIDAInternal();
        
        /**
         * Sets the "IDAInternal" element
         */
        void setIDAInternal(com.idanalytics.products.txp.result.IDAInternalDocument.IDAInternal idaInternal);
        
        /**
         * Appends and returns a new empty "IDAInternal" element
         */
        com.idanalytics.products.txp.result.IDAInternalDocument.IDAInternal addNewIDAInternal();
        
        /**
         * Unsets the "IDAInternal" element
         */
        void unsetIDAInternal();
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.txp.result.TXPResponseDocument.TXPResponse newInstance() {
              return (com.idanalytics.products.txp.result.TXPResponseDocument.TXPResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.txp.result.TXPResponseDocument.TXPResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.txp.result.TXPResponseDocument.TXPResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.txp.result.TXPResponseDocument newInstance() {
          return (com.idanalytics.products.txp.result.TXPResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.txp.result.TXPResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.txp.result.TXPResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.txp.result.TXPResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.txp.result.TXPResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.txp.result.TXPResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.txp.result.TXPResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.txp.result.TXPResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.txp.result.TXPResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.txp.result.TXPResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.txp.result.TXPResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.txp.result.TXPResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.txp.result.TXPResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.txp.result.TXPResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.txp.result.TXPResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.txp.result.TXPResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.txp.result.TXPResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.txp.result.TXPResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.txp.result.TXPResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.txp.result.TXPResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.txp.result.TXPResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.txp.result.TXPResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.txp.result.TXPResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.txp.result.TXPResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.txp.result.TXPResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.txp.result.TXPResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.txp.result.TXPResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.txp.result.TXPResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.txp.result.TXPResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.txp.result.TXPResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.txp.result.TXPResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.txp.result.TXPResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.txp.result.TXPResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.txp.result.TXPResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.txp.result.TXPResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
