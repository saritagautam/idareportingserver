/*
 * An XML document type.
 * Localname: Score
 * Namespace: http://idanalytics.com/products/txp/result
 * Java type: com.idanalytics.products.txp.result.ScoreDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.txp.result.impl;
/**
 * A document containing one Score(@http://idanalytics.com/products/txp/result) element.
 *
 * This is a complex type.
 */
public class ScoreDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.txp.result.ScoreDocument
{
    private static final long serialVersionUID = 1L;
    
    public ScoreDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SCORE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/txp/result", "Score");
    
    
    /**
     * Gets the "Score" element
     */
    public java.lang.String getScore()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SCORE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Score" element
     */
    public com.idanalytics.products.txp.result.ScoreDocument.Score xgetScore()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.txp.result.ScoreDocument.Score target = null;
            target = (com.idanalytics.products.txp.result.ScoreDocument.Score)get_store().find_element_user(SCORE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Score" element
     */
    public void setScore(java.lang.String score)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SCORE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SCORE$0);
            }
            target.setStringValue(score);
        }
    }
    
    /**
     * Sets (as xml) the "Score" element
     */
    public void xsetScore(com.idanalytics.products.txp.result.ScoreDocument.Score score)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.txp.result.ScoreDocument.Score target = null;
            target = (com.idanalytics.products.txp.result.ScoreDocument.Score)get_store().find_element_user(SCORE$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.txp.result.ScoreDocument.Score)get_store().add_element_user(SCORE$0);
            }
            target.set(score);
        }
    }
    /**
     * An XML Score(@http://idanalytics.com/products/txp/result).
     *
     * This is a union type. Instances are of one of the following types:
     *     com.idanalytics.products.txp.result.ScoreDocument$Score$Member
     *     com.idanalytics.products.txp.result.ScoreDocument$Score$Member2
     */
    public static class ScoreImpl extends org.apache.xmlbeans.impl.values.XmlUnionImpl implements com.idanalytics.products.txp.result.ScoreDocument.Score, com.idanalytics.products.txp.result.ScoreDocument.Score.Member, com.idanalytics.products.txp.result.ScoreDocument.Score.Member2
    {
        private static final long serialVersionUID = 1L;
        
        public ScoreImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected ScoreImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
        /**
         * An anonymous inner XML type.
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.txp.result.ScoreDocument$Score$Member.
         */
        public static class MemberImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.txp.result.ScoreDocument.Score.Member
        {
            private static final long serialVersionUID = 1L;
            
            public MemberImpl(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected MemberImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
        /**
         * An anonymous inner XML type.
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.txp.result.ScoreDocument$Score$Member2.
         */
        public static class MemberImpl2 extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.txp.result.ScoreDocument.Score.Member2
        {
            private static final long serialVersionUID = 1L;
            
            public MemberImpl2(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected MemberImpl2(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
    }
}
