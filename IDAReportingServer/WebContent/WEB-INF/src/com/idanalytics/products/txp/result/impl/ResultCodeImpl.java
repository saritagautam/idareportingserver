/*
 * XML Type:  ResultCode
 * Namespace: http://idanalytics.com/products/txp/result
 * Java type: com.idanalytics.products.txp.result.ResultCode
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.txp.result.impl;
/**
 * An XML ResultCode(@http://idanalytics.com/products/txp/result).
 *
 * This is a complex type.
 */
public class ResultCodeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.txp.result.ResultCode
{
    private static final long serialVersionUID = 1L;
    
    public ResultCodeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CODE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/txp/result", "Code");
    private static final javax.xml.namespace.QName IDENTITYTYPE$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/txp/result", "IdentityType");
    private static final javax.xml.namespace.QName IDENTITYSEQUENCENUM$4 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/txp/result", "IdentitySequenceNum");
    private static final javax.xml.namespace.QName DESCRIPTION$6 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/txp/result", "Description");
    
    
    /**
     * Gets the "Code" element
     */
    public java.lang.String getCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CODE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Code" element
     */
    public com.idanalytics.products.common_v1.ReasonCode xgetCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.ReasonCode target = null;
            target = (com.idanalytics.products.common_v1.ReasonCode)get_store().find_element_user(CODE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Code" element
     */
    public void setCode(java.lang.String code)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CODE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CODE$0);
            }
            target.setStringValue(code);
        }
    }
    
    /**
     * Sets (as xml) the "Code" element
     */
    public void xsetCode(com.idanalytics.products.common_v1.ReasonCode code)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.ReasonCode target = null;
            target = (com.idanalytics.products.common_v1.ReasonCode)get_store().find_element_user(CODE$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.ReasonCode)get_store().add_element_user(CODE$0);
            }
            target.set(code);
        }
    }
    
    /**
     * Gets the "IdentityType" element
     */
    public java.lang.String getIdentityType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDENTITYTYPE$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IdentityType" element
     */
    public com.idanalytics.products.common_v1.NormalizedString xgetIdentityType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.NormalizedString target = null;
            target = (com.idanalytics.products.common_v1.NormalizedString)get_store().find_element_user(IDENTITYTYPE$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IdentityType" element
     */
    public void setIdentityType(java.lang.String identityType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDENTITYTYPE$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDENTITYTYPE$2);
            }
            target.setStringValue(identityType);
        }
    }
    
    /**
     * Sets (as xml) the "IdentityType" element
     */
    public void xsetIdentityType(com.idanalytics.products.common_v1.NormalizedString identityType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.NormalizedString target = null;
            target = (com.idanalytics.products.common_v1.NormalizedString)get_store().find_element_user(IDENTITYTYPE$2, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.NormalizedString)get_store().add_element_user(IDENTITYTYPE$2);
            }
            target.set(identityType);
        }
    }
    
    /**
     * Gets the "IdentitySequenceNum" element
     */
    public java.lang.String getIdentitySequenceNum()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDENTITYSEQUENCENUM$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IdentitySequenceNum" element
     */
    public com.idanalytics.products.common_v1.NormalizedString xgetIdentitySequenceNum()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.NormalizedString target = null;
            target = (com.idanalytics.products.common_v1.NormalizedString)get_store().find_element_user(IDENTITYSEQUENCENUM$4, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IdentitySequenceNum" element
     */
    public void setIdentitySequenceNum(java.lang.String identitySequenceNum)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDENTITYSEQUENCENUM$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDENTITYSEQUENCENUM$4);
            }
            target.setStringValue(identitySequenceNum);
        }
    }
    
    /**
     * Sets (as xml) the "IdentitySequenceNum" element
     */
    public void xsetIdentitySequenceNum(com.idanalytics.products.common_v1.NormalizedString identitySequenceNum)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.NormalizedString target = null;
            target = (com.idanalytics.products.common_v1.NormalizedString)get_store().find_element_user(IDENTITYSEQUENCENUM$4, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.NormalizedString)get_store().add_element_user(IDENTITYSEQUENCENUM$4);
            }
            target.set(identitySequenceNum);
        }
    }
    
    /**
     * Gets the "Description" element
     */
    public java.lang.String getDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESCRIPTION$6, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Description" element
     */
    public com.idanalytics.products.common_v1.NormalizedString xgetDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.NormalizedString target = null;
            target = (com.idanalytics.products.common_v1.NormalizedString)get_store().find_element_user(DESCRIPTION$6, 0);
            return target;
        }
    }
    
    /**
     * True if has "Description" element
     */
    public boolean isSetDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(DESCRIPTION$6) != 0;
        }
    }
    
    /**
     * Sets the "Description" element
     */
    public void setDescription(java.lang.String description)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESCRIPTION$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DESCRIPTION$6);
            }
            target.setStringValue(description);
        }
    }
    
    /**
     * Sets (as xml) the "Description" element
     */
    public void xsetDescription(com.idanalytics.products.common_v1.NormalizedString description)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.NormalizedString target = null;
            target = (com.idanalytics.products.common_v1.NormalizedString)get_store().find_element_user(DESCRIPTION$6, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.NormalizedString)get_store().add_element_user(DESCRIPTION$6);
            }
            target.set(description);
        }
    }
    
    /**
     * Unsets the "Description" element
     */
    public void unsetDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(DESCRIPTION$6, 0);
        }
    }
}
