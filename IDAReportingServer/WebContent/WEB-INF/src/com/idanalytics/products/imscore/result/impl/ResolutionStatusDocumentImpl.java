/*
 * An XML document type.
 * Localname: ResolutionStatus
 * Namespace: http://idanalytics.com/products/imscore/result
 * Java type: com.idanalytics.products.imscore.result.ResolutionStatusDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.imscore.result.impl;
/**
 * A document containing one ResolutionStatus(@http://idanalytics.com/products/imscore/result) element.
 *
 * This is a complex type.
 */
public class ResolutionStatusDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.imscore.result.ResolutionStatusDocument
{
    private static final long serialVersionUID = 1L;
    
    public ResolutionStatusDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName RESOLUTIONSTATUS$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/imscore/result", "ResolutionStatus");
    
    
    /**
     * Gets the "ResolutionStatus" element
     */
    public com.idanalytics.products.imscore.result.ResolutionStatusDocument.ResolutionStatus getResolutionStatus()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.imscore.result.ResolutionStatusDocument.ResolutionStatus target = null;
            target = (com.idanalytics.products.imscore.result.ResolutionStatusDocument.ResolutionStatus)get_store().find_element_user(RESOLUTIONSTATUS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "ResolutionStatus" element
     */
    public void setResolutionStatus(com.idanalytics.products.imscore.result.ResolutionStatusDocument.ResolutionStatus resolutionStatus)
    {
        generatedSetterHelperImpl(resolutionStatus, RESOLUTIONSTATUS$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "ResolutionStatus" element
     */
    public com.idanalytics.products.imscore.result.ResolutionStatusDocument.ResolutionStatus addNewResolutionStatus()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.imscore.result.ResolutionStatusDocument.ResolutionStatus target = null;
            target = (com.idanalytics.products.imscore.result.ResolutionStatusDocument.ResolutionStatus)get_store().add_element_user(RESOLUTIONSTATUS$0);
            return target;
        }
    }
    /**
     * An XML ResolutionStatus(@http://idanalytics.com/products/imscore/result).
     *
     * This is a complex type.
     */
    public static class ResolutionStatusImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.imscore.result.ResolutionStatusDocument.ResolutionStatus
    {
        private static final long serialVersionUID = 1L;
        
        public ResolutionStatusImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName CODE$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/imscore/result", "Code");
        private static final javax.xml.namespace.QName DESCRIPTION$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/imscore/result", "Description");
        
        
        /**
         * Gets the "Code" element
         */
        public java.lang.String getCode()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CODE$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "Code" element
         */
        public org.apache.xmlbeans.XmlString xgetCode()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CODE$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "Code" element
         */
        public void setCode(java.lang.String code)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CODE$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CODE$0);
                }
                target.setStringValue(code);
            }
        }
        
        /**
         * Sets (as xml) the "Code" element
         */
        public void xsetCode(org.apache.xmlbeans.XmlString code)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CODE$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(CODE$0);
                }
                target.set(code);
            }
        }
        
        /**
         * Gets the "Description" element
         */
        public java.lang.String getDescription()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESCRIPTION$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "Description" element
         */
        public org.apache.xmlbeans.XmlString xgetDescription()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DESCRIPTION$2, 0);
                return target;
            }
        }
        
        /**
         * True if has "Description" element
         */
        public boolean isSetDescription()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(DESCRIPTION$2) != 0;
            }
        }
        
        /**
         * Sets the "Description" element
         */
        public void setDescription(java.lang.String description)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESCRIPTION$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DESCRIPTION$2);
                }
                target.setStringValue(description);
            }
        }
        
        /**
         * Sets (as xml) the "Description" element
         */
        public void xsetDescription(org.apache.xmlbeans.XmlString description)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(DESCRIPTION$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(DESCRIPTION$2);
                }
                target.set(description);
            }
        }
        
        /**
         * Unsets the "Description" element
         */
        public void unsetDescription()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(DESCRIPTION$2, 0);
            }
        }
    }
}
