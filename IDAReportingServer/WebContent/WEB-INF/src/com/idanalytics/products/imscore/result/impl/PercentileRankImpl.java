/*
 * XML Type:  PercentileRank
 * Namespace: http://idanalytics.com/products/imscore/result
 * Java type: com.idanalytics.products.imscore.result.PercentileRank
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.imscore.result.impl;
/**
 * An XML PercentileRank(@http://idanalytics.com/products/imscore/result).
 *
 * This is a union type. Instances are of one of the following types:
 *     com.idanalytics.products.imscore.result.PercentileRank$Member
 *     com.idanalytics.products.imscore.result.PercentileRank$Member2
 */
public class PercentileRankImpl extends org.apache.xmlbeans.impl.values.XmlUnionImpl implements com.idanalytics.products.imscore.result.PercentileRank, com.idanalytics.products.imscore.result.PercentileRank.Member, com.idanalytics.products.imscore.result.PercentileRank.Member2
{
    private static final long serialVersionUID = 1L;
    
    public PercentileRankImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected PercentileRankImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
    /**
     * An anonymous inner XML type.
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.imscore.result.PercentileRank$Member.
     */
    public static class MemberImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.imscore.result.PercentileRank.Member
    {
        private static final long serialVersionUID = 1L;
        
        public MemberImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected MemberImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
    /**
     * An anonymous inner XML type.
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.imscore.result.PercentileRank$Member2.
     */
    public static class MemberImpl2 extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.imscore.result.PercentileRank.Member2
    {
        private static final long serialVersionUID = 1L;
        
        public MemberImpl2(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected MemberImpl2(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
