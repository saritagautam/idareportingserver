/*
 * An XML document type.
 * Localname: IndicatorGroup
 * Namespace: http://idanalytics.com/products/imscore/result
 * Java type: com.idanalytics.products.imscore.result.IndicatorGroupDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.imscore.result.impl;
/**
 * A document containing one IndicatorGroup(@http://idanalytics.com/products/imscore/result) element.
 *
 * This is a complex type.
 */
public class IndicatorGroupDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.imscore.result.IndicatorGroupDocument
{
    private static final long serialVersionUID = 1L;
    
    public IndicatorGroupDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName INDICATORGROUP$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/imscore/result", "IndicatorGroup");
    
    
    /**
     * Gets the "IndicatorGroup" element
     */
    public com.idanalytics.products.imscore.result.IndicatorGroupDocument.IndicatorGroup getIndicatorGroup()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.imscore.result.IndicatorGroupDocument.IndicatorGroup target = null;
            target = (com.idanalytics.products.imscore.result.IndicatorGroupDocument.IndicatorGroup)get_store().find_element_user(INDICATORGROUP$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "IndicatorGroup" element
     */
    public void setIndicatorGroup(com.idanalytics.products.imscore.result.IndicatorGroupDocument.IndicatorGroup indicatorGroup)
    {
        generatedSetterHelperImpl(indicatorGroup, INDICATORGROUP$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "IndicatorGroup" element
     */
    public com.idanalytics.products.imscore.result.IndicatorGroupDocument.IndicatorGroup addNewIndicatorGroup()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.imscore.result.IndicatorGroupDocument.IndicatorGroup target = null;
            target = (com.idanalytics.products.imscore.result.IndicatorGroupDocument.IndicatorGroup)get_store().add_element_user(INDICATORGROUP$0);
            return target;
        }
    }
    /**
     * An XML IndicatorGroup(@http://idanalytics.com/products/imscore/result).
     *
     * This is a complex type.
     */
    public static class IndicatorGroupImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.imscore.result.IndicatorGroupDocument.IndicatorGroup
    {
        private static final long serialVersionUID = 1L;
        
        public IndicatorGroupImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName INDICATOR$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/imscore/result", "Indicator");
        private static final javax.xml.namespace.QName ENTRY$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/imscore/result", "Entry");
        private static final javax.xml.namespace.QName NAME$4 = 
            new javax.xml.namespace.QName("", "name");
        
        
        /**
         * Gets array of all "Indicator" elements
         */
        public com.idanalytics.products.imscore.result.IndicatorDocument.Indicator[] getIndicatorArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(INDICATOR$0, targetList);
                com.idanalytics.products.imscore.result.IndicatorDocument.Indicator[] result = new com.idanalytics.products.imscore.result.IndicatorDocument.Indicator[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "Indicator" element
         */
        public com.idanalytics.products.imscore.result.IndicatorDocument.Indicator getIndicatorArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.imscore.result.IndicatorDocument.Indicator target = null;
                target = (com.idanalytics.products.imscore.result.IndicatorDocument.Indicator)get_store().find_element_user(INDICATOR$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "Indicator" element
         */
        public int sizeOfIndicatorArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(INDICATOR$0);
            }
        }
        
        /**
         * Sets array of all "Indicator" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setIndicatorArray(com.idanalytics.products.imscore.result.IndicatorDocument.Indicator[] indicatorArray)
        {
            check_orphaned();
            arraySetterHelper(indicatorArray, INDICATOR$0);
        }
        
        /**
         * Sets ith "Indicator" element
         */
        public void setIndicatorArray(int i, com.idanalytics.products.imscore.result.IndicatorDocument.Indicator indicator)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.imscore.result.IndicatorDocument.Indicator target = null;
                target = (com.idanalytics.products.imscore.result.IndicatorDocument.Indicator)get_store().find_element_user(INDICATOR$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(indicator);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "Indicator" element
         */
        public com.idanalytics.products.imscore.result.IndicatorDocument.Indicator insertNewIndicator(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.imscore.result.IndicatorDocument.Indicator target = null;
                target = (com.idanalytics.products.imscore.result.IndicatorDocument.Indicator)get_store().insert_element_user(INDICATOR$0, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "Indicator" element
         */
        public com.idanalytics.products.imscore.result.IndicatorDocument.Indicator addNewIndicator()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.imscore.result.IndicatorDocument.Indicator target = null;
                target = (com.idanalytics.products.imscore.result.IndicatorDocument.Indicator)get_store().add_element_user(INDICATOR$0);
                return target;
            }
        }
        
        /**
         * Removes the ith "Indicator" element
         */
        public void removeIndicator(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(INDICATOR$0, i);
            }
        }
        
        /**
         * Gets array of all "Entry" elements
         */
        public com.idanalytics.products.imscore.result.EntryDocument.Entry[] getEntryArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(ENTRY$2, targetList);
                com.idanalytics.products.imscore.result.EntryDocument.Entry[] result = new com.idanalytics.products.imscore.result.EntryDocument.Entry[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "Entry" element
         */
        public com.idanalytics.products.imscore.result.EntryDocument.Entry getEntryArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.imscore.result.EntryDocument.Entry target = null;
                target = (com.idanalytics.products.imscore.result.EntryDocument.Entry)get_store().find_element_user(ENTRY$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "Entry" element
         */
        public int sizeOfEntryArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(ENTRY$2);
            }
        }
        
        /**
         * Sets array of all "Entry" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setEntryArray(com.idanalytics.products.imscore.result.EntryDocument.Entry[] entryArray)
        {
            check_orphaned();
            arraySetterHelper(entryArray, ENTRY$2);
        }
        
        /**
         * Sets ith "Entry" element
         */
        public void setEntryArray(int i, com.idanalytics.products.imscore.result.EntryDocument.Entry entry)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.imscore.result.EntryDocument.Entry target = null;
                target = (com.idanalytics.products.imscore.result.EntryDocument.Entry)get_store().find_element_user(ENTRY$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(entry);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "Entry" element
         */
        public com.idanalytics.products.imscore.result.EntryDocument.Entry insertNewEntry(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.imscore.result.EntryDocument.Entry target = null;
                target = (com.idanalytics.products.imscore.result.EntryDocument.Entry)get_store().insert_element_user(ENTRY$2, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "Entry" element
         */
        public com.idanalytics.products.imscore.result.EntryDocument.Entry addNewEntry()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.imscore.result.EntryDocument.Entry target = null;
                target = (com.idanalytics.products.imscore.result.EntryDocument.Entry)get_store().add_element_user(ENTRY$2);
                return target;
            }
        }
        
        /**
         * Removes the ith "Entry" element
         */
        public void removeEntry(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(ENTRY$2, i);
            }
        }
        
        /**
         * Gets the "name" attribute
         */
        public java.lang.String getName()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(NAME$4);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "name" attribute
         */
        public org.apache.xmlbeans.XmlString xgetName()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_attribute_user(NAME$4);
                return target;
            }
        }
        
        /**
         * Sets the "name" attribute
         */
        public void setName(java.lang.String name)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(NAME$4);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(NAME$4);
                }
                target.setStringValue(name);
            }
        }
        
        /**
         * Sets (as xml) the "name" attribute
         */
        public void xsetName(org.apache.xmlbeans.XmlString name)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_attribute_user(NAME$4);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlString)get_store().add_attribute_user(NAME$4);
                }
                target.set(name);
            }
        }
    }
}
