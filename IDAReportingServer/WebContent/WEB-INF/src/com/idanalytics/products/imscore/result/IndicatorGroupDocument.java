/*
 * An XML document type.
 * Localname: IndicatorGroup
 * Namespace: http://idanalytics.com/products/imscore/result
 * Java type: com.idanalytics.products.imscore.result.IndicatorGroupDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.imscore.result;


/**
 * A document containing one IndicatorGroup(@http://idanalytics.com/products/imscore/result) element.
 *
 * This is a complex type.
 */
public interface IndicatorGroupDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(IndicatorGroupDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("indicatorgroupd07ddoctype");
    
    /**
     * Gets the "IndicatorGroup" element
     */
    com.idanalytics.products.imscore.result.IndicatorGroupDocument.IndicatorGroup getIndicatorGroup();
    
    /**
     * Sets the "IndicatorGroup" element
     */
    void setIndicatorGroup(com.idanalytics.products.imscore.result.IndicatorGroupDocument.IndicatorGroup indicatorGroup);
    
    /**
     * Appends and returns a new empty "IndicatorGroup" element
     */
    com.idanalytics.products.imscore.result.IndicatorGroupDocument.IndicatorGroup addNewIndicatorGroup();
    
    /**
     * An XML IndicatorGroup(@http://idanalytics.com/products/imscore/result).
     *
     * This is a complex type.
     */
    public interface IndicatorGroup extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(IndicatorGroup.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("indicatorgroup14e9elemtype");
        
        /**
         * Gets array of all "Indicator" elements
         */
        com.idanalytics.products.imscore.result.IndicatorDocument.Indicator[] getIndicatorArray();
        
        /**
         * Gets ith "Indicator" element
         */
        com.idanalytics.products.imscore.result.IndicatorDocument.Indicator getIndicatorArray(int i);
        
        /**
         * Returns number of "Indicator" element
         */
        int sizeOfIndicatorArray();
        
        /**
         * Sets array of all "Indicator" element
         */
        void setIndicatorArray(com.idanalytics.products.imscore.result.IndicatorDocument.Indicator[] indicatorArray);
        
        /**
         * Sets ith "Indicator" element
         */
        void setIndicatorArray(int i, com.idanalytics.products.imscore.result.IndicatorDocument.Indicator indicator);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "Indicator" element
         */
        com.idanalytics.products.imscore.result.IndicatorDocument.Indicator insertNewIndicator(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "Indicator" element
         */
        com.idanalytics.products.imscore.result.IndicatorDocument.Indicator addNewIndicator();
        
        /**
         * Removes the ith "Indicator" element
         */
        void removeIndicator(int i);
        
        /**
         * Gets array of all "Entry" elements
         */
        com.idanalytics.products.imscore.result.EntryDocument.Entry[] getEntryArray();
        
        /**
         * Gets ith "Entry" element
         */
        com.idanalytics.products.imscore.result.EntryDocument.Entry getEntryArray(int i);
        
        /**
         * Returns number of "Entry" element
         */
        int sizeOfEntryArray();
        
        /**
         * Sets array of all "Entry" element
         */
        void setEntryArray(com.idanalytics.products.imscore.result.EntryDocument.Entry[] entryArray);
        
        /**
         * Sets ith "Entry" element
         */
        void setEntryArray(int i, com.idanalytics.products.imscore.result.EntryDocument.Entry entry);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "Entry" element
         */
        com.idanalytics.products.imscore.result.EntryDocument.Entry insertNewEntry(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "Entry" element
         */
        com.idanalytics.products.imscore.result.EntryDocument.Entry addNewEntry();
        
        /**
         * Removes the ith "Entry" element
         */
        void removeEntry(int i);
        
        /**
         * Gets the "name" attribute
         */
        java.lang.String getName();
        
        /**
         * Gets (as xml) the "name" attribute
         */
        org.apache.xmlbeans.XmlString xgetName();
        
        /**
         * Sets the "name" attribute
         */
        void setName(java.lang.String name);
        
        /**
         * Sets (as xml) the "name" attribute
         */
        void xsetName(org.apache.xmlbeans.XmlString name);
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.imscore.result.IndicatorGroupDocument.IndicatorGroup newInstance() {
              return (com.idanalytics.products.imscore.result.IndicatorGroupDocument.IndicatorGroup) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.imscore.result.IndicatorGroupDocument.IndicatorGroup newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.imscore.result.IndicatorGroupDocument.IndicatorGroup) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.imscore.result.IndicatorGroupDocument newInstance() {
          return (com.idanalytics.products.imscore.result.IndicatorGroupDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.imscore.result.IndicatorGroupDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.imscore.result.IndicatorGroupDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.imscore.result.IndicatorGroupDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.imscore.result.IndicatorGroupDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.imscore.result.IndicatorGroupDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.imscore.result.IndicatorGroupDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.imscore.result.IndicatorGroupDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.imscore.result.IndicatorGroupDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.imscore.result.IndicatorGroupDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.imscore.result.IndicatorGroupDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.imscore.result.IndicatorGroupDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.imscore.result.IndicatorGroupDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.imscore.result.IndicatorGroupDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.imscore.result.IndicatorGroupDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.imscore.result.IndicatorGroupDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.imscore.result.IndicatorGroupDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.imscore.result.IndicatorGroupDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.imscore.result.IndicatorGroupDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.imscore.result.IndicatorGroupDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.imscore.result.IndicatorGroupDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.imscore.result.IndicatorGroupDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.imscore.result.IndicatorGroupDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.imscore.result.IndicatorGroupDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.imscore.result.IndicatorGroupDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.imscore.result.IndicatorGroupDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.imscore.result.IndicatorGroupDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.imscore.result.IndicatorGroupDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.imscore.result.IndicatorGroupDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.imscore.result.IndicatorGroupDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.imscore.result.IndicatorGroupDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.imscore.result.IndicatorGroupDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.imscore.result.IndicatorGroupDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.imscore.result.IndicatorGroupDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.imscore.result.IndicatorGroupDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
