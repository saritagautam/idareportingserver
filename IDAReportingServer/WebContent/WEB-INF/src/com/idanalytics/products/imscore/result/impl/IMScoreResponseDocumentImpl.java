/*
 * An XML document type.
 * Localname: IMScoreResponse
 * Namespace: http://idanalytics.com/products/imscore/result
 * Java type: com.idanalytics.products.imscore.result.IMScoreResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.imscore.result.impl;
/**
 * A document containing one IMScoreResponse(@http://idanalytics.com/products/imscore/result) element.
 *
 * This is a complex type.
 */
public class IMScoreResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.imscore.result.IMScoreResponseDocument
{
    private static final long serialVersionUID = 1L;
    
    public IMScoreResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IMSCORERESPONSE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/imscore/result", "IMScoreResponse");
    
    
    /**
     * Gets the "IMScoreResponse" element
     */
    public com.idanalytics.products.imscore.result.IMScoreResponseDocument.IMScoreResponse getIMScoreResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.imscore.result.IMScoreResponseDocument.IMScoreResponse target = null;
            target = (com.idanalytics.products.imscore.result.IMScoreResponseDocument.IMScoreResponse)get_store().find_element_user(IMSCORERESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "IMScoreResponse" element
     */
    public void setIMScoreResponse(com.idanalytics.products.imscore.result.IMScoreResponseDocument.IMScoreResponse imScoreResponse)
    {
        generatedSetterHelperImpl(imScoreResponse, IMSCORERESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "IMScoreResponse" element
     */
    public com.idanalytics.products.imscore.result.IMScoreResponseDocument.IMScoreResponse addNewIMScoreResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.imscore.result.IMScoreResponseDocument.IMScoreResponse target = null;
            target = (com.idanalytics.products.imscore.result.IMScoreResponseDocument.IMScoreResponse)get_store().add_element_user(IMSCORERESPONSE$0);
            return target;
        }
    }
    /**
     * An XML IMScoreResponse(@http://idanalytics.com/products/imscore/result).
     *
     * This is a complex type.
     */
    public static class IMScoreResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.imscore.result.IMScoreResponseDocument.IMScoreResponse
    {
        private static final long serialVersionUID = 1L;
        
        public IMScoreResponseImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName IDASTATUS$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/imscore/result", "IDAStatus");
        private static final javax.xml.namespace.QName APPID$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/imscore/result", "AppID");
        private static final javax.xml.namespace.QName DESIGNATION$4 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/imscore/result", "Designation");
        private static final javax.xml.namespace.QName IDASEQUENCE$6 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/imscore/result", "IDASequence");
        private static final javax.xml.namespace.QName IDATIMESTAMP$8 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/imscore/result", "IDATimeStamp");
        private static final javax.xml.namespace.QName RESOLUTIONSTATUS$10 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/imscore/result", "ResolutionStatus");
        private static final javax.xml.namespace.QName IMPERCENTILERANK$12 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/imscore/result", "IMPercentileRank");
        private static final javax.xml.namespace.QName PASSTHRU1$14 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/imscore/result", "PassThru1");
        private static final javax.xml.namespace.QName PASSTHRU2$16 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/imscore/result", "PassThru2");
        private static final javax.xml.namespace.QName PASSTHRU3$18 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/imscore/result", "PassThru3");
        private static final javax.xml.namespace.QName PASSTHRU4$20 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/imscore/result", "PassThru4");
        private static final javax.xml.namespace.QName INDICATORS$22 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/imscore/result", "Indicators");
        private static final javax.xml.namespace.QName IDAINTERNAL$24 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/imscore/result", "IDAInternal");
        
        
        /**
         * Gets the "IDAStatus" element
         */
        public int getIDAStatus()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASTATUS$0, 0);
                if (target == null)
                {
                    return 0;
                }
                return target.getIntValue();
            }
        }
        
        /**
         * Gets (as xml) the "IDAStatus" element
         */
        public com.idanalytics.products.common_v1.IDAStatus xgetIDAStatus()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDAStatus target = null;
                target = (com.idanalytics.products.common_v1.IDAStatus)get_store().find_element_user(IDASTATUS$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "IDAStatus" element
         */
        public void setIDAStatus(int idaStatus)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASTATUS$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDASTATUS$0);
                }
                target.setIntValue(idaStatus);
            }
        }
        
        /**
         * Sets (as xml) the "IDAStatus" element
         */
        public void xsetIDAStatus(com.idanalytics.products.common_v1.IDAStatus idaStatus)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDAStatus target = null;
                target = (com.idanalytics.products.common_v1.IDAStatus)get_store().find_element_user(IDASTATUS$0, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.IDAStatus)get_store().add_element_user(IDASTATUS$0);
                }
                target.set(idaStatus);
            }
        }
        
        /**
         * Gets the "AppID" element
         */
        public java.lang.String getAppID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(APPID$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "AppID" element
         */
        public com.idanalytics.products.common_v1.AppID xgetAppID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.AppID target = null;
                target = (com.idanalytics.products.common_v1.AppID)get_store().find_element_user(APPID$2, 0);
                return target;
            }
        }
        
        /**
         * Sets the "AppID" element
         */
        public void setAppID(java.lang.String appID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(APPID$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(APPID$2);
                }
                target.setStringValue(appID);
            }
        }
        
        /**
         * Sets (as xml) the "AppID" element
         */
        public void xsetAppID(com.idanalytics.products.common_v1.AppID appID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.AppID target = null;
                target = (com.idanalytics.products.common_v1.AppID)get_store().find_element_user(APPID$2, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.AppID)get_store().add_element_user(APPID$2);
                }
                target.set(appID);
            }
        }
        
        /**
         * Gets the "Designation" element
         */
        public com.idanalytics.products.common_v1.Designation.Enum getDesignation()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESIGNATION$4, 0);
                if (target == null)
                {
                    return null;
                }
                return (com.idanalytics.products.common_v1.Designation.Enum)target.getEnumValue();
            }
        }
        
        /**
         * Gets (as xml) the "Designation" element
         */
        public com.idanalytics.products.common_v1.Designation xgetDesignation()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.Designation target = null;
                target = (com.idanalytics.products.common_v1.Designation)get_store().find_element_user(DESIGNATION$4, 0);
                return target;
            }
        }
        
        /**
         * True if has "Designation" element
         */
        public boolean isSetDesignation()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(DESIGNATION$4) != 0;
            }
        }
        
        /**
         * Sets the "Designation" element
         */
        public void setDesignation(com.idanalytics.products.common_v1.Designation.Enum designation)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESIGNATION$4, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DESIGNATION$4);
                }
                target.setEnumValue(designation);
            }
        }
        
        /**
         * Sets (as xml) the "Designation" element
         */
        public void xsetDesignation(com.idanalytics.products.common_v1.Designation designation)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.Designation target = null;
                target = (com.idanalytics.products.common_v1.Designation)get_store().find_element_user(DESIGNATION$4, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.Designation)get_store().add_element_user(DESIGNATION$4);
                }
                target.set(designation);
            }
        }
        
        /**
         * Unsets the "Designation" element
         */
        public void unsetDesignation()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(DESIGNATION$4, 0);
            }
        }
        
        /**
         * Gets the "IDASequence" element
         */
        public java.lang.String getIDASequence()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASEQUENCE$6, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "IDASequence" element
         */
        public com.idanalytics.products.common_v1.IDASequence xgetIDASequence()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDASequence target = null;
                target = (com.idanalytics.products.common_v1.IDASequence)get_store().find_element_user(IDASEQUENCE$6, 0);
                return target;
            }
        }
        
        /**
         * Sets the "IDASequence" element
         */
        public void setIDASequence(java.lang.String idaSequence)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASEQUENCE$6, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDASEQUENCE$6);
                }
                target.setStringValue(idaSequence);
            }
        }
        
        /**
         * Sets (as xml) the "IDASequence" element
         */
        public void xsetIDASequence(com.idanalytics.products.common_v1.IDASequence idaSequence)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDASequence target = null;
                target = (com.idanalytics.products.common_v1.IDASequence)get_store().find_element_user(IDASEQUENCE$6, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.IDASequence)get_store().add_element_user(IDASEQUENCE$6);
                }
                target.set(idaSequence);
            }
        }
        
        /**
         * Gets the "IDATimeStamp" element
         */
        public java.util.Calendar getIDATimeStamp()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDATIMESTAMP$8, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getCalendarValue();
            }
        }
        
        /**
         * Gets (as xml) the "IDATimeStamp" element
         */
        public com.idanalytics.products.common_v1.IDATimeStamp xgetIDATimeStamp()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDATimeStamp target = null;
                target = (com.idanalytics.products.common_v1.IDATimeStamp)get_store().find_element_user(IDATIMESTAMP$8, 0);
                return target;
            }
        }
        
        /**
         * Sets the "IDATimeStamp" element
         */
        public void setIDATimeStamp(java.util.Calendar idaTimeStamp)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDATIMESTAMP$8, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDATIMESTAMP$8);
                }
                target.setCalendarValue(idaTimeStamp);
            }
        }
        
        /**
         * Sets (as xml) the "IDATimeStamp" element
         */
        public void xsetIDATimeStamp(com.idanalytics.products.common_v1.IDATimeStamp idaTimeStamp)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDATimeStamp target = null;
                target = (com.idanalytics.products.common_v1.IDATimeStamp)get_store().find_element_user(IDATIMESTAMP$8, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.IDATimeStamp)get_store().add_element_user(IDATIMESTAMP$8);
                }
                target.set(idaTimeStamp);
            }
        }
        
        /**
         * Gets the "ResolutionStatus" element
         */
        public com.idanalytics.products.imscore.result.ResolutionStatusDocument.ResolutionStatus getResolutionStatus()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.imscore.result.ResolutionStatusDocument.ResolutionStatus target = null;
                target = (com.idanalytics.products.imscore.result.ResolutionStatusDocument.ResolutionStatus)get_store().find_element_user(RESOLUTIONSTATUS$10, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * Sets the "ResolutionStatus" element
         */
        public void setResolutionStatus(com.idanalytics.products.imscore.result.ResolutionStatusDocument.ResolutionStatus resolutionStatus)
        {
            generatedSetterHelperImpl(resolutionStatus, RESOLUTIONSTATUS$10, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "ResolutionStatus" element
         */
        public com.idanalytics.products.imscore.result.ResolutionStatusDocument.ResolutionStatus addNewResolutionStatus()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.imscore.result.ResolutionStatusDocument.ResolutionStatus target = null;
                target = (com.idanalytics.products.imscore.result.ResolutionStatusDocument.ResolutionStatus)get_store().add_element_user(RESOLUTIONSTATUS$10);
                return target;
            }
        }
        
        /**
         * Gets the "IMPercentileRank" element
         */
        public java.lang.String getIMPercentileRank()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IMPERCENTILERANK$12, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "IMPercentileRank" element
         */
        public com.idanalytics.products.imscore.result.PercentileRank xgetIMPercentileRank()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.imscore.result.PercentileRank target = null;
                target = (com.idanalytics.products.imscore.result.PercentileRank)get_store().find_element_user(IMPERCENTILERANK$12, 0);
                return target;
            }
        }
        
        /**
         * True if has "IMPercentileRank" element
         */
        public boolean isSetIMPercentileRank()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(IMPERCENTILERANK$12) != 0;
            }
        }
        
        /**
         * Sets the "IMPercentileRank" element
         */
        public void setIMPercentileRank(java.lang.String imPercentileRank)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IMPERCENTILERANK$12, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IMPERCENTILERANK$12);
                }
                target.setStringValue(imPercentileRank);
            }
        }
        
        /**
         * Sets (as xml) the "IMPercentileRank" element
         */
        public void xsetIMPercentileRank(com.idanalytics.products.imscore.result.PercentileRank imPercentileRank)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.imscore.result.PercentileRank target = null;
                target = (com.idanalytics.products.imscore.result.PercentileRank)get_store().find_element_user(IMPERCENTILERANK$12, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.imscore.result.PercentileRank)get_store().add_element_user(IMPERCENTILERANK$12);
                }
                target.set(imPercentileRank);
            }
        }
        
        /**
         * Unsets the "IMPercentileRank" element
         */
        public void unsetIMPercentileRank()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(IMPERCENTILERANK$12, 0);
            }
        }
        
        /**
         * Gets the "PassThru1" element
         */
        public java.lang.String getPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU1$14, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru1" element
         */
        public com.idanalytics.products.common_v1.PassThru xgetPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU1$14, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru1" element
         */
        public boolean isSetPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU1$14) != 0;
            }
        }
        
        /**
         * Sets the "PassThru1" element
         */
        public void setPassThru1(java.lang.String passThru1)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU1$14, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU1$14);
                }
                target.setStringValue(passThru1);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru1" element
         */
        public void xsetPassThru1(com.idanalytics.products.common_v1.PassThru passThru1)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU1$14, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.PassThru)get_store().add_element_user(PASSTHRU1$14);
                }
                target.set(passThru1);
            }
        }
        
        /**
         * Unsets the "PassThru1" element
         */
        public void unsetPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU1$14, 0);
            }
        }
        
        /**
         * Gets the "PassThru2" element
         */
        public java.lang.String getPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU2$16, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru2" element
         */
        public com.idanalytics.products.common_v1.PassThru xgetPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU2$16, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru2" element
         */
        public boolean isSetPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU2$16) != 0;
            }
        }
        
        /**
         * Sets the "PassThru2" element
         */
        public void setPassThru2(java.lang.String passThru2)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU2$16, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU2$16);
                }
                target.setStringValue(passThru2);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru2" element
         */
        public void xsetPassThru2(com.idanalytics.products.common_v1.PassThru passThru2)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU2$16, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.PassThru)get_store().add_element_user(PASSTHRU2$16);
                }
                target.set(passThru2);
            }
        }
        
        /**
         * Unsets the "PassThru2" element
         */
        public void unsetPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU2$16, 0);
            }
        }
        
        /**
         * Gets the "PassThru3" element
         */
        public java.lang.String getPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU3$18, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru3" element
         */
        public com.idanalytics.products.common_v1.PassThru xgetPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU3$18, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru3" element
         */
        public boolean isSetPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU3$18) != 0;
            }
        }
        
        /**
         * Sets the "PassThru3" element
         */
        public void setPassThru3(java.lang.String passThru3)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU3$18, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU3$18);
                }
                target.setStringValue(passThru3);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru3" element
         */
        public void xsetPassThru3(com.idanalytics.products.common_v1.PassThru passThru3)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU3$18, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.PassThru)get_store().add_element_user(PASSTHRU3$18);
                }
                target.set(passThru3);
            }
        }
        
        /**
         * Unsets the "PassThru3" element
         */
        public void unsetPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU3$18, 0);
            }
        }
        
        /**
         * Gets the "PassThru4" element
         */
        public java.lang.String getPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU4$20, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru4" element
         */
        public com.idanalytics.products.common_v1.PassThru xgetPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU4$20, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru4" element
         */
        public boolean isSetPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU4$20) != 0;
            }
        }
        
        /**
         * Sets the "PassThru4" element
         */
        public void setPassThru4(java.lang.String passThru4)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU4$20, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU4$20);
                }
                target.setStringValue(passThru4);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru4" element
         */
        public void xsetPassThru4(com.idanalytics.products.common_v1.PassThru passThru4)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU4$20, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.PassThru)get_store().add_element_user(PASSTHRU4$20);
                }
                target.set(passThru4);
            }
        }
        
        /**
         * Unsets the "PassThru4" element
         */
        public void unsetPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU4$20, 0);
            }
        }
        
        /**
         * Gets the "Indicators" element
         */
        public com.idanalytics.products.imscore.result.IndicatorsDocument.Indicators getIndicators()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.imscore.result.IndicatorsDocument.Indicators target = null;
                target = (com.idanalytics.products.imscore.result.IndicatorsDocument.Indicators)get_store().find_element_user(INDICATORS$22, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "Indicators" element
         */
        public boolean isSetIndicators()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(INDICATORS$22) != 0;
            }
        }
        
        /**
         * Sets the "Indicators" element
         */
        public void setIndicators(com.idanalytics.products.imscore.result.IndicatorsDocument.Indicators indicators)
        {
            generatedSetterHelperImpl(indicators, INDICATORS$22, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "Indicators" element
         */
        public com.idanalytics.products.imscore.result.IndicatorsDocument.Indicators addNewIndicators()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.imscore.result.IndicatorsDocument.Indicators target = null;
                target = (com.idanalytics.products.imscore.result.IndicatorsDocument.Indicators)get_store().add_element_user(INDICATORS$22);
                return target;
            }
        }
        
        /**
         * Unsets the "Indicators" element
         */
        public void unsetIndicators()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(INDICATORS$22, 0);
            }
        }
        
        /**
         * Gets the "IDAInternal" element
         */
        public com.idanalytics.products.imscore.result.IDAInternalDocument.IDAInternal getIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.imscore.result.IDAInternalDocument.IDAInternal target = null;
                target = (com.idanalytics.products.imscore.result.IDAInternalDocument.IDAInternal)get_store().find_element_user(IDAINTERNAL$24, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "IDAInternal" element
         */
        public boolean isSetIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(IDAINTERNAL$24) != 0;
            }
        }
        
        /**
         * Sets the "IDAInternal" element
         */
        public void setIDAInternal(com.idanalytics.products.imscore.result.IDAInternalDocument.IDAInternal idaInternal)
        {
            generatedSetterHelperImpl(idaInternal, IDAINTERNAL$24, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "IDAInternal" element
         */
        public com.idanalytics.products.imscore.result.IDAInternalDocument.IDAInternal addNewIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.imscore.result.IDAInternalDocument.IDAInternal target = null;
                target = (com.idanalytics.products.imscore.result.IDAInternalDocument.IDAInternal)get_store().add_element_user(IDAINTERNAL$24);
                return target;
            }
        }
        
        /**
         * Unsets the "IDAInternal" element
         */
        public void unsetIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(IDAINTERNAL$24, 0);
            }
        }
    }
}
