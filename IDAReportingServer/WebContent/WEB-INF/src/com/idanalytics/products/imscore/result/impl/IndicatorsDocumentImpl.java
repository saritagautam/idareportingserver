/*
 * An XML document type.
 * Localname: Indicators
 * Namespace: http://idanalytics.com/products/imscore/result
 * Java type: com.idanalytics.products.imscore.result.IndicatorsDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.imscore.result.impl;
/**
 * A document containing one Indicators(@http://idanalytics.com/products/imscore/result) element.
 *
 * This is a complex type.
 */
public class IndicatorsDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.imscore.result.IndicatorsDocument
{
    private static final long serialVersionUID = 1L;
    
    public IndicatorsDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName INDICATORS$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/imscore/result", "Indicators");
    
    
    /**
     * Gets the "Indicators" element
     */
    public com.idanalytics.products.imscore.result.IndicatorsDocument.Indicators getIndicators()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.imscore.result.IndicatorsDocument.Indicators target = null;
            target = (com.idanalytics.products.imscore.result.IndicatorsDocument.Indicators)get_store().find_element_user(INDICATORS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "Indicators" element
     */
    public void setIndicators(com.idanalytics.products.imscore.result.IndicatorsDocument.Indicators indicators)
    {
        generatedSetterHelperImpl(indicators, INDICATORS$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "Indicators" element
     */
    public com.idanalytics.products.imscore.result.IndicatorsDocument.Indicators addNewIndicators()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.imscore.result.IndicatorsDocument.Indicators target = null;
            target = (com.idanalytics.products.imscore.result.IndicatorsDocument.Indicators)get_store().add_element_user(INDICATORS$0);
            return target;
        }
    }
    /**
     * An XML Indicators(@http://idanalytics.com/products/imscore/result).
     *
     * This is a complex type.
     */
    public static class IndicatorsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.imscore.result.IndicatorsDocument.Indicators
    {
        private static final long serialVersionUID = 1L;
        
        public IndicatorsImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName INDICATORGROUP$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/imscore/result", "IndicatorGroup");
        
        
        /**
         * Gets array of all "IndicatorGroup" elements
         */
        public com.idanalytics.products.imscore.result.IndicatorGroupDocument.IndicatorGroup[] getIndicatorGroupArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(INDICATORGROUP$0, targetList);
                com.idanalytics.products.imscore.result.IndicatorGroupDocument.IndicatorGroup[] result = new com.idanalytics.products.imscore.result.IndicatorGroupDocument.IndicatorGroup[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "IndicatorGroup" element
         */
        public com.idanalytics.products.imscore.result.IndicatorGroupDocument.IndicatorGroup getIndicatorGroupArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.imscore.result.IndicatorGroupDocument.IndicatorGroup target = null;
                target = (com.idanalytics.products.imscore.result.IndicatorGroupDocument.IndicatorGroup)get_store().find_element_user(INDICATORGROUP$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "IndicatorGroup" element
         */
        public int sizeOfIndicatorGroupArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(INDICATORGROUP$0);
            }
        }
        
        /**
         * Sets array of all "IndicatorGroup" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setIndicatorGroupArray(com.idanalytics.products.imscore.result.IndicatorGroupDocument.IndicatorGroup[] indicatorGroupArray)
        {
            check_orphaned();
            arraySetterHelper(indicatorGroupArray, INDICATORGROUP$0);
        }
        
        /**
         * Sets ith "IndicatorGroup" element
         */
        public void setIndicatorGroupArray(int i, com.idanalytics.products.imscore.result.IndicatorGroupDocument.IndicatorGroup indicatorGroup)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.imscore.result.IndicatorGroupDocument.IndicatorGroup target = null;
                target = (com.idanalytics.products.imscore.result.IndicatorGroupDocument.IndicatorGroup)get_store().find_element_user(INDICATORGROUP$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(indicatorGroup);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "IndicatorGroup" element
         */
        public com.idanalytics.products.imscore.result.IndicatorGroupDocument.IndicatorGroup insertNewIndicatorGroup(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.imscore.result.IndicatorGroupDocument.IndicatorGroup target = null;
                target = (com.idanalytics.products.imscore.result.IndicatorGroupDocument.IndicatorGroup)get_store().insert_element_user(INDICATORGROUP$0, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "IndicatorGroup" element
         */
        public com.idanalytics.products.imscore.result.IndicatorGroupDocument.IndicatorGroup addNewIndicatorGroup()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.imscore.result.IndicatorGroupDocument.IndicatorGroup target = null;
                target = (com.idanalytics.products.imscore.result.IndicatorGroupDocument.IndicatorGroup)get_store().add_element_user(INDICATORGROUP$0);
                return target;
            }
        }
        
        /**
         * Removes the ith "IndicatorGroup" element
         */
        public void removeIndicatorGroup(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(INDICATORGROUP$0, i);
            }
        }
    }
}
