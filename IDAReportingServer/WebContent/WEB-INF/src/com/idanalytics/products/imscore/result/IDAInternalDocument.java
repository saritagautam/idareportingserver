/*
 * An XML document type.
 * Localname: IDAInternal
 * Namespace: http://idanalytics.com/products/imscore/result
 * Java type: com.idanalytics.products.imscore.result.IDAInternalDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.imscore.result;


/**
 * A document containing one IDAInternal(@http://idanalytics.com/products/imscore/result) element.
 *
 * This is a complex type.
 */
public interface IDAInternalDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(IDAInternalDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("idainternaldf9cdoctype");
    
    /**
     * Gets the "IDAInternal" element
     */
    com.idanalytics.products.imscore.result.IDAInternalDocument.IDAInternal getIDAInternal();
    
    /**
     * Sets the "IDAInternal" element
     */
    void setIDAInternal(com.idanalytics.products.imscore.result.IDAInternalDocument.IDAInternal idaInternal);
    
    /**
     * Appends and returns a new empty "IDAInternal" element
     */
    com.idanalytics.products.imscore.result.IDAInternalDocument.IDAInternal addNewIDAInternal();
    
    /**
     * An XML IDAInternal(@http://idanalytics.com/products/imscore/result).
     *
     * This is a complex type.
     */
    public interface IDAInternal extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(IDAInternal.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("idainternalfa2delemtype");
        
        /**
         * Gets array of all "Group" elements
         */
        com.idanalytics.products.imscore.result.GroupDocument.Group[] getGroupArray();
        
        /**
         * Gets ith "Group" element
         */
        com.idanalytics.products.imscore.result.GroupDocument.Group getGroupArray(int i);
        
        /**
         * Returns number of "Group" element
         */
        int sizeOfGroupArray();
        
        /**
         * Sets array of all "Group" element
         */
        void setGroupArray(com.idanalytics.products.imscore.result.GroupDocument.Group[] groupArray);
        
        /**
         * Sets ith "Group" element
         */
        void setGroupArray(int i, com.idanalytics.products.imscore.result.GroupDocument.Group group);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "Group" element
         */
        com.idanalytics.products.imscore.result.GroupDocument.Group insertNewGroup(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "Group" element
         */
        com.idanalytics.products.imscore.result.GroupDocument.Group addNewGroup();
        
        /**
         * Removes the ith "Group" element
         */
        void removeGroup(int i);
        
        /**
         * Gets array of all "Xml" elements
         */
        com.idanalytics.products.imscore.result.XmlInternal[] getXmlArray();
        
        /**
         * Gets ith "Xml" element
         */
        com.idanalytics.products.imscore.result.XmlInternal getXmlArray(int i);
        
        /**
         * Returns number of "Xml" element
         */
        int sizeOfXmlArray();
        
        /**
         * Sets array of all "Xml" element
         */
        void setXmlArray(com.idanalytics.products.imscore.result.XmlInternal[] xmlArray);
        
        /**
         * Sets ith "Xml" element
         */
        void setXmlArray(int i, com.idanalytics.products.imscore.result.XmlInternal xml);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "Xml" element
         */
        com.idanalytics.products.imscore.result.XmlInternal insertNewXml(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "Xml" element
         */
        com.idanalytics.products.imscore.result.XmlInternal addNewXml();
        
        /**
         * Removes the ith "Xml" element
         */
        void removeXml(int i);
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.imscore.result.IDAInternalDocument.IDAInternal newInstance() {
              return (com.idanalytics.products.imscore.result.IDAInternalDocument.IDAInternal) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.imscore.result.IDAInternalDocument.IDAInternal newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.imscore.result.IDAInternalDocument.IDAInternal) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.imscore.result.IDAInternalDocument newInstance() {
          return (com.idanalytics.products.imscore.result.IDAInternalDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.imscore.result.IDAInternalDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.imscore.result.IDAInternalDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.imscore.result.IDAInternalDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.imscore.result.IDAInternalDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.imscore.result.IDAInternalDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.imscore.result.IDAInternalDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.imscore.result.IDAInternalDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.imscore.result.IDAInternalDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.imscore.result.IDAInternalDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.imscore.result.IDAInternalDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.imscore.result.IDAInternalDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.imscore.result.IDAInternalDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.imscore.result.IDAInternalDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.imscore.result.IDAInternalDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.imscore.result.IDAInternalDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.imscore.result.IDAInternalDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.imscore.result.IDAInternalDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.imscore.result.IDAInternalDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.imscore.result.IDAInternalDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.imscore.result.IDAInternalDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.imscore.result.IDAInternalDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.imscore.result.IDAInternalDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.imscore.result.IDAInternalDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.imscore.result.IDAInternalDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.imscore.result.IDAInternalDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.imscore.result.IDAInternalDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.imscore.result.IDAInternalDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.imscore.result.IDAInternalDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.imscore.result.IDAInternalDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.imscore.result.IDAInternalDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.imscore.result.IDAInternalDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.imscore.result.IDAInternalDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.imscore.result.IDAInternalDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.imscore.result.IDAInternalDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
