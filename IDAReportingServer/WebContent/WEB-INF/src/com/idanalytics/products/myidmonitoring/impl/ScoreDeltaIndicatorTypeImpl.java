/*
 * XML Type:  ScoreDeltaIndicatorType
 * Namespace: http://idanalytics.com/products/myidmonitoring
 * Java type: com.idanalytics.products.myidmonitoring.ScoreDeltaIndicatorType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.myidmonitoring.impl;
/**
 * An XML ScoreDeltaIndicatorType(@http://idanalytics.com/products/myidmonitoring).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.myidmonitoring.ScoreDeltaIndicatorType.
 */
public class ScoreDeltaIndicatorTypeImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.idanalytics.products.myidmonitoring.ScoreDeltaIndicatorType
{
    private static final long serialVersionUID = 1L;
    
    public ScoreDeltaIndicatorTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected ScoreDeltaIndicatorTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
