/*
 * XML Type:  AlertEvent
 * Namespace: http://idanalytics.com/products/myidmonitoring
 * Java type: com.idanalytics.products.myidmonitoring.AlertEvent
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.myidmonitoring;


/**
 * An XML AlertEvent(@http://idanalytics.com/products/myidmonitoring).
 *
 * This is a complex type.
 */
public interface AlertEvent extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(AlertEvent.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("alerteventfb50type");
    
    /**
     * Gets the "Affiliate" element
     */
    java.lang.String getAffiliate();
    
    /**
     * Gets (as xml) the "Affiliate" element
     */
    com.idanalytics.products.myidmonitoring.String50 xgetAffiliate();
    
    /**
     * Sets the "Affiliate" element
     */
    void setAffiliate(java.lang.String affiliate);
    
    /**
     * Sets (as xml) the "Affiliate" element
     */
    void xsetAffiliate(com.idanalytics.products.myidmonitoring.String50 affiliate);
    
    /**
     * Gets the "ConsumerID" element
     */
    java.lang.String getConsumerID();
    
    /**
     * Gets (as xml) the "ConsumerID" element
     */
    com.idanalytics.products.myidmonitoring.String50 xgetConsumerID();
    
    /**
     * Sets the "ConsumerID" element
     */
    void setConsumerID(java.lang.String consumerID);
    
    /**
     * Sets (as xml) the "ConsumerID" element
     */
    void xsetConsumerID(com.idanalytics.products.myidmonitoring.String50 consumerID);
    
    /**
     * Gets the "IDScore" element
     */
    java.lang.String getIDScore();
    
    /**
     * Gets (as xml) the "IDScore" element
     */
    com.idanalytics.products.myidmonitoring.String3 xgetIDScore();
    
    /**
     * True if has "IDScore" element
     */
    boolean isSetIDScore();
    
    /**
     * Sets the "IDScore" element
     */
    void setIDScore(java.lang.String idScore);
    
    /**
     * Sets (as xml) the "IDScore" element
     */
    void xsetIDScore(com.idanalytics.products.myidmonitoring.String3 idScore);
    
    /**
     * Unsets the "IDScore" element
     */
    void unsetIDScore();
    
    /**
     * Gets the "IDASequence" element
     */
    java.lang.String getIDASequence();
    
    /**
     * Gets (as xml) the "IDASequence" element
     */
    com.idanalytics.products.myidmonitoring.String30 xgetIDASequence();
    
    /**
     * True if has "IDASequence" element
     */
    boolean isSetIDASequence();
    
    /**
     * Sets the "IDASequence" element
     */
    void setIDASequence(java.lang.String idaSequence);
    
    /**
     * Sets (as xml) the "IDASequence" element
     */
    void xsetIDASequence(com.idanalytics.products.myidmonitoring.String30 idaSequence);
    
    /**
     * Unsets the "IDASequence" element
     */
    void unsetIDASequence();
    
    /**
     * Gets the "IDATimeStamp" element
     */
    java.util.Calendar getIDATimeStamp();
    
    /**
     * Gets (as xml) the "IDATimeStamp" element
     */
    org.apache.xmlbeans.XmlDateTime xgetIDATimeStamp();
    
    /**
     * True if has "IDATimeStamp" element
     */
    boolean isSetIDATimeStamp();
    
    /**
     * Sets the "IDATimeStamp" element
     */
    void setIDATimeStamp(java.util.Calendar idaTimeStamp);
    
    /**
     * Sets (as xml) the "IDATimeStamp" element
     */
    void xsetIDATimeStamp(org.apache.xmlbeans.XmlDateTime idaTimeStamp);
    
    /**
     * Unsets the "IDATimeStamp" element
     */
    void unsetIDATimeStamp();
    
    /**
     * Gets the "ScoreBandIndicator" element
     */
    com.idanalytics.products.myidmonitoring.ScoreBandIndicatorType.Enum getScoreBandIndicator();
    
    /**
     * Gets (as xml) the "ScoreBandIndicator" element
     */
    com.idanalytics.products.myidmonitoring.ScoreBandIndicatorType xgetScoreBandIndicator();
    
    /**
     * True if has "ScoreBandIndicator" element
     */
    boolean isSetScoreBandIndicator();
    
    /**
     * Sets the "ScoreBandIndicator" element
     */
    void setScoreBandIndicator(com.idanalytics.products.myidmonitoring.ScoreBandIndicatorType.Enum scoreBandIndicator);
    
    /**
     * Sets (as xml) the "ScoreBandIndicator" element
     */
    void xsetScoreBandIndicator(com.idanalytics.products.myidmonitoring.ScoreBandIndicatorType scoreBandIndicator);
    
    /**
     * Unsets the "ScoreBandIndicator" element
     */
    void unsetScoreBandIndicator();
    
    /**
     * Gets the "ScoreDeltaIndicator" element
     */
    com.idanalytics.products.myidmonitoring.ScoreDeltaIndicatorType.Enum getScoreDeltaIndicator();
    
    /**
     * Gets (as xml) the "ScoreDeltaIndicator" element
     */
    com.idanalytics.products.myidmonitoring.ScoreDeltaIndicatorType xgetScoreDeltaIndicator();
    
    /**
     * True if has "ScoreDeltaIndicator" element
     */
    boolean isSetScoreDeltaIndicator();
    
    /**
     * Sets the "ScoreDeltaIndicator" element
     */
    void setScoreDeltaIndicator(com.idanalytics.products.myidmonitoring.ScoreDeltaIndicatorType.Enum scoreDeltaIndicator);
    
    /**
     * Sets (as xml) the "ScoreDeltaIndicator" element
     */
    void xsetScoreDeltaIndicator(com.idanalytics.products.myidmonitoring.ScoreDeltaIndicatorType scoreDeltaIndicator);
    
    /**
     * Unsets the "ScoreDeltaIndicator" element
     */
    void unsetScoreDeltaIndicator();
    
    /**
     * Gets the "Indicators" element
     */
    com.idanalytics.products.myidmonitoring.IndicatorsDocument.Indicators getIndicators();
    
    /**
     * True if has "Indicators" element
     */
    boolean isSetIndicators();
    
    /**
     * Sets the "Indicators" element
     */
    void setIndicators(com.idanalytics.products.myidmonitoring.IndicatorsDocument.Indicators indicators);
    
    /**
     * Appends and returns a new empty "Indicators" element
     */
    com.idanalytics.products.myidmonitoring.IndicatorsDocument.Indicators addNewIndicators();
    
    /**
     * Unsets the "Indicators" element
     */
    void unsetIndicators();
    
    /**
     * Gets the "TriggerEvent" element
     */
    com.idanalytics.products.myidmonitoring.IdentityEvent getTriggerEvent();
    
    /**
     * True if has "TriggerEvent" element
     */
    boolean isSetTriggerEvent();
    
    /**
     * Sets the "TriggerEvent" element
     */
    void setTriggerEvent(com.idanalytics.products.myidmonitoring.IdentityEvent triggerEvent);
    
    /**
     * Appends and returns a new empty "TriggerEvent" element
     */
    com.idanalytics.products.myidmonitoring.IdentityEvent addNewTriggerEvent();
    
    /**
     * Unsets the "TriggerEvent" element
     */
    void unsetTriggerEvent();
    
    /**
     * Gets the "PreviousEvents" element
     */
    com.idanalytics.products.myidmonitoring.PreviousEventsDocument.PreviousEvents getPreviousEvents();
    
    /**
     * True if has "PreviousEvents" element
     */
    boolean isSetPreviousEvents();
    
    /**
     * Sets the "PreviousEvents" element
     */
    void setPreviousEvents(com.idanalytics.products.myidmonitoring.PreviousEventsDocument.PreviousEvents previousEvents);
    
    /**
     * Appends and returns a new empty "PreviousEvents" element
     */
    com.idanalytics.products.myidmonitoring.PreviousEventsDocument.PreviousEvents addNewPreviousEvents();
    
    /**
     * Unsets the "PreviousEvents" element
     */
    void unsetPreviousEvents();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.myidmonitoring.AlertEvent newInstance() {
          return (com.idanalytics.products.myidmonitoring.AlertEvent) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.myidmonitoring.AlertEvent newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.myidmonitoring.AlertEvent) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.myidmonitoring.AlertEvent parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.myidmonitoring.AlertEvent) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.myidmonitoring.AlertEvent parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.myidmonitoring.AlertEvent) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.myidmonitoring.AlertEvent parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.myidmonitoring.AlertEvent) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.myidmonitoring.AlertEvent parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.myidmonitoring.AlertEvent) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.myidmonitoring.AlertEvent parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.myidmonitoring.AlertEvent) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.myidmonitoring.AlertEvent parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.myidmonitoring.AlertEvent) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.myidmonitoring.AlertEvent parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.myidmonitoring.AlertEvent) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.myidmonitoring.AlertEvent parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.myidmonitoring.AlertEvent) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.myidmonitoring.AlertEvent parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.myidmonitoring.AlertEvent) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.myidmonitoring.AlertEvent parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.myidmonitoring.AlertEvent) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.myidmonitoring.AlertEvent parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.myidmonitoring.AlertEvent) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.myidmonitoring.AlertEvent parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.myidmonitoring.AlertEvent) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.myidmonitoring.AlertEvent parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.myidmonitoring.AlertEvent) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.myidmonitoring.AlertEvent parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.myidmonitoring.AlertEvent) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.myidmonitoring.AlertEvent parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.myidmonitoring.AlertEvent) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.myidmonitoring.AlertEvent parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.myidmonitoring.AlertEvent) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
