/*
 * An XML document type.
 * Localname: TriggerEvent
 * Namespace: http://idanalytics.com/products/myidmonitoring
 * Java type: com.idanalytics.products.myidmonitoring.TriggerEventDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.myidmonitoring.impl;
/**
 * A document containing one TriggerEvent(@http://idanalytics.com/products/myidmonitoring) element.
 *
 * This is a complex type.
 */
public class TriggerEventDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.myidmonitoring.TriggerEventDocument
{
    private static final long serialVersionUID = 1L;
    
    public TriggerEventDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName TRIGGEREVENT$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/myidmonitoring", "TriggerEvent");
    
    
    /**
     * Gets the "TriggerEvent" element
     */
    public com.idanalytics.products.myidmonitoring.IdentityEvent getTriggerEvent()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.myidmonitoring.IdentityEvent target = null;
            target = (com.idanalytics.products.myidmonitoring.IdentityEvent)get_store().find_element_user(TRIGGEREVENT$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "TriggerEvent" element
     */
    public void setTriggerEvent(com.idanalytics.products.myidmonitoring.IdentityEvent triggerEvent)
    {
        generatedSetterHelperImpl(triggerEvent, TRIGGEREVENT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "TriggerEvent" element
     */
    public com.idanalytics.products.myidmonitoring.IdentityEvent addNewTriggerEvent()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.myidmonitoring.IdentityEvent target = null;
            target = (com.idanalytics.products.myidmonitoring.IdentityEvent)get_store().add_element_user(TRIGGEREVENT$0);
            return target;
        }
    }
}
