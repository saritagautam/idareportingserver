/*
 * An XML document type.
 * Localname: PreviousEvents
 * Namespace: http://idanalytics.com/products/myidmonitoring
 * Java type: com.idanalytics.products.myidmonitoring.PreviousEventsDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.myidmonitoring.impl;
/**
 * A document containing one PreviousEvents(@http://idanalytics.com/products/myidmonitoring) element.
 *
 * This is a complex type.
 */
public class PreviousEventsDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.myidmonitoring.PreviousEventsDocument
{
    private static final long serialVersionUID = 1L;
    
    public PreviousEventsDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName PREVIOUSEVENTS$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/myidmonitoring", "PreviousEvents");
    
    
    /**
     * Gets the "PreviousEvents" element
     */
    public com.idanalytics.products.myidmonitoring.PreviousEventsDocument.PreviousEvents getPreviousEvents()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.myidmonitoring.PreviousEventsDocument.PreviousEvents target = null;
            target = (com.idanalytics.products.myidmonitoring.PreviousEventsDocument.PreviousEvents)get_store().find_element_user(PREVIOUSEVENTS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "PreviousEvents" element
     */
    public void setPreviousEvents(com.idanalytics.products.myidmonitoring.PreviousEventsDocument.PreviousEvents previousEvents)
    {
        generatedSetterHelperImpl(previousEvents, PREVIOUSEVENTS$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "PreviousEvents" element
     */
    public com.idanalytics.products.myidmonitoring.PreviousEventsDocument.PreviousEvents addNewPreviousEvents()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.myidmonitoring.PreviousEventsDocument.PreviousEvents target = null;
            target = (com.idanalytics.products.myidmonitoring.PreviousEventsDocument.PreviousEvents)get_store().add_element_user(PREVIOUSEVENTS$0);
            return target;
        }
    }
    /**
     * An XML PreviousEvents(@http://idanalytics.com/products/myidmonitoring).
     *
     * This is a complex type.
     */
    public static class PreviousEventsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.myidmonitoring.PreviousEventsDocument.PreviousEvents
    {
        private static final long serialVersionUID = 1L;
        
        public PreviousEventsImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName PREVIOUSEVENT$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/myidmonitoring", "PreviousEvent");
        
        
        /**
         * Gets array of all "PreviousEvent" elements
         */
        public com.idanalytics.products.myidmonitoring.IdentityEvent[] getPreviousEventArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(PREVIOUSEVENT$0, targetList);
                com.idanalytics.products.myidmonitoring.IdentityEvent[] result = new com.idanalytics.products.myidmonitoring.IdentityEvent[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "PreviousEvent" element
         */
        public com.idanalytics.products.myidmonitoring.IdentityEvent getPreviousEventArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.myidmonitoring.IdentityEvent target = null;
                target = (com.idanalytics.products.myidmonitoring.IdentityEvent)get_store().find_element_user(PREVIOUSEVENT$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "PreviousEvent" element
         */
        public int sizeOfPreviousEventArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PREVIOUSEVENT$0);
            }
        }
        
        /**
         * Sets array of all "PreviousEvent" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setPreviousEventArray(com.idanalytics.products.myidmonitoring.IdentityEvent[] previousEventArray)
        {
            check_orphaned();
            arraySetterHelper(previousEventArray, PREVIOUSEVENT$0);
        }
        
        /**
         * Sets ith "PreviousEvent" element
         */
        public void setPreviousEventArray(int i, com.idanalytics.products.myidmonitoring.IdentityEvent previousEvent)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.myidmonitoring.IdentityEvent target = null;
                target = (com.idanalytics.products.myidmonitoring.IdentityEvent)get_store().find_element_user(PREVIOUSEVENT$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(previousEvent);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "PreviousEvent" element
         */
        public com.idanalytics.products.myidmonitoring.IdentityEvent insertNewPreviousEvent(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.myidmonitoring.IdentityEvent target = null;
                target = (com.idanalytics.products.myidmonitoring.IdentityEvent)get_store().insert_element_user(PREVIOUSEVENT$0, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "PreviousEvent" element
         */
        public com.idanalytics.products.myidmonitoring.IdentityEvent addNewPreviousEvent()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.myidmonitoring.IdentityEvent target = null;
                target = (com.idanalytics.products.myidmonitoring.IdentityEvent)get_store().add_element_user(PREVIOUSEVENT$0);
                return target;
            }
        }
        
        /**
         * Removes the ith "PreviousEvent" element
         */
        public void removePreviousEvent(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PREVIOUSEVENT$0, i);
            }
        }
    }
}
