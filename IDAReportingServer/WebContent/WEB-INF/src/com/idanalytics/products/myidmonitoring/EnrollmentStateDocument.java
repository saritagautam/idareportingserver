/*
 * An XML document type.
 * Localname: EnrollmentState
 * Namespace: http://idanalytics.com/products/myidmonitoring
 * Java type: com.idanalytics.products.myidmonitoring.EnrollmentStateDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.myidmonitoring;


/**
 * A document containing one EnrollmentState(@http://idanalytics.com/products/myidmonitoring) element.
 *
 * This is a complex type.
 */
public interface EnrollmentStateDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(EnrollmentStateDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("enrollmentstate9363doctype");
    
    /**
     * Gets the "EnrollmentState" element
     */
    com.idanalytics.products.myidmonitoring.EnrollmentStateDocument.EnrollmentState getEnrollmentState();
    
    /**
     * Sets the "EnrollmentState" element
     */
    void setEnrollmentState(com.idanalytics.products.myidmonitoring.EnrollmentStateDocument.EnrollmentState enrollmentState);
    
    /**
     * Appends and returns a new empty "EnrollmentState" element
     */
    com.idanalytics.products.myidmonitoring.EnrollmentStateDocument.EnrollmentState addNewEnrollmentState();
    
    /**
     * An XML EnrollmentState(@http://idanalytics.com/products/myidmonitoring).
     *
     * This is a complex type.
     */
    public interface EnrollmentState extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(EnrollmentState.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("enrollmentstatec24aelemtype");
        
        /**
         * Gets the "Affiliate" element
         */
        java.lang.String getAffiliate();
        
        /**
         * Gets (as xml) the "Affiliate" element
         */
        com.idanalytics.products.myidmonitoring.String50 xgetAffiliate();
        
        /**
         * Sets the "Affiliate" element
         */
        void setAffiliate(java.lang.String affiliate);
        
        /**
         * Sets (as xml) the "Affiliate" element
         */
        void xsetAffiliate(com.idanalytics.products.myidmonitoring.String50 affiliate);
        
        /**
         * Gets the "ConsumerID" element
         */
        java.lang.String getConsumerID();
        
        /**
         * Gets (as xml) the "ConsumerID" element
         */
        com.idanalytics.products.myidmonitoring.String50 xgetConsumerID();
        
        /**
         * Sets the "ConsumerID" element
         */
        void setConsumerID(java.lang.String consumerID);
        
        /**
         * Sets (as xml) the "ConsumerID" element
         */
        void xsetConsumerID(com.idanalytics.products.myidmonitoring.String50 consumerID);
        
        /**
         * Gets the "PreviouslyEnrolled" element
         */
        boolean getPreviouslyEnrolled();
        
        /**
         * Gets (as xml) the "PreviouslyEnrolled" element
         */
        org.apache.xmlbeans.XmlBoolean xgetPreviouslyEnrolled();
        
        /**
         * Sets the "PreviouslyEnrolled" element
         */
        void setPreviouslyEnrolled(boolean previouslyEnrolled);
        
        /**
         * Sets (as xml) the "PreviouslyEnrolled" element
         */
        void xsetPreviouslyEnrolled(org.apache.xmlbeans.XmlBoolean previouslyEnrolled);
        
        /**
         * Gets the "EnrollmentDate" element
         */
        java.util.Calendar getEnrollmentDate();
        
        /**
         * Gets (as xml) the "EnrollmentDate" element
         */
        org.apache.xmlbeans.XmlDateTime xgetEnrollmentDate();
        
        /**
         * True if has "EnrollmentDate" element
         */
        boolean isSetEnrollmentDate();
        
        /**
         * Sets the "EnrollmentDate" element
         */
        void setEnrollmentDate(java.util.Calendar enrollmentDate);
        
        /**
         * Sets (as xml) the "EnrollmentDate" element
         */
        void xsetEnrollmentDate(org.apache.xmlbeans.XmlDateTime enrollmentDate);
        
        /**
         * Unsets the "EnrollmentDate" element
         */
        void unsetEnrollmentDate();
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.myidmonitoring.EnrollmentStateDocument.EnrollmentState newInstance() {
              return (com.idanalytics.products.myidmonitoring.EnrollmentStateDocument.EnrollmentState) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.myidmonitoring.EnrollmentStateDocument.EnrollmentState newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.myidmonitoring.EnrollmentStateDocument.EnrollmentState) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.myidmonitoring.EnrollmentStateDocument newInstance() {
          return (com.idanalytics.products.myidmonitoring.EnrollmentStateDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.myidmonitoring.EnrollmentStateDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.myidmonitoring.EnrollmentStateDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.myidmonitoring.EnrollmentStateDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.myidmonitoring.EnrollmentStateDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.myidmonitoring.EnrollmentStateDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.myidmonitoring.EnrollmentStateDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.myidmonitoring.EnrollmentStateDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.myidmonitoring.EnrollmentStateDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.myidmonitoring.EnrollmentStateDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.myidmonitoring.EnrollmentStateDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.myidmonitoring.EnrollmentStateDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.myidmonitoring.EnrollmentStateDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.myidmonitoring.EnrollmentStateDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.myidmonitoring.EnrollmentStateDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.myidmonitoring.EnrollmentStateDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.myidmonitoring.EnrollmentStateDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.myidmonitoring.EnrollmentStateDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.myidmonitoring.EnrollmentStateDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.myidmonitoring.EnrollmentStateDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.myidmonitoring.EnrollmentStateDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.myidmonitoring.EnrollmentStateDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.myidmonitoring.EnrollmentStateDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.myidmonitoring.EnrollmentStateDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.myidmonitoring.EnrollmentStateDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.myidmonitoring.EnrollmentStateDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.myidmonitoring.EnrollmentStateDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.myidmonitoring.EnrollmentStateDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.myidmonitoring.EnrollmentStateDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.myidmonitoring.EnrollmentStateDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.myidmonitoring.EnrollmentStateDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.myidmonitoring.EnrollmentStateDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.myidmonitoring.EnrollmentStateDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.myidmonitoring.EnrollmentStateDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.myidmonitoring.EnrollmentStateDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
