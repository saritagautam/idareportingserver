/*
 * XML Type:  string9z
 * Namespace: http://idanalytics.com/products/myidmonitoring
 * Java type: com.idanalytics.products.myidmonitoring.String9Z
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.myidmonitoring.impl;
/**
 * An XML string9z(@http://idanalytics.com/products/myidmonitoring).
 *
 * This is a union type. Instances are of one of the following types:
 *     com.idanalytics.products.myidmonitoring.String9Z$Member
 *     com.idanalytics.products.myidmonitoring.String9Z$Member2
 *     com.idanalytics.products.myidmonitoring.String9Z$Member3
 */
public class String9ZImpl extends org.apache.xmlbeans.impl.values.XmlUnionImpl implements com.idanalytics.products.myidmonitoring.String9Z, com.idanalytics.products.myidmonitoring.String9Z.Member, com.idanalytics.products.myidmonitoring.String9Z.Member2, com.idanalytics.products.myidmonitoring.String9Z.Member3
{
    private static final long serialVersionUID = 1L;
    
    public String9ZImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected String9ZImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
    /**
     * An anonymous inner XML type.
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.myidmonitoring.String9Z$Member.
     */
    public static class MemberImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.myidmonitoring.String9Z.Member
    {
        private static final long serialVersionUID = 1L;
        
        public MemberImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected MemberImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
    /**
     * An anonymous inner XML type.
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.myidmonitoring.String9Z$Member2.
     */
    public static class MemberImpl2 extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.myidmonitoring.String9Z.Member2
    {
        private static final long serialVersionUID = 1L;
        
        public MemberImpl2(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected MemberImpl2(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
    /**
     * An anonymous inner XML type.
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.myidmonitoring.String9Z$Member3.
     */
    public static class MemberImpl3 extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.myidmonitoring.String9Z.Member3
    {
        private static final long serialVersionUID = 1L;
        
        public MemberImpl3(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected MemberImpl3(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
