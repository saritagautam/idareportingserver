/*
 * An XML document type.
 * Localname: NotificationRequest
 * Namespace: http://idanalytics.com/products/myidmonitoring
 * Java type: com.idanalytics.products.myidmonitoring.NotificationRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.myidmonitoring.impl;
/**
 * A document containing one NotificationRequest(@http://idanalytics.com/products/myidmonitoring) element.
 *
 * This is a complex type.
 */
public class NotificationRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.myidmonitoring.NotificationRequestDocument
{
    private static final long serialVersionUID = 1L;
    
    public NotificationRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName NOTIFICATIONREQUEST$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/myidmonitoring", "NotificationRequest");
    
    
    /**
     * Gets the "NotificationRequest" element
     */
    public com.idanalytics.products.myidmonitoring.NotificationRequestDocument.NotificationRequest getNotificationRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.myidmonitoring.NotificationRequestDocument.NotificationRequest target = null;
            target = (com.idanalytics.products.myidmonitoring.NotificationRequestDocument.NotificationRequest)get_store().find_element_user(NOTIFICATIONREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "NotificationRequest" element
     */
    public void setNotificationRequest(com.idanalytics.products.myidmonitoring.NotificationRequestDocument.NotificationRequest notificationRequest)
    {
        generatedSetterHelperImpl(notificationRequest, NOTIFICATIONREQUEST$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "NotificationRequest" element
     */
    public com.idanalytics.products.myidmonitoring.NotificationRequestDocument.NotificationRequest addNewNotificationRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.myidmonitoring.NotificationRequestDocument.NotificationRequest target = null;
            target = (com.idanalytics.products.myidmonitoring.NotificationRequestDocument.NotificationRequest)get_store().add_element_user(NOTIFICATIONREQUEST$0);
            return target;
        }
    }
    /**
     * An XML NotificationRequest(@http://idanalytics.com/products/myidmonitoring).
     *
     * This is a complex type.
     */
    public static class NotificationRequestImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.myidmonitoring.NotificationRequestDocument.NotificationRequest
    {
        private static final long serialVersionUID = 1L;
        
        public NotificationRequestImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        
    }
}
