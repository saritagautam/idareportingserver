/*
 * XML Type:  string100
 * Namespace: http://idanalytics.com/products/myidmonitoring
 * Java type: com.idanalytics.products.myidmonitoring.String100
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.myidmonitoring.impl;
/**
 * An XML string100(@http://idanalytics.com/products/myidmonitoring).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.myidmonitoring.String100.
 */
public class String100Impl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.myidmonitoring.String100
{
    private static final long serialVersionUID = 1L;
    
    public String100Impl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected String100Impl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
