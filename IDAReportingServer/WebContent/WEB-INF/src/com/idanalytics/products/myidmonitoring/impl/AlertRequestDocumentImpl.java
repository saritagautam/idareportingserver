/*
 * An XML document type.
 * Localname: AlertRequest
 * Namespace: http://idanalytics.com/products/myidmonitoring
 * Java type: com.idanalytics.products.myidmonitoring.AlertRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.myidmonitoring.impl;
/**
 * A document containing one AlertRequest(@http://idanalytics.com/products/myidmonitoring) element.
 *
 * This is a complex type.
 */
public class AlertRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.myidmonitoring.AlertRequestDocument
{
    private static final long serialVersionUID = 1L;
    
    public AlertRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ALERTREQUEST$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/myidmonitoring", "AlertRequest");
    
    
    /**
     * Gets the "AlertRequest" element
     */
    public com.idanalytics.products.myidmonitoring.AlertRequestDocument.AlertRequest getAlertRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.myidmonitoring.AlertRequestDocument.AlertRequest target = null;
            target = (com.idanalytics.products.myidmonitoring.AlertRequestDocument.AlertRequest)get_store().find_element_user(ALERTREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "AlertRequest" element
     */
    public void setAlertRequest(com.idanalytics.products.myidmonitoring.AlertRequestDocument.AlertRequest alertRequest)
    {
        generatedSetterHelperImpl(alertRequest, ALERTREQUEST$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "AlertRequest" element
     */
    public com.idanalytics.products.myidmonitoring.AlertRequestDocument.AlertRequest addNewAlertRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.myidmonitoring.AlertRequestDocument.AlertRequest target = null;
            target = (com.idanalytics.products.myidmonitoring.AlertRequestDocument.AlertRequest)get_store().add_element_user(ALERTREQUEST$0);
            return target;
        }
    }
    /**
     * An XML AlertRequest(@http://idanalytics.com/products/myidmonitoring).
     *
     * This is a complex type.
     */
    public static class AlertRequestImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.myidmonitoring.AlertRequestDocument.AlertRequest
    {
        private static final long serialVersionUID = 1L;
        
        public AlertRequestImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        
    }
}
