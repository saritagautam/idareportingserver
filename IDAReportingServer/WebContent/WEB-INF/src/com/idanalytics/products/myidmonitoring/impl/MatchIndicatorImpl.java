/*
 * XML Type:  MatchIndicator
 * Namespace: http://idanalytics.com/products/myidmonitoring
 * Java type: com.idanalytics.products.myidmonitoring.MatchIndicator
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.myidmonitoring.impl;
/**
 * An XML MatchIndicator(@http://idanalytics.com/products/myidmonitoring).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.myidmonitoring.MatchIndicator.
 */
public class MatchIndicatorImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.idanalytics.products.myidmonitoring.MatchIndicator
{
    private static final long serialVersionUID = 1L;
    
    public MatchIndicatorImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected MatchIndicatorImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
