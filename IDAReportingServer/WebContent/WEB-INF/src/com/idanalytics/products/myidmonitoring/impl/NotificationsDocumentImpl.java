/*
 * An XML document type.
 * Localname: Notifications
 * Namespace: http://idanalytics.com/products/myidmonitoring
 * Java type: com.idanalytics.products.myidmonitoring.NotificationsDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.myidmonitoring.impl;
/**
 * A document containing one Notifications(@http://idanalytics.com/products/myidmonitoring) element.
 *
 * This is a complex type.
 */
public class NotificationsDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.myidmonitoring.NotificationsDocument
{
    private static final long serialVersionUID = 1L;
    
    public NotificationsDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName NOTIFICATIONS$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/myidmonitoring", "Notifications");
    
    
    /**
     * Gets the "Notifications" element
     */
    public com.idanalytics.products.myidmonitoring.NotificationsDocument.Notifications getNotifications()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.myidmonitoring.NotificationsDocument.Notifications target = null;
            target = (com.idanalytics.products.myidmonitoring.NotificationsDocument.Notifications)get_store().find_element_user(NOTIFICATIONS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "Notifications" element
     */
    public void setNotifications(com.idanalytics.products.myidmonitoring.NotificationsDocument.Notifications notifications)
    {
        generatedSetterHelperImpl(notifications, NOTIFICATIONS$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "Notifications" element
     */
    public com.idanalytics.products.myidmonitoring.NotificationsDocument.Notifications addNewNotifications()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.myidmonitoring.NotificationsDocument.Notifications target = null;
            target = (com.idanalytics.products.myidmonitoring.NotificationsDocument.Notifications)get_store().add_element_user(NOTIFICATIONS$0);
            return target;
        }
    }
    /**
     * An XML Notifications(@http://idanalytics.com/products/myidmonitoring).
     *
     * This is a complex type.
     */
    public static class NotificationsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.myidmonitoring.NotificationsDocument.Notifications
    {
        private static final long serialVersionUID = 1L;
        
        public NotificationsImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName SEQUENCE$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/myidmonitoring", "Sequence");
        private static final javax.xml.namespace.QName NOTIFICATION$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/myidmonitoring", "Notification");
        
        
        /**
         * Gets the "Sequence" element
         */
        public java.math.BigInteger getSequence()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SEQUENCE$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getBigIntegerValue();
            }
        }
        
        /**
         * Gets (as xml) the "Sequence" element
         */
        public org.apache.xmlbeans.XmlUnsignedLong xgetSequence()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlUnsignedLong target = null;
                target = (org.apache.xmlbeans.XmlUnsignedLong)get_store().find_element_user(SEQUENCE$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "Sequence" element
         */
        public void setSequence(java.math.BigInteger sequence)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SEQUENCE$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SEQUENCE$0);
                }
                target.setBigIntegerValue(sequence);
            }
        }
        
        /**
         * Sets (as xml) the "Sequence" element
         */
        public void xsetSequence(org.apache.xmlbeans.XmlUnsignedLong sequence)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlUnsignedLong target = null;
                target = (org.apache.xmlbeans.XmlUnsignedLong)get_store().find_element_user(SEQUENCE$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlUnsignedLong)get_store().add_element_user(SEQUENCE$0);
                }
                target.set(sequence);
            }
        }
        
        /**
         * Gets array of all "Notification" elements
         */
        public com.idanalytics.products.myidmonitoring.AlertEvent[] getNotificationArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(NOTIFICATION$2, targetList);
                com.idanalytics.products.myidmonitoring.AlertEvent[] result = new com.idanalytics.products.myidmonitoring.AlertEvent[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "Notification" element
         */
        public com.idanalytics.products.myidmonitoring.AlertEvent getNotificationArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.myidmonitoring.AlertEvent target = null;
                target = (com.idanalytics.products.myidmonitoring.AlertEvent)get_store().find_element_user(NOTIFICATION$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "Notification" element
         */
        public int sizeOfNotificationArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(NOTIFICATION$2);
            }
        }
        
        /**
         * Sets array of all "Notification" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setNotificationArray(com.idanalytics.products.myidmonitoring.AlertEvent[] notificationArray)
        {
            check_orphaned();
            arraySetterHelper(notificationArray, NOTIFICATION$2);
        }
        
        /**
         * Sets ith "Notification" element
         */
        public void setNotificationArray(int i, com.idanalytics.products.myidmonitoring.AlertEvent notification)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.myidmonitoring.AlertEvent target = null;
                target = (com.idanalytics.products.myidmonitoring.AlertEvent)get_store().find_element_user(NOTIFICATION$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(notification);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "Notification" element
         */
        public com.idanalytics.products.myidmonitoring.AlertEvent insertNewNotification(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.myidmonitoring.AlertEvent target = null;
                target = (com.idanalytics.products.myidmonitoring.AlertEvent)get_store().insert_element_user(NOTIFICATION$2, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "Notification" element
         */
        public com.idanalytics.products.myidmonitoring.AlertEvent addNewNotification()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.myidmonitoring.AlertEvent target = null;
                target = (com.idanalytics.products.myidmonitoring.AlertEvent)get_store().add_element_user(NOTIFICATION$2);
                return target;
            }
        }
        
        /**
         * Removes the ith "Notification" element
         */
        public void removeNotification(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(NOTIFICATION$2, i);
            }
        }
    }
}
