/*
 * XML Type:  AlertEvent
 * Namespace: http://idanalytics.com/products/myidmonitoring
 * Java type: com.idanalytics.products.myidmonitoring.AlertEvent
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.myidmonitoring.impl;
/**
 * An XML AlertEvent(@http://idanalytics.com/products/myidmonitoring).
 *
 * This is a complex type.
 */
public class AlertEventImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.myidmonitoring.AlertEvent
{
    private static final long serialVersionUID = 1L;
    
    public AlertEventImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName AFFILIATE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/myidmonitoring", "Affiliate");
    private static final javax.xml.namespace.QName CONSUMERID$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/myidmonitoring", "ConsumerID");
    private static final javax.xml.namespace.QName IDSCORE$4 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/myidmonitoring", "IDScore");
    private static final javax.xml.namespace.QName IDASEQUENCE$6 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/myidmonitoring", "IDASequence");
    private static final javax.xml.namespace.QName IDATIMESTAMP$8 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/myidmonitoring", "IDATimeStamp");
    private static final javax.xml.namespace.QName SCOREBANDINDICATOR$10 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/myidmonitoring", "ScoreBandIndicator");
    private static final javax.xml.namespace.QName SCOREDELTAINDICATOR$12 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/myidmonitoring", "ScoreDeltaIndicator");
    private static final javax.xml.namespace.QName INDICATORS$14 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/myidmonitoring", "Indicators");
    private static final javax.xml.namespace.QName TRIGGEREVENT$16 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/myidmonitoring", "TriggerEvent");
    private static final javax.xml.namespace.QName PREVIOUSEVENTS$18 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/myidmonitoring", "PreviousEvents");
    
    
    /**
     * Gets the "Affiliate" element
     */
    public java.lang.String getAffiliate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(AFFILIATE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Affiliate" element
     */
    public com.idanalytics.products.myidmonitoring.String50 xgetAffiliate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.myidmonitoring.String50 target = null;
            target = (com.idanalytics.products.myidmonitoring.String50)get_store().find_element_user(AFFILIATE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Affiliate" element
     */
    public void setAffiliate(java.lang.String affiliate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(AFFILIATE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(AFFILIATE$0);
            }
            target.setStringValue(affiliate);
        }
    }
    
    /**
     * Sets (as xml) the "Affiliate" element
     */
    public void xsetAffiliate(com.idanalytics.products.myidmonitoring.String50 affiliate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.myidmonitoring.String50 target = null;
            target = (com.idanalytics.products.myidmonitoring.String50)get_store().find_element_user(AFFILIATE$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.myidmonitoring.String50)get_store().add_element_user(AFFILIATE$0);
            }
            target.set(affiliate);
        }
    }
    
    /**
     * Gets the "ConsumerID" element
     */
    public java.lang.String getConsumerID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CONSUMERID$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "ConsumerID" element
     */
    public com.idanalytics.products.myidmonitoring.String50 xgetConsumerID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.myidmonitoring.String50 target = null;
            target = (com.idanalytics.products.myidmonitoring.String50)get_store().find_element_user(CONSUMERID$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "ConsumerID" element
     */
    public void setConsumerID(java.lang.String consumerID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CONSUMERID$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CONSUMERID$2);
            }
            target.setStringValue(consumerID);
        }
    }
    
    /**
     * Sets (as xml) the "ConsumerID" element
     */
    public void xsetConsumerID(com.idanalytics.products.myidmonitoring.String50 consumerID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.myidmonitoring.String50 target = null;
            target = (com.idanalytics.products.myidmonitoring.String50)get_store().find_element_user(CONSUMERID$2, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.myidmonitoring.String50)get_store().add_element_user(CONSUMERID$2);
            }
            target.set(consumerID);
        }
    }
    
    /**
     * Gets the "IDScore" element
     */
    public java.lang.String getIDScore()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDSCORE$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IDScore" element
     */
    public com.idanalytics.products.myidmonitoring.String3 xgetIDScore()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.myidmonitoring.String3 target = null;
            target = (com.idanalytics.products.myidmonitoring.String3)get_store().find_element_user(IDSCORE$4, 0);
            return target;
        }
    }
    
    /**
     * True if has "IDScore" element
     */
    public boolean isSetIDScore()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(IDSCORE$4) != 0;
        }
    }
    
    /**
     * Sets the "IDScore" element
     */
    public void setIDScore(java.lang.String idScore)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDSCORE$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDSCORE$4);
            }
            target.setStringValue(idScore);
        }
    }
    
    /**
     * Sets (as xml) the "IDScore" element
     */
    public void xsetIDScore(com.idanalytics.products.myidmonitoring.String3 idScore)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.myidmonitoring.String3 target = null;
            target = (com.idanalytics.products.myidmonitoring.String3)get_store().find_element_user(IDSCORE$4, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.myidmonitoring.String3)get_store().add_element_user(IDSCORE$4);
            }
            target.set(idScore);
        }
    }
    
    /**
     * Unsets the "IDScore" element
     */
    public void unsetIDScore()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(IDSCORE$4, 0);
        }
    }
    
    /**
     * Gets the "IDASequence" element
     */
    public java.lang.String getIDASequence()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASEQUENCE$6, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IDASequence" element
     */
    public com.idanalytics.products.myidmonitoring.String30 xgetIDASequence()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.myidmonitoring.String30 target = null;
            target = (com.idanalytics.products.myidmonitoring.String30)get_store().find_element_user(IDASEQUENCE$6, 0);
            return target;
        }
    }
    
    /**
     * True if has "IDASequence" element
     */
    public boolean isSetIDASequence()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(IDASEQUENCE$6) != 0;
        }
    }
    
    /**
     * Sets the "IDASequence" element
     */
    public void setIDASequence(java.lang.String idaSequence)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASEQUENCE$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDASEQUENCE$6);
            }
            target.setStringValue(idaSequence);
        }
    }
    
    /**
     * Sets (as xml) the "IDASequence" element
     */
    public void xsetIDASequence(com.idanalytics.products.myidmonitoring.String30 idaSequence)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.myidmonitoring.String30 target = null;
            target = (com.idanalytics.products.myidmonitoring.String30)get_store().find_element_user(IDASEQUENCE$6, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.myidmonitoring.String30)get_store().add_element_user(IDASEQUENCE$6);
            }
            target.set(idaSequence);
        }
    }
    
    /**
     * Unsets the "IDASequence" element
     */
    public void unsetIDASequence()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(IDASEQUENCE$6, 0);
        }
    }
    
    /**
     * Gets the "IDATimeStamp" element
     */
    public java.util.Calendar getIDATimeStamp()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDATIMESTAMP$8, 0);
            if (target == null)
            {
                return null;
            }
            return target.getCalendarValue();
        }
    }
    
    /**
     * Gets (as xml) the "IDATimeStamp" element
     */
    public org.apache.xmlbeans.XmlDateTime xgetIDATimeStamp()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlDateTime target = null;
            target = (org.apache.xmlbeans.XmlDateTime)get_store().find_element_user(IDATIMESTAMP$8, 0);
            return target;
        }
    }
    
    /**
     * True if has "IDATimeStamp" element
     */
    public boolean isSetIDATimeStamp()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(IDATIMESTAMP$8) != 0;
        }
    }
    
    /**
     * Sets the "IDATimeStamp" element
     */
    public void setIDATimeStamp(java.util.Calendar idaTimeStamp)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDATIMESTAMP$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDATIMESTAMP$8);
            }
            target.setCalendarValue(idaTimeStamp);
        }
    }
    
    /**
     * Sets (as xml) the "IDATimeStamp" element
     */
    public void xsetIDATimeStamp(org.apache.xmlbeans.XmlDateTime idaTimeStamp)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlDateTime target = null;
            target = (org.apache.xmlbeans.XmlDateTime)get_store().find_element_user(IDATIMESTAMP$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlDateTime)get_store().add_element_user(IDATIMESTAMP$8);
            }
            target.set(idaTimeStamp);
        }
    }
    
    /**
     * Unsets the "IDATimeStamp" element
     */
    public void unsetIDATimeStamp()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(IDATIMESTAMP$8, 0);
        }
    }
    
    /**
     * Gets the "ScoreBandIndicator" element
     */
    public com.idanalytics.products.myidmonitoring.ScoreBandIndicatorType.Enum getScoreBandIndicator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SCOREBANDINDICATOR$10, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.myidmonitoring.ScoreBandIndicatorType.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "ScoreBandIndicator" element
     */
    public com.idanalytics.products.myidmonitoring.ScoreBandIndicatorType xgetScoreBandIndicator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.myidmonitoring.ScoreBandIndicatorType target = null;
            target = (com.idanalytics.products.myidmonitoring.ScoreBandIndicatorType)get_store().find_element_user(SCOREBANDINDICATOR$10, 0);
            return target;
        }
    }
    
    /**
     * True if has "ScoreBandIndicator" element
     */
    public boolean isSetScoreBandIndicator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SCOREBANDINDICATOR$10) != 0;
        }
    }
    
    /**
     * Sets the "ScoreBandIndicator" element
     */
    public void setScoreBandIndicator(com.idanalytics.products.myidmonitoring.ScoreBandIndicatorType.Enum scoreBandIndicator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SCOREBANDINDICATOR$10, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SCOREBANDINDICATOR$10);
            }
            target.setEnumValue(scoreBandIndicator);
        }
    }
    
    /**
     * Sets (as xml) the "ScoreBandIndicator" element
     */
    public void xsetScoreBandIndicator(com.idanalytics.products.myidmonitoring.ScoreBandIndicatorType scoreBandIndicator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.myidmonitoring.ScoreBandIndicatorType target = null;
            target = (com.idanalytics.products.myidmonitoring.ScoreBandIndicatorType)get_store().find_element_user(SCOREBANDINDICATOR$10, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.myidmonitoring.ScoreBandIndicatorType)get_store().add_element_user(SCOREBANDINDICATOR$10);
            }
            target.set(scoreBandIndicator);
        }
    }
    
    /**
     * Unsets the "ScoreBandIndicator" element
     */
    public void unsetScoreBandIndicator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SCOREBANDINDICATOR$10, 0);
        }
    }
    
    /**
     * Gets the "ScoreDeltaIndicator" element
     */
    public com.idanalytics.products.myidmonitoring.ScoreDeltaIndicatorType.Enum getScoreDeltaIndicator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SCOREDELTAINDICATOR$12, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.myidmonitoring.ScoreDeltaIndicatorType.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "ScoreDeltaIndicator" element
     */
    public com.idanalytics.products.myidmonitoring.ScoreDeltaIndicatorType xgetScoreDeltaIndicator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.myidmonitoring.ScoreDeltaIndicatorType target = null;
            target = (com.idanalytics.products.myidmonitoring.ScoreDeltaIndicatorType)get_store().find_element_user(SCOREDELTAINDICATOR$12, 0);
            return target;
        }
    }
    
    /**
     * True if has "ScoreDeltaIndicator" element
     */
    public boolean isSetScoreDeltaIndicator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SCOREDELTAINDICATOR$12) != 0;
        }
    }
    
    /**
     * Sets the "ScoreDeltaIndicator" element
     */
    public void setScoreDeltaIndicator(com.idanalytics.products.myidmonitoring.ScoreDeltaIndicatorType.Enum scoreDeltaIndicator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SCOREDELTAINDICATOR$12, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SCOREDELTAINDICATOR$12);
            }
            target.setEnumValue(scoreDeltaIndicator);
        }
    }
    
    /**
     * Sets (as xml) the "ScoreDeltaIndicator" element
     */
    public void xsetScoreDeltaIndicator(com.idanalytics.products.myidmonitoring.ScoreDeltaIndicatorType scoreDeltaIndicator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.myidmonitoring.ScoreDeltaIndicatorType target = null;
            target = (com.idanalytics.products.myidmonitoring.ScoreDeltaIndicatorType)get_store().find_element_user(SCOREDELTAINDICATOR$12, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.myidmonitoring.ScoreDeltaIndicatorType)get_store().add_element_user(SCOREDELTAINDICATOR$12);
            }
            target.set(scoreDeltaIndicator);
        }
    }
    
    /**
     * Unsets the "ScoreDeltaIndicator" element
     */
    public void unsetScoreDeltaIndicator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SCOREDELTAINDICATOR$12, 0);
        }
    }
    
    /**
     * Gets the "Indicators" element
     */
    public com.idanalytics.products.myidmonitoring.IndicatorsDocument.Indicators getIndicators()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.myidmonitoring.IndicatorsDocument.Indicators target = null;
            target = (com.idanalytics.products.myidmonitoring.IndicatorsDocument.Indicators)get_store().find_element_user(INDICATORS$14, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "Indicators" element
     */
    public boolean isSetIndicators()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(INDICATORS$14) != 0;
        }
    }
    
    /**
     * Sets the "Indicators" element
     */
    public void setIndicators(com.idanalytics.products.myidmonitoring.IndicatorsDocument.Indicators indicators)
    {
        generatedSetterHelperImpl(indicators, INDICATORS$14, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "Indicators" element
     */
    public com.idanalytics.products.myidmonitoring.IndicatorsDocument.Indicators addNewIndicators()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.myidmonitoring.IndicatorsDocument.Indicators target = null;
            target = (com.idanalytics.products.myidmonitoring.IndicatorsDocument.Indicators)get_store().add_element_user(INDICATORS$14);
            return target;
        }
    }
    
    /**
     * Unsets the "Indicators" element
     */
    public void unsetIndicators()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(INDICATORS$14, 0);
        }
    }
    
    /**
     * Gets the "TriggerEvent" element
     */
    public com.idanalytics.products.myidmonitoring.IdentityEvent getTriggerEvent()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.myidmonitoring.IdentityEvent target = null;
            target = (com.idanalytics.products.myidmonitoring.IdentityEvent)get_store().find_element_user(TRIGGEREVENT$16, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "TriggerEvent" element
     */
    public boolean isSetTriggerEvent()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(TRIGGEREVENT$16) != 0;
        }
    }
    
    /**
     * Sets the "TriggerEvent" element
     */
    public void setTriggerEvent(com.idanalytics.products.myidmonitoring.IdentityEvent triggerEvent)
    {
        generatedSetterHelperImpl(triggerEvent, TRIGGEREVENT$16, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "TriggerEvent" element
     */
    public com.idanalytics.products.myidmonitoring.IdentityEvent addNewTriggerEvent()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.myidmonitoring.IdentityEvent target = null;
            target = (com.idanalytics.products.myidmonitoring.IdentityEvent)get_store().add_element_user(TRIGGEREVENT$16);
            return target;
        }
    }
    
    /**
     * Unsets the "TriggerEvent" element
     */
    public void unsetTriggerEvent()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(TRIGGEREVENT$16, 0);
        }
    }
    
    /**
     * Gets the "PreviousEvents" element
     */
    public com.idanalytics.products.myidmonitoring.PreviousEventsDocument.PreviousEvents getPreviousEvents()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.myidmonitoring.PreviousEventsDocument.PreviousEvents target = null;
            target = (com.idanalytics.products.myidmonitoring.PreviousEventsDocument.PreviousEvents)get_store().find_element_user(PREVIOUSEVENTS$18, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "PreviousEvents" element
     */
    public boolean isSetPreviousEvents()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREVIOUSEVENTS$18) != 0;
        }
    }
    
    /**
     * Sets the "PreviousEvents" element
     */
    public void setPreviousEvents(com.idanalytics.products.myidmonitoring.PreviousEventsDocument.PreviousEvents previousEvents)
    {
        generatedSetterHelperImpl(previousEvents, PREVIOUSEVENTS$18, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "PreviousEvents" element
     */
    public com.idanalytics.products.myidmonitoring.PreviousEventsDocument.PreviousEvents addNewPreviousEvents()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.myidmonitoring.PreviousEventsDocument.PreviousEvents target = null;
            target = (com.idanalytics.products.myidmonitoring.PreviousEventsDocument.PreviousEvents)get_store().add_element_user(PREVIOUSEVENTS$18);
            return target;
        }
    }
    
    /**
     * Unsets the "PreviousEvents" element
     */
    public void unsetPreviousEvents()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREVIOUSEVENTS$18, 0);
        }
    }
}
