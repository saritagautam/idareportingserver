/*
 * An XML document type.
 * Localname: Alerts
 * Namespace: http://idanalytics.com/products/myidmonitoring
 * Java type: com.idanalytics.products.myidmonitoring.AlertsDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.myidmonitoring;


/**
 * A document containing one Alerts(@http://idanalytics.com/products/myidmonitoring) element.
 *
 * This is a complex type.
 */
public interface AlertsDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(AlertsDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("alerts6da7doctype");
    
    /**
     * Gets the "Alerts" element
     */
    com.idanalytics.products.myidmonitoring.AlertsDocument.Alerts getAlerts();
    
    /**
     * Sets the "Alerts" element
     */
    void setAlerts(com.idanalytics.products.myidmonitoring.AlertsDocument.Alerts alerts);
    
    /**
     * Appends and returns a new empty "Alerts" element
     */
    com.idanalytics.products.myidmonitoring.AlertsDocument.Alerts addNewAlerts();
    
    /**
     * An XML Alerts(@http://idanalytics.com/products/myidmonitoring).
     *
     * This is a complex type.
     */
    public interface Alerts extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Alerts.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("alerts1dfaelemtype");
        
        /**
         * Gets the "Sequence" element
         */
        java.math.BigInteger getSequence();
        
        /**
         * Gets (as xml) the "Sequence" element
         */
        org.apache.xmlbeans.XmlUnsignedLong xgetSequence();
        
        /**
         * Sets the "Sequence" element
         */
        void setSequence(java.math.BigInteger sequence);
        
        /**
         * Sets (as xml) the "Sequence" element
         */
        void xsetSequence(org.apache.xmlbeans.XmlUnsignedLong sequence);
        
        /**
         * Gets array of all "Alert" elements
         */
        com.idanalytics.products.myidmonitoring.AlertEvent[] getAlertArray();
        
        /**
         * Gets ith "Alert" element
         */
        com.idanalytics.products.myidmonitoring.AlertEvent getAlertArray(int i);
        
        /**
         * Returns number of "Alert" element
         */
        int sizeOfAlertArray();
        
        /**
         * Sets array of all "Alert" element
         */
        void setAlertArray(com.idanalytics.products.myidmonitoring.AlertEvent[] alertArray);
        
        /**
         * Sets ith "Alert" element
         */
        void setAlertArray(int i, com.idanalytics.products.myidmonitoring.AlertEvent alert);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "Alert" element
         */
        com.idanalytics.products.myidmonitoring.AlertEvent insertNewAlert(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "Alert" element
         */
        com.idanalytics.products.myidmonitoring.AlertEvent addNewAlert();
        
        /**
         * Removes the ith "Alert" element
         */
        void removeAlert(int i);
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.myidmonitoring.AlertsDocument.Alerts newInstance() {
              return (com.idanalytics.products.myidmonitoring.AlertsDocument.Alerts) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.myidmonitoring.AlertsDocument.Alerts newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.myidmonitoring.AlertsDocument.Alerts) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.myidmonitoring.AlertsDocument newInstance() {
          return (com.idanalytics.products.myidmonitoring.AlertsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.myidmonitoring.AlertsDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.myidmonitoring.AlertsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.myidmonitoring.AlertsDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.myidmonitoring.AlertsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.myidmonitoring.AlertsDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.myidmonitoring.AlertsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.myidmonitoring.AlertsDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.myidmonitoring.AlertsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.myidmonitoring.AlertsDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.myidmonitoring.AlertsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.myidmonitoring.AlertsDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.myidmonitoring.AlertsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.myidmonitoring.AlertsDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.myidmonitoring.AlertsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.myidmonitoring.AlertsDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.myidmonitoring.AlertsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.myidmonitoring.AlertsDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.myidmonitoring.AlertsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.myidmonitoring.AlertsDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.myidmonitoring.AlertsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.myidmonitoring.AlertsDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.myidmonitoring.AlertsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.myidmonitoring.AlertsDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.myidmonitoring.AlertsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.myidmonitoring.AlertsDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.myidmonitoring.AlertsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.myidmonitoring.AlertsDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.myidmonitoring.AlertsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.myidmonitoring.AlertsDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.myidmonitoring.AlertsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.myidmonitoring.AlertsDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.myidmonitoring.AlertsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.myidmonitoring.AlertsDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.myidmonitoring.AlertsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
