/*
 * XML Type:  ScoreBandIndicatorType
 * Namespace: http://idanalytics.com/products/myidmonitoring
 * Java type: com.idanalytics.products.myidmonitoring.ScoreBandIndicatorType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.myidmonitoring.impl;
/**
 * An XML ScoreBandIndicatorType(@http://idanalytics.com/products/myidmonitoring).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.myidmonitoring.ScoreBandIndicatorType.
 */
public class ScoreBandIndicatorTypeImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.idanalytics.products.myidmonitoring.ScoreBandIndicatorType
{
    private static final long serialVersionUID = 1L;
    
    public ScoreBandIndicatorTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected ScoreBandIndicatorTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
