/*
 * An XML document type.
 * Localname: Indicator
 * Namespace: http://idanalytics.com/products/myidmonitoring
 * Java type: com.idanalytics.products.myidmonitoring.IndicatorDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.myidmonitoring.impl;
/**
 * A document containing one Indicator(@http://idanalytics.com/products/myidmonitoring) element.
 *
 * This is a complex type.
 */
public class IndicatorDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.myidmonitoring.IndicatorDocument
{
    private static final long serialVersionUID = 1L;
    
    public IndicatorDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName INDICATOR$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/myidmonitoring", "Indicator");
    
    
    /**
     * Gets the "Indicator" element
     */
    public com.idanalytics.products.myidmonitoring.IndicatorDocument.Indicator getIndicator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.myidmonitoring.IndicatorDocument.Indicator target = null;
            target = (com.idanalytics.products.myidmonitoring.IndicatorDocument.Indicator)get_store().find_element_user(INDICATOR$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "Indicator" element
     */
    public void setIndicator(com.idanalytics.products.myidmonitoring.IndicatorDocument.Indicator indicator)
    {
        generatedSetterHelperImpl(indicator, INDICATOR$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "Indicator" element
     */
    public com.idanalytics.products.myidmonitoring.IndicatorDocument.Indicator addNewIndicator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.myidmonitoring.IndicatorDocument.Indicator target = null;
            target = (com.idanalytics.products.myidmonitoring.IndicatorDocument.Indicator)get_store().add_element_user(INDICATOR$0);
            return target;
        }
    }
    /**
     * An XML Indicator(@http://idanalytics.com/products/myidmonitoring).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.myidmonitoring.IndicatorDocument$Indicator.
     */
    public static class IndicatorImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.myidmonitoring.IndicatorDocument.Indicator
    {
        private static final long serialVersionUID = 1L;
        
        public IndicatorImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, true);
        }
        
        protected IndicatorImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
        
        private static final javax.xml.namespace.QName NAME$0 = 
            new javax.xml.namespace.QName("", "name");
        
        
        /**
         * Gets the "name" attribute
         */
        public java.lang.String getName()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(NAME$0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "name" attribute
         */
        public org.apache.xmlbeans.XmlString xgetName()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_attribute_user(NAME$0);
                return target;
            }
        }
        
        /**
         * Sets the "name" attribute
         */
        public void setName(java.lang.String name)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(NAME$0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(NAME$0);
                }
                target.setStringValue(name);
            }
        }
        
        /**
         * Sets (as xml) the "name" attribute
         */
        public void xsetName(org.apache.xmlbeans.XmlString name)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_attribute_user(NAME$0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlString)get_store().add_attribute_user(NAME$0);
                }
                target.set(name);
            }
        }
    }
}
