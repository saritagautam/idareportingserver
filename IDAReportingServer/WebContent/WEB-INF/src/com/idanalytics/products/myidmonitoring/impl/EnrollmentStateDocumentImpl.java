/*
 * An XML document type.
 * Localname: EnrollmentState
 * Namespace: http://idanalytics.com/products/myidmonitoring
 * Java type: com.idanalytics.products.myidmonitoring.EnrollmentStateDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.myidmonitoring.impl;
/**
 * A document containing one EnrollmentState(@http://idanalytics.com/products/myidmonitoring) element.
 *
 * This is a complex type.
 */
public class EnrollmentStateDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.myidmonitoring.EnrollmentStateDocument
{
    private static final long serialVersionUID = 1L;
    
    public EnrollmentStateDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ENROLLMENTSTATE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/myidmonitoring", "EnrollmentState");
    
    
    /**
     * Gets the "EnrollmentState" element
     */
    public com.idanalytics.products.myidmonitoring.EnrollmentStateDocument.EnrollmentState getEnrollmentState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.myidmonitoring.EnrollmentStateDocument.EnrollmentState target = null;
            target = (com.idanalytics.products.myidmonitoring.EnrollmentStateDocument.EnrollmentState)get_store().find_element_user(ENROLLMENTSTATE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "EnrollmentState" element
     */
    public void setEnrollmentState(com.idanalytics.products.myidmonitoring.EnrollmentStateDocument.EnrollmentState enrollmentState)
    {
        generatedSetterHelperImpl(enrollmentState, ENROLLMENTSTATE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "EnrollmentState" element
     */
    public com.idanalytics.products.myidmonitoring.EnrollmentStateDocument.EnrollmentState addNewEnrollmentState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.myidmonitoring.EnrollmentStateDocument.EnrollmentState target = null;
            target = (com.idanalytics.products.myidmonitoring.EnrollmentStateDocument.EnrollmentState)get_store().add_element_user(ENROLLMENTSTATE$0);
            return target;
        }
    }
    /**
     * An XML EnrollmentState(@http://idanalytics.com/products/myidmonitoring).
     *
     * This is a complex type.
     */
    public static class EnrollmentStateImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.myidmonitoring.EnrollmentStateDocument.EnrollmentState
    {
        private static final long serialVersionUID = 1L;
        
        public EnrollmentStateImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName AFFILIATE$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/myidmonitoring", "Affiliate");
        private static final javax.xml.namespace.QName CONSUMERID$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/myidmonitoring", "ConsumerID");
        private static final javax.xml.namespace.QName PREVIOUSLYENROLLED$4 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/myidmonitoring", "PreviouslyEnrolled");
        private static final javax.xml.namespace.QName ENROLLMENTDATE$6 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/myidmonitoring", "EnrollmentDate");
        
        
        /**
         * Gets the "Affiliate" element
         */
        public java.lang.String getAffiliate()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(AFFILIATE$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "Affiliate" element
         */
        public com.idanalytics.products.myidmonitoring.String50 xgetAffiliate()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.myidmonitoring.String50 target = null;
                target = (com.idanalytics.products.myidmonitoring.String50)get_store().find_element_user(AFFILIATE$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "Affiliate" element
         */
        public void setAffiliate(java.lang.String affiliate)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(AFFILIATE$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(AFFILIATE$0);
                }
                target.setStringValue(affiliate);
            }
        }
        
        /**
         * Sets (as xml) the "Affiliate" element
         */
        public void xsetAffiliate(com.idanalytics.products.myidmonitoring.String50 affiliate)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.myidmonitoring.String50 target = null;
                target = (com.idanalytics.products.myidmonitoring.String50)get_store().find_element_user(AFFILIATE$0, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.myidmonitoring.String50)get_store().add_element_user(AFFILIATE$0);
                }
                target.set(affiliate);
            }
        }
        
        /**
         * Gets the "ConsumerID" element
         */
        public java.lang.String getConsumerID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CONSUMERID$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "ConsumerID" element
         */
        public com.idanalytics.products.myidmonitoring.String50 xgetConsumerID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.myidmonitoring.String50 target = null;
                target = (com.idanalytics.products.myidmonitoring.String50)get_store().find_element_user(CONSUMERID$2, 0);
                return target;
            }
        }
        
        /**
         * Sets the "ConsumerID" element
         */
        public void setConsumerID(java.lang.String consumerID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CONSUMERID$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CONSUMERID$2);
                }
                target.setStringValue(consumerID);
            }
        }
        
        /**
         * Sets (as xml) the "ConsumerID" element
         */
        public void xsetConsumerID(com.idanalytics.products.myidmonitoring.String50 consumerID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.myidmonitoring.String50 target = null;
                target = (com.idanalytics.products.myidmonitoring.String50)get_store().find_element_user(CONSUMERID$2, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.myidmonitoring.String50)get_store().add_element_user(CONSUMERID$2);
                }
                target.set(consumerID);
            }
        }
        
        /**
         * Gets the "PreviouslyEnrolled" element
         */
        public boolean getPreviouslyEnrolled()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVIOUSLYENROLLED$4, 0);
                if (target == null)
                {
                    return false;
                }
                return target.getBooleanValue();
            }
        }
        
        /**
         * Gets (as xml) the "PreviouslyEnrolled" element
         */
        public org.apache.xmlbeans.XmlBoolean xgetPreviouslyEnrolled()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlBoolean target = null;
                target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(PREVIOUSLYENROLLED$4, 0);
                return target;
            }
        }
        
        /**
         * Sets the "PreviouslyEnrolled" element
         */
        public void setPreviouslyEnrolled(boolean previouslyEnrolled)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVIOUSLYENROLLED$4, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREVIOUSLYENROLLED$4);
                }
                target.setBooleanValue(previouslyEnrolled);
            }
        }
        
        /**
         * Sets (as xml) the "PreviouslyEnrolled" element
         */
        public void xsetPreviouslyEnrolled(org.apache.xmlbeans.XmlBoolean previouslyEnrolled)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlBoolean target = null;
                target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(PREVIOUSLYENROLLED$4, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlBoolean)get_store().add_element_user(PREVIOUSLYENROLLED$4);
                }
                target.set(previouslyEnrolled);
            }
        }
        
        /**
         * Gets the "EnrollmentDate" element
         */
        public java.util.Calendar getEnrollmentDate()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ENROLLMENTDATE$6, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getCalendarValue();
            }
        }
        
        /**
         * Gets (as xml) the "EnrollmentDate" element
         */
        public org.apache.xmlbeans.XmlDateTime xgetEnrollmentDate()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlDateTime target = null;
                target = (org.apache.xmlbeans.XmlDateTime)get_store().find_element_user(ENROLLMENTDATE$6, 0);
                return target;
            }
        }
        
        /**
         * True if has "EnrollmentDate" element
         */
        public boolean isSetEnrollmentDate()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(ENROLLMENTDATE$6) != 0;
            }
        }
        
        /**
         * Sets the "EnrollmentDate" element
         */
        public void setEnrollmentDate(java.util.Calendar enrollmentDate)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ENROLLMENTDATE$6, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ENROLLMENTDATE$6);
                }
                target.setCalendarValue(enrollmentDate);
            }
        }
        
        /**
         * Sets (as xml) the "EnrollmentDate" element
         */
        public void xsetEnrollmentDate(org.apache.xmlbeans.XmlDateTime enrollmentDate)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlDateTime target = null;
                target = (org.apache.xmlbeans.XmlDateTime)get_store().find_element_user(ENROLLMENTDATE$6, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlDateTime)get_store().add_element_user(ENROLLMENTDATE$6);
                }
                target.set(enrollmentDate);
            }
        }
        
        /**
         * Unsets the "EnrollmentDate" element
         */
        public void unsetEnrollmentDate()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(ENROLLMENTDATE$6, 0);
            }
        }
    }
}
