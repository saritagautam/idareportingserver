/*
 * XML Type:  string30
 * Namespace: http://idanalytics.com/products/myidmonitoring
 * Java type: com.idanalytics.products.myidmonitoring.String30
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.myidmonitoring.impl;
/**
 * An XML string30(@http://idanalytics.com/products/myidmonitoring).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.myidmonitoring.String30.
 */
public class String30Impl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.myidmonitoring.String30
{
    private static final long serialVersionUID = 1L;
    
    public String30Impl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected String30Impl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
