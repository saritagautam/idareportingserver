/*
 * XML Type:  string3
 * Namespace: http://idanalytics.com/products/myidmonitoring
 * Java type: com.idanalytics.products.myidmonitoring.String3
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.myidmonitoring.impl;
/**
 * An XML string3(@http://idanalytics.com/products/myidmonitoring).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.myidmonitoring.String3.
 */
public class String3Impl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.myidmonitoring.String3
{
    private static final long serialVersionUID = 1L;
    
    public String3Impl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected String3Impl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
