/*
 * An XML document type.
 * Localname: Alerts
 * Namespace: http://idanalytics.com/products/myidmonitoring
 * Java type: com.idanalytics.products.myidmonitoring.AlertsDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.myidmonitoring.impl;
/**
 * A document containing one Alerts(@http://idanalytics.com/products/myidmonitoring) element.
 *
 * This is a complex type.
 */
public class AlertsDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.myidmonitoring.AlertsDocument
{
    private static final long serialVersionUID = 1L;
    
    public AlertsDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ALERTS$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/myidmonitoring", "Alerts");
    
    
    /**
     * Gets the "Alerts" element
     */
    public com.idanalytics.products.myidmonitoring.AlertsDocument.Alerts getAlerts()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.myidmonitoring.AlertsDocument.Alerts target = null;
            target = (com.idanalytics.products.myidmonitoring.AlertsDocument.Alerts)get_store().find_element_user(ALERTS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "Alerts" element
     */
    public void setAlerts(com.idanalytics.products.myidmonitoring.AlertsDocument.Alerts alerts)
    {
        generatedSetterHelperImpl(alerts, ALERTS$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "Alerts" element
     */
    public com.idanalytics.products.myidmonitoring.AlertsDocument.Alerts addNewAlerts()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.myidmonitoring.AlertsDocument.Alerts target = null;
            target = (com.idanalytics.products.myidmonitoring.AlertsDocument.Alerts)get_store().add_element_user(ALERTS$0);
            return target;
        }
    }
    /**
     * An XML Alerts(@http://idanalytics.com/products/myidmonitoring).
     *
     * This is a complex type.
     */
    public static class AlertsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.myidmonitoring.AlertsDocument.Alerts
    {
        private static final long serialVersionUID = 1L;
        
        public AlertsImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName SEQUENCE$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/myidmonitoring", "Sequence");
        private static final javax.xml.namespace.QName ALERT$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/myidmonitoring", "Alert");
        
        
        /**
         * Gets the "Sequence" element
         */
        public java.math.BigInteger getSequence()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SEQUENCE$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getBigIntegerValue();
            }
        }
        
        /**
         * Gets (as xml) the "Sequence" element
         */
        public org.apache.xmlbeans.XmlUnsignedLong xgetSequence()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlUnsignedLong target = null;
                target = (org.apache.xmlbeans.XmlUnsignedLong)get_store().find_element_user(SEQUENCE$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "Sequence" element
         */
        public void setSequence(java.math.BigInteger sequence)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SEQUENCE$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SEQUENCE$0);
                }
                target.setBigIntegerValue(sequence);
            }
        }
        
        /**
         * Sets (as xml) the "Sequence" element
         */
        public void xsetSequence(org.apache.xmlbeans.XmlUnsignedLong sequence)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlUnsignedLong target = null;
                target = (org.apache.xmlbeans.XmlUnsignedLong)get_store().find_element_user(SEQUENCE$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlUnsignedLong)get_store().add_element_user(SEQUENCE$0);
                }
                target.set(sequence);
            }
        }
        
        /**
         * Gets array of all "Alert" elements
         */
        public com.idanalytics.products.myidmonitoring.AlertEvent[] getAlertArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(ALERT$2, targetList);
                com.idanalytics.products.myidmonitoring.AlertEvent[] result = new com.idanalytics.products.myidmonitoring.AlertEvent[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "Alert" element
         */
        public com.idanalytics.products.myidmonitoring.AlertEvent getAlertArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.myidmonitoring.AlertEvent target = null;
                target = (com.idanalytics.products.myidmonitoring.AlertEvent)get_store().find_element_user(ALERT$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "Alert" element
         */
        public int sizeOfAlertArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(ALERT$2);
            }
        }
        
        /**
         * Sets array of all "Alert" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setAlertArray(com.idanalytics.products.myidmonitoring.AlertEvent[] alertArray)
        {
            check_orphaned();
            arraySetterHelper(alertArray, ALERT$2);
        }
        
        /**
         * Sets ith "Alert" element
         */
        public void setAlertArray(int i, com.idanalytics.products.myidmonitoring.AlertEvent alert)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.myidmonitoring.AlertEvent target = null;
                target = (com.idanalytics.products.myidmonitoring.AlertEvent)get_store().find_element_user(ALERT$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(alert);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "Alert" element
         */
        public com.idanalytics.products.myidmonitoring.AlertEvent insertNewAlert(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.myidmonitoring.AlertEvent target = null;
                target = (com.idanalytics.products.myidmonitoring.AlertEvent)get_store().insert_element_user(ALERT$2, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "Alert" element
         */
        public com.idanalytics.products.myidmonitoring.AlertEvent addNewAlert()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.myidmonitoring.AlertEvent target = null;
                target = (com.idanalytics.products.myidmonitoring.AlertEvent)get_store().add_element_user(ALERT$2);
                return target;
            }
        }
        
        /**
         * Removes the ith "Alert" element
         */
        public void removeAlert(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(ALERT$2, i);
            }
        }
    }
}
