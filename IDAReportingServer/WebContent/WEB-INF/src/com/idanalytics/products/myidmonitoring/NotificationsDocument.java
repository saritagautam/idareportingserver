/*
 * An XML document type.
 * Localname: Notifications
 * Namespace: http://idanalytics.com/products/myidmonitoring
 * Java type: com.idanalytics.products.myidmonitoring.NotificationsDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.myidmonitoring;


/**
 * A document containing one Notifications(@http://idanalytics.com/products/myidmonitoring) element.
 *
 * This is a complex type.
 */
public interface NotificationsDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(NotificationsDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("notifications4b48doctype");
    
    /**
     * Gets the "Notifications" element
     */
    com.idanalytics.products.myidmonitoring.NotificationsDocument.Notifications getNotifications();
    
    /**
     * Sets the "Notifications" element
     */
    void setNotifications(com.idanalytics.products.myidmonitoring.NotificationsDocument.Notifications notifications);
    
    /**
     * Appends and returns a new empty "Notifications" element
     */
    com.idanalytics.products.myidmonitoring.NotificationsDocument.Notifications addNewNotifications();
    
    /**
     * An XML Notifications(@http://idanalytics.com/products/myidmonitoring).
     *
     * This is a complex type.
     */
    public interface Notifications extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Notifications.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("notifications4194elemtype");
        
        /**
         * Gets the "Sequence" element
         */
        java.math.BigInteger getSequence();
        
        /**
         * Gets (as xml) the "Sequence" element
         */
        org.apache.xmlbeans.XmlUnsignedLong xgetSequence();
        
        /**
         * Sets the "Sequence" element
         */
        void setSequence(java.math.BigInteger sequence);
        
        /**
         * Sets (as xml) the "Sequence" element
         */
        void xsetSequence(org.apache.xmlbeans.XmlUnsignedLong sequence);
        
        /**
         * Gets array of all "Notification" elements
         */
        com.idanalytics.products.myidmonitoring.AlertEvent[] getNotificationArray();
        
        /**
         * Gets ith "Notification" element
         */
        com.idanalytics.products.myidmonitoring.AlertEvent getNotificationArray(int i);
        
        /**
         * Returns number of "Notification" element
         */
        int sizeOfNotificationArray();
        
        /**
         * Sets array of all "Notification" element
         */
        void setNotificationArray(com.idanalytics.products.myidmonitoring.AlertEvent[] notificationArray);
        
        /**
         * Sets ith "Notification" element
         */
        void setNotificationArray(int i, com.idanalytics.products.myidmonitoring.AlertEvent notification);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "Notification" element
         */
        com.idanalytics.products.myidmonitoring.AlertEvent insertNewNotification(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "Notification" element
         */
        com.idanalytics.products.myidmonitoring.AlertEvent addNewNotification();
        
        /**
         * Removes the ith "Notification" element
         */
        void removeNotification(int i);
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.myidmonitoring.NotificationsDocument.Notifications newInstance() {
              return (com.idanalytics.products.myidmonitoring.NotificationsDocument.Notifications) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.myidmonitoring.NotificationsDocument.Notifications newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.myidmonitoring.NotificationsDocument.Notifications) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.myidmonitoring.NotificationsDocument newInstance() {
          return (com.idanalytics.products.myidmonitoring.NotificationsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.myidmonitoring.NotificationsDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.myidmonitoring.NotificationsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.myidmonitoring.NotificationsDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.myidmonitoring.NotificationsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.myidmonitoring.NotificationsDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.myidmonitoring.NotificationsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.myidmonitoring.NotificationsDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.myidmonitoring.NotificationsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.myidmonitoring.NotificationsDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.myidmonitoring.NotificationsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.myidmonitoring.NotificationsDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.myidmonitoring.NotificationsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.myidmonitoring.NotificationsDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.myidmonitoring.NotificationsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.myidmonitoring.NotificationsDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.myidmonitoring.NotificationsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.myidmonitoring.NotificationsDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.myidmonitoring.NotificationsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.myidmonitoring.NotificationsDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.myidmonitoring.NotificationsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.myidmonitoring.NotificationsDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.myidmonitoring.NotificationsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.myidmonitoring.NotificationsDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.myidmonitoring.NotificationsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.myidmonitoring.NotificationsDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.myidmonitoring.NotificationsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.myidmonitoring.NotificationsDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.myidmonitoring.NotificationsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.myidmonitoring.NotificationsDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.myidmonitoring.NotificationsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.myidmonitoring.NotificationsDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.myidmonitoring.NotificationsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.myidmonitoring.NotificationsDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.myidmonitoring.NotificationsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
