/*
 * An XML document type.
 * Localname: RiskSummary
 * Namespace: http://idanalytics.com/products/compliance/result
 * Java type: com.idanalytics.products.compliance.result.RiskSummaryDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.compliance.result.impl;
/**
 * A document containing one RiskSummary(@http://idanalytics.com/products/compliance/result) element.
 *
 * This is a complex type.
 */
public class RiskSummaryDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.compliance.result.RiskSummaryDocument
{
    private static final long serialVersionUID = 1L;
    
    public RiskSummaryDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName RISKSUMMARY$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/compliance/result", "RiskSummary");
    
    
    /**
     * Gets the "RiskSummary" element
     */
    public com.idanalytics.products.compliance.result.RiskSummaryDocument.RiskSummary getRiskSummary()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.compliance.result.RiskSummaryDocument.RiskSummary target = null;
            target = (com.idanalytics.products.compliance.result.RiskSummaryDocument.RiskSummary)get_store().find_element_user(RISKSUMMARY$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "RiskSummary" element
     */
    public void setRiskSummary(com.idanalytics.products.compliance.result.RiskSummaryDocument.RiskSummary riskSummary)
    {
        generatedSetterHelperImpl(riskSummary, RISKSUMMARY$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "RiskSummary" element
     */
    public com.idanalytics.products.compliance.result.RiskSummaryDocument.RiskSummary addNewRiskSummary()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.compliance.result.RiskSummaryDocument.RiskSummary target = null;
            target = (com.idanalytics.products.compliance.result.RiskSummaryDocument.RiskSummary)get_store().add_element_user(RISKSUMMARY$0);
            return target;
        }
    }
    /**
     * An XML RiskSummary(@http://idanalytics.com/products/compliance/result).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.compliance.result.RiskSummaryDocument$RiskSummary.
     */
    public static class RiskSummaryImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.compliance.result.RiskSummaryDocument.RiskSummary
    {
        private static final long serialVersionUID = 1L;
        
        public RiskSummaryImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, true);
        }
        
        protected RiskSummaryImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
        
        
    }
}
