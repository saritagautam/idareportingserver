/*
 * An XML document type.
 * Localname: Value
 * Namespace: http://idanalytics.com/products/compliance/result
 * Java type: com.idanalytics.products.compliance.result.ValueDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.compliance.result.impl;
/**
 * A document containing one Value(@http://idanalytics.com/products/compliance/result) element.
 *
 * This is a complex type.
 */
public class ValueDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.compliance.result.ValueDocument
{
    private static final long serialVersionUID = 1L;
    
    public ValueDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName VALUE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/compliance/result", "Value");
    
    
    /**
     * Gets the "Value" element
     */
    public java.lang.String getValue()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(VALUE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Value" element
     */
    public com.idanalytics.products.compliance.result.ValueDocument.Value xgetValue()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.compliance.result.ValueDocument.Value target = null;
            target = (com.idanalytics.products.compliance.result.ValueDocument.Value)get_store().find_element_user(VALUE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Value" element
     */
    public void setValue(java.lang.String value)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(VALUE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(VALUE$0);
            }
            target.setStringValue(value);
        }
    }
    
    /**
     * Sets (as xml) the "Value" element
     */
    public void xsetValue(com.idanalytics.products.compliance.result.ValueDocument.Value value)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.compliance.result.ValueDocument.Value target = null;
            target = (com.idanalytics.products.compliance.result.ValueDocument.Value)get_store().find_element_user(VALUE$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.compliance.result.ValueDocument.Value)get_store().add_element_user(VALUE$0);
            }
            target.set(value);
        }
    }
    /**
     * An XML Value(@http://idanalytics.com/products/compliance/result).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.compliance.result.ValueDocument$Value.
     */
    public static class ValueImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.compliance.result.ValueDocument.Value
    {
        private static final long serialVersionUID = 1L;
        
        public ValueImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected ValueImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
