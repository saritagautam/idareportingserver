/*
 * An XML document type.
 * Localname: ComplianceRecord
 * Namespace: http://idanalytics.com/products/compliance/result
 * Java type: com.idanalytics.products.compliance.result.ComplianceRecordDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.compliance.result.impl;
/**
 * A document containing one ComplianceRecord(@http://idanalytics.com/products/compliance/result) element.
 *
 * This is a complex type.
 */
public class ComplianceRecordDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.compliance.result.ComplianceRecordDocument
{
    private static final long serialVersionUID = 1L;
    
    public ComplianceRecordDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName COMPLIANCERECORD$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/compliance/result", "ComplianceRecord");
    
    
    /**
     * Gets the "ComplianceRecord" element
     */
    public com.idanalytics.products.compliance.result.ComplianceRecordDocument.ComplianceRecord getComplianceRecord()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.compliance.result.ComplianceRecordDocument.ComplianceRecord target = null;
            target = (com.idanalytics.products.compliance.result.ComplianceRecordDocument.ComplianceRecord)get_store().find_element_user(COMPLIANCERECORD$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "ComplianceRecord" element
     */
    public void setComplianceRecord(com.idanalytics.products.compliance.result.ComplianceRecordDocument.ComplianceRecord complianceRecord)
    {
        generatedSetterHelperImpl(complianceRecord, COMPLIANCERECORD$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "ComplianceRecord" element
     */
    public com.idanalytics.products.compliance.result.ComplianceRecordDocument.ComplianceRecord addNewComplianceRecord()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.compliance.result.ComplianceRecordDocument.ComplianceRecord target = null;
            target = (com.idanalytics.products.compliance.result.ComplianceRecordDocument.ComplianceRecord)get_store().add_element_user(COMPLIANCERECORD$0);
            return target;
        }
    }
    /**
     * An XML ComplianceRecord(@http://idanalytics.com/products/compliance/result).
     *
     * This is a complex type.
     */
    public static class ComplianceRecordImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.compliance.result.ComplianceRecordDocument.ComplianceRecord
    {
        private static final long serialVersionUID = 1L;
        
        public ComplianceRecordImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName APPID$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/compliance/result", "AppID");
        private static final javax.xml.namespace.QName IDASEQUENCE$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/compliance/result", "IDASequence");
        private static final javax.xml.namespace.QName IDATIMESTAMP$4 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/compliance/result", "IDATimeStamp");
        private static final javax.xml.namespace.QName PASSTHRU1$6 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/compliance/result", "PassThru1");
        private static final javax.xml.namespace.QName PASSTHRU2$8 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/compliance/result", "PassThru2");
        private static final javax.xml.namespace.QName PASSTHRU3$10 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/compliance/result", "PassThru3");
        private static final javax.xml.namespace.QName PASSTHRU4$12 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/compliance/result", "PassThru4");
        private static final javax.xml.namespace.QName RISKSUMMARY$14 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/compliance/result", "RiskSummary");
        private static final javax.xml.namespace.QName INDICATORS$16 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/compliance/result", "Indicators");
        private static final javax.xml.namespace.QName ACTIONSUMMARY$18 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/compliance/result", "ActionSummary");
        private static final javax.xml.namespace.QName SCHEMAVERSION$20 = 
            new javax.xml.namespace.QName("", "schemaVersion");
        
        
        /**
         * Gets the "AppID" element
         */
        public java.lang.String getAppID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(APPID$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "AppID" element
         */
        public com.idanalytics.products.compliance.result.AppIDDocument.AppID xgetAppID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.AppIDDocument.AppID target = null;
                target = (com.idanalytics.products.compliance.result.AppIDDocument.AppID)get_store().find_element_user(APPID$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "AppID" element
         */
        public void setAppID(java.lang.String appID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(APPID$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(APPID$0);
                }
                target.setStringValue(appID);
            }
        }
        
        /**
         * Sets (as xml) the "AppID" element
         */
        public void xsetAppID(com.idanalytics.products.compliance.result.AppIDDocument.AppID appID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.AppIDDocument.AppID target = null;
                target = (com.idanalytics.products.compliance.result.AppIDDocument.AppID)get_store().find_element_user(APPID$0, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.compliance.result.AppIDDocument.AppID)get_store().add_element_user(APPID$0);
                }
                target.set(appID);
            }
        }
        
        /**
         * Gets the "IDASequence" element
         */
        public java.lang.String getIDASequence()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASEQUENCE$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "IDASequence" element
         */
        public com.idanalytics.products.compliance.result.IDASequenceDocument.IDASequence xgetIDASequence()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.IDASequenceDocument.IDASequence target = null;
                target = (com.idanalytics.products.compliance.result.IDASequenceDocument.IDASequence)get_store().find_element_user(IDASEQUENCE$2, 0);
                return target;
            }
        }
        
        /**
         * Sets the "IDASequence" element
         */
        public void setIDASequence(java.lang.String idaSequence)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASEQUENCE$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDASEQUENCE$2);
                }
                target.setStringValue(idaSequence);
            }
        }
        
        /**
         * Sets (as xml) the "IDASequence" element
         */
        public void xsetIDASequence(com.idanalytics.products.compliance.result.IDASequenceDocument.IDASequence idaSequence)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.IDASequenceDocument.IDASequence target = null;
                target = (com.idanalytics.products.compliance.result.IDASequenceDocument.IDASequence)get_store().find_element_user(IDASEQUENCE$2, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.compliance.result.IDASequenceDocument.IDASequence)get_store().add_element_user(IDASEQUENCE$2);
                }
                target.set(idaSequence);
            }
        }
        
        /**
         * Gets the "IDATimeStamp" element
         */
        public java.util.Calendar getIDATimeStamp()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDATIMESTAMP$4, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getCalendarValue();
            }
        }
        
        /**
         * Gets (as xml) the "IDATimeStamp" element
         */
        public com.idanalytics.products.compliance.result.IDATimeStampDocument.IDATimeStamp xgetIDATimeStamp()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.IDATimeStampDocument.IDATimeStamp target = null;
                target = (com.idanalytics.products.compliance.result.IDATimeStampDocument.IDATimeStamp)get_store().find_element_user(IDATIMESTAMP$4, 0);
                return target;
            }
        }
        
        /**
         * Sets the "IDATimeStamp" element
         */
        public void setIDATimeStamp(java.util.Calendar idaTimeStamp)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDATIMESTAMP$4, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDATIMESTAMP$4);
                }
                target.setCalendarValue(idaTimeStamp);
            }
        }
        
        /**
         * Sets (as xml) the "IDATimeStamp" element
         */
        public void xsetIDATimeStamp(com.idanalytics.products.compliance.result.IDATimeStampDocument.IDATimeStamp idaTimeStamp)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.IDATimeStampDocument.IDATimeStamp target = null;
                target = (com.idanalytics.products.compliance.result.IDATimeStampDocument.IDATimeStamp)get_store().find_element_user(IDATIMESTAMP$4, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.compliance.result.IDATimeStampDocument.IDATimeStamp)get_store().add_element_user(IDATIMESTAMP$4);
                }
                target.set(idaTimeStamp);
            }
        }
        
        /**
         * Gets the "PassThru1" element
         */
        public java.lang.String getPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU1$6, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru1" element
         */
        public com.idanalytics.products.compliance.result.PassThru1Document.PassThru1 xgetPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.PassThru1Document.PassThru1 target = null;
                target = (com.idanalytics.products.compliance.result.PassThru1Document.PassThru1)get_store().find_element_user(PASSTHRU1$6, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru1" element
         */
        public boolean isSetPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU1$6) != 0;
            }
        }
        
        /**
         * Sets the "PassThru1" element
         */
        public void setPassThru1(java.lang.String passThru1)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU1$6, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU1$6);
                }
                target.setStringValue(passThru1);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru1" element
         */
        public void xsetPassThru1(com.idanalytics.products.compliance.result.PassThru1Document.PassThru1 passThru1)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.PassThru1Document.PassThru1 target = null;
                target = (com.idanalytics.products.compliance.result.PassThru1Document.PassThru1)get_store().find_element_user(PASSTHRU1$6, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.compliance.result.PassThru1Document.PassThru1)get_store().add_element_user(PASSTHRU1$6);
                }
                target.set(passThru1);
            }
        }
        
        /**
         * Unsets the "PassThru1" element
         */
        public void unsetPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU1$6, 0);
            }
        }
        
        /**
         * Gets the "PassThru2" element
         */
        public java.lang.String getPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU2$8, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru2" element
         */
        public com.idanalytics.products.compliance.result.PassThru2Document.PassThru2 xgetPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.PassThru2Document.PassThru2 target = null;
                target = (com.idanalytics.products.compliance.result.PassThru2Document.PassThru2)get_store().find_element_user(PASSTHRU2$8, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru2" element
         */
        public boolean isSetPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU2$8) != 0;
            }
        }
        
        /**
         * Sets the "PassThru2" element
         */
        public void setPassThru2(java.lang.String passThru2)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU2$8, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU2$8);
                }
                target.setStringValue(passThru2);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru2" element
         */
        public void xsetPassThru2(com.idanalytics.products.compliance.result.PassThru2Document.PassThru2 passThru2)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.PassThru2Document.PassThru2 target = null;
                target = (com.idanalytics.products.compliance.result.PassThru2Document.PassThru2)get_store().find_element_user(PASSTHRU2$8, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.compliance.result.PassThru2Document.PassThru2)get_store().add_element_user(PASSTHRU2$8);
                }
                target.set(passThru2);
            }
        }
        
        /**
         * Unsets the "PassThru2" element
         */
        public void unsetPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU2$8, 0);
            }
        }
        
        /**
         * Gets the "PassThru3" element
         */
        public java.lang.String getPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU3$10, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru3" element
         */
        public com.idanalytics.products.compliance.result.PassThru3Document.PassThru3 xgetPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.PassThru3Document.PassThru3 target = null;
                target = (com.idanalytics.products.compliance.result.PassThru3Document.PassThru3)get_store().find_element_user(PASSTHRU3$10, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru3" element
         */
        public boolean isSetPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU3$10) != 0;
            }
        }
        
        /**
         * Sets the "PassThru3" element
         */
        public void setPassThru3(java.lang.String passThru3)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU3$10, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU3$10);
                }
                target.setStringValue(passThru3);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru3" element
         */
        public void xsetPassThru3(com.idanalytics.products.compliance.result.PassThru3Document.PassThru3 passThru3)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.PassThru3Document.PassThru3 target = null;
                target = (com.idanalytics.products.compliance.result.PassThru3Document.PassThru3)get_store().find_element_user(PASSTHRU3$10, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.compliance.result.PassThru3Document.PassThru3)get_store().add_element_user(PASSTHRU3$10);
                }
                target.set(passThru3);
            }
        }
        
        /**
         * Unsets the "PassThru3" element
         */
        public void unsetPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU3$10, 0);
            }
        }
        
        /**
         * Gets the "PassThru4" element
         */
        public java.lang.String getPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU4$12, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru4" element
         */
        public com.idanalytics.products.compliance.result.PassThru4Document.PassThru4 xgetPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.PassThru4Document.PassThru4 target = null;
                target = (com.idanalytics.products.compliance.result.PassThru4Document.PassThru4)get_store().find_element_user(PASSTHRU4$12, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru4" element
         */
        public boolean isSetPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU4$12) != 0;
            }
        }
        
        /**
         * Sets the "PassThru4" element
         */
        public void setPassThru4(java.lang.String passThru4)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU4$12, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU4$12);
                }
                target.setStringValue(passThru4);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru4" element
         */
        public void xsetPassThru4(com.idanalytics.products.compliance.result.PassThru4Document.PassThru4 passThru4)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.PassThru4Document.PassThru4 target = null;
                target = (com.idanalytics.products.compliance.result.PassThru4Document.PassThru4)get_store().find_element_user(PASSTHRU4$12, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.compliance.result.PassThru4Document.PassThru4)get_store().add_element_user(PASSTHRU4$12);
                }
                target.set(passThru4);
            }
        }
        
        /**
         * Unsets the "PassThru4" element
         */
        public void unsetPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU4$12, 0);
            }
        }
        
        /**
         * Gets the "RiskSummary" element
         */
        public com.idanalytics.products.compliance.result.RiskSummaryDocument.RiskSummary getRiskSummary()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.RiskSummaryDocument.RiskSummary target = null;
                target = (com.idanalytics.products.compliance.result.RiskSummaryDocument.RiskSummary)get_store().find_element_user(RISKSUMMARY$14, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * Sets the "RiskSummary" element
         */
        public void setRiskSummary(com.idanalytics.products.compliance.result.RiskSummaryDocument.RiskSummary riskSummary)
        {
            generatedSetterHelperImpl(riskSummary, RISKSUMMARY$14, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "RiskSummary" element
         */
        public com.idanalytics.products.compliance.result.RiskSummaryDocument.RiskSummary addNewRiskSummary()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.RiskSummaryDocument.RiskSummary target = null;
                target = (com.idanalytics.products.compliance.result.RiskSummaryDocument.RiskSummary)get_store().add_element_user(RISKSUMMARY$14);
                return target;
            }
        }
        
        /**
         * Gets the "Indicators" element
         */
        public com.idanalytics.products.compliance.result.IndicatorsDocument.Indicators getIndicators()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.IndicatorsDocument.Indicators target = null;
                target = (com.idanalytics.products.compliance.result.IndicatorsDocument.Indicators)get_store().find_element_user(INDICATORS$16, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * Sets the "Indicators" element
         */
        public void setIndicators(com.idanalytics.products.compliance.result.IndicatorsDocument.Indicators indicators)
        {
            generatedSetterHelperImpl(indicators, INDICATORS$16, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "Indicators" element
         */
        public com.idanalytics.products.compliance.result.IndicatorsDocument.Indicators addNewIndicators()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.IndicatorsDocument.Indicators target = null;
                target = (com.idanalytics.products.compliance.result.IndicatorsDocument.Indicators)get_store().add_element_user(INDICATORS$16);
                return target;
            }
        }
        
        /**
         * Gets the "ActionSummary" element
         */
        public com.idanalytics.products.compliance.result.ActionSummaryDocument.ActionSummary getActionSummary()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.ActionSummaryDocument.ActionSummary target = null;
                target = (com.idanalytics.products.compliance.result.ActionSummaryDocument.ActionSummary)get_store().find_element_user(ACTIONSUMMARY$18, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "ActionSummary" element
         */
        public boolean isSetActionSummary()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(ACTIONSUMMARY$18) != 0;
            }
        }
        
        /**
         * Sets the "ActionSummary" element
         */
        public void setActionSummary(com.idanalytics.products.compliance.result.ActionSummaryDocument.ActionSummary actionSummary)
        {
            generatedSetterHelperImpl(actionSummary, ACTIONSUMMARY$18, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "ActionSummary" element
         */
        public com.idanalytics.products.compliance.result.ActionSummaryDocument.ActionSummary addNewActionSummary()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.ActionSummaryDocument.ActionSummary target = null;
                target = (com.idanalytics.products.compliance.result.ActionSummaryDocument.ActionSummary)get_store().add_element_user(ACTIONSUMMARY$18);
                return target;
            }
        }
        
        /**
         * Unsets the "ActionSummary" element
         */
        public void unsetActionSummary()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(ACTIONSUMMARY$18, 0);
            }
        }
        
        /**
         * Gets the "schemaVersion" attribute
         */
        public java.math.BigDecimal getSchemaVersion()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(SCHEMAVERSION$20);
                if (target == null)
                {
                    return null;
                }
                return target.getBigDecimalValue();
            }
        }
        
        /**
         * Gets (as xml) the "schemaVersion" attribute
         */
        public org.apache.xmlbeans.XmlDecimal xgetSchemaVersion()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlDecimal target = null;
                target = (org.apache.xmlbeans.XmlDecimal)get_store().find_attribute_user(SCHEMAVERSION$20);
                return target;
            }
        }
        
        /**
         * True if has "schemaVersion" attribute
         */
        public boolean isSetSchemaVersion()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().find_attribute_user(SCHEMAVERSION$20) != null;
            }
        }
        
        /**
         * Sets the "schemaVersion" attribute
         */
        public void setSchemaVersion(java.math.BigDecimal schemaVersion)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(SCHEMAVERSION$20);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(SCHEMAVERSION$20);
                }
                target.setBigDecimalValue(schemaVersion);
            }
        }
        
        /**
         * Sets (as xml) the "schemaVersion" attribute
         */
        public void xsetSchemaVersion(org.apache.xmlbeans.XmlDecimal schemaVersion)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlDecimal target = null;
                target = (org.apache.xmlbeans.XmlDecimal)get_store().find_attribute_user(SCHEMAVERSION$20);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlDecimal)get_store().add_attribute_user(SCHEMAVERSION$20);
                }
                target.set(schemaVersion);
            }
        }
        
        /**
         * Unsets the "schemaVersion" attribute
         */
        public void unsetSchemaVersion()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_attribute(SCHEMAVERSION$20);
            }
        }
    }
}
