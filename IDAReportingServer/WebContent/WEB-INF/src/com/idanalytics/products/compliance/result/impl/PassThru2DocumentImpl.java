/*
 * An XML document type.
 * Localname: PassThru2
 * Namespace: http://idanalytics.com/products/compliance/result
 * Java type: com.idanalytics.products.compliance.result.PassThru2Document
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.compliance.result.impl;
/**
 * A document containing one PassThru2(@http://idanalytics.com/products/compliance/result) element.
 *
 * This is a complex type.
 */
public class PassThru2DocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.compliance.result.PassThru2Document
{
    private static final long serialVersionUID = 1L;
    
    public PassThru2DocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName PASSTHRU2$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/compliance/result", "PassThru2");
    
    
    /**
     * Gets the "PassThru2" element
     */
    public java.lang.String getPassThru2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU2$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PassThru2" element
     */
    public com.idanalytics.products.compliance.result.PassThru2Document.PassThru2 xgetPassThru2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.compliance.result.PassThru2Document.PassThru2 target = null;
            target = (com.idanalytics.products.compliance.result.PassThru2Document.PassThru2)get_store().find_element_user(PASSTHRU2$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "PassThru2" element
     */
    public void setPassThru2(java.lang.String passThru2)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU2$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU2$0);
            }
            target.setStringValue(passThru2);
        }
    }
    
    /**
     * Sets (as xml) the "PassThru2" element
     */
    public void xsetPassThru2(com.idanalytics.products.compliance.result.PassThru2Document.PassThru2 passThru2)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.compliance.result.PassThru2Document.PassThru2 target = null;
            target = (com.idanalytics.products.compliance.result.PassThru2Document.PassThru2)get_store().find_element_user(PASSTHRU2$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.compliance.result.PassThru2Document.PassThru2)get_store().add_element_user(PASSTHRU2$0);
            }
            target.set(passThru2);
        }
    }
    /**
     * An XML PassThru2(@http://idanalytics.com/products/compliance/result).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.compliance.result.PassThru2Document$PassThru2.
     */
    public static class PassThru2Impl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.compliance.result.PassThru2Document.PassThru2
    {
        private static final long serialVersionUID = 1L;
        
        public PassThru2Impl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected PassThru2Impl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
