/*
 * An XML document type.
 * Localname: Group
 * Namespace: http://idanalytics.com/products/compliance/result
 * Java type: com.idanalytics.products.compliance.result.GroupDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.compliance.result.impl;
/**
 * A document containing one Group(@http://idanalytics.com/products/compliance/result) element.
 *
 * This is a complex type.
 */
public class GroupDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.compliance.result.GroupDocument
{
    private static final long serialVersionUID = 1L;
    
    public GroupDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GROUP$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/compliance/result", "Group");
    
    
    /**
     * Gets the "Group" element
     */
    public com.idanalytics.products.compliance.result.GroupDocument.Group getGroup()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.compliance.result.GroupDocument.Group target = null;
            target = (com.idanalytics.products.compliance.result.GroupDocument.Group)get_store().find_element_user(GROUP$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "Group" element
     */
    public void setGroup(com.idanalytics.products.compliance.result.GroupDocument.Group group)
    {
        generatedSetterHelperImpl(group, GROUP$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "Group" element
     */
    public com.idanalytics.products.compliance.result.GroupDocument.Group addNewGroup()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.compliance.result.GroupDocument.Group target = null;
            target = (com.idanalytics.products.compliance.result.GroupDocument.Group)get_store().add_element_user(GROUP$0);
            return target;
        }
    }
    /**
     * An XML Group(@http://idanalytics.com/products/compliance/result).
     *
     * This is a complex type.
     */
    public static class GroupImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.compliance.result.GroupDocument.Group
    {
        private static final long serialVersionUID = 1L;
        
        public GroupImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName STATUS$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/compliance/result", "Status");
        private static final javax.xml.namespace.QName INDICATOR$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/compliance/result", "Indicator");
        private static final javax.xml.namespace.QName NAME$4 = 
            new javax.xml.namespace.QName("", "name");
        
        
        /**
         * Gets the "Status" element
         */
        public java.lang.String getStatus()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STATUS$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "Status" element
         */
        public com.idanalytics.products.compliance.result.StatusDocument.Status xgetStatus()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.StatusDocument.Status target = null;
                target = (com.idanalytics.products.compliance.result.StatusDocument.Status)get_store().find_element_user(STATUS$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "Status" element
         */
        public void setStatus(java.lang.String status)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STATUS$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(STATUS$0);
                }
                target.setStringValue(status);
            }
        }
        
        /**
         * Sets (as xml) the "Status" element
         */
        public void xsetStatus(com.idanalytics.products.compliance.result.StatusDocument.Status status)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.StatusDocument.Status target = null;
                target = (com.idanalytics.products.compliance.result.StatusDocument.Status)get_store().find_element_user(STATUS$0, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.compliance.result.StatusDocument.Status)get_store().add_element_user(STATUS$0);
                }
                target.set(status);
            }
        }
        
        /**
         * Gets array of all "Indicator" elements
         */
        public com.idanalytics.products.compliance.result.IndicatorDocument.Indicator[] getIndicatorArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(INDICATOR$2, targetList);
                com.idanalytics.products.compliance.result.IndicatorDocument.Indicator[] result = new com.idanalytics.products.compliance.result.IndicatorDocument.Indicator[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "Indicator" element
         */
        public com.idanalytics.products.compliance.result.IndicatorDocument.Indicator getIndicatorArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.IndicatorDocument.Indicator target = null;
                target = (com.idanalytics.products.compliance.result.IndicatorDocument.Indicator)get_store().find_element_user(INDICATOR$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "Indicator" element
         */
        public int sizeOfIndicatorArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(INDICATOR$2);
            }
        }
        
        /**
         * Sets array of all "Indicator" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setIndicatorArray(com.idanalytics.products.compliance.result.IndicatorDocument.Indicator[] indicatorArray)
        {
            check_orphaned();
            arraySetterHelper(indicatorArray, INDICATOR$2);
        }
        
        /**
         * Sets ith "Indicator" element
         */
        public void setIndicatorArray(int i, com.idanalytics.products.compliance.result.IndicatorDocument.Indicator indicator)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.IndicatorDocument.Indicator target = null;
                target = (com.idanalytics.products.compliance.result.IndicatorDocument.Indicator)get_store().find_element_user(INDICATOR$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(indicator);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "Indicator" element
         */
        public com.idanalytics.products.compliance.result.IndicatorDocument.Indicator insertNewIndicator(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.IndicatorDocument.Indicator target = null;
                target = (com.idanalytics.products.compliance.result.IndicatorDocument.Indicator)get_store().insert_element_user(INDICATOR$2, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "Indicator" element
         */
        public com.idanalytics.products.compliance.result.IndicatorDocument.Indicator addNewIndicator()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.IndicatorDocument.Indicator target = null;
                target = (com.idanalytics.products.compliance.result.IndicatorDocument.Indicator)get_store().add_element_user(INDICATOR$2);
                return target;
            }
        }
        
        /**
         * Removes the ith "Indicator" element
         */
        public void removeIndicator(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(INDICATOR$2, i);
            }
        }
        
        /**
         * Gets the "name" attribute
         */
        public java.lang.String getName()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(NAME$4);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "name" attribute
         */
        public org.apache.xmlbeans.XmlString xgetName()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_attribute_user(NAME$4);
                return target;
            }
        }
        
        /**
         * Sets the "name" attribute
         */
        public void setName(java.lang.String name)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(NAME$4);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(NAME$4);
                }
                target.setStringValue(name);
            }
        }
        
        /**
         * Sets (as xml) the "name" attribute
         */
        public void xsetName(org.apache.xmlbeans.XmlString name)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_attribute_user(NAME$4);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlString)get_store().add_attribute_user(NAME$4);
                }
                target.set(name);
            }
        }
    }
}
