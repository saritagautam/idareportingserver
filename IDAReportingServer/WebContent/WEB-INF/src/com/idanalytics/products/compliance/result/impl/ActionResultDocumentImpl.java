/*
 * An XML document type.
 * Localname: ActionResult
 * Namespace: http://idanalytics.com/products/compliance/result
 * Java type: com.idanalytics.products.compliance.result.ActionResultDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.compliance.result.impl;
/**
 * A document containing one ActionResult(@http://idanalytics.com/products/compliance/result) element.
 *
 * This is a complex type.
 */
public class ActionResultDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.compliance.result.ActionResultDocument
{
    private static final long serialVersionUID = 1L;
    
    public ActionResultDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ACTIONRESULT$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/compliance/result", "ActionResult");
    
    
    /**
     * Gets the "ActionResult" element
     */
    public com.idanalytics.products.compliance.result.ActionResultDocument.ActionResult getActionResult()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.compliance.result.ActionResultDocument.ActionResult target = null;
            target = (com.idanalytics.products.compliance.result.ActionResultDocument.ActionResult)get_store().find_element_user(ACTIONRESULT$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "ActionResult" element
     */
    public void setActionResult(com.idanalytics.products.compliance.result.ActionResultDocument.ActionResult actionResult)
    {
        generatedSetterHelperImpl(actionResult, ACTIONRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "ActionResult" element
     */
    public com.idanalytics.products.compliance.result.ActionResultDocument.ActionResult addNewActionResult()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.compliance.result.ActionResultDocument.ActionResult target = null;
            target = (com.idanalytics.products.compliance.result.ActionResultDocument.ActionResult)get_store().add_element_user(ACTIONRESULT$0);
            return target;
        }
    }
    /**
     * An XML ActionResult(@http://idanalytics.com/products/compliance/result).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.compliance.result.ActionResultDocument$ActionResult.
     */
    public static class ActionResultImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.compliance.result.ActionResultDocument.ActionResult
    {
        private static final long serialVersionUID = 1L;
        
        public ActionResultImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, true);
        }
        
        protected ActionResultImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
        
        
    }
}
