/*
 * An XML document type.
 * Localname: ActionSummary
 * Namespace: http://idanalytics.com/products/compliance/result
 * Java type: com.idanalytics.products.compliance.result.ActionSummaryDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.compliance.result.impl;
/**
 * A document containing one ActionSummary(@http://idanalytics.com/products/compliance/result) element.
 *
 * This is a complex type.
 */
public class ActionSummaryDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.compliance.result.ActionSummaryDocument
{
    private static final long serialVersionUID = 1L;
    
    public ActionSummaryDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ACTIONSUMMARY$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/compliance/result", "ActionSummary");
    
    
    /**
     * Gets the "ActionSummary" element
     */
    public com.idanalytics.products.compliance.result.ActionSummaryDocument.ActionSummary getActionSummary()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.compliance.result.ActionSummaryDocument.ActionSummary target = null;
            target = (com.idanalytics.products.compliance.result.ActionSummaryDocument.ActionSummary)get_store().find_element_user(ACTIONSUMMARY$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "ActionSummary" element
     */
    public void setActionSummary(com.idanalytics.products.compliance.result.ActionSummaryDocument.ActionSummary actionSummary)
    {
        generatedSetterHelperImpl(actionSummary, ACTIONSUMMARY$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "ActionSummary" element
     */
    public com.idanalytics.products.compliance.result.ActionSummaryDocument.ActionSummary addNewActionSummary()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.compliance.result.ActionSummaryDocument.ActionSummary target = null;
            target = (com.idanalytics.products.compliance.result.ActionSummaryDocument.ActionSummary)get_store().add_element_user(ACTIONSUMMARY$0);
            return target;
        }
    }
    /**
     * An XML ActionSummary(@http://idanalytics.com/products/compliance/result).
     *
     * This is a complex type.
     */
    public static class ActionSummaryImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.compliance.result.ActionSummaryDocument.ActionSummary
    {
        private static final long serialVersionUID = 1L;
        
        public ActionSummaryImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName RECOMMENDEDACTION$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/compliance/result", "RecommendedAction");
        private static final javax.xml.namespace.QName ACTIONRESULT$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/compliance/result", "ActionResult");
        
        
        /**
         * Gets array of all "RecommendedAction" elements
         */
        public com.idanalytics.products.compliance.result.RecommendedActionDocument.RecommendedAction[] getRecommendedActionArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(RECOMMENDEDACTION$0, targetList);
                com.idanalytics.products.compliance.result.RecommendedActionDocument.RecommendedAction[] result = new com.idanalytics.products.compliance.result.RecommendedActionDocument.RecommendedAction[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "RecommendedAction" element
         */
        public com.idanalytics.products.compliance.result.RecommendedActionDocument.RecommendedAction getRecommendedActionArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.RecommendedActionDocument.RecommendedAction target = null;
                target = (com.idanalytics.products.compliance.result.RecommendedActionDocument.RecommendedAction)get_store().find_element_user(RECOMMENDEDACTION$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "RecommendedAction" element
         */
        public int sizeOfRecommendedActionArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(RECOMMENDEDACTION$0);
            }
        }
        
        /**
         * Sets array of all "RecommendedAction" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setRecommendedActionArray(com.idanalytics.products.compliance.result.RecommendedActionDocument.RecommendedAction[] recommendedActionArray)
        {
            check_orphaned();
            arraySetterHelper(recommendedActionArray, RECOMMENDEDACTION$0);
        }
        
        /**
         * Sets ith "RecommendedAction" element
         */
        public void setRecommendedActionArray(int i, com.idanalytics.products.compliance.result.RecommendedActionDocument.RecommendedAction recommendedAction)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.RecommendedActionDocument.RecommendedAction target = null;
                target = (com.idanalytics.products.compliance.result.RecommendedActionDocument.RecommendedAction)get_store().find_element_user(RECOMMENDEDACTION$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(recommendedAction);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "RecommendedAction" element
         */
        public com.idanalytics.products.compliance.result.RecommendedActionDocument.RecommendedAction insertNewRecommendedAction(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.RecommendedActionDocument.RecommendedAction target = null;
                target = (com.idanalytics.products.compliance.result.RecommendedActionDocument.RecommendedAction)get_store().insert_element_user(RECOMMENDEDACTION$0, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "RecommendedAction" element
         */
        public com.idanalytics.products.compliance.result.RecommendedActionDocument.RecommendedAction addNewRecommendedAction()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.RecommendedActionDocument.RecommendedAction target = null;
                target = (com.idanalytics.products.compliance.result.RecommendedActionDocument.RecommendedAction)get_store().add_element_user(RECOMMENDEDACTION$0);
                return target;
            }
        }
        
        /**
         * Removes the ith "RecommendedAction" element
         */
        public void removeRecommendedAction(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(RECOMMENDEDACTION$0, i);
            }
        }
        
        /**
         * Gets array of all "ActionResult" elements
         */
        public com.idanalytics.products.compliance.result.ActionResultDocument.ActionResult[] getActionResultArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(ACTIONRESULT$2, targetList);
                com.idanalytics.products.compliance.result.ActionResultDocument.ActionResult[] result = new com.idanalytics.products.compliance.result.ActionResultDocument.ActionResult[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "ActionResult" element
         */
        public com.idanalytics.products.compliance.result.ActionResultDocument.ActionResult getActionResultArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.ActionResultDocument.ActionResult target = null;
                target = (com.idanalytics.products.compliance.result.ActionResultDocument.ActionResult)get_store().find_element_user(ACTIONRESULT$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "ActionResult" element
         */
        public int sizeOfActionResultArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(ACTIONRESULT$2);
            }
        }
        
        /**
         * Sets array of all "ActionResult" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setActionResultArray(com.idanalytics.products.compliance.result.ActionResultDocument.ActionResult[] actionResultArray)
        {
            check_orphaned();
            arraySetterHelper(actionResultArray, ACTIONRESULT$2);
        }
        
        /**
         * Sets ith "ActionResult" element
         */
        public void setActionResultArray(int i, com.idanalytics.products.compliance.result.ActionResultDocument.ActionResult actionResult)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.ActionResultDocument.ActionResult target = null;
                target = (com.idanalytics.products.compliance.result.ActionResultDocument.ActionResult)get_store().find_element_user(ACTIONRESULT$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(actionResult);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "ActionResult" element
         */
        public com.idanalytics.products.compliance.result.ActionResultDocument.ActionResult insertNewActionResult(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.ActionResultDocument.ActionResult target = null;
                target = (com.idanalytics.products.compliance.result.ActionResultDocument.ActionResult)get_store().insert_element_user(ACTIONRESULT$2, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "ActionResult" element
         */
        public com.idanalytics.products.compliance.result.ActionResultDocument.ActionResult addNewActionResult()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.ActionResultDocument.ActionResult target = null;
                target = (com.idanalytics.products.compliance.result.ActionResultDocument.ActionResult)get_store().add_element_user(ACTIONRESULT$2);
                return target;
            }
        }
        
        /**
         * Removes the ith "ActionResult" element
         */
        public void removeActionResult(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(ACTIONRESULT$2, i);
            }
        }
    }
}
