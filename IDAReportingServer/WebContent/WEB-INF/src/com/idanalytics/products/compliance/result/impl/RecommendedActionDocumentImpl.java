/*
 * An XML document type.
 * Localname: RecommendedAction
 * Namespace: http://idanalytics.com/products/compliance/result
 * Java type: com.idanalytics.products.compliance.result.RecommendedActionDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.compliance.result.impl;
/**
 * A document containing one RecommendedAction(@http://idanalytics.com/products/compliance/result) element.
 *
 * This is a complex type.
 */
public class RecommendedActionDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.compliance.result.RecommendedActionDocument
{
    private static final long serialVersionUID = 1L;
    
    public RecommendedActionDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName RECOMMENDEDACTION$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/compliance/result", "RecommendedAction");
    
    
    /**
     * Gets the "RecommendedAction" element
     */
    public com.idanalytics.products.compliance.result.RecommendedActionDocument.RecommendedAction getRecommendedAction()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.compliance.result.RecommendedActionDocument.RecommendedAction target = null;
            target = (com.idanalytics.products.compliance.result.RecommendedActionDocument.RecommendedAction)get_store().find_element_user(RECOMMENDEDACTION$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "RecommendedAction" element
     */
    public void setRecommendedAction(com.idanalytics.products.compliance.result.RecommendedActionDocument.RecommendedAction recommendedAction)
    {
        generatedSetterHelperImpl(recommendedAction, RECOMMENDEDACTION$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "RecommendedAction" element
     */
    public com.idanalytics.products.compliance.result.RecommendedActionDocument.RecommendedAction addNewRecommendedAction()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.compliance.result.RecommendedActionDocument.RecommendedAction target = null;
            target = (com.idanalytics.products.compliance.result.RecommendedActionDocument.RecommendedAction)get_store().add_element_user(RECOMMENDEDACTION$0);
            return target;
        }
    }
    /**
     * An XML RecommendedAction(@http://idanalytics.com/products/compliance/result).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.compliance.result.RecommendedActionDocument$RecommendedAction.
     */
    public static class RecommendedActionImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.compliance.result.RecommendedActionDocument.RecommendedAction
    {
        private static final long serialVersionUID = 1L;
        
        public RecommendedActionImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, true);
        }
        
        protected RecommendedActionImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
        
        
    }
}
