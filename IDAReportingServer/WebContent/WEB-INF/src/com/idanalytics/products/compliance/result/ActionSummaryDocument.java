/*
 * An XML document type.
 * Localname: ActionSummary
 * Namespace: http://idanalytics.com/products/compliance/result
 * Java type: com.idanalytics.products.compliance.result.ActionSummaryDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.compliance.result;


/**
 * A document containing one ActionSummary(@http://idanalytics.com/products/compliance/result) element.
 *
 * This is a complex type.
 */
public interface ActionSummaryDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ActionSummaryDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("actionsummary9ba0doctype");
    
    /**
     * Gets the "ActionSummary" element
     */
    com.idanalytics.products.compliance.result.ActionSummaryDocument.ActionSummary getActionSummary();
    
    /**
     * Sets the "ActionSummary" element
     */
    void setActionSummary(com.idanalytics.products.compliance.result.ActionSummaryDocument.ActionSummary actionSummary);
    
    /**
     * Appends and returns a new empty "ActionSummary" element
     */
    com.idanalytics.products.compliance.result.ActionSummaryDocument.ActionSummary addNewActionSummary();
    
    /**
     * An XML ActionSummary(@http://idanalytics.com/products/compliance/result).
     *
     * This is a complex type.
     */
    public interface ActionSummary extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ActionSummary.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("actionsummarycb7celemtype");
        
        /**
         * Gets array of all "RecommendedAction" elements
         */
        com.idanalytics.products.compliance.result.RecommendedActionDocument.RecommendedAction[] getRecommendedActionArray();
        
        /**
         * Gets ith "RecommendedAction" element
         */
        com.idanalytics.products.compliance.result.RecommendedActionDocument.RecommendedAction getRecommendedActionArray(int i);
        
        /**
         * Returns number of "RecommendedAction" element
         */
        int sizeOfRecommendedActionArray();
        
        /**
         * Sets array of all "RecommendedAction" element
         */
        void setRecommendedActionArray(com.idanalytics.products.compliance.result.RecommendedActionDocument.RecommendedAction[] recommendedActionArray);
        
        /**
         * Sets ith "RecommendedAction" element
         */
        void setRecommendedActionArray(int i, com.idanalytics.products.compliance.result.RecommendedActionDocument.RecommendedAction recommendedAction);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "RecommendedAction" element
         */
        com.idanalytics.products.compliance.result.RecommendedActionDocument.RecommendedAction insertNewRecommendedAction(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "RecommendedAction" element
         */
        com.idanalytics.products.compliance.result.RecommendedActionDocument.RecommendedAction addNewRecommendedAction();
        
        /**
         * Removes the ith "RecommendedAction" element
         */
        void removeRecommendedAction(int i);
        
        /**
         * Gets array of all "ActionResult" elements
         */
        com.idanalytics.products.compliance.result.ActionResultDocument.ActionResult[] getActionResultArray();
        
        /**
         * Gets ith "ActionResult" element
         */
        com.idanalytics.products.compliance.result.ActionResultDocument.ActionResult getActionResultArray(int i);
        
        /**
         * Returns number of "ActionResult" element
         */
        int sizeOfActionResultArray();
        
        /**
         * Sets array of all "ActionResult" element
         */
        void setActionResultArray(com.idanalytics.products.compliance.result.ActionResultDocument.ActionResult[] actionResultArray);
        
        /**
         * Sets ith "ActionResult" element
         */
        void setActionResultArray(int i, com.idanalytics.products.compliance.result.ActionResultDocument.ActionResult actionResult);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "ActionResult" element
         */
        com.idanalytics.products.compliance.result.ActionResultDocument.ActionResult insertNewActionResult(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "ActionResult" element
         */
        com.idanalytics.products.compliance.result.ActionResultDocument.ActionResult addNewActionResult();
        
        /**
         * Removes the ith "ActionResult" element
         */
        void removeActionResult(int i);
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.compliance.result.ActionSummaryDocument.ActionSummary newInstance() {
              return (com.idanalytics.products.compliance.result.ActionSummaryDocument.ActionSummary) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.compliance.result.ActionSummaryDocument.ActionSummary newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.compliance.result.ActionSummaryDocument.ActionSummary) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.compliance.result.ActionSummaryDocument newInstance() {
          return (com.idanalytics.products.compliance.result.ActionSummaryDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.compliance.result.ActionSummaryDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.compliance.result.ActionSummaryDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.compliance.result.ActionSummaryDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.compliance.result.ActionSummaryDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.compliance.result.ActionSummaryDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.compliance.result.ActionSummaryDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.compliance.result.ActionSummaryDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.compliance.result.ActionSummaryDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.compliance.result.ActionSummaryDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.compliance.result.ActionSummaryDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.compliance.result.ActionSummaryDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.compliance.result.ActionSummaryDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.compliance.result.ActionSummaryDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.compliance.result.ActionSummaryDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.compliance.result.ActionSummaryDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.compliance.result.ActionSummaryDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.compliance.result.ActionSummaryDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.compliance.result.ActionSummaryDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.compliance.result.ActionSummaryDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.compliance.result.ActionSummaryDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.compliance.result.ActionSummaryDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.compliance.result.ActionSummaryDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.compliance.result.ActionSummaryDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.compliance.result.ActionSummaryDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.compliance.result.ActionSummaryDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.compliance.result.ActionSummaryDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.compliance.result.ActionSummaryDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.compliance.result.ActionSummaryDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.compliance.result.ActionSummaryDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.compliance.result.ActionSummaryDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.compliance.result.ActionSummaryDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.compliance.result.ActionSummaryDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.compliance.result.ActionSummaryDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.compliance.result.ActionSummaryDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
