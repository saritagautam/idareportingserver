/*
 * An XML document type.
 * Localname: AppID
 * Namespace: http://idanalytics.com/products/compliance/result
 * Java type: com.idanalytics.products.compliance.result.AppIDDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.compliance.result.impl;
/**
 * A document containing one AppID(@http://idanalytics.com/products/compliance/result) element.
 *
 * This is a complex type.
 */
public class AppIDDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.compliance.result.AppIDDocument
{
    private static final long serialVersionUID = 1L;
    
    public AppIDDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName APPID$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/compliance/result", "AppID");
    
    
    /**
     * Gets the "AppID" element
     */
    public java.lang.String getAppID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(APPID$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "AppID" element
     */
    public com.idanalytics.products.compliance.result.AppIDDocument.AppID xgetAppID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.compliance.result.AppIDDocument.AppID target = null;
            target = (com.idanalytics.products.compliance.result.AppIDDocument.AppID)get_store().find_element_user(APPID$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "AppID" element
     */
    public void setAppID(java.lang.String appID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(APPID$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(APPID$0);
            }
            target.setStringValue(appID);
        }
    }
    
    /**
     * Sets (as xml) the "AppID" element
     */
    public void xsetAppID(com.idanalytics.products.compliance.result.AppIDDocument.AppID appID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.compliance.result.AppIDDocument.AppID target = null;
            target = (com.idanalytics.products.compliance.result.AppIDDocument.AppID)get_store().find_element_user(APPID$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.compliance.result.AppIDDocument.AppID)get_store().add_element_user(APPID$0);
            }
            target.set(appID);
        }
    }
    /**
     * An XML AppID(@http://idanalytics.com/products/compliance/result).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.compliance.result.AppIDDocument$AppID.
     */
    public static class AppIDImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.compliance.result.AppIDDocument.AppID
    {
        private static final long serialVersionUID = 1L;
        
        public AppIDImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected AppIDImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
