/*
 * An XML document type.
 * Localname: Indicator
 * Namespace: http://idanalytics.com/products/compliance/result
 * Java type: com.idanalytics.products.compliance.result.IndicatorDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.compliance.result.impl;
/**
 * A document containing one Indicator(@http://idanalytics.com/products/compliance/result) element.
 *
 * This is a complex type.
 */
public class IndicatorDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.compliance.result.IndicatorDocument
{
    private static final long serialVersionUID = 1L;
    
    public IndicatorDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName INDICATOR$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/compliance/result", "Indicator");
    
    
    /**
     * Gets the "Indicator" element
     */
    public com.idanalytics.products.compliance.result.IndicatorDocument.Indicator getIndicator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.compliance.result.IndicatorDocument.Indicator target = null;
            target = (com.idanalytics.products.compliance.result.IndicatorDocument.Indicator)get_store().find_element_user(INDICATOR$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "Indicator" element
     */
    public void setIndicator(com.idanalytics.products.compliance.result.IndicatorDocument.Indicator indicator)
    {
        generatedSetterHelperImpl(indicator, INDICATOR$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "Indicator" element
     */
    public com.idanalytics.products.compliance.result.IndicatorDocument.Indicator addNewIndicator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.compliance.result.IndicatorDocument.Indicator target = null;
            target = (com.idanalytics.products.compliance.result.IndicatorDocument.Indicator)get_store().add_element_user(INDICATOR$0);
            return target;
        }
    }
    /**
     * An XML Indicator(@http://idanalytics.com/products/compliance/result).
     *
     * This is a complex type.
     */
    public static class IndicatorImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.compliance.result.IndicatorDocument.Indicator
    {
        private static final long serialVersionUID = 1L;
        
        public IndicatorImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName DESCRIPTION$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/compliance/result", "Description");
        private static final javax.xml.namespace.QName VALUE$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/compliance/result", "Value");
        private static final javax.xml.namespace.QName STATUS$4 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/compliance/result", "Status");
        private static final javax.xml.namespace.QName NAME$6 = 
            new javax.xml.namespace.QName("", "name");
        
        
        /**
         * Gets the "Description" element
         */
        public java.lang.String getDescription()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESCRIPTION$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "Description" element
         */
        public com.idanalytics.products.compliance.result.DescriptionDocument.Description xgetDescription()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.DescriptionDocument.Description target = null;
                target = (com.idanalytics.products.compliance.result.DescriptionDocument.Description)get_store().find_element_user(DESCRIPTION$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "Description" element
         */
        public void setDescription(java.lang.String description)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESCRIPTION$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DESCRIPTION$0);
                }
                target.setStringValue(description);
            }
        }
        
        /**
         * Sets (as xml) the "Description" element
         */
        public void xsetDescription(com.idanalytics.products.compliance.result.DescriptionDocument.Description description)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.DescriptionDocument.Description target = null;
                target = (com.idanalytics.products.compliance.result.DescriptionDocument.Description)get_store().find_element_user(DESCRIPTION$0, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.compliance.result.DescriptionDocument.Description)get_store().add_element_user(DESCRIPTION$0);
                }
                target.set(description);
            }
        }
        
        /**
         * Gets the "Value" element
         */
        public java.lang.String getValue()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(VALUE$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "Value" element
         */
        public com.idanalytics.products.compliance.result.ValueDocument.Value xgetValue()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.ValueDocument.Value target = null;
                target = (com.idanalytics.products.compliance.result.ValueDocument.Value)get_store().find_element_user(VALUE$2, 0);
                return target;
            }
        }
        
        /**
         * Sets the "Value" element
         */
        public void setValue(java.lang.String value)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(VALUE$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(VALUE$2);
                }
                target.setStringValue(value);
            }
        }
        
        /**
         * Sets (as xml) the "Value" element
         */
        public void xsetValue(com.idanalytics.products.compliance.result.ValueDocument.Value value)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.ValueDocument.Value target = null;
                target = (com.idanalytics.products.compliance.result.ValueDocument.Value)get_store().find_element_user(VALUE$2, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.compliance.result.ValueDocument.Value)get_store().add_element_user(VALUE$2);
                }
                target.set(value);
            }
        }
        
        /**
         * Gets the "Status" element
         */
        public java.lang.String getStatus()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STATUS$4, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "Status" element
         */
        public com.idanalytics.products.compliance.result.StatusDocument.Status xgetStatus()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.StatusDocument.Status target = null;
                target = (com.idanalytics.products.compliance.result.StatusDocument.Status)get_store().find_element_user(STATUS$4, 0);
                return target;
            }
        }
        
        /**
         * True if has "Status" element
         */
        public boolean isSetStatus()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(STATUS$4) != 0;
            }
        }
        
        /**
         * Sets the "Status" element
         */
        public void setStatus(java.lang.String status)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STATUS$4, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(STATUS$4);
                }
                target.setStringValue(status);
            }
        }
        
        /**
         * Sets (as xml) the "Status" element
         */
        public void xsetStatus(com.idanalytics.products.compliance.result.StatusDocument.Status status)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.compliance.result.StatusDocument.Status target = null;
                target = (com.idanalytics.products.compliance.result.StatusDocument.Status)get_store().find_element_user(STATUS$4, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.compliance.result.StatusDocument.Status)get_store().add_element_user(STATUS$4);
                }
                target.set(status);
            }
        }
        
        /**
         * Unsets the "Status" element
         */
        public void unsetStatus()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(STATUS$4, 0);
            }
        }
        
        /**
         * Gets the "name" attribute
         */
        public java.lang.String getName()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(NAME$6);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "name" attribute
         */
        public org.apache.xmlbeans.XmlString xgetName()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_attribute_user(NAME$6);
                return target;
            }
        }
        
        /**
         * Sets the "name" attribute
         */
        public void setName(java.lang.String name)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(NAME$6);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(NAME$6);
                }
                target.setStringValue(name);
            }
        }
        
        /**
         * Sets (as xml) the "name" attribute
         */
        public void xsetName(org.apache.xmlbeans.XmlString name)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_attribute_user(NAME$6);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlString)get_store().add_attribute_user(NAME$6);
                }
                target.set(name);
            }
        }
    }
}
