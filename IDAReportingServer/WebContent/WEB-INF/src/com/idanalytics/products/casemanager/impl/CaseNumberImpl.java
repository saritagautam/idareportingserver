/*
 * XML Type:  CaseNumber
 * Namespace: http://idanalytics.com/products/casemanager
 * Java type: com.idanalytics.products.casemanager.CaseNumber
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.casemanager.impl;
/**
 * An XML CaseNumber(@http://idanalytics.com/products/casemanager).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.casemanager.CaseNumber.
 */
public class CaseNumberImpl extends org.apache.xmlbeans.impl.values.JavaIntHolderEx implements com.idanalytics.products.casemanager.CaseNumber
{
    private static final long serialVersionUID = 1L;
    
    public CaseNumberImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected CaseNumberImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
