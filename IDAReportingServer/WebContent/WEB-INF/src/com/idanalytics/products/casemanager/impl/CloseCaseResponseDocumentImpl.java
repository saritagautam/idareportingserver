/*
 * An XML document type.
 * Localname: CloseCaseResponse
 * Namespace: http://idanalytics.com/products/casemanager
 * Java type: com.idanalytics.products.casemanager.CloseCaseResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.casemanager.impl;
/**
 * A document containing one CloseCaseResponse(@http://idanalytics.com/products/casemanager) element.
 *
 * This is a complex type.
 */
public class CloseCaseResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.casemanager.CloseCaseResponseDocument
{
    private static final long serialVersionUID = 1L;
    
    public CloseCaseResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CLOSECASERESPONSE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/casemanager", "CloseCaseResponse");
    
    
    /**
     * Gets the "CloseCaseResponse" element
     */
    public com.idanalytics.products.casemanager.CloseCaseResponseDocument.CloseCaseResponse getCloseCaseResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.casemanager.CloseCaseResponseDocument.CloseCaseResponse target = null;
            target = (com.idanalytics.products.casemanager.CloseCaseResponseDocument.CloseCaseResponse)get_store().find_element_user(CLOSECASERESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "CloseCaseResponse" element
     */
    public void setCloseCaseResponse(com.idanalytics.products.casemanager.CloseCaseResponseDocument.CloseCaseResponse closeCaseResponse)
    {
        generatedSetterHelperImpl(closeCaseResponse, CLOSECASERESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "CloseCaseResponse" element
     */
    public com.idanalytics.products.casemanager.CloseCaseResponseDocument.CloseCaseResponse addNewCloseCaseResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.casemanager.CloseCaseResponseDocument.CloseCaseResponse target = null;
            target = (com.idanalytics.products.casemanager.CloseCaseResponseDocument.CloseCaseResponse)get_store().add_element_user(CLOSECASERESPONSE$0);
            return target;
        }
    }
    /**
     * An XML CloseCaseResponse(@http://idanalytics.com/products/casemanager).
     *
     * This is a complex type.
     */
    public static class CloseCaseResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.casemanager.CloseCaseResponseDocument.CloseCaseResponse
    {
        private static final long serialVersionUID = 1L;
        
        public CloseCaseResponseImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName RESULT$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/casemanager", "Result");
        
        
        /**
         * Gets the "Result" element
         */
        public com.idanalytics.products.casemanager.ResultDocument.Result.Enum getResult()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RESULT$0, 0);
                if (target == null)
                {
                    return null;
                }
                return (com.idanalytics.products.casemanager.ResultDocument.Result.Enum)target.getEnumValue();
            }
        }
        
        /**
         * Gets (as xml) the "Result" element
         */
        public com.idanalytics.products.casemanager.ResultDocument.Result xgetResult()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.casemanager.ResultDocument.Result target = null;
                target = (com.idanalytics.products.casemanager.ResultDocument.Result)get_store().find_element_user(RESULT$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "Result" element
         */
        public void setResult(com.idanalytics.products.casemanager.ResultDocument.Result.Enum result)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RESULT$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(RESULT$0);
                }
                target.setEnumValue(result);
            }
        }
        
        /**
         * Sets (as xml) the "Result" element
         */
        public void xsetResult(com.idanalytics.products.casemanager.ResultDocument.Result result)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.casemanager.ResultDocument.Result target = null;
                target = (com.idanalytics.products.casemanager.ResultDocument.Result)get_store().find_element_user(RESULT$0, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.casemanager.ResultDocument.Result)get_store().add_element_user(RESULT$0);
                }
                target.set(result);
            }
        }
    }
}
