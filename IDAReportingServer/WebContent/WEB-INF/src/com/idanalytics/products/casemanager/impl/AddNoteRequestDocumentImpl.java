/*
 * An XML document type.
 * Localname: AddNoteRequest
 * Namespace: http://idanalytics.com/products/casemanager
 * Java type: com.idanalytics.products.casemanager.AddNoteRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.casemanager.impl;
/**
 * A document containing one AddNoteRequest(@http://idanalytics.com/products/casemanager) element.
 *
 * This is a complex type.
 */
public class AddNoteRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.casemanager.AddNoteRequestDocument
{
    private static final long serialVersionUID = 1L;
    
    public AddNoteRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ADDNOTEREQUEST$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/casemanager", "AddNoteRequest");
    
    
    /**
     * Gets the "AddNoteRequest" element
     */
    public com.idanalytics.products.casemanager.AddNoteRequestDocument.AddNoteRequest getAddNoteRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.casemanager.AddNoteRequestDocument.AddNoteRequest target = null;
            target = (com.idanalytics.products.casemanager.AddNoteRequestDocument.AddNoteRequest)get_store().find_element_user(ADDNOTEREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "AddNoteRequest" element
     */
    public void setAddNoteRequest(com.idanalytics.products.casemanager.AddNoteRequestDocument.AddNoteRequest addNoteRequest)
    {
        generatedSetterHelperImpl(addNoteRequest, ADDNOTEREQUEST$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "AddNoteRequest" element
     */
    public com.idanalytics.products.casemanager.AddNoteRequestDocument.AddNoteRequest addNewAddNoteRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.casemanager.AddNoteRequestDocument.AddNoteRequest target = null;
            target = (com.idanalytics.products.casemanager.AddNoteRequestDocument.AddNoteRequest)get_store().add_element_user(ADDNOTEREQUEST$0);
            return target;
        }
    }
    /**
     * An XML AddNoteRequest(@http://idanalytics.com/products/casemanager).
     *
     * This is a complex type.
     */
    public static class AddNoteRequestImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.casemanager.AddNoteRequestDocument.AddNoteRequest
    {
        private static final long serialVersionUID = 1L;
        
        public AddNoteRequestImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName CASENUMBER$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/casemanager", "CaseNumber");
        private static final javax.xml.namespace.QName NOTE$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/casemanager", "Note");
        
        
        /**
         * Gets the "CaseNumber" element
         */
        public int getCaseNumber()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CASENUMBER$0, 0);
                if (target == null)
                {
                    return 0;
                }
                return target.getIntValue();
            }
        }
        
        /**
         * Gets (as xml) the "CaseNumber" element
         */
        public com.idanalytics.products.casemanager.CaseNumber xgetCaseNumber()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.casemanager.CaseNumber target = null;
                target = (com.idanalytics.products.casemanager.CaseNumber)get_store().find_element_user(CASENUMBER$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "CaseNumber" element
         */
        public void setCaseNumber(int caseNumber)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CASENUMBER$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CASENUMBER$0);
                }
                target.setIntValue(caseNumber);
            }
        }
        
        /**
         * Sets (as xml) the "CaseNumber" element
         */
        public void xsetCaseNumber(com.idanalytics.products.casemanager.CaseNumber caseNumber)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.casemanager.CaseNumber target = null;
                target = (com.idanalytics.products.casemanager.CaseNumber)get_store().find_element_user(CASENUMBER$0, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.casemanager.CaseNumber)get_store().add_element_user(CASENUMBER$0);
                }
                target.set(caseNumber);
            }
        }
        
        /**
         * Gets the "Note" element
         */
        public java.lang.String getNote()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NOTE$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "Note" element
         */
        public com.idanalytics.products.casemanager.Note xgetNote()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.casemanager.Note target = null;
                target = (com.idanalytics.products.casemanager.Note)get_store().find_element_user(NOTE$2, 0);
                return target;
            }
        }
        
        /**
         * Sets the "Note" element
         */
        public void setNote(java.lang.String note)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NOTE$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(NOTE$2);
                }
                target.setStringValue(note);
            }
        }
        
        /**
         * Sets (as xml) the "Note" element
         */
        public void xsetNote(com.idanalytics.products.casemanager.Note note)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.casemanager.Note target = null;
                target = (com.idanalytics.products.casemanager.Note)get_store().find_element_user(NOTE$2, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.casemanager.Note)get_store().add_element_user(NOTE$2);
                }
                target.set(note);
            }
        }
    }
}
