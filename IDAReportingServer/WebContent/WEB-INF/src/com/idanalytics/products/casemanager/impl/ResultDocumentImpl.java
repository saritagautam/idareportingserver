/*
 * An XML document type.
 * Localname: Result
 * Namespace: http://idanalytics.com/products/casemanager
 * Java type: com.idanalytics.products.casemanager.ResultDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.casemanager.impl;
/**
 * A document containing one Result(@http://idanalytics.com/products/casemanager) element.
 *
 * This is a complex type.
 */
public class ResultDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.casemanager.ResultDocument
{
    private static final long serialVersionUID = 1L;
    
    public ResultDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName RESULT$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/casemanager", "Result");
    
    
    /**
     * Gets the "Result" element
     */
    public com.idanalytics.products.casemanager.ResultDocument.Result.Enum getResult()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RESULT$0, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.casemanager.ResultDocument.Result.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "Result" element
     */
    public com.idanalytics.products.casemanager.ResultDocument.Result xgetResult()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.casemanager.ResultDocument.Result target = null;
            target = (com.idanalytics.products.casemanager.ResultDocument.Result)get_store().find_element_user(RESULT$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Result" element
     */
    public void setResult(com.idanalytics.products.casemanager.ResultDocument.Result.Enum result)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RESULT$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(RESULT$0);
            }
            target.setEnumValue(result);
        }
    }
    
    /**
     * Sets (as xml) the "Result" element
     */
    public void xsetResult(com.idanalytics.products.casemanager.ResultDocument.Result result)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.casemanager.ResultDocument.Result target = null;
            target = (com.idanalytics.products.casemanager.ResultDocument.Result)get_store().find_element_user(RESULT$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.casemanager.ResultDocument.Result)get_store().add_element_user(RESULT$0);
            }
            target.set(result);
        }
    }
    /**
     * An XML Result(@http://idanalytics.com/products/casemanager).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.casemanager.ResultDocument$Result.
     */
    public static class ResultImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.idanalytics.products.casemanager.ResultDocument.Result
    {
        private static final long serialVersionUID = 1L;
        
        public ResultImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected ResultImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
