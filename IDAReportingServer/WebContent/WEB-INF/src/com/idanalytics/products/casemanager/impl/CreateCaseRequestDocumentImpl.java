/*
 * An XML document type.
 * Localname: CreateCaseRequest
 * Namespace: http://idanalytics.com/products/casemanager
 * Java type: com.idanalytics.products.casemanager.CreateCaseRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.casemanager.impl;
/**
 * A document containing one CreateCaseRequest(@http://idanalytics.com/products/casemanager) element.
 *
 * This is a complex type.
 */
public class CreateCaseRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.casemanager.CreateCaseRequestDocument
{
    private static final long serialVersionUID = 1L;
    
    public CreateCaseRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CREATECASEREQUEST$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/casemanager", "CreateCaseRequest");
    
    
    /**
     * Gets the "CreateCaseRequest" element
     */
    public com.idanalytics.products.casemanager.CreateCaseRequestDocument.CreateCaseRequest getCreateCaseRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.casemanager.CreateCaseRequestDocument.CreateCaseRequest target = null;
            target = (com.idanalytics.products.casemanager.CreateCaseRequestDocument.CreateCaseRequest)get_store().find_element_user(CREATECASEREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "CreateCaseRequest" element
     */
    public void setCreateCaseRequest(com.idanalytics.products.casemanager.CreateCaseRequestDocument.CreateCaseRequest createCaseRequest)
    {
        generatedSetterHelperImpl(createCaseRequest, CREATECASEREQUEST$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "CreateCaseRequest" element
     */
    public com.idanalytics.products.casemanager.CreateCaseRequestDocument.CreateCaseRequest addNewCreateCaseRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.casemanager.CreateCaseRequestDocument.CreateCaseRequest target = null;
            target = (com.idanalytics.products.casemanager.CreateCaseRequestDocument.CreateCaseRequest)get_store().add_element_user(CREATECASEREQUEST$0);
            return target;
        }
    }
    /**
     * An XML CreateCaseRequest(@http://idanalytics.com/products/casemanager).
     *
     * This is a complex type.
     */
    public static class CreateCaseRequestImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.casemanager.CreateCaseRequestDocument.CreateCaseRequest
    {
        private static final long serialVersionUID = 1L;
        
        public CreateCaseRequestImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName EVENTID$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/casemanager", "EventID");
        private static final javax.xml.namespace.QName CONSUMERID$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/casemanager", "ConsumerID");
        private static final javax.xml.namespace.QName PARTNERID$4 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/casemanager", "PartnerID");
        private static final javax.xml.namespace.QName NOTE$6 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/casemanager", "Note");
        
        
        /**
         * Gets the "EventID" element
         */
        public java.lang.String getEventID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EVENTID$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "EventID" element
         */
        public com.idanalytics.products.casemanager.String30 xgetEventID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.casemanager.String30 target = null;
                target = (com.idanalytics.products.casemanager.String30)get_store().find_element_user(EVENTID$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "EventID" element
         */
        public void setEventID(java.lang.String eventID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EVENTID$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(EVENTID$0);
                }
                target.setStringValue(eventID);
            }
        }
        
        /**
         * Sets (as xml) the "EventID" element
         */
        public void xsetEventID(com.idanalytics.products.casemanager.String30 eventID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.casemanager.String30 target = null;
                target = (com.idanalytics.products.casemanager.String30)get_store().find_element_user(EVENTID$0, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.casemanager.String30)get_store().add_element_user(EVENTID$0);
                }
                target.set(eventID);
            }
        }
        
        /**
         * Gets the "ConsumerID" element
         */
        public java.lang.String getConsumerID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CONSUMERID$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "ConsumerID" element
         */
        public com.idanalytics.products.casemanager.ConsumerIDDocument.ConsumerID xgetConsumerID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.casemanager.ConsumerIDDocument.ConsumerID target = null;
                target = (com.idanalytics.products.casemanager.ConsumerIDDocument.ConsumerID)get_store().find_element_user(CONSUMERID$2, 0);
                return target;
            }
        }
        
        /**
         * Sets the "ConsumerID" element
         */
        public void setConsumerID(java.lang.String consumerID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CONSUMERID$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CONSUMERID$2);
                }
                target.setStringValue(consumerID);
            }
        }
        
        /**
         * Sets (as xml) the "ConsumerID" element
         */
        public void xsetConsumerID(com.idanalytics.products.casemanager.ConsumerIDDocument.ConsumerID consumerID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.casemanager.ConsumerIDDocument.ConsumerID target = null;
                target = (com.idanalytics.products.casemanager.ConsumerIDDocument.ConsumerID)get_store().find_element_user(CONSUMERID$2, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.casemanager.ConsumerIDDocument.ConsumerID)get_store().add_element_user(CONSUMERID$2);
                }
                target.set(consumerID);
            }
        }
        
        /**
         * Gets the "PartnerID" element
         */
        public java.lang.String getPartnerID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PARTNERID$4, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PartnerID" element
         */
        public com.idanalytics.products.casemanager.PartnerID xgetPartnerID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.casemanager.PartnerID target = null;
                target = (com.idanalytics.products.casemanager.PartnerID)get_store().find_element_user(PARTNERID$4, 0);
                return target;
            }
        }
        
        /**
         * True if has "PartnerID" element
         */
        public boolean isSetPartnerID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PARTNERID$4) != 0;
            }
        }
        
        /**
         * Sets the "PartnerID" element
         */
        public void setPartnerID(java.lang.String partnerID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PARTNERID$4, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PARTNERID$4);
                }
                target.setStringValue(partnerID);
            }
        }
        
        /**
         * Sets (as xml) the "PartnerID" element
         */
        public void xsetPartnerID(com.idanalytics.products.casemanager.PartnerID partnerID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.casemanager.PartnerID target = null;
                target = (com.idanalytics.products.casemanager.PartnerID)get_store().find_element_user(PARTNERID$4, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.casemanager.PartnerID)get_store().add_element_user(PARTNERID$4);
                }
                target.set(partnerID);
            }
        }
        
        /**
         * Unsets the "PartnerID" element
         */
        public void unsetPartnerID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PARTNERID$4, 0);
            }
        }
        
        /**
         * Gets the "Note" element
         */
        public java.lang.String getNote()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NOTE$6, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "Note" element
         */
        public com.idanalytics.products.casemanager.Note xgetNote()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.casemanager.Note target = null;
                target = (com.idanalytics.products.casemanager.Note)get_store().find_element_user(NOTE$6, 0);
                return target;
            }
        }
        
        /**
         * True if has "Note" element
         */
        public boolean isSetNote()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(NOTE$6) != 0;
            }
        }
        
        /**
         * Sets the "Note" element
         */
        public void setNote(java.lang.String note)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NOTE$6, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(NOTE$6);
                }
                target.setStringValue(note);
            }
        }
        
        /**
         * Sets (as xml) the "Note" element
         */
        public void xsetNote(com.idanalytics.products.casemanager.Note note)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.casemanager.Note target = null;
                target = (com.idanalytics.products.casemanager.Note)get_store().find_element_user(NOTE$6, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.casemanager.Note)get_store().add_element_user(NOTE$6);
                }
                target.set(note);
            }
        }
        
        /**
         * Unsets the "Note" element
         */
        public void unsetNote()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(NOTE$6, 0);
            }
        }
    }
}
