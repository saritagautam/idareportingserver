/*
 * An XML document type.
 * Localname: GetResolvedCasesRequest
 * Namespace: http://idanalytics.com/products/casemanager
 * Java type: com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.casemanager;


/**
 * A document containing one GetResolvedCasesRequest(@http://idanalytics.com/products/casemanager) element.
 *
 * This is a complex type.
 */
public interface GetResolvedCasesRequestDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetResolvedCasesRequestDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("getresolvedcasesrequestd6d6doctype");
    
    /**
     * Gets the "GetResolvedCasesRequest" element
     */
    com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument.GetResolvedCasesRequest getGetResolvedCasesRequest();
    
    /**
     * Sets the "GetResolvedCasesRequest" element
     */
    void setGetResolvedCasesRequest(com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument.GetResolvedCasesRequest getResolvedCasesRequest);
    
    /**
     * Appends and returns a new empty "GetResolvedCasesRequest" element
     */
    com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument.GetResolvedCasesRequest addNewGetResolvedCasesRequest();
    
    /**
     * An XML GetResolvedCasesRequest(@http://idanalytics.com/products/casemanager).
     *
     * This is a complex type.
     */
    public interface GetResolvedCasesRequest extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetResolvedCasesRequest.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("getresolvedcasesrequestccfcelemtype");
        
        /**
         * Gets the "BeginDate" element
         */
        java.util.Calendar getBeginDate();
        
        /**
         * Gets (as xml) the "BeginDate" element
         */
        org.apache.xmlbeans.XmlDateTime xgetBeginDate();
        
        /**
         * Sets the "BeginDate" element
         */
        void setBeginDate(java.util.Calendar beginDate);
        
        /**
         * Sets (as xml) the "BeginDate" element
         */
        void xsetBeginDate(org.apache.xmlbeans.XmlDateTime beginDate);
        
        /**
         * Gets the "EndDate" element
         */
        java.util.Calendar getEndDate();
        
        /**
         * Gets (as xml) the "EndDate" element
         */
        org.apache.xmlbeans.XmlDateTime xgetEndDate();
        
        /**
         * True if has "EndDate" element
         */
        boolean isSetEndDate();
        
        /**
         * Sets the "EndDate" element
         */
        void setEndDate(java.util.Calendar endDate);
        
        /**
         * Sets (as xml) the "EndDate" element
         */
        void xsetEndDate(org.apache.xmlbeans.XmlDateTime endDate);
        
        /**
         * Unsets the "EndDate" element
         */
        void unsetEndDate();
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument.GetResolvedCasesRequest newInstance() {
              return (com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument.GetResolvedCasesRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument.GetResolvedCasesRequest newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument.GetResolvedCasesRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument newInstance() {
          return (com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
