/*
 * An XML document type.
 * Localname: CloseCaseRequest
 * Namespace: http://idanalytics.com/products/casemanager
 * Java type: com.idanalytics.products.casemanager.CloseCaseRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.casemanager.impl;
/**
 * A document containing one CloseCaseRequest(@http://idanalytics.com/products/casemanager) element.
 *
 * This is a complex type.
 */
public class CloseCaseRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.casemanager.CloseCaseRequestDocument
{
    private static final long serialVersionUID = 1L;
    
    public CloseCaseRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CLOSECASEREQUEST$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/casemanager", "CloseCaseRequest");
    
    
    /**
     * Gets the "CloseCaseRequest" element
     */
    public com.idanalytics.products.casemanager.CloseCaseRequestDocument.CloseCaseRequest getCloseCaseRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.casemanager.CloseCaseRequestDocument.CloseCaseRequest target = null;
            target = (com.idanalytics.products.casemanager.CloseCaseRequestDocument.CloseCaseRequest)get_store().find_element_user(CLOSECASEREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "CloseCaseRequest" element
     */
    public void setCloseCaseRequest(com.idanalytics.products.casemanager.CloseCaseRequestDocument.CloseCaseRequest closeCaseRequest)
    {
        generatedSetterHelperImpl(closeCaseRequest, CLOSECASEREQUEST$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "CloseCaseRequest" element
     */
    public com.idanalytics.products.casemanager.CloseCaseRequestDocument.CloseCaseRequest addNewCloseCaseRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.casemanager.CloseCaseRequestDocument.CloseCaseRequest target = null;
            target = (com.idanalytics.products.casemanager.CloseCaseRequestDocument.CloseCaseRequest)get_store().add_element_user(CLOSECASEREQUEST$0);
            return target;
        }
    }
    /**
     * An XML CloseCaseRequest(@http://idanalytics.com/products/casemanager).
     *
     * This is a complex type.
     */
    public static class CloseCaseRequestImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.casemanager.CloseCaseRequestDocument.CloseCaseRequest
    {
        private static final long serialVersionUID = 1L;
        
        public CloseCaseRequestImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName CASENUMBER$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/casemanager", "CaseNumber");
        
        
        /**
         * Gets the "CaseNumber" element
         */
        public int getCaseNumber()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CASENUMBER$0, 0);
                if (target == null)
                {
                    return 0;
                }
                return target.getIntValue();
            }
        }
        
        /**
         * Gets (as xml) the "CaseNumber" element
         */
        public com.idanalytics.products.casemanager.CaseNumber xgetCaseNumber()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.casemanager.CaseNumber target = null;
                target = (com.idanalytics.products.casemanager.CaseNumber)get_store().find_element_user(CASENUMBER$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "CaseNumber" element
         */
        public void setCaseNumber(int caseNumber)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CASENUMBER$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CASENUMBER$0);
                }
                target.setIntValue(caseNumber);
            }
        }
        
        /**
         * Sets (as xml) the "CaseNumber" element
         */
        public void xsetCaseNumber(com.idanalytics.products.casemanager.CaseNumber caseNumber)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.casemanager.CaseNumber target = null;
                target = (com.idanalytics.products.casemanager.CaseNumber)get_store().find_element_user(CASENUMBER$0, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.casemanager.CaseNumber)get_store().add_element_user(CASENUMBER$0);
                }
                target.set(caseNumber);
            }
        }
    }
}
