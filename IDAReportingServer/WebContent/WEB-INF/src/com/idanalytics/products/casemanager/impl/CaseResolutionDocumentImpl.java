/*
 * An XML document type.
 * Localname: CaseResolution
 * Namespace: http://idanalytics.com/products/casemanager
 * Java type: com.idanalytics.products.casemanager.CaseResolutionDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.casemanager.impl;
/**
 * A document containing one CaseResolution(@http://idanalytics.com/products/casemanager) element.
 *
 * This is a complex type.
 */
public class CaseResolutionDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.casemanager.CaseResolutionDocument
{
    private static final long serialVersionUID = 1L;
    
    public CaseResolutionDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CASERESOLUTION$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/casemanager", "CaseResolution");
    
    
    /**
     * Gets the "CaseResolution" element
     */
    public com.idanalytics.products.casemanager.CaseResolutionDocument.CaseResolution getCaseResolution()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.casemanager.CaseResolutionDocument.CaseResolution target = null;
            target = (com.idanalytics.products.casemanager.CaseResolutionDocument.CaseResolution)get_store().find_element_user(CASERESOLUTION$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "CaseResolution" element
     */
    public void setCaseResolution(com.idanalytics.products.casemanager.CaseResolutionDocument.CaseResolution caseResolution)
    {
        generatedSetterHelperImpl(caseResolution, CASERESOLUTION$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "CaseResolution" element
     */
    public com.idanalytics.products.casemanager.CaseResolutionDocument.CaseResolution addNewCaseResolution()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.casemanager.CaseResolutionDocument.CaseResolution target = null;
            target = (com.idanalytics.products.casemanager.CaseResolutionDocument.CaseResolution)get_store().add_element_user(CASERESOLUTION$0);
            return target;
        }
    }
    /**
     * An XML CaseResolution(@http://idanalytics.com/products/casemanager).
     *
     * This is a complex type.
     */
    public static class CaseResolutionImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.casemanager.CaseResolutionDocument.CaseResolution
    {
        private static final long serialVersionUID = 1L;
        
        public CaseResolutionImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName CASENUMBER$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/casemanager", "CaseNumber");
        private static final javax.xml.namespace.QName EVENTID$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/casemanager", "EventID");
        private static final javax.xml.namespace.QName PARTNERID$4 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/casemanager", "PartnerID");
        private static final javax.xml.namespace.QName DATECREATED$6 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/casemanager", "DateCreated");
        private static final javax.xml.namespace.QName DATECLOSED$8 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/casemanager", "DateClosed");
        private static final javax.xml.namespace.QName RESOLUTION$10 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/casemanager", "Resolution");
        
        
        /**
         * Gets the "CaseNumber" element
         */
        public int getCaseNumber()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CASENUMBER$0, 0);
                if (target == null)
                {
                    return 0;
                }
                return target.getIntValue();
            }
        }
        
        /**
         * Gets (as xml) the "CaseNumber" element
         */
        public com.idanalytics.products.casemanager.CaseNumber xgetCaseNumber()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.casemanager.CaseNumber target = null;
                target = (com.idanalytics.products.casemanager.CaseNumber)get_store().find_element_user(CASENUMBER$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "CaseNumber" element
         */
        public void setCaseNumber(int caseNumber)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CASENUMBER$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CASENUMBER$0);
                }
                target.setIntValue(caseNumber);
            }
        }
        
        /**
         * Sets (as xml) the "CaseNumber" element
         */
        public void xsetCaseNumber(com.idanalytics.products.casemanager.CaseNumber caseNumber)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.casemanager.CaseNumber target = null;
                target = (com.idanalytics.products.casemanager.CaseNumber)get_store().find_element_user(CASENUMBER$0, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.casemanager.CaseNumber)get_store().add_element_user(CASENUMBER$0);
                }
                target.set(caseNumber);
            }
        }
        
        /**
         * Gets the "EventID" element
         */
        public java.lang.String getEventID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EVENTID$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "EventID" element
         */
        public com.idanalytics.products.casemanager.String30 xgetEventID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.casemanager.String30 target = null;
                target = (com.idanalytics.products.casemanager.String30)get_store().find_element_user(EVENTID$2, 0);
                return target;
            }
        }
        
        /**
         * Sets the "EventID" element
         */
        public void setEventID(java.lang.String eventID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EVENTID$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(EVENTID$2);
                }
                target.setStringValue(eventID);
            }
        }
        
        /**
         * Sets (as xml) the "EventID" element
         */
        public void xsetEventID(com.idanalytics.products.casemanager.String30 eventID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.casemanager.String30 target = null;
                target = (com.idanalytics.products.casemanager.String30)get_store().find_element_user(EVENTID$2, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.casemanager.String30)get_store().add_element_user(EVENTID$2);
                }
                target.set(eventID);
            }
        }
        
        /**
         * Gets the "PartnerID" element
         */
        public java.lang.String getPartnerID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PARTNERID$4, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PartnerID" element
         */
        public com.idanalytics.products.casemanager.PartnerID xgetPartnerID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.casemanager.PartnerID target = null;
                target = (com.idanalytics.products.casemanager.PartnerID)get_store().find_element_user(PARTNERID$4, 0);
                return target;
            }
        }
        
        /**
         * True if has "PartnerID" element
         */
        public boolean isSetPartnerID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PARTNERID$4) != 0;
            }
        }
        
        /**
         * Sets the "PartnerID" element
         */
        public void setPartnerID(java.lang.String partnerID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PARTNERID$4, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PARTNERID$4);
                }
                target.setStringValue(partnerID);
            }
        }
        
        /**
         * Sets (as xml) the "PartnerID" element
         */
        public void xsetPartnerID(com.idanalytics.products.casemanager.PartnerID partnerID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.casemanager.PartnerID target = null;
                target = (com.idanalytics.products.casemanager.PartnerID)get_store().find_element_user(PARTNERID$4, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.casemanager.PartnerID)get_store().add_element_user(PARTNERID$4);
                }
                target.set(partnerID);
            }
        }
        
        /**
         * Unsets the "PartnerID" element
         */
        public void unsetPartnerID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PARTNERID$4, 0);
            }
        }
        
        /**
         * Gets the "DateCreated" element
         */
        public java.util.Calendar getDateCreated()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DATECREATED$6, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getCalendarValue();
            }
        }
        
        /**
         * Gets (as xml) the "DateCreated" element
         */
        public org.apache.xmlbeans.XmlDateTime xgetDateCreated()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlDateTime target = null;
                target = (org.apache.xmlbeans.XmlDateTime)get_store().find_element_user(DATECREATED$6, 0);
                return target;
            }
        }
        
        /**
         * Sets the "DateCreated" element
         */
        public void setDateCreated(java.util.Calendar dateCreated)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DATECREATED$6, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DATECREATED$6);
                }
                target.setCalendarValue(dateCreated);
            }
        }
        
        /**
         * Sets (as xml) the "DateCreated" element
         */
        public void xsetDateCreated(org.apache.xmlbeans.XmlDateTime dateCreated)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlDateTime target = null;
                target = (org.apache.xmlbeans.XmlDateTime)get_store().find_element_user(DATECREATED$6, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlDateTime)get_store().add_element_user(DATECREATED$6);
                }
                target.set(dateCreated);
            }
        }
        
        /**
         * Gets the "DateClosed" element
         */
        public java.util.Calendar getDateClosed()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DATECLOSED$8, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getCalendarValue();
            }
        }
        
        /**
         * Gets (as xml) the "DateClosed" element
         */
        public org.apache.xmlbeans.XmlDateTime xgetDateClosed()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlDateTime target = null;
                target = (org.apache.xmlbeans.XmlDateTime)get_store().find_element_user(DATECLOSED$8, 0);
                return target;
            }
        }
        
        /**
         * True if has "DateClosed" element
         */
        public boolean isSetDateClosed()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(DATECLOSED$8) != 0;
            }
        }
        
        /**
         * Sets the "DateClosed" element
         */
        public void setDateClosed(java.util.Calendar dateClosed)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DATECLOSED$8, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DATECLOSED$8);
                }
                target.setCalendarValue(dateClosed);
            }
        }
        
        /**
         * Sets (as xml) the "DateClosed" element
         */
        public void xsetDateClosed(org.apache.xmlbeans.XmlDateTime dateClosed)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlDateTime target = null;
                target = (org.apache.xmlbeans.XmlDateTime)get_store().find_element_user(DATECLOSED$8, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlDateTime)get_store().add_element_user(DATECLOSED$8);
                }
                target.set(dateClosed);
            }
        }
        
        /**
         * Unsets the "DateClosed" element
         */
        public void unsetDateClosed()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(DATECLOSED$8, 0);
            }
        }
        
        /**
         * Gets the "Resolution" element
         */
        public com.idanalytics.products.casemanager.ResolutionDocument.Resolution getResolution()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.casemanager.ResolutionDocument.Resolution target = null;
                target = (com.idanalytics.products.casemanager.ResolutionDocument.Resolution)get_store().find_element_user(RESOLUTION$10, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * Sets the "Resolution" element
         */
        public void setResolution(com.idanalytics.products.casemanager.ResolutionDocument.Resolution resolution)
        {
            generatedSetterHelperImpl(resolution, RESOLUTION$10, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "Resolution" element
         */
        public com.idanalytics.products.casemanager.ResolutionDocument.Resolution addNewResolution()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.casemanager.ResolutionDocument.Resolution target = null;
                target = (com.idanalytics.products.casemanager.ResolutionDocument.Resolution)get_store().add_element_user(RESOLUTION$10);
                return target;
            }
        }
    }
}
