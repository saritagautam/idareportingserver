/*
 * XML Type:  Note
 * Namespace: http://idanalytics.com/products/casemanager
 * Java type: com.idanalytics.products.casemanager.Note
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.casemanager.impl;
/**
 * An XML Note(@http://idanalytics.com/products/casemanager).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.casemanager.Note.
 */
public class NoteImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.casemanager.Note
{
    private static final long serialVersionUID = 1L;
    
    public NoteImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected NoteImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
