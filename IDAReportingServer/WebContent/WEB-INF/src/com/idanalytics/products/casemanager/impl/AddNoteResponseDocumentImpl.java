/*
 * An XML document type.
 * Localname: AddNoteResponse
 * Namespace: http://idanalytics.com/products/casemanager
 * Java type: com.idanalytics.products.casemanager.AddNoteResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.casemanager.impl;
/**
 * A document containing one AddNoteResponse(@http://idanalytics.com/products/casemanager) element.
 *
 * This is a complex type.
 */
public class AddNoteResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.casemanager.AddNoteResponseDocument
{
    private static final long serialVersionUID = 1L;
    
    public AddNoteResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ADDNOTERESPONSE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/casemanager", "AddNoteResponse");
    
    
    /**
     * Gets the "AddNoteResponse" element
     */
    public com.idanalytics.products.casemanager.AddNoteResponseDocument.AddNoteResponse getAddNoteResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.casemanager.AddNoteResponseDocument.AddNoteResponse target = null;
            target = (com.idanalytics.products.casemanager.AddNoteResponseDocument.AddNoteResponse)get_store().find_element_user(ADDNOTERESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "AddNoteResponse" element
     */
    public void setAddNoteResponse(com.idanalytics.products.casemanager.AddNoteResponseDocument.AddNoteResponse addNoteResponse)
    {
        generatedSetterHelperImpl(addNoteResponse, ADDNOTERESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "AddNoteResponse" element
     */
    public com.idanalytics.products.casemanager.AddNoteResponseDocument.AddNoteResponse addNewAddNoteResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.casemanager.AddNoteResponseDocument.AddNoteResponse target = null;
            target = (com.idanalytics.products.casemanager.AddNoteResponseDocument.AddNoteResponse)get_store().add_element_user(ADDNOTERESPONSE$0);
            return target;
        }
    }
    /**
     * An XML AddNoteResponse(@http://idanalytics.com/products/casemanager).
     *
     * This is a complex type.
     */
    public static class AddNoteResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.casemanager.AddNoteResponseDocument.AddNoteResponse
    {
        private static final long serialVersionUID = 1L;
        
        public AddNoteResponseImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName RESULT$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/casemanager", "Result");
        
        
        /**
         * Gets the "Result" element
         */
        public com.idanalytics.products.casemanager.ResultDocument.Result.Enum getResult()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RESULT$0, 0);
                if (target == null)
                {
                    return null;
                }
                return (com.idanalytics.products.casemanager.ResultDocument.Result.Enum)target.getEnumValue();
            }
        }
        
        /**
         * Gets (as xml) the "Result" element
         */
        public com.idanalytics.products.casemanager.ResultDocument.Result xgetResult()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.casemanager.ResultDocument.Result target = null;
                target = (com.idanalytics.products.casemanager.ResultDocument.Result)get_store().find_element_user(RESULT$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "Result" element
         */
        public void setResult(com.idanalytics.products.casemanager.ResultDocument.Result.Enum result)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RESULT$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(RESULT$0);
                }
                target.setEnumValue(result);
            }
        }
        
        /**
         * Sets (as xml) the "Result" element
         */
        public void xsetResult(com.idanalytics.products.casemanager.ResultDocument.Result result)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.casemanager.ResultDocument.Result target = null;
                target = (com.idanalytics.products.casemanager.ResultDocument.Result)get_store().find_element_user(RESULT$0, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.casemanager.ResultDocument.Result)get_store().add_element_user(RESULT$0);
                }
                target.set(result);
            }
        }
    }
}
