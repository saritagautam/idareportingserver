/*
 * An XML document type.
 * Localname: GetResolvedCasesResponse
 * Namespace: http://idanalytics.com/products/casemanager
 * Java type: com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.casemanager.impl;
/**
 * A document containing one GetResolvedCasesResponse(@http://idanalytics.com/products/casemanager) element.
 *
 * This is a complex type.
 */
public class GetResolvedCasesResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument
{
    private static final long serialVersionUID = 1L;
    
    public GetResolvedCasesResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETRESOLVEDCASESRESPONSE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/casemanager", "GetResolvedCasesResponse");
    
    
    /**
     * Gets the "GetResolvedCasesResponse" element
     */
    public com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument.GetResolvedCasesResponse getGetResolvedCasesResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument.GetResolvedCasesResponse target = null;
            target = (com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument.GetResolvedCasesResponse)get_store().find_element_user(GETRESOLVEDCASESRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "GetResolvedCasesResponse" element
     */
    public void setGetResolvedCasesResponse(com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument.GetResolvedCasesResponse getResolvedCasesResponse)
    {
        generatedSetterHelperImpl(getResolvedCasesResponse, GETRESOLVEDCASESRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "GetResolvedCasesResponse" element
     */
    public com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument.GetResolvedCasesResponse addNewGetResolvedCasesResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument.GetResolvedCasesResponse target = null;
            target = (com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument.GetResolvedCasesResponse)get_store().add_element_user(GETRESOLVEDCASESRESPONSE$0);
            return target;
        }
    }
    /**
     * An XML GetResolvedCasesResponse(@http://idanalytics.com/products/casemanager).
     *
     * This is a complex type.
     */
    public static class GetResolvedCasesResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument.GetResolvedCasesResponse
    {
        private static final long serialVersionUID = 1L;
        
        public GetResolvedCasesResponseImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName RESULT$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/casemanager", "Result");
        private static final javax.xml.namespace.QName CASERESOLUTION$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/casemanager", "CaseResolution");
        
        
        /**
         * Gets the "Result" element
         */
        public com.idanalytics.products.casemanager.ResultDocument.Result.Enum getResult()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RESULT$0, 0);
                if (target == null)
                {
                    return null;
                }
                return (com.idanalytics.products.casemanager.ResultDocument.Result.Enum)target.getEnumValue();
            }
        }
        
        /**
         * Gets (as xml) the "Result" element
         */
        public com.idanalytics.products.casemanager.ResultDocument.Result xgetResult()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.casemanager.ResultDocument.Result target = null;
                target = (com.idanalytics.products.casemanager.ResultDocument.Result)get_store().find_element_user(RESULT$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "Result" element
         */
        public void setResult(com.idanalytics.products.casemanager.ResultDocument.Result.Enum result)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RESULT$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(RESULT$0);
                }
                target.setEnumValue(result);
            }
        }
        
        /**
         * Sets (as xml) the "Result" element
         */
        public void xsetResult(com.idanalytics.products.casemanager.ResultDocument.Result result)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.casemanager.ResultDocument.Result target = null;
                target = (com.idanalytics.products.casemanager.ResultDocument.Result)get_store().find_element_user(RESULT$0, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.casemanager.ResultDocument.Result)get_store().add_element_user(RESULT$0);
                }
                target.set(result);
            }
        }
        
        /**
         * Gets array of all "CaseResolution" elements
         */
        public com.idanalytics.products.casemanager.CaseResolutionDocument.CaseResolution[] getCaseResolutionArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(CASERESOLUTION$2, targetList);
                com.idanalytics.products.casemanager.CaseResolutionDocument.CaseResolution[] result = new com.idanalytics.products.casemanager.CaseResolutionDocument.CaseResolution[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "CaseResolution" element
         */
        public com.idanalytics.products.casemanager.CaseResolutionDocument.CaseResolution getCaseResolutionArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.casemanager.CaseResolutionDocument.CaseResolution target = null;
                target = (com.idanalytics.products.casemanager.CaseResolutionDocument.CaseResolution)get_store().find_element_user(CASERESOLUTION$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "CaseResolution" element
         */
        public int sizeOfCaseResolutionArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(CASERESOLUTION$2);
            }
        }
        
        /**
         * Sets array of all "CaseResolution" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setCaseResolutionArray(com.idanalytics.products.casemanager.CaseResolutionDocument.CaseResolution[] caseResolutionArray)
        {
            check_orphaned();
            arraySetterHelper(caseResolutionArray, CASERESOLUTION$2);
        }
        
        /**
         * Sets ith "CaseResolution" element
         */
        public void setCaseResolutionArray(int i, com.idanalytics.products.casemanager.CaseResolutionDocument.CaseResolution caseResolution)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.casemanager.CaseResolutionDocument.CaseResolution target = null;
                target = (com.idanalytics.products.casemanager.CaseResolutionDocument.CaseResolution)get_store().find_element_user(CASERESOLUTION$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(caseResolution);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "CaseResolution" element
         */
        public com.idanalytics.products.casemanager.CaseResolutionDocument.CaseResolution insertNewCaseResolution(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.casemanager.CaseResolutionDocument.CaseResolution target = null;
                target = (com.idanalytics.products.casemanager.CaseResolutionDocument.CaseResolution)get_store().insert_element_user(CASERESOLUTION$2, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "CaseResolution" element
         */
        public com.idanalytics.products.casemanager.CaseResolutionDocument.CaseResolution addNewCaseResolution()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.casemanager.CaseResolutionDocument.CaseResolution target = null;
                target = (com.idanalytics.products.casemanager.CaseResolutionDocument.CaseResolution)get_store().add_element_user(CASERESOLUTION$2);
                return target;
            }
        }
        
        /**
         * Removes the ith "CaseResolution" element
         */
        public void removeCaseResolution(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(CASERESOLUTION$2, i);
            }
        }
    }
}
