/*
 * An XML document type.
 * Localname: Result
 * Namespace: http://idanalytics.com/products/casemanager
 * Java type: com.idanalytics.products.casemanager.ResultDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.casemanager;


/**
 * A document containing one Result(@http://idanalytics.com/products/casemanager) element.
 *
 * This is a complex type.
 */
public interface ResultDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ResultDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("result3babdoctype");
    
    /**
     * Gets the "Result" element
     */
    com.idanalytics.products.casemanager.ResultDocument.Result.Enum getResult();
    
    /**
     * Gets (as xml) the "Result" element
     */
    com.idanalytics.products.casemanager.ResultDocument.Result xgetResult();
    
    /**
     * Sets the "Result" element
     */
    void setResult(com.idanalytics.products.casemanager.ResultDocument.Result.Enum result);
    
    /**
     * Sets (as xml) the "Result" element
     */
    void xsetResult(com.idanalytics.products.casemanager.ResultDocument.Result result);
    
    /**
     * An XML Result(@http://idanalytics.com/products/casemanager).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.casemanager.ResultDocument$Result.
     */
    public interface Result extends org.apache.xmlbeans.XmlString
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Result.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("resulte672elemtype");
        
        org.apache.xmlbeans.StringEnumAbstractBase enumValue();
        void set(org.apache.xmlbeans.StringEnumAbstractBase e);
        
        static final Enum SUCCESS = Enum.forString("SUCCESS");
        static final Enum NO_MATCH_FOR_CASE_NUMBER = Enum.forString("NO_MATCH_FOR_CASE_NUMBER");
        static final Enum NO_MATCH_FOR_EVENT_ID = Enum.forString("NO_MATCH_FOR_EVENT_ID");
        static final Enum CONSUMER_ID_NOT_CORRELATED_TO_EVENT_ID = Enum.forString("CONSUMER_ID_NOT_CORRELATED_TO_EVENT_ID");
        static final Enum OPERATION_NOT_ALLOWED_ON_CLOSED_CASE = Enum.forString("OPERATION_NOT_ALLOWED_ON_CLOSED_CASE");
        
        static final int INT_SUCCESS = Enum.INT_SUCCESS;
        static final int INT_NO_MATCH_FOR_CASE_NUMBER = Enum.INT_NO_MATCH_FOR_CASE_NUMBER;
        static final int INT_NO_MATCH_FOR_EVENT_ID = Enum.INT_NO_MATCH_FOR_EVENT_ID;
        static final int INT_CONSUMER_ID_NOT_CORRELATED_TO_EVENT_ID = Enum.INT_CONSUMER_ID_NOT_CORRELATED_TO_EVENT_ID;
        static final int INT_OPERATION_NOT_ALLOWED_ON_CLOSED_CASE = Enum.INT_OPERATION_NOT_ALLOWED_ON_CLOSED_CASE;
        
        /**
         * Enumeration value class for com.idanalytics.products.casemanager.ResultDocument$Result.
         * These enum values can be used as follows:
         * <pre>
         * enum.toString(); // returns the string value of the enum
         * enum.intValue(); // returns an int value, useful for switches
         * // e.g., case Enum.INT_SUCCESS
         * Enum.forString(s); // returns the enum value for a string
         * Enum.forInt(i); // returns the enum value for an int
         * </pre>
         * Enumeration objects are immutable singleton objects that
         * can be compared using == object equality. They have no
         * public constructor. See the constants defined within this
         * class for all the valid values.
         */
        static final class Enum extends org.apache.xmlbeans.StringEnumAbstractBase
        {
            /**
             * Returns the enum value for a string, or null if none.
             */
            public static Enum forString(java.lang.String s)
                { return (Enum)table.forString(s); }
            /**
             * Returns the enum value corresponding to an int, or null if none.
             */
            public static Enum forInt(int i)
                { return (Enum)table.forInt(i); }
            
            private Enum(java.lang.String s, int i)
                { super(s, i); }
            
            static final int INT_SUCCESS = 1;
            static final int INT_NO_MATCH_FOR_CASE_NUMBER = 2;
            static final int INT_NO_MATCH_FOR_EVENT_ID = 3;
            static final int INT_CONSUMER_ID_NOT_CORRELATED_TO_EVENT_ID = 4;
            static final int INT_OPERATION_NOT_ALLOWED_ON_CLOSED_CASE = 5;
            
            public static final org.apache.xmlbeans.StringEnumAbstractBase.Table table =
                new org.apache.xmlbeans.StringEnumAbstractBase.Table
            (
                new Enum[]
                {
                    new Enum("SUCCESS", INT_SUCCESS),
                    new Enum("NO_MATCH_FOR_CASE_NUMBER", INT_NO_MATCH_FOR_CASE_NUMBER),
                    new Enum("NO_MATCH_FOR_EVENT_ID", INT_NO_MATCH_FOR_EVENT_ID),
                    new Enum("CONSUMER_ID_NOT_CORRELATED_TO_EVENT_ID", INT_CONSUMER_ID_NOT_CORRELATED_TO_EVENT_ID),
                    new Enum("OPERATION_NOT_ALLOWED_ON_CLOSED_CASE", INT_OPERATION_NOT_ALLOWED_ON_CLOSED_CASE),
                }
            );
            private static final long serialVersionUID = 1L;
            private java.lang.Object readResolve() { return forInt(intValue()); } 
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.casemanager.ResultDocument.Result newValue(java.lang.Object obj) {
              return (com.idanalytics.products.casemanager.ResultDocument.Result) type.newValue( obj ); }
            
            public static com.idanalytics.products.casemanager.ResultDocument.Result newInstance() {
              return (com.idanalytics.products.casemanager.ResultDocument.Result) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.casemanager.ResultDocument.Result newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.casemanager.ResultDocument.Result) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.casemanager.ResultDocument newInstance() {
          return (com.idanalytics.products.casemanager.ResultDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.casemanager.ResultDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.casemanager.ResultDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.casemanager.ResultDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.casemanager.ResultDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.casemanager.ResultDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.casemanager.ResultDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.casemanager.ResultDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.casemanager.ResultDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.casemanager.ResultDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.casemanager.ResultDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.casemanager.ResultDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.casemanager.ResultDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.casemanager.ResultDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.casemanager.ResultDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.casemanager.ResultDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.casemanager.ResultDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.casemanager.ResultDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.casemanager.ResultDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.casemanager.ResultDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.casemanager.ResultDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.casemanager.ResultDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.casemanager.ResultDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.casemanager.ResultDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.casemanager.ResultDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.casemanager.ResultDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.casemanager.ResultDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.casemanager.ResultDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.casemanager.ResultDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.casemanager.ResultDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.casemanager.ResultDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.casemanager.ResultDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.casemanager.ResultDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.casemanager.ResultDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.casemanager.ResultDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
