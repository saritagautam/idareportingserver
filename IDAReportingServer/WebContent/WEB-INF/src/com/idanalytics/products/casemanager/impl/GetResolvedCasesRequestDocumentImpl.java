/*
 * An XML document type.
 * Localname: GetResolvedCasesRequest
 * Namespace: http://idanalytics.com/products/casemanager
 * Java type: com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.casemanager.impl;
/**
 * A document containing one GetResolvedCasesRequest(@http://idanalytics.com/products/casemanager) element.
 *
 * This is a complex type.
 */
public class GetResolvedCasesRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument
{
    private static final long serialVersionUID = 1L;
    
    public GetResolvedCasesRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GETRESOLVEDCASESREQUEST$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/casemanager", "GetResolvedCasesRequest");
    
    
    /**
     * Gets the "GetResolvedCasesRequest" element
     */
    public com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument.GetResolvedCasesRequest getGetResolvedCasesRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument.GetResolvedCasesRequest target = null;
            target = (com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument.GetResolvedCasesRequest)get_store().find_element_user(GETRESOLVEDCASESREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "GetResolvedCasesRequest" element
     */
    public void setGetResolvedCasesRequest(com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument.GetResolvedCasesRequest getResolvedCasesRequest)
    {
        generatedSetterHelperImpl(getResolvedCasesRequest, GETRESOLVEDCASESREQUEST$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "GetResolvedCasesRequest" element
     */
    public com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument.GetResolvedCasesRequest addNewGetResolvedCasesRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument.GetResolvedCasesRequest target = null;
            target = (com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument.GetResolvedCasesRequest)get_store().add_element_user(GETRESOLVEDCASESREQUEST$0);
            return target;
        }
    }
    /**
     * An XML GetResolvedCasesRequest(@http://idanalytics.com/products/casemanager).
     *
     * This is a complex type.
     */
    public static class GetResolvedCasesRequestImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.casemanager.GetResolvedCasesRequestDocument.GetResolvedCasesRequest
    {
        private static final long serialVersionUID = 1L;
        
        public GetResolvedCasesRequestImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName BEGINDATE$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/casemanager", "BeginDate");
        private static final javax.xml.namespace.QName ENDDATE$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/casemanager", "EndDate");
        
        
        /**
         * Gets the "BeginDate" element
         */
        public java.util.Calendar getBeginDate()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(BEGINDATE$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getCalendarValue();
            }
        }
        
        /**
         * Gets (as xml) the "BeginDate" element
         */
        public org.apache.xmlbeans.XmlDateTime xgetBeginDate()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlDateTime target = null;
                target = (org.apache.xmlbeans.XmlDateTime)get_store().find_element_user(BEGINDATE$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "BeginDate" element
         */
        public void setBeginDate(java.util.Calendar beginDate)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(BEGINDATE$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(BEGINDATE$0);
                }
                target.setCalendarValue(beginDate);
            }
        }
        
        /**
         * Sets (as xml) the "BeginDate" element
         */
        public void xsetBeginDate(org.apache.xmlbeans.XmlDateTime beginDate)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlDateTime target = null;
                target = (org.apache.xmlbeans.XmlDateTime)get_store().find_element_user(BEGINDATE$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlDateTime)get_store().add_element_user(BEGINDATE$0);
                }
                target.set(beginDate);
            }
        }
        
        /**
         * Gets the "EndDate" element
         */
        public java.util.Calendar getEndDate()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ENDDATE$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getCalendarValue();
            }
        }
        
        /**
         * Gets (as xml) the "EndDate" element
         */
        public org.apache.xmlbeans.XmlDateTime xgetEndDate()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlDateTime target = null;
                target = (org.apache.xmlbeans.XmlDateTime)get_store().find_element_user(ENDDATE$2, 0);
                return target;
            }
        }
        
        /**
         * True if has "EndDate" element
         */
        public boolean isSetEndDate()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(ENDDATE$2) != 0;
            }
        }
        
        /**
         * Sets the "EndDate" element
         */
        public void setEndDate(java.util.Calendar endDate)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ENDDATE$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ENDDATE$2);
                }
                target.setCalendarValue(endDate);
            }
        }
        
        /**
         * Sets (as xml) the "EndDate" element
         */
        public void xsetEndDate(org.apache.xmlbeans.XmlDateTime endDate)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlDateTime target = null;
                target = (org.apache.xmlbeans.XmlDateTime)get_store().find_element_user(ENDDATE$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlDateTime)get_store().add_element_user(ENDDATE$2);
                }
                target.set(endDate);
            }
        }
        
        /**
         * Unsets the "EndDate" element
         */
        public void unsetEndDate()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(ENDDATE$2, 0);
            }
        }
    }
}
