/*
 * An XML document type.
 * Localname: CreateCaseResponse
 * Namespace: http://idanalytics.com/products/casemanager
 * Java type: com.idanalytics.products.casemanager.CreateCaseResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.casemanager.impl;
/**
 * A document containing one CreateCaseResponse(@http://idanalytics.com/products/casemanager) element.
 *
 * This is a complex type.
 */
public class CreateCaseResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.casemanager.CreateCaseResponseDocument
{
    private static final long serialVersionUID = 1L;
    
    public CreateCaseResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CREATECASERESPONSE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/casemanager", "CreateCaseResponse");
    
    
    /**
     * Gets the "CreateCaseResponse" element
     */
    public com.idanalytics.products.casemanager.CreateCaseResponseDocument.CreateCaseResponse getCreateCaseResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.casemanager.CreateCaseResponseDocument.CreateCaseResponse target = null;
            target = (com.idanalytics.products.casemanager.CreateCaseResponseDocument.CreateCaseResponse)get_store().find_element_user(CREATECASERESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "CreateCaseResponse" element
     */
    public void setCreateCaseResponse(com.idanalytics.products.casemanager.CreateCaseResponseDocument.CreateCaseResponse createCaseResponse)
    {
        generatedSetterHelperImpl(createCaseResponse, CREATECASERESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "CreateCaseResponse" element
     */
    public com.idanalytics.products.casemanager.CreateCaseResponseDocument.CreateCaseResponse addNewCreateCaseResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.casemanager.CreateCaseResponseDocument.CreateCaseResponse target = null;
            target = (com.idanalytics.products.casemanager.CreateCaseResponseDocument.CreateCaseResponse)get_store().add_element_user(CREATECASERESPONSE$0);
            return target;
        }
    }
    /**
     * An XML CreateCaseResponse(@http://idanalytics.com/products/casemanager).
     *
     * This is a complex type.
     */
    public static class CreateCaseResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.casemanager.CreateCaseResponseDocument.CreateCaseResponse
    {
        private static final long serialVersionUID = 1L;
        
        public CreateCaseResponseImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName RESULT$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/casemanager", "Result");
        private static final javax.xml.namespace.QName CASENUMBER$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/casemanager", "CaseNumber");
        
        
        /**
         * Gets the "Result" element
         */
        public com.idanalytics.products.casemanager.ResultDocument.Result.Enum getResult()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RESULT$0, 0);
                if (target == null)
                {
                    return null;
                }
                return (com.idanalytics.products.casemanager.ResultDocument.Result.Enum)target.getEnumValue();
            }
        }
        
        /**
         * Gets (as xml) the "Result" element
         */
        public com.idanalytics.products.casemanager.ResultDocument.Result xgetResult()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.casemanager.ResultDocument.Result target = null;
                target = (com.idanalytics.products.casemanager.ResultDocument.Result)get_store().find_element_user(RESULT$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "Result" element
         */
        public void setResult(com.idanalytics.products.casemanager.ResultDocument.Result.Enum result)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RESULT$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(RESULT$0);
                }
                target.setEnumValue(result);
            }
        }
        
        /**
         * Sets (as xml) the "Result" element
         */
        public void xsetResult(com.idanalytics.products.casemanager.ResultDocument.Result result)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.casemanager.ResultDocument.Result target = null;
                target = (com.idanalytics.products.casemanager.ResultDocument.Result)get_store().find_element_user(RESULT$0, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.casemanager.ResultDocument.Result)get_store().add_element_user(RESULT$0);
                }
                target.set(result);
            }
        }
        
        /**
         * Gets the "CaseNumber" element
         */
        public int getCaseNumber()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CASENUMBER$2, 0);
                if (target == null)
                {
                    return 0;
                }
                return target.getIntValue();
            }
        }
        
        /**
         * Gets (as xml) the "CaseNumber" element
         */
        public com.idanalytics.products.casemanager.CaseNumber xgetCaseNumber()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.casemanager.CaseNumber target = null;
                target = (com.idanalytics.products.casemanager.CaseNumber)get_store().find_element_user(CASENUMBER$2, 0);
                return target;
            }
        }
        
        /**
         * True if has "CaseNumber" element
         */
        public boolean isSetCaseNumber()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(CASENUMBER$2) != 0;
            }
        }
        
        /**
         * Sets the "CaseNumber" element
         */
        public void setCaseNumber(int caseNumber)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CASENUMBER$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CASENUMBER$2);
                }
                target.setIntValue(caseNumber);
            }
        }
        
        /**
         * Sets (as xml) the "CaseNumber" element
         */
        public void xsetCaseNumber(com.idanalytics.products.casemanager.CaseNumber caseNumber)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.casemanager.CaseNumber target = null;
                target = (com.idanalytics.products.casemanager.CaseNumber)get_store().find_element_user(CASENUMBER$2, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.casemanager.CaseNumber)get_store().add_element_user(CASENUMBER$2);
                }
                target.set(caseNumber);
            }
        }
        
        /**
         * Unsets the "CaseNumber" element
         */
        public void unsetCaseNumber()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(CASENUMBER$2, 0);
            }
        }
    }
}
