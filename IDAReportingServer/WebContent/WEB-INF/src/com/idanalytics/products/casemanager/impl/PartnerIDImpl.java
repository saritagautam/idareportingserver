/*
 * XML Type:  PartnerID
 * Namespace: http://idanalytics.com/products/casemanager
 * Java type: com.idanalytics.products.casemanager.PartnerID
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.casemanager.impl;
/**
 * An XML PartnerID(@http://idanalytics.com/products/casemanager).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.casemanager.PartnerID.
 */
public class PartnerIDImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.casemanager.PartnerID
{
    private static final long serialVersionUID = 1L;
    
    public PartnerIDImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected PartnerIDImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
