/*
 * An XML document type.
 * Localname: GetResolvedCasesResponse
 * Namespace: http://idanalytics.com/products/casemanager
 * Java type: com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.casemanager;


/**
 * A document containing one GetResolvedCasesResponse(@http://idanalytics.com/products/casemanager) element.
 *
 * This is a complex type.
 */
public interface GetResolvedCasesResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetResolvedCasesResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("getresolvedcasesresponse67a4doctype");
    
    /**
     * Gets the "GetResolvedCasesResponse" element
     */
    com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument.GetResolvedCasesResponse getGetResolvedCasesResponse();
    
    /**
     * Sets the "GetResolvedCasesResponse" element
     */
    void setGetResolvedCasesResponse(com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument.GetResolvedCasesResponse getResolvedCasesResponse);
    
    /**
     * Appends and returns a new empty "GetResolvedCasesResponse" element
     */
    com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument.GetResolvedCasesResponse addNewGetResolvedCasesResponse();
    
    /**
     * An XML GetResolvedCasesResponse(@http://idanalytics.com/products/casemanager).
     *
     * This is a complex type.
     */
    public interface GetResolvedCasesResponse extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetResolvedCasesResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("getresolvedcasesresponse00d2elemtype");
        
        /**
         * Gets the "Result" element
         */
        com.idanalytics.products.casemanager.ResultDocument.Result.Enum getResult();
        
        /**
         * Gets (as xml) the "Result" element
         */
        com.idanalytics.products.casemanager.ResultDocument.Result xgetResult();
        
        /**
         * Sets the "Result" element
         */
        void setResult(com.idanalytics.products.casemanager.ResultDocument.Result.Enum result);
        
        /**
         * Sets (as xml) the "Result" element
         */
        void xsetResult(com.idanalytics.products.casemanager.ResultDocument.Result result);
        
        /**
         * Gets array of all "CaseResolution" elements
         */
        com.idanalytics.products.casemanager.CaseResolutionDocument.CaseResolution[] getCaseResolutionArray();
        
        /**
         * Gets ith "CaseResolution" element
         */
        com.idanalytics.products.casemanager.CaseResolutionDocument.CaseResolution getCaseResolutionArray(int i);
        
        /**
         * Returns number of "CaseResolution" element
         */
        int sizeOfCaseResolutionArray();
        
        /**
         * Sets array of all "CaseResolution" element
         */
        void setCaseResolutionArray(com.idanalytics.products.casemanager.CaseResolutionDocument.CaseResolution[] caseResolutionArray);
        
        /**
         * Sets ith "CaseResolution" element
         */
        void setCaseResolutionArray(int i, com.idanalytics.products.casemanager.CaseResolutionDocument.CaseResolution caseResolution);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "CaseResolution" element
         */
        com.idanalytics.products.casemanager.CaseResolutionDocument.CaseResolution insertNewCaseResolution(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "CaseResolution" element
         */
        com.idanalytics.products.casemanager.CaseResolutionDocument.CaseResolution addNewCaseResolution();
        
        /**
         * Removes the ith "CaseResolution" element
         */
        void removeCaseResolution(int i);
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument.GetResolvedCasesResponse newInstance() {
              return (com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument.GetResolvedCasesResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument.GetResolvedCasesResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument.GetResolvedCasesResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument newInstance() {
          return (com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.casemanager.GetResolvedCasesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
