/*
 * An XML document type.
 * Localname: Grade
 * Namespace: http://idanalytics.com/products/comply360/result
 * Java type: com.idanalytics.products.comply360.result.GradeDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.comply360.result.impl;
/**
 * A document containing one Grade(@http://idanalytics.com/products/comply360/result) element.
 *
 * This is a complex type.
 */
public class GradeDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.comply360.result.GradeDocument
{
    private static final long serialVersionUID = 1L;
    
    public GradeDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GRADE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/comply360/result", "Grade");
    
    
    /**
     * Gets the "Grade" element
     */
    public com.idanalytics.products.comply360.result.GradeDocument.Grade.Enum getGrade()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(GRADE$0, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.comply360.result.GradeDocument.Grade.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "Grade" element
     */
    public com.idanalytics.products.comply360.result.GradeDocument.Grade xgetGrade()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.comply360.result.GradeDocument.Grade target = null;
            target = (com.idanalytics.products.comply360.result.GradeDocument.Grade)get_store().find_element_user(GRADE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Grade" element
     */
    public void setGrade(com.idanalytics.products.comply360.result.GradeDocument.Grade.Enum grade)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(GRADE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(GRADE$0);
            }
            target.setEnumValue(grade);
        }
    }
    
    /**
     * Sets (as xml) the "Grade" element
     */
    public void xsetGrade(com.idanalytics.products.comply360.result.GradeDocument.Grade grade)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.comply360.result.GradeDocument.Grade target = null;
            target = (com.idanalytics.products.comply360.result.GradeDocument.Grade)get_store().find_element_user(GRADE$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.comply360.result.GradeDocument.Grade)get_store().add_element_user(GRADE$0);
            }
            target.set(grade);
        }
    }
    /**
     * An XML Grade(@http://idanalytics.com/products/comply360/result).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.comply360.result.GradeDocument$Grade.
     */
    public static class GradeImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.idanalytics.products.comply360.result.GradeDocument.Grade
    {
        private static final long serialVersionUID = 1L;
        
        public GradeImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected GradeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
