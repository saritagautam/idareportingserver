/*
 * XML Type:  VerificationIndicator
 * Namespace: http://idanalytics.com/products/comply360/result
 * Java type: com.idanalytics.products.comply360.result.VerificationIndicator
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.comply360.result.impl;
/**
 * An XML VerificationIndicator(@http://idanalytics.com/products/comply360/result).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.comply360.result.VerificationIndicator.
 */
public class VerificationIndicatorImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.comply360.result.VerificationIndicator
{
    private static final long serialVersionUID = 1L;
    
    public VerificationIndicatorImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected VerificationIndicatorImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
