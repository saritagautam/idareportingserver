/*
 * XML Type:  DOBType
 * Namespace: http://idanalytics.com/products/comply360/result
 * Java type: com.idanalytics.products.comply360.result.DOBType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.comply360.result.impl;
/**
 * An XML DOBType(@http://idanalytics.com/products/comply360/result).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.comply360.result.DOBType.
 */
public class DOBTypeImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.comply360.result.DOBType
{
    private static final long serialVersionUID = 1L;
    
    public DOBTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected DOBTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
