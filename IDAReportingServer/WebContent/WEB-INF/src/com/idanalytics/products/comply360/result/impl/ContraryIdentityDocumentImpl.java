/*
 * An XML document type.
 * Localname: ContraryIdentity
 * Namespace: http://idanalytics.com/products/comply360/result
 * Java type: com.idanalytics.products.comply360.result.ContraryIdentityDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.comply360.result.impl;
/**
 * A document containing one ContraryIdentity(@http://idanalytics.com/products/comply360/result) element.
 *
 * This is a complex type.
 */
public class ContraryIdentityDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.comply360.result.ContraryIdentityDocument
{
    private static final long serialVersionUID = 1L;
    
    public ContraryIdentityDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CONTRARYIDENTITY$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/comply360/result", "ContraryIdentity");
    
    
    /**
     * Gets the "ContraryIdentity" element
     */
    public com.idanalytics.products.comply360.result.ContraryIdentityDocument.ContraryIdentity getContraryIdentity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.comply360.result.ContraryIdentityDocument.ContraryIdentity target = null;
            target = (com.idanalytics.products.comply360.result.ContraryIdentityDocument.ContraryIdentity)get_store().find_element_user(CONTRARYIDENTITY$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "ContraryIdentity" element
     */
    public void setContraryIdentity(com.idanalytics.products.comply360.result.ContraryIdentityDocument.ContraryIdentity contraryIdentity)
    {
        generatedSetterHelperImpl(contraryIdentity, CONTRARYIDENTITY$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "ContraryIdentity" element
     */
    public com.idanalytics.products.comply360.result.ContraryIdentityDocument.ContraryIdentity addNewContraryIdentity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.comply360.result.ContraryIdentityDocument.ContraryIdentity target = null;
            target = (com.idanalytics.products.comply360.result.ContraryIdentityDocument.ContraryIdentity)get_store().add_element_user(CONTRARYIDENTITY$0);
            return target;
        }
    }
    /**
     * An XML ContraryIdentity(@http://idanalytics.com/products/comply360/result).
     *
     * This is a complex type.
     */
    public static class ContraryIdentityImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.comply360.result.ContraryIdentityDocument.ContraryIdentity
    {
        private static final long serialVersionUID = 1L;
        
        public ContraryIdentityImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName SSN$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/comply360/result", "SSN");
        private static final javax.xml.namespace.QName FIRSTNAME$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/comply360/result", "FirstName");
        private static final javax.xml.namespace.QName LASTNAME$4 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/comply360/result", "LastName");
        private static final javax.xml.namespace.QName ADDRESS$6 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/comply360/result", "Address");
        private static final javax.xml.namespace.QName ZIP$8 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/comply360/result", "Zip");
        private static final javax.xml.namespace.QName PHONE$10 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/comply360/result", "Phone");
        private static final javax.xml.namespace.QName DOB$12 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/comply360/result", "DOB");
        
        
        /**
         * Gets the "SSN" element
         */
        public java.lang.String getSSN()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SSN$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "SSN" element
         */
        public com.idanalytics.products.common_v1.SSNType xgetSSN()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.SSNType target = null;
                target = (com.idanalytics.products.common_v1.SSNType)get_store().find_element_user(SSN$0, 0);
                return target;
            }
        }
        
        /**
         * True if has "SSN" element
         */
        public boolean isSetSSN()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(SSN$0) != 0;
            }
        }
        
        /**
         * Sets the "SSN" element
         */
        public void setSSN(java.lang.String ssn)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SSN$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SSN$0);
                }
                target.setStringValue(ssn);
            }
        }
        
        /**
         * Sets (as xml) the "SSN" element
         */
        public void xsetSSN(com.idanalytics.products.common_v1.SSNType ssn)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.SSNType target = null;
                target = (com.idanalytics.products.common_v1.SSNType)get_store().find_element_user(SSN$0, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.SSNType)get_store().add_element_user(SSN$0);
                }
                target.set(ssn);
            }
        }
        
        /**
         * Unsets the "SSN" element
         */
        public void unsetSSN()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(SSN$0, 0);
            }
        }
        
        /**
         * Gets the "FirstName" element
         */
        public java.lang.String getFirstName()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FIRSTNAME$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "FirstName" element
         */
        public com.idanalytics.products.common_v1.NameType xgetFirstName()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.NameType target = null;
                target = (com.idanalytics.products.common_v1.NameType)get_store().find_element_user(FIRSTNAME$2, 0);
                return target;
            }
        }
        
        /**
         * True if has "FirstName" element
         */
        public boolean isSetFirstName()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(FIRSTNAME$2) != 0;
            }
        }
        
        /**
         * Sets the "FirstName" element
         */
        public void setFirstName(java.lang.String firstName)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FIRSTNAME$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(FIRSTNAME$2);
                }
                target.setStringValue(firstName);
            }
        }
        
        /**
         * Sets (as xml) the "FirstName" element
         */
        public void xsetFirstName(com.idanalytics.products.common_v1.NameType firstName)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.NameType target = null;
                target = (com.idanalytics.products.common_v1.NameType)get_store().find_element_user(FIRSTNAME$2, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.NameType)get_store().add_element_user(FIRSTNAME$2);
                }
                target.set(firstName);
            }
        }
        
        /**
         * Unsets the "FirstName" element
         */
        public void unsetFirstName()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(FIRSTNAME$2, 0);
            }
        }
        
        /**
         * Gets the "LastName" element
         */
        public java.lang.String getLastName()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LASTNAME$4, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "LastName" element
         */
        public com.idanalytics.products.common_v1.NameType xgetLastName()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.NameType target = null;
                target = (com.idanalytics.products.common_v1.NameType)get_store().find_element_user(LASTNAME$4, 0);
                return target;
            }
        }
        
        /**
         * True if has "LastName" element
         */
        public boolean isSetLastName()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(LASTNAME$4) != 0;
            }
        }
        
        /**
         * Sets the "LastName" element
         */
        public void setLastName(java.lang.String lastName)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LASTNAME$4, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(LASTNAME$4);
                }
                target.setStringValue(lastName);
            }
        }
        
        /**
         * Sets (as xml) the "LastName" element
         */
        public void xsetLastName(com.idanalytics.products.common_v1.NameType lastName)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.NameType target = null;
                target = (com.idanalytics.products.common_v1.NameType)get_store().find_element_user(LASTNAME$4, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.NameType)get_store().add_element_user(LASTNAME$4);
                }
                target.set(lastName);
            }
        }
        
        /**
         * Unsets the "LastName" element
         */
        public void unsetLastName()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(LASTNAME$4, 0);
            }
        }
        
        /**
         * Gets the "Address" element
         */
        public java.lang.String getAddress()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ADDRESS$6, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "Address" element
         */
        public com.idanalytics.products.common_v1.AddressType xgetAddress()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.AddressType target = null;
                target = (com.idanalytics.products.common_v1.AddressType)get_store().find_element_user(ADDRESS$6, 0);
                return target;
            }
        }
        
        /**
         * True if has "Address" element
         */
        public boolean isSetAddress()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(ADDRESS$6) != 0;
            }
        }
        
        /**
         * Sets the "Address" element
         */
        public void setAddress(java.lang.String address)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ADDRESS$6, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ADDRESS$6);
                }
                target.setStringValue(address);
            }
        }
        
        /**
         * Sets (as xml) the "Address" element
         */
        public void xsetAddress(com.idanalytics.products.common_v1.AddressType address)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.AddressType target = null;
                target = (com.idanalytics.products.common_v1.AddressType)get_store().find_element_user(ADDRESS$6, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.AddressType)get_store().add_element_user(ADDRESS$6);
                }
                target.set(address);
            }
        }
        
        /**
         * Unsets the "Address" element
         */
        public void unsetAddress()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(ADDRESS$6, 0);
            }
        }
        
        /**
         * Gets the "Zip" element
         */
        public java.lang.String getZip()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ZIP$8, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "Zip" element
         */
        public com.idanalytics.products.common_v1.ZipType xgetZip()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.ZipType target = null;
                target = (com.idanalytics.products.common_v1.ZipType)get_store().find_element_user(ZIP$8, 0);
                return target;
            }
        }
        
        /**
         * True if has "Zip" element
         */
        public boolean isSetZip()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(ZIP$8) != 0;
            }
        }
        
        /**
         * Sets the "Zip" element
         */
        public void setZip(java.lang.String zip)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ZIP$8, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ZIP$8);
                }
                target.setStringValue(zip);
            }
        }
        
        /**
         * Sets (as xml) the "Zip" element
         */
        public void xsetZip(com.idanalytics.products.common_v1.ZipType zip)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.ZipType target = null;
                target = (com.idanalytics.products.common_v1.ZipType)get_store().find_element_user(ZIP$8, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.ZipType)get_store().add_element_user(ZIP$8);
                }
                target.set(zip);
            }
        }
        
        /**
         * Unsets the "Zip" element
         */
        public void unsetZip()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(ZIP$8, 0);
            }
        }
        
        /**
         * Gets the "Phone" element
         */
        public java.lang.String getPhone()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PHONE$10, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "Phone" element
         */
        public com.idanalytics.products.common_v1.PhoneType xgetPhone()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PhoneType target = null;
                target = (com.idanalytics.products.common_v1.PhoneType)get_store().find_element_user(PHONE$10, 0);
                return target;
            }
        }
        
        /**
         * True if has "Phone" element
         */
        public boolean isSetPhone()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PHONE$10) != 0;
            }
        }
        
        /**
         * Sets the "Phone" element
         */
        public void setPhone(java.lang.String phone)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PHONE$10, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PHONE$10);
                }
                target.setStringValue(phone);
            }
        }
        
        /**
         * Sets (as xml) the "Phone" element
         */
        public void xsetPhone(com.idanalytics.products.common_v1.PhoneType phone)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PhoneType target = null;
                target = (com.idanalytics.products.common_v1.PhoneType)get_store().find_element_user(PHONE$10, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.PhoneType)get_store().add_element_user(PHONE$10);
                }
                target.set(phone);
            }
        }
        
        /**
         * Unsets the "Phone" element
         */
        public void unsetPhone()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PHONE$10, 0);
            }
        }
        
        /**
         * Gets the "DOB" element
         */
        public java.lang.String getDOB()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DOB$12, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "DOB" element
         */
        public com.idanalytics.products.comply360.result.DOBType xgetDOB()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.comply360.result.DOBType target = null;
                target = (com.idanalytics.products.comply360.result.DOBType)get_store().find_element_user(DOB$12, 0);
                return target;
            }
        }
        
        /**
         * True if has "DOB" element
         */
        public boolean isSetDOB()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(DOB$12) != 0;
            }
        }
        
        /**
         * Sets the "DOB" element
         */
        public void setDOB(java.lang.String dob)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DOB$12, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DOB$12);
                }
                target.setStringValue(dob);
            }
        }
        
        /**
         * Sets (as xml) the "DOB" element
         */
        public void xsetDOB(com.idanalytics.products.comply360.result.DOBType dob)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.comply360.result.DOBType target = null;
                target = (com.idanalytics.products.comply360.result.DOBType)get_store().find_element_user(DOB$12, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.comply360.result.DOBType)get_store().add_element_user(DOB$12);
                }
                target.set(dob);
            }
        }
        
        /**
         * Unsets the "DOB" element
         */
        public void unsetDOB()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(DOB$12, 0);
            }
        }
    }
}
