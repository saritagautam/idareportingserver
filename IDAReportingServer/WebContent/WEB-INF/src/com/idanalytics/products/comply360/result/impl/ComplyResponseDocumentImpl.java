/*
 * An XML document type.
 * Localname: ComplyResponse
 * Namespace: http://idanalytics.com/products/comply360/result
 * Java type: com.idanalytics.products.comply360.result.ComplyResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.comply360.result.impl;
/**
 * A document containing one ComplyResponse(@http://idanalytics.com/products/comply360/result) element.
 *
 * This is a complex type.
 */
public class ComplyResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.comply360.result.ComplyResponseDocument
{
    private static final long serialVersionUID = 1L;
    
    public ComplyResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName COMPLYRESPONSE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/comply360/result", "ComplyResponse");
    
    
    /**
     * Gets the "ComplyResponse" element
     */
    public com.idanalytics.products.comply360.result.ComplyResponseDocument.ComplyResponse getComplyResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.comply360.result.ComplyResponseDocument.ComplyResponse target = null;
            target = (com.idanalytics.products.comply360.result.ComplyResponseDocument.ComplyResponse)get_store().find_element_user(COMPLYRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "ComplyResponse" element
     */
    public void setComplyResponse(com.idanalytics.products.comply360.result.ComplyResponseDocument.ComplyResponse complyResponse)
    {
        generatedSetterHelperImpl(complyResponse, COMPLYRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "ComplyResponse" element
     */
    public com.idanalytics.products.comply360.result.ComplyResponseDocument.ComplyResponse addNewComplyResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.comply360.result.ComplyResponseDocument.ComplyResponse target = null;
            target = (com.idanalytics.products.comply360.result.ComplyResponseDocument.ComplyResponse)get_store().add_element_user(COMPLYRESPONSE$0);
            return target;
        }
    }
    /**
     * An XML ComplyResponse(@http://idanalytics.com/products/comply360/result).
     *
     * This is a complex type.
     */
    public static class ComplyResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.comply360.result.ComplyResponseDocument.ComplyResponse
    {
        private static final long serialVersionUID = 1L;
        
        public ComplyResponseImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName IDASTATUS$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/comply360/result", "IDAStatus");
        private static final javax.xml.namespace.QName APPID$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/comply360/result", "AppID");
        private static final javax.xml.namespace.QName DESIGNATION$4 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/comply360/result", "Designation");
        private static final javax.xml.namespace.QName ACCTLINKKEY$6 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/comply360/result", "AcctLinkKey");
        private static final javax.xml.namespace.QName IDASEQUENCE$8 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/comply360/result", "IDASequence");
        private static final javax.xml.namespace.QName IDATIMESTAMP$10 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/comply360/result", "IDATimeStamp");
        private static final javax.xml.namespace.QName GRADE$12 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/comply360/result", "Grade");
        private static final javax.xml.namespace.QName IDENTITYVERIFICATION$14 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/comply360/result", "IdentityVerification");
        private static final javax.xml.namespace.QName CONTRARYIDENTITY$16 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/comply360/result", "ContraryIdentity");
        private static final javax.xml.namespace.QName RESULTCODE1$18 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/comply360/result", "ResultCode1");
        private static final javax.xml.namespace.QName RESULTCODE2$20 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/comply360/result", "ResultCode2");
        private static final javax.xml.namespace.QName RESULTCODE3$22 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/comply360/result", "ResultCode3");
        private static final javax.xml.namespace.QName RESULTCODE4$24 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/comply360/result", "ResultCode4");
        private static final javax.xml.namespace.QName PASSTHRU1$26 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/comply360/result", "PassThru1");
        private static final javax.xml.namespace.QName PASSTHRU2$28 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/comply360/result", "PassThru2");
        private static final javax.xml.namespace.QName PASSTHRU3$30 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/comply360/result", "PassThru3");
        private static final javax.xml.namespace.QName PASSTHRU4$32 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/comply360/result", "PassThru4");
        private static final javax.xml.namespace.QName IDAINTERNAL$34 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/comply360/result", "IDAInternal");
        
        
        /**
         * Gets the "IDAStatus" element
         */
        public int getIDAStatus()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASTATUS$0, 0);
                if (target == null)
                {
                    return 0;
                }
                return target.getIntValue();
            }
        }
        
        /**
         * Gets (as xml) the "IDAStatus" element
         */
        public com.idanalytics.products.common_v1.IDAStatus xgetIDAStatus()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDAStatus target = null;
                target = (com.idanalytics.products.common_v1.IDAStatus)get_store().find_element_user(IDASTATUS$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "IDAStatus" element
         */
        public void setIDAStatus(int idaStatus)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASTATUS$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDASTATUS$0);
                }
                target.setIntValue(idaStatus);
            }
        }
        
        /**
         * Sets (as xml) the "IDAStatus" element
         */
        public void xsetIDAStatus(com.idanalytics.products.common_v1.IDAStatus idaStatus)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDAStatus target = null;
                target = (com.idanalytics.products.common_v1.IDAStatus)get_store().find_element_user(IDASTATUS$0, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.IDAStatus)get_store().add_element_user(IDASTATUS$0);
                }
                target.set(idaStatus);
            }
        }
        
        /**
         * Gets the "AppID" element
         */
        public java.lang.String getAppID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(APPID$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "AppID" element
         */
        public com.idanalytics.products.common_v1.AppID xgetAppID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.AppID target = null;
                target = (com.idanalytics.products.common_v1.AppID)get_store().find_element_user(APPID$2, 0);
                return target;
            }
        }
        
        /**
         * Sets the "AppID" element
         */
        public void setAppID(java.lang.String appID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(APPID$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(APPID$2);
                }
                target.setStringValue(appID);
            }
        }
        
        /**
         * Sets (as xml) the "AppID" element
         */
        public void xsetAppID(com.idanalytics.products.common_v1.AppID appID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.AppID target = null;
                target = (com.idanalytics.products.common_v1.AppID)get_store().find_element_user(APPID$2, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.AppID)get_store().add_element_user(APPID$2);
                }
                target.set(appID);
            }
        }
        
        /**
         * Gets the "Designation" element
         */
        public com.idanalytics.products.common_v1.Designation.Enum getDesignation()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESIGNATION$4, 0);
                if (target == null)
                {
                    return null;
                }
                return (com.idanalytics.products.common_v1.Designation.Enum)target.getEnumValue();
            }
        }
        
        /**
         * Gets (as xml) the "Designation" element
         */
        public com.idanalytics.products.common_v1.Designation xgetDesignation()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.Designation target = null;
                target = (com.idanalytics.products.common_v1.Designation)get_store().find_element_user(DESIGNATION$4, 0);
                return target;
            }
        }
        
        /**
         * True if has "Designation" element
         */
        public boolean isSetDesignation()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(DESIGNATION$4) != 0;
            }
        }
        
        /**
         * Sets the "Designation" element
         */
        public void setDesignation(com.idanalytics.products.common_v1.Designation.Enum designation)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESIGNATION$4, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DESIGNATION$4);
                }
                target.setEnumValue(designation);
            }
        }
        
        /**
         * Sets (as xml) the "Designation" element
         */
        public void xsetDesignation(com.idanalytics.products.common_v1.Designation designation)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.Designation target = null;
                target = (com.idanalytics.products.common_v1.Designation)get_store().find_element_user(DESIGNATION$4, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.Designation)get_store().add_element_user(DESIGNATION$4);
                }
                target.set(designation);
            }
        }
        
        /**
         * Unsets the "Designation" element
         */
        public void unsetDesignation()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(DESIGNATION$4, 0);
            }
        }
        
        /**
         * Gets the "AcctLinkKey" element
         */
        public java.lang.String getAcctLinkKey()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ACCTLINKKEY$6, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "AcctLinkKey" element
         */
        public com.idanalytics.products.comply360.result.AcctLinkKeyDocument.AcctLinkKey xgetAcctLinkKey()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.comply360.result.AcctLinkKeyDocument.AcctLinkKey target = null;
                target = (com.idanalytics.products.comply360.result.AcctLinkKeyDocument.AcctLinkKey)get_store().find_element_user(ACCTLINKKEY$6, 0);
                return target;
            }
        }
        
        /**
         * True if has "AcctLinkKey" element
         */
        public boolean isSetAcctLinkKey()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(ACCTLINKKEY$6) != 0;
            }
        }
        
        /**
         * Sets the "AcctLinkKey" element
         */
        public void setAcctLinkKey(java.lang.String acctLinkKey)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ACCTLINKKEY$6, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ACCTLINKKEY$6);
                }
                target.setStringValue(acctLinkKey);
            }
        }
        
        /**
         * Sets (as xml) the "AcctLinkKey" element
         */
        public void xsetAcctLinkKey(com.idanalytics.products.comply360.result.AcctLinkKeyDocument.AcctLinkKey acctLinkKey)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.comply360.result.AcctLinkKeyDocument.AcctLinkKey target = null;
                target = (com.idanalytics.products.comply360.result.AcctLinkKeyDocument.AcctLinkKey)get_store().find_element_user(ACCTLINKKEY$6, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.comply360.result.AcctLinkKeyDocument.AcctLinkKey)get_store().add_element_user(ACCTLINKKEY$6);
                }
                target.set(acctLinkKey);
            }
        }
        
        /**
         * Unsets the "AcctLinkKey" element
         */
        public void unsetAcctLinkKey()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(ACCTLINKKEY$6, 0);
            }
        }
        
        /**
         * Gets the "IDASequence" element
         */
        public java.lang.String getIDASequence()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASEQUENCE$8, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "IDASequence" element
         */
        public com.idanalytics.products.common_v1.IDASequence xgetIDASequence()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDASequence target = null;
                target = (com.idanalytics.products.common_v1.IDASequence)get_store().find_element_user(IDASEQUENCE$8, 0);
                return target;
            }
        }
        
        /**
         * Sets the "IDASequence" element
         */
        public void setIDASequence(java.lang.String idaSequence)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASEQUENCE$8, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDASEQUENCE$8);
                }
                target.setStringValue(idaSequence);
            }
        }
        
        /**
         * Sets (as xml) the "IDASequence" element
         */
        public void xsetIDASequence(com.idanalytics.products.common_v1.IDASequence idaSequence)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDASequence target = null;
                target = (com.idanalytics.products.common_v1.IDASequence)get_store().find_element_user(IDASEQUENCE$8, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.IDASequence)get_store().add_element_user(IDASEQUENCE$8);
                }
                target.set(idaSequence);
            }
        }
        
        /**
         * Gets the "IDATimeStamp" element
         */
        public java.util.Calendar getIDATimeStamp()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDATIMESTAMP$10, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getCalendarValue();
            }
        }
        
        /**
         * Gets (as xml) the "IDATimeStamp" element
         */
        public com.idanalytics.products.common_v1.IDATimeStamp xgetIDATimeStamp()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDATimeStamp target = null;
                target = (com.idanalytics.products.common_v1.IDATimeStamp)get_store().find_element_user(IDATIMESTAMP$10, 0);
                return target;
            }
        }
        
        /**
         * Sets the "IDATimeStamp" element
         */
        public void setIDATimeStamp(java.util.Calendar idaTimeStamp)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDATIMESTAMP$10, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDATIMESTAMP$10);
                }
                target.setCalendarValue(idaTimeStamp);
            }
        }
        
        /**
         * Sets (as xml) the "IDATimeStamp" element
         */
        public void xsetIDATimeStamp(com.idanalytics.products.common_v1.IDATimeStamp idaTimeStamp)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDATimeStamp target = null;
                target = (com.idanalytics.products.common_v1.IDATimeStamp)get_store().find_element_user(IDATIMESTAMP$10, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.IDATimeStamp)get_store().add_element_user(IDATIMESTAMP$10);
                }
                target.set(idaTimeStamp);
            }
        }
        
        /**
         * Gets the "Grade" element
         */
        public com.idanalytics.products.comply360.result.GradeDocument.Grade.Enum getGrade()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(GRADE$12, 0);
                if (target == null)
                {
                    return null;
                }
                return (com.idanalytics.products.comply360.result.GradeDocument.Grade.Enum)target.getEnumValue();
            }
        }
        
        /**
         * Gets (as xml) the "Grade" element
         */
        public com.idanalytics.products.comply360.result.GradeDocument.Grade xgetGrade()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.comply360.result.GradeDocument.Grade target = null;
                target = (com.idanalytics.products.comply360.result.GradeDocument.Grade)get_store().find_element_user(GRADE$12, 0);
                return target;
            }
        }
        
        /**
         * True if has "Grade" element
         */
        public boolean isSetGrade()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(GRADE$12) != 0;
            }
        }
        
        /**
         * Sets the "Grade" element
         */
        public void setGrade(com.idanalytics.products.comply360.result.GradeDocument.Grade.Enum grade)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(GRADE$12, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(GRADE$12);
                }
                target.setEnumValue(grade);
            }
        }
        
        /**
         * Sets (as xml) the "Grade" element
         */
        public void xsetGrade(com.idanalytics.products.comply360.result.GradeDocument.Grade grade)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.comply360.result.GradeDocument.Grade target = null;
                target = (com.idanalytics.products.comply360.result.GradeDocument.Grade)get_store().find_element_user(GRADE$12, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.comply360.result.GradeDocument.Grade)get_store().add_element_user(GRADE$12);
                }
                target.set(grade);
            }
        }
        
        /**
         * Unsets the "Grade" element
         */
        public void unsetGrade()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(GRADE$12, 0);
            }
        }
        
        /**
         * Gets the "IdentityVerification" element
         */
        public com.idanalytics.products.comply360.result.IdentityVerificationDocument.IdentityVerification getIdentityVerification()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.comply360.result.IdentityVerificationDocument.IdentityVerification target = null;
                target = (com.idanalytics.products.comply360.result.IdentityVerificationDocument.IdentityVerification)get_store().find_element_user(IDENTITYVERIFICATION$14, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "IdentityVerification" element
         */
        public boolean isSetIdentityVerification()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(IDENTITYVERIFICATION$14) != 0;
            }
        }
        
        /**
         * Sets the "IdentityVerification" element
         */
        public void setIdentityVerification(com.idanalytics.products.comply360.result.IdentityVerificationDocument.IdentityVerification identityVerification)
        {
            generatedSetterHelperImpl(identityVerification, IDENTITYVERIFICATION$14, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "IdentityVerification" element
         */
        public com.idanalytics.products.comply360.result.IdentityVerificationDocument.IdentityVerification addNewIdentityVerification()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.comply360.result.IdentityVerificationDocument.IdentityVerification target = null;
                target = (com.idanalytics.products.comply360.result.IdentityVerificationDocument.IdentityVerification)get_store().add_element_user(IDENTITYVERIFICATION$14);
                return target;
            }
        }
        
        /**
         * Unsets the "IdentityVerification" element
         */
        public void unsetIdentityVerification()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(IDENTITYVERIFICATION$14, 0);
            }
        }
        
        /**
         * Gets the "ContraryIdentity" element
         */
        public com.idanalytics.products.comply360.result.ContraryIdentityDocument.ContraryIdentity getContraryIdentity()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.comply360.result.ContraryIdentityDocument.ContraryIdentity target = null;
                target = (com.idanalytics.products.comply360.result.ContraryIdentityDocument.ContraryIdentity)get_store().find_element_user(CONTRARYIDENTITY$16, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "ContraryIdentity" element
         */
        public boolean isSetContraryIdentity()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(CONTRARYIDENTITY$16) != 0;
            }
        }
        
        /**
         * Sets the "ContraryIdentity" element
         */
        public void setContraryIdentity(com.idanalytics.products.comply360.result.ContraryIdentityDocument.ContraryIdentity contraryIdentity)
        {
            generatedSetterHelperImpl(contraryIdentity, CONTRARYIDENTITY$16, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "ContraryIdentity" element
         */
        public com.idanalytics.products.comply360.result.ContraryIdentityDocument.ContraryIdentity addNewContraryIdentity()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.comply360.result.ContraryIdentityDocument.ContraryIdentity target = null;
                target = (com.idanalytics.products.comply360.result.ContraryIdentityDocument.ContraryIdentity)get_store().add_element_user(CONTRARYIDENTITY$16);
                return target;
            }
        }
        
        /**
         * Unsets the "ContraryIdentity" element
         */
        public void unsetContraryIdentity()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(CONTRARYIDENTITY$16, 0);
            }
        }
        
        /**
         * Gets the "ResultCode1" element
         */
        public java.lang.String getResultCode1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RESULTCODE1$18, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "ResultCode1" element
         */
        public com.idanalytics.products.common_v1.ReasonCode xgetResultCode1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.ReasonCode target = null;
                target = (com.idanalytics.products.common_v1.ReasonCode)get_store().find_element_user(RESULTCODE1$18, 0);
                return target;
            }
        }
        
        /**
         * True if has "ResultCode1" element
         */
        public boolean isSetResultCode1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(RESULTCODE1$18) != 0;
            }
        }
        
        /**
         * Sets the "ResultCode1" element
         */
        public void setResultCode1(java.lang.String resultCode1)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RESULTCODE1$18, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(RESULTCODE1$18);
                }
                target.setStringValue(resultCode1);
            }
        }
        
        /**
         * Sets (as xml) the "ResultCode1" element
         */
        public void xsetResultCode1(com.idanalytics.products.common_v1.ReasonCode resultCode1)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.ReasonCode target = null;
                target = (com.idanalytics.products.common_v1.ReasonCode)get_store().find_element_user(RESULTCODE1$18, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.ReasonCode)get_store().add_element_user(RESULTCODE1$18);
                }
                target.set(resultCode1);
            }
        }
        
        /**
         * Unsets the "ResultCode1" element
         */
        public void unsetResultCode1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(RESULTCODE1$18, 0);
            }
        }
        
        /**
         * Gets the "ResultCode2" element
         */
        public java.lang.String getResultCode2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RESULTCODE2$20, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "ResultCode2" element
         */
        public com.idanalytics.products.common_v1.ReasonCode xgetResultCode2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.ReasonCode target = null;
                target = (com.idanalytics.products.common_v1.ReasonCode)get_store().find_element_user(RESULTCODE2$20, 0);
                return target;
            }
        }
        
        /**
         * True if has "ResultCode2" element
         */
        public boolean isSetResultCode2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(RESULTCODE2$20) != 0;
            }
        }
        
        /**
         * Sets the "ResultCode2" element
         */
        public void setResultCode2(java.lang.String resultCode2)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RESULTCODE2$20, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(RESULTCODE2$20);
                }
                target.setStringValue(resultCode2);
            }
        }
        
        /**
         * Sets (as xml) the "ResultCode2" element
         */
        public void xsetResultCode2(com.idanalytics.products.common_v1.ReasonCode resultCode2)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.ReasonCode target = null;
                target = (com.idanalytics.products.common_v1.ReasonCode)get_store().find_element_user(RESULTCODE2$20, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.ReasonCode)get_store().add_element_user(RESULTCODE2$20);
                }
                target.set(resultCode2);
            }
        }
        
        /**
         * Unsets the "ResultCode2" element
         */
        public void unsetResultCode2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(RESULTCODE2$20, 0);
            }
        }
        
        /**
         * Gets the "ResultCode3" element
         */
        public java.lang.String getResultCode3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RESULTCODE3$22, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "ResultCode3" element
         */
        public com.idanalytics.products.common_v1.ReasonCode xgetResultCode3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.ReasonCode target = null;
                target = (com.idanalytics.products.common_v1.ReasonCode)get_store().find_element_user(RESULTCODE3$22, 0);
                return target;
            }
        }
        
        /**
         * True if has "ResultCode3" element
         */
        public boolean isSetResultCode3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(RESULTCODE3$22) != 0;
            }
        }
        
        /**
         * Sets the "ResultCode3" element
         */
        public void setResultCode3(java.lang.String resultCode3)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RESULTCODE3$22, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(RESULTCODE3$22);
                }
                target.setStringValue(resultCode3);
            }
        }
        
        /**
         * Sets (as xml) the "ResultCode3" element
         */
        public void xsetResultCode3(com.idanalytics.products.common_v1.ReasonCode resultCode3)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.ReasonCode target = null;
                target = (com.idanalytics.products.common_v1.ReasonCode)get_store().find_element_user(RESULTCODE3$22, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.ReasonCode)get_store().add_element_user(RESULTCODE3$22);
                }
                target.set(resultCode3);
            }
        }
        
        /**
         * Unsets the "ResultCode3" element
         */
        public void unsetResultCode3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(RESULTCODE3$22, 0);
            }
        }
        
        /**
         * Gets the "ResultCode4" element
         */
        public java.lang.String getResultCode4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RESULTCODE4$24, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "ResultCode4" element
         */
        public com.idanalytics.products.common_v1.ReasonCode xgetResultCode4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.ReasonCode target = null;
                target = (com.idanalytics.products.common_v1.ReasonCode)get_store().find_element_user(RESULTCODE4$24, 0);
                return target;
            }
        }
        
        /**
         * True if has "ResultCode4" element
         */
        public boolean isSetResultCode4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(RESULTCODE4$24) != 0;
            }
        }
        
        /**
         * Sets the "ResultCode4" element
         */
        public void setResultCode4(java.lang.String resultCode4)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RESULTCODE4$24, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(RESULTCODE4$24);
                }
                target.setStringValue(resultCode4);
            }
        }
        
        /**
         * Sets (as xml) the "ResultCode4" element
         */
        public void xsetResultCode4(com.idanalytics.products.common_v1.ReasonCode resultCode4)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.ReasonCode target = null;
                target = (com.idanalytics.products.common_v1.ReasonCode)get_store().find_element_user(RESULTCODE4$24, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.ReasonCode)get_store().add_element_user(RESULTCODE4$24);
                }
                target.set(resultCode4);
            }
        }
        
        /**
         * Unsets the "ResultCode4" element
         */
        public void unsetResultCode4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(RESULTCODE4$24, 0);
            }
        }
        
        /**
         * Gets the "PassThru1" element
         */
        public java.lang.String getPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU1$26, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru1" element
         */
        public com.idanalytics.products.common_v1.PassThru xgetPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU1$26, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru1" element
         */
        public boolean isSetPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU1$26) != 0;
            }
        }
        
        /**
         * Sets the "PassThru1" element
         */
        public void setPassThru1(java.lang.String passThru1)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU1$26, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU1$26);
                }
                target.setStringValue(passThru1);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru1" element
         */
        public void xsetPassThru1(com.idanalytics.products.common_v1.PassThru passThru1)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU1$26, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.PassThru)get_store().add_element_user(PASSTHRU1$26);
                }
                target.set(passThru1);
            }
        }
        
        /**
         * Unsets the "PassThru1" element
         */
        public void unsetPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU1$26, 0);
            }
        }
        
        /**
         * Gets the "PassThru2" element
         */
        public java.lang.String getPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU2$28, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru2" element
         */
        public com.idanalytics.products.common_v1.PassThru xgetPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU2$28, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru2" element
         */
        public boolean isSetPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU2$28) != 0;
            }
        }
        
        /**
         * Sets the "PassThru2" element
         */
        public void setPassThru2(java.lang.String passThru2)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU2$28, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU2$28);
                }
                target.setStringValue(passThru2);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru2" element
         */
        public void xsetPassThru2(com.idanalytics.products.common_v1.PassThru passThru2)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU2$28, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.PassThru)get_store().add_element_user(PASSTHRU2$28);
                }
                target.set(passThru2);
            }
        }
        
        /**
         * Unsets the "PassThru2" element
         */
        public void unsetPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU2$28, 0);
            }
        }
        
        /**
         * Gets the "PassThru3" element
         */
        public java.lang.String getPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU3$30, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru3" element
         */
        public com.idanalytics.products.common_v1.PassThru xgetPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU3$30, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru3" element
         */
        public boolean isSetPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU3$30) != 0;
            }
        }
        
        /**
         * Sets the "PassThru3" element
         */
        public void setPassThru3(java.lang.String passThru3)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU3$30, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU3$30);
                }
                target.setStringValue(passThru3);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru3" element
         */
        public void xsetPassThru3(com.idanalytics.products.common_v1.PassThru passThru3)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU3$30, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.PassThru)get_store().add_element_user(PASSTHRU3$30);
                }
                target.set(passThru3);
            }
        }
        
        /**
         * Unsets the "PassThru3" element
         */
        public void unsetPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU3$30, 0);
            }
        }
        
        /**
         * Gets the "PassThru4" element
         */
        public java.lang.String getPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU4$32, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru4" element
         */
        public com.idanalytics.products.common_v1.PassThru xgetPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU4$32, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru4" element
         */
        public boolean isSetPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU4$32) != 0;
            }
        }
        
        /**
         * Sets the "PassThru4" element
         */
        public void setPassThru4(java.lang.String passThru4)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU4$32, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU4$32);
                }
                target.setStringValue(passThru4);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru4" element
         */
        public void xsetPassThru4(com.idanalytics.products.common_v1.PassThru passThru4)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU4$32, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.PassThru)get_store().add_element_user(PASSTHRU4$32);
                }
                target.set(passThru4);
            }
        }
        
        /**
         * Unsets the "PassThru4" element
         */
        public void unsetPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU4$32, 0);
            }
        }
        
        /**
         * Gets the "IDAInternal" element
         */
        public com.idanalytics.products.comply360.result.IDAInternalDocument.IDAInternal getIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.comply360.result.IDAInternalDocument.IDAInternal target = null;
                target = (com.idanalytics.products.comply360.result.IDAInternalDocument.IDAInternal)get_store().find_element_user(IDAINTERNAL$34, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "IDAInternal" element
         */
        public boolean isSetIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(IDAINTERNAL$34) != 0;
            }
        }
        
        /**
         * Sets the "IDAInternal" element
         */
        public void setIDAInternal(com.idanalytics.products.comply360.result.IDAInternalDocument.IDAInternal idaInternal)
        {
            generatedSetterHelperImpl(idaInternal, IDAINTERNAL$34, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "IDAInternal" element
         */
        public com.idanalytics.products.comply360.result.IDAInternalDocument.IDAInternal addNewIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.comply360.result.IDAInternalDocument.IDAInternal target = null;
                target = (com.idanalytics.products.comply360.result.IDAInternalDocument.IDAInternal)get_store().add_element_user(IDAINTERNAL$34);
                return target;
            }
        }
        
        /**
         * Unsets the "IDAInternal" element
         */
        public void unsetIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(IDAINTERNAL$34, 0);
            }
        }
    }
}
