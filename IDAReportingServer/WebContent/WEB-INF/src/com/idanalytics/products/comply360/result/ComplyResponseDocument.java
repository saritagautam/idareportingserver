/*
 * An XML document type.
 * Localname: ComplyResponse
 * Namespace: http://idanalytics.com/products/comply360/result
 * Java type: com.idanalytics.products.comply360.result.ComplyResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.comply360.result;


/**
 * A document containing one ComplyResponse(@http://idanalytics.com/products/comply360/result) element.
 *
 * This is a complex type.
 */
public interface ComplyResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ComplyResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("complyresponseab8ddoctype");
    
    /**
     * Gets the "ComplyResponse" element
     */
    com.idanalytics.products.comply360.result.ComplyResponseDocument.ComplyResponse getComplyResponse();
    
    /**
     * Sets the "ComplyResponse" element
     */
    void setComplyResponse(com.idanalytics.products.comply360.result.ComplyResponseDocument.ComplyResponse complyResponse);
    
    /**
     * Appends and returns a new empty "ComplyResponse" element
     */
    com.idanalytics.products.comply360.result.ComplyResponseDocument.ComplyResponse addNewComplyResponse();
    
    /**
     * An XML ComplyResponse(@http://idanalytics.com/products/comply360/result).
     *
     * This is a complex type.
     */
    public interface ComplyResponse extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ComplyResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("complyresponse4546elemtype");
        
        /**
         * Gets the "IDAStatus" element
         */
        int getIDAStatus();
        
        /**
         * Gets (as xml) the "IDAStatus" element
         */
        com.idanalytics.products.common_v1.IDAStatus xgetIDAStatus();
        
        /**
         * Sets the "IDAStatus" element
         */
        void setIDAStatus(int idaStatus);
        
        /**
         * Sets (as xml) the "IDAStatus" element
         */
        void xsetIDAStatus(com.idanalytics.products.common_v1.IDAStatus idaStatus);
        
        /**
         * Gets the "AppID" element
         */
        java.lang.String getAppID();
        
        /**
         * Gets (as xml) the "AppID" element
         */
        com.idanalytics.products.common_v1.AppID xgetAppID();
        
        /**
         * Sets the "AppID" element
         */
        void setAppID(java.lang.String appID);
        
        /**
         * Sets (as xml) the "AppID" element
         */
        void xsetAppID(com.idanalytics.products.common_v1.AppID appID);
        
        /**
         * Gets the "Designation" element
         */
        com.idanalytics.products.common_v1.Designation.Enum getDesignation();
        
        /**
         * Gets (as xml) the "Designation" element
         */
        com.idanalytics.products.common_v1.Designation xgetDesignation();
        
        /**
         * True if has "Designation" element
         */
        boolean isSetDesignation();
        
        /**
         * Sets the "Designation" element
         */
        void setDesignation(com.idanalytics.products.common_v1.Designation.Enum designation);
        
        /**
         * Sets (as xml) the "Designation" element
         */
        void xsetDesignation(com.idanalytics.products.common_v1.Designation designation);
        
        /**
         * Unsets the "Designation" element
         */
        void unsetDesignation();
        
        /**
         * Gets the "AcctLinkKey" element
         */
        java.lang.String getAcctLinkKey();
        
        /**
         * Gets (as xml) the "AcctLinkKey" element
         */
        com.idanalytics.products.comply360.result.AcctLinkKeyDocument.AcctLinkKey xgetAcctLinkKey();
        
        /**
         * True if has "AcctLinkKey" element
         */
        boolean isSetAcctLinkKey();
        
        /**
         * Sets the "AcctLinkKey" element
         */
        void setAcctLinkKey(java.lang.String acctLinkKey);
        
        /**
         * Sets (as xml) the "AcctLinkKey" element
         */
        void xsetAcctLinkKey(com.idanalytics.products.comply360.result.AcctLinkKeyDocument.AcctLinkKey acctLinkKey);
        
        /**
         * Unsets the "AcctLinkKey" element
         */
        void unsetAcctLinkKey();
        
        /**
         * Gets the "IDASequence" element
         */
        java.lang.String getIDASequence();
        
        /**
         * Gets (as xml) the "IDASequence" element
         */
        com.idanalytics.products.common_v1.IDASequence xgetIDASequence();
        
        /**
         * Sets the "IDASequence" element
         */
        void setIDASequence(java.lang.String idaSequence);
        
        /**
         * Sets (as xml) the "IDASequence" element
         */
        void xsetIDASequence(com.idanalytics.products.common_v1.IDASequence idaSequence);
        
        /**
         * Gets the "IDATimeStamp" element
         */
        java.util.Calendar getIDATimeStamp();
        
        /**
         * Gets (as xml) the "IDATimeStamp" element
         */
        com.idanalytics.products.common_v1.IDATimeStamp xgetIDATimeStamp();
        
        /**
         * Sets the "IDATimeStamp" element
         */
        void setIDATimeStamp(java.util.Calendar idaTimeStamp);
        
        /**
         * Sets (as xml) the "IDATimeStamp" element
         */
        void xsetIDATimeStamp(com.idanalytics.products.common_v1.IDATimeStamp idaTimeStamp);
        
        /**
         * Gets the "Grade" element
         */
        com.idanalytics.products.comply360.result.GradeDocument.Grade.Enum getGrade();
        
        /**
         * Gets (as xml) the "Grade" element
         */
        com.idanalytics.products.comply360.result.GradeDocument.Grade xgetGrade();
        
        /**
         * True if has "Grade" element
         */
        boolean isSetGrade();
        
        /**
         * Sets the "Grade" element
         */
        void setGrade(com.idanalytics.products.comply360.result.GradeDocument.Grade.Enum grade);
        
        /**
         * Sets (as xml) the "Grade" element
         */
        void xsetGrade(com.idanalytics.products.comply360.result.GradeDocument.Grade grade);
        
        /**
         * Unsets the "Grade" element
         */
        void unsetGrade();
        
        /**
         * Gets the "IdentityVerification" element
         */
        com.idanalytics.products.comply360.result.IdentityVerificationDocument.IdentityVerification getIdentityVerification();
        
        /**
         * True if has "IdentityVerification" element
         */
        boolean isSetIdentityVerification();
        
        /**
         * Sets the "IdentityVerification" element
         */
        void setIdentityVerification(com.idanalytics.products.comply360.result.IdentityVerificationDocument.IdentityVerification identityVerification);
        
        /**
         * Appends and returns a new empty "IdentityVerification" element
         */
        com.idanalytics.products.comply360.result.IdentityVerificationDocument.IdentityVerification addNewIdentityVerification();
        
        /**
         * Unsets the "IdentityVerification" element
         */
        void unsetIdentityVerification();
        
        /**
         * Gets the "ContraryIdentity" element
         */
        com.idanalytics.products.comply360.result.ContraryIdentityDocument.ContraryIdentity getContraryIdentity();
        
        /**
         * True if has "ContraryIdentity" element
         */
        boolean isSetContraryIdentity();
        
        /**
         * Sets the "ContraryIdentity" element
         */
        void setContraryIdentity(com.idanalytics.products.comply360.result.ContraryIdentityDocument.ContraryIdentity contraryIdentity);
        
        /**
         * Appends and returns a new empty "ContraryIdentity" element
         */
        com.idanalytics.products.comply360.result.ContraryIdentityDocument.ContraryIdentity addNewContraryIdentity();
        
        /**
         * Unsets the "ContraryIdentity" element
         */
        void unsetContraryIdentity();
        
        /**
         * Gets the "ResultCode1" element
         */
        java.lang.String getResultCode1();
        
        /**
         * Gets (as xml) the "ResultCode1" element
         */
        com.idanalytics.products.common_v1.ReasonCode xgetResultCode1();
        
        /**
         * True if has "ResultCode1" element
         */
        boolean isSetResultCode1();
        
        /**
         * Sets the "ResultCode1" element
         */
        void setResultCode1(java.lang.String resultCode1);
        
        /**
         * Sets (as xml) the "ResultCode1" element
         */
        void xsetResultCode1(com.idanalytics.products.common_v1.ReasonCode resultCode1);
        
        /**
         * Unsets the "ResultCode1" element
         */
        void unsetResultCode1();
        
        /**
         * Gets the "ResultCode2" element
         */
        java.lang.String getResultCode2();
        
        /**
         * Gets (as xml) the "ResultCode2" element
         */
        com.idanalytics.products.common_v1.ReasonCode xgetResultCode2();
        
        /**
         * True if has "ResultCode2" element
         */
        boolean isSetResultCode2();
        
        /**
         * Sets the "ResultCode2" element
         */
        void setResultCode2(java.lang.String resultCode2);
        
        /**
         * Sets (as xml) the "ResultCode2" element
         */
        void xsetResultCode2(com.idanalytics.products.common_v1.ReasonCode resultCode2);
        
        /**
         * Unsets the "ResultCode2" element
         */
        void unsetResultCode2();
        
        /**
         * Gets the "ResultCode3" element
         */
        java.lang.String getResultCode3();
        
        /**
         * Gets (as xml) the "ResultCode3" element
         */
        com.idanalytics.products.common_v1.ReasonCode xgetResultCode3();
        
        /**
         * True if has "ResultCode3" element
         */
        boolean isSetResultCode3();
        
        /**
         * Sets the "ResultCode3" element
         */
        void setResultCode3(java.lang.String resultCode3);
        
        /**
         * Sets (as xml) the "ResultCode3" element
         */
        void xsetResultCode3(com.idanalytics.products.common_v1.ReasonCode resultCode3);
        
        /**
         * Unsets the "ResultCode3" element
         */
        void unsetResultCode3();
        
        /**
         * Gets the "ResultCode4" element
         */
        java.lang.String getResultCode4();
        
        /**
         * Gets (as xml) the "ResultCode4" element
         */
        com.idanalytics.products.common_v1.ReasonCode xgetResultCode4();
        
        /**
         * True if has "ResultCode4" element
         */
        boolean isSetResultCode4();
        
        /**
         * Sets the "ResultCode4" element
         */
        void setResultCode4(java.lang.String resultCode4);
        
        /**
         * Sets (as xml) the "ResultCode4" element
         */
        void xsetResultCode4(com.idanalytics.products.common_v1.ReasonCode resultCode4);
        
        /**
         * Unsets the "ResultCode4" element
         */
        void unsetResultCode4();
        
        /**
         * Gets the "PassThru1" element
         */
        java.lang.String getPassThru1();
        
        /**
         * Gets (as xml) the "PassThru1" element
         */
        com.idanalytics.products.common_v1.PassThru xgetPassThru1();
        
        /**
         * True if has "PassThru1" element
         */
        boolean isSetPassThru1();
        
        /**
         * Sets the "PassThru1" element
         */
        void setPassThru1(java.lang.String passThru1);
        
        /**
         * Sets (as xml) the "PassThru1" element
         */
        void xsetPassThru1(com.idanalytics.products.common_v1.PassThru passThru1);
        
        /**
         * Unsets the "PassThru1" element
         */
        void unsetPassThru1();
        
        /**
         * Gets the "PassThru2" element
         */
        java.lang.String getPassThru2();
        
        /**
         * Gets (as xml) the "PassThru2" element
         */
        com.idanalytics.products.common_v1.PassThru xgetPassThru2();
        
        /**
         * True if has "PassThru2" element
         */
        boolean isSetPassThru2();
        
        /**
         * Sets the "PassThru2" element
         */
        void setPassThru2(java.lang.String passThru2);
        
        /**
         * Sets (as xml) the "PassThru2" element
         */
        void xsetPassThru2(com.idanalytics.products.common_v1.PassThru passThru2);
        
        /**
         * Unsets the "PassThru2" element
         */
        void unsetPassThru2();
        
        /**
         * Gets the "PassThru3" element
         */
        java.lang.String getPassThru3();
        
        /**
         * Gets (as xml) the "PassThru3" element
         */
        com.idanalytics.products.common_v1.PassThru xgetPassThru3();
        
        /**
         * True if has "PassThru3" element
         */
        boolean isSetPassThru3();
        
        /**
         * Sets the "PassThru3" element
         */
        void setPassThru3(java.lang.String passThru3);
        
        /**
         * Sets (as xml) the "PassThru3" element
         */
        void xsetPassThru3(com.idanalytics.products.common_v1.PassThru passThru3);
        
        /**
         * Unsets the "PassThru3" element
         */
        void unsetPassThru3();
        
        /**
         * Gets the "PassThru4" element
         */
        java.lang.String getPassThru4();
        
        /**
         * Gets (as xml) the "PassThru4" element
         */
        com.idanalytics.products.common_v1.PassThru xgetPassThru4();
        
        /**
         * True if has "PassThru4" element
         */
        boolean isSetPassThru4();
        
        /**
         * Sets the "PassThru4" element
         */
        void setPassThru4(java.lang.String passThru4);
        
        /**
         * Sets (as xml) the "PassThru4" element
         */
        void xsetPassThru4(com.idanalytics.products.common_v1.PassThru passThru4);
        
        /**
         * Unsets the "PassThru4" element
         */
        void unsetPassThru4();
        
        /**
         * Gets the "IDAInternal" element
         */
        com.idanalytics.products.comply360.result.IDAInternalDocument.IDAInternal getIDAInternal();
        
        /**
         * True if has "IDAInternal" element
         */
        boolean isSetIDAInternal();
        
        /**
         * Sets the "IDAInternal" element
         */
        void setIDAInternal(com.idanalytics.products.comply360.result.IDAInternalDocument.IDAInternal idaInternal);
        
        /**
         * Appends and returns a new empty "IDAInternal" element
         */
        com.idanalytics.products.comply360.result.IDAInternalDocument.IDAInternal addNewIDAInternal();
        
        /**
         * Unsets the "IDAInternal" element
         */
        void unsetIDAInternal();
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.comply360.result.ComplyResponseDocument.ComplyResponse newInstance() {
              return (com.idanalytics.products.comply360.result.ComplyResponseDocument.ComplyResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.comply360.result.ComplyResponseDocument.ComplyResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.comply360.result.ComplyResponseDocument.ComplyResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.comply360.result.ComplyResponseDocument newInstance() {
          return (com.idanalytics.products.comply360.result.ComplyResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.comply360.result.ComplyResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.comply360.result.ComplyResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.comply360.result.ComplyResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.comply360.result.ComplyResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.comply360.result.ComplyResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.comply360.result.ComplyResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.comply360.result.ComplyResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.comply360.result.ComplyResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.comply360.result.ComplyResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.comply360.result.ComplyResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.comply360.result.ComplyResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.comply360.result.ComplyResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.comply360.result.ComplyResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.comply360.result.ComplyResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.comply360.result.ComplyResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.comply360.result.ComplyResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.comply360.result.ComplyResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.comply360.result.ComplyResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.comply360.result.ComplyResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.comply360.result.ComplyResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.comply360.result.ComplyResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.comply360.result.ComplyResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.comply360.result.ComplyResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.comply360.result.ComplyResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.comply360.result.ComplyResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.comply360.result.ComplyResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.comply360.result.ComplyResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.comply360.result.ComplyResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.comply360.result.ComplyResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.comply360.result.ComplyResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.comply360.result.ComplyResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.comply360.result.ComplyResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.comply360.result.ComplyResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.comply360.result.ComplyResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
