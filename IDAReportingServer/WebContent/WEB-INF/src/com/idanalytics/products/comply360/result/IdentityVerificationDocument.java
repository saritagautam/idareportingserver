/*
 * An XML document type.
 * Localname: IdentityVerification
 * Namespace: http://idanalytics.com/products/comply360/result
 * Java type: com.idanalytics.products.comply360.result.IdentityVerificationDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.comply360.result;


/**
 * A document containing one IdentityVerification(@http://idanalytics.com/products/comply360/result) element.
 *
 * This is a complex type.
 */
public interface IdentityVerificationDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(IdentityVerificationDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("identityverification3d11doctype");
    
    /**
     * Gets the "IdentityVerification" element
     */
    com.idanalytics.products.comply360.result.IdentityVerificationDocument.IdentityVerification getIdentityVerification();
    
    /**
     * Sets the "IdentityVerification" element
     */
    void setIdentityVerification(com.idanalytics.products.comply360.result.IdentityVerificationDocument.IdentityVerification identityVerification);
    
    /**
     * Appends and returns a new empty "IdentityVerification" element
     */
    com.idanalytics.products.comply360.result.IdentityVerificationDocument.IdentityVerification addNewIdentityVerification();
    
    /**
     * An XML IdentityVerification(@http://idanalytics.com/products/comply360/result).
     *
     * This is a complex type.
     */
    public interface IdentityVerification extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(IdentityVerification.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("identityverificationfec6elemtype");
        
        /**
         * Gets the "SSN" element
         */
        java.lang.String getSSN();
        
        /**
         * Gets (as xml) the "SSN" element
         */
        com.idanalytics.products.comply360.result.VerificationIndicator xgetSSN();
        
        /**
         * True if has "SSN" element
         */
        boolean isSetSSN();
        
        /**
         * Sets the "SSN" element
         */
        void setSSN(java.lang.String ssn);
        
        /**
         * Sets (as xml) the "SSN" element
         */
        void xsetSSN(com.idanalytics.products.comply360.result.VerificationIndicator ssn);
        
        /**
         * Unsets the "SSN" element
         */
        void unsetSSN();
        
        /**
         * Gets the "FirstName" element
         */
        java.lang.String getFirstName();
        
        /**
         * Gets (as xml) the "FirstName" element
         */
        com.idanalytics.products.comply360.result.VerificationIndicator xgetFirstName();
        
        /**
         * True if has "FirstName" element
         */
        boolean isSetFirstName();
        
        /**
         * Sets the "FirstName" element
         */
        void setFirstName(java.lang.String firstName);
        
        /**
         * Sets (as xml) the "FirstName" element
         */
        void xsetFirstName(com.idanalytics.products.comply360.result.VerificationIndicator firstName);
        
        /**
         * Unsets the "FirstName" element
         */
        void unsetFirstName();
        
        /**
         * Gets the "LastName" element
         */
        java.lang.String getLastName();
        
        /**
         * Gets (as xml) the "LastName" element
         */
        com.idanalytics.products.comply360.result.VerificationIndicator xgetLastName();
        
        /**
         * True if has "LastName" element
         */
        boolean isSetLastName();
        
        /**
         * Sets the "LastName" element
         */
        void setLastName(java.lang.String lastName);
        
        /**
         * Sets (as xml) the "LastName" element
         */
        void xsetLastName(com.idanalytics.products.comply360.result.VerificationIndicator lastName);
        
        /**
         * Unsets the "LastName" element
         */
        void unsetLastName();
        
        /**
         * Gets the "Address" element
         */
        java.lang.String getAddress();
        
        /**
         * Gets (as xml) the "Address" element
         */
        com.idanalytics.products.comply360.result.VerificationIndicator xgetAddress();
        
        /**
         * True if has "Address" element
         */
        boolean isSetAddress();
        
        /**
         * Sets the "Address" element
         */
        void setAddress(java.lang.String address);
        
        /**
         * Sets (as xml) the "Address" element
         */
        void xsetAddress(com.idanalytics.products.comply360.result.VerificationIndicator address);
        
        /**
         * Unsets the "Address" element
         */
        void unsetAddress();
        
        /**
         * Gets the "Phone" element
         */
        java.lang.String getPhone();
        
        /**
         * Gets (as xml) the "Phone" element
         */
        com.idanalytics.products.comply360.result.VerificationIndicator xgetPhone();
        
        /**
         * True if has "Phone" element
         */
        boolean isSetPhone();
        
        /**
         * Sets the "Phone" element
         */
        void setPhone(java.lang.String phone);
        
        /**
         * Sets (as xml) the "Phone" element
         */
        void xsetPhone(com.idanalytics.products.comply360.result.VerificationIndicator phone);
        
        /**
         * Unsets the "Phone" element
         */
        void unsetPhone();
        
        /**
         * Gets the "DOB" element
         */
        java.lang.String getDOB();
        
        /**
         * Gets (as xml) the "DOB" element
         */
        com.idanalytics.products.comply360.result.VerificationIndicator xgetDOB();
        
        /**
         * True if has "DOB" element
         */
        boolean isSetDOB();
        
        /**
         * Sets the "DOB" element
         */
        void setDOB(java.lang.String dob);
        
        /**
         * Sets (as xml) the "DOB" element
         */
        void xsetDOB(com.idanalytics.products.comply360.result.VerificationIndicator dob);
        
        /**
         * Unsets the "DOB" element
         */
        void unsetDOB();
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.comply360.result.IdentityVerificationDocument.IdentityVerification newInstance() {
              return (com.idanalytics.products.comply360.result.IdentityVerificationDocument.IdentityVerification) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.comply360.result.IdentityVerificationDocument.IdentityVerification newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.comply360.result.IdentityVerificationDocument.IdentityVerification) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.comply360.result.IdentityVerificationDocument newInstance() {
          return (com.idanalytics.products.comply360.result.IdentityVerificationDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.comply360.result.IdentityVerificationDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.comply360.result.IdentityVerificationDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.comply360.result.IdentityVerificationDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.comply360.result.IdentityVerificationDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.comply360.result.IdentityVerificationDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.comply360.result.IdentityVerificationDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.comply360.result.IdentityVerificationDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.comply360.result.IdentityVerificationDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.comply360.result.IdentityVerificationDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.comply360.result.IdentityVerificationDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.comply360.result.IdentityVerificationDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.comply360.result.IdentityVerificationDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.comply360.result.IdentityVerificationDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.comply360.result.IdentityVerificationDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.comply360.result.IdentityVerificationDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.comply360.result.IdentityVerificationDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.comply360.result.IdentityVerificationDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.comply360.result.IdentityVerificationDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.comply360.result.IdentityVerificationDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.comply360.result.IdentityVerificationDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.comply360.result.IdentityVerificationDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.comply360.result.IdentityVerificationDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.comply360.result.IdentityVerificationDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.comply360.result.IdentityVerificationDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.comply360.result.IdentityVerificationDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.comply360.result.IdentityVerificationDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.comply360.result.IdentityVerificationDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.comply360.result.IdentityVerificationDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.comply360.result.IdentityVerificationDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.comply360.result.IdentityVerificationDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.comply360.result.IdentityVerificationDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.comply360.result.IdentityVerificationDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.comply360.result.IdentityVerificationDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.comply360.result.IdentityVerificationDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
