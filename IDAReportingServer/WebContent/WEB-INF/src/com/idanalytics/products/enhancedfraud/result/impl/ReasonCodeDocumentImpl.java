/*
 * An XML document type.
 * Localname: ReasonCode
 * Namespace: http://idanalytics.com/products/enhancedfraud/result
 * Java type: com.idanalytics.products.enhancedfraud.result.ReasonCodeDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.enhancedfraud.result.impl;
/**
 * A document containing one ReasonCode(@http://idanalytics.com/products/enhancedfraud/result) element.
 *
 * This is a complex type.
 */
public class ReasonCodeDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.enhancedfraud.result.ReasonCodeDocument
{
    private static final long serialVersionUID = 1L;
    
    public ReasonCodeDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName REASONCODE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/enhancedfraud/result", "ReasonCode");
    
    
    /**
     * Gets the "ReasonCode" element
     */
    public java.lang.String getReasonCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(REASONCODE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "ReasonCode" element
     */
    public com.idanalytics.products.enhancedfraud.result.ReasonCodeDocument.ReasonCode xgetReasonCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.enhancedfraud.result.ReasonCodeDocument.ReasonCode target = null;
            target = (com.idanalytics.products.enhancedfraud.result.ReasonCodeDocument.ReasonCode)get_store().find_element_user(REASONCODE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "ReasonCode" element
     */
    public void setReasonCode(java.lang.String reasonCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(REASONCODE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(REASONCODE$0);
            }
            target.setStringValue(reasonCode);
        }
    }
    
    /**
     * Sets (as xml) the "ReasonCode" element
     */
    public void xsetReasonCode(com.idanalytics.products.enhancedfraud.result.ReasonCodeDocument.ReasonCode reasonCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.enhancedfraud.result.ReasonCodeDocument.ReasonCode target = null;
            target = (com.idanalytics.products.enhancedfraud.result.ReasonCodeDocument.ReasonCode)get_store().find_element_user(REASONCODE$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.enhancedfraud.result.ReasonCodeDocument.ReasonCode)get_store().add_element_user(REASONCODE$0);
            }
            target.set(reasonCode);
        }
    }
    /**
     * An XML ReasonCode(@http://idanalytics.com/products/enhancedfraud/result).
     *
     * This is a union type. Instances are of one of the following types:
     *     com.idanalytics.products.enhancedfraud.result.ReasonCodeDocument$ReasonCode$Member
     *     com.idanalytics.products.enhancedfraud.result.ReasonCodeDocument$ReasonCode$Member2
     */
    public static class ReasonCodeImpl extends org.apache.xmlbeans.impl.values.XmlUnionImpl implements com.idanalytics.products.enhancedfraud.result.ReasonCodeDocument.ReasonCode, com.idanalytics.products.enhancedfraud.result.ReasonCodeDocument.ReasonCode.Member, com.idanalytics.products.enhancedfraud.result.ReasonCodeDocument.ReasonCode.Member2
    {
        private static final long serialVersionUID = 1L;
        
        public ReasonCodeImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected ReasonCodeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
        /**
         * An anonymous inner XML type.
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.enhancedfraud.result.ReasonCodeDocument$ReasonCode$Member.
         */
        public static class MemberImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.enhancedfraud.result.ReasonCodeDocument.ReasonCode.Member
        {
            private static final long serialVersionUID = 1L;
            
            public MemberImpl(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected MemberImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
        /**
         * An anonymous inner XML type.
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.enhancedfraud.result.ReasonCodeDocument$ReasonCode$Member2.
         */
        public static class MemberImpl2 extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.enhancedfraud.result.ReasonCodeDocument.ReasonCode.Member2
        {
            private static final long serialVersionUID = 1L;
            
            public MemberImpl2(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected MemberImpl2(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
    }
}
