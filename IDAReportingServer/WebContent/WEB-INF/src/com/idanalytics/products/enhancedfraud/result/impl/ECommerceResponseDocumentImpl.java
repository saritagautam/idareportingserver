/*
 * An XML document type.
 * Localname: ECommerceResponse
 * Namespace: http://idanalytics.com/products/enhancedfraud/result
 * Java type: com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.enhancedfraud.result.impl;
/**
 * A document containing one ECommerceResponse(@http://idanalytics.com/products/enhancedfraud/result) element.
 *
 * This is a complex type.
 */
public class ECommerceResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument
{
    private static final long serialVersionUID = 1L;
    
    public ECommerceResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ECOMMERCERESPONSE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/enhancedfraud/result", "ECommerceResponse");
    
    
    /**
     * Gets the "ECommerceResponse" element
     */
    public com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument.ECommerceResponse getECommerceResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument.ECommerceResponse target = null;
            target = (com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument.ECommerceResponse)get_store().find_element_user(ECOMMERCERESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "ECommerceResponse" element
     */
    public void setECommerceResponse(com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument.ECommerceResponse eCommerceResponse)
    {
        generatedSetterHelperImpl(eCommerceResponse, ECOMMERCERESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "ECommerceResponse" element
     */
    public com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument.ECommerceResponse addNewECommerceResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument.ECommerceResponse target = null;
            target = (com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument.ECommerceResponse)get_store().add_element_user(ECOMMERCERESPONSE$0);
            return target;
        }
    }
    /**
     * An XML ECommerceResponse(@http://idanalytics.com/products/enhancedfraud/result).
     *
     * This is a complex type.
     */
    public static class ECommerceResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument.ECommerceResponse
    {
        private static final long serialVersionUID = 1L;
        
        public ECommerceResponseImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName MERCHANTID$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/enhancedfraud/result", "MerchantID");
        private static final javax.xml.namespace.QName ORDERNUMBER$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/enhancedfraud/result", "OrderNumber");
        private static final javax.xml.namespace.QName BILLTORESULT$4 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/enhancedfraud/result", "Bill-to-Result");
        private static final javax.xml.namespace.QName SHIPTORESULT$6 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/enhancedfraud/result", "Ship-to-Result");
        private static final javax.xml.namespace.QName IPADDRESS$8 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/enhancedfraud/result", "IPAddress");
        
        
        /**
         * Gets the "MerchantID" element
         */
        public java.lang.String getMerchantID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MERCHANTID$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "MerchantID" element
         */
        public com.idanalytics.products.enhancedfraud.result.MerchantID xgetMerchantID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.enhancedfraud.result.MerchantID target = null;
                target = (com.idanalytics.products.enhancedfraud.result.MerchantID)get_store().find_element_user(MERCHANTID$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "MerchantID" element
         */
        public void setMerchantID(java.lang.String merchantID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MERCHANTID$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(MERCHANTID$0);
                }
                target.setStringValue(merchantID);
            }
        }
        
        /**
         * Sets (as xml) the "MerchantID" element
         */
        public void xsetMerchantID(com.idanalytics.products.enhancedfraud.result.MerchantID merchantID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.enhancedfraud.result.MerchantID target = null;
                target = (com.idanalytics.products.enhancedfraud.result.MerchantID)get_store().find_element_user(MERCHANTID$0, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.enhancedfraud.result.MerchantID)get_store().add_element_user(MERCHANTID$0);
                }
                target.set(merchantID);
            }
        }
        
        /**
         * Gets the "OrderNumber" element
         */
        public java.lang.String getOrderNumber()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ORDERNUMBER$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "OrderNumber" element
         */
        public com.idanalytics.products.enhancedfraud.result.OrderNumber xgetOrderNumber()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.enhancedfraud.result.OrderNumber target = null;
                target = (com.idanalytics.products.enhancedfraud.result.OrderNumber)get_store().find_element_user(ORDERNUMBER$2, 0);
                return target;
            }
        }
        
        /**
         * Sets the "OrderNumber" element
         */
        public void setOrderNumber(java.lang.String orderNumber)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ORDERNUMBER$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ORDERNUMBER$2);
                }
                target.setStringValue(orderNumber);
            }
        }
        
        /**
         * Sets (as xml) the "OrderNumber" element
         */
        public void xsetOrderNumber(com.idanalytics.products.enhancedfraud.result.OrderNumber orderNumber)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.enhancedfraud.result.OrderNumber target = null;
                target = (com.idanalytics.products.enhancedfraud.result.OrderNumber)get_store().find_element_user(ORDERNUMBER$2, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.enhancedfraud.result.OrderNumber)get_store().add_element_user(ORDERNUMBER$2);
                }
                target.set(orderNumber);
            }
        }
        
        /**
         * Gets the "Bill-to-Result" element
         */
        public com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult getBillToResult()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult target = null;
                target = (com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult)get_store().find_element_user(BILLTORESULT$4, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * Sets the "Bill-to-Result" element
         */
        public void setBillToResult(com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult billToResult)
        {
            generatedSetterHelperImpl(billToResult, BILLTORESULT$4, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "Bill-to-Result" element
         */
        public com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult addNewBillToResult()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult target = null;
                target = (com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult)get_store().add_element_user(BILLTORESULT$4);
                return target;
            }
        }
        
        /**
         * Gets array of all "Ship-to-Result" elements
         */
        public com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult[] getShipToResultArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(SHIPTORESULT$6, targetList);
                com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult[] result = new com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "Ship-to-Result" element
         */
        public com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult getShipToResultArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult target = null;
                target = (com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult)get_store().find_element_user(SHIPTORESULT$6, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "Ship-to-Result" element
         */
        public int sizeOfShipToResultArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(SHIPTORESULT$6);
            }
        }
        
        /**
         * Sets array of all "Ship-to-Result" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setShipToResultArray(com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult[] shipToResultArray)
        {
            check_orphaned();
            arraySetterHelper(shipToResultArray, SHIPTORESULT$6);
        }
        
        /**
         * Sets ith "Ship-to-Result" element
         */
        public void setShipToResultArray(int i, com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult shipToResult)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult target = null;
                target = (com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult)get_store().find_element_user(SHIPTORESULT$6, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(shipToResult);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "Ship-to-Result" element
         */
        public com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult insertNewShipToResult(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult target = null;
                target = (com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult)get_store().insert_element_user(SHIPTORESULT$6, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "Ship-to-Result" element
         */
        public com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult addNewShipToResult()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult target = null;
                target = (com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult)get_store().add_element_user(SHIPTORESULT$6);
                return target;
            }
        }
        
        /**
         * Removes the ith "Ship-to-Result" element
         */
        public void removeShipToResult(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(SHIPTORESULT$6, i);
            }
        }
        
        /**
         * Gets the "IPAddress" element
         */
        public com.idanalytics.products.enhancedfraud.result.ElementResultType getIPAddress()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.enhancedfraud.result.ElementResultType target = null;
                target = (com.idanalytics.products.enhancedfraud.result.ElementResultType)get_store().find_element_user(IPADDRESS$8, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * Sets the "IPAddress" element
         */
        public void setIPAddress(com.idanalytics.products.enhancedfraud.result.ElementResultType ipAddress)
        {
            generatedSetterHelperImpl(ipAddress, IPADDRESS$8, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "IPAddress" element
         */
        public com.idanalytics.products.enhancedfraud.result.ElementResultType addNewIPAddress()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.enhancedfraud.result.ElementResultType target = null;
                target = (com.idanalytics.products.enhancedfraud.result.ElementResultType)get_store().add_element_user(IPADDRESS$8);
                return target;
            }
        }
    }
}
