/*
 * XML Type:  EnhancedFraudHitResult
 * Namespace: http://idanalytics.com/products/enhancedfraud/result
 * Java type: com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.enhancedfraud.result.impl;
/**
 * An XML EnhancedFraudHitResult(@http://idanalytics.com/products/enhancedfraud/result).
 *
 * This is a complex type.
 */
public class EnhancedFraudHitResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult
{
    private static final long serialVersionUID = 1L;
    
    public EnhancedFraudHitResultImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName NAMERESULT$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/enhancedfraud/result", "NameResult");
    private static final javax.xml.namespace.QName ADDRESSRESULT$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/enhancedfraud/result", "AddressResult");
    private static final javax.xml.namespace.QName PHONERESULT$4 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/enhancedfraud/result", "PhoneResult");
    private static final javax.xml.namespace.QName EMAILRESULT$6 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/enhancedfraud/result", "EmailResult");
    private static final javax.xml.namespace.QName IDENTITYRESULT$8 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/enhancedfraud/result", "IdentityResult");
    
    
    /**
     * Gets the "NameResult" element
     */
    public com.idanalytics.products.enhancedfraud.result.ElementResultType getNameResult()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.enhancedfraud.result.ElementResultType target = null;
            target = (com.idanalytics.products.enhancedfraud.result.ElementResultType)get_store().find_element_user(NAMERESULT$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "NameResult" element
     */
    public void setNameResult(com.idanalytics.products.enhancedfraud.result.ElementResultType nameResult)
    {
        generatedSetterHelperImpl(nameResult, NAMERESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "NameResult" element
     */
    public com.idanalytics.products.enhancedfraud.result.ElementResultType addNewNameResult()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.enhancedfraud.result.ElementResultType target = null;
            target = (com.idanalytics.products.enhancedfraud.result.ElementResultType)get_store().add_element_user(NAMERESULT$0);
            return target;
        }
    }
    
    /**
     * Gets the "AddressResult" element
     */
    public com.idanalytics.products.enhancedfraud.result.ElementResultType getAddressResult()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.enhancedfraud.result.ElementResultType target = null;
            target = (com.idanalytics.products.enhancedfraud.result.ElementResultType)get_store().find_element_user(ADDRESSRESULT$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "AddressResult" element
     */
    public void setAddressResult(com.idanalytics.products.enhancedfraud.result.ElementResultType addressResult)
    {
        generatedSetterHelperImpl(addressResult, ADDRESSRESULT$2, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "AddressResult" element
     */
    public com.idanalytics.products.enhancedfraud.result.ElementResultType addNewAddressResult()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.enhancedfraud.result.ElementResultType target = null;
            target = (com.idanalytics.products.enhancedfraud.result.ElementResultType)get_store().add_element_user(ADDRESSRESULT$2);
            return target;
        }
    }
    
    /**
     * Gets the "PhoneResult" element
     */
    public com.idanalytics.products.enhancedfraud.result.ElementResultType getPhoneResult()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.enhancedfraud.result.ElementResultType target = null;
            target = (com.idanalytics.products.enhancedfraud.result.ElementResultType)get_store().find_element_user(PHONERESULT$4, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "PhoneResult" element
     */
    public void setPhoneResult(com.idanalytics.products.enhancedfraud.result.ElementResultType phoneResult)
    {
        generatedSetterHelperImpl(phoneResult, PHONERESULT$4, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "PhoneResult" element
     */
    public com.idanalytics.products.enhancedfraud.result.ElementResultType addNewPhoneResult()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.enhancedfraud.result.ElementResultType target = null;
            target = (com.idanalytics.products.enhancedfraud.result.ElementResultType)get_store().add_element_user(PHONERESULT$4);
            return target;
        }
    }
    
    /**
     * Gets the "EmailResult" element
     */
    public com.idanalytics.products.enhancedfraud.result.ElementResultType getEmailResult()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.enhancedfraud.result.ElementResultType target = null;
            target = (com.idanalytics.products.enhancedfraud.result.ElementResultType)get_store().find_element_user(EMAILRESULT$6, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "EmailResult" element
     */
    public void setEmailResult(com.idanalytics.products.enhancedfraud.result.ElementResultType emailResult)
    {
        generatedSetterHelperImpl(emailResult, EMAILRESULT$6, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "EmailResult" element
     */
    public com.idanalytics.products.enhancedfraud.result.ElementResultType addNewEmailResult()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.enhancedfraud.result.ElementResultType target = null;
            target = (com.idanalytics.products.enhancedfraud.result.ElementResultType)get_store().add_element_user(EMAILRESULT$6);
            return target;
        }
    }
    
    /**
     * Gets the "IdentityResult" element
     */
    public com.idanalytics.products.enhancedfraud.result.ElementResultType getIdentityResult()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.enhancedfraud.result.ElementResultType target = null;
            target = (com.idanalytics.products.enhancedfraud.result.ElementResultType)get_store().find_element_user(IDENTITYRESULT$8, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "IdentityResult" element
     */
    public void setIdentityResult(com.idanalytics.products.enhancedfraud.result.ElementResultType identityResult)
    {
        generatedSetterHelperImpl(identityResult, IDENTITYRESULT$8, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "IdentityResult" element
     */
    public com.idanalytics.products.enhancedfraud.result.ElementResultType addNewIdentityResult()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.enhancedfraud.result.ElementResultType target = null;
            target = (com.idanalytics.products.enhancedfraud.result.ElementResultType)get_store().add_element_user(IDENTITYRESULT$8);
            return target;
        }
    }
}
