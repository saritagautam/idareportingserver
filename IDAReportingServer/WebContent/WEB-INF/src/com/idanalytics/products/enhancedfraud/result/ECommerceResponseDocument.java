/*
 * An XML document type.
 * Localname: ECommerceResponse
 * Namespace: http://idanalytics.com/products/enhancedfraud/result
 * Java type: com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.enhancedfraud.result;


/**
 * A document containing one ECommerceResponse(@http://idanalytics.com/products/enhancedfraud/result) element.
 *
 * This is a complex type.
 */
public interface ECommerceResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ECommerceResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("ecommerceresponsed11edoctype");
    
    /**
     * Gets the "ECommerceResponse" element
     */
    com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument.ECommerceResponse getECommerceResponse();
    
    /**
     * Sets the "ECommerceResponse" element
     */
    void setECommerceResponse(com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument.ECommerceResponse eCommerceResponse);
    
    /**
     * Appends and returns a new empty "ECommerceResponse" element
     */
    com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument.ECommerceResponse addNewECommerceResponse();
    
    /**
     * An XML ECommerceResponse(@http://idanalytics.com/products/enhancedfraud/result).
     *
     * This is a complex type.
     */
    public interface ECommerceResponse extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ECommerceResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("ecommerceresponse6ab1elemtype");
        
        /**
         * Gets the "MerchantID" element
         */
        java.lang.String getMerchantID();
        
        /**
         * Gets (as xml) the "MerchantID" element
         */
        com.idanalytics.products.enhancedfraud.result.MerchantID xgetMerchantID();
        
        /**
         * Sets the "MerchantID" element
         */
        void setMerchantID(java.lang.String merchantID);
        
        /**
         * Sets (as xml) the "MerchantID" element
         */
        void xsetMerchantID(com.idanalytics.products.enhancedfraud.result.MerchantID merchantID);
        
        /**
         * Gets the "OrderNumber" element
         */
        java.lang.String getOrderNumber();
        
        /**
         * Gets (as xml) the "OrderNumber" element
         */
        com.idanalytics.products.enhancedfraud.result.OrderNumber xgetOrderNumber();
        
        /**
         * Sets the "OrderNumber" element
         */
        void setOrderNumber(java.lang.String orderNumber);
        
        /**
         * Sets (as xml) the "OrderNumber" element
         */
        void xsetOrderNumber(com.idanalytics.products.enhancedfraud.result.OrderNumber orderNumber);
        
        /**
         * Gets the "Bill-to-Result" element
         */
        com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult getBillToResult();
        
        /**
         * Sets the "Bill-to-Result" element
         */
        void setBillToResult(com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult billToResult);
        
        /**
         * Appends and returns a new empty "Bill-to-Result" element
         */
        com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult addNewBillToResult();
        
        /**
         * Gets array of all "Ship-to-Result" elements
         */
        com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult[] getShipToResultArray();
        
        /**
         * Gets ith "Ship-to-Result" element
         */
        com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult getShipToResultArray(int i);
        
        /**
         * Returns number of "Ship-to-Result" element
         */
        int sizeOfShipToResultArray();
        
        /**
         * Sets array of all "Ship-to-Result" element
         */
        void setShipToResultArray(com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult[] shipToResultArray);
        
        /**
         * Sets ith "Ship-to-Result" element
         */
        void setShipToResultArray(int i, com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult shipToResult);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "Ship-to-Result" element
         */
        com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult insertNewShipToResult(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "Ship-to-Result" element
         */
        com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult addNewShipToResult();
        
        /**
         * Removes the ith "Ship-to-Result" element
         */
        void removeShipToResult(int i);
        
        /**
         * Gets the "IPAddress" element
         */
        com.idanalytics.products.enhancedfraud.result.ElementResultType getIPAddress();
        
        /**
         * Sets the "IPAddress" element
         */
        void setIPAddress(com.idanalytics.products.enhancedfraud.result.ElementResultType ipAddress);
        
        /**
         * Appends and returns a new empty "IPAddress" element
         */
        com.idanalytics.products.enhancedfraud.result.ElementResultType addNewIPAddress();
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument.ECommerceResponse newInstance() {
              return (com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument.ECommerceResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument.ECommerceResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument.ECommerceResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument newInstance() {
          return (com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
