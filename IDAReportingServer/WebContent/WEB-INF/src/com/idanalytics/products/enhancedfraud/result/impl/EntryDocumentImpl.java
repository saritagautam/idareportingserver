/*
 * An XML document type.
 * Localname: Entry
 * Namespace: http://idanalytics.com/products/enhancedfraud/result
 * Java type: com.idanalytics.products.enhancedfraud.result.EntryDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.enhancedfraud.result.impl;
/**
 * A document containing one Entry(@http://idanalytics.com/products/enhancedfraud/result) element.
 *
 * This is a complex type.
 */
public class EntryDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.enhancedfraud.result.EntryDocument
{
    private static final long serialVersionUID = 1L;
    
    public EntryDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ENTRY$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/enhancedfraud/result", "Entry");
    
    
    /**
     * Gets the "Entry" element
     */
    public com.idanalytics.products.enhancedfraud.result.EntryDocument.Entry getEntry()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.enhancedfraud.result.EntryDocument.Entry target = null;
            target = (com.idanalytics.products.enhancedfraud.result.EntryDocument.Entry)get_store().find_element_user(ENTRY$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "Entry" element
     */
    public void setEntry(com.idanalytics.products.enhancedfraud.result.EntryDocument.Entry entry)
    {
        generatedSetterHelperImpl(entry, ENTRY$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "Entry" element
     */
    public com.idanalytics.products.enhancedfraud.result.EntryDocument.Entry addNewEntry()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.enhancedfraud.result.EntryDocument.Entry target = null;
            target = (com.idanalytics.products.enhancedfraud.result.EntryDocument.Entry)get_store().add_element_user(ENTRY$0);
            return target;
        }
    }
    /**
     * An XML Entry(@http://idanalytics.com/products/enhancedfraud/result).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.enhancedfraud.result.EntryDocument$Entry.
     */
    public static class EntryImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.enhancedfraud.result.EntryDocument.Entry
    {
        private static final long serialVersionUID = 1L;
        
        public EntryImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, true);
        }
        
        protected EntryImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
        
        private static final javax.xml.namespace.QName NAME$0 = 
            new javax.xml.namespace.QName("", "name");
        
        
        /**
         * Gets the "name" attribute
         */
        public java.lang.String getName()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(NAME$0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "name" attribute
         */
        public org.apache.xmlbeans.XmlString xgetName()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_attribute_user(NAME$0);
                return target;
            }
        }
        
        /**
         * Sets the "name" attribute
         */
        public void setName(java.lang.String name)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(NAME$0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(NAME$0);
                }
                target.setStringValue(name);
            }
        }
        
        /**
         * Sets (as xml) the "name" attribute
         */
        public void xsetName(org.apache.xmlbeans.XmlString name)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_attribute_user(NAME$0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlString)get_store().add_attribute_user(NAME$0);
                }
                target.set(name);
            }
        }
    }
}
