/*
 * An XML document type.
 * Localname: IDAInternal
 * Namespace: http://idanalytics.com/products/enhancedfraud/result
 * Java type: com.idanalytics.products.enhancedfraud.result.IDAInternalDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.enhancedfraud.result.impl;
/**
 * A document containing one IDAInternal(@http://idanalytics.com/products/enhancedfraud/result) element.
 *
 * This is a complex type.
 */
public class IDAInternalDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.enhancedfraud.result.IDAInternalDocument
{
    private static final long serialVersionUID = 1L;
    
    public IDAInternalDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IDAINTERNAL$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/enhancedfraud/result", "IDAInternal");
    
    
    /**
     * Gets the "IDAInternal" element
     */
    public com.idanalytics.products.enhancedfraud.result.IDAInternalDocument.IDAInternal getIDAInternal()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.enhancedfraud.result.IDAInternalDocument.IDAInternal target = null;
            target = (com.idanalytics.products.enhancedfraud.result.IDAInternalDocument.IDAInternal)get_store().find_element_user(IDAINTERNAL$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "IDAInternal" element
     */
    public void setIDAInternal(com.idanalytics.products.enhancedfraud.result.IDAInternalDocument.IDAInternal idaInternal)
    {
        generatedSetterHelperImpl(idaInternal, IDAINTERNAL$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "IDAInternal" element
     */
    public com.idanalytics.products.enhancedfraud.result.IDAInternalDocument.IDAInternal addNewIDAInternal()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.enhancedfraud.result.IDAInternalDocument.IDAInternal target = null;
            target = (com.idanalytics.products.enhancedfraud.result.IDAInternalDocument.IDAInternal)get_store().add_element_user(IDAINTERNAL$0);
            return target;
        }
    }
    /**
     * An XML IDAInternal(@http://idanalytics.com/products/enhancedfraud/result).
     *
     * This is a complex type.
     */
    public static class IDAInternalImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.enhancedfraud.result.IDAInternalDocument.IDAInternal
    {
        private static final long serialVersionUID = 1L;
        
        public IDAInternalImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName GROUP$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/enhancedfraud/result", "Group");
        private static final javax.xml.namespace.QName XML$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/enhancedfraud/result", "Xml");
        
        
        /**
         * Gets array of all "Group" elements
         */
        public com.idanalytics.products.enhancedfraud.result.GroupDocument.Group[] getGroupArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(GROUP$0, targetList);
                com.idanalytics.products.enhancedfraud.result.GroupDocument.Group[] result = new com.idanalytics.products.enhancedfraud.result.GroupDocument.Group[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "Group" element
         */
        public com.idanalytics.products.enhancedfraud.result.GroupDocument.Group getGroupArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.enhancedfraud.result.GroupDocument.Group target = null;
                target = (com.idanalytics.products.enhancedfraud.result.GroupDocument.Group)get_store().find_element_user(GROUP$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "Group" element
         */
        public int sizeOfGroupArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(GROUP$0);
            }
        }
        
        /**
         * Sets array of all "Group" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setGroupArray(com.idanalytics.products.enhancedfraud.result.GroupDocument.Group[] groupArray)
        {
            check_orphaned();
            arraySetterHelper(groupArray, GROUP$0);
        }
        
        /**
         * Sets ith "Group" element
         */
        public void setGroupArray(int i, com.idanalytics.products.enhancedfraud.result.GroupDocument.Group group)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.enhancedfraud.result.GroupDocument.Group target = null;
                target = (com.idanalytics.products.enhancedfraud.result.GroupDocument.Group)get_store().find_element_user(GROUP$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(group);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "Group" element
         */
        public com.idanalytics.products.enhancedfraud.result.GroupDocument.Group insertNewGroup(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.enhancedfraud.result.GroupDocument.Group target = null;
                target = (com.idanalytics.products.enhancedfraud.result.GroupDocument.Group)get_store().insert_element_user(GROUP$0, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "Group" element
         */
        public com.idanalytics.products.enhancedfraud.result.GroupDocument.Group addNewGroup()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.enhancedfraud.result.GroupDocument.Group target = null;
                target = (com.idanalytics.products.enhancedfraud.result.GroupDocument.Group)get_store().add_element_user(GROUP$0);
                return target;
            }
        }
        
        /**
         * Removes the ith "Group" element
         */
        public void removeGroup(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(GROUP$0, i);
            }
        }
        
        /**
         * Gets array of all "Xml" elements
         */
        public com.idanalytics.products.enhancedfraud.result.XmlInternal[] getXmlArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(XML$2, targetList);
                com.idanalytics.products.enhancedfraud.result.XmlInternal[] result = new com.idanalytics.products.enhancedfraud.result.XmlInternal[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "Xml" element
         */
        public com.idanalytics.products.enhancedfraud.result.XmlInternal getXmlArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.enhancedfraud.result.XmlInternal target = null;
                target = (com.idanalytics.products.enhancedfraud.result.XmlInternal)get_store().find_element_user(XML$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "Xml" element
         */
        public int sizeOfXmlArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(XML$2);
            }
        }
        
        /**
         * Sets array of all "Xml" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setXmlArray(com.idanalytics.products.enhancedfraud.result.XmlInternal[] xmlArray)
        {
            check_orphaned();
            arraySetterHelper(xmlArray, XML$2);
        }
        
        /**
         * Sets ith "Xml" element
         */
        public void setXmlArray(int i, com.idanalytics.products.enhancedfraud.result.XmlInternal xml)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.enhancedfraud.result.XmlInternal target = null;
                target = (com.idanalytics.products.enhancedfraud.result.XmlInternal)get_store().find_element_user(XML$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(xml);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "Xml" element
         */
        public com.idanalytics.products.enhancedfraud.result.XmlInternal insertNewXml(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.enhancedfraud.result.XmlInternal target = null;
                target = (com.idanalytics.products.enhancedfraud.result.XmlInternal)get_store().insert_element_user(XML$2, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "Xml" element
         */
        public com.idanalytics.products.enhancedfraud.result.XmlInternal addNewXml()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.enhancedfraud.result.XmlInternal target = null;
                target = (com.idanalytics.products.enhancedfraud.result.XmlInternal)get_store().add_element_user(XML$2);
                return target;
            }
        }
        
        /**
         * Removes the ith "Xml" element
         */
        public void removeXml(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(XML$2, i);
            }
        }
    }
}
