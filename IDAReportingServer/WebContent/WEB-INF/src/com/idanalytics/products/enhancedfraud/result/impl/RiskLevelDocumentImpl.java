/*
 * An XML document type.
 * Localname: RiskLevel
 * Namespace: http://idanalytics.com/products/enhancedfraud/result
 * Java type: com.idanalytics.products.enhancedfraud.result.RiskLevelDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.enhancedfraud.result.impl;
/**
 * A document containing one RiskLevel(@http://idanalytics.com/products/enhancedfraud/result) element.
 *
 * This is a complex type.
 */
public class RiskLevelDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.enhancedfraud.result.RiskLevelDocument
{
    private static final long serialVersionUID = 1L;
    
    public RiskLevelDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName RISKLEVEL$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/enhancedfraud/result", "RiskLevel");
    
    
    /**
     * Gets the "RiskLevel" element
     */
    public com.idanalytics.products.enhancedfraud.result.RiskLevelDocument.RiskLevel.Enum getRiskLevel()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RISKLEVEL$0, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.enhancedfraud.result.RiskLevelDocument.RiskLevel.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "RiskLevel" element
     */
    public com.idanalytics.products.enhancedfraud.result.RiskLevelDocument.RiskLevel xgetRiskLevel()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.enhancedfraud.result.RiskLevelDocument.RiskLevel target = null;
            target = (com.idanalytics.products.enhancedfraud.result.RiskLevelDocument.RiskLevel)get_store().find_element_user(RISKLEVEL$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "RiskLevel" element
     */
    public void setRiskLevel(com.idanalytics.products.enhancedfraud.result.RiskLevelDocument.RiskLevel.Enum riskLevel)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RISKLEVEL$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(RISKLEVEL$0);
            }
            target.setEnumValue(riskLevel);
        }
    }
    
    /**
     * Sets (as xml) the "RiskLevel" element
     */
    public void xsetRiskLevel(com.idanalytics.products.enhancedfraud.result.RiskLevelDocument.RiskLevel riskLevel)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.enhancedfraud.result.RiskLevelDocument.RiskLevel target = null;
            target = (com.idanalytics.products.enhancedfraud.result.RiskLevelDocument.RiskLevel)get_store().find_element_user(RISKLEVEL$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.enhancedfraud.result.RiskLevelDocument.RiskLevel)get_store().add_element_user(RISKLEVEL$0);
            }
            target.set(riskLevel);
        }
    }
    /**
     * An XML RiskLevel(@http://idanalytics.com/products/enhancedfraud/result).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.enhancedfraud.result.RiskLevelDocument$RiskLevel.
     */
    public static class RiskLevelImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.idanalytics.products.enhancedfraud.result.RiskLevelDocument.RiskLevel
    {
        private static final long serialVersionUID = 1L;
        
        public RiskLevelImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected RiskLevelImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
