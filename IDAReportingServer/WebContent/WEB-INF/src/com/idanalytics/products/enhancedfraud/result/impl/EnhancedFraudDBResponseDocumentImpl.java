/*
 * An XML document type.
 * Localname: EnhancedFraudDBResponse
 * Namespace: http://idanalytics.com/products/enhancedfraud/result
 * Java type: com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.enhancedfraud.result.impl;
/**
 * A document containing one EnhancedFraudDBResponse(@http://idanalytics.com/products/enhancedfraud/result) element.
 *
 * This is a complex type.
 */
public class EnhancedFraudDBResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument
{
    private static final long serialVersionUID = 1L;
    
    public EnhancedFraudDBResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ENHANCEDFRAUDDBRESPONSE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/enhancedfraud/result", "EnhancedFraudDBResponse");
    
    
    /**
     * Gets the "EnhancedFraudDBResponse" element
     */
    public com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument.EnhancedFraudDBResponse getEnhancedFraudDBResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument.EnhancedFraudDBResponse target = null;
            target = (com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument.EnhancedFraudDBResponse)get_store().find_element_user(ENHANCEDFRAUDDBRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "EnhancedFraudDBResponse" element
     */
    public void setEnhancedFraudDBResponse(com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument.EnhancedFraudDBResponse enhancedFraudDBResponse)
    {
        generatedSetterHelperImpl(enhancedFraudDBResponse, ENHANCEDFRAUDDBRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "EnhancedFraudDBResponse" element
     */
    public com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument.EnhancedFraudDBResponse addNewEnhancedFraudDBResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument.EnhancedFraudDBResponse target = null;
            target = (com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument.EnhancedFraudDBResponse)get_store().add_element_user(ENHANCEDFRAUDDBRESPONSE$0);
            return target;
        }
    }
    /**
     * An XML EnhancedFraudDBResponse(@http://idanalytics.com/products/enhancedfraud/result).
     *
     * This is a complex type.
     */
    public static class EnhancedFraudDBResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument.EnhancedFraudDBResponse
    {
        private static final long serialVersionUID = 1L;
        
        public EnhancedFraudDBResponseImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName IDASTATUS$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/enhancedfraud/result", "IDAStatus");
        private static final javax.xml.namespace.QName HITSTATUS$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/enhancedfraud/result", "HitStatus");
        private static final javax.xml.namespace.QName APPID$4 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/enhancedfraud/result", "AppID");
        private static final javax.xml.namespace.QName DESIGNATION$6 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/enhancedfraud/result", "Designation");
        private static final javax.xml.namespace.QName ACCTLINKKEY$8 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/enhancedfraud/result", "AcctLinkKey");
        private static final javax.xml.namespace.QName IDASEQUENCE$10 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/enhancedfraud/result", "IDASequence");
        private static final javax.xml.namespace.QName IDATIMESTAMP$12 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/enhancedfraud/result", "IDATimeStamp");
        private static final javax.xml.namespace.QName ECOMMERCERESPONSE$14 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/enhancedfraud/result", "ECommerceResponse");
        private static final javax.xml.namespace.QName IDAINTERNAL$16 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/enhancedfraud/result", "IDAInternal");
        
        
        /**
         * Gets the "IDAStatus" element
         */
        public int getIDAStatus()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASTATUS$0, 0);
                if (target == null)
                {
                    return 0;
                }
                return target.getIntValue();
            }
        }
        
        /**
         * Gets (as xml) the "IDAStatus" element
         */
        public com.idanalytics.products.common_v1.IDAStatus xgetIDAStatus()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDAStatus target = null;
                target = (com.idanalytics.products.common_v1.IDAStatus)get_store().find_element_user(IDASTATUS$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "IDAStatus" element
         */
        public void setIDAStatus(int idaStatus)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASTATUS$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDASTATUS$0);
                }
                target.setIntValue(idaStatus);
            }
        }
        
        /**
         * Sets (as xml) the "IDAStatus" element
         */
        public void xsetIDAStatus(com.idanalytics.products.common_v1.IDAStatus idaStatus)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDAStatus target = null;
                target = (com.idanalytics.products.common_v1.IDAStatus)get_store().find_element_user(IDASTATUS$0, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.IDAStatus)get_store().add_element_user(IDASTATUS$0);
                }
                target.set(idaStatus);
            }
        }
        
        /**
         * Gets the "HitStatus" element
         */
        public java.lang.String getHitStatus()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(HITSTATUS$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "HitStatus" element
         */
        public org.apache.xmlbeans.XmlString xgetHitStatus()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(HITSTATUS$2, 0);
                return target;
            }
        }
        
        /**
         * True if has "HitStatus" element
         */
        public boolean isSetHitStatus()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(HITSTATUS$2) != 0;
            }
        }
        
        /**
         * Sets the "HitStatus" element
         */
        public void setHitStatus(java.lang.String hitStatus)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(HITSTATUS$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(HITSTATUS$2);
                }
                target.setStringValue(hitStatus);
            }
        }
        
        /**
         * Sets (as xml) the "HitStatus" element
         */
        public void xsetHitStatus(org.apache.xmlbeans.XmlString hitStatus)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(HITSTATUS$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(HITSTATUS$2);
                }
                target.set(hitStatus);
            }
        }
        
        /**
         * Unsets the "HitStatus" element
         */
        public void unsetHitStatus()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(HITSTATUS$2, 0);
            }
        }
        
        /**
         * Gets the "AppID" element
         */
        public java.lang.String getAppID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(APPID$4, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "AppID" element
         */
        public com.idanalytics.products.enhancedfraud.result.AppIDDocument.AppID xgetAppID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.enhancedfraud.result.AppIDDocument.AppID target = null;
                target = (com.idanalytics.products.enhancedfraud.result.AppIDDocument.AppID)get_store().find_element_user(APPID$4, 0);
                return target;
            }
        }
        
        /**
         * Sets the "AppID" element
         */
        public void setAppID(java.lang.String appID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(APPID$4, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(APPID$4);
                }
                target.setStringValue(appID);
            }
        }
        
        /**
         * Sets (as xml) the "AppID" element
         */
        public void xsetAppID(com.idanalytics.products.enhancedfraud.result.AppIDDocument.AppID appID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.enhancedfraud.result.AppIDDocument.AppID target = null;
                target = (com.idanalytics.products.enhancedfraud.result.AppIDDocument.AppID)get_store().find_element_user(APPID$4, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.enhancedfraud.result.AppIDDocument.AppID)get_store().add_element_user(APPID$4);
                }
                target.set(appID);
            }
        }
        
        /**
         * Gets the "Designation" element
         */
        public com.idanalytics.products.enhancedfraud.result.DesignationDocument.Designation.Enum getDesignation()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESIGNATION$6, 0);
                if (target == null)
                {
                    return null;
                }
                return (com.idanalytics.products.enhancedfraud.result.DesignationDocument.Designation.Enum)target.getEnumValue();
            }
        }
        
        /**
         * Gets (as xml) the "Designation" element
         */
        public com.idanalytics.products.enhancedfraud.result.DesignationDocument.Designation xgetDesignation()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.enhancedfraud.result.DesignationDocument.Designation target = null;
                target = (com.idanalytics.products.enhancedfraud.result.DesignationDocument.Designation)get_store().find_element_user(DESIGNATION$6, 0);
                return target;
            }
        }
        
        /**
         * True if has "Designation" element
         */
        public boolean isSetDesignation()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(DESIGNATION$6) != 0;
            }
        }
        
        /**
         * Sets the "Designation" element
         */
        public void setDesignation(com.idanalytics.products.enhancedfraud.result.DesignationDocument.Designation.Enum designation)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESIGNATION$6, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DESIGNATION$6);
                }
                target.setEnumValue(designation);
            }
        }
        
        /**
         * Sets (as xml) the "Designation" element
         */
        public void xsetDesignation(com.idanalytics.products.enhancedfraud.result.DesignationDocument.Designation designation)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.enhancedfraud.result.DesignationDocument.Designation target = null;
                target = (com.idanalytics.products.enhancedfraud.result.DesignationDocument.Designation)get_store().find_element_user(DESIGNATION$6, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.enhancedfraud.result.DesignationDocument.Designation)get_store().add_element_user(DESIGNATION$6);
                }
                target.set(designation);
            }
        }
        
        /**
         * Unsets the "Designation" element
         */
        public void unsetDesignation()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(DESIGNATION$6, 0);
            }
        }
        
        /**
         * Gets the "AcctLinkKey" element
         */
        public java.lang.String getAcctLinkKey()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ACCTLINKKEY$8, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "AcctLinkKey" element
         */
        public com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument.AcctLinkKey xgetAcctLinkKey()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument.AcctLinkKey target = null;
                target = (com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument.AcctLinkKey)get_store().find_element_user(ACCTLINKKEY$8, 0);
                return target;
            }
        }
        
        /**
         * True if has "AcctLinkKey" element
         */
        public boolean isSetAcctLinkKey()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(ACCTLINKKEY$8) != 0;
            }
        }
        
        /**
         * Sets the "AcctLinkKey" element
         */
        public void setAcctLinkKey(java.lang.String acctLinkKey)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ACCTLINKKEY$8, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ACCTLINKKEY$8);
                }
                target.setStringValue(acctLinkKey);
            }
        }
        
        /**
         * Sets (as xml) the "AcctLinkKey" element
         */
        public void xsetAcctLinkKey(com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument.AcctLinkKey acctLinkKey)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument.AcctLinkKey target = null;
                target = (com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument.AcctLinkKey)get_store().find_element_user(ACCTLINKKEY$8, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument.AcctLinkKey)get_store().add_element_user(ACCTLINKKEY$8);
                }
                target.set(acctLinkKey);
            }
        }
        
        /**
         * Unsets the "AcctLinkKey" element
         */
        public void unsetAcctLinkKey()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(ACCTLINKKEY$8, 0);
            }
        }
        
        /**
         * Gets the "IDASequence" element
         */
        public java.lang.String getIDASequence()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASEQUENCE$10, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "IDASequence" element
         */
        public com.idanalytics.products.common_v1.IDASequence xgetIDASequence()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDASequence target = null;
                target = (com.idanalytics.products.common_v1.IDASequence)get_store().find_element_user(IDASEQUENCE$10, 0);
                return target;
            }
        }
        
        /**
         * Sets the "IDASequence" element
         */
        public void setIDASequence(java.lang.String idaSequence)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASEQUENCE$10, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDASEQUENCE$10);
                }
                target.setStringValue(idaSequence);
            }
        }
        
        /**
         * Sets (as xml) the "IDASequence" element
         */
        public void xsetIDASequence(com.idanalytics.products.common_v1.IDASequence idaSequence)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDASequence target = null;
                target = (com.idanalytics.products.common_v1.IDASequence)get_store().find_element_user(IDASEQUENCE$10, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.IDASequence)get_store().add_element_user(IDASEQUENCE$10);
                }
                target.set(idaSequence);
            }
        }
        
        /**
         * Gets the "IDATimeStamp" element
         */
        public java.util.Calendar getIDATimeStamp()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDATIMESTAMP$12, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getCalendarValue();
            }
        }
        
        /**
         * Gets (as xml) the "IDATimeStamp" element
         */
        public com.idanalytics.products.common_v1.IDATimeStamp xgetIDATimeStamp()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDATimeStamp target = null;
                target = (com.idanalytics.products.common_v1.IDATimeStamp)get_store().find_element_user(IDATIMESTAMP$12, 0);
                return target;
            }
        }
        
        /**
         * Sets the "IDATimeStamp" element
         */
        public void setIDATimeStamp(java.util.Calendar idaTimeStamp)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDATIMESTAMP$12, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDATIMESTAMP$12);
                }
                target.setCalendarValue(idaTimeStamp);
            }
        }
        
        /**
         * Sets (as xml) the "IDATimeStamp" element
         */
        public void xsetIDATimeStamp(com.idanalytics.products.common_v1.IDATimeStamp idaTimeStamp)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDATimeStamp target = null;
                target = (com.idanalytics.products.common_v1.IDATimeStamp)get_store().find_element_user(IDATIMESTAMP$12, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.IDATimeStamp)get_store().add_element_user(IDATIMESTAMP$12);
                }
                target.set(idaTimeStamp);
            }
        }
        
        /**
         * Gets the "ECommerceResponse" element
         */
        public com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument.ECommerceResponse getECommerceResponse()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument.ECommerceResponse target = null;
                target = (com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument.ECommerceResponse)get_store().find_element_user(ECOMMERCERESPONSE$14, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "ECommerceResponse" element
         */
        public boolean isSetECommerceResponse()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(ECOMMERCERESPONSE$14) != 0;
            }
        }
        
        /**
         * Sets the "ECommerceResponse" element
         */
        public void setECommerceResponse(com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument.ECommerceResponse eCommerceResponse)
        {
            generatedSetterHelperImpl(eCommerceResponse, ECOMMERCERESPONSE$14, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "ECommerceResponse" element
         */
        public com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument.ECommerceResponse addNewECommerceResponse()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument.ECommerceResponse target = null;
                target = (com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument.ECommerceResponse)get_store().add_element_user(ECOMMERCERESPONSE$14);
                return target;
            }
        }
        
        /**
         * Unsets the "ECommerceResponse" element
         */
        public void unsetECommerceResponse()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(ECOMMERCERESPONSE$14, 0);
            }
        }
        
        /**
         * Gets the "IDAInternal" element
         */
        public com.idanalytics.products.enhancedfraud.result.IDAInternalDocument.IDAInternal getIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.enhancedfraud.result.IDAInternalDocument.IDAInternal target = null;
                target = (com.idanalytics.products.enhancedfraud.result.IDAInternalDocument.IDAInternal)get_store().find_element_user(IDAINTERNAL$16, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "IDAInternal" element
         */
        public boolean isSetIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(IDAINTERNAL$16) != 0;
            }
        }
        
        /**
         * Sets the "IDAInternal" element
         */
        public void setIDAInternal(com.idanalytics.products.enhancedfraud.result.IDAInternalDocument.IDAInternal idaInternal)
        {
            generatedSetterHelperImpl(idaInternal, IDAINTERNAL$16, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "IDAInternal" element
         */
        public com.idanalytics.products.enhancedfraud.result.IDAInternalDocument.IDAInternal addNewIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.enhancedfraud.result.IDAInternalDocument.IDAInternal target = null;
                target = (com.idanalytics.products.enhancedfraud.result.IDAInternalDocument.IDAInternal)get_store().add_element_user(IDAINTERNAL$16);
                return target;
            }
        }
        
        /**
         * Unsets the "IDAInternal" element
         */
        public void unsetIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(IDAINTERNAL$16, 0);
            }
        }
    }
}
