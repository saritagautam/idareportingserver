/*
 * An XML document type.
 * Localname: ReasonDesc
 * Namespace: http://idanalytics.com/products/enhancedfraud/result
 * Java type: com.idanalytics.products.enhancedfraud.result.ReasonDescDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.enhancedfraud.result.impl;
/**
 * A document containing one ReasonDesc(@http://idanalytics.com/products/enhancedfraud/result) element.
 *
 * This is a complex type.
 */
public class ReasonDescDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.enhancedfraud.result.ReasonDescDocument
{
    private static final long serialVersionUID = 1L;
    
    public ReasonDescDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName REASONDESC$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/enhancedfraud/result", "ReasonDesc");
    
    
    /**
     * Gets the "ReasonDesc" element
     */
    public java.lang.String getReasonDesc()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(REASONDESC$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "ReasonDesc" element
     */
    public com.idanalytics.products.enhancedfraud.result.ReasonDescDocument.ReasonDesc xgetReasonDesc()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.enhancedfraud.result.ReasonDescDocument.ReasonDesc target = null;
            target = (com.idanalytics.products.enhancedfraud.result.ReasonDescDocument.ReasonDesc)get_store().find_element_user(REASONDESC$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "ReasonDesc" element
     */
    public void setReasonDesc(java.lang.String reasonDesc)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(REASONDESC$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(REASONDESC$0);
            }
            target.setStringValue(reasonDesc);
        }
    }
    
    /**
     * Sets (as xml) the "ReasonDesc" element
     */
    public void xsetReasonDesc(com.idanalytics.products.enhancedfraud.result.ReasonDescDocument.ReasonDesc reasonDesc)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.enhancedfraud.result.ReasonDescDocument.ReasonDesc target = null;
            target = (com.idanalytics.products.enhancedfraud.result.ReasonDescDocument.ReasonDesc)get_store().find_element_user(REASONDESC$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.enhancedfraud.result.ReasonDescDocument.ReasonDesc)get_store().add_element_user(REASONDESC$0);
            }
            target.set(reasonDesc);
        }
    }
    /**
     * An XML ReasonDesc(@http://idanalytics.com/products/enhancedfraud/result).
     *
     * This is a union type. Instances are of one of the following types:
     *     com.idanalytics.products.enhancedfraud.result.ReasonDescDocument$ReasonDesc$Member
     *     com.idanalytics.products.enhancedfraud.result.ReasonDescDocument$ReasonDesc$Member2
     */
    public static class ReasonDescImpl extends org.apache.xmlbeans.impl.values.XmlUnionImpl implements com.idanalytics.products.enhancedfraud.result.ReasonDescDocument.ReasonDesc, com.idanalytics.products.enhancedfraud.result.ReasonDescDocument.ReasonDesc.Member, com.idanalytics.products.enhancedfraud.result.ReasonDescDocument.ReasonDesc.Member2
    {
        private static final long serialVersionUID = 1L;
        
        public ReasonDescImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected ReasonDescImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
        /**
         * An anonymous inner XML type.
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.enhancedfraud.result.ReasonDescDocument$ReasonDesc$Member.
         */
        public static class MemberImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.enhancedfraud.result.ReasonDescDocument.ReasonDesc.Member
        {
            private static final long serialVersionUID = 1L;
            
            public MemberImpl(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected MemberImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
        /**
         * An anonymous inner XML type.
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.enhancedfraud.result.ReasonDescDocument$ReasonDesc$Member2.
         */
        public static class MemberImpl2 extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.enhancedfraud.result.ReasonDescDocument.ReasonDesc.Member2
        {
            private static final long serialVersionUID = 1L;
            
            public MemberImpl2(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected MemberImpl2(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
    }
}
