/*
 * XML Type:  EnhancedFraudHitResult
 * Namespace: http://idanalytics.com/products/enhancedfraud/result
 * Java type: com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.enhancedfraud.result;


/**
 * An XML EnhancedFraudHitResult(@http://idanalytics.com/products/enhancedfraud/result).
 *
 * This is a complex type.
 */
public interface EnhancedFraudHitResult extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(EnhancedFraudHitResult.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("enhancedfraudhitresult2ddbtype");
    
    /**
     * Gets the "NameResult" element
     */
    com.idanalytics.products.enhancedfraud.result.ElementResultType getNameResult();
    
    /**
     * Sets the "NameResult" element
     */
    void setNameResult(com.idanalytics.products.enhancedfraud.result.ElementResultType nameResult);
    
    /**
     * Appends and returns a new empty "NameResult" element
     */
    com.idanalytics.products.enhancedfraud.result.ElementResultType addNewNameResult();
    
    /**
     * Gets the "AddressResult" element
     */
    com.idanalytics.products.enhancedfraud.result.ElementResultType getAddressResult();
    
    /**
     * Sets the "AddressResult" element
     */
    void setAddressResult(com.idanalytics.products.enhancedfraud.result.ElementResultType addressResult);
    
    /**
     * Appends and returns a new empty "AddressResult" element
     */
    com.idanalytics.products.enhancedfraud.result.ElementResultType addNewAddressResult();
    
    /**
     * Gets the "PhoneResult" element
     */
    com.idanalytics.products.enhancedfraud.result.ElementResultType getPhoneResult();
    
    /**
     * Sets the "PhoneResult" element
     */
    void setPhoneResult(com.idanalytics.products.enhancedfraud.result.ElementResultType phoneResult);
    
    /**
     * Appends and returns a new empty "PhoneResult" element
     */
    com.idanalytics.products.enhancedfraud.result.ElementResultType addNewPhoneResult();
    
    /**
     * Gets the "EmailResult" element
     */
    com.idanalytics.products.enhancedfraud.result.ElementResultType getEmailResult();
    
    /**
     * Sets the "EmailResult" element
     */
    void setEmailResult(com.idanalytics.products.enhancedfraud.result.ElementResultType emailResult);
    
    /**
     * Appends and returns a new empty "EmailResult" element
     */
    com.idanalytics.products.enhancedfraud.result.ElementResultType addNewEmailResult();
    
    /**
     * Gets the "IdentityResult" element
     */
    com.idanalytics.products.enhancedfraud.result.ElementResultType getIdentityResult();
    
    /**
     * Sets the "IdentityResult" element
     */
    void setIdentityResult(com.idanalytics.products.enhancedfraud.result.ElementResultType identityResult);
    
    /**
     * Appends and returns a new empty "IdentityResult" element
     */
    com.idanalytics.products.enhancedfraud.result.ElementResultType addNewIdentityResult();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult newInstance() {
          return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudHitResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
