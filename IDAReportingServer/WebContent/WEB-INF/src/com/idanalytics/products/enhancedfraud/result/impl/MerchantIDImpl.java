/*
 * XML Type:  MerchantID
 * Namespace: http://idanalytics.com/products/enhancedfraud/result
 * Java type: com.idanalytics.products.enhancedfraud.result.MerchantID
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.enhancedfraud.result.impl;
/**
 * An XML MerchantID(@http://idanalytics.com/products/enhancedfraud/result).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.enhancedfraud.result.MerchantID.
 */
public class MerchantIDImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.enhancedfraud.result.MerchantID
{
    private static final long serialVersionUID = 1L;
    
    public MerchantIDImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected MerchantIDImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
