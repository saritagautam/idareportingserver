/*
 * An XML document type.
 * Localname: AcctLinkKey
 * Namespace: http://idanalytics.com/products/enhancedfraud/result
 * Java type: com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.enhancedfraud.result;


/**
 * A document containing one AcctLinkKey(@http://idanalytics.com/products/enhancedfraud/result) element.
 *
 * This is a complex type.
 */
public interface AcctLinkKeyDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(AcctLinkKeyDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("acctlinkkey36cddoctype");
    
    /**
     * Gets the "AcctLinkKey" element
     */
    java.lang.String getAcctLinkKey();
    
    /**
     * Gets (as xml) the "AcctLinkKey" element
     */
    com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument.AcctLinkKey xgetAcctLinkKey();
    
    /**
     * Sets the "AcctLinkKey" element
     */
    void setAcctLinkKey(java.lang.String acctLinkKey);
    
    /**
     * Sets (as xml) the "AcctLinkKey" element
     */
    void xsetAcctLinkKey(com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument.AcctLinkKey acctLinkKey);
    
    /**
     * An XML AcctLinkKey(@http://idanalytics.com/products/enhancedfraud/result).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument$AcctLinkKey.
     */
    public interface AcctLinkKey extends org.apache.xmlbeans.XmlString
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(AcctLinkKey.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("acctlinkkeya54felemtype");
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument.AcctLinkKey newValue(java.lang.Object obj) {
              return (com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument.AcctLinkKey) type.newValue( obj ); }
            
            public static com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument.AcctLinkKey newInstance() {
              return (com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument.AcctLinkKey) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument.AcctLinkKey newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument.AcctLinkKey) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument newInstance() {
          return (com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
