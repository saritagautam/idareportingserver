/*
 * An XML document type.
 * Localname: EnhancedFraudDBResponse
 * Namespace: http://idanalytics.com/products/enhancedfraud/result
 * Java type: com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.enhancedfraud.result;


/**
 * A document containing one EnhancedFraudDBResponse(@http://idanalytics.com/products/enhancedfraud/result) element.
 *
 * This is a complex type.
 */
public interface EnhancedFraudDBResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(EnhancedFraudDBResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("enhancedfrauddbresponse76b2doctype");
    
    /**
     * Gets the "EnhancedFraudDBResponse" element
     */
    com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument.EnhancedFraudDBResponse getEnhancedFraudDBResponse();
    
    /**
     * Sets the "EnhancedFraudDBResponse" element
     */
    void setEnhancedFraudDBResponse(com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument.EnhancedFraudDBResponse enhancedFraudDBResponse);
    
    /**
     * Appends and returns a new empty "EnhancedFraudDBResponse" element
     */
    com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument.EnhancedFraudDBResponse addNewEnhancedFraudDBResponse();
    
    /**
     * An XML EnhancedFraudDBResponse(@http://idanalytics.com/products/enhancedfraud/result).
     *
     * This is a complex type.
     */
    public interface EnhancedFraudDBResponse extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(EnhancedFraudDBResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("enhancedfrauddbresponse9759elemtype");
        
        /**
         * Gets the "IDAStatus" element
         */
        int getIDAStatus();
        
        /**
         * Gets (as xml) the "IDAStatus" element
         */
        com.idanalytics.products.common_v1.IDAStatus xgetIDAStatus();
        
        /**
         * Sets the "IDAStatus" element
         */
        void setIDAStatus(int idaStatus);
        
        /**
         * Sets (as xml) the "IDAStatus" element
         */
        void xsetIDAStatus(com.idanalytics.products.common_v1.IDAStatus idaStatus);
        
        /**
         * Gets the "HitStatus" element
         */
        java.lang.String getHitStatus();
        
        /**
         * Gets (as xml) the "HitStatus" element
         */
        org.apache.xmlbeans.XmlString xgetHitStatus();
        
        /**
         * True if has "HitStatus" element
         */
        boolean isSetHitStatus();
        
        /**
         * Sets the "HitStatus" element
         */
        void setHitStatus(java.lang.String hitStatus);
        
        /**
         * Sets (as xml) the "HitStatus" element
         */
        void xsetHitStatus(org.apache.xmlbeans.XmlString hitStatus);
        
        /**
         * Unsets the "HitStatus" element
         */
        void unsetHitStatus();
        
        /**
         * Gets the "AppID" element
         */
        java.lang.String getAppID();
        
        /**
         * Gets (as xml) the "AppID" element
         */
        com.idanalytics.products.enhancedfraud.result.AppIDDocument.AppID xgetAppID();
        
        /**
         * Sets the "AppID" element
         */
        void setAppID(java.lang.String appID);
        
        /**
         * Sets (as xml) the "AppID" element
         */
        void xsetAppID(com.idanalytics.products.enhancedfraud.result.AppIDDocument.AppID appID);
        
        /**
         * Gets the "Designation" element
         */
        com.idanalytics.products.enhancedfraud.result.DesignationDocument.Designation.Enum getDesignation();
        
        /**
         * Gets (as xml) the "Designation" element
         */
        com.idanalytics.products.enhancedfraud.result.DesignationDocument.Designation xgetDesignation();
        
        /**
         * True if has "Designation" element
         */
        boolean isSetDesignation();
        
        /**
         * Sets the "Designation" element
         */
        void setDesignation(com.idanalytics.products.enhancedfraud.result.DesignationDocument.Designation.Enum designation);
        
        /**
         * Sets (as xml) the "Designation" element
         */
        void xsetDesignation(com.idanalytics.products.enhancedfraud.result.DesignationDocument.Designation designation);
        
        /**
         * Unsets the "Designation" element
         */
        void unsetDesignation();
        
        /**
         * Gets the "AcctLinkKey" element
         */
        java.lang.String getAcctLinkKey();
        
        /**
         * Gets (as xml) the "AcctLinkKey" element
         */
        com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument.AcctLinkKey xgetAcctLinkKey();
        
        /**
         * True if has "AcctLinkKey" element
         */
        boolean isSetAcctLinkKey();
        
        /**
         * Sets the "AcctLinkKey" element
         */
        void setAcctLinkKey(java.lang.String acctLinkKey);
        
        /**
         * Sets (as xml) the "AcctLinkKey" element
         */
        void xsetAcctLinkKey(com.idanalytics.products.enhancedfraud.result.AcctLinkKeyDocument.AcctLinkKey acctLinkKey);
        
        /**
         * Unsets the "AcctLinkKey" element
         */
        void unsetAcctLinkKey();
        
        /**
         * Gets the "IDASequence" element
         */
        java.lang.String getIDASequence();
        
        /**
         * Gets (as xml) the "IDASequence" element
         */
        com.idanalytics.products.common_v1.IDASequence xgetIDASequence();
        
        /**
         * Sets the "IDASequence" element
         */
        void setIDASequence(java.lang.String idaSequence);
        
        /**
         * Sets (as xml) the "IDASequence" element
         */
        void xsetIDASequence(com.idanalytics.products.common_v1.IDASequence idaSequence);
        
        /**
         * Gets the "IDATimeStamp" element
         */
        java.util.Calendar getIDATimeStamp();
        
        /**
         * Gets (as xml) the "IDATimeStamp" element
         */
        com.idanalytics.products.common_v1.IDATimeStamp xgetIDATimeStamp();
        
        /**
         * Sets the "IDATimeStamp" element
         */
        void setIDATimeStamp(java.util.Calendar idaTimeStamp);
        
        /**
         * Sets (as xml) the "IDATimeStamp" element
         */
        void xsetIDATimeStamp(com.idanalytics.products.common_v1.IDATimeStamp idaTimeStamp);
        
        /**
         * Gets the "ECommerceResponse" element
         */
        com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument.ECommerceResponse getECommerceResponse();
        
        /**
         * True if has "ECommerceResponse" element
         */
        boolean isSetECommerceResponse();
        
        /**
         * Sets the "ECommerceResponse" element
         */
        void setECommerceResponse(com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument.ECommerceResponse eCommerceResponse);
        
        /**
         * Appends and returns a new empty "ECommerceResponse" element
         */
        com.idanalytics.products.enhancedfraud.result.ECommerceResponseDocument.ECommerceResponse addNewECommerceResponse();
        
        /**
         * Unsets the "ECommerceResponse" element
         */
        void unsetECommerceResponse();
        
        /**
         * Gets the "IDAInternal" element
         */
        com.idanalytics.products.enhancedfraud.result.IDAInternalDocument.IDAInternal getIDAInternal();
        
        /**
         * True if has "IDAInternal" element
         */
        boolean isSetIDAInternal();
        
        /**
         * Sets the "IDAInternal" element
         */
        void setIDAInternal(com.idanalytics.products.enhancedfraud.result.IDAInternalDocument.IDAInternal idaInternal);
        
        /**
         * Appends and returns a new empty "IDAInternal" element
         */
        com.idanalytics.products.enhancedfraud.result.IDAInternalDocument.IDAInternal addNewIDAInternal();
        
        /**
         * Unsets the "IDAInternal" element
         */
        void unsetIDAInternal();
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument.EnhancedFraudDBResponse newInstance() {
              return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument.EnhancedFraudDBResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument.EnhancedFraudDBResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument.EnhancedFraudDBResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument newInstance() {
          return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.enhancedfraud.result.EnhancedFraudDBResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
