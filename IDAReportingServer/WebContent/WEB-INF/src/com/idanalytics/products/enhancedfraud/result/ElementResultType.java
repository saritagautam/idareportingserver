/*
 * XML Type:  ElementResultType
 * Namespace: http://idanalytics.com/products/enhancedfraud/result
 * Java type: com.idanalytics.products.enhancedfraud.result.ElementResultType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.enhancedfraud.result;


/**
 * An XML ElementResultType(@http://idanalytics.com/products/enhancedfraud/result).
 *
 * This is a complex type.
 */
public interface ElementResultType extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ElementResultType.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("elementresulttype3bfctype");
    
    /**
     * Gets the "Value" element
     */
    java.lang.String getValue();
    
    /**
     * Gets (as xml) the "Value" element
     */
    com.idanalytics.products.enhancedfraud.result.ValueDocument.Value xgetValue();
    
    /**
     * Sets the "Value" element
     */
    void setValue(java.lang.String value);
    
    /**
     * Sets (as xml) the "Value" element
     */
    void xsetValue(com.idanalytics.products.enhancedfraud.result.ValueDocument.Value value);
    
    /**
     * Gets the "RiskLevel" element
     */
    com.idanalytics.products.enhancedfraud.result.RiskLevelDocument.RiskLevel.Enum getRiskLevel();
    
    /**
     * Gets (as xml) the "RiskLevel" element
     */
    com.idanalytics.products.enhancedfraud.result.RiskLevelDocument.RiskLevel xgetRiskLevel();
    
    /**
     * Sets the "RiskLevel" element
     */
    void setRiskLevel(com.idanalytics.products.enhancedfraud.result.RiskLevelDocument.RiskLevel.Enum riskLevel);
    
    /**
     * Sets (as xml) the "RiskLevel" element
     */
    void xsetRiskLevel(com.idanalytics.products.enhancedfraud.result.RiskLevelDocument.RiskLevel riskLevel);
    
    /**
     * Gets the "ReasonCode" element
     */
    java.lang.String getReasonCode();
    
    /**
     * Gets (as xml) the "ReasonCode" element
     */
    com.idanalytics.products.enhancedfraud.result.ReasonCodeDocument.ReasonCode xgetReasonCode();
    
    /**
     * True if has "ReasonCode" element
     */
    boolean isSetReasonCode();
    
    /**
     * Sets the "ReasonCode" element
     */
    void setReasonCode(java.lang.String reasonCode);
    
    /**
     * Sets (as xml) the "ReasonCode" element
     */
    void xsetReasonCode(com.idanalytics.products.enhancedfraud.result.ReasonCodeDocument.ReasonCode reasonCode);
    
    /**
     * Unsets the "ReasonCode" element
     */
    void unsetReasonCode();
    
    /**
     * Gets the "ReasonDesc" element
     */
    java.lang.String getReasonDesc();
    
    /**
     * Gets (as xml) the "ReasonDesc" element
     */
    com.idanalytics.products.enhancedfraud.result.ReasonDescDocument.ReasonDesc xgetReasonDesc();
    
    /**
     * True if has "ReasonDesc" element
     */
    boolean isSetReasonDesc();
    
    /**
     * Sets the "ReasonDesc" element
     */
    void setReasonDesc(java.lang.String reasonDesc);
    
    /**
     * Sets (as xml) the "ReasonDesc" element
     */
    void xsetReasonDesc(com.idanalytics.products.enhancedfraud.result.ReasonDescDocument.ReasonDesc reasonDesc);
    
    /**
     * Unsets the "ReasonDesc" element
     */
    void unsetReasonDesc();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.enhancedfraud.result.ElementResultType newInstance() {
          return (com.idanalytics.products.enhancedfraud.result.ElementResultType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.ElementResultType newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.enhancedfraud.result.ElementResultType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.enhancedfraud.result.ElementResultType parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.enhancedfraud.result.ElementResultType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.ElementResultType parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.enhancedfraud.result.ElementResultType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.enhancedfraud.result.ElementResultType parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.ElementResultType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.ElementResultType parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.ElementResultType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.enhancedfraud.result.ElementResultType parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.ElementResultType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.ElementResultType parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.ElementResultType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.enhancedfraud.result.ElementResultType parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.ElementResultType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.ElementResultType parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.ElementResultType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.enhancedfraud.result.ElementResultType parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.ElementResultType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.ElementResultType parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.enhancedfraud.result.ElementResultType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.enhancedfraud.result.ElementResultType parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.enhancedfraud.result.ElementResultType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.ElementResultType parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.enhancedfraud.result.ElementResultType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.enhancedfraud.result.ElementResultType parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.enhancedfraud.result.ElementResultType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.enhancedfraud.result.ElementResultType parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.enhancedfraud.result.ElementResultType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.enhancedfraud.result.ElementResultType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.enhancedfraud.result.ElementResultType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.enhancedfraud.result.ElementResultType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.enhancedfraud.result.ElementResultType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
