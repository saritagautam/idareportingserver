/*
 * XML Type:  OrderNumber
 * Namespace: http://idanalytics.com/products/enhancedfraud/result
 * Java type: com.idanalytics.products.enhancedfraud.result.OrderNumber
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.enhancedfraud.result.impl;
/**
 * An XML OrderNumber(@http://idanalytics.com/products/enhancedfraud/result).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.enhancedfraud.result.OrderNumber.
 */
public class OrderNumberImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.enhancedfraud.result.OrderNumber
{
    private static final long serialVersionUID = 1L;
    
    public OrderNumberImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected OrderNumberImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
