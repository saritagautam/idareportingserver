/*
 * XML Type:  ElementResultType
 * Namespace: http://idanalytics.com/products/enhancedfraud/result
 * Java type: com.idanalytics.products.enhancedfraud.result.ElementResultType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.enhancedfraud.result.impl;
/**
 * An XML ElementResultType(@http://idanalytics.com/products/enhancedfraud/result).
 *
 * This is a complex type.
 */
public class ElementResultTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.enhancedfraud.result.ElementResultType
{
    private static final long serialVersionUID = 1L;
    
    public ElementResultTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName VALUE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/enhancedfraud/result", "Value");
    private static final javax.xml.namespace.QName RISKLEVEL$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/enhancedfraud/result", "RiskLevel");
    private static final javax.xml.namespace.QName REASONCODE$4 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/enhancedfraud/result", "ReasonCode");
    private static final javax.xml.namespace.QName REASONDESC$6 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/enhancedfraud/result", "ReasonDesc");
    
    
    /**
     * Gets the "Value" element
     */
    public java.lang.String getValue()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(VALUE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Value" element
     */
    public com.idanalytics.products.enhancedfraud.result.ValueDocument.Value xgetValue()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.enhancedfraud.result.ValueDocument.Value target = null;
            target = (com.idanalytics.products.enhancedfraud.result.ValueDocument.Value)get_store().find_element_user(VALUE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Value" element
     */
    public void setValue(java.lang.String value)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(VALUE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(VALUE$0);
            }
            target.setStringValue(value);
        }
    }
    
    /**
     * Sets (as xml) the "Value" element
     */
    public void xsetValue(com.idanalytics.products.enhancedfraud.result.ValueDocument.Value value)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.enhancedfraud.result.ValueDocument.Value target = null;
            target = (com.idanalytics.products.enhancedfraud.result.ValueDocument.Value)get_store().find_element_user(VALUE$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.enhancedfraud.result.ValueDocument.Value)get_store().add_element_user(VALUE$0);
            }
            target.set(value);
        }
    }
    
    /**
     * Gets the "RiskLevel" element
     */
    public com.idanalytics.products.enhancedfraud.result.RiskLevelDocument.RiskLevel.Enum getRiskLevel()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RISKLEVEL$2, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.enhancedfraud.result.RiskLevelDocument.RiskLevel.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "RiskLevel" element
     */
    public com.idanalytics.products.enhancedfraud.result.RiskLevelDocument.RiskLevel xgetRiskLevel()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.enhancedfraud.result.RiskLevelDocument.RiskLevel target = null;
            target = (com.idanalytics.products.enhancedfraud.result.RiskLevelDocument.RiskLevel)get_store().find_element_user(RISKLEVEL$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "RiskLevel" element
     */
    public void setRiskLevel(com.idanalytics.products.enhancedfraud.result.RiskLevelDocument.RiskLevel.Enum riskLevel)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RISKLEVEL$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(RISKLEVEL$2);
            }
            target.setEnumValue(riskLevel);
        }
    }
    
    /**
     * Sets (as xml) the "RiskLevel" element
     */
    public void xsetRiskLevel(com.idanalytics.products.enhancedfraud.result.RiskLevelDocument.RiskLevel riskLevel)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.enhancedfraud.result.RiskLevelDocument.RiskLevel target = null;
            target = (com.idanalytics.products.enhancedfraud.result.RiskLevelDocument.RiskLevel)get_store().find_element_user(RISKLEVEL$2, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.enhancedfraud.result.RiskLevelDocument.RiskLevel)get_store().add_element_user(RISKLEVEL$2);
            }
            target.set(riskLevel);
        }
    }
    
    /**
     * Gets the "ReasonCode" element
     */
    public java.lang.String getReasonCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(REASONCODE$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "ReasonCode" element
     */
    public com.idanalytics.products.enhancedfraud.result.ReasonCodeDocument.ReasonCode xgetReasonCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.enhancedfraud.result.ReasonCodeDocument.ReasonCode target = null;
            target = (com.idanalytics.products.enhancedfraud.result.ReasonCodeDocument.ReasonCode)get_store().find_element_user(REASONCODE$4, 0);
            return target;
        }
    }
    
    /**
     * True if has "ReasonCode" element
     */
    public boolean isSetReasonCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(REASONCODE$4) != 0;
        }
    }
    
    /**
     * Sets the "ReasonCode" element
     */
    public void setReasonCode(java.lang.String reasonCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(REASONCODE$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(REASONCODE$4);
            }
            target.setStringValue(reasonCode);
        }
    }
    
    /**
     * Sets (as xml) the "ReasonCode" element
     */
    public void xsetReasonCode(com.idanalytics.products.enhancedfraud.result.ReasonCodeDocument.ReasonCode reasonCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.enhancedfraud.result.ReasonCodeDocument.ReasonCode target = null;
            target = (com.idanalytics.products.enhancedfraud.result.ReasonCodeDocument.ReasonCode)get_store().find_element_user(REASONCODE$4, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.enhancedfraud.result.ReasonCodeDocument.ReasonCode)get_store().add_element_user(REASONCODE$4);
            }
            target.set(reasonCode);
        }
    }
    
    /**
     * Unsets the "ReasonCode" element
     */
    public void unsetReasonCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(REASONCODE$4, 0);
        }
    }
    
    /**
     * Gets the "ReasonDesc" element
     */
    public java.lang.String getReasonDesc()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(REASONDESC$6, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "ReasonDesc" element
     */
    public com.idanalytics.products.enhancedfraud.result.ReasonDescDocument.ReasonDesc xgetReasonDesc()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.enhancedfraud.result.ReasonDescDocument.ReasonDesc target = null;
            target = (com.idanalytics.products.enhancedfraud.result.ReasonDescDocument.ReasonDesc)get_store().find_element_user(REASONDESC$6, 0);
            return target;
        }
    }
    
    /**
     * True if has "ReasonDesc" element
     */
    public boolean isSetReasonDesc()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(REASONDESC$6) != 0;
        }
    }
    
    /**
     * Sets the "ReasonDesc" element
     */
    public void setReasonDesc(java.lang.String reasonDesc)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(REASONDESC$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(REASONDESC$6);
            }
            target.setStringValue(reasonDesc);
        }
    }
    
    /**
     * Sets (as xml) the "ReasonDesc" element
     */
    public void xsetReasonDesc(com.idanalytics.products.enhancedfraud.result.ReasonDescDocument.ReasonDesc reasonDesc)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.enhancedfraud.result.ReasonDescDocument.ReasonDesc target = null;
            target = (com.idanalytics.products.enhancedfraud.result.ReasonDescDocument.ReasonDesc)get_store().find_element_user(REASONDESC$6, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.enhancedfraud.result.ReasonDescDocument.ReasonDesc)get_store().add_element_user(REASONDESC$6);
            }
            target.set(reasonDesc);
        }
    }
    
    /**
     * Unsets the "ReasonDesc" element
     */
    public void unsetReasonDesc()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(REASONDESC$6, 0);
        }
    }
}
