/*
 * An XML document type.
 * Localname: SecondaryPortfolio
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.SecondaryPortfolioDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one SecondaryPortfolio(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class SecondaryPortfolioDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.SecondaryPortfolioDocument
{
    private static final long serialVersionUID = 1L;
    
    public SecondaryPortfolioDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SECONDARYPORTFOLIO$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "SecondaryPortfolio");
    
    
    /**
     * Gets the "SecondaryPortfolio" element
     */
    public java.lang.String getSecondaryPortfolio()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SECONDARYPORTFOLIO$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "SecondaryPortfolio" element
     */
    public com.idanalytics.products.certainid.SecondaryPortfolioDocument.SecondaryPortfolio xgetSecondaryPortfolio()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.SecondaryPortfolioDocument.SecondaryPortfolio target = null;
            target = (com.idanalytics.products.certainid.SecondaryPortfolioDocument.SecondaryPortfolio)get_store().find_element_user(SECONDARYPORTFOLIO$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "SecondaryPortfolio" element
     */
    public void setSecondaryPortfolio(java.lang.String secondaryPortfolio)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SECONDARYPORTFOLIO$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SECONDARYPORTFOLIO$0);
            }
            target.setStringValue(secondaryPortfolio);
        }
    }
    
    /**
     * Sets (as xml) the "SecondaryPortfolio" element
     */
    public void xsetSecondaryPortfolio(com.idanalytics.products.certainid.SecondaryPortfolioDocument.SecondaryPortfolio secondaryPortfolio)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.SecondaryPortfolioDocument.SecondaryPortfolio target = null;
            target = (com.idanalytics.products.certainid.SecondaryPortfolioDocument.SecondaryPortfolio)get_store().find_element_user(SECONDARYPORTFOLIO$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.SecondaryPortfolioDocument.SecondaryPortfolio)get_store().add_element_user(SECONDARYPORTFOLIO$0);
            }
            target.set(secondaryPortfolio);
        }
    }
    /**
     * An XML SecondaryPortfolio(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.SecondaryPortfolioDocument$SecondaryPortfolio.
     */
    public static class SecondaryPortfolioImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.certainid.SecondaryPortfolioDocument.SecondaryPortfolio
    {
        private static final long serialVersionUID = 1L;
        
        public SecondaryPortfolioImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected SecondaryPortfolioImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
