/*
 * An XML document type.
 * Localname: SecondaryDecisionCode
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.SecondaryDecisionCodeDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid;


/**
 * A document containing one SecondaryDecisionCode(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public interface SecondaryDecisionCodeDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(SecondaryDecisionCodeDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("secondarydecisioncode72f9doctype");
    
    /**
     * Gets the "SecondaryDecisionCode" element
     */
    java.lang.String getSecondaryDecisionCode();
    
    /**
     * Gets (as xml) the "SecondaryDecisionCode" element
     */
    com.idanalytics.products.certainid.SecondaryDecisionCodeDocument.SecondaryDecisionCode xgetSecondaryDecisionCode();
    
    /**
     * Sets the "SecondaryDecisionCode" element
     */
    void setSecondaryDecisionCode(java.lang.String secondaryDecisionCode);
    
    /**
     * Sets (as xml) the "SecondaryDecisionCode" element
     */
    void xsetSecondaryDecisionCode(com.idanalytics.products.certainid.SecondaryDecisionCodeDocument.SecondaryDecisionCode secondaryDecisionCode);
    
    /**
     * An XML SecondaryDecisionCode(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.SecondaryDecisionCodeDocument$SecondaryDecisionCode.
     */
    public interface SecondaryDecisionCode extends org.apache.xmlbeans.XmlString
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(SecondaryDecisionCode.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("secondarydecisioncodeae82elemtype");
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.certainid.SecondaryDecisionCodeDocument.SecondaryDecisionCode newValue(java.lang.Object obj) {
              return (com.idanalytics.products.certainid.SecondaryDecisionCodeDocument.SecondaryDecisionCode) type.newValue( obj ); }
            
            public static com.idanalytics.products.certainid.SecondaryDecisionCodeDocument.SecondaryDecisionCode newInstance() {
              return (com.idanalytics.products.certainid.SecondaryDecisionCodeDocument.SecondaryDecisionCode) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.certainid.SecondaryDecisionCodeDocument.SecondaryDecisionCode newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.certainid.SecondaryDecisionCodeDocument.SecondaryDecisionCode) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.certainid.SecondaryDecisionCodeDocument newInstance() {
          return (com.idanalytics.products.certainid.SecondaryDecisionCodeDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.certainid.SecondaryDecisionCodeDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.certainid.SecondaryDecisionCodeDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.certainid.SecondaryDecisionCodeDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.certainid.SecondaryDecisionCodeDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.certainid.SecondaryDecisionCodeDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.certainid.SecondaryDecisionCodeDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.certainid.SecondaryDecisionCodeDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.certainid.SecondaryDecisionCodeDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.certainid.SecondaryDecisionCodeDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.certainid.SecondaryDecisionCodeDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.certainid.SecondaryDecisionCodeDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.certainid.SecondaryDecisionCodeDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.certainid.SecondaryDecisionCodeDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.certainid.SecondaryDecisionCodeDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.certainid.SecondaryDecisionCodeDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.certainid.SecondaryDecisionCodeDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.certainid.SecondaryDecisionCodeDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.certainid.SecondaryDecisionCodeDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.certainid.SecondaryDecisionCodeDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.certainid.SecondaryDecisionCodeDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.certainid.SecondaryDecisionCodeDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.certainid.SecondaryDecisionCodeDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.certainid.SecondaryDecisionCodeDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.certainid.SecondaryDecisionCodeDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.certainid.SecondaryDecisionCodeDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.certainid.SecondaryDecisionCodeDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.certainid.SecondaryDecisionCodeDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.certainid.SecondaryDecisionCodeDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.certainid.SecondaryDecisionCodeDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.certainid.SecondaryDecisionCodeDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.certainid.SecondaryDecisionCodeDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.certainid.SecondaryDecisionCodeDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.certainid.SecondaryDecisionCodeDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.certainid.SecondaryDecisionCodeDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
