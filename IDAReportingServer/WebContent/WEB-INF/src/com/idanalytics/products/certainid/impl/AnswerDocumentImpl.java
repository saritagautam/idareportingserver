/*
 * An XML document type.
 * Localname: Answer
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.AnswerDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one Answer(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class AnswerDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.AnswerDocument
{
    private static final long serialVersionUID = 1L;
    
    public AnswerDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ANSWER$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "Answer");
    
    
    /**
     * Gets the "Answer" element
     */
    public com.idanalytics.products.certainid.AnswerDocument.Answer getAnswer()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.AnswerDocument.Answer target = null;
            target = (com.idanalytics.products.certainid.AnswerDocument.Answer)get_store().find_element_user(ANSWER$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "Answer" element
     */
    public void setAnswer(com.idanalytics.products.certainid.AnswerDocument.Answer answer)
    {
        generatedSetterHelperImpl(answer, ANSWER$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "Answer" element
     */
    public com.idanalytics.products.certainid.AnswerDocument.Answer addNewAnswer()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.AnswerDocument.Answer target = null;
            target = (com.idanalytics.products.certainid.AnswerDocument.Answer)get_store().add_element_user(ANSWER$0);
            return target;
        }
    }
    /**
     * An XML Answer(@http://idanalytics.com/products/certainid).
     *
     * This is a complex type.
     */
    public static class AnswerImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.AnswerDocument.Answer
    {
        private static final long serialVersionUID = 1L;
        
        public AnswerImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName QUESTIONID$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "QuestionId");
        private static final javax.xml.namespace.QName CHOICE$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "Choice");
        
        
        /**
         * Gets the "QuestionId" element
         */
        public java.math.BigInteger getQuestionId()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUESTIONID$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getBigIntegerValue();
            }
        }
        
        /**
         * Gets (as xml) the "QuestionId" element
         */
        public org.apache.xmlbeans.XmlInteger xgetQuestionId()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlInteger target = null;
                target = (org.apache.xmlbeans.XmlInteger)get_store().find_element_user(QUESTIONID$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "QuestionId" element
         */
        public void setQuestionId(java.math.BigInteger questionId)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUESTIONID$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(QUESTIONID$0);
                }
                target.setBigIntegerValue(questionId);
            }
        }
        
        /**
         * Sets (as xml) the "QuestionId" element
         */
        public void xsetQuestionId(org.apache.xmlbeans.XmlInteger questionId)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlInteger target = null;
                target = (org.apache.xmlbeans.XmlInteger)get_store().find_element_user(QUESTIONID$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlInteger)get_store().add_element_user(QUESTIONID$0);
                }
                target.set(questionId);
            }
        }
        
        /**
         * Gets the "Choice" element
         */
        public java.lang.String getChoice()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CHOICE$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "Choice" element
         */
        public org.apache.xmlbeans.XmlString xgetChoice()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CHOICE$2, 0);
                return target;
            }
        }
        
        /**
         * Sets the "Choice" element
         */
        public void setChoice(java.lang.String choice)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CHOICE$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CHOICE$2);
                }
                target.setStringValue(choice);
            }
        }
        
        /**
         * Sets (as xml) the "Choice" element
         */
        public void xsetChoice(org.apache.xmlbeans.XmlString choice)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CHOICE$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(CHOICE$2);
                }
                target.set(choice);
            }
        }
    }
}
