/*
 * An XML document type.
 * Localname: AquisitionMethod
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.AquisitionMethodDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one AquisitionMethod(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class AquisitionMethodDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.AquisitionMethodDocument
{
    private static final long serialVersionUID = 1L;
    
    public AquisitionMethodDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName AQUISITIONMETHOD$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "AquisitionMethod");
    
    
    /**
     * Gets the "AquisitionMethod" element
     */
    public java.lang.String getAquisitionMethod()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(AQUISITIONMETHOD$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "AquisitionMethod" element
     */
    public com.idanalytics.products.certainid.AquisitionMethodDocument.AquisitionMethod xgetAquisitionMethod()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.AquisitionMethodDocument.AquisitionMethod target = null;
            target = (com.idanalytics.products.certainid.AquisitionMethodDocument.AquisitionMethod)get_store().find_element_user(AQUISITIONMETHOD$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "AquisitionMethod" element
     */
    public void setAquisitionMethod(java.lang.String aquisitionMethod)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(AQUISITIONMETHOD$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(AQUISITIONMETHOD$0);
            }
            target.setStringValue(aquisitionMethod);
        }
    }
    
    /**
     * Sets (as xml) the "AquisitionMethod" element
     */
    public void xsetAquisitionMethod(com.idanalytics.products.certainid.AquisitionMethodDocument.AquisitionMethod aquisitionMethod)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.AquisitionMethodDocument.AquisitionMethod target = null;
            target = (com.idanalytics.products.certainid.AquisitionMethodDocument.AquisitionMethod)get_store().find_element_user(AQUISITIONMETHOD$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.AquisitionMethodDocument.AquisitionMethod)get_store().add_element_user(AQUISITIONMETHOD$0);
            }
            target.set(aquisitionMethod);
        }
    }
    /**
     * An XML AquisitionMethod(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.AquisitionMethodDocument$AquisitionMethod.
     */
    public static class AquisitionMethodImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.certainid.AquisitionMethodDocument.AquisitionMethod
    {
        private static final long serialVersionUID = 1L;
        
        public AquisitionMethodImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected AquisitionMethodImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
