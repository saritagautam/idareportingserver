/*
 * An XML document type.
 * Localname: SecondaryFraudCode
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.SecondaryFraudCodeDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one SecondaryFraudCode(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class SecondaryFraudCodeDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.SecondaryFraudCodeDocument
{
    private static final long serialVersionUID = 1L;
    
    public SecondaryFraudCodeDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SECONDARYFRAUDCODE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "SecondaryFraudCode");
    
    
    /**
     * Gets the "SecondaryFraudCode" element
     */
    public java.lang.String getSecondaryFraudCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SECONDARYFRAUDCODE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "SecondaryFraudCode" element
     */
    public com.idanalytics.products.certainid.SecondaryFraudCodeDocument.SecondaryFraudCode xgetSecondaryFraudCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.SecondaryFraudCodeDocument.SecondaryFraudCode target = null;
            target = (com.idanalytics.products.certainid.SecondaryFraudCodeDocument.SecondaryFraudCode)get_store().find_element_user(SECONDARYFRAUDCODE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "SecondaryFraudCode" element
     */
    public void setSecondaryFraudCode(java.lang.String secondaryFraudCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SECONDARYFRAUDCODE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SECONDARYFRAUDCODE$0);
            }
            target.setStringValue(secondaryFraudCode);
        }
    }
    
    /**
     * Sets (as xml) the "SecondaryFraudCode" element
     */
    public void xsetSecondaryFraudCode(com.idanalytics.products.certainid.SecondaryFraudCodeDocument.SecondaryFraudCode secondaryFraudCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.SecondaryFraudCodeDocument.SecondaryFraudCode target = null;
            target = (com.idanalytics.products.certainid.SecondaryFraudCodeDocument.SecondaryFraudCode)get_store().find_element_user(SECONDARYFRAUDCODE$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.SecondaryFraudCodeDocument.SecondaryFraudCode)get_store().add_element_user(SECONDARYFRAUDCODE$0);
            }
            target.set(secondaryFraudCode);
        }
    }
    /**
     * An XML SecondaryFraudCode(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.SecondaryFraudCodeDocument$SecondaryFraudCode.
     */
    public static class SecondaryFraudCodeImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.certainid.SecondaryFraudCodeDocument.SecondaryFraudCode
    {
        private static final long serialVersionUID = 1L;
        
        public SecondaryFraudCodeImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected SecondaryFraudCodeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
