/*
 * An XML document type.
 * Localname: CBRUtilized
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.CBRUtilizedDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one CBRUtilized(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class CBRUtilizedDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.CBRUtilizedDocument
{
    private static final long serialVersionUID = 1L;
    
    public CBRUtilizedDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CBRUTILIZED$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "CBRUtilized");
    
    
    /**
     * Gets the "CBRUtilized" element
     */
    public java.lang.String getCBRUtilized()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CBRUTILIZED$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "CBRUtilized" element
     */
    public com.idanalytics.products.certainid.CBRUtilizedDocument.CBRUtilized xgetCBRUtilized()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.CBRUtilizedDocument.CBRUtilized target = null;
            target = (com.idanalytics.products.certainid.CBRUtilizedDocument.CBRUtilized)get_store().find_element_user(CBRUTILIZED$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "CBRUtilized" element
     */
    public void setCBRUtilized(java.lang.String cbrUtilized)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CBRUTILIZED$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CBRUTILIZED$0);
            }
            target.setStringValue(cbrUtilized);
        }
    }
    
    /**
     * Sets (as xml) the "CBRUtilized" element
     */
    public void xsetCBRUtilized(com.idanalytics.products.certainid.CBRUtilizedDocument.CBRUtilized cbrUtilized)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.CBRUtilizedDocument.CBRUtilized target = null;
            target = (com.idanalytics.products.certainid.CBRUtilizedDocument.CBRUtilized)get_store().find_element_user(CBRUTILIZED$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.CBRUtilizedDocument.CBRUtilized)get_store().add_element_user(CBRUTILIZED$0);
            }
            target.set(cbrUtilized);
        }
    }
    /**
     * An XML CBRUtilized(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.CBRUtilizedDocument$CBRUtilized.
     */
    public static class CBRUtilizedImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.certainid.CBRUtilizedDocument.CBRUtilized
    {
        private static final long serialVersionUID = 1L;
        
        public CBRUtilizedImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected CBRUtilizedImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
