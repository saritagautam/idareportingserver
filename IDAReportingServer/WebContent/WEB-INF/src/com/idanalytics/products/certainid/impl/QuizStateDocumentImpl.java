/*
 * An XML document type.
 * Localname: QuizState
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.QuizStateDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one QuizState(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class QuizStateDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.QuizStateDocument
{
    private static final long serialVersionUID = 1L;
    
    public QuizStateDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName QUIZSTATE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "QuizState");
    
    
    /**
     * Gets the "QuizState" element
     */
    public com.idanalytics.products.certainid.QuizStateDocument.QuizState.Enum getQuizState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUIZSTATE$0, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.certainid.QuizStateDocument.QuizState.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "QuizState" element
     */
    public com.idanalytics.products.certainid.QuizStateDocument.QuizState xgetQuizState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.QuizStateDocument.QuizState target = null;
            target = (com.idanalytics.products.certainid.QuizStateDocument.QuizState)get_store().find_element_user(QUIZSTATE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "QuizState" element
     */
    public void setQuizState(com.idanalytics.products.certainid.QuizStateDocument.QuizState.Enum quizState)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUIZSTATE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(QUIZSTATE$0);
            }
            target.setEnumValue(quizState);
        }
    }
    
    /**
     * Sets (as xml) the "QuizState" element
     */
    public void xsetQuizState(com.idanalytics.products.certainid.QuizStateDocument.QuizState quizState)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.QuizStateDocument.QuizState target = null;
            target = (com.idanalytics.products.certainid.QuizStateDocument.QuizState)get_store().find_element_user(QUIZSTATE$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.QuizStateDocument.QuizState)get_store().add_element_user(QUIZSTATE$0);
            }
            target.set(quizState);
        }
    }
    /**
     * An XML QuizState(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.QuizStateDocument$QuizState.
     */
    public static class QuizStateImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.idanalytics.products.certainid.QuizStateDocument.QuizState
    {
        private static final long serialVersionUID = 1L;
        
        public QuizStateImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected QuizStateImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
