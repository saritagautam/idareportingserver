/*
 * An XML document type.
 * Localname: RequestType
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.RequestTypeDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one RequestType(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class RequestTypeDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.RequestTypeDocument
{
    private static final long serialVersionUID = 1L;
    
    public RequestTypeDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName REQUESTTYPE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "RequestType");
    
    
    /**
     * Gets the "RequestType" element
     */
    public java.lang.Object getRequestType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(REQUESTTYPE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getObjectValue();
        }
    }
    
    /**
     * Gets (as xml) the "RequestType" element
     */
    public com.idanalytics.products.certainid.RequestTypeDocument.RequestType xgetRequestType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.RequestTypeDocument.RequestType target = null;
            target = (com.idanalytics.products.certainid.RequestTypeDocument.RequestType)get_store().find_element_user(REQUESTTYPE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "RequestType" element
     */
    public void setRequestType(java.lang.Object requestType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(REQUESTTYPE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(REQUESTTYPE$0);
            }
            target.setObjectValue(requestType);
        }
    }
    
    /**
     * Sets (as xml) the "RequestType" element
     */
    public void xsetRequestType(com.idanalytics.products.certainid.RequestTypeDocument.RequestType requestType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.RequestTypeDocument.RequestType target = null;
            target = (com.idanalytics.products.certainid.RequestTypeDocument.RequestType)get_store().find_element_user(REQUESTTYPE$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.RequestTypeDocument.RequestType)get_store().add_element_user(REQUESTTYPE$0);
            }
            target.set(requestType);
        }
    }
    /**
     * An XML RequestType(@http://idanalytics.com/products/certainid).
     *
     * This is a union type. Instances are of one of the following types:
     *     org.apache.xmlbeans.XmlInt
     *     com.idanalytics.products.certainid.RequestTypeDocument$RequestType$Member
     */
    public static class RequestTypeImpl extends org.apache.xmlbeans.impl.values.XmlUnionImpl implements com.idanalytics.products.certainid.RequestTypeDocument.RequestType, org.apache.xmlbeans.XmlInt, com.idanalytics.products.certainid.RequestTypeDocument.RequestType.Member
    {
        private static final long serialVersionUID = 1L;
        
        public RequestTypeImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected RequestTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
        /**
         * An anonymous inner XML type.
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.certainid.RequestTypeDocument$RequestType$Member.
         */
        public static class MemberImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.idanalytics.products.certainid.RequestTypeDocument.RequestType.Member
        {
            private static final long serialVersionUID = 1L;
            
            public MemberImpl(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected MemberImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
    }
}
