/*
 * An XML document type.
 * Localname: PatriotActDOB
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.PatriotActDOBDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one PatriotActDOB(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class PatriotActDOBDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.PatriotActDOBDocument
{
    private static final long serialVersionUID = 1L;
    
    public PatriotActDOBDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName PATRIOTACTDOB$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "PatriotActDOB");
    
    
    /**
     * Gets the "PatriotActDOB" element
     */
    public com.idanalytics.products.certainid.PatriotActDOBDocument.PatriotActDOB.Enum getPatriotActDOB()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PATRIOTACTDOB$0, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.certainid.PatriotActDOBDocument.PatriotActDOB.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "PatriotActDOB" element
     */
    public com.idanalytics.products.certainid.PatriotActDOBDocument.PatriotActDOB xgetPatriotActDOB()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.PatriotActDOBDocument.PatriotActDOB target = null;
            target = (com.idanalytics.products.certainid.PatriotActDOBDocument.PatriotActDOB)get_store().find_element_user(PATRIOTACTDOB$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "PatriotActDOB" element
     */
    public void setPatriotActDOB(com.idanalytics.products.certainid.PatriotActDOBDocument.PatriotActDOB.Enum patriotActDOB)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PATRIOTACTDOB$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PATRIOTACTDOB$0);
            }
            target.setEnumValue(patriotActDOB);
        }
    }
    
    /**
     * Sets (as xml) the "PatriotActDOB" element
     */
    public void xsetPatriotActDOB(com.idanalytics.products.certainid.PatriotActDOBDocument.PatriotActDOB patriotActDOB)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.PatriotActDOBDocument.PatriotActDOB target = null;
            target = (com.idanalytics.products.certainid.PatriotActDOBDocument.PatriotActDOB)get_store().find_element_user(PATRIOTACTDOB$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.PatriotActDOBDocument.PatriotActDOB)get_store().add_element_user(PATRIOTACTDOB$0);
            }
            target.set(patriotActDOB);
        }
    }
    /**
     * An XML PatriotActDOB(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.PatriotActDOBDocument$PatriotActDOB.
     */
    public static class PatriotActDOBImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.idanalytics.products.certainid.PatriotActDOBDocument.PatriotActDOB
    {
        private static final long serialVersionUID = 1L;
        
        public PatriotActDOBImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected PatriotActDOBImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
