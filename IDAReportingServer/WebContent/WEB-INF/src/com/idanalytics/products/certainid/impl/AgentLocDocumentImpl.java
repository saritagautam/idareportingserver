/*
 * An XML document type.
 * Localname: AgentLoc
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.AgentLocDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one AgentLoc(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class AgentLocDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.AgentLocDocument
{
    private static final long serialVersionUID = 1L;
    
    public AgentLocDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName AGENTLOC$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "AgentLoc");
    
    
    /**
     * Gets the "AgentLoc" element
     */
    public java.lang.String getAgentLoc()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(AGENTLOC$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "AgentLoc" element
     */
    public com.idanalytics.products.certainid.AgentLocDocument.AgentLoc xgetAgentLoc()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.AgentLocDocument.AgentLoc target = null;
            target = (com.idanalytics.products.certainid.AgentLocDocument.AgentLoc)get_store().find_element_user(AGENTLOC$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "AgentLoc" element
     */
    public void setAgentLoc(java.lang.String agentLoc)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(AGENTLOC$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(AGENTLOC$0);
            }
            target.setStringValue(agentLoc);
        }
    }
    
    /**
     * Sets (as xml) the "AgentLoc" element
     */
    public void xsetAgentLoc(com.idanalytics.products.certainid.AgentLocDocument.AgentLoc agentLoc)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.AgentLocDocument.AgentLoc target = null;
            target = (com.idanalytics.products.certainid.AgentLocDocument.AgentLoc)get_store().find_element_user(AGENTLOC$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.AgentLocDocument.AgentLoc)get_store().add_element_user(AGENTLOC$0);
            }
            target.set(agentLoc);
        }
    }
    /**
     * An XML AgentLoc(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.AgentLocDocument$AgentLoc.
     */
    public static class AgentLocImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.certainid.AgentLocDocument.AgentLoc
    {
        private static final long serialVersionUID = 1L;
        
        public AgentLocImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected AgentLocImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
