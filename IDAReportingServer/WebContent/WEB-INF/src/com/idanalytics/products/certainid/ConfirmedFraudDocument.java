/*
 * An XML document type.
 * Localname: ConfirmedFraud
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.ConfirmedFraudDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid;


/**
 * A document containing one ConfirmedFraud(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public interface ConfirmedFraudDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ConfirmedFraudDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("confirmedfrauda113doctype");
    
    /**
     * Gets the "ConfirmedFraud" element
     */
    com.idanalytics.products.certainid.ConfirmedFraudDocument.ConfirmedFraud.Enum getConfirmedFraud();
    
    /**
     * Gets (as xml) the "ConfirmedFraud" element
     */
    com.idanalytics.products.certainid.ConfirmedFraudDocument.ConfirmedFraud xgetConfirmedFraud();
    
    /**
     * Sets the "ConfirmedFraud" element
     */
    void setConfirmedFraud(com.idanalytics.products.certainid.ConfirmedFraudDocument.ConfirmedFraud.Enum confirmedFraud);
    
    /**
     * Sets (as xml) the "ConfirmedFraud" element
     */
    void xsetConfirmedFraud(com.idanalytics.products.certainid.ConfirmedFraudDocument.ConfirmedFraud confirmedFraud);
    
    /**
     * An XML ConfirmedFraud(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.ConfirmedFraudDocument$ConfirmedFraud.
     */
    public interface ConfirmedFraud extends org.apache.xmlbeans.XmlString
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ConfirmedFraud.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("confirmedfrauda772elemtype");
        
        org.apache.xmlbeans.StringEnumAbstractBase enumValue();
        void set(org.apache.xmlbeans.StringEnumAbstractBase e);
        
        static final Enum Y = Enum.forString("Y");
        static final Enum N = Enum.forString("N");
        
        static final int INT_Y = Enum.INT_Y;
        static final int INT_N = Enum.INT_N;
        
        /**
         * Enumeration value class for com.idanalytics.products.certainid.ConfirmedFraudDocument$ConfirmedFraud.
         * These enum values can be used as follows:
         * <pre>
         * enum.toString(); // returns the string value of the enum
         * enum.intValue(); // returns an int value, useful for switches
         * // e.g., case Enum.INT_Y
         * Enum.forString(s); // returns the enum value for a string
         * Enum.forInt(i); // returns the enum value for an int
         * </pre>
         * Enumeration objects are immutable singleton objects that
         * can be compared using == object equality. They have no
         * public constructor. See the constants defined within this
         * class for all the valid values.
         */
        static final class Enum extends org.apache.xmlbeans.StringEnumAbstractBase
        {
            /**
             * Returns the enum value for a string, or null if none.
             */
            public static Enum forString(java.lang.String s)
                { return (Enum)table.forString(s); }
            /**
             * Returns the enum value corresponding to an int, or null if none.
             */
            public static Enum forInt(int i)
                { return (Enum)table.forInt(i); }
            
            private Enum(java.lang.String s, int i)
                { super(s, i); }
            
            static final int INT_Y = 1;
            static final int INT_N = 2;
            
            public static final org.apache.xmlbeans.StringEnumAbstractBase.Table table =
                new org.apache.xmlbeans.StringEnumAbstractBase.Table
            (
                new Enum[]
                {
                    new Enum("Y", INT_Y),
                    new Enum("N", INT_N),
                }
            );
            private static final long serialVersionUID = 1L;
            private java.lang.Object readResolve() { return forInt(intValue()); } 
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.certainid.ConfirmedFraudDocument.ConfirmedFraud newValue(java.lang.Object obj) {
              return (com.idanalytics.products.certainid.ConfirmedFraudDocument.ConfirmedFraud) type.newValue( obj ); }
            
            public static com.idanalytics.products.certainid.ConfirmedFraudDocument.ConfirmedFraud newInstance() {
              return (com.idanalytics.products.certainid.ConfirmedFraudDocument.ConfirmedFraud) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.certainid.ConfirmedFraudDocument.ConfirmedFraud newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.certainid.ConfirmedFraudDocument.ConfirmedFraud) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.certainid.ConfirmedFraudDocument newInstance() {
          return (com.idanalytics.products.certainid.ConfirmedFraudDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.certainid.ConfirmedFraudDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.certainid.ConfirmedFraudDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.certainid.ConfirmedFraudDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.certainid.ConfirmedFraudDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.certainid.ConfirmedFraudDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.certainid.ConfirmedFraudDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.certainid.ConfirmedFraudDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.certainid.ConfirmedFraudDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.certainid.ConfirmedFraudDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.certainid.ConfirmedFraudDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.certainid.ConfirmedFraudDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.certainid.ConfirmedFraudDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.certainid.ConfirmedFraudDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.certainid.ConfirmedFraudDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.certainid.ConfirmedFraudDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.certainid.ConfirmedFraudDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.certainid.ConfirmedFraudDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.certainid.ConfirmedFraudDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.certainid.ConfirmedFraudDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.certainid.ConfirmedFraudDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.certainid.ConfirmedFraudDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.certainid.ConfirmedFraudDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.certainid.ConfirmedFraudDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.certainid.ConfirmedFraudDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.certainid.ConfirmedFraudDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.certainid.ConfirmedFraudDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.certainid.ConfirmedFraudDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.certainid.ConfirmedFraudDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.certainid.ConfirmedFraudDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.certainid.ConfirmedFraudDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.certainid.ConfirmedFraudDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.certainid.ConfirmedFraudDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.certainid.ConfirmedFraudDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.certainid.ConfirmedFraudDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
