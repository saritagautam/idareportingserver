/*
 * An XML document type.
 * Localname: ServiceType
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.ServiceTypeDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one ServiceType(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class ServiceTypeDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.ServiceTypeDocument
{
    private static final long serialVersionUID = 1L;
    
    public ServiceTypeDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SERVICETYPE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "ServiceType");
    
    
    /**
     * Gets the "ServiceType" element
     */
    public com.idanalytics.products.certainid.ServiceTypeDocument.ServiceType.Enum getServiceType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SERVICETYPE$0, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.certainid.ServiceTypeDocument.ServiceType.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "ServiceType" element
     */
    public com.idanalytics.products.certainid.ServiceTypeDocument.ServiceType xgetServiceType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.ServiceTypeDocument.ServiceType target = null;
            target = (com.idanalytics.products.certainid.ServiceTypeDocument.ServiceType)get_store().find_element_user(SERVICETYPE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "ServiceType" element
     */
    public void setServiceType(com.idanalytics.products.certainid.ServiceTypeDocument.ServiceType.Enum serviceType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SERVICETYPE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SERVICETYPE$0);
            }
            target.setEnumValue(serviceType);
        }
    }
    
    /**
     * Sets (as xml) the "ServiceType" element
     */
    public void xsetServiceType(com.idanalytics.products.certainid.ServiceTypeDocument.ServiceType serviceType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.ServiceTypeDocument.ServiceType target = null;
            target = (com.idanalytics.products.certainid.ServiceTypeDocument.ServiceType)get_store().find_element_user(SERVICETYPE$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.ServiceTypeDocument.ServiceType)get_store().add_element_user(SERVICETYPE$0);
            }
            target.set(serviceType);
        }
    }
    /**
     * An XML ServiceType(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.ServiceTypeDocument$ServiceType.
     */
    public static class ServiceTypeImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.idanalytics.products.certainid.ServiceTypeDocument.ServiceType
    {
        private static final long serialVersionUID = 1L;
        
        public ServiceTypeImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected ServiceTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
