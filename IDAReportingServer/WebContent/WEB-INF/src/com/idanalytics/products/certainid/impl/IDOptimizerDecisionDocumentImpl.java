/*
 * An XML document type.
 * Localname: IDOptimizerDecision
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.IDOptimizerDecisionDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one IDOptimizerDecision(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class IDOptimizerDecisionDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.IDOptimizerDecisionDocument
{
    private static final long serialVersionUID = 1L;
    
    public IDOptimizerDecisionDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IDOPTIMIZERDECISION$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "IDOptimizerDecision");
    
    
    /**
     * Gets the "IDOptimizerDecision" element
     */
    public com.idanalytics.products.certainid.IDOptimizerDecisionDocument.IDOptimizerDecision.Enum getIDOptimizerDecision()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDOPTIMIZERDECISION$0, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.certainid.IDOptimizerDecisionDocument.IDOptimizerDecision.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "IDOptimizerDecision" element
     */
    public com.idanalytics.products.certainid.IDOptimizerDecisionDocument.IDOptimizerDecision xgetIDOptimizerDecision()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.IDOptimizerDecisionDocument.IDOptimizerDecision target = null;
            target = (com.idanalytics.products.certainid.IDOptimizerDecisionDocument.IDOptimizerDecision)get_store().find_element_user(IDOPTIMIZERDECISION$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IDOptimizerDecision" element
     */
    public void setIDOptimizerDecision(com.idanalytics.products.certainid.IDOptimizerDecisionDocument.IDOptimizerDecision.Enum idOptimizerDecision)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDOPTIMIZERDECISION$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDOPTIMIZERDECISION$0);
            }
            target.setEnumValue(idOptimizerDecision);
        }
    }
    
    /**
     * Sets (as xml) the "IDOptimizerDecision" element
     */
    public void xsetIDOptimizerDecision(com.idanalytics.products.certainid.IDOptimizerDecisionDocument.IDOptimizerDecision idOptimizerDecision)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.IDOptimizerDecisionDocument.IDOptimizerDecision target = null;
            target = (com.idanalytics.products.certainid.IDOptimizerDecisionDocument.IDOptimizerDecision)get_store().find_element_user(IDOPTIMIZERDECISION$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.IDOptimizerDecisionDocument.IDOptimizerDecision)get_store().add_element_user(IDOPTIMIZERDECISION$0);
            }
            target.set(idOptimizerDecision);
        }
    }
    /**
     * An XML IDOptimizerDecision(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.IDOptimizerDecisionDocument$IDOptimizerDecision.
     */
    public static class IDOptimizerDecisionImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.idanalytics.products.certainid.IDOptimizerDecisionDocument.IDOptimizerDecision
    {
        private static final long serialVersionUID = 1L;
        
        public IDOptimizerDecisionImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected IDOptimizerDecisionImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
