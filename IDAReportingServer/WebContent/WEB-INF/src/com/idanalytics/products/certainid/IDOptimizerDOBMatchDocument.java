/*
 * An XML document type.
 * Localname: IDOptimizerDOBMatch
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid;


/**
 * A document containing one IDOptimizerDOBMatch(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public interface IDOptimizerDOBMatchDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(IDOptimizerDOBMatchDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("idoptimizerdobmatcheae4doctype");
    
    /**
     * Gets the "IDOptimizerDOBMatch" element
     */
    com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument.IDOptimizerDOBMatch.Enum getIDOptimizerDOBMatch();
    
    /**
     * Gets (as xml) the "IDOptimizerDOBMatch" element
     */
    com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument.IDOptimizerDOBMatch xgetIDOptimizerDOBMatch();
    
    /**
     * Sets the "IDOptimizerDOBMatch" element
     */
    void setIDOptimizerDOBMatch(com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument.IDOptimizerDOBMatch.Enum idOptimizerDOBMatch);
    
    /**
     * Sets (as xml) the "IDOptimizerDOBMatch" element
     */
    void xsetIDOptimizerDOBMatch(com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument.IDOptimizerDOBMatch idOptimizerDOBMatch);
    
    /**
     * An XML IDOptimizerDOBMatch(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument$IDOptimizerDOBMatch.
     */
    public interface IDOptimizerDOBMatch extends org.apache.xmlbeans.XmlString
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(IDOptimizerDOBMatch.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("idoptimizerdobmatchb698elemtype");
        
        org.apache.xmlbeans.StringEnumAbstractBase enumValue();
        void set(org.apache.xmlbeans.StringEnumAbstractBase e);
        
        static final Enum X_01 = Enum.forString("01");
        static final Enum X_02 = Enum.forString("02");
        static final Enum X_03 = Enum.forString("03");
        static final Enum X_04 = Enum.forString("04");
        static final Enum X_20 = Enum.forString("20");
        
        static final int INT_X_01 = Enum.INT_X_01;
        static final int INT_X_02 = Enum.INT_X_02;
        static final int INT_X_03 = Enum.INT_X_03;
        static final int INT_X_04 = Enum.INT_X_04;
        static final int INT_X_20 = Enum.INT_X_20;
        
        /**
         * Enumeration value class for com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument$IDOptimizerDOBMatch.
         * These enum values can be used as follows:
         * <pre>
         * enum.toString(); // returns the string value of the enum
         * enum.intValue(); // returns an int value, useful for switches
         * // e.g., case Enum.INT_X_01
         * Enum.forString(s); // returns the enum value for a string
         * Enum.forInt(i); // returns the enum value for an int
         * </pre>
         * Enumeration objects are immutable singleton objects that
         * can be compared using == object equality. They have no
         * public constructor. See the constants defined within this
         * class for all the valid values.
         */
        static final class Enum extends org.apache.xmlbeans.StringEnumAbstractBase
        {
            /**
             * Returns the enum value for a string, or null if none.
             */
            public static Enum forString(java.lang.String s)
                { return (Enum)table.forString(s); }
            /**
             * Returns the enum value corresponding to an int, or null if none.
             */
            public static Enum forInt(int i)
                { return (Enum)table.forInt(i); }
            
            private Enum(java.lang.String s, int i)
                { super(s, i); }
            
            static final int INT_X_01 = 1;
            static final int INT_X_02 = 2;
            static final int INT_X_03 = 3;
            static final int INT_X_04 = 4;
            static final int INT_X_20 = 5;
            
            public static final org.apache.xmlbeans.StringEnumAbstractBase.Table table =
                new org.apache.xmlbeans.StringEnumAbstractBase.Table
            (
                new Enum[]
                {
                    new Enum("01", INT_X_01),
                    new Enum("02", INT_X_02),
                    new Enum("03", INT_X_03),
                    new Enum("04", INT_X_04),
                    new Enum("20", INT_X_20),
                }
            );
            private static final long serialVersionUID = 1L;
            private java.lang.Object readResolve() { return forInt(intValue()); } 
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument.IDOptimizerDOBMatch newValue(java.lang.Object obj) {
              return (com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument.IDOptimizerDOBMatch) type.newValue( obj ); }
            
            public static com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument.IDOptimizerDOBMatch newInstance() {
              return (com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument.IDOptimizerDOBMatch) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument.IDOptimizerDOBMatch newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument.IDOptimizerDOBMatch) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument newInstance() {
          return (com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
