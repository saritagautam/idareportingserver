/*
 * An XML document type.
 * Localname: DOB
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.DOBDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one DOB(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class DOBDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.DOBDocument
{
    private static final long serialVersionUID = 1L;
    
    public DOBDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName DOB$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "DOB");
    
    
    /**
     * Gets the "DOB" element
     */
    public java.lang.Object getDOB()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DOB$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getObjectValue();
        }
    }
    
    /**
     * Gets (as xml) the "DOB" element
     */
    public com.idanalytics.products.certainid.DOBDocument.DOB xgetDOB()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.DOBDocument.DOB target = null;
            target = (com.idanalytics.products.certainid.DOBDocument.DOB)get_store().find_element_user(DOB$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "DOB" element
     */
    public void setDOB(java.lang.Object dob)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DOB$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DOB$0);
            }
            target.setObjectValue(dob);
        }
    }
    
    /**
     * Sets (as xml) the "DOB" element
     */
    public void xsetDOB(com.idanalytics.products.certainid.DOBDocument.DOB dob)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.DOBDocument.DOB target = null;
            target = (com.idanalytics.products.certainid.DOBDocument.DOB)get_store().find_element_user(DOB$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.DOBDocument.DOB)get_store().add_element_user(DOB$0);
            }
            target.set(dob);
        }
    }
    /**
     * An XML DOB(@http://idanalytics.com/products/certainid).
     *
     * This is a union type. Instances are of one of the following types:
     *     com.idanalytics.products.certainid.DOBDocument$DOB$Member
     *     com.idanalytics.products.certainid.DOBDocument$DOB$Member2
     */
    public static class DOBImpl extends org.apache.xmlbeans.impl.values.XmlUnionImpl implements com.idanalytics.products.certainid.DOBDocument.DOB, com.idanalytics.products.certainid.DOBDocument.DOB.Member, com.idanalytics.products.certainid.DOBDocument.DOB.Member2
    {
        private static final long serialVersionUID = 1L;
        
        public DOBImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected DOBImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
        /**
         * An anonymous inner XML type.
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.certainid.DOBDocument$DOB$Member.
         */
        public static class MemberImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.certainid.DOBDocument.DOB.Member
        {
            private static final long serialVersionUID = 1L;
            
            public MemberImpl(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected MemberImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
        /**
         * An anonymous inner XML type.
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.certainid.DOBDocument$DOB$Member2.
         */
        public static class MemberImpl2 extends org.apache.xmlbeans.impl.values.JavaGDateHolderEx implements com.idanalytics.products.certainid.DOBDocument.DOB.Member2
        {
            private static final long serialVersionUID = 1L;
            
            public MemberImpl2(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected MemberImpl2(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
    }
}
