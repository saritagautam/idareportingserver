/*
 * An XML document type.
 * Localname: CPC
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.CPCDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one CPC(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class CPCDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.CPCDocument
{
    private static final long serialVersionUID = 1L;
    
    public CPCDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CPC$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "CPC");
    
    
    /**
     * Gets the "CPC" element
     */
    public java.lang.String getCPC()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CPC$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "CPC" element
     */
    public com.idanalytics.products.certainid.CPCDocument.CPC xgetCPC()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.CPCDocument.CPC target = null;
            target = (com.idanalytics.products.certainid.CPCDocument.CPC)get_store().find_element_user(CPC$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "CPC" element
     */
    public void setCPC(java.lang.String cpc)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CPC$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CPC$0);
            }
            target.setStringValue(cpc);
        }
    }
    
    /**
     * Sets (as xml) the "CPC" element
     */
    public void xsetCPC(com.idanalytics.products.certainid.CPCDocument.CPC cpc)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.CPCDocument.CPC target = null;
            target = (com.idanalytics.products.certainid.CPCDocument.CPC)get_store().find_element_user(CPC$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.CPCDocument.CPC)get_store().add_element_user(CPC$0);
            }
            target.set(cpc);
        }
    }
    /**
     * An XML CPC(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.CPCDocument$CPC.
     */
    public static class CPCImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.certainid.CPCDocument.CPC
    {
        private static final long serialVersionUID = 1L;
        
        public CPCImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected CPCImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
