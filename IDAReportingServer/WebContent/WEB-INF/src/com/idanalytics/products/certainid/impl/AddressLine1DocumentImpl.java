/*
 * An XML document type.
 * Localname: AddressLine1
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.AddressLine1Document
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one AddressLine1(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class AddressLine1DocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.AddressLine1Document
{
    private static final long serialVersionUID = 1L;
    
    public AddressLine1DocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ADDRESSLINE1$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "AddressLine1");
    
    
    /**
     * Gets the "AddressLine1" element
     */
    public java.lang.String getAddressLine1()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ADDRESSLINE1$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "AddressLine1" element
     */
    public com.idanalytics.products.certainid.AddressLine1Document.AddressLine1 xgetAddressLine1()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.AddressLine1Document.AddressLine1 target = null;
            target = (com.idanalytics.products.certainid.AddressLine1Document.AddressLine1)get_store().find_element_user(ADDRESSLINE1$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "AddressLine1" element
     */
    public void setAddressLine1(java.lang.String addressLine1)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ADDRESSLINE1$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ADDRESSLINE1$0);
            }
            target.setStringValue(addressLine1);
        }
    }
    
    /**
     * Sets (as xml) the "AddressLine1" element
     */
    public void xsetAddressLine1(com.idanalytics.products.certainid.AddressLine1Document.AddressLine1 addressLine1)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.AddressLine1Document.AddressLine1 target = null;
            target = (com.idanalytics.products.certainid.AddressLine1Document.AddressLine1)get_store().find_element_user(ADDRESSLINE1$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.AddressLine1Document.AddressLine1)get_store().add_element_user(ADDRESSLINE1$0);
            }
            target.set(addressLine1);
        }
    }
    /**
     * An XML AddressLine1(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.AddressLine1Document$AddressLine1.
     */
    public static class AddressLine1Impl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.certainid.AddressLine1Document.AddressLine1
    {
        private static final long serialVersionUID = 1L;
        
        public AddressLine1Impl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected AddressLine1Impl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
