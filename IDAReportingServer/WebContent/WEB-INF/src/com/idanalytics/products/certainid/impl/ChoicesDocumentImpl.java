/*
 * An XML document type.
 * Localname: Choices
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.ChoicesDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one Choices(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class ChoicesDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.ChoicesDocument
{
    private static final long serialVersionUID = 1L;
    
    public ChoicesDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CHOICES$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "Choices");
    
    
    /**
     * Gets the "Choices" element
     */
    public com.idanalytics.products.certainid.ChoicesDocument.Choices getChoices()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.ChoicesDocument.Choices target = null;
            target = (com.idanalytics.products.certainid.ChoicesDocument.Choices)get_store().find_element_user(CHOICES$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "Choices" element
     */
    public void setChoices(com.idanalytics.products.certainid.ChoicesDocument.Choices choices)
    {
        generatedSetterHelperImpl(choices, CHOICES$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "Choices" element
     */
    public com.idanalytics.products.certainid.ChoicesDocument.Choices addNewChoices()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.ChoicesDocument.Choices target = null;
            target = (com.idanalytics.products.certainid.ChoicesDocument.Choices)get_store().add_element_user(CHOICES$0);
            return target;
        }
    }
    /**
     * An XML Choices(@http://idanalytics.com/products/certainid).
     *
     * This is a complex type.
     */
    public static class ChoicesImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.ChoicesDocument.Choices
    {
        private static final long serialVersionUID = 1L;
        
        public ChoicesImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName CHOICE$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "Choice");
        
        
        /**
         * Gets array of all "Choice" elements
         */
        public java.lang.String[] getChoiceArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(CHOICE$0, targetList);
                java.lang.String[] result = new java.lang.String[targetList.size()];
                for (int i = 0, len = targetList.size() ; i < len ; i++)
                    result[i] = ((org.apache.xmlbeans.SimpleValue)targetList.get(i)).getStringValue();
                return result;
            }
        }
        
        /**
         * Gets ith "Choice" element
         */
        public java.lang.String getChoiceArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CHOICE$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) array of all "Choice" elements
         */
        public org.apache.xmlbeans.XmlString[] xgetChoiceArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(CHOICE$0, targetList);
                org.apache.xmlbeans.XmlString[] result = new org.apache.xmlbeans.XmlString[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets (as xml) ith "Choice" element
         */
        public org.apache.xmlbeans.XmlString xgetChoiceArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CHOICE$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "Choice" element
         */
        public int sizeOfChoiceArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(CHOICE$0);
            }
        }
        
        /**
         * Sets array of all "Choice" element
         */
        public void setChoiceArray(java.lang.String[] choiceArray)
        {
            synchronized (monitor())
            {
                check_orphaned();
                arraySetterHelper(choiceArray, CHOICE$0);
            }
        }
        
        /**
         * Sets ith "Choice" element
         */
        public void setChoiceArray(int i, java.lang.String choice)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CHOICE$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.setStringValue(choice);
            }
        }
        
        /**
         * Sets (as xml) array of all "Choice" element
         */
        public void xsetChoiceArray(org.apache.xmlbeans.XmlString[]choiceArray)
        {
            synchronized (monitor())
            {
                check_orphaned();
                arraySetterHelper(choiceArray, CHOICE$0);
            }
        }
        
        /**
         * Sets (as xml) ith "Choice" element
         */
        public void xsetChoiceArray(int i, org.apache.xmlbeans.XmlString choice)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CHOICE$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(choice);
            }
        }
        
        /**
         * Inserts the value as the ith "Choice" element
         */
        public void insertChoice(int i, java.lang.String choice)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = 
                    (org.apache.xmlbeans.SimpleValue)get_store().insert_element_user(CHOICE$0, i);
                target.setStringValue(choice);
            }
        }
        
        /**
         * Appends the value as the last "Choice" element
         */
        public void addChoice(java.lang.String choice)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CHOICE$0);
                target.setStringValue(choice);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "Choice" element
         */
        public org.apache.xmlbeans.XmlString insertNewChoice(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().insert_element_user(CHOICE$0, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "Choice" element
         */
        public org.apache.xmlbeans.XmlString addNewChoice()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(CHOICE$0);
                return target;
            }
        }
        
        /**
         * Removes the ith "Choice" element
         */
        public void removeChoice(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(CHOICE$0, i);
            }
        }
    }
}
