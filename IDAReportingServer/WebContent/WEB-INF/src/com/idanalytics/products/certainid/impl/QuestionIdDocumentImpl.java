/*
 * An XML document type.
 * Localname: QuestionId
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.QuestionIdDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one QuestionId(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class QuestionIdDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.QuestionIdDocument
{
    private static final long serialVersionUID = 1L;
    
    public QuestionIdDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName QUESTIONID$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "QuestionId");
    
    
    /**
     * Gets the "QuestionId" element
     */
    public java.math.BigInteger getQuestionId()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUESTIONID$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getBigIntegerValue();
        }
    }
    
    /**
     * Gets (as xml) the "QuestionId" element
     */
    public org.apache.xmlbeans.XmlInteger xgetQuestionId()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlInteger target = null;
            target = (org.apache.xmlbeans.XmlInteger)get_store().find_element_user(QUESTIONID$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "QuestionId" element
     */
    public void setQuestionId(java.math.BigInteger questionId)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUESTIONID$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(QUESTIONID$0);
            }
            target.setBigIntegerValue(questionId);
        }
    }
    
    /**
     * Sets (as xml) the "QuestionId" element
     */
    public void xsetQuestionId(org.apache.xmlbeans.XmlInteger questionId)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlInteger target = null;
            target = (org.apache.xmlbeans.XmlInteger)get_store().find_element_user(QUESTIONID$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlInteger)get_store().add_element_user(QUESTIONID$0);
            }
            target.set(questionId);
        }
    }
}
