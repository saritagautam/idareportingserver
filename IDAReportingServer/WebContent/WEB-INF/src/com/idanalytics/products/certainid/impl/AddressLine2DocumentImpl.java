/*
 * An XML document type.
 * Localname: AddressLine2
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.AddressLine2Document
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one AddressLine2(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class AddressLine2DocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.AddressLine2Document
{
    private static final long serialVersionUID = 1L;
    
    public AddressLine2DocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ADDRESSLINE2$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "AddressLine2");
    
    
    /**
     * Gets the "AddressLine2" element
     */
    public java.lang.String getAddressLine2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ADDRESSLINE2$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "AddressLine2" element
     */
    public com.idanalytics.products.certainid.AddressLine2Document.AddressLine2 xgetAddressLine2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.AddressLine2Document.AddressLine2 target = null;
            target = (com.idanalytics.products.certainid.AddressLine2Document.AddressLine2)get_store().find_element_user(ADDRESSLINE2$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "AddressLine2" element
     */
    public void setAddressLine2(java.lang.String addressLine2)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ADDRESSLINE2$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ADDRESSLINE2$0);
            }
            target.setStringValue(addressLine2);
        }
    }
    
    /**
     * Sets (as xml) the "AddressLine2" element
     */
    public void xsetAddressLine2(com.idanalytics.products.certainid.AddressLine2Document.AddressLine2 addressLine2)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.AddressLine2Document.AddressLine2 target = null;
            target = (com.idanalytics.products.certainid.AddressLine2Document.AddressLine2)get_store().find_element_user(ADDRESSLINE2$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.AddressLine2Document.AddressLine2)get_store().add_element_user(ADDRESSLINE2$0);
            }
            target.set(addressLine2);
        }
    }
    /**
     * An XML AddressLine2(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.AddressLine2Document$AddressLine2.
     */
    public static class AddressLine2Impl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.certainid.AddressLine2Document.AddressLine2
    {
        private static final long serialVersionUID = 1L;
        
        public AddressLine2Impl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected AddressLine2Impl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
