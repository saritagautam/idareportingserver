/*
 * An XML document type.
 * Localname: Answers
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.AnswersDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one Answers(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class AnswersDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.AnswersDocument
{
    private static final long serialVersionUID = 1L;
    
    public AnswersDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ANSWERS$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "Answers");
    
    
    /**
     * Gets the "Answers" element
     */
    public com.idanalytics.products.certainid.AnswersDocument.Answers getAnswers()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.AnswersDocument.Answers target = null;
            target = (com.idanalytics.products.certainid.AnswersDocument.Answers)get_store().find_element_user(ANSWERS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "Answers" element
     */
    public void setAnswers(com.idanalytics.products.certainid.AnswersDocument.Answers answers)
    {
        generatedSetterHelperImpl(answers, ANSWERS$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "Answers" element
     */
    public com.idanalytics.products.certainid.AnswersDocument.Answers addNewAnswers()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.AnswersDocument.Answers target = null;
            target = (com.idanalytics.products.certainid.AnswersDocument.Answers)get_store().add_element_user(ANSWERS$0);
            return target;
        }
    }
    /**
     * An XML Answers(@http://idanalytics.com/products/certainid).
     *
     * This is a complex type.
     */
    public static class AnswersImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.AnswersDocument.Answers
    {
        private static final long serialVersionUID = 1L;
        
        public AnswersImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName ANSWER$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "Answer");
        
        
        /**
         * Gets array of all "Answer" elements
         */
        public com.idanalytics.products.certainid.AnswerDocument.Answer[] getAnswerArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(ANSWER$0, targetList);
                com.idanalytics.products.certainid.AnswerDocument.Answer[] result = new com.idanalytics.products.certainid.AnswerDocument.Answer[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "Answer" element
         */
        public com.idanalytics.products.certainid.AnswerDocument.Answer getAnswerArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.certainid.AnswerDocument.Answer target = null;
                target = (com.idanalytics.products.certainid.AnswerDocument.Answer)get_store().find_element_user(ANSWER$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "Answer" element
         */
        public int sizeOfAnswerArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(ANSWER$0);
            }
        }
        
        /**
         * Sets array of all "Answer" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setAnswerArray(com.idanalytics.products.certainid.AnswerDocument.Answer[] answerArray)
        {
            check_orphaned();
            arraySetterHelper(answerArray, ANSWER$0);
        }
        
        /**
         * Sets ith "Answer" element
         */
        public void setAnswerArray(int i, com.idanalytics.products.certainid.AnswerDocument.Answer answer)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.certainid.AnswerDocument.Answer target = null;
                target = (com.idanalytics.products.certainid.AnswerDocument.Answer)get_store().find_element_user(ANSWER$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(answer);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "Answer" element
         */
        public com.idanalytics.products.certainid.AnswerDocument.Answer insertNewAnswer(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.certainid.AnswerDocument.Answer target = null;
                target = (com.idanalytics.products.certainid.AnswerDocument.Answer)get_store().insert_element_user(ANSWER$0, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "Answer" element
         */
        public com.idanalytics.products.certainid.AnswerDocument.Answer addNewAnswer()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.certainid.AnswerDocument.Answer target = null;
                target = (com.idanalytics.products.certainid.AnswerDocument.Answer)get_store().add_element_user(ANSWER$0);
                return target;
            }
        }
        
        /**
         * Removes the ith "Answer" element
         */
        public void removeAnswer(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(ANSWER$0, i);
            }
        }
    }
}
