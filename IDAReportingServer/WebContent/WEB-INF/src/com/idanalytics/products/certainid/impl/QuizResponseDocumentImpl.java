/*
 * An XML document type.
 * Localname: QuizResponse
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.QuizResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one QuizResponse(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class QuizResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.QuizResponseDocument
{
    private static final long serialVersionUID = 1L;
    
    public QuizResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName QUIZRESPONSE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "QuizResponse");
    
    
    /**
     * Gets the "QuizResponse" element
     */
    public com.idanalytics.products.certainid.QuizResponseDocument.QuizResponse getQuizResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.QuizResponseDocument.QuizResponse target = null;
            target = (com.idanalytics.products.certainid.QuizResponseDocument.QuizResponse)get_store().find_element_user(QUIZRESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "QuizResponse" element
     */
    public void setQuizResponse(com.idanalytics.products.certainid.QuizResponseDocument.QuizResponse quizResponse)
    {
        generatedSetterHelperImpl(quizResponse, QUIZRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "QuizResponse" element
     */
    public com.idanalytics.products.certainid.QuizResponseDocument.QuizResponse addNewQuizResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.QuizResponseDocument.QuizResponse target = null;
            target = (com.idanalytics.products.certainid.QuizResponseDocument.QuizResponse)get_store().add_element_user(QUIZRESPONSE$0);
            return target;
        }
    }
    /**
     * An XML QuizResponse(@http://idanalytics.com/products/certainid).
     *
     * This is a complex type.
     */
    public static class QuizResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.QuizResponseDocument.QuizResponse
    {
        private static final long serialVersionUID = 1L;
        
        public QuizResponseImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName IDASEQUENCE$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "IDASequence");
        private static final javax.xml.namespace.QName APPID$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "AppID");
        private static final javax.xml.namespace.QName IDATIMESTAMP$4 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "IDATimeStamp");
        private static final javax.xml.namespace.QName IDASTATUS$6 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "IDAStatus");
        private static final javax.xml.namespace.QName QUIZID$8 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "QuizId");
        private static final javax.xml.namespace.QName QUESTIONS$10 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "Questions");
        private static final javax.xml.namespace.QName RESULT$12 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "Result");
        private static final javax.xml.namespace.QName QUIZSTATE$14 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "QuizState");
        
        
        /**
         * Gets the "IDASequence" element
         */
        public java.lang.String getIDASequence()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASEQUENCE$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "IDASequence" element
         */
        public com.idanalytics.products.certainid.IDASequenceDocument.IDASequence xgetIDASequence()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.certainid.IDASequenceDocument.IDASequence target = null;
                target = (com.idanalytics.products.certainid.IDASequenceDocument.IDASequence)get_store().find_element_user(IDASEQUENCE$0, 0);
                return target;
            }
        }
        
        /**
         * True if has "IDASequence" element
         */
        public boolean isSetIDASequence()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(IDASEQUENCE$0) != 0;
            }
        }
        
        /**
         * Sets the "IDASequence" element
         */
        public void setIDASequence(java.lang.String idaSequence)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASEQUENCE$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDASEQUENCE$0);
                }
                target.setStringValue(idaSequence);
            }
        }
        
        /**
         * Sets (as xml) the "IDASequence" element
         */
        public void xsetIDASequence(com.idanalytics.products.certainid.IDASequenceDocument.IDASequence idaSequence)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.certainid.IDASequenceDocument.IDASequence target = null;
                target = (com.idanalytics.products.certainid.IDASequenceDocument.IDASequence)get_store().find_element_user(IDASEQUENCE$0, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.certainid.IDASequenceDocument.IDASequence)get_store().add_element_user(IDASEQUENCE$0);
                }
                target.set(idaSequence);
            }
        }
        
        /**
         * Unsets the "IDASequence" element
         */
        public void unsetIDASequence()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(IDASEQUENCE$0, 0);
            }
        }
        
        /**
         * Gets the "AppID" element
         */
        public java.lang.String getAppID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(APPID$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "AppID" element
         */
        public com.idanalytics.products.certainid.AppIDDocument.AppID xgetAppID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.certainid.AppIDDocument.AppID target = null;
                target = (com.idanalytics.products.certainid.AppIDDocument.AppID)get_store().find_element_user(APPID$2, 0);
                return target;
            }
        }
        
        /**
         * True if has "AppID" element
         */
        public boolean isSetAppID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(APPID$2) != 0;
            }
        }
        
        /**
         * Sets the "AppID" element
         */
        public void setAppID(java.lang.String appID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(APPID$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(APPID$2);
                }
                target.setStringValue(appID);
            }
        }
        
        /**
         * Sets (as xml) the "AppID" element
         */
        public void xsetAppID(com.idanalytics.products.certainid.AppIDDocument.AppID appID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.certainid.AppIDDocument.AppID target = null;
                target = (com.idanalytics.products.certainid.AppIDDocument.AppID)get_store().find_element_user(APPID$2, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.certainid.AppIDDocument.AppID)get_store().add_element_user(APPID$2);
                }
                target.set(appID);
            }
        }
        
        /**
         * Unsets the "AppID" element
         */
        public void unsetAppID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(APPID$2, 0);
            }
        }
        
        /**
         * Gets the "IDATimeStamp" element
         */
        public java.util.Calendar getIDATimeStamp()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDATIMESTAMP$4, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getCalendarValue();
            }
        }
        
        /**
         * Gets (as xml) the "IDATimeStamp" element
         */
        public com.idanalytics.products.certainid.IDATimeStampDocument.IDATimeStamp xgetIDATimeStamp()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.certainid.IDATimeStampDocument.IDATimeStamp target = null;
                target = (com.idanalytics.products.certainid.IDATimeStampDocument.IDATimeStamp)get_store().find_element_user(IDATIMESTAMP$4, 0);
                return target;
            }
        }
        
        /**
         * Sets the "IDATimeStamp" element
         */
        public void setIDATimeStamp(java.util.Calendar idaTimeStamp)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDATIMESTAMP$4, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDATIMESTAMP$4);
                }
                target.setCalendarValue(idaTimeStamp);
            }
        }
        
        /**
         * Sets (as xml) the "IDATimeStamp" element
         */
        public void xsetIDATimeStamp(com.idanalytics.products.certainid.IDATimeStampDocument.IDATimeStamp idaTimeStamp)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.certainid.IDATimeStampDocument.IDATimeStamp target = null;
                target = (com.idanalytics.products.certainid.IDATimeStampDocument.IDATimeStamp)get_store().find_element_user(IDATIMESTAMP$4, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.certainid.IDATimeStampDocument.IDATimeStamp)get_store().add_element_user(IDATIMESTAMP$4);
                }
                target.set(idaTimeStamp);
            }
        }
        
        /**
         * Gets the "IDAStatus" element
         */
        public int getIDAStatus()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASTATUS$6, 0);
                if (target == null)
                {
                    return 0;
                }
                return target.getIntValue();
            }
        }
        
        /**
         * Gets (as xml) the "IDAStatus" element
         */
        public com.idanalytics.products.certainid.IDAStatusDocument.IDAStatus xgetIDAStatus()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.certainid.IDAStatusDocument.IDAStatus target = null;
                target = (com.idanalytics.products.certainid.IDAStatusDocument.IDAStatus)get_store().find_element_user(IDASTATUS$6, 0);
                return target;
            }
        }
        
        /**
         * Sets the "IDAStatus" element
         */
        public void setIDAStatus(int idaStatus)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASTATUS$6, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDASTATUS$6);
                }
                target.setIntValue(idaStatus);
            }
        }
        
        /**
         * Sets (as xml) the "IDAStatus" element
         */
        public void xsetIDAStatus(com.idanalytics.products.certainid.IDAStatusDocument.IDAStatus idaStatus)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.certainid.IDAStatusDocument.IDAStatus target = null;
                target = (com.idanalytics.products.certainid.IDAStatusDocument.IDAStatus)get_store().find_element_user(IDASTATUS$6, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.certainid.IDAStatusDocument.IDAStatus)get_store().add_element_user(IDASTATUS$6);
                }
                target.set(idaStatus);
            }
        }
        
        /**
         * Gets the "QuizId" element
         */
        public java.lang.String getQuizId()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUIZID$8, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "QuizId" element
         */
        public com.idanalytics.products.certainid.QuizIdDocument.QuizId xgetQuizId()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.certainid.QuizIdDocument.QuizId target = null;
                target = (com.idanalytics.products.certainid.QuizIdDocument.QuizId)get_store().find_element_user(QUIZID$8, 0);
                return target;
            }
        }
        
        /**
         * True if has "QuizId" element
         */
        public boolean isSetQuizId()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(QUIZID$8) != 0;
            }
        }
        
        /**
         * Sets the "QuizId" element
         */
        public void setQuizId(java.lang.String quizId)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUIZID$8, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(QUIZID$8);
                }
                target.setStringValue(quizId);
            }
        }
        
        /**
         * Sets (as xml) the "QuizId" element
         */
        public void xsetQuizId(com.idanalytics.products.certainid.QuizIdDocument.QuizId quizId)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.certainid.QuizIdDocument.QuizId target = null;
                target = (com.idanalytics.products.certainid.QuizIdDocument.QuizId)get_store().find_element_user(QUIZID$8, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.certainid.QuizIdDocument.QuizId)get_store().add_element_user(QUIZID$8);
                }
                target.set(quizId);
            }
        }
        
        /**
         * Unsets the "QuizId" element
         */
        public void unsetQuizId()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(QUIZID$8, 0);
            }
        }
        
        /**
         * Gets the "Questions" element
         */
        public com.idanalytics.products.certainid.QuestionsDocument.Questions getQuestions()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.certainid.QuestionsDocument.Questions target = null;
                target = (com.idanalytics.products.certainid.QuestionsDocument.Questions)get_store().find_element_user(QUESTIONS$10, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "Questions" element
         */
        public boolean isSetQuestions()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(QUESTIONS$10) != 0;
            }
        }
        
        /**
         * Sets the "Questions" element
         */
        public void setQuestions(com.idanalytics.products.certainid.QuestionsDocument.Questions questions)
        {
            generatedSetterHelperImpl(questions, QUESTIONS$10, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "Questions" element
         */
        public com.idanalytics.products.certainid.QuestionsDocument.Questions addNewQuestions()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.certainid.QuestionsDocument.Questions target = null;
                target = (com.idanalytics.products.certainid.QuestionsDocument.Questions)get_store().add_element_user(QUESTIONS$10);
                return target;
            }
        }
        
        /**
         * Unsets the "Questions" element
         */
        public void unsetQuestions()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(QUESTIONS$10, 0);
            }
        }
        
        /**
         * Gets the "Result" element
         */
        public com.idanalytics.products.certainid.ResultDocument.Result.Enum getResult()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RESULT$12, 0);
                if (target == null)
                {
                    return null;
                }
                return (com.idanalytics.products.certainid.ResultDocument.Result.Enum)target.getEnumValue();
            }
        }
        
        /**
         * Gets (as xml) the "Result" element
         */
        public com.idanalytics.products.certainid.ResultDocument.Result xgetResult()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.certainid.ResultDocument.Result target = null;
                target = (com.idanalytics.products.certainid.ResultDocument.Result)get_store().find_element_user(RESULT$12, 0);
                return target;
            }
        }
        
        /**
         * True if has "Result" element
         */
        public boolean isSetResult()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(RESULT$12) != 0;
            }
        }
        
        /**
         * Sets the "Result" element
         */
        public void setResult(com.idanalytics.products.certainid.ResultDocument.Result.Enum result)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RESULT$12, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(RESULT$12);
                }
                target.setEnumValue(result);
            }
        }
        
        /**
         * Sets (as xml) the "Result" element
         */
        public void xsetResult(com.idanalytics.products.certainid.ResultDocument.Result result)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.certainid.ResultDocument.Result target = null;
                target = (com.idanalytics.products.certainid.ResultDocument.Result)get_store().find_element_user(RESULT$12, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.certainid.ResultDocument.Result)get_store().add_element_user(RESULT$12);
                }
                target.set(result);
            }
        }
        
        /**
         * Unsets the "Result" element
         */
        public void unsetResult()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(RESULT$12, 0);
            }
        }
        
        /**
         * Gets the "QuizState" element
         */
        public com.idanalytics.products.certainid.QuizStateDocument.QuizState.Enum getQuizState()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUIZSTATE$14, 0);
                if (target == null)
                {
                    return null;
                }
                return (com.idanalytics.products.certainid.QuizStateDocument.QuizState.Enum)target.getEnumValue();
            }
        }
        
        /**
         * Gets (as xml) the "QuizState" element
         */
        public com.idanalytics.products.certainid.QuizStateDocument.QuizState xgetQuizState()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.certainid.QuizStateDocument.QuizState target = null;
                target = (com.idanalytics.products.certainid.QuizStateDocument.QuizState)get_store().find_element_user(QUIZSTATE$14, 0);
                return target;
            }
        }
        
        /**
         * True if has "QuizState" element
         */
        public boolean isSetQuizState()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(QUIZSTATE$14) != 0;
            }
        }
        
        /**
         * Sets the "QuizState" element
         */
        public void setQuizState(com.idanalytics.products.certainid.QuizStateDocument.QuizState.Enum quizState)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUIZSTATE$14, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(QUIZSTATE$14);
                }
                target.setEnumValue(quizState);
            }
        }
        
        /**
         * Sets (as xml) the "QuizState" element
         */
        public void xsetQuizState(com.idanalytics.products.certainid.QuizStateDocument.QuizState quizState)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.certainid.QuizStateDocument.QuizState target = null;
                target = (com.idanalytics.products.certainid.QuizStateDocument.QuizState)get_store().find_element_user(QUIZSTATE$14, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.certainid.QuizStateDocument.QuizState)get_store().add_element_user(QUIZSTATE$14);
                }
                target.set(quizState);
            }
        }
        
        /**
         * Unsets the "QuizState" element
         */
        public void unsetQuizState()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(QUIZSTATE$14, 0);
            }
        }
    }
}
