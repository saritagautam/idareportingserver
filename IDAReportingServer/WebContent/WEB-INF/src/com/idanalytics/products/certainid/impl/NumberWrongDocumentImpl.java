/*
 * An XML document type.
 * Localname: NumberWrong
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.NumberWrongDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one NumberWrong(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class NumberWrongDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.NumberWrongDocument
{
    private static final long serialVersionUID = 1L;
    
    public NumberWrongDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName NUMBERWRONG$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "NumberWrong");
    
    
    /**
     * Gets the "NumberWrong" element
     */
    public java.math.BigInteger getNumberWrong()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NUMBERWRONG$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getBigIntegerValue();
        }
    }
    
    /**
     * Gets (as xml) the "NumberWrong" element
     */
    public org.apache.xmlbeans.XmlInteger xgetNumberWrong()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlInteger target = null;
            target = (org.apache.xmlbeans.XmlInteger)get_store().find_element_user(NUMBERWRONG$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "NumberWrong" element
     */
    public void setNumberWrong(java.math.BigInteger numberWrong)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NUMBERWRONG$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(NUMBERWRONG$0);
            }
            target.setBigIntegerValue(numberWrong);
        }
    }
    
    /**
     * Sets (as xml) the "NumberWrong" element
     */
    public void xsetNumberWrong(org.apache.xmlbeans.XmlInteger numberWrong)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlInteger target = null;
            target = (org.apache.xmlbeans.XmlInteger)get_store().find_element_user(NUMBERWRONG$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlInteger)get_store().add_element_user(NUMBERWRONG$0);
            }
            target.set(numberWrong);
        }
    }
}
