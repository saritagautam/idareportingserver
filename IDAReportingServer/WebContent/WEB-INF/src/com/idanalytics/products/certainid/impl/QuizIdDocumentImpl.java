/*
 * An XML document type.
 * Localname: QuizId
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.QuizIdDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one QuizId(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class QuizIdDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.QuizIdDocument
{
    private static final long serialVersionUID = 1L;
    
    public QuizIdDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName QUIZID$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "QuizId");
    
    
    /**
     * Gets the "QuizId" element
     */
    public java.lang.String getQuizId()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUIZID$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "QuizId" element
     */
    public com.idanalytics.products.certainid.QuizIdDocument.QuizId xgetQuizId()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.QuizIdDocument.QuizId target = null;
            target = (com.idanalytics.products.certainid.QuizIdDocument.QuizId)get_store().find_element_user(QUIZID$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "QuizId" element
     */
    public void setQuizId(java.lang.String quizId)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUIZID$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(QUIZID$0);
            }
            target.setStringValue(quizId);
        }
    }
    
    /**
     * Sets (as xml) the "QuizId" element
     */
    public void xsetQuizId(com.idanalytics.products.certainid.QuizIdDocument.QuizId quizId)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.QuizIdDocument.QuizId target = null;
            target = (com.idanalytics.products.certainid.QuizIdDocument.QuizId)get_store().find_element_user(QUIZID$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.QuizIdDocument.QuizId)get_store().add_element_user(QUIZID$0);
            }
            target.set(quizId);
        }
    }
    /**
     * An XML QuizId(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.QuizIdDocument$QuizId.
     */
    public static class QuizIdImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.certainid.QuizIdDocument.QuizId
    {
        private static final long serialVersionUID = 1L;
        
        public QuizIdImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected QuizIdImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
