/*
 * An XML document type.
 * Localname: IDASequence
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.IDASequenceDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one IDASequence(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class IDASequenceDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.IDASequenceDocument
{
    private static final long serialVersionUID = 1L;
    
    public IDASequenceDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IDASEQUENCE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "IDASequence");
    
    
    /**
     * Gets the "IDASequence" element
     */
    public java.lang.String getIDASequence()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASEQUENCE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IDASequence" element
     */
    public com.idanalytics.products.certainid.IDASequenceDocument.IDASequence xgetIDASequence()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.IDASequenceDocument.IDASequence target = null;
            target = (com.idanalytics.products.certainid.IDASequenceDocument.IDASequence)get_store().find_element_user(IDASEQUENCE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IDASequence" element
     */
    public void setIDASequence(java.lang.String idaSequence)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASEQUENCE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDASEQUENCE$0);
            }
            target.setStringValue(idaSequence);
        }
    }
    
    /**
     * Sets (as xml) the "IDASequence" element
     */
    public void xsetIDASequence(com.idanalytics.products.certainid.IDASequenceDocument.IDASequence idaSequence)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.IDASequenceDocument.IDASequence target = null;
            target = (com.idanalytics.products.certainid.IDASequenceDocument.IDASequence)get_store().find_element_user(IDASEQUENCE$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.IDASequenceDocument.IDASequence)get_store().add_element_user(IDASEQUENCE$0);
            }
            target.set(idaSequence);
        }
    }
    /**
     * An XML IDASequence(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.IDASequenceDocument$IDASequence.
     */
    public static class IDASequenceImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.certainid.IDASequenceDocument.IDASequence
    {
        private static final long serialVersionUID = 1L;
        
        public IDASequenceImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected IDASequenceImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
