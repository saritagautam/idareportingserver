/*
 * An XML document type.
 * Localname: IDType
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.IDTypeDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one IDType(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class IDTypeDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.IDTypeDocument
{
    private static final long serialVersionUID = 1L;
    
    public IDTypeDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IDTYPE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "IDType");
    
    
    /**
     * Gets the "IDType" element
     */
    public java.lang.String getIDType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDTYPE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IDType" element
     */
    public com.idanalytics.products.certainid.IDTypeDocument.IDType xgetIDType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.IDTypeDocument.IDType target = null;
            target = (com.idanalytics.products.certainid.IDTypeDocument.IDType)get_store().find_element_user(IDTYPE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IDType" element
     */
    public void setIDType(java.lang.String idType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDTYPE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDTYPE$0);
            }
            target.setStringValue(idType);
        }
    }
    
    /**
     * Sets (as xml) the "IDType" element
     */
    public void xsetIDType(com.idanalytics.products.certainid.IDTypeDocument.IDType idType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.IDTypeDocument.IDType target = null;
            target = (com.idanalytics.products.certainid.IDTypeDocument.IDType)get_store().find_element_user(IDTYPE$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.IDTypeDocument.IDType)get_store().add_element_user(IDTYPE$0);
            }
            target.set(idType);
        }
    }
    /**
     * An XML IDType(@http://idanalytics.com/products/certainid).
     *
     * This is a union type. Instances are of one of the following types:
     *     com.idanalytics.products.certainid.IDTypeDocument$IDType$Member
     *     com.idanalytics.products.certainid.IDTypeDocument$IDType$Member2
     */
    public static class IDTypeImpl extends org.apache.xmlbeans.impl.values.XmlUnionImpl implements com.idanalytics.products.certainid.IDTypeDocument.IDType, com.idanalytics.products.certainid.IDTypeDocument.IDType.Member, com.idanalytics.products.certainid.IDTypeDocument.IDType.Member2
    {
        private static final long serialVersionUID = 1L;
        
        public IDTypeImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected IDTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
        /**
         * An anonymous inner XML type.
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.certainid.IDTypeDocument$IDType$Member.
         */
        public static class MemberImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.certainid.IDTypeDocument.IDType.Member
        {
            private static final long serialVersionUID = 1L;
            
            public MemberImpl(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected MemberImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
        /**
         * An anonymous inner XML type.
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.certainid.IDTypeDocument$IDType$Member2.
         */
        public static class MemberImpl2 extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.idanalytics.products.certainid.IDTypeDocument.IDType.Member2
        {
            private static final long serialVersionUID = 1L;
            
            public MemberImpl2(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected MemberImpl2(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
    }
}
