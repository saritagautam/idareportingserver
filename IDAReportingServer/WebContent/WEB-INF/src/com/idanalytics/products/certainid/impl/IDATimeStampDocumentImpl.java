/*
 * An XML document type.
 * Localname: IDATimeStamp
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.IDATimeStampDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one IDATimeStamp(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class IDATimeStampDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.IDATimeStampDocument
{
    private static final long serialVersionUID = 1L;
    
    public IDATimeStampDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IDATIMESTAMP$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "IDATimeStamp");
    
    
    /**
     * Gets the "IDATimeStamp" element
     */
    public java.util.Calendar getIDATimeStamp()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDATIMESTAMP$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getCalendarValue();
        }
    }
    
    /**
     * Gets (as xml) the "IDATimeStamp" element
     */
    public com.idanalytics.products.certainid.IDATimeStampDocument.IDATimeStamp xgetIDATimeStamp()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.IDATimeStampDocument.IDATimeStamp target = null;
            target = (com.idanalytics.products.certainid.IDATimeStampDocument.IDATimeStamp)get_store().find_element_user(IDATIMESTAMP$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IDATimeStamp" element
     */
    public void setIDATimeStamp(java.util.Calendar idaTimeStamp)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDATIMESTAMP$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDATIMESTAMP$0);
            }
            target.setCalendarValue(idaTimeStamp);
        }
    }
    
    /**
     * Sets (as xml) the "IDATimeStamp" element
     */
    public void xsetIDATimeStamp(com.idanalytics.products.certainid.IDATimeStampDocument.IDATimeStamp idaTimeStamp)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.IDATimeStampDocument.IDATimeStamp target = null;
            target = (com.idanalytics.products.certainid.IDATimeStampDocument.IDATimeStamp)get_store().find_element_user(IDATIMESTAMP$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.IDATimeStampDocument.IDATimeStamp)get_store().add_element_user(IDATIMESTAMP$0);
            }
            target.set(idaTimeStamp);
        }
    }
    /**
     * An XML IDATimeStamp(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.IDATimeStampDocument$IDATimeStamp.
     */
    public static class IDATimeStampImpl extends org.apache.xmlbeans.impl.values.JavaGDateHolderEx implements com.idanalytics.products.certainid.IDATimeStampDocument.IDATimeStamp
    {
        private static final long serialVersionUID = 1L;
        
        public IDATimeStampImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected IDATimeStampImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
