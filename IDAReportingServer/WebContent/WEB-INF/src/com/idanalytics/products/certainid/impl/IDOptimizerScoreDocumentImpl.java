/*
 * An XML document type.
 * Localname: IDOptimizerScore
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.IDOptimizerScoreDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one IDOptimizerScore(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class IDOptimizerScoreDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.IDOptimizerScoreDocument
{
    private static final long serialVersionUID = 1L;
    
    public IDOptimizerScoreDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IDOPTIMIZERSCORE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "IDOptimizerScore");
    
    
    /**
     * Gets the "IDOptimizerScore" element
     */
    public java.lang.String getIDOptimizerScore()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDOPTIMIZERSCORE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IDOptimizerScore" element
     */
    public com.idanalytics.products.certainid.IDOptimizerScoreDocument.IDOptimizerScore xgetIDOptimizerScore()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.IDOptimizerScoreDocument.IDOptimizerScore target = null;
            target = (com.idanalytics.products.certainid.IDOptimizerScoreDocument.IDOptimizerScore)get_store().find_element_user(IDOPTIMIZERSCORE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IDOptimizerScore" element
     */
    public void setIDOptimizerScore(java.lang.String idOptimizerScore)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDOPTIMIZERSCORE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDOPTIMIZERSCORE$0);
            }
            target.setStringValue(idOptimizerScore);
        }
    }
    
    /**
     * Sets (as xml) the "IDOptimizerScore" element
     */
    public void xsetIDOptimizerScore(com.idanalytics.products.certainid.IDOptimizerScoreDocument.IDOptimizerScore idOptimizerScore)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.IDOptimizerScoreDocument.IDOptimizerScore target = null;
            target = (com.idanalytics.products.certainid.IDOptimizerScoreDocument.IDOptimizerScore)get_store().find_element_user(IDOPTIMIZERSCORE$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.IDOptimizerScoreDocument.IDOptimizerScore)get_store().add_element_user(IDOPTIMIZERSCORE$0);
            }
            target.set(idOptimizerScore);
        }
    }
    /**
     * An XML IDOptimizerScore(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.IDOptimizerScoreDocument$IDOptimizerScore.
     */
    public static class IDOptimizerScoreImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.certainid.IDOptimizerScoreDocument.IDOptimizerScore
    {
        private static final long serialVersionUID = 1L;
        
        public IDOptimizerScoreImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected IDOptimizerScoreImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
