/*
 * An XML document type.
 * Localname: IDOrigin
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.IDOriginDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one IDOrigin(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class IDOriginDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.IDOriginDocument
{
    private static final long serialVersionUID = 1L;
    
    public IDOriginDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IDORIGIN$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "IDOrigin");
    
    
    /**
     * Gets the "IDOrigin" element
     */
    public java.lang.String getIDOrigin()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDORIGIN$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IDOrigin" element
     */
    public com.idanalytics.products.certainid.IDOriginDocument.IDOrigin xgetIDOrigin()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.IDOriginDocument.IDOrigin target = null;
            target = (com.idanalytics.products.certainid.IDOriginDocument.IDOrigin)get_store().find_element_user(IDORIGIN$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IDOrigin" element
     */
    public void setIDOrigin(java.lang.String idOrigin)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDORIGIN$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDORIGIN$0);
            }
            target.setStringValue(idOrigin);
        }
    }
    
    /**
     * Sets (as xml) the "IDOrigin" element
     */
    public void xsetIDOrigin(com.idanalytics.products.certainid.IDOriginDocument.IDOrigin idOrigin)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.IDOriginDocument.IDOrigin target = null;
            target = (com.idanalytics.products.certainid.IDOriginDocument.IDOrigin)get_store().find_element_user(IDORIGIN$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.IDOriginDocument.IDOrigin)get_store().add_element_user(IDORIGIN$0);
            }
            target.set(idOrigin);
        }
    }
    /**
     * An XML IDOrigin(@http://idanalytics.com/products/certainid).
     *
     * This is a union type. Instances are of one of the following types:
     *     com.idanalytics.products.certainid.IDOriginDocument$IDOrigin$Member
     *     com.idanalytics.products.certainid.IDOriginDocument$IDOrigin$Member2
     */
    public static class IDOriginImpl extends org.apache.xmlbeans.impl.values.XmlUnionImpl implements com.idanalytics.products.certainid.IDOriginDocument.IDOrigin, com.idanalytics.products.certainid.IDOriginDocument.IDOrigin.Member, com.idanalytics.products.certainid.IDOriginDocument.IDOrigin.Member2
    {
        private static final long serialVersionUID = 1L;
        
        public IDOriginImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected IDOriginImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
        /**
         * An anonymous inner XML type.
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.certainid.IDOriginDocument$IDOrigin$Member.
         */
        public static class MemberImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.certainid.IDOriginDocument.IDOrigin.Member
        {
            private static final long serialVersionUID = 1L;
            
            public MemberImpl(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected MemberImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
        /**
         * An anonymous inner XML type.
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.certainid.IDOriginDocument$IDOrigin$Member2.
         */
        public static class MemberImpl2 extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.certainid.IDOriginDocument.IDOrigin.Member2
        {
            private static final long serialVersionUID = 1L;
            
            public MemberImpl2(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected MemberImpl2(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
    }
}
