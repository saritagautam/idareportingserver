/*
 * An XML document type.
 * Localname: Email
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.EmailDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one Email(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class EmailDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.EmailDocument
{
    private static final long serialVersionUID = 1L;
    
    public EmailDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName EMAIL$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "Email");
    
    
    /**
     * Gets the "Email" element
     */
    public java.lang.String getEmail()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EMAIL$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Email" element
     */
    public com.idanalytics.products.certainid.EmailDocument.Email xgetEmail()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.EmailDocument.Email target = null;
            target = (com.idanalytics.products.certainid.EmailDocument.Email)get_store().find_element_user(EMAIL$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Email" element
     */
    public void setEmail(java.lang.String email)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EMAIL$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(EMAIL$0);
            }
            target.setStringValue(email);
        }
    }
    
    /**
     * Sets (as xml) the "Email" element
     */
    public void xsetEmail(com.idanalytics.products.certainid.EmailDocument.Email email)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.EmailDocument.Email target = null;
            target = (com.idanalytics.products.certainid.EmailDocument.Email)get_store().find_element_user(EMAIL$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.EmailDocument.Email)get_store().add_element_user(EMAIL$0);
            }
            target.set(email);
        }
    }
    /**
     * An XML Email(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.EmailDocument$Email.
     */
    public static class EmailImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.certainid.EmailDocument.Email
    {
        private static final long serialVersionUID = 1L;
        
        public EmailImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected EmailImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
