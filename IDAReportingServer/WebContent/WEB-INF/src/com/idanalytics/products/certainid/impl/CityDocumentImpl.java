/*
 * An XML document type.
 * Localname: City
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.CityDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one City(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class CityDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.CityDocument
{
    private static final long serialVersionUID = 1L;
    
    public CityDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CITY$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "City");
    
    
    /**
     * Gets the "City" element
     */
    public java.lang.String getCity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CITY$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "City" element
     */
    public com.idanalytics.products.certainid.CityDocument.City xgetCity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.CityDocument.City target = null;
            target = (com.idanalytics.products.certainid.CityDocument.City)get_store().find_element_user(CITY$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "City" element
     */
    public void setCity(java.lang.String city)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CITY$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CITY$0);
            }
            target.setStringValue(city);
        }
    }
    
    /**
     * Sets (as xml) the "City" element
     */
    public void xsetCity(com.idanalytics.products.certainid.CityDocument.City city)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.CityDocument.City target = null;
            target = (com.idanalytics.products.certainid.CityDocument.City)get_store().find_element_user(CITY$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.CityDocument.City)get_store().add_element_user(CITY$0);
            }
            target.set(city);
        }
    }
    /**
     * An XML City(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.CityDocument$City.
     */
    public static class CityImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.certainid.CityDocument.City
    {
        private static final long serialVersionUID = 1L;
        
        public CityImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected CityImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
