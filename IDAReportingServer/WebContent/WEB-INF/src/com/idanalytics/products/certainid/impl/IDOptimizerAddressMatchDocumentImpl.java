/*
 * An XML document type.
 * Localname: IDOptimizerAddressMatch
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.IDOptimizerAddressMatchDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one IDOptimizerAddressMatch(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class IDOptimizerAddressMatchDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.IDOptimizerAddressMatchDocument
{
    private static final long serialVersionUID = 1L;
    
    public IDOptimizerAddressMatchDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IDOPTIMIZERADDRESSMATCH$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "IDOptimizerAddressMatch");
    
    
    /**
     * Gets the "IDOptimizerAddressMatch" element
     */
    public com.idanalytics.products.certainid.IDOptimizerAddressMatchDocument.IDOptimizerAddressMatch.Enum getIDOptimizerAddressMatch()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDOPTIMIZERADDRESSMATCH$0, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.certainid.IDOptimizerAddressMatchDocument.IDOptimizerAddressMatch.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "IDOptimizerAddressMatch" element
     */
    public com.idanalytics.products.certainid.IDOptimizerAddressMatchDocument.IDOptimizerAddressMatch xgetIDOptimizerAddressMatch()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.IDOptimizerAddressMatchDocument.IDOptimizerAddressMatch target = null;
            target = (com.idanalytics.products.certainid.IDOptimizerAddressMatchDocument.IDOptimizerAddressMatch)get_store().find_element_user(IDOPTIMIZERADDRESSMATCH$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IDOptimizerAddressMatch" element
     */
    public void setIDOptimizerAddressMatch(com.idanalytics.products.certainid.IDOptimizerAddressMatchDocument.IDOptimizerAddressMatch.Enum idOptimizerAddressMatch)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDOPTIMIZERADDRESSMATCH$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDOPTIMIZERADDRESSMATCH$0);
            }
            target.setEnumValue(idOptimizerAddressMatch);
        }
    }
    
    /**
     * Sets (as xml) the "IDOptimizerAddressMatch" element
     */
    public void xsetIDOptimizerAddressMatch(com.idanalytics.products.certainid.IDOptimizerAddressMatchDocument.IDOptimizerAddressMatch idOptimizerAddressMatch)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.IDOptimizerAddressMatchDocument.IDOptimizerAddressMatch target = null;
            target = (com.idanalytics.products.certainid.IDOptimizerAddressMatchDocument.IDOptimizerAddressMatch)get_store().find_element_user(IDOPTIMIZERADDRESSMATCH$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.IDOptimizerAddressMatchDocument.IDOptimizerAddressMatch)get_store().add_element_user(IDOPTIMIZERADDRESSMATCH$0);
            }
            target.set(idOptimizerAddressMatch);
        }
    }
    /**
     * An XML IDOptimizerAddressMatch(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.IDOptimizerAddressMatchDocument$IDOptimizerAddressMatch.
     */
    public static class IDOptimizerAddressMatchImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.idanalytics.products.certainid.IDOptimizerAddressMatchDocument.IDOptimizerAddressMatch
    {
        private static final long serialVersionUID = 1L;
        
        public IDOptimizerAddressMatchImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected IDOptimizerAddressMatchImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
