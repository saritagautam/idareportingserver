/*
 * An XML document type.
 * Localname: IDScoreResultCode4
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.IDScoreResultCode4Document
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one IDScoreResultCode4(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class IDScoreResultCode4DocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.IDScoreResultCode4Document
{
    private static final long serialVersionUID = 1L;
    
    public IDScoreResultCode4DocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IDSCORERESULTCODE4$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "IDScoreResultCode4");
    
    
    /**
     * Gets the "IDScoreResultCode4" element
     */
    public java.lang.String getIDScoreResultCode4()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDSCORERESULTCODE4$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IDScoreResultCode4" element
     */
    public com.idanalytics.products.certainid.IDScoreResultCode4Document.IDScoreResultCode4 xgetIDScoreResultCode4()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.IDScoreResultCode4Document.IDScoreResultCode4 target = null;
            target = (com.idanalytics.products.certainid.IDScoreResultCode4Document.IDScoreResultCode4)get_store().find_element_user(IDSCORERESULTCODE4$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IDScoreResultCode4" element
     */
    public void setIDScoreResultCode4(java.lang.String idScoreResultCode4)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDSCORERESULTCODE4$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDSCORERESULTCODE4$0);
            }
            target.setStringValue(idScoreResultCode4);
        }
    }
    
    /**
     * Sets (as xml) the "IDScoreResultCode4" element
     */
    public void xsetIDScoreResultCode4(com.idanalytics.products.certainid.IDScoreResultCode4Document.IDScoreResultCode4 idScoreResultCode4)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.IDScoreResultCode4Document.IDScoreResultCode4 target = null;
            target = (com.idanalytics.products.certainid.IDScoreResultCode4Document.IDScoreResultCode4)get_store().find_element_user(IDSCORERESULTCODE4$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.IDScoreResultCode4Document.IDScoreResultCode4)get_store().add_element_user(IDSCORERESULTCODE4$0);
            }
            target.set(idScoreResultCode4);
        }
    }
    /**
     * An XML IDScoreResultCode4(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.IDScoreResultCode4Document$IDScoreResultCode4.
     */
    public static class IDScoreResultCode4Impl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.certainid.IDScoreResultCode4Document.IDScoreResultCode4
    {
        private static final long serialVersionUID = 1L;
        
        public IDScoreResultCode4Impl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected IDScoreResultCode4Impl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
