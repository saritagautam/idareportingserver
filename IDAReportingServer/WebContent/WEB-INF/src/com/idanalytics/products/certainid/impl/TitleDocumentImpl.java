/*
 * An XML document type.
 * Localname: Title
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.TitleDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one Title(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class TitleDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.TitleDocument
{
    private static final long serialVersionUID = 1L;
    
    public TitleDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName TITLE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "Title");
    
    
    /**
     * Gets the "Title" element
     */
    public java.lang.String getTitle()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TITLE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Title" element
     */
    public com.idanalytics.products.certainid.TitleDocument.Title xgetTitle()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.TitleDocument.Title target = null;
            target = (com.idanalytics.products.certainid.TitleDocument.Title)get_store().find_element_user(TITLE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Title" element
     */
    public void setTitle(java.lang.String title)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TITLE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TITLE$0);
            }
            target.setStringValue(title);
        }
    }
    
    /**
     * Sets (as xml) the "Title" element
     */
    public void xsetTitle(com.idanalytics.products.certainid.TitleDocument.Title title)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.TitleDocument.Title target = null;
            target = (com.idanalytics.products.certainid.TitleDocument.Title)get_store().find_element_user(TITLE$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.TitleDocument.Title)get_store().add_element_user(TITLE$0);
            }
            target.set(title);
        }
    }
    /**
     * An XML Title(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.TitleDocument$Title.
     */
    public static class TitleImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.certainid.TitleDocument.Title
    {
        private static final long serialVersionUID = 1L;
        
        public TitleImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected TitleImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
