/*
 * An XML document type.
 * Localname: PatriotActPhysicalAddress
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.PatriotActPhysicalAddressDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one PatriotActPhysicalAddress(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class PatriotActPhysicalAddressDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.PatriotActPhysicalAddressDocument
{
    private static final long serialVersionUID = 1L;
    
    public PatriotActPhysicalAddressDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName PATRIOTACTPHYSICALADDRESS$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "PatriotActPhysicalAddress");
    
    
    /**
     * Gets the "PatriotActPhysicalAddress" element
     */
    public com.idanalytics.products.certainid.PatriotActPhysicalAddressDocument.PatriotActPhysicalAddress.Enum getPatriotActPhysicalAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PATRIOTACTPHYSICALADDRESS$0, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.certainid.PatriotActPhysicalAddressDocument.PatriotActPhysicalAddress.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "PatriotActPhysicalAddress" element
     */
    public com.idanalytics.products.certainid.PatriotActPhysicalAddressDocument.PatriotActPhysicalAddress xgetPatriotActPhysicalAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.PatriotActPhysicalAddressDocument.PatriotActPhysicalAddress target = null;
            target = (com.idanalytics.products.certainid.PatriotActPhysicalAddressDocument.PatriotActPhysicalAddress)get_store().find_element_user(PATRIOTACTPHYSICALADDRESS$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "PatriotActPhysicalAddress" element
     */
    public void setPatriotActPhysicalAddress(com.idanalytics.products.certainid.PatriotActPhysicalAddressDocument.PatriotActPhysicalAddress.Enum patriotActPhysicalAddress)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PATRIOTACTPHYSICALADDRESS$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PATRIOTACTPHYSICALADDRESS$0);
            }
            target.setEnumValue(patriotActPhysicalAddress);
        }
    }
    
    /**
     * Sets (as xml) the "PatriotActPhysicalAddress" element
     */
    public void xsetPatriotActPhysicalAddress(com.idanalytics.products.certainid.PatriotActPhysicalAddressDocument.PatriotActPhysicalAddress patriotActPhysicalAddress)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.PatriotActPhysicalAddressDocument.PatriotActPhysicalAddress target = null;
            target = (com.idanalytics.products.certainid.PatriotActPhysicalAddressDocument.PatriotActPhysicalAddress)get_store().find_element_user(PATRIOTACTPHYSICALADDRESS$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.PatriotActPhysicalAddressDocument.PatriotActPhysicalAddress)get_store().add_element_user(PATRIOTACTPHYSICALADDRESS$0);
            }
            target.set(patriotActPhysicalAddress);
        }
    }
    /**
     * An XML PatriotActPhysicalAddress(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.PatriotActPhysicalAddressDocument$PatriotActPhysicalAddress.
     */
    public static class PatriotActPhysicalAddressImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.idanalytics.products.certainid.PatriotActPhysicalAddressDocument.PatriotActPhysicalAddress
    {
        private static final long serialVersionUID = 1L;
        
        public PatriotActPhysicalAddressImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected PatriotActPhysicalAddressImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
