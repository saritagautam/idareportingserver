/*
 * An XML document type.
 * Localname: Suffix
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.SuffixDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one Suffix(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class SuffixDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.SuffixDocument
{
    private static final long serialVersionUID = 1L;
    
    public SuffixDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SUFFIX$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "Suffix");
    
    
    /**
     * Gets the "Suffix" element
     */
    public java.lang.String getSuffix()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SUFFIX$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Suffix" element
     */
    public com.idanalytics.products.certainid.SuffixDocument.Suffix xgetSuffix()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.SuffixDocument.Suffix target = null;
            target = (com.idanalytics.products.certainid.SuffixDocument.Suffix)get_store().find_element_user(SUFFIX$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Suffix" element
     */
    public void setSuffix(java.lang.String suffix)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SUFFIX$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SUFFIX$0);
            }
            target.setStringValue(suffix);
        }
    }
    
    /**
     * Sets (as xml) the "Suffix" element
     */
    public void xsetSuffix(com.idanalytics.products.certainid.SuffixDocument.Suffix suffix)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.SuffixDocument.Suffix target = null;
            target = (com.idanalytics.products.certainid.SuffixDocument.Suffix)get_store().find_element_user(SUFFIX$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.SuffixDocument.Suffix)get_store().add_element_user(SUFFIX$0);
            }
            target.set(suffix);
        }
    }
    /**
     * An XML Suffix(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.SuffixDocument$Suffix.
     */
    public static class SuffixImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.certainid.SuffixDocument.Suffix
    {
        private static final long serialVersionUID = 1L;
        
        public SuffixImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected SuffixImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
