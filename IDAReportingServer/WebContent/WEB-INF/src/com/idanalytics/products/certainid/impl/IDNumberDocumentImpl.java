/*
 * An XML document type.
 * Localname: IDNumber
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.IDNumberDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one IDNumber(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class IDNumberDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.IDNumberDocument
{
    private static final long serialVersionUID = 1L;
    
    public IDNumberDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IDNUMBER$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "IDNumber");
    
    
    /**
     * Gets the "IDNumber" element
     */
    public java.lang.String getIDNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDNUMBER$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IDNumber" element
     */
    public com.idanalytics.products.certainid.IDNumberDocument.IDNumber xgetIDNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.IDNumberDocument.IDNumber target = null;
            target = (com.idanalytics.products.certainid.IDNumberDocument.IDNumber)get_store().find_element_user(IDNUMBER$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IDNumber" element
     */
    public void setIDNumber(java.lang.String idNumber)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDNUMBER$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDNUMBER$0);
            }
            target.setStringValue(idNumber);
        }
    }
    
    /**
     * Sets (as xml) the "IDNumber" element
     */
    public void xsetIDNumber(com.idanalytics.products.certainid.IDNumberDocument.IDNumber idNumber)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.IDNumberDocument.IDNumber target = null;
            target = (com.idanalytics.products.certainid.IDNumberDocument.IDNumber)get_store().find_element_user(IDNUMBER$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.IDNumberDocument.IDNumber)get_store().add_element_user(IDNUMBER$0);
            }
            target.set(idNumber);
        }
    }
    /**
     * An XML IDNumber(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.IDNumberDocument$IDNumber.
     */
    public static class IDNumberImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.certainid.IDNumberDocument.IDNumber
    {
        private static final long serialVersionUID = 1L;
        
        public IDNumberImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected IDNumberImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
