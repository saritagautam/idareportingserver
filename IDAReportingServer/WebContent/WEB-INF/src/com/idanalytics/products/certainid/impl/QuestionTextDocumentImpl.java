/*
 * An XML document type.
 * Localname: QuestionText
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.QuestionTextDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one QuestionText(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class QuestionTextDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.QuestionTextDocument
{
    private static final long serialVersionUID = 1L;
    
    public QuestionTextDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName QUESTIONTEXT$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "QuestionText");
    
    
    /**
     * Gets the "QuestionText" element
     */
    public java.lang.String getQuestionText()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUESTIONTEXT$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "QuestionText" element
     */
    public org.apache.xmlbeans.XmlString xgetQuestionText()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(QUESTIONTEXT$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "QuestionText" element
     */
    public void setQuestionText(java.lang.String questionText)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUESTIONTEXT$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(QUESTIONTEXT$0);
            }
            target.setStringValue(questionText);
        }
    }
    
    /**
     * Sets (as xml) the "QuestionText" element
     */
    public void xsetQuestionText(org.apache.xmlbeans.XmlString questionText)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(QUESTIONTEXT$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(QUESTIONTEXT$0);
            }
            target.set(questionText);
        }
    }
}
