/*
 * An XML document type.
 * Localname: FirstName
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.FirstNameDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one FirstName(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class FirstNameDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.FirstNameDocument
{
    private static final long serialVersionUID = 1L;
    
    public FirstNameDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName FIRSTNAME$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "FirstName");
    
    
    /**
     * Gets the "FirstName" element
     */
    public java.lang.String getFirstName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FIRSTNAME$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "FirstName" element
     */
    public com.idanalytics.products.certainid.FirstNameDocument.FirstName xgetFirstName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.FirstNameDocument.FirstName target = null;
            target = (com.idanalytics.products.certainid.FirstNameDocument.FirstName)get_store().find_element_user(FIRSTNAME$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "FirstName" element
     */
    public void setFirstName(java.lang.String firstName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FIRSTNAME$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(FIRSTNAME$0);
            }
            target.setStringValue(firstName);
        }
    }
    
    /**
     * Sets (as xml) the "FirstName" element
     */
    public void xsetFirstName(com.idanalytics.products.certainid.FirstNameDocument.FirstName firstName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.FirstNameDocument.FirstName target = null;
            target = (com.idanalytics.products.certainid.FirstNameDocument.FirstName)get_store().find_element_user(FIRSTNAME$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.FirstNameDocument.FirstName)get_store().add_element_user(FIRSTNAME$0);
            }
            target.set(firstName);
        }
    }
    /**
     * An XML FirstName(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.FirstNameDocument$FirstName.
     */
    public static class FirstNameImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.certainid.FirstNameDocument.FirstName
    {
        private static final long serialVersionUID = 1L;
        
        public FirstNameImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected FirstNameImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
