/*
 * An XML document type.
 * Localname: IDOptimizerDOBMatch
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one IDOptimizerDOBMatch(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class IDOptimizerDOBMatchDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument
{
    private static final long serialVersionUID = 1L;
    
    public IDOptimizerDOBMatchDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IDOPTIMIZERDOBMATCH$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "IDOptimizerDOBMatch");
    
    
    /**
     * Gets the "IDOptimizerDOBMatch" element
     */
    public com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument.IDOptimizerDOBMatch.Enum getIDOptimizerDOBMatch()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDOPTIMIZERDOBMATCH$0, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument.IDOptimizerDOBMatch.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "IDOptimizerDOBMatch" element
     */
    public com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument.IDOptimizerDOBMatch xgetIDOptimizerDOBMatch()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument.IDOptimizerDOBMatch target = null;
            target = (com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument.IDOptimizerDOBMatch)get_store().find_element_user(IDOPTIMIZERDOBMATCH$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IDOptimizerDOBMatch" element
     */
    public void setIDOptimizerDOBMatch(com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument.IDOptimizerDOBMatch.Enum idOptimizerDOBMatch)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDOPTIMIZERDOBMATCH$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDOPTIMIZERDOBMATCH$0);
            }
            target.setEnumValue(idOptimizerDOBMatch);
        }
    }
    
    /**
     * Sets (as xml) the "IDOptimizerDOBMatch" element
     */
    public void xsetIDOptimizerDOBMatch(com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument.IDOptimizerDOBMatch idOptimizerDOBMatch)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument.IDOptimizerDOBMatch target = null;
            target = (com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument.IDOptimizerDOBMatch)get_store().find_element_user(IDOPTIMIZERDOBMATCH$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument.IDOptimizerDOBMatch)get_store().add_element_user(IDOPTIMIZERDOBMATCH$0);
            }
            target.set(idOptimizerDOBMatch);
        }
    }
    /**
     * An XML IDOptimizerDOBMatch(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument$IDOptimizerDOBMatch.
     */
    public static class IDOptimizerDOBMatchImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.idanalytics.products.certainid.IDOptimizerDOBMatchDocument.IDOptimizerDOBMatch
    {
        private static final long serialVersionUID = 1L;
        
        public IDOptimizerDOBMatchImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected IDOptimizerDOBMatchImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
