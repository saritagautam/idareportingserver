/*
 * An XML document type.
 * Localname: IDOptimizerSSNMatch
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.IDOptimizerSSNMatchDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one IDOptimizerSSNMatch(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class IDOptimizerSSNMatchDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.IDOptimizerSSNMatchDocument
{
    private static final long serialVersionUID = 1L;
    
    public IDOptimizerSSNMatchDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IDOPTIMIZERSSNMATCH$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "IDOptimizerSSNMatch");
    
    
    /**
     * Gets the "IDOptimizerSSNMatch" element
     */
    public com.idanalytics.products.certainid.IDOptimizerSSNMatchDocument.IDOptimizerSSNMatch.Enum getIDOptimizerSSNMatch()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDOPTIMIZERSSNMATCH$0, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.certainid.IDOptimizerSSNMatchDocument.IDOptimizerSSNMatch.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "IDOptimizerSSNMatch" element
     */
    public com.idanalytics.products.certainid.IDOptimizerSSNMatchDocument.IDOptimizerSSNMatch xgetIDOptimizerSSNMatch()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.IDOptimizerSSNMatchDocument.IDOptimizerSSNMatch target = null;
            target = (com.idanalytics.products.certainid.IDOptimizerSSNMatchDocument.IDOptimizerSSNMatch)get_store().find_element_user(IDOPTIMIZERSSNMATCH$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IDOptimizerSSNMatch" element
     */
    public void setIDOptimizerSSNMatch(com.idanalytics.products.certainid.IDOptimizerSSNMatchDocument.IDOptimizerSSNMatch.Enum idOptimizerSSNMatch)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDOPTIMIZERSSNMATCH$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDOPTIMIZERSSNMATCH$0);
            }
            target.setEnumValue(idOptimizerSSNMatch);
        }
    }
    
    /**
     * Sets (as xml) the "IDOptimizerSSNMatch" element
     */
    public void xsetIDOptimizerSSNMatch(com.idanalytics.products.certainid.IDOptimizerSSNMatchDocument.IDOptimizerSSNMatch idOptimizerSSNMatch)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.IDOptimizerSSNMatchDocument.IDOptimizerSSNMatch target = null;
            target = (com.idanalytics.products.certainid.IDOptimizerSSNMatchDocument.IDOptimizerSSNMatch)get_store().find_element_user(IDOPTIMIZERSSNMATCH$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.IDOptimizerSSNMatchDocument.IDOptimizerSSNMatch)get_store().add_element_user(IDOPTIMIZERSSNMATCH$0);
            }
            target.set(idOptimizerSSNMatch);
        }
    }
    /**
     * An XML IDOptimizerSSNMatch(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.IDOptimizerSSNMatchDocument$IDOptimizerSSNMatch.
     */
    public static class IDOptimizerSSNMatchImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.idanalytics.products.certainid.IDOptimizerSSNMatchDocument.IDOptimizerSSNMatch
    {
        private static final long serialVersionUID = 1L;
        
        public IDOptimizerSSNMatchImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected IDOptimizerSSNMatchImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
