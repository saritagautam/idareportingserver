/*
 * An XML document type.
 * Localname: SSN
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.SSNDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one SSN(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class SSNDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.SSNDocument
{
    private static final long serialVersionUID = 1L;
    
    public SSNDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SSN$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "SSN");
    
    
    /**
     * Gets the "SSN" element
     */
    public java.lang.String getSSN()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SSN$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "SSN" element
     */
    public com.idanalytics.products.certainid.SSNDocument.SSN xgetSSN()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.SSNDocument.SSN target = null;
            target = (com.idanalytics.products.certainid.SSNDocument.SSN)get_store().find_element_user(SSN$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "SSN" element
     */
    public void setSSN(java.lang.String ssn)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SSN$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SSN$0);
            }
            target.setStringValue(ssn);
        }
    }
    
    /**
     * Sets (as xml) the "SSN" element
     */
    public void xsetSSN(com.idanalytics.products.certainid.SSNDocument.SSN ssn)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.SSNDocument.SSN target = null;
            target = (com.idanalytics.products.certainid.SSNDocument.SSN)get_store().find_element_user(SSN$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.SSNDocument.SSN)get_store().add_element_user(SSN$0);
            }
            target.set(ssn);
        }
    }
    /**
     * An XML SSN(@http://idanalytics.com/products/certainid).
     *
     * This is a union type. Instances are of one of the following types:
     *     com.idanalytics.products.certainid.SSNDocument$SSN$Member
     *     com.idanalytics.products.certainid.SSNDocument$SSN$Member2
     *     com.idanalytics.products.certainid.SSNDocument$SSN$Member3
     */
    public static class SSNImpl extends org.apache.xmlbeans.impl.values.XmlUnionImpl implements com.idanalytics.products.certainid.SSNDocument.SSN, com.idanalytics.products.certainid.SSNDocument.SSN.Member, com.idanalytics.products.certainid.SSNDocument.SSN.Member2, com.idanalytics.products.certainid.SSNDocument.SSN.Member3
    {
        private static final long serialVersionUID = 1L;
        
        public SSNImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected SSNImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
        /**
         * An anonymous inner XML type.
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.certainid.SSNDocument$SSN$Member.
         */
        public static class MemberImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.certainid.SSNDocument.SSN.Member
        {
            private static final long serialVersionUID = 1L;
            
            public MemberImpl(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected MemberImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
        /**
         * An anonymous inner XML type.
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.certainid.SSNDocument$SSN$Member2.
         */
        public static class MemberImpl2 extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.certainid.SSNDocument.SSN.Member2
        {
            private static final long serialVersionUID = 1L;
            
            public MemberImpl2(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected MemberImpl2(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
        /**
         * An anonymous inner XML type.
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.certainid.SSNDocument$SSN$Member3.
         */
        public static class MemberImpl3 extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.certainid.SSNDocument.SSN.Member3
        {
            private static final long serialVersionUID = 1L;
            
            public MemberImpl3(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected MemberImpl3(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
    }
}
