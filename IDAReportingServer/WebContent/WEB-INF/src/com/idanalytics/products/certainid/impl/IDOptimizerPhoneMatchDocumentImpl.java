/*
 * An XML document type.
 * Localname: IDOptimizerPhoneMatch
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.IDOptimizerPhoneMatchDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one IDOptimizerPhoneMatch(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class IDOptimizerPhoneMatchDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.IDOptimizerPhoneMatchDocument
{
    private static final long serialVersionUID = 1L;
    
    public IDOptimizerPhoneMatchDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IDOPTIMIZERPHONEMATCH$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "IDOptimizerPhoneMatch");
    
    
    /**
     * Gets the "IDOptimizerPhoneMatch" element
     */
    public com.idanalytics.products.certainid.IDOptimizerPhoneMatchDocument.IDOptimizerPhoneMatch.Enum getIDOptimizerPhoneMatch()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDOPTIMIZERPHONEMATCH$0, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.certainid.IDOptimizerPhoneMatchDocument.IDOptimizerPhoneMatch.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "IDOptimizerPhoneMatch" element
     */
    public com.idanalytics.products.certainid.IDOptimizerPhoneMatchDocument.IDOptimizerPhoneMatch xgetIDOptimizerPhoneMatch()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.IDOptimizerPhoneMatchDocument.IDOptimizerPhoneMatch target = null;
            target = (com.idanalytics.products.certainid.IDOptimizerPhoneMatchDocument.IDOptimizerPhoneMatch)get_store().find_element_user(IDOPTIMIZERPHONEMATCH$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IDOptimizerPhoneMatch" element
     */
    public void setIDOptimizerPhoneMatch(com.idanalytics.products.certainid.IDOptimizerPhoneMatchDocument.IDOptimizerPhoneMatch.Enum idOptimizerPhoneMatch)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDOPTIMIZERPHONEMATCH$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDOPTIMIZERPHONEMATCH$0);
            }
            target.setEnumValue(idOptimizerPhoneMatch);
        }
    }
    
    /**
     * Sets (as xml) the "IDOptimizerPhoneMatch" element
     */
    public void xsetIDOptimizerPhoneMatch(com.idanalytics.products.certainid.IDOptimizerPhoneMatchDocument.IDOptimizerPhoneMatch idOptimizerPhoneMatch)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.IDOptimizerPhoneMatchDocument.IDOptimizerPhoneMatch target = null;
            target = (com.idanalytics.products.certainid.IDOptimizerPhoneMatchDocument.IDOptimizerPhoneMatch)get_store().find_element_user(IDOPTIMIZERPHONEMATCH$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.IDOptimizerPhoneMatchDocument.IDOptimizerPhoneMatch)get_store().add_element_user(IDOPTIMIZERPHONEMATCH$0);
            }
            target.set(idOptimizerPhoneMatch);
        }
    }
    /**
     * An XML IDOptimizerPhoneMatch(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.IDOptimizerPhoneMatchDocument$IDOptimizerPhoneMatch.
     */
    public static class IDOptimizerPhoneMatchImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.idanalytics.products.certainid.IDOptimizerPhoneMatchDocument.IDOptimizerPhoneMatch
    {
        private static final long serialVersionUID = 1L;
        
        public IDOptimizerPhoneMatchImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected IDOptimizerPhoneMatchImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
