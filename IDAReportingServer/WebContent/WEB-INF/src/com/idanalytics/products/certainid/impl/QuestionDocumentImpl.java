/*
 * An XML document type.
 * Localname: Question
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.QuestionDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one Question(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class QuestionDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.QuestionDocument
{
    private static final long serialVersionUID = 1L;
    
    public QuestionDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName QUESTION$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "Question");
    
    
    /**
     * Gets the "Question" element
     */
    public com.idanalytics.products.certainid.QuestionDocument.Question getQuestion()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.QuestionDocument.Question target = null;
            target = (com.idanalytics.products.certainid.QuestionDocument.Question)get_store().find_element_user(QUESTION$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "Question" element
     */
    public void setQuestion(com.idanalytics.products.certainid.QuestionDocument.Question question)
    {
        generatedSetterHelperImpl(question, QUESTION$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "Question" element
     */
    public com.idanalytics.products.certainid.QuestionDocument.Question addNewQuestion()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.QuestionDocument.Question target = null;
            target = (com.idanalytics.products.certainid.QuestionDocument.Question)get_store().add_element_user(QUESTION$0);
            return target;
        }
    }
    /**
     * An XML Question(@http://idanalytics.com/products/certainid).
     *
     * This is a complex type.
     */
    public static class QuestionImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.QuestionDocument.Question
    {
        private static final long serialVersionUID = 1L;
        
        public QuestionImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName QUESTIONID$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "QuestionId");
        private static final javax.xml.namespace.QName QUESTIONTEXT$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "QuestionText");
        private static final javax.xml.namespace.QName CHOICES$4 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "Choices");
        
        
        /**
         * Gets the "QuestionId" element
         */
        public java.math.BigInteger getQuestionId()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUESTIONID$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getBigIntegerValue();
            }
        }
        
        /**
         * Gets (as xml) the "QuestionId" element
         */
        public org.apache.xmlbeans.XmlInteger xgetQuestionId()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlInteger target = null;
                target = (org.apache.xmlbeans.XmlInteger)get_store().find_element_user(QUESTIONID$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "QuestionId" element
         */
        public void setQuestionId(java.math.BigInteger questionId)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUESTIONID$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(QUESTIONID$0);
                }
                target.setBigIntegerValue(questionId);
            }
        }
        
        /**
         * Sets (as xml) the "QuestionId" element
         */
        public void xsetQuestionId(org.apache.xmlbeans.XmlInteger questionId)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlInteger target = null;
                target = (org.apache.xmlbeans.XmlInteger)get_store().find_element_user(QUESTIONID$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlInteger)get_store().add_element_user(QUESTIONID$0);
                }
                target.set(questionId);
            }
        }
        
        /**
         * Gets the "QuestionText" element
         */
        public java.lang.String getQuestionText()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUESTIONTEXT$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "QuestionText" element
         */
        public org.apache.xmlbeans.XmlString xgetQuestionText()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(QUESTIONTEXT$2, 0);
                return target;
            }
        }
        
        /**
         * Sets the "QuestionText" element
         */
        public void setQuestionText(java.lang.String questionText)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUESTIONTEXT$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(QUESTIONTEXT$2);
                }
                target.setStringValue(questionText);
            }
        }
        
        /**
         * Sets (as xml) the "QuestionText" element
         */
        public void xsetQuestionText(org.apache.xmlbeans.XmlString questionText)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(QUESTIONTEXT$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(QUESTIONTEXT$2);
                }
                target.set(questionText);
            }
        }
        
        /**
         * Gets the "Choices" element
         */
        public com.idanalytics.products.certainid.ChoicesDocument.Choices getChoices()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.certainid.ChoicesDocument.Choices target = null;
                target = (com.idanalytics.products.certainid.ChoicesDocument.Choices)get_store().find_element_user(CHOICES$4, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * Sets the "Choices" element
         */
        public void setChoices(com.idanalytics.products.certainid.ChoicesDocument.Choices choices)
        {
            generatedSetterHelperImpl(choices, CHOICES$4, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "Choices" element
         */
        public com.idanalytics.products.certainid.ChoicesDocument.Choices addNewChoices()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.certainid.ChoicesDocument.Choices target = null;
                target = (com.idanalytics.products.certainid.ChoicesDocument.Choices)get_store().add_element_user(CHOICES$4);
                return target;
            }
        }
    }
}
