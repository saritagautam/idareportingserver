/*
 * An XML document type.
 * Localname: ScoreQuizRequest
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.ScoreQuizRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one ScoreQuizRequest(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class ScoreQuizRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.ScoreQuizRequestDocument
{
    private static final long serialVersionUID = 1L;
    
    public ScoreQuizRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SCOREQUIZREQUEST$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "ScoreQuizRequest");
    
    
    /**
     * Gets the "ScoreQuizRequest" element
     */
    public com.idanalytics.products.certainid.ScoreQuizRequestDocument.ScoreQuizRequest getScoreQuizRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.ScoreQuizRequestDocument.ScoreQuizRequest target = null;
            target = (com.idanalytics.products.certainid.ScoreQuizRequestDocument.ScoreQuizRequest)get_store().find_element_user(SCOREQUIZREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "ScoreQuizRequest" element
     */
    public void setScoreQuizRequest(com.idanalytics.products.certainid.ScoreQuizRequestDocument.ScoreQuizRequest scoreQuizRequest)
    {
        generatedSetterHelperImpl(scoreQuizRequest, SCOREQUIZREQUEST$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "ScoreQuizRequest" element
     */
    public com.idanalytics.products.certainid.ScoreQuizRequestDocument.ScoreQuizRequest addNewScoreQuizRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.ScoreQuizRequestDocument.ScoreQuizRequest target = null;
            target = (com.idanalytics.products.certainid.ScoreQuizRequestDocument.ScoreQuizRequest)get_store().add_element_user(SCOREQUIZREQUEST$0);
            return target;
        }
    }
    /**
     * An XML ScoreQuizRequest(@http://idanalytics.com/products/certainid).
     *
     * This is a complex type.
     */
    public static class ScoreQuizRequestImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.ScoreQuizRequestDocument.ScoreQuizRequest
    {
        private static final long serialVersionUID = 1L;
        
        public ScoreQuizRequestImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName APPID$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "AppID");
        private static final javax.xml.namespace.QName QUIZID$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "QuizId");
        private static final javax.xml.namespace.QName ANSWERS$4 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "Answers");
        
        
        /**
         * Gets the "AppID" element
         */
        public java.lang.String getAppID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(APPID$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "AppID" element
         */
        public com.idanalytics.products.certainid.AppIDDocument.AppID xgetAppID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.certainid.AppIDDocument.AppID target = null;
                target = (com.idanalytics.products.certainid.AppIDDocument.AppID)get_store().find_element_user(APPID$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "AppID" element
         */
        public void setAppID(java.lang.String appID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(APPID$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(APPID$0);
                }
                target.setStringValue(appID);
            }
        }
        
        /**
         * Sets (as xml) the "AppID" element
         */
        public void xsetAppID(com.idanalytics.products.certainid.AppIDDocument.AppID appID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.certainid.AppIDDocument.AppID target = null;
                target = (com.idanalytics.products.certainid.AppIDDocument.AppID)get_store().find_element_user(APPID$0, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.certainid.AppIDDocument.AppID)get_store().add_element_user(APPID$0);
                }
                target.set(appID);
            }
        }
        
        /**
         * Gets the "QuizId" element
         */
        public java.lang.String getQuizId()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUIZID$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "QuizId" element
         */
        public com.idanalytics.products.certainid.QuizIdDocument.QuizId xgetQuizId()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.certainid.QuizIdDocument.QuizId target = null;
                target = (com.idanalytics.products.certainid.QuizIdDocument.QuizId)get_store().find_element_user(QUIZID$2, 0);
                return target;
            }
        }
        
        /**
         * True if has "QuizId" element
         */
        public boolean isSetQuizId()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(QUIZID$2) != 0;
            }
        }
        
        /**
         * Sets the "QuizId" element
         */
        public void setQuizId(java.lang.String quizId)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUIZID$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(QUIZID$2);
                }
                target.setStringValue(quizId);
            }
        }
        
        /**
         * Sets (as xml) the "QuizId" element
         */
        public void xsetQuizId(com.idanalytics.products.certainid.QuizIdDocument.QuizId quizId)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.certainid.QuizIdDocument.QuizId target = null;
                target = (com.idanalytics.products.certainid.QuizIdDocument.QuizId)get_store().find_element_user(QUIZID$2, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.certainid.QuizIdDocument.QuizId)get_store().add_element_user(QUIZID$2);
                }
                target.set(quizId);
            }
        }
        
        /**
         * Unsets the "QuizId" element
         */
        public void unsetQuizId()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(QUIZID$2, 0);
            }
        }
        
        /**
         * Gets the "Answers" element
         */
        public com.idanalytics.products.certainid.AnswersDocument.Answers getAnswers()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.certainid.AnswersDocument.Answers target = null;
                target = (com.idanalytics.products.certainid.AnswersDocument.Answers)get_store().find_element_user(ANSWERS$4, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * Sets the "Answers" element
         */
        public void setAnswers(com.idanalytics.products.certainid.AnswersDocument.Answers answers)
        {
            generatedSetterHelperImpl(answers, ANSWERS$4, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "Answers" element
         */
        public com.idanalytics.products.certainid.AnswersDocument.Answers addNewAnswers()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.certainid.AnswersDocument.Answers target = null;
                target = (com.idanalytics.products.certainid.AnswersDocument.Answers)get_store().add_element_user(ANSWERS$4);
                return target;
            }
        }
    }
}
