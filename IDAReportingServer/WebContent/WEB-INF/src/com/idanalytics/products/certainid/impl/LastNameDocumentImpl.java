/*
 * An XML document type.
 * Localname: LastName
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.LastNameDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one LastName(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class LastNameDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.LastNameDocument
{
    private static final long serialVersionUID = 1L;
    
    public LastNameDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName LASTNAME$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "LastName");
    
    
    /**
     * Gets the "LastName" element
     */
    public java.lang.String getLastName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LASTNAME$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "LastName" element
     */
    public com.idanalytics.products.certainid.LastNameDocument.LastName xgetLastName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.LastNameDocument.LastName target = null;
            target = (com.idanalytics.products.certainid.LastNameDocument.LastName)get_store().find_element_user(LASTNAME$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "LastName" element
     */
    public void setLastName(java.lang.String lastName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LASTNAME$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(LASTNAME$0);
            }
            target.setStringValue(lastName);
        }
    }
    
    /**
     * Sets (as xml) the "LastName" element
     */
    public void xsetLastName(com.idanalytics.products.certainid.LastNameDocument.LastName lastName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.LastNameDocument.LastName target = null;
            target = (com.idanalytics.products.certainid.LastNameDocument.LastName)get_store().find_element_user(LASTNAME$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.LastNameDocument.LastName)get_store().add_element_user(LASTNAME$0);
            }
            target.set(lastName);
        }
    }
    /**
     * An XML LastName(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.LastNameDocument$LastName.
     */
    public static class LastNameImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.certainid.LastNameDocument.LastName
    {
        private static final long serialVersionUID = 1L;
        
        public LastNameImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected LastNameImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
