/*
 * An XML document type.
 * Localname: WorkPhone
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.WorkPhoneDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one WorkPhone(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class WorkPhoneDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.WorkPhoneDocument
{
    private static final long serialVersionUID = 1L;
    
    public WorkPhoneDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName WORKPHONE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "WorkPhone");
    
    
    /**
     * Gets the "WorkPhone" element
     */
    public java.lang.String getWorkPhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(WORKPHONE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "WorkPhone" element
     */
    public com.idanalytics.products.certainid.WorkPhoneDocument.WorkPhone xgetWorkPhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.WorkPhoneDocument.WorkPhone target = null;
            target = (com.idanalytics.products.certainid.WorkPhoneDocument.WorkPhone)get_store().find_element_user(WORKPHONE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "WorkPhone" element
     */
    public void setWorkPhone(java.lang.String workPhone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(WORKPHONE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(WORKPHONE$0);
            }
            target.setStringValue(workPhone);
        }
    }
    
    /**
     * Sets (as xml) the "WorkPhone" element
     */
    public void xsetWorkPhone(com.idanalytics.products.certainid.WorkPhoneDocument.WorkPhone workPhone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.WorkPhoneDocument.WorkPhone target = null;
            target = (com.idanalytics.products.certainid.WorkPhoneDocument.WorkPhone)get_store().find_element_user(WORKPHONE$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.WorkPhoneDocument.WorkPhone)get_store().add_element_user(WORKPHONE$0);
            }
            target.set(workPhone);
        }
    }
    /**
     * An XML WorkPhone(@http://idanalytics.com/products/certainid).
     *
     * This is a union type. Instances are of one of the following types:
     *     com.idanalytics.products.certainid.WorkPhoneDocument$WorkPhone$Member
     *     com.idanalytics.products.certainid.WorkPhoneDocument$WorkPhone$Member2
     */
    public static class WorkPhoneImpl extends org.apache.xmlbeans.impl.values.XmlUnionImpl implements com.idanalytics.products.certainid.WorkPhoneDocument.WorkPhone, com.idanalytics.products.certainid.WorkPhoneDocument.WorkPhone.Member, com.idanalytics.products.certainid.WorkPhoneDocument.WorkPhone.Member2
    {
        private static final long serialVersionUID = 1L;
        
        public WorkPhoneImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected WorkPhoneImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
        /**
         * An anonymous inner XML type.
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.certainid.WorkPhoneDocument$WorkPhone$Member.
         */
        public static class MemberImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.certainid.WorkPhoneDocument.WorkPhone.Member
        {
            private static final long serialVersionUID = 1L;
            
            public MemberImpl(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected MemberImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
        /**
         * An anonymous inner XML type.
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.certainid.WorkPhoneDocument$WorkPhone$Member2.
         */
        public static class MemberImpl2 extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.certainid.WorkPhoneDocument.WorkPhone.Member2
        {
            private static final long serialVersionUID = 1L;
            
            public MemberImpl2(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected MemberImpl2(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
    }
}
