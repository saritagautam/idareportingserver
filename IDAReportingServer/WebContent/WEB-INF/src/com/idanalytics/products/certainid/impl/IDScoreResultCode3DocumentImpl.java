/*
 * An XML document type.
 * Localname: IDScoreResultCode3
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.IDScoreResultCode3Document
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one IDScoreResultCode3(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class IDScoreResultCode3DocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.IDScoreResultCode3Document
{
    private static final long serialVersionUID = 1L;
    
    public IDScoreResultCode3DocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IDSCORERESULTCODE3$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "IDScoreResultCode3");
    
    
    /**
     * Gets the "IDScoreResultCode3" element
     */
    public java.lang.String getIDScoreResultCode3()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDSCORERESULTCODE3$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IDScoreResultCode3" element
     */
    public com.idanalytics.products.certainid.IDScoreResultCode3Document.IDScoreResultCode3 xgetIDScoreResultCode3()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.IDScoreResultCode3Document.IDScoreResultCode3 target = null;
            target = (com.idanalytics.products.certainid.IDScoreResultCode3Document.IDScoreResultCode3)get_store().find_element_user(IDSCORERESULTCODE3$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IDScoreResultCode3" element
     */
    public void setIDScoreResultCode3(java.lang.String idScoreResultCode3)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDSCORERESULTCODE3$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDSCORERESULTCODE3$0);
            }
            target.setStringValue(idScoreResultCode3);
        }
    }
    
    /**
     * Sets (as xml) the "IDScoreResultCode3" element
     */
    public void xsetIDScoreResultCode3(com.idanalytics.products.certainid.IDScoreResultCode3Document.IDScoreResultCode3 idScoreResultCode3)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.IDScoreResultCode3Document.IDScoreResultCode3 target = null;
            target = (com.idanalytics.products.certainid.IDScoreResultCode3Document.IDScoreResultCode3)get_store().find_element_user(IDSCORERESULTCODE3$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.IDScoreResultCode3Document.IDScoreResultCode3)get_store().add_element_user(IDSCORERESULTCODE3$0);
            }
            target.set(idScoreResultCode3);
        }
    }
    /**
     * An XML IDScoreResultCode3(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.IDScoreResultCode3Document$IDScoreResultCode3.
     */
    public static class IDScoreResultCode3Impl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.certainid.IDScoreResultCode3Document.IDScoreResultCode3
    {
        private static final long serialVersionUID = 1L;
        
        public IDScoreResultCode3Impl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected IDScoreResultCode3Impl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
