/*
 * An XML document type.
 * Localname: PassThru1
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.PassThru1Document
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one PassThru1(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class PassThru1DocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.PassThru1Document
{
    private static final long serialVersionUID = 1L;
    
    public PassThru1DocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName PASSTHRU1$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "PassThru1");
    
    
    /**
     * Gets the "PassThru1" element
     */
    public java.lang.String getPassThru1()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU1$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PassThru1" element
     */
    public com.idanalytics.products.certainid.PassThru1Document.PassThru1 xgetPassThru1()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.PassThru1Document.PassThru1 target = null;
            target = (com.idanalytics.products.certainid.PassThru1Document.PassThru1)get_store().find_element_user(PASSTHRU1$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "PassThru1" element
     */
    public void setPassThru1(java.lang.String passThru1)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU1$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU1$0);
            }
            target.setStringValue(passThru1);
        }
    }
    
    /**
     * Sets (as xml) the "PassThru1" element
     */
    public void xsetPassThru1(com.idanalytics.products.certainid.PassThru1Document.PassThru1 passThru1)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.PassThru1Document.PassThru1 target = null;
            target = (com.idanalytics.products.certainid.PassThru1Document.PassThru1)get_store().find_element_user(PASSTHRU1$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.PassThru1Document.PassThru1)get_store().add_element_user(PASSTHRU1$0);
            }
            target.set(passThru1);
        }
    }
    /**
     * An XML PassThru1(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.PassThru1Document$PassThru1.
     */
    public static class PassThru1Impl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.certainid.PassThru1Document.PassThru1
    {
        private static final long serialVersionUID = 1L;
        
        public PassThru1Impl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected PassThru1Impl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
