/*
 * An XML document type.
 * Localname: Questions
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.QuestionsDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one Questions(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class QuestionsDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.QuestionsDocument
{
    private static final long serialVersionUID = 1L;
    
    public QuestionsDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName QUESTIONS$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "Questions");
    
    
    /**
     * Gets the "Questions" element
     */
    public com.idanalytics.products.certainid.QuestionsDocument.Questions getQuestions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.QuestionsDocument.Questions target = null;
            target = (com.idanalytics.products.certainid.QuestionsDocument.Questions)get_store().find_element_user(QUESTIONS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "Questions" element
     */
    public void setQuestions(com.idanalytics.products.certainid.QuestionsDocument.Questions questions)
    {
        generatedSetterHelperImpl(questions, QUESTIONS$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "Questions" element
     */
    public com.idanalytics.products.certainid.QuestionsDocument.Questions addNewQuestions()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.QuestionsDocument.Questions target = null;
            target = (com.idanalytics.products.certainid.QuestionsDocument.Questions)get_store().add_element_user(QUESTIONS$0);
            return target;
        }
    }
    /**
     * An XML Questions(@http://idanalytics.com/products/certainid).
     *
     * This is a complex type.
     */
    public static class QuestionsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.QuestionsDocument.Questions
    {
        private static final long serialVersionUID = 1L;
        
        public QuestionsImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName QUESTION$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "Question");
        
        
        /**
         * Gets array of all "Question" elements
         */
        public com.idanalytics.products.certainid.QuestionDocument.Question[] getQuestionArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(QUESTION$0, targetList);
                com.idanalytics.products.certainid.QuestionDocument.Question[] result = new com.idanalytics.products.certainid.QuestionDocument.Question[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "Question" element
         */
        public com.idanalytics.products.certainid.QuestionDocument.Question getQuestionArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.certainid.QuestionDocument.Question target = null;
                target = (com.idanalytics.products.certainid.QuestionDocument.Question)get_store().find_element_user(QUESTION$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "Question" element
         */
        public int sizeOfQuestionArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(QUESTION$0);
            }
        }
        
        /**
         * Sets array of all "Question" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setQuestionArray(com.idanalytics.products.certainid.QuestionDocument.Question[] questionArray)
        {
            check_orphaned();
            arraySetterHelper(questionArray, QUESTION$0);
        }
        
        /**
         * Sets ith "Question" element
         */
        public void setQuestionArray(int i, com.idanalytics.products.certainid.QuestionDocument.Question question)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.certainid.QuestionDocument.Question target = null;
                target = (com.idanalytics.products.certainid.QuestionDocument.Question)get_store().find_element_user(QUESTION$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(question);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "Question" element
         */
        public com.idanalytics.products.certainid.QuestionDocument.Question insertNewQuestion(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.certainid.QuestionDocument.Question target = null;
                target = (com.idanalytics.products.certainid.QuestionDocument.Question)get_store().insert_element_user(QUESTION$0, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "Question" element
         */
        public com.idanalytics.products.certainid.QuestionDocument.Question addNewQuestion()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.certainid.QuestionDocument.Question target = null;
                target = (com.idanalytics.products.certainid.QuestionDocument.Question)get_store().add_element_user(QUESTION$0);
                return target;
            }
        }
        
        /**
         * Removes the ith "Question" element
         */
        public void removeQuestion(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(QUESTION$0, i);
            }
        }
    }
}
