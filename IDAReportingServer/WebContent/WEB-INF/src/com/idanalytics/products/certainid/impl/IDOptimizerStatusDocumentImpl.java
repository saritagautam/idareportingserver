/*
 * An XML document type.
 * Localname: IDOptimizerStatus
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.IDOptimizerStatusDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one IDOptimizerStatus(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class IDOptimizerStatusDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.IDOptimizerStatusDocument
{
    private static final long serialVersionUID = 1L;
    
    public IDOptimizerStatusDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IDOPTIMIZERSTATUS$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "IDOptimizerStatus");
    
    
    /**
     * Gets the "IDOptimizerStatus" element
     */
    public com.idanalytics.products.certainid.IDOptimizerStatusDocument.IDOptimizerStatus.Enum getIDOptimizerStatus()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDOPTIMIZERSTATUS$0, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.certainid.IDOptimizerStatusDocument.IDOptimizerStatus.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "IDOptimizerStatus" element
     */
    public com.idanalytics.products.certainid.IDOptimizerStatusDocument.IDOptimizerStatus xgetIDOptimizerStatus()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.IDOptimizerStatusDocument.IDOptimizerStatus target = null;
            target = (com.idanalytics.products.certainid.IDOptimizerStatusDocument.IDOptimizerStatus)get_store().find_element_user(IDOPTIMIZERSTATUS$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IDOptimizerStatus" element
     */
    public void setIDOptimizerStatus(com.idanalytics.products.certainid.IDOptimizerStatusDocument.IDOptimizerStatus.Enum idOptimizerStatus)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDOPTIMIZERSTATUS$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDOPTIMIZERSTATUS$0);
            }
            target.setEnumValue(idOptimizerStatus);
        }
    }
    
    /**
     * Sets (as xml) the "IDOptimizerStatus" element
     */
    public void xsetIDOptimizerStatus(com.idanalytics.products.certainid.IDOptimizerStatusDocument.IDOptimizerStatus idOptimizerStatus)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.IDOptimizerStatusDocument.IDOptimizerStatus target = null;
            target = (com.idanalytics.products.certainid.IDOptimizerStatusDocument.IDOptimizerStatus)get_store().find_element_user(IDOPTIMIZERSTATUS$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.IDOptimizerStatusDocument.IDOptimizerStatus)get_store().add_element_user(IDOPTIMIZERSTATUS$0);
            }
            target.set(idOptimizerStatus);
        }
    }
    /**
     * An XML IDOptimizerStatus(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.IDOptimizerStatusDocument$IDOptimizerStatus.
     */
    public static class IDOptimizerStatusImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.idanalytics.products.certainid.IDOptimizerStatusDocument.IDOptimizerStatus
    {
        private static final long serialVersionUID = 1L;
        
        public IDOptimizerStatusImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected IDOptimizerStatusImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
