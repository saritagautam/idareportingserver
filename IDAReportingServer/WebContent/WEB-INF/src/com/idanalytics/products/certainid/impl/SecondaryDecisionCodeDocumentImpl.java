/*
 * An XML document type.
 * Localname: SecondaryDecisionCode
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.SecondaryDecisionCodeDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one SecondaryDecisionCode(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class SecondaryDecisionCodeDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.SecondaryDecisionCodeDocument
{
    private static final long serialVersionUID = 1L;
    
    public SecondaryDecisionCodeDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SECONDARYDECISIONCODE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "SecondaryDecisionCode");
    
    
    /**
     * Gets the "SecondaryDecisionCode" element
     */
    public java.lang.String getSecondaryDecisionCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SECONDARYDECISIONCODE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "SecondaryDecisionCode" element
     */
    public com.idanalytics.products.certainid.SecondaryDecisionCodeDocument.SecondaryDecisionCode xgetSecondaryDecisionCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.SecondaryDecisionCodeDocument.SecondaryDecisionCode target = null;
            target = (com.idanalytics.products.certainid.SecondaryDecisionCodeDocument.SecondaryDecisionCode)get_store().find_element_user(SECONDARYDECISIONCODE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "SecondaryDecisionCode" element
     */
    public void setSecondaryDecisionCode(java.lang.String secondaryDecisionCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SECONDARYDECISIONCODE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SECONDARYDECISIONCODE$0);
            }
            target.setStringValue(secondaryDecisionCode);
        }
    }
    
    /**
     * Sets (as xml) the "SecondaryDecisionCode" element
     */
    public void xsetSecondaryDecisionCode(com.idanalytics.products.certainid.SecondaryDecisionCodeDocument.SecondaryDecisionCode secondaryDecisionCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.SecondaryDecisionCodeDocument.SecondaryDecisionCode target = null;
            target = (com.idanalytics.products.certainid.SecondaryDecisionCodeDocument.SecondaryDecisionCode)get_store().find_element_user(SECONDARYDECISIONCODE$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.SecondaryDecisionCodeDocument.SecondaryDecisionCode)get_store().add_element_user(SECONDARYDECISIONCODE$0);
            }
            target.set(secondaryDecisionCode);
        }
    }
    /**
     * An XML SecondaryDecisionCode(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.SecondaryDecisionCodeDocument$SecondaryDecisionCode.
     */
    public static class SecondaryDecisionCodeImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.certainid.SecondaryDecisionCodeDocument.SecondaryDecisionCode
    {
        private static final long serialVersionUID = 1L;
        
        public SecondaryDecisionCodeImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected SecondaryDecisionCodeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
