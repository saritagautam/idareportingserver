/*
 * An XML document type.
 * Localname: Choice
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.ChoiceDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one Choice(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class ChoiceDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.ChoiceDocument
{
    private static final long serialVersionUID = 1L;
    
    public ChoiceDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CHOICE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "Choice");
    
    
    /**
     * Gets the "Choice" element
     */
    public java.lang.String getChoice()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CHOICE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Choice" element
     */
    public org.apache.xmlbeans.XmlString xgetChoice()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CHOICE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Choice" element
     */
    public void setChoice(java.lang.String choice)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CHOICE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CHOICE$0);
            }
            target.setStringValue(choice);
        }
    }
    
    /**
     * Sets (as xml) the "Choice" element
     */
    public void xsetChoice(org.apache.xmlbeans.XmlString choice)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CHOICE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(CHOICE$0);
            }
            target.set(choice);
        }
    }
}
