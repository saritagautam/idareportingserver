/*
 * An XML document type.
 * Localname: State
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.StateDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one State(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class StateDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.StateDocument
{
    private static final long serialVersionUID = 1L;
    
    public StateDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName STATE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "State");
    
    
    /**
     * Gets the "State" element
     */
    public java.lang.String getState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STATE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "State" element
     */
    public com.idanalytics.products.certainid.StateDocument.State xgetState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.StateDocument.State target = null;
            target = (com.idanalytics.products.certainid.StateDocument.State)get_store().find_element_user(STATE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "State" element
     */
    public void setState(java.lang.String state)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STATE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(STATE$0);
            }
            target.setStringValue(state);
        }
    }
    
    /**
     * Sets (as xml) the "State" element
     */
    public void xsetState(com.idanalytics.products.certainid.StateDocument.State state)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.StateDocument.State target = null;
            target = (com.idanalytics.products.certainid.StateDocument.State)get_store().find_element_user(STATE$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.StateDocument.State)get_store().add_element_user(STATE$0);
            }
            target.set(state);
        }
    }
    /**
     * An XML State(@http://idanalytics.com/products/certainid).
     *
     * This is a union type. Instances are of one of the following types:
     *     com.idanalytics.products.certainid.StateDocument$State$Member
     *     com.idanalytics.products.certainid.StateDocument$State$Member2
     */
    public static class StateImpl extends org.apache.xmlbeans.impl.values.XmlUnionImpl implements com.idanalytics.products.certainid.StateDocument.State, com.idanalytics.products.certainid.StateDocument.State.Member, com.idanalytics.products.certainid.StateDocument.State.Member2
    {
        private static final long serialVersionUID = 1L;
        
        public StateImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected StateImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
        /**
         * An anonymous inner XML type.
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.certainid.StateDocument$State$Member.
         */
        public static class MemberImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.certainid.StateDocument.State.Member
        {
            private static final long serialVersionUID = 1L;
            
            public MemberImpl(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected MemberImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
        /**
         * An anonymous inner XML type.
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.certainid.StateDocument$State$Member2.
         */
        public static class MemberImpl2 extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.certainid.StateDocument.State.Member2
        {
            private static final long serialVersionUID = 1L;
            
            public MemberImpl2(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected MemberImpl2(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
    }
}
