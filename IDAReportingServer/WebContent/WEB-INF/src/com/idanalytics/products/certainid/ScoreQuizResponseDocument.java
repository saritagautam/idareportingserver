/*
 * An XML document type.
 * Localname: ScoreQuizResponse
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.ScoreQuizResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid;


/**
 * A document containing one ScoreQuizResponse(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public interface ScoreQuizResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ScoreQuizResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("scorequizresponse9aa4doctype");
    
    /**
     * Gets the "ScoreQuizResponse" element
     */
    com.idanalytics.products.certainid.ScoreQuizResponseDocument.ScoreQuizResponse getScoreQuizResponse();
    
    /**
     * Sets the "ScoreQuizResponse" element
     */
    void setScoreQuizResponse(com.idanalytics.products.certainid.ScoreQuizResponseDocument.ScoreQuizResponse scoreQuizResponse);
    
    /**
     * Appends and returns a new empty "ScoreQuizResponse" element
     */
    com.idanalytics.products.certainid.ScoreQuizResponseDocument.ScoreQuizResponse addNewScoreQuizResponse();
    
    /**
     * An XML ScoreQuizResponse(@http://idanalytics.com/products/certainid).
     *
     * This is a complex type.
     */
    public interface ScoreQuizResponse extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ScoreQuizResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("scorequizresponse8f58elemtype");
        
        /**
         * Gets the "IDASequence" element
         */
        java.lang.String getIDASequence();
        
        /**
         * Gets (as xml) the "IDASequence" element
         */
        com.idanalytics.products.certainid.IDASequenceDocument.IDASequence xgetIDASequence();
        
        /**
         * True if has "IDASequence" element
         */
        boolean isSetIDASequence();
        
        /**
         * Sets the "IDASequence" element
         */
        void setIDASequence(java.lang.String idaSequence);
        
        /**
         * Sets (as xml) the "IDASequence" element
         */
        void xsetIDASequence(com.idanalytics.products.certainid.IDASequenceDocument.IDASequence idaSequence);
        
        /**
         * Unsets the "IDASequence" element
         */
        void unsetIDASequence();
        
        /**
         * Gets the "AppID" element
         */
        java.lang.String getAppID();
        
        /**
         * Gets (as xml) the "AppID" element
         */
        com.idanalytics.products.certainid.AppIDDocument.AppID xgetAppID();
        
        /**
         * True if has "AppID" element
         */
        boolean isSetAppID();
        
        /**
         * Sets the "AppID" element
         */
        void setAppID(java.lang.String appID);
        
        /**
         * Sets (as xml) the "AppID" element
         */
        void xsetAppID(com.idanalytics.products.certainid.AppIDDocument.AppID appID);
        
        /**
         * Unsets the "AppID" element
         */
        void unsetAppID();
        
        /**
         * Gets the "IDATimeStamp" element
         */
        java.util.Calendar getIDATimeStamp();
        
        /**
         * Gets (as xml) the "IDATimeStamp" element
         */
        com.idanalytics.products.certainid.IDATimeStampDocument.IDATimeStamp xgetIDATimeStamp();
        
        /**
         * Sets the "IDATimeStamp" element
         */
        void setIDATimeStamp(java.util.Calendar idaTimeStamp);
        
        /**
         * Sets (as xml) the "IDATimeStamp" element
         */
        void xsetIDATimeStamp(com.idanalytics.products.certainid.IDATimeStampDocument.IDATimeStamp idaTimeStamp);
        
        /**
         * Gets the "IDAStatus" element
         */
        int getIDAStatus();
        
        /**
         * Gets (as xml) the "IDAStatus" element
         */
        com.idanalytics.products.certainid.IDAStatusDocument.IDAStatus xgetIDAStatus();
        
        /**
         * Sets the "IDAStatus" element
         */
        void setIDAStatus(int idaStatus);
        
        /**
         * Sets (as xml) the "IDAStatus" element
         */
        void xsetIDAStatus(com.idanalytics.products.certainid.IDAStatusDocument.IDAStatus idaStatus);
        
        /**
         * Gets the "QuizId" element
         */
        java.lang.String getQuizId();
        
        /**
         * Gets (as xml) the "QuizId" element
         */
        com.idanalytics.products.certainid.QuizIdDocument.QuizId xgetQuizId();
        
        /**
         * True if has "QuizId" element
         */
        boolean isSetQuizId();
        
        /**
         * Sets the "QuizId" element
         */
        void setQuizId(java.lang.String quizId);
        
        /**
         * Sets (as xml) the "QuizId" element
         */
        void xsetQuizId(com.idanalytics.products.certainid.QuizIdDocument.QuizId quizId);
        
        /**
         * Unsets the "QuizId" element
         */
        void unsetQuizId();
        
        /**
         * Gets the "Questions" element
         */
        com.idanalytics.products.certainid.QuestionsDocument.Questions getQuestions();
        
        /**
         * True if has "Questions" element
         */
        boolean isSetQuestions();
        
        /**
         * Sets the "Questions" element
         */
        void setQuestions(com.idanalytics.products.certainid.QuestionsDocument.Questions questions);
        
        /**
         * Appends and returns a new empty "Questions" element
         */
        com.idanalytics.products.certainid.QuestionsDocument.Questions addNewQuestions();
        
        /**
         * Unsets the "Questions" element
         */
        void unsetQuestions();
        
        /**
         * Gets the "NumberWrong" element
         */
        java.math.BigInteger getNumberWrong();
        
        /**
         * Gets (as xml) the "NumberWrong" element
         */
        org.apache.xmlbeans.XmlInteger xgetNumberWrong();
        
        /**
         * True if has "NumberWrong" element
         */
        boolean isSetNumberWrong();
        
        /**
         * Sets the "NumberWrong" element
         */
        void setNumberWrong(java.math.BigInteger numberWrong);
        
        /**
         * Sets (as xml) the "NumberWrong" element
         */
        void xsetNumberWrong(org.apache.xmlbeans.XmlInteger numberWrong);
        
        /**
         * Unsets the "NumberWrong" element
         */
        void unsetNumberWrong();
        
        /**
         * Gets the "Result" element
         */
        com.idanalytics.products.certainid.ResultDocument.Result.Enum getResult();
        
        /**
         * Gets (as xml) the "Result" element
         */
        com.idanalytics.products.certainid.ResultDocument.Result xgetResult();
        
        /**
         * True if has "Result" element
         */
        boolean isSetResult();
        
        /**
         * Sets the "Result" element
         */
        void setResult(com.idanalytics.products.certainid.ResultDocument.Result.Enum result);
        
        /**
         * Sets (as xml) the "Result" element
         */
        void xsetResult(com.idanalytics.products.certainid.ResultDocument.Result result);
        
        /**
         * Unsets the "Result" element
         */
        void unsetResult();
        
        /**
         * Gets the "QuizState" element
         */
        com.idanalytics.products.certainid.QuizStateDocument.QuizState.Enum getQuizState();
        
        /**
         * Gets (as xml) the "QuizState" element
         */
        com.idanalytics.products.certainid.QuizStateDocument.QuizState xgetQuizState();
        
        /**
         * True if has "QuizState" element
         */
        boolean isSetQuizState();
        
        /**
         * Sets the "QuizState" element
         */
        void setQuizState(com.idanalytics.products.certainid.QuizStateDocument.QuizState.Enum quizState);
        
        /**
         * Sets (as xml) the "QuizState" element
         */
        void xsetQuizState(com.idanalytics.products.certainid.QuizStateDocument.QuizState quizState);
        
        /**
         * Unsets the "QuizState" element
         */
        void unsetQuizState();
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.certainid.ScoreQuizResponseDocument.ScoreQuizResponse newInstance() {
              return (com.idanalytics.products.certainid.ScoreQuizResponseDocument.ScoreQuizResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.certainid.ScoreQuizResponseDocument.ScoreQuizResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.certainid.ScoreQuizResponseDocument.ScoreQuizResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.certainid.ScoreQuizResponseDocument newInstance() {
          return (com.idanalytics.products.certainid.ScoreQuizResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.certainid.ScoreQuizResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.certainid.ScoreQuizResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.certainid.ScoreQuizResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.certainid.ScoreQuizResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.certainid.ScoreQuizResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.certainid.ScoreQuizResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.certainid.ScoreQuizResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.certainid.ScoreQuizResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.certainid.ScoreQuizResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.certainid.ScoreQuizResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.certainid.ScoreQuizResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.certainid.ScoreQuizResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.certainid.ScoreQuizResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.certainid.ScoreQuizResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.certainid.ScoreQuizResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.certainid.ScoreQuizResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.certainid.ScoreQuizResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.certainid.ScoreQuizResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.certainid.ScoreQuizResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.certainid.ScoreQuizResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.certainid.ScoreQuizResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.certainid.ScoreQuizResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.certainid.ScoreQuizResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.certainid.ScoreQuizResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.certainid.ScoreQuizResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.certainid.ScoreQuizResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.certainid.ScoreQuizResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.certainid.ScoreQuizResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.certainid.ScoreQuizResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.certainid.ScoreQuizResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.certainid.ScoreQuizResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.certainid.ScoreQuizResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.certainid.ScoreQuizResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.certainid.ScoreQuizResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
