/*
 * An XML document type.
 * Localname: SourceIP
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.SourceIPDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one SourceIP(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class SourceIPDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.SourceIPDocument
{
    private static final long serialVersionUID = 1L;
    
    public SourceIPDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SOURCEIP$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "SourceIP");
    
    
    /**
     * Gets the "SourceIP" element
     */
    public java.lang.String getSourceIP()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SOURCEIP$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "SourceIP" element
     */
    public com.idanalytics.products.certainid.SourceIPDocument.SourceIP xgetSourceIP()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.SourceIPDocument.SourceIP target = null;
            target = (com.idanalytics.products.certainid.SourceIPDocument.SourceIP)get_store().find_element_user(SOURCEIP$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "SourceIP" element
     */
    public void setSourceIP(java.lang.String sourceIP)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SOURCEIP$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SOURCEIP$0);
            }
            target.setStringValue(sourceIP);
        }
    }
    
    /**
     * Sets (as xml) the "SourceIP" element
     */
    public void xsetSourceIP(com.idanalytics.products.certainid.SourceIPDocument.SourceIP sourceIP)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.SourceIPDocument.SourceIP target = null;
            target = (com.idanalytics.products.certainid.SourceIPDocument.SourceIP)get_store().find_element_user(SOURCEIP$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.SourceIPDocument.SourceIP)get_store().add_element_user(SOURCEIP$0);
            }
            target.set(sourceIP);
        }
    }
    /**
     * An XML SourceIP(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.SourceIPDocument$SourceIP.
     */
    public static class SourceIPImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.certainid.SourceIPDocument.SourceIP
    {
        private static final long serialVersionUID = 1L;
        
        public SourceIPImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected SourceIPImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
