/*
 * An XML document type.
 * Localname: PrimaryPortfolio
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.PrimaryPortfolioDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one PrimaryPortfolio(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class PrimaryPortfolioDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.PrimaryPortfolioDocument
{
    private static final long serialVersionUID = 1L;
    
    public PrimaryPortfolioDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName PRIMARYPORTFOLIO$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "PrimaryPortfolio");
    
    
    /**
     * Gets the "PrimaryPortfolio" element
     */
    public java.lang.String getPrimaryPortfolio()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PRIMARYPORTFOLIO$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrimaryPortfolio" element
     */
    public com.idanalytics.products.certainid.PrimaryPortfolioDocument.PrimaryPortfolio xgetPrimaryPortfolio()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.PrimaryPortfolioDocument.PrimaryPortfolio target = null;
            target = (com.idanalytics.products.certainid.PrimaryPortfolioDocument.PrimaryPortfolio)get_store().find_element_user(PRIMARYPORTFOLIO$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "PrimaryPortfolio" element
     */
    public void setPrimaryPortfolio(java.lang.String primaryPortfolio)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PRIMARYPORTFOLIO$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PRIMARYPORTFOLIO$0);
            }
            target.setStringValue(primaryPortfolio);
        }
    }
    
    /**
     * Sets (as xml) the "PrimaryPortfolio" element
     */
    public void xsetPrimaryPortfolio(com.idanalytics.products.certainid.PrimaryPortfolioDocument.PrimaryPortfolio primaryPortfolio)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.PrimaryPortfolioDocument.PrimaryPortfolio target = null;
            target = (com.idanalytics.products.certainid.PrimaryPortfolioDocument.PrimaryPortfolio)get_store().find_element_user(PRIMARYPORTFOLIO$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.PrimaryPortfolioDocument.PrimaryPortfolio)get_store().add_element_user(PRIMARYPORTFOLIO$0);
            }
            target.set(primaryPortfolio);
        }
    }
    /**
     * An XML PrimaryPortfolio(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.PrimaryPortfolioDocument$PrimaryPortfolio.
     */
    public static class PrimaryPortfolioImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.certainid.PrimaryPortfolioDocument.PrimaryPortfolio
    {
        private static final long serialVersionUID = 1L;
        
        public PrimaryPortfolioImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected PrimaryPortfolioImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
