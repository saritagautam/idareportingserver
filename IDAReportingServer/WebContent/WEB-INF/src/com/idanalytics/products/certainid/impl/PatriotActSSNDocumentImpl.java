/*
 * An XML document type.
 * Localname: PatriotActSSN
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.PatriotActSSNDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one PatriotActSSN(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class PatriotActSSNDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.PatriotActSSNDocument
{
    private static final long serialVersionUID = 1L;
    
    public PatriotActSSNDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName PATRIOTACTSSN$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "PatriotActSSN");
    
    
    /**
     * Gets the "PatriotActSSN" element
     */
    public com.idanalytics.products.certainid.PatriotActSSNDocument.PatriotActSSN.Enum getPatriotActSSN()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PATRIOTACTSSN$0, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.certainid.PatriotActSSNDocument.PatriotActSSN.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "PatriotActSSN" element
     */
    public com.idanalytics.products.certainid.PatriotActSSNDocument.PatriotActSSN xgetPatriotActSSN()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.PatriotActSSNDocument.PatriotActSSN target = null;
            target = (com.idanalytics.products.certainid.PatriotActSSNDocument.PatriotActSSN)get_store().find_element_user(PATRIOTACTSSN$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "PatriotActSSN" element
     */
    public void setPatriotActSSN(com.idanalytics.products.certainid.PatriotActSSNDocument.PatriotActSSN.Enum patriotActSSN)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PATRIOTACTSSN$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PATRIOTACTSSN$0);
            }
            target.setEnumValue(patriotActSSN);
        }
    }
    
    /**
     * Sets (as xml) the "PatriotActSSN" element
     */
    public void xsetPatriotActSSN(com.idanalytics.products.certainid.PatriotActSSNDocument.PatriotActSSN patriotActSSN)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.PatriotActSSNDocument.PatriotActSSN target = null;
            target = (com.idanalytics.products.certainid.PatriotActSSNDocument.PatriotActSSN)get_store().find_element_user(PATRIOTACTSSN$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.PatriotActSSNDocument.PatriotActSSN)get_store().add_element_user(PATRIOTACTSSN$0);
            }
            target.set(patriotActSSN);
        }
    }
    /**
     * An XML PatriotActSSN(@http://idanalytics.com/products/certainid).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.certainid.PatriotActSSNDocument$PatriotActSSN.
     */
    public static class PatriotActSSNImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.idanalytics.products.certainid.PatriotActSSNDocument.PatriotActSSN
    {
        private static final long serialVersionUID = 1L;
        
        public PatriotActSSNImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected PatriotActSSNImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
