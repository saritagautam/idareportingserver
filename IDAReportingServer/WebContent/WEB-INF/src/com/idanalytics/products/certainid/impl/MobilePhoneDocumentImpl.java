/*
 * An XML document type.
 * Localname: MobilePhone
 * Namespace: http://idanalytics.com/products/certainid
 * Java type: com.idanalytics.products.certainid.MobilePhoneDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.certainid.impl;
/**
 * A document containing one MobilePhone(@http://idanalytics.com/products/certainid) element.
 *
 * This is a complex type.
 */
public class MobilePhoneDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.certainid.MobilePhoneDocument
{
    private static final long serialVersionUID = 1L;
    
    public MobilePhoneDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName MOBILEPHONE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/certainid", "MobilePhone");
    
    
    /**
     * Gets the "MobilePhone" element
     */
    public java.lang.String getMobilePhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MOBILEPHONE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "MobilePhone" element
     */
    public com.idanalytics.products.certainid.MobilePhoneDocument.MobilePhone xgetMobilePhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.MobilePhoneDocument.MobilePhone target = null;
            target = (com.idanalytics.products.certainid.MobilePhoneDocument.MobilePhone)get_store().find_element_user(MOBILEPHONE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "MobilePhone" element
     */
    public void setMobilePhone(java.lang.String mobilePhone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MOBILEPHONE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(MOBILEPHONE$0);
            }
            target.setStringValue(mobilePhone);
        }
    }
    
    /**
     * Sets (as xml) the "MobilePhone" element
     */
    public void xsetMobilePhone(com.idanalytics.products.certainid.MobilePhoneDocument.MobilePhone mobilePhone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.certainid.MobilePhoneDocument.MobilePhone target = null;
            target = (com.idanalytics.products.certainid.MobilePhoneDocument.MobilePhone)get_store().find_element_user(MOBILEPHONE$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.certainid.MobilePhoneDocument.MobilePhone)get_store().add_element_user(MOBILEPHONE$0);
            }
            target.set(mobilePhone);
        }
    }
    /**
     * An XML MobilePhone(@http://idanalytics.com/products/certainid).
     *
     * This is a union type. Instances are of one of the following types:
     *     com.idanalytics.products.certainid.MobilePhoneDocument$MobilePhone$Member
     *     com.idanalytics.products.certainid.MobilePhoneDocument$MobilePhone$Member2
     */
    public static class MobilePhoneImpl extends org.apache.xmlbeans.impl.values.XmlUnionImpl implements com.idanalytics.products.certainid.MobilePhoneDocument.MobilePhone, com.idanalytics.products.certainid.MobilePhoneDocument.MobilePhone.Member, com.idanalytics.products.certainid.MobilePhoneDocument.MobilePhone.Member2
    {
        private static final long serialVersionUID = 1L;
        
        public MobilePhoneImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected MobilePhoneImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
        /**
         * An anonymous inner XML type.
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.certainid.MobilePhoneDocument$MobilePhone$Member.
         */
        public static class MemberImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.certainid.MobilePhoneDocument.MobilePhone.Member
        {
            private static final long serialVersionUID = 1L;
            
            public MemberImpl(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected MemberImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
        /**
         * An anonymous inner XML type.
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.certainid.MobilePhoneDocument$MobilePhone$Member2.
         */
        public static class MemberImpl2 extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.certainid.MobilePhoneDocument.MobilePhone.Member2
        {
            private static final long serialVersionUID = 1L;
            
            public MemberImpl2(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected MemberImpl2(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
    }
}
