/*
 * An XML document type.
 * Localname: ConfirmedFraud
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.ConfirmedFraudDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * A document containing one ConfirmedFraud(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public class ConfirmedFraudDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.ConfirmedFraudDocument
{
    private static final long serialVersionUID = 1L;
    
    public ConfirmedFraudDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CONFIRMEDFRAUD$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ConfirmedFraud");
    
    
    /**
     * Gets the "ConfirmedFraud" element
     */
    public com.idanalytics.products.idscore.request.ConfirmedFraudDocument.ConfirmedFraud.Enum getConfirmedFraud()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CONFIRMEDFRAUD$0, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.idscore.request.ConfirmedFraudDocument.ConfirmedFraud.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "ConfirmedFraud" element
     */
    public com.idanalytics.products.idscore.request.ConfirmedFraudDocument.ConfirmedFraud xgetConfirmedFraud()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ConfirmedFraudDocument.ConfirmedFraud target = null;
            target = (com.idanalytics.products.idscore.request.ConfirmedFraudDocument.ConfirmedFraud)get_store().find_element_user(CONFIRMEDFRAUD$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "ConfirmedFraud" element
     */
    public void setConfirmedFraud(com.idanalytics.products.idscore.request.ConfirmedFraudDocument.ConfirmedFraud.Enum confirmedFraud)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CONFIRMEDFRAUD$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CONFIRMEDFRAUD$0);
            }
            target.setEnumValue(confirmedFraud);
        }
    }
    
    /**
     * Sets (as xml) the "ConfirmedFraud" element
     */
    public void xsetConfirmedFraud(com.idanalytics.products.idscore.request.ConfirmedFraudDocument.ConfirmedFraud confirmedFraud)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ConfirmedFraudDocument.ConfirmedFraud target = null;
            target = (com.idanalytics.products.idscore.request.ConfirmedFraudDocument.ConfirmedFraud)get_store().find_element_user(CONFIRMEDFRAUD$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.ConfirmedFraudDocument.ConfirmedFraud)get_store().add_element_user(CONFIRMEDFRAUD$0);
            }
            target.set(confirmedFraud);
        }
    }
    /**
     * An XML ConfirmedFraud(@http://idanalytics.com/products/idscore/request).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.ConfirmedFraudDocument$ConfirmedFraud.
     */
    public static class ConfirmedFraudImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.idanalytics.products.idscore.request.ConfirmedFraudDocument.ConfirmedFraud
    {
        private static final long serialVersionUID = 1L;
        
        public ConfirmedFraudImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected ConfirmedFraudImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
