/*
 * XML Type:  Origination
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.Origination
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML Origination(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public class OriginationImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.Origination
{
    private static final long serialVersionUID = 1L;
    
    public OriginationImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CPC$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "CPC");
    private static final javax.xml.namespace.QName REQUESTTYPE$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "RequestType");
    private static final javax.xml.namespace.QName APPLICATIONDATE$4 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ApplicationDate");
    private static final javax.xml.namespace.QName APPID$6 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "AppID");
    private static final javax.xml.namespace.QName DESIGNATION$8 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Designation");
    private static final javax.xml.namespace.QName ACCTLINKKEY$10 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "AcctLinkKey");
    private static final javax.xml.namespace.QName EVENTTYPE$12 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "EventType");
    private static final javax.xml.namespace.QName EVENTTYPES$14 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "EventTypes");
    private static final javax.xml.namespace.QName INDUSTRYTYPE$16 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "IndustryType");
    private static final javax.xml.namespace.QName CHANGEREQUESTSOURCE$18 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ChangeRequestSource");
    private static final javax.xml.namespace.QName PRESCREENDATE$20 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrescreenDate");
    private static final javax.xml.namespace.QName PROGRAMDATE$22 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ProgramDate");
    private static final javax.xml.namespace.QName PROMOCODE$24 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PromoCode");
    private static final javax.xml.namespace.QName PASSTHRU1$26 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PassThru1");
    private static final javax.xml.namespace.QName PASSTHRU2$28 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PassThru2");
    private static final javax.xml.namespace.QName PASSTHRU3$30 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PassThru3");
    private static final javax.xml.namespace.QName PASSTHRU4$32 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PassThru4");
    
    
    /**
     * Gets the "CPC" element
     */
    public java.lang.String getCPC()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CPC$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "CPC" element
     */
    public com.idanalytics.products.idscore.request.CPCDocument.CPC xgetCPC()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.CPCDocument.CPC target = null;
            target = (com.idanalytics.products.idscore.request.CPCDocument.CPC)get_store().find_element_user(CPC$0, 0);
            return target;
        }
    }
    
    /**
     * True if has "CPC" element
     */
    public boolean isSetCPC()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CPC$0) != 0;
        }
    }
    
    /**
     * Sets the "CPC" element
     */
    public void setCPC(java.lang.String cpc)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CPC$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CPC$0);
            }
            target.setStringValue(cpc);
        }
    }
    
    /**
     * Sets (as xml) the "CPC" element
     */
    public void xsetCPC(com.idanalytics.products.idscore.request.CPCDocument.CPC cpc)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.CPCDocument.CPC target = null;
            target = (com.idanalytics.products.idscore.request.CPCDocument.CPC)get_store().find_element_user(CPC$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.CPCDocument.CPC)get_store().add_element_user(CPC$0);
            }
            target.set(cpc);
        }
    }
    
    /**
     * Unsets the "CPC" element
     */
    public void unsetCPC()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CPC$0, 0);
        }
    }
    
    /**
     * Gets the "RequestType" element
     */
    public java.lang.Object getRequestType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(REQUESTTYPE$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getObjectValue();
        }
    }
    
    /**
     * Gets (as xml) the "RequestType" element
     */
    public com.idanalytics.products.idscore.request.RequestTypeDocument.RequestType xgetRequestType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.RequestTypeDocument.RequestType target = null;
            target = (com.idanalytics.products.idscore.request.RequestTypeDocument.RequestType)get_store().find_element_user(REQUESTTYPE$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "RequestType" element
     */
    public void setRequestType(java.lang.Object requestType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(REQUESTTYPE$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(REQUESTTYPE$2);
            }
            target.setObjectValue(requestType);
        }
    }
    
    /**
     * Sets (as xml) the "RequestType" element
     */
    public void xsetRequestType(com.idanalytics.products.idscore.request.RequestTypeDocument.RequestType requestType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.RequestTypeDocument.RequestType target = null;
            target = (com.idanalytics.products.idscore.request.RequestTypeDocument.RequestType)get_store().find_element_user(REQUESTTYPE$2, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.RequestTypeDocument.RequestType)get_store().add_element_user(REQUESTTYPE$2);
            }
            target.set(requestType);
        }
    }
    
    /**
     * Gets the "ApplicationDate" element
     */
    public java.util.Calendar getApplicationDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(APPLICATIONDATE$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getCalendarValue();
        }
    }
    
    /**
     * Gets (as xml) the "ApplicationDate" element
     */
    public com.idanalytics.products.idscore.request.ApplicationDateDocument.ApplicationDate xgetApplicationDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ApplicationDateDocument.ApplicationDate target = null;
            target = (com.idanalytics.products.idscore.request.ApplicationDateDocument.ApplicationDate)get_store().find_element_user(APPLICATIONDATE$4, 0);
            return target;
        }
    }
    
    /**
     * Sets the "ApplicationDate" element
     */
    public void setApplicationDate(java.util.Calendar applicationDate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(APPLICATIONDATE$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(APPLICATIONDATE$4);
            }
            target.setCalendarValue(applicationDate);
        }
    }
    
    /**
     * Sets (as xml) the "ApplicationDate" element
     */
    public void xsetApplicationDate(com.idanalytics.products.idscore.request.ApplicationDateDocument.ApplicationDate applicationDate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ApplicationDateDocument.ApplicationDate target = null;
            target = (com.idanalytics.products.idscore.request.ApplicationDateDocument.ApplicationDate)get_store().find_element_user(APPLICATIONDATE$4, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.ApplicationDateDocument.ApplicationDate)get_store().add_element_user(APPLICATIONDATE$4);
            }
            target.set(applicationDate);
        }
    }
    
    /**
     * Gets the "AppID" element
     */
    public java.lang.String getAppID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(APPID$6, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "AppID" element
     */
    public com.idanalytics.products.common_v1.AppID xgetAppID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.AppID target = null;
            target = (com.idanalytics.products.common_v1.AppID)get_store().find_element_user(APPID$6, 0);
            return target;
        }
    }
    
    /**
     * Sets the "AppID" element
     */
    public void setAppID(java.lang.String appID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(APPID$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(APPID$6);
            }
            target.setStringValue(appID);
        }
    }
    
    /**
     * Sets (as xml) the "AppID" element
     */
    public void xsetAppID(com.idanalytics.products.common_v1.AppID appID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.AppID target = null;
            target = (com.idanalytics.products.common_v1.AppID)get_store().find_element_user(APPID$6, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.AppID)get_store().add_element_user(APPID$6);
            }
            target.set(appID);
        }
    }
    
    /**
     * Gets the "Designation" element
     */
    public com.idanalytics.products.idscore.request.DesignationDocument.Designation.Enum getDesignation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESIGNATION$8, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.idscore.request.DesignationDocument.Designation.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "Designation" element
     */
    public com.idanalytics.products.idscore.request.DesignationDocument.Designation xgetDesignation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.DesignationDocument.Designation target = null;
            target = (com.idanalytics.products.idscore.request.DesignationDocument.Designation)get_store().find_element_user(DESIGNATION$8, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Designation" element
     */
    public void setDesignation(com.idanalytics.products.idscore.request.DesignationDocument.Designation.Enum designation)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESIGNATION$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DESIGNATION$8);
            }
            target.setEnumValue(designation);
        }
    }
    
    /**
     * Sets (as xml) the "Designation" element
     */
    public void xsetDesignation(com.idanalytics.products.idscore.request.DesignationDocument.Designation designation)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.DesignationDocument.Designation target = null;
            target = (com.idanalytics.products.idscore.request.DesignationDocument.Designation)get_store().find_element_user(DESIGNATION$8, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.DesignationDocument.Designation)get_store().add_element_user(DESIGNATION$8);
            }
            target.set(designation);
        }
    }
    
    /**
     * Gets the "AcctLinkKey" element
     */
    public java.lang.String getAcctLinkKey()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ACCTLINKKEY$10, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "AcctLinkKey" element
     */
    public com.idanalytics.products.idscore.request.AcctLinkKeyDocument.AcctLinkKey xgetAcctLinkKey()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.AcctLinkKeyDocument.AcctLinkKey target = null;
            target = (com.idanalytics.products.idscore.request.AcctLinkKeyDocument.AcctLinkKey)get_store().find_element_user(ACCTLINKKEY$10, 0);
            return target;
        }
    }
    
    /**
     * True if has "AcctLinkKey" element
     */
    public boolean isSetAcctLinkKey()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ACCTLINKKEY$10) != 0;
        }
    }
    
    /**
     * Sets the "AcctLinkKey" element
     */
    public void setAcctLinkKey(java.lang.String acctLinkKey)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ACCTLINKKEY$10, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ACCTLINKKEY$10);
            }
            target.setStringValue(acctLinkKey);
        }
    }
    
    /**
     * Sets (as xml) the "AcctLinkKey" element
     */
    public void xsetAcctLinkKey(com.idanalytics.products.idscore.request.AcctLinkKeyDocument.AcctLinkKey acctLinkKey)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.AcctLinkKeyDocument.AcctLinkKey target = null;
            target = (com.idanalytics.products.idscore.request.AcctLinkKeyDocument.AcctLinkKey)get_store().find_element_user(ACCTLINKKEY$10, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.AcctLinkKeyDocument.AcctLinkKey)get_store().add_element_user(ACCTLINKKEY$10);
            }
            target.set(acctLinkKey);
        }
    }
    
    /**
     * Unsets the "AcctLinkKey" element
     */
    public void unsetAcctLinkKey()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ACCTLINKKEY$10, 0);
        }
    }
    
    /**
     * Gets the "EventType" element
     */
    public java.lang.String getEventType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EVENTTYPE$12, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "EventType" element
     */
    public com.idanalytics.products.idscore.request.EventTypeDocument.EventType xgetEventType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.EventTypeDocument.EventType target = null;
            target = (com.idanalytics.products.idscore.request.EventTypeDocument.EventType)get_store().find_element_user(EVENTTYPE$12, 0);
            return target;
        }
    }
    
    /**
     * True if has "EventType" element
     */
    public boolean isSetEventType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(EVENTTYPE$12) != 0;
        }
    }
    
    /**
     * Sets the "EventType" element
     */
    public void setEventType(java.lang.String eventType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EVENTTYPE$12, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(EVENTTYPE$12);
            }
            target.setStringValue(eventType);
        }
    }
    
    /**
     * Sets (as xml) the "EventType" element
     */
    public void xsetEventType(com.idanalytics.products.idscore.request.EventTypeDocument.EventType eventType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.EventTypeDocument.EventType target = null;
            target = (com.idanalytics.products.idscore.request.EventTypeDocument.EventType)get_store().find_element_user(EVENTTYPE$12, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.EventTypeDocument.EventType)get_store().add_element_user(EVENTTYPE$12);
            }
            target.set(eventType);
        }
    }
    
    /**
     * Unsets the "EventType" element
     */
    public void unsetEventType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(EVENTTYPE$12, 0);
        }
    }
    
    /**
     * Gets the "EventTypes" element
     */
    public com.idanalytics.products.idscore.request.EventTypesDocument.EventTypes getEventTypes()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.EventTypesDocument.EventTypes target = null;
            target = (com.idanalytics.products.idscore.request.EventTypesDocument.EventTypes)get_store().find_element_user(EVENTTYPES$14, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "EventTypes" element
     */
    public boolean isSetEventTypes()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(EVENTTYPES$14) != 0;
        }
    }
    
    /**
     * Sets the "EventTypes" element
     */
    public void setEventTypes(com.idanalytics.products.idscore.request.EventTypesDocument.EventTypes eventTypes)
    {
        generatedSetterHelperImpl(eventTypes, EVENTTYPES$14, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "EventTypes" element
     */
    public com.idanalytics.products.idscore.request.EventTypesDocument.EventTypes addNewEventTypes()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.EventTypesDocument.EventTypes target = null;
            target = (com.idanalytics.products.idscore.request.EventTypesDocument.EventTypes)get_store().add_element_user(EVENTTYPES$14);
            return target;
        }
    }
    
    /**
     * Unsets the "EventTypes" element
     */
    public void unsetEventTypes()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(EVENTTYPES$14, 0);
        }
    }
    
    /**
     * Gets the "IndustryType" element
     */
    public java.lang.String getIndustryType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(INDUSTRYTYPE$16, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IndustryType" element
     */
    public com.idanalytics.products.idscore.request.IndustryTypeDocument.IndustryType xgetIndustryType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.IndustryTypeDocument.IndustryType target = null;
            target = (com.idanalytics.products.idscore.request.IndustryTypeDocument.IndustryType)get_store().find_element_user(INDUSTRYTYPE$16, 0);
            return target;
        }
    }
    
    /**
     * True if has "IndustryType" element
     */
    public boolean isSetIndustryType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(INDUSTRYTYPE$16) != 0;
        }
    }
    
    /**
     * Sets the "IndustryType" element
     */
    public void setIndustryType(java.lang.String industryType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(INDUSTRYTYPE$16, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(INDUSTRYTYPE$16);
            }
            target.setStringValue(industryType);
        }
    }
    
    /**
     * Sets (as xml) the "IndustryType" element
     */
    public void xsetIndustryType(com.idanalytics.products.idscore.request.IndustryTypeDocument.IndustryType industryType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.IndustryTypeDocument.IndustryType target = null;
            target = (com.idanalytics.products.idscore.request.IndustryTypeDocument.IndustryType)get_store().find_element_user(INDUSTRYTYPE$16, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.IndustryTypeDocument.IndustryType)get_store().add_element_user(INDUSTRYTYPE$16);
            }
            target.set(industryType);
        }
    }
    
    /**
     * Unsets the "IndustryType" element
     */
    public void unsetIndustryType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(INDUSTRYTYPE$16, 0);
        }
    }
    
    /**
     * Gets the "ChangeRequestSource" element
     */
    public com.idanalytics.products.idscore.request.ChangeRequestSource.Enum getChangeRequestSource()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CHANGEREQUESTSOURCE$18, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.idscore.request.ChangeRequestSource.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "ChangeRequestSource" element
     */
    public com.idanalytics.products.idscore.request.ChangeRequestSource xgetChangeRequestSource()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ChangeRequestSource target = null;
            target = (com.idanalytics.products.idscore.request.ChangeRequestSource)get_store().find_element_user(CHANGEREQUESTSOURCE$18, 0);
            return target;
        }
    }
    
    /**
     * True if has "ChangeRequestSource" element
     */
    public boolean isSetChangeRequestSource()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CHANGEREQUESTSOURCE$18) != 0;
        }
    }
    
    /**
     * Sets the "ChangeRequestSource" element
     */
    public void setChangeRequestSource(com.idanalytics.products.idscore.request.ChangeRequestSource.Enum changeRequestSource)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CHANGEREQUESTSOURCE$18, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CHANGEREQUESTSOURCE$18);
            }
            target.setEnumValue(changeRequestSource);
        }
    }
    
    /**
     * Sets (as xml) the "ChangeRequestSource" element
     */
    public void xsetChangeRequestSource(com.idanalytics.products.idscore.request.ChangeRequestSource changeRequestSource)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ChangeRequestSource target = null;
            target = (com.idanalytics.products.idscore.request.ChangeRequestSource)get_store().find_element_user(CHANGEREQUESTSOURCE$18, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.ChangeRequestSource)get_store().add_element_user(CHANGEREQUESTSOURCE$18);
            }
            target.set(changeRequestSource);
        }
    }
    
    /**
     * Unsets the "ChangeRequestSource" element
     */
    public void unsetChangeRequestSource()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CHANGEREQUESTSOURCE$18, 0);
        }
    }
    
    /**
     * Gets the "PrescreenDate" element
     */
    public java.lang.String getPrescreenDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PRESCREENDATE$20, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrescreenDate" element
     */
    public com.idanalytics.products.common_v1.CampaignIdentifier xgetPrescreenDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.CampaignIdentifier target = null;
            target = (com.idanalytics.products.common_v1.CampaignIdentifier)get_store().find_element_user(PRESCREENDATE$20, 0);
            return target;
        }
    }
    
    /**
     * True if has "PrescreenDate" element
     */
    public boolean isSetPrescreenDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PRESCREENDATE$20) != 0;
        }
    }
    
    /**
     * Sets the "PrescreenDate" element
     */
    public void setPrescreenDate(java.lang.String prescreenDate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PRESCREENDATE$20, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PRESCREENDATE$20);
            }
            target.setStringValue(prescreenDate);
        }
    }
    
    /**
     * Sets (as xml) the "PrescreenDate" element
     */
    public void xsetPrescreenDate(com.idanalytics.products.common_v1.CampaignIdentifier prescreenDate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.CampaignIdentifier target = null;
            target = (com.idanalytics.products.common_v1.CampaignIdentifier)get_store().find_element_user(PRESCREENDATE$20, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.CampaignIdentifier)get_store().add_element_user(PRESCREENDATE$20);
            }
            target.set(prescreenDate);
        }
    }
    
    /**
     * Unsets the "PrescreenDate" element
     */
    public void unsetPrescreenDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PRESCREENDATE$20, 0);
        }
    }
    
    /**
     * Gets the "ProgramDate" element
     */
    public java.lang.String getProgramDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PROGRAMDATE$22, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "ProgramDate" element
     */
    public com.idanalytics.products.common_v1.CampaignIdentifier xgetProgramDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.CampaignIdentifier target = null;
            target = (com.idanalytics.products.common_v1.CampaignIdentifier)get_store().find_element_user(PROGRAMDATE$22, 0);
            return target;
        }
    }
    
    /**
     * True if has "ProgramDate" element
     */
    public boolean isSetProgramDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PROGRAMDATE$22) != 0;
        }
    }
    
    /**
     * Sets the "ProgramDate" element
     */
    public void setProgramDate(java.lang.String programDate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PROGRAMDATE$22, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PROGRAMDATE$22);
            }
            target.setStringValue(programDate);
        }
    }
    
    /**
     * Sets (as xml) the "ProgramDate" element
     */
    public void xsetProgramDate(com.idanalytics.products.common_v1.CampaignIdentifier programDate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.CampaignIdentifier target = null;
            target = (com.idanalytics.products.common_v1.CampaignIdentifier)get_store().find_element_user(PROGRAMDATE$22, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.CampaignIdentifier)get_store().add_element_user(PROGRAMDATE$22);
            }
            target.set(programDate);
        }
    }
    
    /**
     * Unsets the "ProgramDate" element
     */
    public void unsetProgramDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PROGRAMDATE$22, 0);
        }
    }
    
    /**
     * Gets the "PromoCode" element
     */
    public java.lang.String getPromoCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PROMOCODE$24, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PromoCode" element
     */
    public org.apache.xmlbeans.XmlString xgetPromoCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(PROMOCODE$24, 0);
            return target;
        }
    }
    
    /**
     * True if has "PromoCode" element
     */
    public boolean isSetPromoCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PROMOCODE$24) != 0;
        }
    }
    
    /**
     * Sets the "PromoCode" element
     */
    public void setPromoCode(java.lang.String promoCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PROMOCODE$24, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PROMOCODE$24);
            }
            target.setStringValue(promoCode);
        }
    }
    
    /**
     * Sets (as xml) the "PromoCode" element
     */
    public void xsetPromoCode(org.apache.xmlbeans.XmlString promoCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(PROMOCODE$24, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(PROMOCODE$24);
            }
            target.set(promoCode);
        }
    }
    
    /**
     * Unsets the "PromoCode" element
     */
    public void unsetPromoCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PROMOCODE$24, 0);
        }
    }
    
    /**
     * Gets the "PassThru1" element
     */
    public java.lang.String getPassThru1()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU1$26, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PassThru1" element
     */
    public com.idanalytics.products.common_v1.PassThru xgetPassThru1()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.PassThru target = null;
            target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU1$26, 0);
            return target;
        }
    }
    
    /**
     * True if has "PassThru1" element
     */
    public boolean isSetPassThru1()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PASSTHRU1$26) != 0;
        }
    }
    
    /**
     * Sets the "PassThru1" element
     */
    public void setPassThru1(java.lang.String passThru1)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU1$26, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU1$26);
            }
            target.setStringValue(passThru1);
        }
    }
    
    /**
     * Sets (as xml) the "PassThru1" element
     */
    public void xsetPassThru1(com.idanalytics.products.common_v1.PassThru passThru1)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.PassThru target = null;
            target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU1$26, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.PassThru)get_store().add_element_user(PASSTHRU1$26);
            }
            target.set(passThru1);
        }
    }
    
    /**
     * Unsets the "PassThru1" element
     */
    public void unsetPassThru1()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PASSTHRU1$26, 0);
        }
    }
    
    /**
     * Gets the "PassThru2" element
     */
    public java.lang.String getPassThru2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU2$28, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PassThru2" element
     */
    public com.idanalytics.products.common_v1.PassThru xgetPassThru2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.PassThru target = null;
            target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU2$28, 0);
            return target;
        }
    }
    
    /**
     * True if has "PassThru2" element
     */
    public boolean isSetPassThru2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PASSTHRU2$28) != 0;
        }
    }
    
    /**
     * Sets the "PassThru2" element
     */
    public void setPassThru2(java.lang.String passThru2)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU2$28, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU2$28);
            }
            target.setStringValue(passThru2);
        }
    }
    
    /**
     * Sets (as xml) the "PassThru2" element
     */
    public void xsetPassThru2(com.idanalytics.products.common_v1.PassThru passThru2)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.PassThru target = null;
            target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU2$28, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.PassThru)get_store().add_element_user(PASSTHRU2$28);
            }
            target.set(passThru2);
        }
    }
    
    /**
     * Unsets the "PassThru2" element
     */
    public void unsetPassThru2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PASSTHRU2$28, 0);
        }
    }
    
    /**
     * Gets the "PassThru3" element
     */
    public java.lang.String getPassThru3()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU3$30, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PassThru3" element
     */
    public com.idanalytics.products.common_v1.PassThru xgetPassThru3()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.PassThru target = null;
            target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU3$30, 0);
            return target;
        }
    }
    
    /**
     * True if has "PassThru3" element
     */
    public boolean isSetPassThru3()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PASSTHRU3$30) != 0;
        }
    }
    
    /**
     * Sets the "PassThru3" element
     */
    public void setPassThru3(java.lang.String passThru3)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU3$30, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU3$30);
            }
            target.setStringValue(passThru3);
        }
    }
    
    /**
     * Sets (as xml) the "PassThru3" element
     */
    public void xsetPassThru3(com.idanalytics.products.common_v1.PassThru passThru3)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.PassThru target = null;
            target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU3$30, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.PassThru)get_store().add_element_user(PASSTHRU3$30);
            }
            target.set(passThru3);
        }
    }
    
    /**
     * Unsets the "PassThru3" element
     */
    public void unsetPassThru3()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PASSTHRU3$30, 0);
        }
    }
    
    /**
     * Gets the "PassThru4" element
     */
    public java.lang.String getPassThru4()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU4$32, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PassThru4" element
     */
    public com.idanalytics.products.common_v1.PassThru xgetPassThru4()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.PassThru target = null;
            target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU4$32, 0);
            return target;
        }
    }
    
    /**
     * True if has "PassThru4" element
     */
    public boolean isSetPassThru4()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PASSTHRU4$32) != 0;
        }
    }
    
    /**
     * Sets the "PassThru4" element
     */
    public void setPassThru4(java.lang.String passThru4)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU4$32, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU4$32);
            }
            target.setStringValue(passThru4);
        }
    }
    
    /**
     * Sets (as xml) the "PassThru4" element
     */
    public void xsetPassThru4(com.idanalytics.products.common_v1.PassThru passThru4)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.PassThru target = null;
            target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU4$32, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.PassThru)get_store().add_element_user(PASSTHRU4$32);
            }
            target.set(passThru4);
        }
    }
    
    /**
     * Unsets the "PassThru4" element
     */
    public void unsetPassThru4()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PASSTHRU4$32, 0);
        }
    }
}
