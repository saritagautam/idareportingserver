/*
 * An XML document type.
 * Localname: DeviceIDSource
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.DeviceIDSourceDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * A document containing one DeviceIDSource(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public class DeviceIDSourceDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.DeviceIDSourceDocument
{
    private static final long serialVersionUID = 1L;
    
    public DeviceIDSourceDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName DEVICEIDSOURCE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "DeviceIDSource");
    
    
    /**
     * Gets the "DeviceIDSource" element
     */
    public java.lang.String getDeviceIDSource()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DEVICEIDSOURCE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "DeviceIDSource" element
     */
    public com.idanalytics.products.idscore.request.DeviceIDSourceDocument.DeviceIDSource xgetDeviceIDSource()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.DeviceIDSourceDocument.DeviceIDSource target = null;
            target = (com.idanalytics.products.idscore.request.DeviceIDSourceDocument.DeviceIDSource)get_store().find_element_user(DEVICEIDSOURCE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "DeviceIDSource" element
     */
    public void setDeviceIDSource(java.lang.String deviceIDSource)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DEVICEIDSOURCE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DEVICEIDSOURCE$0);
            }
            target.setStringValue(deviceIDSource);
        }
    }
    
    /**
     * Sets (as xml) the "DeviceIDSource" element
     */
    public void xsetDeviceIDSource(com.idanalytics.products.idscore.request.DeviceIDSourceDocument.DeviceIDSource deviceIDSource)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.DeviceIDSourceDocument.DeviceIDSource target = null;
            target = (com.idanalytics.products.idscore.request.DeviceIDSourceDocument.DeviceIDSource)get_store().find_element_user(DEVICEIDSOURCE$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.DeviceIDSourceDocument.DeviceIDSource)get_store().add_element_user(DEVICEIDSOURCE$0);
            }
            target.set(deviceIDSource);
        }
    }
    /**
     * An XML DeviceIDSource(@http://idanalytics.com/products/idscore/request).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.DeviceIDSourceDocument$DeviceIDSource.
     */
    public static class DeviceIDSourceImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.request.DeviceIDSourceDocument.DeviceIDSource
    {
        private static final long serialVersionUID = 1L;
        
        public DeviceIDSourceImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected DeviceIDSourceImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
