/*
 * XML Type:  ECommerce
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.ECommerce
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML ECommerce(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public class ECommerceImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.ECommerce
{
    private static final long serialVersionUID = 1L;
    
    public ECommerceImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SHIPPINGIDENTITY$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ShippingIdentity");
    private static final javax.xml.namespace.QName ORDERINFO$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "OrderInfo");
    
    
    /**
     * Gets array of all "ShippingIdentity" elements
     */
    public com.idanalytics.products.idscore.request.ShippingIdentity[] getShippingIdentityArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(SHIPPINGIDENTITY$0, targetList);
            com.idanalytics.products.idscore.request.ShippingIdentity[] result = new com.idanalytics.products.idscore.request.ShippingIdentity[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "ShippingIdentity" element
     */
    public com.idanalytics.products.idscore.request.ShippingIdentity getShippingIdentityArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ShippingIdentity target = null;
            target = (com.idanalytics.products.idscore.request.ShippingIdentity)get_store().find_element_user(SHIPPINGIDENTITY$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "ShippingIdentity" element
     */
    public int sizeOfShippingIdentityArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SHIPPINGIDENTITY$0);
        }
    }
    
    /**
     * Sets array of all "ShippingIdentity" element  WARNING: This method is not atomicaly synchronized.
     */
    public void setShippingIdentityArray(com.idanalytics.products.idscore.request.ShippingIdentity[] shippingIdentityArray)
    {
        check_orphaned();
        arraySetterHelper(shippingIdentityArray, SHIPPINGIDENTITY$0);
    }
    
    /**
     * Sets ith "ShippingIdentity" element
     */
    public void setShippingIdentityArray(int i, com.idanalytics.products.idscore.request.ShippingIdentity shippingIdentity)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ShippingIdentity target = null;
            target = (com.idanalytics.products.idscore.request.ShippingIdentity)get_store().find_element_user(SHIPPINGIDENTITY$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(shippingIdentity);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "ShippingIdentity" element
     */
    public com.idanalytics.products.idscore.request.ShippingIdentity insertNewShippingIdentity(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ShippingIdentity target = null;
            target = (com.idanalytics.products.idscore.request.ShippingIdentity)get_store().insert_element_user(SHIPPINGIDENTITY$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "ShippingIdentity" element
     */
    public com.idanalytics.products.idscore.request.ShippingIdentity addNewShippingIdentity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ShippingIdentity target = null;
            target = (com.idanalytics.products.idscore.request.ShippingIdentity)get_store().add_element_user(SHIPPINGIDENTITY$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "ShippingIdentity" element
     */
    public void removeShippingIdentity(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SHIPPINGIDENTITY$0, i);
        }
    }
    
    /**
     * Gets the "OrderInfo" element
     */
    public com.idanalytics.products.idscore.request.OrderInfo getOrderInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.OrderInfo target = null;
            target = (com.idanalytics.products.idscore.request.OrderInfo)get_store().find_element_user(ORDERINFO$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "OrderInfo" element
     */
    public void setOrderInfo(com.idanalytics.products.idscore.request.OrderInfo orderInfo)
    {
        generatedSetterHelperImpl(orderInfo, ORDERINFO$2, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "OrderInfo" element
     */
    public com.idanalytics.products.idscore.request.OrderInfo addNewOrderInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.OrderInfo target = null;
            target = (com.idanalytics.products.idscore.request.OrderInfo)get_store().add_element_user(ORDERINFO$2);
            return target;
        }
    }
}
