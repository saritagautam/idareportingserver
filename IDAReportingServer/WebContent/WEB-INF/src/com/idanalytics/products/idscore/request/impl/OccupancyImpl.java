/*
 * XML Type:  Occupancy
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.Occupancy
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML Occupancy(@http://idanalytics.com/products/idscore/request).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.Occupancy.
 */
public class OccupancyImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.idanalytics.products.idscore.request.Occupancy
{
    private static final long serialVersionUID = 1L;
    
    public OccupancyImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected OccupancyImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
