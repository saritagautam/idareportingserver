/*
 * XML Type:  Status
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.Status
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML Status(@http://idanalytics.com/products/idscore/request).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.Status.
 */
public class StatusImpl extends org.apache.xmlbeans.impl.values.JavaIntHolderEx implements com.idanalytics.products.idscore.request.Status
{
    private static final long serialVersionUID = 1L;
    
    public StatusImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected StatusImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
