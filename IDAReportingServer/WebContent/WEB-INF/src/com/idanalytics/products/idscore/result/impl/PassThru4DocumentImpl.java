/*
 * An XML document type.
 * Localname: PassThru4
 * Namespace: http://idanalytics.com/products/idscore/result
 * Java type: com.idanalytics.products.idscore.result.PassThru4Document
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.result.impl;
/**
 * A document containing one PassThru4(@http://idanalytics.com/products/idscore/result) element.
 *
 * This is a complex type.
 */
public class PassThru4DocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.result.PassThru4Document
{
    private static final long serialVersionUID = 1L;
    
    public PassThru4DocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName PASSTHRU4$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "PassThru4");
    
    
    /**
     * Gets the "PassThru4" element
     */
    public java.lang.String getPassThru4()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU4$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PassThru4" element
     */
    public com.idanalytics.products.idscore.result.PassThru4Document.PassThru4 xgetPassThru4()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.PassThru4Document.PassThru4 target = null;
            target = (com.idanalytics.products.idscore.result.PassThru4Document.PassThru4)get_store().find_element_user(PASSTHRU4$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "PassThru4" element
     */
    public void setPassThru4(java.lang.String passThru4)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU4$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU4$0);
            }
            target.setStringValue(passThru4);
        }
    }
    
    /**
     * Sets (as xml) the "PassThru4" element
     */
    public void xsetPassThru4(com.idanalytics.products.idscore.result.PassThru4Document.PassThru4 passThru4)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.PassThru4Document.PassThru4 target = null;
            target = (com.idanalytics.products.idscore.result.PassThru4Document.PassThru4)get_store().find_element_user(PASSTHRU4$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.result.PassThru4Document.PassThru4)get_store().add_element_user(PASSTHRU4$0);
            }
            target.set(passThru4);
        }
    }
    /**
     * An XML PassThru4(@http://idanalytics.com/products/idscore/result).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result.PassThru4Document$PassThru4.
     */
    public static class PassThru4Impl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.result.PassThru4Document.PassThru4
    {
        private static final long serialVersionUID = 1L;
        
        public PassThru4Impl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected PassThru4Impl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
