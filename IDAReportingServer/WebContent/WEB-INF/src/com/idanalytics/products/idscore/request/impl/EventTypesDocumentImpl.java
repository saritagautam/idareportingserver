/*
 * An XML document type.
 * Localname: EventTypes
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.EventTypesDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * A document containing one EventTypes(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public class EventTypesDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.EventTypesDocument
{
    private static final long serialVersionUID = 1L;
    
    public EventTypesDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName EVENTTYPES$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "EventTypes");
    
    
    /**
     * Gets the "EventTypes" element
     */
    public com.idanalytics.products.idscore.request.EventTypesDocument.EventTypes getEventTypes()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.EventTypesDocument.EventTypes target = null;
            target = (com.idanalytics.products.idscore.request.EventTypesDocument.EventTypes)get_store().find_element_user(EVENTTYPES$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "EventTypes" element
     */
    public void setEventTypes(com.idanalytics.products.idscore.request.EventTypesDocument.EventTypes eventTypes)
    {
        generatedSetterHelperImpl(eventTypes, EVENTTYPES$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "EventTypes" element
     */
    public com.idanalytics.products.idscore.request.EventTypesDocument.EventTypes addNewEventTypes()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.EventTypesDocument.EventTypes target = null;
            target = (com.idanalytics.products.idscore.request.EventTypesDocument.EventTypes)get_store().add_element_user(EVENTTYPES$0);
            return target;
        }
    }
    /**
     * An XML EventTypes(@http://idanalytics.com/products/idscore/request).
     *
     * This is a complex type.
     */
    public static class EventTypesImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.EventTypesDocument.EventTypes
    {
        private static final long serialVersionUID = 1L;
        
        public EventTypesImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName EVENTTYPE$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "EventType");
        
        
        /**
         * Gets array of all "EventType" elements
         */
        public java.lang.String[] getEventTypeArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(EVENTTYPE$0, targetList);
                java.lang.String[] result = new java.lang.String[targetList.size()];
                for (int i = 0, len = targetList.size() ; i < len ; i++)
                    result[i] = ((org.apache.xmlbeans.SimpleValue)targetList.get(i)).getStringValue();
                return result;
            }
        }
        
        /**
         * Gets ith "EventType" element
         */
        public java.lang.String getEventTypeArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EVENTTYPE$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) array of all "EventType" elements
         */
        public com.idanalytics.products.idscore.request.EventTypeDocument.EventType[] xgetEventTypeArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(EVENTTYPE$0, targetList);
                com.idanalytics.products.idscore.request.EventTypeDocument.EventType[] result = new com.idanalytics.products.idscore.request.EventTypeDocument.EventType[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets (as xml) ith "EventType" element
         */
        public com.idanalytics.products.idscore.request.EventTypeDocument.EventType xgetEventTypeArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.EventTypeDocument.EventType target = null;
                target = (com.idanalytics.products.idscore.request.EventTypeDocument.EventType)get_store().find_element_user(EVENTTYPE$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "EventType" element
         */
        public int sizeOfEventTypeArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(EVENTTYPE$0);
            }
        }
        
        /**
         * Sets array of all "EventType" element
         */
        public void setEventTypeArray(java.lang.String[] eventTypeArray)
        {
            synchronized (monitor())
            {
                check_orphaned();
                arraySetterHelper(eventTypeArray, EVENTTYPE$0);
            }
        }
        
        /**
         * Sets ith "EventType" element
         */
        public void setEventTypeArray(int i, java.lang.String eventType)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EVENTTYPE$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.setStringValue(eventType);
            }
        }
        
        /**
         * Sets (as xml) array of all "EventType" element
         */
        public void xsetEventTypeArray(com.idanalytics.products.idscore.request.EventTypeDocument.EventType[]eventTypeArray)
        {
            synchronized (monitor())
            {
                check_orphaned();
                arraySetterHelper(eventTypeArray, EVENTTYPE$0);
            }
        }
        
        /**
         * Sets (as xml) ith "EventType" element
         */
        public void xsetEventTypeArray(int i, com.idanalytics.products.idscore.request.EventTypeDocument.EventType eventType)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.EventTypeDocument.EventType target = null;
                target = (com.idanalytics.products.idscore.request.EventTypeDocument.EventType)get_store().find_element_user(EVENTTYPE$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(eventType);
            }
        }
        
        /**
         * Inserts the value as the ith "EventType" element
         */
        public void insertEventType(int i, java.lang.String eventType)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = 
                    (org.apache.xmlbeans.SimpleValue)get_store().insert_element_user(EVENTTYPE$0, i);
                target.setStringValue(eventType);
            }
        }
        
        /**
         * Appends the value as the last "EventType" element
         */
        public void addEventType(java.lang.String eventType)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(EVENTTYPE$0);
                target.setStringValue(eventType);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "EventType" element
         */
        public com.idanalytics.products.idscore.request.EventTypeDocument.EventType insertNewEventType(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.EventTypeDocument.EventType target = null;
                target = (com.idanalytics.products.idscore.request.EventTypeDocument.EventType)get_store().insert_element_user(EVENTTYPE$0, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "EventType" element
         */
        public com.idanalytics.products.idscore.request.EventTypeDocument.EventType addNewEventType()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.EventTypeDocument.EventType target = null;
                target = (com.idanalytics.products.idscore.request.EventTypeDocument.EventType)get_store().add_element_user(EVENTTYPE$0);
                return target;
            }
        }
        
        /**
         * Removes the ith "EventType" element
         */
        public void removeEventType(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(EVENTTYPE$0, i);
            }
        }
    }
}
