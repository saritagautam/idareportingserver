/*
 * An XML document type.
 * Localname: LastName
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.LastNameDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * A document containing one LastName(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public class LastNameDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.LastNameDocument
{
    private static final long serialVersionUID = 1L;
    
    public LastNameDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName LASTNAME$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "LastName");
    
    
    /**
     * Gets the "LastName" element
     */
    public java.lang.String getLastName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LASTNAME$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "LastName" element
     */
    public com.idanalytics.products.idscore.request.LastNameDocument.LastName xgetLastName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.LastNameDocument.LastName target = null;
            target = (com.idanalytics.products.idscore.request.LastNameDocument.LastName)get_store().find_element_user(LASTNAME$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "LastName" element
     */
    public void setLastName(java.lang.String lastName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LASTNAME$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(LASTNAME$0);
            }
            target.setStringValue(lastName);
        }
    }
    
    /**
     * Sets (as xml) the "LastName" element
     */
    public void xsetLastName(com.idanalytics.products.idscore.request.LastNameDocument.LastName lastName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.LastNameDocument.LastName target = null;
            target = (com.idanalytics.products.idscore.request.LastNameDocument.LastName)get_store().find_element_user(LASTNAME$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.LastNameDocument.LastName)get_store().add_element_user(LASTNAME$0);
            }
            target.set(lastName);
        }
    }
    /**
     * An XML LastName(@http://idanalytics.com/products/idscore/request).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.LastNameDocument$LastName.
     */
    public static class LastNameImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.request.LastNameDocument.LastName
    {
        private static final long serialVersionUID = 1L;
        
        public LastNameImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected LastNameImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
