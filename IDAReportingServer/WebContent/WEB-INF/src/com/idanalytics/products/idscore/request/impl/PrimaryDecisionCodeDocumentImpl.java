/*
 * An XML document type.
 * Localname: PrimaryDecisionCode
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.PrimaryDecisionCodeDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * A document containing one PrimaryDecisionCode(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public class PrimaryDecisionCodeDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.PrimaryDecisionCodeDocument
{
    private static final long serialVersionUID = 1L;
    
    public PrimaryDecisionCodeDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName PRIMARYDECISIONCODE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrimaryDecisionCode");
    
    
    /**
     * Gets the "PrimaryDecisionCode" element
     */
    public java.lang.String getPrimaryDecisionCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PRIMARYDECISIONCODE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrimaryDecisionCode" element
     */
    public com.idanalytics.products.idscore.request.PrimaryDecisionCodeDocument.PrimaryDecisionCode xgetPrimaryDecisionCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.PrimaryDecisionCodeDocument.PrimaryDecisionCode target = null;
            target = (com.idanalytics.products.idscore.request.PrimaryDecisionCodeDocument.PrimaryDecisionCode)get_store().find_element_user(PRIMARYDECISIONCODE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "PrimaryDecisionCode" element
     */
    public void setPrimaryDecisionCode(java.lang.String primaryDecisionCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PRIMARYDECISIONCODE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PRIMARYDECISIONCODE$0);
            }
            target.setStringValue(primaryDecisionCode);
        }
    }
    
    /**
     * Sets (as xml) the "PrimaryDecisionCode" element
     */
    public void xsetPrimaryDecisionCode(com.idanalytics.products.idscore.request.PrimaryDecisionCodeDocument.PrimaryDecisionCode primaryDecisionCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.PrimaryDecisionCodeDocument.PrimaryDecisionCode target = null;
            target = (com.idanalytics.products.idscore.request.PrimaryDecisionCodeDocument.PrimaryDecisionCode)get_store().find_element_user(PRIMARYDECISIONCODE$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.PrimaryDecisionCodeDocument.PrimaryDecisionCode)get_store().add_element_user(PRIMARYDECISIONCODE$0);
            }
            target.set(primaryDecisionCode);
        }
    }
    /**
     * An XML PrimaryDecisionCode(@http://idanalytics.com/products/idscore/request).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.PrimaryDecisionCodeDocument$PrimaryDecisionCode.
     */
    public static class PrimaryDecisionCodeImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.request.PrimaryDecisionCodeDocument.PrimaryDecisionCode
    {
        private static final long serialVersionUID = 1L;
        
        public PrimaryDecisionCodeImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected PrimaryDecisionCodeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
