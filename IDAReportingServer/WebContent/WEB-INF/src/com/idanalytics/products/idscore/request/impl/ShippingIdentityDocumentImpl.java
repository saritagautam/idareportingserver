/*
 * An XML document type.
 * Localname: ShippingIdentity
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.ShippingIdentityDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * A document containing one ShippingIdentity(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public class ShippingIdentityDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.ShippingIdentityDocument
{
    private static final long serialVersionUID = 1L;
    
    public ShippingIdentityDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SHIPPINGIDENTITY$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ShippingIdentity");
    
    
    /**
     * Gets the "ShippingIdentity" element
     */
    public com.idanalytics.products.idscore.request.OrderIdentity getShippingIdentity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.OrderIdentity target = null;
            target = (com.idanalytics.products.idscore.request.OrderIdentity)get_store().find_element_user(SHIPPINGIDENTITY$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "ShippingIdentity" element
     */
    public void setShippingIdentity(com.idanalytics.products.idscore.request.OrderIdentity shippingIdentity)
    {
        generatedSetterHelperImpl(shippingIdentity, SHIPPINGIDENTITY$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "ShippingIdentity" element
     */
    public com.idanalytics.products.idscore.request.OrderIdentity addNewShippingIdentity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.OrderIdentity target = null;
            target = (com.idanalytics.products.idscore.request.OrderIdentity)get_store().add_element_user(SHIPPINGIDENTITY$0);
            return target;
        }
    }
}
