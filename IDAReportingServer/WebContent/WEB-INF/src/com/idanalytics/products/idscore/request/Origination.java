/*
 * XML Type:  Origination
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.Origination
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request;


/**
 * An XML Origination(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public interface Origination extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Origination.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("origination5075type");
    
    /**
     * Gets the "CPC" element
     */
    java.lang.String getCPC();
    
    /**
     * Gets (as xml) the "CPC" element
     */
    com.idanalytics.products.idscore.request.CPCDocument.CPC xgetCPC();
    
    /**
     * True if has "CPC" element
     */
    boolean isSetCPC();
    
    /**
     * Sets the "CPC" element
     */
    void setCPC(java.lang.String cpc);
    
    /**
     * Sets (as xml) the "CPC" element
     */
    void xsetCPC(com.idanalytics.products.idscore.request.CPCDocument.CPC cpc);
    
    /**
     * Unsets the "CPC" element
     */
    void unsetCPC();
    
    /**
     * Gets the "RequestType" element
     */
    java.lang.Object getRequestType();
    
    /**
     * Gets (as xml) the "RequestType" element
     */
    com.idanalytics.products.idscore.request.RequestTypeDocument.RequestType xgetRequestType();
    
    /**
     * Sets the "RequestType" element
     */
    void setRequestType(java.lang.Object requestType);
    
    /**
     * Sets (as xml) the "RequestType" element
     */
    void xsetRequestType(com.idanalytics.products.idscore.request.RequestTypeDocument.RequestType requestType);
    
    /**
     * Gets the "ApplicationDate" element
     */
    java.util.Calendar getApplicationDate();
    
    /**
     * Gets (as xml) the "ApplicationDate" element
     */
    com.idanalytics.products.idscore.request.ApplicationDateDocument.ApplicationDate xgetApplicationDate();
    
    /**
     * Sets the "ApplicationDate" element
     */
    void setApplicationDate(java.util.Calendar applicationDate);
    
    /**
     * Sets (as xml) the "ApplicationDate" element
     */
    void xsetApplicationDate(com.idanalytics.products.idscore.request.ApplicationDateDocument.ApplicationDate applicationDate);
    
    /**
     * Gets the "AppID" element
     */
    java.lang.String getAppID();
    
    /**
     * Gets (as xml) the "AppID" element
     */
    com.idanalytics.products.common_v1.AppID xgetAppID();
    
    /**
     * Sets the "AppID" element
     */
    void setAppID(java.lang.String appID);
    
    /**
     * Sets (as xml) the "AppID" element
     */
    void xsetAppID(com.idanalytics.products.common_v1.AppID appID);
    
    /**
     * Gets the "Designation" element
     */
    com.idanalytics.products.idscore.request.DesignationDocument.Designation.Enum getDesignation();
    
    /**
     * Gets (as xml) the "Designation" element
     */
    com.idanalytics.products.idscore.request.DesignationDocument.Designation xgetDesignation();
    
    /**
     * Sets the "Designation" element
     */
    void setDesignation(com.idanalytics.products.idscore.request.DesignationDocument.Designation.Enum designation);
    
    /**
     * Sets (as xml) the "Designation" element
     */
    void xsetDesignation(com.idanalytics.products.idscore.request.DesignationDocument.Designation designation);
    
    /**
     * Gets the "AcctLinkKey" element
     */
    java.lang.String getAcctLinkKey();
    
    /**
     * Gets (as xml) the "AcctLinkKey" element
     */
    com.idanalytics.products.idscore.request.AcctLinkKeyDocument.AcctLinkKey xgetAcctLinkKey();
    
    /**
     * True if has "AcctLinkKey" element
     */
    boolean isSetAcctLinkKey();
    
    /**
     * Sets the "AcctLinkKey" element
     */
    void setAcctLinkKey(java.lang.String acctLinkKey);
    
    /**
     * Sets (as xml) the "AcctLinkKey" element
     */
    void xsetAcctLinkKey(com.idanalytics.products.idscore.request.AcctLinkKeyDocument.AcctLinkKey acctLinkKey);
    
    /**
     * Unsets the "AcctLinkKey" element
     */
    void unsetAcctLinkKey();
    
    /**
     * Gets the "EventType" element
     */
    java.lang.String getEventType();
    
    /**
     * Gets (as xml) the "EventType" element
     */
    com.idanalytics.products.idscore.request.EventTypeDocument.EventType xgetEventType();
    
    /**
     * True if has "EventType" element
     */
    boolean isSetEventType();
    
    /**
     * Sets the "EventType" element
     */
    void setEventType(java.lang.String eventType);
    
    /**
     * Sets (as xml) the "EventType" element
     */
    void xsetEventType(com.idanalytics.products.idscore.request.EventTypeDocument.EventType eventType);
    
    /**
     * Unsets the "EventType" element
     */
    void unsetEventType();
    
    /**
     * Gets the "EventTypes" element
     */
    com.idanalytics.products.idscore.request.EventTypesDocument.EventTypes getEventTypes();
    
    /**
     * True if has "EventTypes" element
     */
    boolean isSetEventTypes();
    
    /**
     * Sets the "EventTypes" element
     */
    void setEventTypes(com.idanalytics.products.idscore.request.EventTypesDocument.EventTypes eventTypes);
    
    /**
     * Appends and returns a new empty "EventTypes" element
     */
    com.idanalytics.products.idscore.request.EventTypesDocument.EventTypes addNewEventTypes();
    
    /**
     * Unsets the "EventTypes" element
     */
    void unsetEventTypes();
    
    /**
     * Gets the "IndustryType" element
     */
    java.lang.String getIndustryType();
    
    /**
     * Gets (as xml) the "IndustryType" element
     */
    com.idanalytics.products.idscore.request.IndustryTypeDocument.IndustryType xgetIndustryType();
    
    /**
     * True if has "IndustryType" element
     */
    boolean isSetIndustryType();
    
    /**
     * Sets the "IndustryType" element
     */
    void setIndustryType(java.lang.String industryType);
    
    /**
     * Sets (as xml) the "IndustryType" element
     */
    void xsetIndustryType(com.idanalytics.products.idscore.request.IndustryTypeDocument.IndustryType industryType);
    
    /**
     * Unsets the "IndustryType" element
     */
    void unsetIndustryType();
    
    /**
     * Gets the "ChangeRequestSource" element
     */
    com.idanalytics.products.idscore.request.ChangeRequestSource.Enum getChangeRequestSource();
    
    /**
     * Gets (as xml) the "ChangeRequestSource" element
     */
    com.idanalytics.products.idscore.request.ChangeRequestSource xgetChangeRequestSource();
    
    /**
     * True if has "ChangeRequestSource" element
     */
    boolean isSetChangeRequestSource();
    
    /**
     * Sets the "ChangeRequestSource" element
     */
    void setChangeRequestSource(com.idanalytics.products.idscore.request.ChangeRequestSource.Enum changeRequestSource);
    
    /**
     * Sets (as xml) the "ChangeRequestSource" element
     */
    void xsetChangeRequestSource(com.idanalytics.products.idscore.request.ChangeRequestSource changeRequestSource);
    
    /**
     * Unsets the "ChangeRequestSource" element
     */
    void unsetChangeRequestSource();
    
    /**
     * Gets the "PrescreenDate" element
     */
    java.lang.String getPrescreenDate();
    
    /**
     * Gets (as xml) the "PrescreenDate" element
     */
    com.idanalytics.products.common_v1.CampaignIdentifier xgetPrescreenDate();
    
    /**
     * True if has "PrescreenDate" element
     */
    boolean isSetPrescreenDate();
    
    /**
     * Sets the "PrescreenDate" element
     */
    void setPrescreenDate(java.lang.String prescreenDate);
    
    /**
     * Sets (as xml) the "PrescreenDate" element
     */
    void xsetPrescreenDate(com.idanalytics.products.common_v1.CampaignIdentifier prescreenDate);
    
    /**
     * Unsets the "PrescreenDate" element
     */
    void unsetPrescreenDate();
    
    /**
     * Gets the "ProgramDate" element
     */
    java.lang.String getProgramDate();
    
    /**
     * Gets (as xml) the "ProgramDate" element
     */
    com.idanalytics.products.common_v1.CampaignIdentifier xgetProgramDate();
    
    /**
     * True if has "ProgramDate" element
     */
    boolean isSetProgramDate();
    
    /**
     * Sets the "ProgramDate" element
     */
    void setProgramDate(java.lang.String programDate);
    
    /**
     * Sets (as xml) the "ProgramDate" element
     */
    void xsetProgramDate(com.idanalytics.products.common_v1.CampaignIdentifier programDate);
    
    /**
     * Unsets the "ProgramDate" element
     */
    void unsetProgramDate();
    
    /**
     * Gets the "PromoCode" element
     */
    java.lang.String getPromoCode();
    
    /**
     * Gets (as xml) the "PromoCode" element
     */
    org.apache.xmlbeans.XmlString xgetPromoCode();
    
    /**
     * True if has "PromoCode" element
     */
    boolean isSetPromoCode();
    
    /**
     * Sets the "PromoCode" element
     */
    void setPromoCode(java.lang.String promoCode);
    
    /**
     * Sets (as xml) the "PromoCode" element
     */
    void xsetPromoCode(org.apache.xmlbeans.XmlString promoCode);
    
    /**
     * Unsets the "PromoCode" element
     */
    void unsetPromoCode();
    
    /**
     * Gets the "PassThru1" element
     */
    java.lang.String getPassThru1();
    
    /**
     * Gets (as xml) the "PassThru1" element
     */
    com.idanalytics.products.common_v1.PassThru xgetPassThru1();
    
    /**
     * True if has "PassThru1" element
     */
    boolean isSetPassThru1();
    
    /**
     * Sets the "PassThru1" element
     */
    void setPassThru1(java.lang.String passThru1);
    
    /**
     * Sets (as xml) the "PassThru1" element
     */
    void xsetPassThru1(com.idanalytics.products.common_v1.PassThru passThru1);
    
    /**
     * Unsets the "PassThru1" element
     */
    void unsetPassThru1();
    
    /**
     * Gets the "PassThru2" element
     */
    java.lang.String getPassThru2();
    
    /**
     * Gets (as xml) the "PassThru2" element
     */
    com.idanalytics.products.common_v1.PassThru xgetPassThru2();
    
    /**
     * True if has "PassThru2" element
     */
    boolean isSetPassThru2();
    
    /**
     * Sets the "PassThru2" element
     */
    void setPassThru2(java.lang.String passThru2);
    
    /**
     * Sets (as xml) the "PassThru2" element
     */
    void xsetPassThru2(com.idanalytics.products.common_v1.PassThru passThru2);
    
    /**
     * Unsets the "PassThru2" element
     */
    void unsetPassThru2();
    
    /**
     * Gets the "PassThru3" element
     */
    java.lang.String getPassThru3();
    
    /**
     * Gets (as xml) the "PassThru3" element
     */
    com.idanalytics.products.common_v1.PassThru xgetPassThru3();
    
    /**
     * True if has "PassThru3" element
     */
    boolean isSetPassThru3();
    
    /**
     * Sets the "PassThru3" element
     */
    void setPassThru3(java.lang.String passThru3);
    
    /**
     * Sets (as xml) the "PassThru3" element
     */
    void xsetPassThru3(com.idanalytics.products.common_v1.PassThru passThru3);
    
    /**
     * Unsets the "PassThru3" element
     */
    void unsetPassThru3();
    
    /**
     * Gets the "PassThru4" element
     */
    java.lang.String getPassThru4();
    
    /**
     * Gets (as xml) the "PassThru4" element
     */
    com.idanalytics.products.common_v1.PassThru xgetPassThru4();
    
    /**
     * True if has "PassThru4" element
     */
    boolean isSetPassThru4();
    
    /**
     * Sets the "PassThru4" element
     */
    void setPassThru4(java.lang.String passThru4);
    
    /**
     * Sets (as xml) the "PassThru4" element
     */
    void xsetPassThru4(com.idanalytics.products.common_v1.PassThru passThru4);
    
    /**
     * Unsets the "PassThru4" element
     */
    void unsetPassThru4();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.idscore.request.Origination newInstance() {
          return (com.idanalytics.products.idscore.request.Origination) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.idscore.request.Origination newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.idscore.request.Origination) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.idscore.request.Origination parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.Origination) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.idscore.request.Origination parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.Origination) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.idscore.request.Origination parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.Origination) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.idscore.request.Origination parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.Origination) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.idscore.request.Origination parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.Origination) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.idscore.request.Origination parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.Origination) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.idscore.request.Origination parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.Origination) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.idscore.request.Origination parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.Origination) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.idscore.request.Origination parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.Origination) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.idscore.request.Origination parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.Origination) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.idscore.request.Origination parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.Origination) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.idscore.request.Origination parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.Origination) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.idscore.request.Origination parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.Origination) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.idscore.request.Origination parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.Origination) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.request.Origination parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.request.Origination) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.request.Origination parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.request.Origination) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
