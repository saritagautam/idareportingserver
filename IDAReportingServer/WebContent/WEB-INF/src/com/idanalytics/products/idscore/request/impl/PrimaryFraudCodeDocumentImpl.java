/*
 * An XML document type.
 * Localname: PrimaryFraudCode
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.PrimaryFraudCodeDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * A document containing one PrimaryFraudCode(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public class PrimaryFraudCodeDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.PrimaryFraudCodeDocument
{
    private static final long serialVersionUID = 1L;
    
    public PrimaryFraudCodeDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName PRIMARYFRAUDCODE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrimaryFraudCode");
    
    
    /**
     * Gets the "PrimaryFraudCode" element
     */
    public java.lang.String getPrimaryFraudCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PRIMARYFRAUDCODE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrimaryFraudCode" element
     */
    public com.idanalytics.products.idscore.request.PrimaryFraudCodeDocument.PrimaryFraudCode xgetPrimaryFraudCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.PrimaryFraudCodeDocument.PrimaryFraudCode target = null;
            target = (com.idanalytics.products.idscore.request.PrimaryFraudCodeDocument.PrimaryFraudCode)get_store().find_element_user(PRIMARYFRAUDCODE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "PrimaryFraudCode" element
     */
    public void setPrimaryFraudCode(java.lang.String primaryFraudCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PRIMARYFRAUDCODE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PRIMARYFRAUDCODE$0);
            }
            target.setStringValue(primaryFraudCode);
        }
    }
    
    /**
     * Sets (as xml) the "PrimaryFraudCode" element
     */
    public void xsetPrimaryFraudCode(com.idanalytics.products.idscore.request.PrimaryFraudCodeDocument.PrimaryFraudCode primaryFraudCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.PrimaryFraudCodeDocument.PrimaryFraudCode target = null;
            target = (com.idanalytics.products.idscore.request.PrimaryFraudCodeDocument.PrimaryFraudCode)get_store().find_element_user(PRIMARYFRAUDCODE$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.PrimaryFraudCodeDocument.PrimaryFraudCode)get_store().add_element_user(PRIMARYFRAUDCODE$0);
            }
            target.set(primaryFraudCode);
        }
    }
    /**
     * An XML PrimaryFraudCode(@http://idanalytics.com/products/idscore/request).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.PrimaryFraudCodeDocument$PrimaryFraudCode.
     */
    public static class PrimaryFraudCodeImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.request.PrimaryFraudCodeDocument.PrimaryFraudCode
    {
        private static final long serialVersionUID = 1L;
        
        public PrimaryFraudCodeImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected PrimaryFraudCodeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
