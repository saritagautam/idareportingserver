/*
 * XML Type:  CommerceIdentity
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.CommerceIdentity
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML CommerceIdentity(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public class CommerceIdentityImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.CommerceIdentity
{
    private static final long serialVersionUID = 1L;
    
    public CommerceIdentityImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName TITLE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Title");
    private static final javax.xml.namespace.QName FIRSTNAME$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "FirstName");
    private static final javax.xml.namespace.QName MIDDLENAME$4 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "MiddleName");
    private static final javax.xml.namespace.QName LASTNAME$6 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "LastName");
    private static final javax.xml.namespace.QName SUFFIX$8 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Suffix");
    private static final javax.xml.namespace.QName COMPANY$10 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Company");
    private static final javax.xml.namespace.QName ADDRESS$12 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Address");
    private static final javax.xml.namespace.QName CITY$14 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "City");
    private static final javax.xml.namespace.QName STATE$16 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "State");
    private static final javax.xml.namespace.QName ZIP$18 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Zip");
    private static final javax.xml.namespace.QName COUNTRY$20 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Country");
    private static final javax.xml.namespace.QName OCCUPANCY$22 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Occupancy");
    private static final javax.xml.namespace.QName HOMEPHONE$24 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "HomePhone");
    private static final javax.xml.namespace.QName MOBILEPHONE$26 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "MobilePhone");
    private static final javax.xml.namespace.QName WORKPHONE$28 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "WorkPhone");
    private static final javax.xml.namespace.QName EMAIL$30 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Email");
    
    
    /**
     * Gets the "Title" element
     */
    public java.lang.String getTitle()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TITLE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Title" element
     */
    public com.idanalytics.products.idscore.request.Title xgetTitle()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Title target = null;
            target = (com.idanalytics.products.idscore.request.Title)get_store().find_element_user(TITLE$0, 0);
            return target;
        }
    }
    
    /**
     * True if has "Title" element
     */
    public boolean isSetTitle()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(TITLE$0) != 0;
        }
    }
    
    /**
     * Sets the "Title" element
     */
    public void setTitle(java.lang.String title)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TITLE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TITLE$0);
            }
            target.setStringValue(title);
        }
    }
    
    /**
     * Sets (as xml) the "Title" element
     */
    public void xsetTitle(com.idanalytics.products.idscore.request.Title title)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Title target = null;
            target = (com.idanalytics.products.idscore.request.Title)get_store().find_element_user(TITLE$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Title)get_store().add_element_user(TITLE$0);
            }
            target.set(title);
        }
    }
    
    /**
     * Unsets the "Title" element
     */
    public void unsetTitle()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(TITLE$0, 0);
        }
    }
    
    /**
     * Gets the "FirstName" element
     */
    public java.lang.String getFirstName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FIRSTNAME$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "FirstName" element
     */
    public com.idanalytics.products.idscore.request.Name xgetFirstName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Name target = null;
            target = (com.idanalytics.products.idscore.request.Name)get_store().find_element_user(FIRSTNAME$2, 0);
            return target;
        }
    }
    
    /**
     * True if has "FirstName" element
     */
    public boolean isSetFirstName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(FIRSTNAME$2) != 0;
        }
    }
    
    /**
     * Sets the "FirstName" element
     */
    public void setFirstName(java.lang.String firstName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FIRSTNAME$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(FIRSTNAME$2);
            }
            target.setStringValue(firstName);
        }
    }
    
    /**
     * Sets (as xml) the "FirstName" element
     */
    public void xsetFirstName(com.idanalytics.products.idscore.request.Name firstName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Name target = null;
            target = (com.idanalytics.products.idscore.request.Name)get_store().find_element_user(FIRSTNAME$2, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Name)get_store().add_element_user(FIRSTNAME$2);
            }
            target.set(firstName);
        }
    }
    
    /**
     * Unsets the "FirstName" element
     */
    public void unsetFirstName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(FIRSTNAME$2, 0);
        }
    }
    
    /**
     * Gets the "MiddleName" element
     */
    public java.lang.String getMiddleName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MIDDLENAME$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "MiddleName" element
     */
    public com.idanalytics.products.idscore.request.Name xgetMiddleName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Name target = null;
            target = (com.idanalytics.products.idscore.request.Name)get_store().find_element_user(MIDDLENAME$4, 0);
            return target;
        }
    }
    
    /**
     * True if has "MiddleName" element
     */
    public boolean isSetMiddleName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(MIDDLENAME$4) != 0;
        }
    }
    
    /**
     * Sets the "MiddleName" element
     */
    public void setMiddleName(java.lang.String middleName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MIDDLENAME$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(MIDDLENAME$4);
            }
            target.setStringValue(middleName);
        }
    }
    
    /**
     * Sets (as xml) the "MiddleName" element
     */
    public void xsetMiddleName(com.idanalytics.products.idscore.request.Name middleName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Name target = null;
            target = (com.idanalytics.products.idscore.request.Name)get_store().find_element_user(MIDDLENAME$4, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Name)get_store().add_element_user(MIDDLENAME$4);
            }
            target.set(middleName);
        }
    }
    
    /**
     * Unsets the "MiddleName" element
     */
    public void unsetMiddleName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(MIDDLENAME$4, 0);
        }
    }
    
    /**
     * Gets the "LastName" element
     */
    public java.lang.String getLastName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LASTNAME$6, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "LastName" element
     */
    public com.idanalytics.products.idscore.request.Name xgetLastName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Name target = null;
            target = (com.idanalytics.products.idscore.request.Name)get_store().find_element_user(LASTNAME$6, 0);
            return target;
        }
    }
    
    /**
     * True if has "LastName" element
     */
    public boolean isSetLastName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(LASTNAME$6) != 0;
        }
    }
    
    /**
     * Sets the "LastName" element
     */
    public void setLastName(java.lang.String lastName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LASTNAME$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(LASTNAME$6);
            }
            target.setStringValue(lastName);
        }
    }
    
    /**
     * Sets (as xml) the "LastName" element
     */
    public void xsetLastName(com.idanalytics.products.idscore.request.Name lastName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Name target = null;
            target = (com.idanalytics.products.idscore.request.Name)get_store().find_element_user(LASTNAME$6, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Name)get_store().add_element_user(LASTNAME$6);
            }
            target.set(lastName);
        }
    }
    
    /**
     * Unsets the "LastName" element
     */
    public void unsetLastName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(LASTNAME$6, 0);
        }
    }
    
    /**
     * Gets the "Suffix" element
     */
    public java.lang.String getSuffix()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SUFFIX$8, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Suffix" element
     */
    public com.idanalytics.products.common_v1.SuffixType xgetSuffix()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.SuffixType target = null;
            target = (com.idanalytics.products.common_v1.SuffixType)get_store().find_element_user(SUFFIX$8, 0);
            return target;
        }
    }
    
    /**
     * True if has "Suffix" element
     */
    public boolean isSetSuffix()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SUFFIX$8) != 0;
        }
    }
    
    /**
     * Sets the "Suffix" element
     */
    public void setSuffix(java.lang.String suffix)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SUFFIX$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SUFFIX$8);
            }
            target.setStringValue(suffix);
        }
    }
    
    /**
     * Sets (as xml) the "Suffix" element
     */
    public void xsetSuffix(com.idanalytics.products.common_v1.SuffixType suffix)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.SuffixType target = null;
            target = (com.idanalytics.products.common_v1.SuffixType)get_store().find_element_user(SUFFIX$8, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.SuffixType)get_store().add_element_user(SUFFIX$8);
            }
            target.set(suffix);
        }
    }
    
    /**
     * Unsets the "Suffix" element
     */
    public void unsetSuffix()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SUFFIX$8, 0);
        }
    }
    
    /**
     * Gets the "Company" element
     */
    public java.lang.String getCompany()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(COMPANY$10, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Company" element
     */
    public com.idanalytics.products.common_v1.NormalizedString xgetCompany()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.NormalizedString target = null;
            target = (com.idanalytics.products.common_v1.NormalizedString)get_store().find_element_user(COMPANY$10, 0);
            return target;
        }
    }
    
    /**
     * True if has "Company" element
     */
    public boolean isSetCompany()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(COMPANY$10) != 0;
        }
    }
    
    /**
     * Sets the "Company" element
     */
    public void setCompany(java.lang.String company)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(COMPANY$10, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(COMPANY$10);
            }
            target.setStringValue(company);
        }
    }
    
    /**
     * Sets (as xml) the "Company" element
     */
    public void xsetCompany(com.idanalytics.products.common_v1.NormalizedString company)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.NormalizedString target = null;
            target = (com.idanalytics.products.common_v1.NormalizedString)get_store().find_element_user(COMPANY$10, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.NormalizedString)get_store().add_element_user(COMPANY$10);
            }
            target.set(company);
        }
    }
    
    /**
     * Unsets the "Company" element
     */
    public void unsetCompany()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(COMPANY$10, 0);
        }
    }
    
    /**
     * Gets the "Address" element
     */
    public com.idanalytics.products.idscore.request.Address getAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Address target = null;
            target = (com.idanalytics.products.idscore.request.Address)get_store().find_element_user(ADDRESS$12, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "Address" element
     */
    public boolean isSetAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ADDRESS$12) != 0;
        }
    }
    
    /**
     * Sets the "Address" element
     */
    public void setAddress(com.idanalytics.products.idscore.request.Address address)
    {
        generatedSetterHelperImpl(address, ADDRESS$12, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "Address" element
     */
    public com.idanalytics.products.idscore.request.Address addNewAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Address target = null;
            target = (com.idanalytics.products.idscore.request.Address)get_store().add_element_user(ADDRESS$12);
            return target;
        }
    }
    
    /**
     * Unsets the "Address" element
     */
    public void unsetAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ADDRESS$12, 0);
        }
    }
    
    /**
     * Gets the "City" element
     */
    public java.lang.String getCity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CITY$14, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "City" element
     */
    public com.idanalytics.products.idscore.request.City xgetCity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.City target = null;
            target = (com.idanalytics.products.idscore.request.City)get_store().find_element_user(CITY$14, 0);
            return target;
        }
    }
    
    /**
     * True if has "City" element
     */
    public boolean isSetCity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CITY$14) != 0;
        }
    }
    
    /**
     * Sets the "City" element
     */
    public void setCity(java.lang.String city)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CITY$14, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CITY$14);
            }
            target.setStringValue(city);
        }
    }
    
    /**
     * Sets (as xml) the "City" element
     */
    public void xsetCity(com.idanalytics.products.idscore.request.City city)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.City target = null;
            target = (com.idanalytics.products.idscore.request.City)get_store().find_element_user(CITY$14, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.City)get_store().add_element_user(CITY$14);
            }
            target.set(city);
        }
    }
    
    /**
     * Unsets the "City" element
     */
    public void unsetCity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CITY$14, 0);
        }
    }
    
    /**
     * Gets the "State" element
     */
    public java.lang.String getState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STATE$16, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "State" element
     */
    public com.idanalytics.products.idscore.request.State xgetState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.State target = null;
            target = (com.idanalytics.products.idscore.request.State)get_store().find_element_user(STATE$16, 0);
            return target;
        }
    }
    
    /**
     * True if has "State" element
     */
    public boolean isSetState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(STATE$16) != 0;
        }
    }
    
    /**
     * Sets the "State" element
     */
    public void setState(java.lang.String state)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STATE$16, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(STATE$16);
            }
            target.setStringValue(state);
        }
    }
    
    /**
     * Sets (as xml) the "State" element
     */
    public void xsetState(com.idanalytics.products.idscore.request.State state)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.State target = null;
            target = (com.idanalytics.products.idscore.request.State)get_store().find_element_user(STATE$16, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.State)get_store().add_element_user(STATE$16);
            }
            target.set(state);
        }
    }
    
    /**
     * Unsets the "State" element
     */
    public void unsetState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(STATE$16, 0);
        }
    }
    
    /**
     * Gets the "Zip" element
     */
    public java.lang.String getZip()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ZIP$18, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Zip" element
     */
    public com.idanalytics.products.idscore.request.Zip xgetZip()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Zip target = null;
            target = (com.idanalytics.products.idscore.request.Zip)get_store().find_element_user(ZIP$18, 0);
            return target;
        }
    }
    
    /**
     * True if has "Zip" element
     */
    public boolean isSetZip()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ZIP$18) != 0;
        }
    }
    
    /**
     * Sets the "Zip" element
     */
    public void setZip(java.lang.String zip)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ZIP$18, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ZIP$18);
            }
            target.setStringValue(zip);
        }
    }
    
    /**
     * Sets (as xml) the "Zip" element
     */
    public void xsetZip(com.idanalytics.products.idscore.request.Zip zip)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Zip target = null;
            target = (com.idanalytics.products.idscore.request.Zip)get_store().find_element_user(ZIP$18, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Zip)get_store().add_element_user(ZIP$18);
            }
            target.set(zip);
        }
    }
    
    /**
     * Unsets the "Zip" element
     */
    public void unsetZip()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ZIP$18, 0);
        }
    }
    
    /**
     * Gets the "Country" element
     */
    public java.lang.String getCountry()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(COUNTRY$20, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Country" element
     */
    public com.idanalytics.products.idscore.request.Country xgetCountry()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Country target = null;
            target = (com.idanalytics.products.idscore.request.Country)get_store().find_element_user(COUNTRY$20, 0);
            return target;
        }
    }
    
    /**
     * True if has "Country" element
     */
    public boolean isSetCountry()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(COUNTRY$20) != 0;
        }
    }
    
    /**
     * Sets the "Country" element
     */
    public void setCountry(java.lang.String country)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(COUNTRY$20, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(COUNTRY$20);
            }
            target.setStringValue(country);
        }
    }
    
    /**
     * Sets (as xml) the "Country" element
     */
    public void xsetCountry(com.idanalytics.products.idscore.request.Country country)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Country target = null;
            target = (com.idanalytics.products.idscore.request.Country)get_store().find_element_user(COUNTRY$20, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Country)get_store().add_element_user(COUNTRY$20);
            }
            target.set(country);
        }
    }
    
    /**
     * Unsets the "Country" element
     */
    public void unsetCountry()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(COUNTRY$20, 0);
        }
    }
    
    /**
     * Gets the "Occupancy" element
     */
    public com.idanalytics.products.idscore.request.Occupancy.Enum getOccupancy()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(OCCUPANCY$22, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.idscore.request.Occupancy.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "Occupancy" element
     */
    public com.idanalytics.products.idscore.request.Occupancy xgetOccupancy()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Occupancy target = null;
            target = (com.idanalytics.products.idscore.request.Occupancy)get_store().find_element_user(OCCUPANCY$22, 0);
            return target;
        }
    }
    
    /**
     * True if has "Occupancy" element
     */
    public boolean isSetOccupancy()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(OCCUPANCY$22) != 0;
        }
    }
    
    /**
     * Sets the "Occupancy" element
     */
    public void setOccupancy(com.idanalytics.products.idscore.request.Occupancy.Enum occupancy)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(OCCUPANCY$22, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(OCCUPANCY$22);
            }
            target.setEnumValue(occupancy);
        }
    }
    
    /**
     * Sets (as xml) the "Occupancy" element
     */
    public void xsetOccupancy(com.idanalytics.products.idscore.request.Occupancy occupancy)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Occupancy target = null;
            target = (com.idanalytics.products.idscore.request.Occupancy)get_store().find_element_user(OCCUPANCY$22, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Occupancy)get_store().add_element_user(OCCUPANCY$22);
            }
            target.set(occupancy);
        }
    }
    
    /**
     * Unsets the "Occupancy" element
     */
    public void unsetOccupancy()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(OCCUPANCY$22, 0);
        }
    }
    
    /**
     * Gets the "HomePhone" element
     */
    public java.lang.String getHomePhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(HOMEPHONE$24, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "HomePhone" element
     */
    public com.idanalytics.products.idscore.request.Phone xgetHomePhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().find_element_user(HOMEPHONE$24, 0);
            return target;
        }
    }
    
    /**
     * True if has "HomePhone" element
     */
    public boolean isSetHomePhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(HOMEPHONE$24) != 0;
        }
    }
    
    /**
     * Sets the "HomePhone" element
     */
    public void setHomePhone(java.lang.String homePhone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(HOMEPHONE$24, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(HOMEPHONE$24);
            }
            target.setStringValue(homePhone);
        }
    }
    
    /**
     * Sets (as xml) the "HomePhone" element
     */
    public void xsetHomePhone(com.idanalytics.products.idscore.request.Phone homePhone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().find_element_user(HOMEPHONE$24, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Phone)get_store().add_element_user(HOMEPHONE$24);
            }
            target.set(homePhone);
        }
    }
    
    /**
     * Unsets the "HomePhone" element
     */
    public void unsetHomePhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(HOMEPHONE$24, 0);
        }
    }
    
    /**
     * Gets the "MobilePhone" element
     */
    public java.lang.String getMobilePhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MOBILEPHONE$26, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "MobilePhone" element
     */
    public com.idanalytics.products.idscore.request.Phone xgetMobilePhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().find_element_user(MOBILEPHONE$26, 0);
            return target;
        }
    }
    
    /**
     * True if has "MobilePhone" element
     */
    public boolean isSetMobilePhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(MOBILEPHONE$26) != 0;
        }
    }
    
    /**
     * Sets the "MobilePhone" element
     */
    public void setMobilePhone(java.lang.String mobilePhone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MOBILEPHONE$26, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(MOBILEPHONE$26);
            }
            target.setStringValue(mobilePhone);
        }
    }
    
    /**
     * Sets (as xml) the "MobilePhone" element
     */
    public void xsetMobilePhone(com.idanalytics.products.idscore.request.Phone mobilePhone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().find_element_user(MOBILEPHONE$26, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Phone)get_store().add_element_user(MOBILEPHONE$26);
            }
            target.set(mobilePhone);
        }
    }
    
    /**
     * Unsets the "MobilePhone" element
     */
    public void unsetMobilePhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(MOBILEPHONE$26, 0);
        }
    }
    
    /**
     * Gets the "WorkPhone" element
     */
    public java.lang.String getWorkPhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(WORKPHONE$28, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "WorkPhone" element
     */
    public com.idanalytics.products.idscore.request.Phone xgetWorkPhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().find_element_user(WORKPHONE$28, 0);
            return target;
        }
    }
    
    /**
     * True if has "WorkPhone" element
     */
    public boolean isSetWorkPhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(WORKPHONE$28) != 0;
        }
    }
    
    /**
     * Sets the "WorkPhone" element
     */
    public void setWorkPhone(java.lang.String workPhone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(WORKPHONE$28, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(WORKPHONE$28);
            }
            target.setStringValue(workPhone);
        }
    }
    
    /**
     * Sets (as xml) the "WorkPhone" element
     */
    public void xsetWorkPhone(com.idanalytics.products.idscore.request.Phone workPhone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().find_element_user(WORKPHONE$28, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Phone)get_store().add_element_user(WORKPHONE$28);
            }
            target.set(workPhone);
        }
    }
    
    /**
     * Unsets the "WorkPhone" element
     */
    public void unsetWorkPhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(WORKPHONE$28, 0);
        }
    }
    
    /**
     * Gets the "Email" element
     */
    public java.lang.String getEmail()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EMAIL$30, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Email" element
     */
    public com.idanalytics.products.idscore.request.Email xgetEmail()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Email target = null;
            target = (com.idanalytics.products.idscore.request.Email)get_store().find_element_user(EMAIL$30, 0);
            return target;
        }
    }
    
    /**
     * True if has "Email" element
     */
    public boolean isSetEmail()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(EMAIL$30) != 0;
        }
    }
    
    /**
     * Sets the "Email" element
     */
    public void setEmail(java.lang.String email)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EMAIL$30, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(EMAIL$30);
            }
            target.setStringValue(email);
        }
    }
    
    /**
     * Sets (as xml) the "Email" element
     */
    public void xsetEmail(com.idanalytics.products.idscore.request.Email email)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Email target = null;
            target = (com.idanalytics.products.idscore.request.Email)get_store().find_element_user(EMAIL$30, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Email)get_store().add_element_user(EMAIL$30);
            }
            target.set(email);
        }
    }
    
    /**
     * Unsets the "Email" element
     */
    public void unsetEmail()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(EMAIL$30, 0);
        }
    }
}
