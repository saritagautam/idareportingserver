/*
 * XML Type:  TimeStamp
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.TimeStamp
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML TimeStamp(@http://idanalytics.com/products/idscore/request).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.TimeStamp.
 */
public class TimeStampImpl extends org.apache.xmlbeans.impl.values.JavaGDateHolderEx implements com.idanalytics.products.idscore.request.TimeStamp
{
    private static final long serialVersionUID = 1L;
    
    public TimeStampImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected TimeStampImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
