/*
 * XML Type:  ItemInfo
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.ItemInfo
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML ItemInfo(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public class ItemInfoImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.ItemInfo
{
    private static final long serialVersionUID = 1L;
    
    public ItemInfoImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SHIPPINGCOST$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ShippingCost");
    private static final javax.xml.namespace.QName ITEMID$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ItemID");
    private static final javax.xml.namespace.QName ITEMNAME$4 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ItemName");
    private static final javax.xml.namespace.QName ITEMDESCRIPTION$6 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ItemDescription");
    private static final javax.xml.namespace.QName ITEMQUANTITY$8 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ItemQuantity");
    private static final javax.xml.namespace.QName ITEMPRICE$10 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ItemPrice");
    private static final javax.xml.namespace.QName ITEMCATEGORY$12 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ItemCategory");
    private static final javax.xml.namespace.QName ACCESSORY$14 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Accessory");
    private static final javax.xml.namespace.QName SHIPPINGDATE$16 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ShippingDate");
    private static final javax.xml.namespace.QName DELIVERYDATE$18 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "DeliveryDate");
    private static final javax.xml.namespace.QName CARDMESSAGE$20 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "CardMessage");
    private static final javax.xml.namespace.QName OCCASION$22 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Occasion");
    
    
    /**
     * Gets the "ShippingCost" element
     */
    public java.lang.Object getShippingCost()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SHIPPINGCOST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getObjectValue();
        }
    }
    
    /**
     * Gets (as xml) the "ShippingCost" element
     */
    public com.idanalytics.products.common_v1.OptionalDecimal xgetShippingCost()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.OptionalDecimal target = null;
            target = (com.idanalytics.products.common_v1.OptionalDecimal)get_store().find_element_user(SHIPPINGCOST$0, 0);
            return target;
        }
    }
    
    /**
     * True if has "ShippingCost" element
     */
    public boolean isSetShippingCost()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SHIPPINGCOST$0) != 0;
        }
    }
    
    /**
     * Sets the "ShippingCost" element
     */
    public void setShippingCost(java.lang.Object shippingCost)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SHIPPINGCOST$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SHIPPINGCOST$0);
            }
            target.setObjectValue(shippingCost);
        }
    }
    
    /**
     * Sets (as xml) the "ShippingCost" element
     */
    public void xsetShippingCost(com.idanalytics.products.common_v1.OptionalDecimal shippingCost)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.OptionalDecimal target = null;
            target = (com.idanalytics.products.common_v1.OptionalDecimal)get_store().find_element_user(SHIPPINGCOST$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.OptionalDecimal)get_store().add_element_user(SHIPPINGCOST$0);
            }
            target.set(shippingCost);
        }
    }
    
    /**
     * Unsets the "ShippingCost" element
     */
    public void unsetShippingCost()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SHIPPINGCOST$0, 0);
        }
    }
    
    /**
     * Gets the "ItemID" element
     */
    public java.lang.String getItemID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ITEMID$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "ItemID" element
     */
    public com.idanalytics.products.common_v1.NormalizedString xgetItemID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.NormalizedString target = null;
            target = (com.idanalytics.products.common_v1.NormalizedString)get_store().find_element_user(ITEMID$2, 0);
            return target;
        }
    }
    
    /**
     * True if has "ItemID" element
     */
    public boolean isSetItemID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ITEMID$2) != 0;
        }
    }
    
    /**
     * Sets the "ItemID" element
     */
    public void setItemID(java.lang.String itemID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ITEMID$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ITEMID$2);
            }
            target.setStringValue(itemID);
        }
    }
    
    /**
     * Sets (as xml) the "ItemID" element
     */
    public void xsetItemID(com.idanalytics.products.common_v1.NormalizedString itemID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.NormalizedString target = null;
            target = (com.idanalytics.products.common_v1.NormalizedString)get_store().find_element_user(ITEMID$2, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.NormalizedString)get_store().add_element_user(ITEMID$2);
            }
            target.set(itemID);
        }
    }
    
    /**
     * Unsets the "ItemID" element
     */
    public void unsetItemID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ITEMID$2, 0);
        }
    }
    
    /**
     * Gets the "ItemName" element
     */
    public java.lang.String getItemName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ITEMNAME$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "ItemName" element
     */
    public com.idanalytics.products.common_v1.NormalizedString xgetItemName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.NormalizedString target = null;
            target = (com.idanalytics.products.common_v1.NormalizedString)get_store().find_element_user(ITEMNAME$4, 0);
            return target;
        }
    }
    
    /**
     * True if has "ItemName" element
     */
    public boolean isSetItemName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ITEMNAME$4) != 0;
        }
    }
    
    /**
     * Sets the "ItemName" element
     */
    public void setItemName(java.lang.String itemName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ITEMNAME$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ITEMNAME$4);
            }
            target.setStringValue(itemName);
        }
    }
    
    /**
     * Sets (as xml) the "ItemName" element
     */
    public void xsetItemName(com.idanalytics.products.common_v1.NormalizedString itemName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.NormalizedString target = null;
            target = (com.idanalytics.products.common_v1.NormalizedString)get_store().find_element_user(ITEMNAME$4, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.NormalizedString)get_store().add_element_user(ITEMNAME$4);
            }
            target.set(itemName);
        }
    }
    
    /**
     * Unsets the "ItemName" element
     */
    public void unsetItemName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ITEMNAME$4, 0);
        }
    }
    
    /**
     * Gets the "ItemDescription" element
     */
    public java.lang.String getItemDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ITEMDESCRIPTION$6, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "ItemDescription" element
     */
    public com.idanalytics.products.common_v1.NormalizedString xgetItemDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.NormalizedString target = null;
            target = (com.idanalytics.products.common_v1.NormalizedString)get_store().find_element_user(ITEMDESCRIPTION$6, 0);
            return target;
        }
    }
    
    /**
     * True if has "ItemDescription" element
     */
    public boolean isSetItemDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ITEMDESCRIPTION$6) != 0;
        }
    }
    
    /**
     * Sets the "ItemDescription" element
     */
    public void setItemDescription(java.lang.String itemDescription)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ITEMDESCRIPTION$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ITEMDESCRIPTION$6);
            }
            target.setStringValue(itemDescription);
        }
    }
    
    /**
     * Sets (as xml) the "ItemDescription" element
     */
    public void xsetItemDescription(com.idanalytics.products.common_v1.NormalizedString itemDescription)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.NormalizedString target = null;
            target = (com.idanalytics.products.common_v1.NormalizedString)get_store().find_element_user(ITEMDESCRIPTION$6, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.NormalizedString)get_store().add_element_user(ITEMDESCRIPTION$6);
            }
            target.set(itemDescription);
        }
    }
    
    /**
     * Unsets the "ItemDescription" element
     */
    public void unsetItemDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ITEMDESCRIPTION$6, 0);
        }
    }
    
    /**
     * Gets the "ItemQuantity" element
     */
    public java.lang.Object getItemQuantity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ITEMQUANTITY$8, 0);
            if (target == null)
            {
                return null;
            }
            return target.getObjectValue();
        }
    }
    
    /**
     * Gets (as xml) the "ItemQuantity" element
     */
    public com.idanalytics.products.common_v1.OptionalDecimal xgetItemQuantity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.OptionalDecimal target = null;
            target = (com.idanalytics.products.common_v1.OptionalDecimal)get_store().find_element_user(ITEMQUANTITY$8, 0);
            return target;
        }
    }
    
    /**
     * True if has "ItemQuantity" element
     */
    public boolean isSetItemQuantity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ITEMQUANTITY$8) != 0;
        }
    }
    
    /**
     * Sets the "ItemQuantity" element
     */
    public void setItemQuantity(java.lang.Object itemQuantity)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ITEMQUANTITY$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ITEMQUANTITY$8);
            }
            target.setObjectValue(itemQuantity);
        }
    }
    
    /**
     * Sets (as xml) the "ItemQuantity" element
     */
    public void xsetItemQuantity(com.idanalytics.products.common_v1.OptionalDecimal itemQuantity)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.OptionalDecimal target = null;
            target = (com.idanalytics.products.common_v1.OptionalDecimal)get_store().find_element_user(ITEMQUANTITY$8, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.OptionalDecimal)get_store().add_element_user(ITEMQUANTITY$8);
            }
            target.set(itemQuantity);
        }
    }
    
    /**
     * Unsets the "ItemQuantity" element
     */
    public void unsetItemQuantity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ITEMQUANTITY$8, 0);
        }
    }
    
    /**
     * Gets the "ItemPrice" element
     */
    public java.lang.Object getItemPrice()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ITEMPRICE$10, 0);
            if (target == null)
            {
                return null;
            }
            return target.getObjectValue();
        }
    }
    
    /**
     * Gets (as xml) the "ItemPrice" element
     */
    public com.idanalytics.products.common_v1.OptionalDecimal xgetItemPrice()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.OptionalDecimal target = null;
            target = (com.idanalytics.products.common_v1.OptionalDecimal)get_store().find_element_user(ITEMPRICE$10, 0);
            return target;
        }
    }
    
    /**
     * True if has "ItemPrice" element
     */
    public boolean isSetItemPrice()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ITEMPRICE$10) != 0;
        }
    }
    
    /**
     * Sets the "ItemPrice" element
     */
    public void setItemPrice(java.lang.Object itemPrice)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ITEMPRICE$10, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ITEMPRICE$10);
            }
            target.setObjectValue(itemPrice);
        }
    }
    
    /**
     * Sets (as xml) the "ItemPrice" element
     */
    public void xsetItemPrice(com.idanalytics.products.common_v1.OptionalDecimal itemPrice)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.OptionalDecimal target = null;
            target = (com.idanalytics.products.common_v1.OptionalDecimal)get_store().find_element_user(ITEMPRICE$10, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.OptionalDecimal)get_store().add_element_user(ITEMPRICE$10);
            }
            target.set(itemPrice);
        }
    }
    
    /**
     * Unsets the "ItemPrice" element
     */
    public void unsetItemPrice()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ITEMPRICE$10, 0);
        }
    }
    
    /**
     * Gets the "ItemCategory" element
     */
    public java.lang.String getItemCategory()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ITEMCATEGORY$12, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "ItemCategory" element
     */
    public com.idanalytics.products.common_v1.NormalizedString xgetItemCategory()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.NormalizedString target = null;
            target = (com.idanalytics.products.common_v1.NormalizedString)get_store().find_element_user(ITEMCATEGORY$12, 0);
            return target;
        }
    }
    
    /**
     * True if has "ItemCategory" element
     */
    public boolean isSetItemCategory()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ITEMCATEGORY$12) != 0;
        }
    }
    
    /**
     * Sets the "ItemCategory" element
     */
    public void setItemCategory(java.lang.String itemCategory)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ITEMCATEGORY$12, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ITEMCATEGORY$12);
            }
            target.setStringValue(itemCategory);
        }
    }
    
    /**
     * Sets (as xml) the "ItemCategory" element
     */
    public void xsetItemCategory(com.idanalytics.products.common_v1.NormalizedString itemCategory)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.NormalizedString target = null;
            target = (com.idanalytics.products.common_v1.NormalizedString)get_store().find_element_user(ITEMCATEGORY$12, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.NormalizedString)get_store().add_element_user(ITEMCATEGORY$12);
            }
            target.set(itemCategory);
        }
    }
    
    /**
     * Unsets the "ItemCategory" element
     */
    public void unsetItemCategory()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ITEMCATEGORY$12, 0);
        }
    }
    
    /**
     * Gets the "Accessory" element
     */
    public java.lang.String getAccessory()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ACCESSORY$14, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Accessory" element
     */
    public com.idanalytics.products.idscore.request.Accessory xgetAccessory()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Accessory target = null;
            target = (com.idanalytics.products.idscore.request.Accessory)get_store().find_element_user(ACCESSORY$14, 0);
            return target;
        }
    }
    
    /**
     * True if has "Accessory" element
     */
    public boolean isSetAccessory()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ACCESSORY$14) != 0;
        }
    }
    
    /**
     * Sets the "Accessory" element
     */
    public void setAccessory(java.lang.String accessory)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ACCESSORY$14, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ACCESSORY$14);
            }
            target.setStringValue(accessory);
        }
    }
    
    /**
     * Sets (as xml) the "Accessory" element
     */
    public void xsetAccessory(com.idanalytics.products.idscore.request.Accessory accessory)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Accessory target = null;
            target = (com.idanalytics.products.idscore.request.Accessory)get_store().find_element_user(ACCESSORY$14, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Accessory)get_store().add_element_user(ACCESSORY$14);
            }
            target.set(accessory);
        }
    }
    
    /**
     * Unsets the "Accessory" element
     */
    public void unsetAccessory()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ACCESSORY$14, 0);
        }
    }
    
    /**
     * Gets the "ShippingDate" element
     */
    public java.lang.Object getShippingDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SHIPPINGDATE$16, 0);
            if (target == null)
            {
                return null;
            }
            return target.getObjectValue();
        }
    }
    
    /**
     * Gets (as xml) the "ShippingDate" element
     */
    public com.idanalytics.products.common_v1.OptionalDate xgetShippingDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.OptionalDate target = null;
            target = (com.idanalytics.products.common_v1.OptionalDate)get_store().find_element_user(SHIPPINGDATE$16, 0);
            return target;
        }
    }
    
    /**
     * True if has "ShippingDate" element
     */
    public boolean isSetShippingDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SHIPPINGDATE$16) != 0;
        }
    }
    
    /**
     * Sets the "ShippingDate" element
     */
    public void setShippingDate(java.lang.Object shippingDate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SHIPPINGDATE$16, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SHIPPINGDATE$16);
            }
            target.setObjectValue(shippingDate);
        }
    }
    
    /**
     * Sets (as xml) the "ShippingDate" element
     */
    public void xsetShippingDate(com.idanalytics.products.common_v1.OptionalDate shippingDate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.OptionalDate target = null;
            target = (com.idanalytics.products.common_v1.OptionalDate)get_store().find_element_user(SHIPPINGDATE$16, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.OptionalDate)get_store().add_element_user(SHIPPINGDATE$16);
            }
            target.set(shippingDate);
        }
    }
    
    /**
     * Unsets the "ShippingDate" element
     */
    public void unsetShippingDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SHIPPINGDATE$16, 0);
        }
    }
    
    /**
     * Gets the "DeliveryDate" element
     */
    public java.lang.Object getDeliveryDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DELIVERYDATE$18, 0);
            if (target == null)
            {
                return null;
            }
            return target.getObjectValue();
        }
    }
    
    /**
     * Gets (as xml) the "DeliveryDate" element
     */
    public com.idanalytics.products.common_v1.OptionalDate xgetDeliveryDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.OptionalDate target = null;
            target = (com.idanalytics.products.common_v1.OptionalDate)get_store().find_element_user(DELIVERYDATE$18, 0);
            return target;
        }
    }
    
    /**
     * True if has "DeliveryDate" element
     */
    public boolean isSetDeliveryDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(DELIVERYDATE$18) != 0;
        }
    }
    
    /**
     * Sets the "DeliveryDate" element
     */
    public void setDeliveryDate(java.lang.Object deliveryDate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DELIVERYDATE$18, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DELIVERYDATE$18);
            }
            target.setObjectValue(deliveryDate);
        }
    }
    
    /**
     * Sets (as xml) the "DeliveryDate" element
     */
    public void xsetDeliveryDate(com.idanalytics.products.common_v1.OptionalDate deliveryDate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.OptionalDate target = null;
            target = (com.idanalytics.products.common_v1.OptionalDate)get_store().find_element_user(DELIVERYDATE$18, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.OptionalDate)get_store().add_element_user(DELIVERYDATE$18);
            }
            target.set(deliveryDate);
        }
    }
    
    /**
     * Unsets the "DeliveryDate" element
     */
    public void unsetDeliveryDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(DELIVERYDATE$18, 0);
        }
    }
    
    /**
     * Gets the "CardMessage" element
     */
    public java.lang.String getCardMessage()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CARDMESSAGE$20, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "CardMessage" element
     */
    public org.apache.xmlbeans.XmlString xgetCardMessage()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CARDMESSAGE$20, 0);
            return target;
        }
    }
    
    /**
     * True if has "CardMessage" element
     */
    public boolean isSetCardMessage()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CARDMESSAGE$20) != 0;
        }
    }
    
    /**
     * Sets the "CardMessage" element
     */
    public void setCardMessage(java.lang.String cardMessage)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CARDMESSAGE$20, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CARDMESSAGE$20);
            }
            target.setStringValue(cardMessage);
        }
    }
    
    /**
     * Sets (as xml) the "CardMessage" element
     */
    public void xsetCardMessage(org.apache.xmlbeans.XmlString cardMessage)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(CARDMESSAGE$20, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(CARDMESSAGE$20);
            }
            target.set(cardMessage);
        }
    }
    
    /**
     * Unsets the "CardMessage" element
     */
    public void unsetCardMessage()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CARDMESSAGE$20, 0);
        }
    }
    
    /**
     * Gets the "Occasion" element
     */
    public java.lang.String getOccasion()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(OCCASION$22, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Occasion" element
     */
    public org.apache.xmlbeans.XmlString xgetOccasion()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(OCCASION$22, 0);
            return target;
        }
    }
    
    /**
     * True if has "Occasion" element
     */
    public boolean isSetOccasion()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(OCCASION$22) != 0;
        }
    }
    
    /**
     * Sets the "Occasion" element
     */
    public void setOccasion(java.lang.String occasion)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(OCCASION$22, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(OCCASION$22);
            }
            target.setStringValue(occasion);
        }
    }
    
    /**
     * Sets (as xml) the "Occasion" element
     */
    public void xsetOccasion(org.apache.xmlbeans.XmlString occasion)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(OCCASION$22, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(OCCASION$22);
            }
            target.set(occasion);
        }
    }
    
    /**
     * Unsets the "Occasion" element
     */
    public void unsetOccasion()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(OCCASION$22, 0);
        }
    }
}
