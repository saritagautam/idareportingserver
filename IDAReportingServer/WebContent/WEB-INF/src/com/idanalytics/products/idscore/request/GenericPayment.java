/*
 * XML Type:  GenericPayment
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.GenericPayment
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request;


/**
 * An XML GenericPayment(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public interface GenericPayment extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GenericPayment.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("genericpayment1a07type");
    
    /**
     * Gets the "PaymentCompany" element
     */
    java.lang.String getPaymentCompany();
    
    /**
     * Gets (as xml) the "PaymentCompany" element
     */
    com.idanalytics.products.common_v1.NormalizedString xgetPaymentCompany();
    
    /**
     * True if has "PaymentCompany" element
     */
    boolean isSetPaymentCompany();
    
    /**
     * Sets the "PaymentCompany" element
     */
    void setPaymentCompany(java.lang.String paymentCompany);
    
    /**
     * Sets (as xml) the "PaymentCompany" element
     */
    void xsetPaymentCompany(com.idanalytics.products.common_v1.NormalizedString paymentCompany);
    
    /**
     * Unsets the "PaymentCompany" element
     */
    void unsetPaymentCompany();
    
    /**
     * Gets the "AccountID" element
     */
    java.lang.String getAccountID();
    
    /**
     * Gets (as xml) the "AccountID" element
     */
    com.idanalytics.products.common_v1.NormalizedString xgetAccountID();
    
    /**
     * True if has "AccountID" element
     */
    boolean isSetAccountID();
    
    /**
     * Sets the "AccountID" element
     */
    void setAccountID(java.lang.String accountID);
    
    /**
     * Sets (as xml) the "AccountID" element
     */
    void xsetAccountID(com.idanalytics.products.common_v1.NormalizedString accountID);
    
    /**
     * Unsets the "AccountID" element
     */
    void unsetAccountID();
    
    /**
     * Gets the "TransactionID" element
     */
    java.lang.String getTransactionID();
    
    /**
     * Gets (as xml) the "TransactionID" element
     */
    com.idanalytics.products.common_v1.NormalizedString xgetTransactionID();
    
    /**
     * True if has "TransactionID" element
     */
    boolean isSetTransactionID();
    
    /**
     * Sets the "TransactionID" element
     */
    void setTransactionID(java.lang.String transactionID);
    
    /**
     * Sets (as xml) the "TransactionID" element
     */
    void xsetTransactionID(com.idanalytics.products.common_v1.NormalizedString transactionID);
    
    /**
     * Unsets the "TransactionID" element
     */
    void unsetTransactionID();
    
    /**
     * Gets the "BankRouting" element
     */
    java.lang.String getBankRouting();
    
    /**
     * Gets (as xml) the "BankRouting" element
     */
    com.idanalytics.products.common_v1.NormalizedString xgetBankRouting();
    
    /**
     * True if has "BankRouting" element
     */
    boolean isSetBankRouting();
    
    /**
     * Sets the "BankRouting" element
     */
    void setBankRouting(java.lang.String bankRouting);
    
    /**
     * Sets (as xml) the "BankRouting" element
     */
    void xsetBankRouting(com.idanalytics.products.common_v1.NormalizedString bankRouting);
    
    /**
     * Unsets the "BankRouting" element
     */
    void unsetBankRouting();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.idscore.request.GenericPayment newInstance() {
          return (com.idanalytics.products.idscore.request.GenericPayment) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.idscore.request.GenericPayment newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.idscore.request.GenericPayment) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.idscore.request.GenericPayment parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.GenericPayment) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.idscore.request.GenericPayment parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.GenericPayment) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.idscore.request.GenericPayment parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.GenericPayment) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.idscore.request.GenericPayment parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.GenericPayment) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.idscore.request.GenericPayment parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.GenericPayment) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.idscore.request.GenericPayment parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.GenericPayment) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.idscore.request.GenericPayment parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.GenericPayment) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.idscore.request.GenericPayment parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.GenericPayment) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.idscore.request.GenericPayment parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.GenericPayment) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.idscore.request.GenericPayment parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.GenericPayment) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.idscore.request.GenericPayment parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.GenericPayment) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.idscore.request.GenericPayment parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.GenericPayment) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.idscore.request.GenericPayment parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.GenericPayment) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.idscore.request.GenericPayment parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.GenericPayment) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.request.GenericPayment parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.request.GenericPayment) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.request.GenericPayment parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.request.GenericPayment) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
