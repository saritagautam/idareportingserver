/*
 * XML Type:  SurveyResults
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.SurveyResults
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML SurveyResults(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public class SurveyResultsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.SurveyResults
{
    private static final long serialVersionUID = 1L;
    
    public SurveyResultsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName QUESTIONRESPONSE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "QuestionResponse");
    private static final javax.xml.namespace.QName SOURCE$2 = 
        new javax.xml.namespace.QName("", "source");
    
    
    /**
     * Gets array of all "QuestionResponse" elements
     */
    public com.idanalytics.products.idscore.request.QuestionResponse[] getQuestionResponseArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(QUESTIONRESPONSE$0, targetList);
            com.idanalytics.products.idscore.request.QuestionResponse[] result = new com.idanalytics.products.idscore.request.QuestionResponse[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "QuestionResponse" element
     */
    public com.idanalytics.products.idscore.request.QuestionResponse getQuestionResponseArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.QuestionResponse target = null;
            target = (com.idanalytics.products.idscore.request.QuestionResponse)get_store().find_element_user(QUESTIONRESPONSE$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "QuestionResponse" element
     */
    public int sizeOfQuestionResponseArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(QUESTIONRESPONSE$0);
        }
    }
    
    /**
     * Sets array of all "QuestionResponse" element  WARNING: This method is not atomicaly synchronized.
     */
    public void setQuestionResponseArray(com.idanalytics.products.idscore.request.QuestionResponse[] questionResponseArray)
    {
        check_orphaned();
        arraySetterHelper(questionResponseArray, QUESTIONRESPONSE$0);
    }
    
    /**
     * Sets ith "QuestionResponse" element
     */
    public void setQuestionResponseArray(int i, com.idanalytics.products.idscore.request.QuestionResponse questionResponse)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.QuestionResponse target = null;
            target = (com.idanalytics.products.idscore.request.QuestionResponse)get_store().find_element_user(QUESTIONRESPONSE$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(questionResponse);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "QuestionResponse" element
     */
    public com.idanalytics.products.idscore.request.QuestionResponse insertNewQuestionResponse(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.QuestionResponse target = null;
            target = (com.idanalytics.products.idscore.request.QuestionResponse)get_store().insert_element_user(QUESTIONRESPONSE$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "QuestionResponse" element
     */
    public com.idanalytics.products.idscore.request.QuestionResponse addNewQuestionResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.QuestionResponse target = null;
            target = (com.idanalytics.products.idscore.request.QuestionResponse)get_store().add_element_user(QUESTIONRESPONSE$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "QuestionResponse" element
     */
    public void removeQuestionResponse(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(QUESTIONRESPONSE$0, i);
        }
    }
    
    /**
     * Gets the "source" attribute
     */
    public java.lang.String getSource()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(SOURCE$2);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "source" attribute
     */
    public org.apache.xmlbeans.XmlString xgetSource()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_attribute_user(SOURCE$2);
            return target;
        }
    }
    
    /**
     * Sets the "source" attribute
     */
    public void setSource(java.lang.String source)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(SOURCE$2);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(SOURCE$2);
            }
            target.setStringValue(source);
        }
    }
    
    /**
     * Sets (as xml) the "source" attribute
     */
    public void xsetSource(org.apache.xmlbeans.XmlString source)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_attribute_user(SOURCE$2);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_attribute_user(SOURCE$2);
            }
            target.set(source);
        }
    }
}
