/*
 * XML Type:  Score
 * Namespace: http://idanalytics.com/products/idscore/result.v31
 * Java type: com.idanalytics.products.idscore.result_v31.Score
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.result_v31;


/**
 * An XML Score(@http://idanalytics.com/products/idscore/result.v31).
 *
 * This is a complex type.
 */
public interface Score extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Score.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("score7b3atype");
    
    /**
     * Gets the "IDScore" element
     */
    java.lang.String getIDScore();
    
    /**
     * Gets (as xml) the "IDScore" element
     */
    com.idanalytics.products.idscore.result_v31.IDScoreDocument.IDScore xgetIDScore();
    
    /**
     * Sets the "IDScore" element
     */
    void setIDScore(java.lang.String idScore);
    
    /**
     * Sets (as xml) the "IDScore" element
     */
    void xsetIDScore(com.idanalytics.products.idscore.result_v31.IDScoreDocument.IDScore idScore);
    
    /**
     * Gets the "ScoreBandIndicator" element
     */
    com.idanalytics.products.idscore.result_v31.ScoreBandIndicatorDocument.ScoreBandIndicator.Enum getScoreBandIndicator();
    
    /**
     * Gets (as xml) the "ScoreBandIndicator" element
     */
    com.idanalytics.products.idscore.result_v31.ScoreBandIndicatorDocument.ScoreBandIndicator xgetScoreBandIndicator();
    
    /**
     * True if has "ScoreBandIndicator" element
     */
    boolean isSetScoreBandIndicator();
    
    /**
     * Sets the "ScoreBandIndicator" element
     */
    void setScoreBandIndicator(com.idanalytics.products.idscore.result_v31.ScoreBandIndicatorDocument.ScoreBandIndicator.Enum scoreBandIndicator);
    
    /**
     * Sets (as xml) the "ScoreBandIndicator" element
     */
    void xsetScoreBandIndicator(com.idanalytics.products.idscore.result_v31.ScoreBandIndicatorDocument.ScoreBandIndicator scoreBandIndicator);
    
    /**
     * Unsets the "ScoreBandIndicator" element
     */
    void unsetScoreBandIndicator();
    
    /**
     * Gets the "ScoreDeltaIndicator" element
     */
    com.idanalytics.products.idscore.result_v31.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator.Enum getScoreDeltaIndicator();
    
    /**
     * Gets (as xml) the "ScoreDeltaIndicator" element
     */
    com.idanalytics.products.idscore.result_v31.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator xgetScoreDeltaIndicator();
    
    /**
     * True if has "ScoreDeltaIndicator" element
     */
    boolean isSetScoreDeltaIndicator();
    
    /**
     * Sets the "ScoreDeltaIndicator" element
     */
    void setScoreDeltaIndicator(com.idanalytics.products.idscore.result_v31.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator.Enum scoreDeltaIndicator);
    
    /**
     * Sets (as xml) the "ScoreDeltaIndicator" element
     */
    void xsetScoreDeltaIndicator(com.idanalytics.products.idscore.result_v31.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator scoreDeltaIndicator);
    
    /**
     * Unsets the "ScoreDeltaIndicator" element
     */
    void unsetScoreDeltaIndicator();
    
    /**
     * Gets array of all "IDScoreResultCode" elements
     */
    com.idanalytics.products.idscore.result_v31.IDScoreResultCode[] getIDScoreResultCodeArray();
    
    /**
     * Gets ith "IDScoreResultCode" element
     */
    com.idanalytics.products.idscore.result_v31.IDScoreResultCode getIDScoreResultCodeArray(int i);
    
    /**
     * Returns number of "IDScoreResultCode" element
     */
    int sizeOfIDScoreResultCodeArray();
    
    /**
     * Sets array of all "IDScoreResultCode" element
     */
    void setIDScoreResultCodeArray(com.idanalytics.products.idscore.result_v31.IDScoreResultCode[] idScoreResultCodeArray);
    
    /**
     * Sets ith "IDScoreResultCode" element
     */
    void setIDScoreResultCodeArray(int i, com.idanalytics.products.idscore.result_v31.IDScoreResultCode idScoreResultCode);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "IDScoreResultCode" element
     */
    com.idanalytics.products.idscore.result_v31.IDScoreResultCode insertNewIDScoreResultCode(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "IDScoreResultCode" element
     */
    com.idanalytics.products.idscore.result_v31.IDScoreResultCode addNewIDScoreResultCode();
    
    /**
     * Removes the ith "IDScoreResultCode" element
     */
    void removeIDScoreResultCode(int i);
    
    /**
     * Gets the "scoreType" attribute
     */
    org.apache.xmlbeans.XmlAnySimpleType getScoreType();
    
    /**
     * Sets the "scoreType" attribute
     */
    void setScoreType(org.apache.xmlbeans.XmlAnySimpleType scoreType);
    
    /**
     * Appends and returns a new empty "scoreType" attribute
     */
    org.apache.xmlbeans.XmlAnySimpleType addNewScoreType();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.idscore.result_v31.Score newInstance() {
          return (com.idanalytics.products.idscore.result_v31.Score) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.idscore.result_v31.Score newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.idscore.result_v31.Score) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.idscore.result_v31.Score parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result_v31.Score) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.idscore.result_v31.Score parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result_v31.Score) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.idscore.result_v31.Score parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result_v31.Score) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.idscore.result_v31.Score parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result_v31.Score) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.idscore.result_v31.Score parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result_v31.Score) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.idscore.result_v31.Score parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result_v31.Score) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.idscore.result_v31.Score parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result_v31.Score) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.idscore.result_v31.Score parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result_v31.Score) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.idscore.result_v31.Score parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result_v31.Score) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.idscore.result_v31.Score parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result_v31.Score) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.idscore.result_v31.Score parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result_v31.Score) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.idscore.result_v31.Score parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result_v31.Score) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.idscore.result_v31.Score parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result_v31.Score) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.idscore.result_v31.Score parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result_v31.Score) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.result_v31.Score parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.result_v31.Score) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.result_v31.Score parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.result_v31.Score) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
