/*
 * XML Type:  ShippingIdentity
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.ShippingIdentity
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML ShippingIdentity(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public class ShippingIdentityImpl extends com.idanalytics.products.idscore.request.impl.CommerceIdentityImpl implements com.idanalytics.products.idscore.request.ShippingIdentity
{
    private static final long serialVersionUID = 1L;
    
    public ShippingIdentityImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    
}
