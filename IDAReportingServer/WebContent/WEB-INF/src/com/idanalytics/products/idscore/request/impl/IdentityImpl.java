/*
 * XML Type:  Identity
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.Identity
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML Identity(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public class IdentityImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.Identity
{
    private static final long serialVersionUID = 1L;
    
    public IdentityImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SSN$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "SSN");
    private static final javax.xml.namespace.QName SSNLAST4$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "SSNLast4");
    private static final javax.xml.namespace.QName SSNFIRST8$4 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "SSNFirst8");
    private static final javax.xml.namespace.QName SSNLAST8$6 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "SSNLast8");
    private static final javax.xml.namespace.QName TITLE$8 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Title");
    private static final javax.xml.namespace.QName FIRSTNAME$10 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "FirstName");
    private static final javax.xml.namespace.QName MIDDLENAME$12 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "MiddleName");
    private static final javax.xml.namespace.QName LASTNAME$14 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "LastName");
    private static final javax.xml.namespace.QName SUFFIX$16 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Suffix");
    private static final javax.xml.namespace.QName DOB$18 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "DOB");
    private static final javax.xml.namespace.QName GENDER$20 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Gender");
    private static final javax.xml.namespace.QName PLACEOFBIRTH$22 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PlaceOfBirth");
    private static final javax.xml.namespace.QName MOTHERSMAIDEN$24 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "MothersMaiden");
    private static final javax.xml.namespace.QName COMPANY$26 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Company");
    private static final javax.xml.namespace.QName ADDRESS$28 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Address");
    private static final javax.xml.namespace.QName PARSEDADDRESS$30 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ParsedAddress");
    private static final javax.xml.namespace.QName CITY$32 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "City");
    private static final javax.xml.namespace.QName STATE$34 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "State");
    private static final javax.xml.namespace.QName ZIP$36 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Zip");
    private static final javax.xml.namespace.QName COUNTRY$38 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Country");
    private static final javax.xml.namespace.QName TIMEATRESIDENCE$40 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "TimeAtResidence");
    private static final javax.xml.namespace.QName ADDRESSSINCE$42 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "AddressSince");
    private static final javax.xml.namespace.QName OCCUPANCY$44 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Occupancy");
    private static final javax.xml.namespace.QName PHYSICALADDRESS$46 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PhysicalAddress");
    private static final javax.xml.namespace.QName PARSEDPHYSICALADDRESS$48 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ParsedPhysicalAddress");
    private static final javax.xml.namespace.QName PHYSICALCITY$50 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PhysicalCity");
    private static final javax.xml.namespace.QName PHYSICALSTATE$52 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PhysicalState");
    private static final javax.xml.namespace.QName PHYSICALZIP$54 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PhysicalZip");
    private static final javax.xml.namespace.QName PHYSICALCOUNTRY$56 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PhysicalCountry");
    private static final javax.xml.namespace.QName PREVSSN$58 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrevSSN");
    private static final javax.xml.namespace.QName PREVTITLE$60 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrevTitle");
    private static final javax.xml.namespace.QName PREVFIRSTNAME$62 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrevFirstName");
    private static final javax.xml.namespace.QName PREVMIDDLENAME$64 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrevMiddleName");
    private static final javax.xml.namespace.QName PREVLASTNAME$66 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrevLastName");
    private static final javax.xml.namespace.QName PREVSUFFIX$68 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrevSuffix");
    private static final javax.xml.namespace.QName PREVDOB$70 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrevDOB");
    private static final javax.xml.namespace.QName PREVADDRESS$72 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrevAddress");
    private static final javax.xml.namespace.QName PARSEDPREVADDRESS$74 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ParsedPrevAddress");
    private static final javax.xml.namespace.QName PREVCITY$76 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrevCity");
    private static final javax.xml.namespace.QName PREVSTATE$78 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrevState");
    private static final javax.xml.namespace.QName PREVZIP$80 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrevZip");
    private static final javax.xml.namespace.QName PREVCOUNTRY$82 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrevCountry");
    private static final javax.xml.namespace.QName PREVADDRESSSINCE$84 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrevAddressSince");
    private static final javax.xml.namespace.QName PREVOCCUPANCY$86 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrevOccupancy");
    private static final javax.xml.namespace.QName HOMEPHONE$88 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "HomePhone");
    private static final javax.xml.namespace.QName HOMEPHONE2$90 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "HomePhone2");
    private static final javax.xml.namespace.QName MOBILEPHONE$92 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "MobilePhone");
    private static final javax.xml.namespace.QName WORKPHONE$94 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "WorkPhone");
    private static final javax.xml.namespace.QName PREVHOMEPHONE$96 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrevHomePhone");
    private static final javax.xml.namespace.QName PREVHOMEPHONE2$98 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrevHomePhone2");
    private static final javax.xml.namespace.QName PREVMOBILEPHONE$100 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrevMobilePhone");
    private static final javax.xml.namespace.QName PREVWORKPHONE$102 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrevWorkPhone");
    private static final javax.xml.namespace.QName EMAIL$104 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Email");
    private static final javax.xml.namespace.QName PREVEMAIL$106 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrevEmail");
    private static final javax.xml.namespace.QName IDTYPE$108 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "IDType");
    private static final javax.xml.namespace.QName IDORIGIN$110 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "IDOrigin");
    private static final javax.xml.namespace.QName IDNUMBER$112 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "IDNumber");
    private static final javax.xml.namespace.QName EMPLOYMENT$114 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Employment");
    
    
    /**
     * Gets the "SSN" element
     */
    public java.lang.String getSSN()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SSN$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "SSN" element
     */
    public com.idanalytics.products.idscore.request.SSN xgetSSN()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.SSN target = null;
            target = (com.idanalytics.products.idscore.request.SSN)get_store().find_element_user(SSN$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "SSN" element
     */
    public void setSSN(java.lang.String ssn)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SSN$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SSN$0);
            }
            target.setStringValue(ssn);
        }
    }
    
    /**
     * Sets (as xml) the "SSN" element
     */
    public void xsetSSN(com.idanalytics.products.idscore.request.SSN ssn)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.SSN target = null;
            target = (com.idanalytics.products.idscore.request.SSN)get_store().find_element_user(SSN$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.SSN)get_store().add_element_user(SSN$0);
            }
            target.set(ssn);
        }
    }
    
    /**
     * Gets the "SSNLast4" element
     */
    public java.lang.String getSSNLast4()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SSNLAST4$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "SSNLast4" element
     */
    public com.idanalytics.products.idscore.request.SSNLast4 xgetSSNLast4()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.SSNLast4 target = null;
            target = (com.idanalytics.products.idscore.request.SSNLast4)get_store().find_element_user(SSNLAST4$2, 0);
            return target;
        }
    }
    
    /**
     * True if has "SSNLast4" element
     */
    public boolean isSetSSNLast4()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SSNLAST4$2) != 0;
        }
    }
    
    /**
     * Sets the "SSNLast4" element
     */
    public void setSSNLast4(java.lang.String ssnLast4)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SSNLAST4$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SSNLAST4$2);
            }
            target.setStringValue(ssnLast4);
        }
    }
    
    /**
     * Sets (as xml) the "SSNLast4" element
     */
    public void xsetSSNLast4(com.idanalytics.products.idscore.request.SSNLast4 ssnLast4)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.SSNLast4 target = null;
            target = (com.idanalytics.products.idscore.request.SSNLast4)get_store().find_element_user(SSNLAST4$2, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.SSNLast4)get_store().add_element_user(SSNLAST4$2);
            }
            target.set(ssnLast4);
        }
    }
    
    /**
     * Unsets the "SSNLast4" element
     */
    public void unsetSSNLast4()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SSNLAST4$2, 0);
        }
    }
    
    /**
     * Gets the "SSNFirst8" element
     */
    public java.lang.String getSSNFirst8()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SSNFIRST8$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "SSNFirst8" element
     */
    public com.idanalytics.products.common_v1.SSN8 xgetSSNFirst8()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.SSN8 target = null;
            target = (com.idanalytics.products.common_v1.SSN8)get_store().find_element_user(SSNFIRST8$4, 0);
            return target;
        }
    }
    
    /**
     * True if has "SSNFirst8" element
     */
    public boolean isSetSSNFirst8()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SSNFIRST8$4) != 0;
        }
    }
    
    /**
     * Sets the "SSNFirst8" element
     */
    public void setSSNFirst8(java.lang.String ssnFirst8)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SSNFIRST8$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SSNFIRST8$4);
            }
            target.setStringValue(ssnFirst8);
        }
    }
    
    /**
     * Sets (as xml) the "SSNFirst8" element
     */
    public void xsetSSNFirst8(com.idanalytics.products.common_v1.SSN8 ssnFirst8)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.SSN8 target = null;
            target = (com.idanalytics.products.common_v1.SSN8)get_store().find_element_user(SSNFIRST8$4, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.SSN8)get_store().add_element_user(SSNFIRST8$4);
            }
            target.set(ssnFirst8);
        }
    }
    
    /**
     * Unsets the "SSNFirst8" element
     */
    public void unsetSSNFirst8()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SSNFIRST8$4, 0);
        }
    }
    
    /**
     * Gets the "SSNLast8" element
     */
    public java.lang.String getSSNLast8()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SSNLAST8$6, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "SSNLast8" element
     */
    public com.idanalytics.products.common_v1.SSN8 xgetSSNLast8()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.SSN8 target = null;
            target = (com.idanalytics.products.common_v1.SSN8)get_store().find_element_user(SSNLAST8$6, 0);
            return target;
        }
    }
    
    /**
     * True if has "SSNLast8" element
     */
    public boolean isSetSSNLast8()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SSNLAST8$6) != 0;
        }
    }
    
    /**
     * Sets the "SSNLast8" element
     */
    public void setSSNLast8(java.lang.String ssnLast8)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SSNLAST8$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SSNLAST8$6);
            }
            target.setStringValue(ssnLast8);
        }
    }
    
    /**
     * Sets (as xml) the "SSNLast8" element
     */
    public void xsetSSNLast8(com.idanalytics.products.common_v1.SSN8 ssnLast8)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.SSN8 target = null;
            target = (com.idanalytics.products.common_v1.SSN8)get_store().find_element_user(SSNLAST8$6, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.SSN8)get_store().add_element_user(SSNLAST8$6);
            }
            target.set(ssnLast8);
        }
    }
    
    /**
     * Unsets the "SSNLast8" element
     */
    public void unsetSSNLast8()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SSNLAST8$6, 0);
        }
    }
    
    /**
     * Gets the "Title" element
     */
    public java.lang.String getTitle()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TITLE$8, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Title" element
     */
    public com.idanalytics.products.idscore.request.Title xgetTitle()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Title target = null;
            target = (com.idanalytics.products.idscore.request.Title)get_store().find_element_user(TITLE$8, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Title" element
     */
    public void setTitle(java.lang.String title)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TITLE$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TITLE$8);
            }
            target.setStringValue(title);
        }
    }
    
    /**
     * Sets (as xml) the "Title" element
     */
    public void xsetTitle(com.idanalytics.products.idscore.request.Title title)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Title target = null;
            target = (com.idanalytics.products.idscore.request.Title)get_store().find_element_user(TITLE$8, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Title)get_store().add_element_user(TITLE$8);
            }
            target.set(title);
        }
    }
    
    /**
     * Gets the "FirstName" element
     */
    public java.lang.String getFirstName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FIRSTNAME$10, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "FirstName" element
     */
    public com.idanalytics.products.idscore.request.Name xgetFirstName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Name target = null;
            target = (com.idanalytics.products.idscore.request.Name)get_store().find_element_user(FIRSTNAME$10, 0);
            return target;
        }
    }
    
    /**
     * Sets the "FirstName" element
     */
    public void setFirstName(java.lang.String firstName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FIRSTNAME$10, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(FIRSTNAME$10);
            }
            target.setStringValue(firstName);
        }
    }
    
    /**
     * Sets (as xml) the "FirstName" element
     */
    public void xsetFirstName(com.idanalytics.products.idscore.request.Name firstName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Name target = null;
            target = (com.idanalytics.products.idscore.request.Name)get_store().find_element_user(FIRSTNAME$10, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Name)get_store().add_element_user(FIRSTNAME$10);
            }
            target.set(firstName);
        }
    }
    
    /**
     * Gets the "MiddleName" element
     */
    public java.lang.String getMiddleName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MIDDLENAME$12, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "MiddleName" element
     */
    public com.idanalytics.products.idscore.request.Name xgetMiddleName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Name target = null;
            target = (com.idanalytics.products.idscore.request.Name)get_store().find_element_user(MIDDLENAME$12, 0);
            return target;
        }
    }
    
    /**
     * Sets the "MiddleName" element
     */
    public void setMiddleName(java.lang.String middleName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MIDDLENAME$12, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(MIDDLENAME$12);
            }
            target.setStringValue(middleName);
        }
    }
    
    /**
     * Sets (as xml) the "MiddleName" element
     */
    public void xsetMiddleName(com.idanalytics.products.idscore.request.Name middleName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Name target = null;
            target = (com.idanalytics.products.idscore.request.Name)get_store().find_element_user(MIDDLENAME$12, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Name)get_store().add_element_user(MIDDLENAME$12);
            }
            target.set(middleName);
        }
    }
    
    /**
     * Gets the "LastName" element
     */
    public java.lang.String getLastName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LASTNAME$14, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "LastName" element
     */
    public com.idanalytics.products.idscore.request.Name xgetLastName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Name target = null;
            target = (com.idanalytics.products.idscore.request.Name)get_store().find_element_user(LASTNAME$14, 0);
            return target;
        }
    }
    
    /**
     * Sets the "LastName" element
     */
    public void setLastName(java.lang.String lastName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LASTNAME$14, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(LASTNAME$14);
            }
            target.setStringValue(lastName);
        }
    }
    
    /**
     * Sets (as xml) the "LastName" element
     */
    public void xsetLastName(com.idanalytics.products.idscore.request.Name lastName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Name target = null;
            target = (com.idanalytics.products.idscore.request.Name)get_store().find_element_user(LASTNAME$14, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Name)get_store().add_element_user(LASTNAME$14);
            }
            target.set(lastName);
        }
    }
    
    /**
     * Gets the "Suffix" element
     */
    public java.lang.String getSuffix()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SUFFIX$16, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Suffix" element
     */
    public com.idanalytics.products.idscore.request.Name xgetSuffix()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Name target = null;
            target = (com.idanalytics.products.idscore.request.Name)get_store().find_element_user(SUFFIX$16, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Suffix" element
     */
    public void setSuffix(java.lang.String suffix)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SUFFIX$16, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SUFFIX$16);
            }
            target.setStringValue(suffix);
        }
    }
    
    /**
     * Sets (as xml) the "Suffix" element
     */
    public void xsetSuffix(com.idanalytics.products.idscore.request.Name suffix)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Name target = null;
            target = (com.idanalytics.products.idscore.request.Name)get_store().find_element_user(SUFFIX$16, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Name)get_store().add_element_user(SUFFIX$16);
            }
            target.set(suffix);
        }
    }
    
    /**
     * Gets the "DOB" element
     */
    public java.lang.Object getDOB()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DOB$18, 0);
            if (target == null)
            {
                return null;
            }
            return target.getObjectValue();
        }
    }
    
    /**
     * Gets (as xml) the "DOB" element
     */
    public com.idanalytics.products.idscore.request.Date xgetDOB()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Date target = null;
            target = (com.idanalytics.products.idscore.request.Date)get_store().find_element_user(DOB$18, 0);
            return target;
        }
    }
    
    /**
     * Sets the "DOB" element
     */
    public void setDOB(java.lang.Object dob)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DOB$18, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DOB$18);
            }
            target.setObjectValue(dob);
        }
    }
    
    /**
     * Sets (as xml) the "DOB" element
     */
    public void xsetDOB(com.idanalytics.products.idscore.request.Date dob)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Date target = null;
            target = (com.idanalytics.products.idscore.request.Date)get_store().find_element_user(DOB$18, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Date)get_store().add_element_user(DOB$18);
            }
            target.set(dob);
        }
    }
    
    /**
     * Gets the "Gender" element
     */
    public java.lang.String getGender()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(GENDER$20, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Gender" element
     */
    public com.idanalytics.products.idscore.request.Gender xgetGender()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Gender target = null;
            target = (com.idanalytics.products.idscore.request.Gender)get_store().find_element_user(GENDER$20, 0);
            return target;
        }
    }
    
    /**
     * True if has "Gender" element
     */
    public boolean isSetGender()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(GENDER$20) != 0;
        }
    }
    
    /**
     * Sets the "Gender" element
     */
    public void setGender(java.lang.String gender)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(GENDER$20, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(GENDER$20);
            }
            target.setStringValue(gender);
        }
    }
    
    /**
     * Sets (as xml) the "Gender" element
     */
    public void xsetGender(com.idanalytics.products.idscore.request.Gender gender)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Gender target = null;
            target = (com.idanalytics.products.idscore.request.Gender)get_store().find_element_user(GENDER$20, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Gender)get_store().add_element_user(GENDER$20);
            }
            target.set(gender);
        }
    }
    
    /**
     * Unsets the "Gender" element
     */
    public void unsetGender()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(GENDER$20, 0);
        }
    }
    
    /**
     * Gets the "PlaceOfBirth" element
     */
    public java.lang.String getPlaceOfBirth()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PLACEOFBIRTH$22, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PlaceOfBirth" element
     */
    public com.idanalytics.products.idscore.request.PlaceOfBirth xgetPlaceOfBirth()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.PlaceOfBirth target = null;
            target = (com.idanalytics.products.idscore.request.PlaceOfBirth)get_store().find_element_user(PLACEOFBIRTH$22, 0);
            return target;
        }
    }
    
    /**
     * True if has "PlaceOfBirth" element
     */
    public boolean isSetPlaceOfBirth()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PLACEOFBIRTH$22) != 0;
        }
    }
    
    /**
     * Sets the "PlaceOfBirth" element
     */
    public void setPlaceOfBirth(java.lang.String placeOfBirth)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PLACEOFBIRTH$22, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PLACEOFBIRTH$22);
            }
            target.setStringValue(placeOfBirth);
        }
    }
    
    /**
     * Sets (as xml) the "PlaceOfBirth" element
     */
    public void xsetPlaceOfBirth(com.idanalytics.products.idscore.request.PlaceOfBirth placeOfBirth)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.PlaceOfBirth target = null;
            target = (com.idanalytics.products.idscore.request.PlaceOfBirth)get_store().find_element_user(PLACEOFBIRTH$22, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.PlaceOfBirth)get_store().add_element_user(PLACEOFBIRTH$22);
            }
            target.set(placeOfBirth);
        }
    }
    
    /**
     * Unsets the "PlaceOfBirth" element
     */
    public void unsetPlaceOfBirth()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PLACEOFBIRTH$22, 0);
        }
    }
    
    /**
     * Gets the "MothersMaiden" element
     */
    public java.lang.String getMothersMaiden()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MOTHERSMAIDEN$24, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "MothersMaiden" element
     */
    public com.idanalytics.products.idscore.request.MothersMaiden xgetMothersMaiden()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.MothersMaiden target = null;
            target = (com.idanalytics.products.idscore.request.MothersMaiden)get_store().find_element_user(MOTHERSMAIDEN$24, 0);
            return target;
        }
    }
    
    /**
     * True if has "MothersMaiden" element
     */
    public boolean isSetMothersMaiden()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(MOTHERSMAIDEN$24) != 0;
        }
    }
    
    /**
     * Sets the "MothersMaiden" element
     */
    public void setMothersMaiden(java.lang.String mothersMaiden)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MOTHERSMAIDEN$24, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(MOTHERSMAIDEN$24);
            }
            target.setStringValue(mothersMaiden);
        }
    }
    
    /**
     * Sets (as xml) the "MothersMaiden" element
     */
    public void xsetMothersMaiden(com.idanalytics.products.idscore.request.MothersMaiden mothersMaiden)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.MothersMaiden target = null;
            target = (com.idanalytics.products.idscore.request.MothersMaiden)get_store().find_element_user(MOTHERSMAIDEN$24, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.MothersMaiden)get_store().add_element_user(MOTHERSMAIDEN$24);
            }
            target.set(mothersMaiden);
        }
    }
    
    /**
     * Unsets the "MothersMaiden" element
     */
    public void unsetMothersMaiden()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(MOTHERSMAIDEN$24, 0);
        }
    }
    
    /**
     * Gets the "Company" element
     */
    public java.lang.String getCompany()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(COMPANY$26, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Company" element
     */
    public com.idanalytics.products.common_v1.NormalizedString xgetCompany()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.NormalizedString target = null;
            target = (com.idanalytics.products.common_v1.NormalizedString)get_store().find_element_user(COMPANY$26, 0);
            return target;
        }
    }
    
    /**
     * True if has "Company" element
     */
    public boolean isSetCompany()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(COMPANY$26) != 0;
        }
    }
    
    /**
     * Sets the "Company" element
     */
    public void setCompany(java.lang.String company)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(COMPANY$26, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(COMPANY$26);
            }
            target.setStringValue(company);
        }
    }
    
    /**
     * Sets (as xml) the "Company" element
     */
    public void xsetCompany(com.idanalytics.products.common_v1.NormalizedString company)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.NormalizedString target = null;
            target = (com.idanalytics.products.common_v1.NormalizedString)get_store().find_element_user(COMPANY$26, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.NormalizedString)get_store().add_element_user(COMPANY$26);
            }
            target.set(company);
        }
    }
    
    /**
     * Unsets the "Company" element
     */
    public void unsetCompany()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(COMPANY$26, 0);
        }
    }
    
    /**
     * Gets the "Address" element
     */
    public com.idanalytics.products.idscore.request.Address getAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Address target = null;
            target = (com.idanalytics.products.idscore.request.Address)get_store().find_element_user(ADDRESS$28, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "Address" element
     */
    public boolean isSetAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ADDRESS$28) != 0;
        }
    }
    
    /**
     * Sets the "Address" element
     */
    public void setAddress(com.idanalytics.products.idscore.request.Address address)
    {
        generatedSetterHelperImpl(address, ADDRESS$28, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "Address" element
     */
    public com.idanalytics.products.idscore.request.Address addNewAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Address target = null;
            target = (com.idanalytics.products.idscore.request.Address)get_store().add_element_user(ADDRESS$28);
            return target;
        }
    }
    
    /**
     * Unsets the "Address" element
     */
    public void unsetAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ADDRESS$28, 0);
        }
    }
    
    /**
     * Gets the "ParsedAddress" element
     */
    public com.idanalytics.products.idscore.request.ParsedAddress getParsedAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ParsedAddress target = null;
            target = (com.idanalytics.products.idscore.request.ParsedAddress)get_store().find_element_user(PARSEDADDRESS$30, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "ParsedAddress" element
     */
    public boolean isSetParsedAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PARSEDADDRESS$30) != 0;
        }
    }
    
    /**
     * Sets the "ParsedAddress" element
     */
    public void setParsedAddress(com.idanalytics.products.idscore.request.ParsedAddress parsedAddress)
    {
        generatedSetterHelperImpl(parsedAddress, PARSEDADDRESS$30, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "ParsedAddress" element
     */
    public com.idanalytics.products.idscore.request.ParsedAddress addNewParsedAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ParsedAddress target = null;
            target = (com.idanalytics.products.idscore.request.ParsedAddress)get_store().add_element_user(PARSEDADDRESS$30);
            return target;
        }
    }
    
    /**
     * Unsets the "ParsedAddress" element
     */
    public void unsetParsedAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PARSEDADDRESS$30, 0);
        }
    }
    
    /**
     * Gets the "City" element
     */
    public java.lang.String getCity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CITY$32, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "City" element
     */
    public com.idanalytics.products.idscore.request.City xgetCity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.City target = null;
            target = (com.idanalytics.products.idscore.request.City)get_store().find_element_user(CITY$32, 0);
            return target;
        }
    }
    
    /**
     * Sets the "City" element
     */
    public void setCity(java.lang.String city)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CITY$32, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CITY$32);
            }
            target.setStringValue(city);
        }
    }
    
    /**
     * Sets (as xml) the "City" element
     */
    public void xsetCity(com.idanalytics.products.idscore.request.City city)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.City target = null;
            target = (com.idanalytics.products.idscore.request.City)get_store().find_element_user(CITY$32, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.City)get_store().add_element_user(CITY$32);
            }
            target.set(city);
        }
    }
    
    /**
     * Gets the "State" element
     */
    public java.lang.String getState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STATE$34, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "State" element
     */
    public com.idanalytics.products.idscore.request.State xgetState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.State target = null;
            target = (com.idanalytics.products.idscore.request.State)get_store().find_element_user(STATE$34, 0);
            return target;
        }
    }
    
    /**
     * Sets the "State" element
     */
    public void setState(java.lang.String state)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STATE$34, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(STATE$34);
            }
            target.setStringValue(state);
        }
    }
    
    /**
     * Sets (as xml) the "State" element
     */
    public void xsetState(com.idanalytics.products.idscore.request.State state)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.State target = null;
            target = (com.idanalytics.products.idscore.request.State)get_store().find_element_user(STATE$34, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.State)get_store().add_element_user(STATE$34);
            }
            target.set(state);
        }
    }
    
    /**
     * Gets the "Zip" element
     */
    public java.lang.String getZip()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ZIP$36, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Zip" element
     */
    public com.idanalytics.products.idscore.request.Zip xgetZip()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Zip target = null;
            target = (com.idanalytics.products.idscore.request.Zip)get_store().find_element_user(ZIP$36, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Zip" element
     */
    public void setZip(java.lang.String zip)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ZIP$36, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ZIP$36);
            }
            target.setStringValue(zip);
        }
    }
    
    /**
     * Sets (as xml) the "Zip" element
     */
    public void xsetZip(com.idanalytics.products.idscore.request.Zip zip)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Zip target = null;
            target = (com.idanalytics.products.idscore.request.Zip)get_store().find_element_user(ZIP$36, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Zip)get_store().add_element_user(ZIP$36);
            }
            target.set(zip);
        }
    }
    
    /**
     * Gets the "Country" element
     */
    public java.lang.String getCountry()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(COUNTRY$38, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Country" element
     */
    public com.idanalytics.products.idscore.request.Country xgetCountry()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Country target = null;
            target = (com.idanalytics.products.idscore.request.Country)get_store().find_element_user(COUNTRY$38, 0);
            return target;
        }
    }
    
    /**
     * True if has "Country" element
     */
    public boolean isSetCountry()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(COUNTRY$38) != 0;
        }
    }
    
    /**
     * Sets the "Country" element
     */
    public void setCountry(java.lang.String country)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(COUNTRY$38, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(COUNTRY$38);
            }
            target.setStringValue(country);
        }
    }
    
    /**
     * Sets (as xml) the "Country" element
     */
    public void xsetCountry(com.idanalytics.products.idscore.request.Country country)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Country target = null;
            target = (com.idanalytics.products.idscore.request.Country)get_store().find_element_user(COUNTRY$38, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Country)get_store().add_element_user(COUNTRY$38);
            }
            target.set(country);
        }
    }
    
    /**
     * Unsets the "Country" element
     */
    public void unsetCountry()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(COUNTRY$38, 0);
        }
    }
    
    /**
     * Gets the "TimeAtResidence" element
     */
    public java.lang.String getTimeAtResidence()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TIMEATRESIDENCE$40, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "TimeAtResidence" element
     */
    public com.idanalytics.products.idscore.request.TimeAt xgetTimeAtResidence()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.TimeAt target = null;
            target = (com.idanalytics.products.idscore.request.TimeAt)get_store().find_element_user(TIMEATRESIDENCE$40, 0);
            return target;
        }
    }
    
    /**
     * True if has "TimeAtResidence" element
     */
    public boolean isSetTimeAtResidence()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(TIMEATRESIDENCE$40) != 0;
        }
    }
    
    /**
     * Sets the "TimeAtResidence" element
     */
    public void setTimeAtResidence(java.lang.String timeAtResidence)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TIMEATRESIDENCE$40, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TIMEATRESIDENCE$40);
            }
            target.setStringValue(timeAtResidence);
        }
    }
    
    /**
     * Sets (as xml) the "TimeAtResidence" element
     */
    public void xsetTimeAtResidence(com.idanalytics.products.idscore.request.TimeAt timeAtResidence)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.TimeAt target = null;
            target = (com.idanalytics.products.idscore.request.TimeAt)get_store().find_element_user(TIMEATRESIDENCE$40, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.TimeAt)get_store().add_element_user(TIMEATRESIDENCE$40);
            }
            target.set(timeAtResidence);
        }
    }
    
    /**
     * Unsets the "TimeAtResidence" element
     */
    public void unsetTimeAtResidence()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(TIMEATRESIDENCE$40, 0);
        }
    }
    
    /**
     * Gets the "AddressSince" element
     */
    public java.lang.Object getAddressSince()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ADDRESSSINCE$42, 0);
            if (target == null)
            {
                return null;
            }
            return target.getObjectValue();
        }
    }
    
    /**
     * Gets (as xml) the "AddressSince" element
     */
    public com.idanalytics.products.idscore.request.Date xgetAddressSince()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Date target = null;
            target = (com.idanalytics.products.idscore.request.Date)get_store().find_element_user(ADDRESSSINCE$42, 0);
            return target;
        }
    }
    
    /**
     * True if has "AddressSince" element
     */
    public boolean isSetAddressSince()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ADDRESSSINCE$42) != 0;
        }
    }
    
    /**
     * Sets the "AddressSince" element
     */
    public void setAddressSince(java.lang.Object addressSince)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ADDRESSSINCE$42, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ADDRESSSINCE$42);
            }
            target.setObjectValue(addressSince);
        }
    }
    
    /**
     * Sets (as xml) the "AddressSince" element
     */
    public void xsetAddressSince(com.idanalytics.products.idscore.request.Date addressSince)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Date target = null;
            target = (com.idanalytics.products.idscore.request.Date)get_store().find_element_user(ADDRESSSINCE$42, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Date)get_store().add_element_user(ADDRESSSINCE$42);
            }
            target.set(addressSince);
        }
    }
    
    /**
     * Unsets the "AddressSince" element
     */
    public void unsetAddressSince()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ADDRESSSINCE$42, 0);
        }
    }
    
    /**
     * Gets the "Occupancy" element
     */
    public com.idanalytics.products.idscore.request.Occupancy.Enum getOccupancy()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(OCCUPANCY$44, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.idscore.request.Occupancy.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "Occupancy" element
     */
    public com.idanalytics.products.idscore.request.Occupancy xgetOccupancy()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Occupancy target = null;
            target = (com.idanalytics.products.idscore.request.Occupancy)get_store().find_element_user(OCCUPANCY$44, 0);
            return target;
        }
    }
    
    /**
     * True if has "Occupancy" element
     */
    public boolean isSetOccupancy()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(OCCUPANCY$44) != 0;
        }
    }
    
    /**
     * Sets the "Occupancy" element
     */
    public void setOccupancy(com.idanalytics.products.idscore.request.Occupancy.Enum occupancy)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(OCCUPANCY$44, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(OCCUPANCY$44);
            }
            target.setEnumValue(occupancy);
        }
    }
    
    /**
     * Sets (as xml) the "Occupancy" element
     */
    public void xsetOccupancy(com.idanalytics.products.idscore.request.Occupancy occupancy)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Occupancy target = null;
            target = (com.idanalytics.products.idscore.request.Occupancy)get_store().find_element_user(OCCUPANCY$44, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Occupancy)get_store().add_element_user(OCCUPANCY$44);
            }
            target.set(occupancy);
        }
    }
    
    /**
     * Unsets the "Occupancy" element
     */
    public void unsetOccupancy()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(OCCUPANCY$44, 0);
        }
    }
    
    /**
     * Gets the "PhysicalAddress" element
     */
    public com.idanalytics.products.idscore.request.Address getPhysicalAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Address target = null;
            target = (com.idanalytics.products.idscore.request.Address)get_store().find_element_user(PHYSICALADDRESS$46, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "PhysicalAddress" element
     */
    public boolean isSetPhysicalAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PHYSICALADDRESS$46) != 0;
        }
    }
    
    /**
     * Sets the "PhysicalAddress" element
     */
    public void setPhysicalAddress(com.idanalytics.products.idscore.request.Address physicalAddress)
    {
        generatedSetterHelperImpl(physicalAddress, PHYSICALADDRESS$46, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "PhysicalAddress" element
     */
    public com.idanalytics.products.idscore.request.Address addNewPhysicalAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Address target = null;
            target = (com.idanalytics.products.idscore.request.Address)get_store().add_element_user(PHYSICALADDRESS$46);
            return target;
        }
    }
    
    /**
     * Unsets the "PhysicalAddress" element
     */
    public void unsetPhysicalAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PHYSICALADDRESS$46, 0);
        }
    }
    
    /**
     * Gets the "ParsedPhysicalAddress" element
     */
    public com.idanalytics.products.idscore.request.ParsedAddress getParsedPhysicalAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ParsedAddress target = null;
            target = (com.idanalytics.products.idscore.request.ParsedAddress)get_store().find_element_user(PARSEDPHYSICALADDRESS$48, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "ParsedPhysicalAddress" element
     */
    public boolean isSetParsedPhysicalAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PARSEDPHYSICALADDRESS$48) != 0;
        }
    }
    
    /**
     * Sets the "ParsedPhysicalAddress" element
     */
    public void setParsedPhysicalAddress(com.idanalytics.products.idscore.request.ParsedAddress parsedPhysicalAddress)
    {
        generatedSetterHelperImpl(parsedPhysicalAddress, PARSEDPHYSICALADDRESS$48, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "ParsedPhysicalAddress" element
     */
    public com.idanalytics.products.idscore.request.ParsedAddress addNewParsedPhysicalAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ParsedAddress target = null;
            target = (com.idanalytics.products.idscore.request.ParsedAddress)get_store().add_element_user(PARSEDPHYSICALADDRESS$48);
            return target;
        }
    }
    
    /**
     * Unsets the "ParsedPhysicalAddress" element
     */
    public void unsetParsedPhysicalAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PARSEDPHYSICALADDRESS$48, 0);
        }
    }
    
    /**
     * Gets the "PhysicalCity" element
     */
    public java.lang.String getPhysicalCity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PHYSICALCITY$50, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PhysicalCity" element
     */
    public com.idanalytics.products.idscore.request.City xgetPhysicalCity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.City target = null;
            target = (com.idanalytics.products.idscore.request.City)get_store().find_element_user(PHYSICALCITY$50, 0);
            return target;
        }
    }
    
    /**
     * True if has "PhysicalCity" element
     */
    public boolean isSetPhysicalCity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PHYSICALCITY$50) != 0;
        }
    }
    
    /**
     * Sets the "PhysicalCity" element
     */
    public void setPhysicalCity(java.lang.String physicalCity)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PHYSICALCITY$50, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PHYSICALCITY$50);
            }
            target.setStringValue(physicalCity);
        }
    }
    
    /**
     * Sets (as xml) the "PhysicalCity" element
     */
    public void xsetPhysicalCity(com.idanalytics.products.idscore.request.City physicalCity)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.City target = null;
            target = (com.idanalytics.products.idscore.request.City)get_store().find_element_user(PHYSICALCITY$50, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.City)get_store().add_element_user(PHYSICALCITY$50);
            }
            target.set(physicalCity);
        }
    }
    
    /**
     * Unsets the "PhysicalCity" element
     */
    public void unsetPhysicalCity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PHYSICALCITY$50, 0);
        }
    }
    
    /**
     * Gets the "PhysicalState" element
     */
    public java.lang.String getPhysicalState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PHYSICALSTATE$52, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PhysicalState" element
     */
    public com.idanalytics.products.idscore.request.State xgetPhysicalState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.State target = null;
            target = (com.idanalytics.products.idscore.request.State)get_store().find_element_user(PHYSICALSTATE$52, 0);
            return target;
        }
    }
    
    /**
     * True if has "PhysicalState" element
     */
    public boolean isSetPhysicalState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PHYSICALSTATE$52) != 0;
        }
    }
    
    /**
     * Sets the "PhysicalState" element
     */
    public void setPhysicalState(java.lang.String physicalState)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PHYSICALSTATE$52, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PHYSICALSTATE$52);
            }
            target.setStringValue(physicalState);
        }
    }
    
    /**
     * Sets (as xml) the "PhysicalState" element
     */
    public void xsetPhysicalState(com.idanalytics.products.idscore.request.State physicalState)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.State target = null;
            target = (com.idanalytics.products.idscore.request.State)get_store().find_element_user(PHYSICALSTATE$52, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.State)get_store().add_element_user(PHYSICALSTATE$52);
            }
            target.set(physicalState);
        }
    }
    
    /**
     * Unsets the "PhysicalState" element
     */
    public void unsetPhysicalState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PHYSICALSTATE$52, 0);
        }
    }
    
    /**
     * Gets the "PhysicalZip" element
     */
    public java.lang.String getPhysicalZip()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PHYSICALZIP$54, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PhysicalZip" element
     */
    public com.idanalytics.products.idscore.request.Zip xgetPhysicalZip()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Zip target = null;
            target = (com.idanalytics.products.idscore.request.Zip)get_store().find_element_user(PHYSICALZIP$54, 0);
            return target;
        }
    }
    
    /**
     * True if has "PhysicalZip" element
     */
    public boolean isSetPhysicalZip()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PHYSICALZIP$54) != 0;
        }
    }
    
    /**
     * Sets the "PhysicalZip" element
     */
    public void setPhysicalZip(java.lang.String physicalZip)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PHYSICALZIP$54, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PHYSICALZIP$54);
            }
            target.setStringValue(physicalZip);
        }
    }
    
    /**
     * Sets (as xml) the "PhysicalZip" element
     */
    public void xsetPhysicalZip(com.idanalytics.products.idscore.request.Zip physicalZip)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Zip target = null;
            target = (com.idanalytics.products.idscore.request.Zip)get_store().find_element_user(PHYSICALZIP$54, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Zip)get_store().add_element_user(PHYSICALZIP$54);
            }
            target.set(physicalZip);
        }
    }
    
    /**
     * Unsets the "PhysicalZip" element
     */
    public void unsetPhysicalZip()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PHYSICALZIP$54, 0);
        }
    }
    
    /**
     * Gets the "PhysicalCountry" element
     */
    public java.lang.String getPhysicalCountry()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PHYSICALCOUNTRY$56, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PhysicalCountry" element
     */
    public com.idanalytics.products.idscore.request.Country xgetPhysicalCountry()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Country target = null;
            target = (com.idanalytics.products.idscore.request.Country)get_store().find_element_user(PHYSICALCOUNTRY$56, 0);
            return target;
        }
    }
    
    /**
     * True if has "PhysicalCountry" element
     */
    public boolean isSetPhysicalCountry()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PHYSICALCOUNTRY$56) != 0;
        }
    }
    
    /**
     * Sets the "PhysicalCountry" element
     */
    public void setPhysicalCountry(java.lang.String physicalCountry)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PHYSICALCOUNTRY$56, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PHYSICALCOUNTRY$56);
            }
            target.setStringValue(physicalCountry);
        }
    }
    
    /**
     * Sets (as xml) the "PhysicalCountry" element
     */
    public void xsetPhysicalCountry(com.idanalytics.products.idscore.request.Country physicalCountry)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Country target = null;
            target = (com.idanalytics.products.idscore.request.Country)get_store().find_element_user(PHYSICALCOUNTRY$56, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Country)get_store().add_element_user(PHYSICALCOUNTRY$56);
            }
            target.set(physicalCountry);
        }
    }
    
    /**
     * Unsets the "PhysicalCountry" element
     */
    public void unsetPhysicalCountry()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PHYSICALCOUNTRY$56, 0);
        }
    }
    
    /**
     * Gets the "PrevSSN" element
     */
    public java.lang.String getPrevSSN()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVSSN$58, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrevSSN" element
     */
    public com.idanalytics.products.idscore.request.SSN xgetPrevSSN()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.SSN target = null;
            target = (com.idanalytics.products.idscore.request.SSN)get_store().find_element_user(PREVSSN$58, 0);
            return target;
        }
    }
    
    /**
     * True if has "PrevSSN" element
     */
    public boolean isSetPrevSSN()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREVSSN$58) != 0;
        }
    }
    
    /**
     * Sets the "PrevSSN" element
     */
    public void setPrevSSN(java.lang.String prevSSN)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVSSN$58, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREVSSN$58);
            }
            target.setStringValue(prevSSN);
        }
    }
    
    /**
     * Sets (as xml) the "PrevSSN" element
     */
    public void xsetPrevSSN(com.idanalytics.products.idscore.request.SSN prevSSN)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.SSN target = null;
            target = (com.idanalytics.products.idscore.request.SSN)get_store().find_element_user(PREVSSN$58, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.SSN)get_store().add_element_user(PREVSSN$58);
            }
            target.set(prevSSN);
        }
    }
    
    /**
     * Unsets the "PrevSSN" element
     */
    public void unsetPrevSSN()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREVSSN$58, 0);
        }
    }
    
    /**
     * Gets the "PrevTitle" element
     */
    public java.lang.String getPrevTitle()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVTITLE$60, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrevTitle" element
     */
    public com.idanalytics.products.idscore.request.Title xgetPrevTitle()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Title target = null;
            target = (com.idanalytics.products.idscore.request.Title)get_store().find_element_user(PREVTITLE$60, 0);
            return target;
        }
    }
    
    /**
     * True if has "PrevTitle" element
     */
    public boolean isSetPrevTitle()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREVTITLE$60) != 0;
        }
    }
    
    /**
     * Sets the "PrevTitle" element
     */
    public void setPrevTitle(java.lang.String prevTitle)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVTITLE$60, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREVTITLE$60);
            }
            target.setStringValue(prevTitle);
        }
    }
    
    /**
     * Sets (as xml) the "PrevTitle" element
     */
    public void xsetPrevTitle(com.idanalytics.products.idscore.request.Title prevTitle)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Title target = null;
            target = (com.idanalytics.products.idscore.request.Title)get_store().find_element_user(PREVTITLE$60, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Title)get_store().add_element_user(PREVTITLE$60);
            }
            target.set(prevTitle);
        }
    }
    
    /**
     * Unsets the "PrevTitle" element
     */
    public void unsetPrevTitle()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREVTITLE$60, 0);
        }
    }
    
    /**
     * Gets the "PrevFirstName" element
     */
    public java.lang.String getPrevFirstName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVFIRSTNAME$62, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrevFirstName" element
     */
    public com.idanalytics.products.idscore.request.Name xgetPrevFirstName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Name target = null;
            target = (com.idanalytics.products.idscore.request.Name)get_store().find_element_user(PREVFIRSTNAME$62, 0);
            return target;
        }
    }
    
    /**
     * True if has "PrevFirstName" element
     */
    public boolean isSetPrevFirstName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREVFIRSTNAME$62) != 0;
        }
    }
    
    /**
     * Sets the "PrevFirstName" element
     */
    public void setPrevFirstName(java.lang.String prevFirstName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVFIRSTNAME$62, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREVFIRSTNAME$62);
            }
            target.setStringValue(prevFirstName);
        }
    }
    
    /**
     * Sets (as xml) the "PrevFirstName" element
     */
    public void xsetPrevFirstName(com.idanalytics.products.idscore.request.Name prevFirstName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Name target = null;
            target = (com.idanalytics.products.idscore.request.Name)get_store().find_element_user(PREVFIRSTNAME$62, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Name)get_store().add_element_user(PREVFIRSTNAME$62);
            }
            target.set(prevFirstName);
        }
    }
    
    /**
     * Unsets the "PrevFirstName" element
     */
    public void unsetPrevFirstName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREVFIRSTNAME$62, 0);
        }
    }
    
    /**
     * Gets the "PrevMiddleName" element
     */
    public java.lang.String getPrevMiddleName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVMIDDLENAME$64, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrevMiddleName" element
     */
    public com.idanalytics.products.idscore.request.Name xgetPrevMiddleName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Name target = null;
            target = (com.idanalytics.products.idscore.request.Name)get_store().find_element_user(PREVMIDDLENAME$64, 0);
            return target;
        }
    }
    
    /**
     * True if has "PrevMiddleName" element
     */
    public boolean isSetPrevMiddleName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREVMIDDLENAME$64) != 0;
        }
    }
    
    /**
     * Sets the "PrevMiddleName" element
     */
    public void setPrevMiddleName(java.lang.String prevMiddleName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVMIDDLENAME$64, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREVMIDDLENAME$64);
            }
            target.setStringValue(prevMiddleName);
        }
    }
    
    /**
     * Sets (as xml) the "PrevMiddleName" element
     */
    public void xsetPrevMiddleName(com.idanalytics.products.idscore.request.Name prevMiddleName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Name target = null;
            target = (com.idanalytics.products.idscore.request.Name)get_store().find_element_user(PREVMIDDLENAME$64, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Name)get_store().add_element_user(PREVMIDDLENAME$64);
            }
            target.set(prevMiddleName);
        }
    }
    
    /**
     * Unsets the "PrevMiddleName" element
     */
    public void unsetPrevMiddleName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREVMIDDLENAME$64, 0);
        }
    }
    
    /**
     * Gets the "PrevLastName" element
     */
    public java.lang.String getPrevLastName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVLASTNAME$66, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrevLastName" element
     */
    public com.idanalytics.products.idscore.request.Name xgetPrevLastName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Name target = null;
            target = (com.idanalytics.products.idscore.request.Name)get_store().find_element_user(PREVLASTNAME$66, 0);
            return target;
        }
    }
    
    /**
     * True if has "PrevLastName" element
     */
    public boolean isSetPrevLastName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREVLASTNAME$66) != 0;
        }
    }
    
    /**
     * Sets the "PrevLastName" element
     */
    public void setPrevLastName(java.lang.String prevLastName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVLASTNAME$66, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREVLASTNAME$66);
            }
            target.setStringValue(prevLastName);
        }
    }
    
    /**
     * Sets (as xml) the "PrevLastName" element
     */
    public void xsetPrevLastName(com.idanalytics.products.idscore.request.Name prevLastName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Name target = null;
            target = (com.idanalytics.products.idscore.request.Name)get_store().find_element_user(PREVLASTNAME$66, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Name)get_store().add_element_user(PREVLASTNAME$66);
            }
            target.set(prevLastName);
        }
    }
    
    /**
     * Unsets the "PrevLastName" element
     */
    public void unsetPrevLastName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREVLASTNAME$66, 0);
        }
    }
    
    /**
     * Gets the "PrevSuffix" element
     */
    public java.lang.String getPrevSuffix()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVSUFFIX$68, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrevSuffix" element
     */
    public com.idanalytics.products.idscore.request.Name xgetPrevSuffix()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Name target = null;
            target = (com.idanalytics.products.idscore.request.Name)get_store().find_element_user(PREVSUFFIX$68, 0);
            return target;
        }
    }
    
    /**
     * True if has "PrevSuffix" element
     */
    public boolean isSetPrevSuffix()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREVSUFFIX$68) != 0;
        }
    }
    
    /**
     * Sets the "PrevSuffix" element
     */
    public void setPrevSuffix(java.lang.String prevSuffix)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVSUFFIX$68, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREVSUFFIX$68);
            }
            target.setStringValue(prevSuffix);
        }
    }
    
    /**
     * Sets (as xml) the "PrevSuffix" element
     */
    public void xsetPrevSuffix(com.idanalytics.products.idscore.request.Name prevSuffix)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Name target = null;
            target = (com.idanalytics.products.idscore.request.Name)get_store().find_element_user(PREVSUFFIX$68, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Name)get_store().add_element_user(PREVSUFFIX$68);
            }
            target.set(prevSuffix);
        }
    }
    
    /**
     * Unsets the "PrevSuffix" element
     */
    public void unsetPrevSuffix()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREVSUFFIX$68, 0);
        }
    }
    
    /**
     * Gets the "PrevDOB" element
     */
    public java.lang.Object getPrevDOB()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVDOB$70, 0);
            if (target == null)
            {
                return null;
            }
            return target.getObjectValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrevDOB" element
     */
    public com.idanalytics.products.idscore.request.Date xgetPrevDOB()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Date target = null;
            target = (com.idanalytics.products.idscore.request.Date)get_store().find_element_user(PREVDOB$70, 0);
            return target;
        }
    }
    
    /**
     * True if has "PrevDOB" element
     */
    public boolean isSetPrevDOB()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREVDOB$70) != 0;
        }
    }
    
    /**
     * Sets the "PrevDOB" element
     */
    public void setPrevDOB(java.lang.Object prevDOB)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVDOB$70, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREVDOB$70);
            }
            target.setObjectValue(prevDOB);
        }
    }
    
    /**
     * Sets (as xml) the "PrevDOB" element
     */
    public void xsetPrevDOB(com.idanalytics.products.idscore.request.Date prevDOB)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Date target = null;
            target = (com.idanalytics.products.idscore.request.Date)get_store().find_element_user(PREVDOB$70, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Date)get_store().add_element_user(PREVDOB$70);
            }
            target.set(prevDOB);
        }
    }
    
    /**
     * Unsets the "PrevDOB" element
     */
    public void unsetPrevDOB()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREVDOB$70, 0);
        }
    }
    
    /**
     * Gets the "PrevAddress" element
     */
    public com.idanalytics.products.idscore.request.Address getPrevAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Address target = null;
            target = (com.idanalytics.products.idscore.request.Address)get_store().find_element_user(PREVADDRESS$72, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "PrevAddress" element
     */
    public boolean isSetPrevAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREVADDRESS$72) != 0;
        }
    }
    
    /**
     * Sets the "PrevAddress" element
     */
    public void setPrevAddress(com.idanalytics.products.idscore.request.Address prevAddress)
    {
        generatedSetterHelperImpl(prevAddress, PREVADDRESS$72, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "PrevAddress" element
     */
    public com.idanalytics.products.idscore.request.Address addNewPrevAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Address target = null;
            target = (com.idanalytics.products.idscore.request.Address)get_store().add_element_user(PREVADDRESS$72);
            return target;
        }
    }
    
    /**
     * Unsets the "PrevAddress" element
     */
    public void unsetPrevAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREVADDRESS$72, 0);
        }
    }
    
    /**
     * Gets the "ParsedPrevAddress" element
     */
    public com.idanalytics.products.idscore.request.ParsedAddress getParsedPrevAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ParsedAddress target = null;
            target = (com.idanalytics.products.idscore.request.ParsedAddress)get_store().find_element_user(PARSEDPREVADDRESS$74, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "ParsedPrevAddress" element
     */
    public boolean isSetParsedPrevAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PARSEDPREVADDRESS$74) != 0;
        }
    }
    
    /**
     * Sets the "ParsedPrevAddress" element
     */
    public void setParsedPrevAddress(com.idanalytics.products.idscore.request.ParsedAddress parsedPrevAddress)
    {
        generatedSetterHelperImpl(parsedPrevAddress, PARSEDPREVADDRESS$74, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "ParsedPrevAddress" element
     */
    public com.idanalytics.products.idscore.request.ParsedAddress addNewParsedPrevAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ParsedAddress target = null;
            target = (com.idanalytics.products.idscore.request.ParsedAddress)get_store().add_element_user(PARSEDPREVADDRESS$74);
            return target;
        }
    }
    
    /**
     * Unsets the "ParsedPrevAddress" element
     */
    public void unsetParsedPrevAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PARSEDPREVADDRESS$74, 0);
        }
    }
    
    /**
     * Gets the "PrevCity" element
     */
    public java.lang.String getPrevCity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVCITY$76, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrevCity" element
     */
    public com.idanalytics.products.idscore.request.City xgetPrevCity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.City target = null;
            target = (com.idanalytics.products.idscore.request.City)get_store().find_element_user(PREVCITY$76, 0);
            return target;
        }
    }
    
    /**
     * True if has "PrevCity" element
     */
    public boolean isSetPrevCity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREVCITY$76) != 0;
        }
    }
    
    /**
     * Sets the "PrevCity" element
     */
    public void setPrevCity(java.lang.String prevCity)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVCITY$76, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREVCITY$76);
            }
            target.setStringValue(prevCity);
        }
    }
    
    /**
     * Sets (as xml) the "PrevCity" element
     */
    public void xsetPrevCity(com.idanalytics.products.idscore.request.City prevCity)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.City target = null;
            target = (com.idanalytics.products.idscore.request.City)get_store().find_element_user(PREVCITY$76, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.City)get_store().add_element_user(PREVCITY$76);
            }
            target.set(prevCity);
        }
    }
    
    /**
     * Unsets the "PrevCity" element
     */
    public void unsetPrevCity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREVCITY$76, 0);
        }
    }
    
    /**
     * Gets the "PrevState" element
     */
    public java.lang.String getPrevState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVSTATE$78, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrevState" element
     */
    public com.idanalytics.products.idscore.request.State xgetPrevState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.State target = null;
            target = (com.idanalytics.products.idscore.request.State)get_store().find_element_user(PREVSTATE$78, 0);
            return target;
        }
    }
    
    /**
     * True if has "PrevState" element
     */
    public boolean isSetPrevState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREVSTATE$78) != 0;
        }
    }
    
    /**
     * Sets the "PrevState" element
     */
    public void setPrevState(java.lang.String prevState)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVSTATE$78, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREVSTATE$78);
            }
            target.setStringValue(prevState);
        }
    }
    
    /**
     * Sets (as xml) the "PrevState" element
     */
    public void xsetPrevState(com.idanalytics.products.idscore.request.State prevState)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.State target = null;
            target = (com.idanalytics.products.idscore.request.State)get_store().find_element_user(PREVSTATE$78, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.State)get_store().add_element_user(PREVSTATE$78);
            }
            target.set(prevState);
        }
    }
    
    /**
     * Unsets the "PrevState" element
     */
    public void unsetPrevState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREVSTATE$78, 0);
        }
    }
    
    /**
     * Gets the "PrevZip" element
     */
    public java.lang.String getPrevZip()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVZIP$80, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrevZip" element
     */
    public com.idanalytics.products.idscore.request.Zip xgetPrevZip()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Zip target = null;
            target = (com.idanalytics.products.idscore.request.Zip)get_store().find_element_user(PREVZIP$80, 0);
            return target;
        }
    }
    
    /**
     * True if has "PrevZip" element
     */
    public boolean isSetPrevZip()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREVZIP$80) != 0;
        }
    }
    
    /**
     * Sets the "PrevZip" element
     */
    public void setPrevZip(java.lang.String prevZip)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVZIP$80, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREVZIP$80);
            }
            target.setStringValue(prevZip);
        }
    }
    
    /**
     * Sets (as xml) the "PrevZip" element
     */
    public void xsetPrevZip(com.idanalytics.products.idscore.request.Zip prevZip)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Zip target = null;
            target = (com.idanalytics.products.idscore.request.Zip)get_store().find_element_user(PREVZIP$80, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Zip)get_store().add_element_user(PREVZIP$80);
            }
            target.set(prevZip);
        }
    }
    
    /**
     * Unsets the "PrevZip" element
     */
    public void unsetPrevZip()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREVZIP$80, 0);
        }
    }
    
    /**
     * Gets the "PrevCountry" element
     */
    public java.lang.String getPrevCountry()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVCOUNTRY$82, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrevCountry" element
     */
    public com.idanalytics.products.idscore.request.Country xgetPrevCountry()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Country target = null;
            target = (com.idanalytics.products.idscore.request.Country)get_store().find_element_user(PREVCOUNTRY$82, 0);
            return target;
        }
    }
    
    /**
     * True if has "PrevCountry" element
     */
    public boolean isSetPrevCountry()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREVCOUNTRY$82) != 0;
        }
    }
    
    /**
     * Sets the "PrevCountry" element
     */
    public void setPrevCountry(java.lang.String prevCountry)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVCOUNTRY$82, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREVCOUNTRY$82);
            }
            target.setStringValue(prevCountry);
        }
    }
    
    /**
     * Sets (as xml) the "PrevCountry" element
     */
    public void xsetPrevCountry(com.idanalytics.products.idscore.request.Country prevCountry)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Country target = null;
            target = (com.idanalytics.products.idscore.request.Country)get_store().find_element_user(PREVCOUNTRY$82, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Country)get_store().add_element_user(PREVCOUNTRY$82);
            }
            target.set(prevCountry);
        }
    }
    
    /**
     * Unsets the "PrevCountry" element
     */
    public void unsetPrevCountry()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREVCOUNTRY$82, 0);
        }
    }
    
    /**
     * Gets the "PrevAddressSince" element
     */
    public java.lang.Object getPrevAddressSince()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVADDRESSSINCE$84, 0);
            if (target == null)
            {
                return null;
            }
            return target.getObjectValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrevAddressSince" element
     */
    public com.idanalytics.products.idscore.request.Date xgetPrevAddressSince()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Date target = null;
            target = (com.idanalytics.products.idscore.request.Date)get_store().find_element_user(PREVADDRESSSINCE$84, 0);
            return target;
        }
    }
    
    /**
     * True if has "PrevAddressSince" element
     */
    public boolean isSetPrevAddressSince()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREVADDRESSSINCE$84) != 0;
        }
    }
    
    /**
     * Sets the "PrevAddressSince" element
     */
    public void setPrevAddressSince(java.lang.Object prevAddressSince)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVADDRESSSINCE$84, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREVADDRESSSINCE$84);
            }
            target.setObjectValue(prevAddressSince);
        }
    }
    
    /**
     * Sets (as xml) the "PrevAddressSince" element
     */
    public void xsetPrevAddressSince(com.idanalytics.products.idscore.request.Date prevAddressSince)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Date target = null;
            target = (com.idanalytics.products.idscore.request.Date)get_store().find_element_user(PREVADDRESSSINCE$84, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Date)get_store().add_element_user(PREVADDRESSSINCE$84);
            }
            target.set(prevAddressSince);
        }
    }
    
    /**
     * Unsets the "PrevAddressSince" element
     */
    public void unsetPrevAddressSince()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREVADDRESSSINCE$84, 0);
        }
    }
    
    /**
     * Gets the "PrevOccupancy" element
     */
    public com.idanalytics.products.idscore.request.Occupancy.Enum getPrevOccupancy()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVOCCUPANCY$86, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.idscore.request.Occupancy.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrevOccupancy" element
     */
    public com.idanalytics.products.idscore.request.Occupancy xgetPrevOccupancy()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Occupancy target = null;
            target = (com.idanalytics.products.idscore.request.Occupancy)get_store().find_element_user(PREVOCCUPANCY$86, 0);
            return target;
        }
    }
    
    /**
     * True if has "PrevOccupancy" element
     */
    public boolean isSetPrevOccupancy()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREVOCCUPANCY$86) != 0;
        }
    }
    
    /**
     * Sets the "PrevOccupancy" element
     */
    public void setPrevOccupancy(com.idanalytics.products.idscore.request.Occupancy.Enum prevOccupancy)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVOCCUPANCY$86, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREVOCCUPANCY$86);
            }
            target.setEnumValue(prevOccupancy);
        }
    }
    
    /**
     * Sets (as xml) the "PrevOccupancy" element
     */
    public void xsetPrevOccupancy(com.idanalytics.products.idscore.request.Occupancy prevOccupancy)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Occupancy target = null;
            target = (com.idanalytics.products.idscore.request.Occupancy)get_store().find_element_user(PREVOCCUPANCY$86, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Occupancy)get_store().add_element_user(PREVOCCUPANCY$86);
            }
            target.set(prevOccupancy);
        }
    }
    
    /**
     * Unsets the "PrevOccupancy" element
     */
    public void unsetPrevOccupancy()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREVOCCUPANCY$86, 0);
        }
    }
    
    /**
     * Gets the "HomePhone" element
     */
    public java.lang.String getHomePhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(HOMEPHONE$88, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "HomePhone" element
     */
    public com.idanalytics.products.idscore.request.Phone xgetHomePhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().find_element_user(HOMEPHONE$88, 0);
            return target;
        }
    }
    
    /**
     * Sets the "HomePhone" element
     */
    public void setHomePhone(java.lang.String homePhone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(HOMEPHONE$88, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(HOMEPHONE$88);
            }
            target.setStringValue(homePhone);
        }
    }
    
    /**
     * Sets (as xml) the "HomePhone" element
     */
    public void xsetHomePhone(com.idanalytics.products.idscore.request.Phone homePhone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().find_element_user(HOMEPHONE$88, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Phone)get_store().add_element_user(HOMEPHONE$88);
            }
            target.set(homePhone);
        }
    }
    
    /**
     * Gets the "HomePhone2" element
     */
    public java.lang.String getHomePhone2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(HOMEPHONE2$90, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "HomePhone2" element
     */
    public com.idanalytics.products.idscore.request.Phone xgetHomePhone2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().find_element_user(HOMEPHONE2$90, 0);
            return target;
        }
    }
    
    /**
     * True if has "HomePhone2" element
     */
    public boolean isSetHomePhone2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(HOMEPHONE2$90) != 0;
        }
    }
    
    /**
     * Sets the "HomePhone2" element
     */
    public void setHomePhone2(java.lang.String homePhone2)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(HOMEPHONE2$90, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(HOMEPHONE2$90);
            }
            target.setStringValue(homePhone2);
        }
    }
    
    /**
     * Sets (as xml) the "HomePhone2" element
     */
    public void xsetHomePhone2(com.idanalytics.products.idscore.request.Phone homePhone2)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().find_element_user(HOMEPHONE2$90, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Phone)get_store().add_element_user(HOMEPHONE2$90);
            }
            target.set(homePhone2);
        }
    }
    
    /**
     * Unsets the "HomePhone2" element
     */
    public void unsetHomePhone2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(HOMEPHONE2$90, 0);
        }
    }
    
    /**
     * Gets the "MobilePhone" element
     */
    public java.lang.String getMobilePhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MOBILEPHONE$92, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "MobilePhone" element
     */
    public com.idanalytics.products.idscore.request.Phone xgetMobilePhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().find_element_user(MOBILEPHONE$92, 0);
            return target;
        }
    }
    
    /**
     * Sets the "MobilePhone" element
     */
    public void setMobilePhone(java.lang.String mobilePhone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MOBILEPHONE$92, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(MOBILEPHONE$92);
            }
            target.setStringValue(mobilePhone);
        }
    }
    
    /**
     * Sets (as xml) the "MobilePhone" element
     */
    public void xsetMobilePhone(com.idanalytics.products.idscore.request.Phone mobilePhone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().find_element_user(MOBILEPHONE$92, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Phone)get_store().add_element_user(MOBILEPHONE$92);
            }
            target.set(mobilePhone);
        }
    }
    
    /**
     * Gets the "WorkPhone" element
     */
    public java.lang.String getWorkPhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(WORKPHONE$94, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "WorkPhone" element
     */
    public com.idanalytics.products.idscore.request.Phone xgetWorkPhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().find_element_user(WORKPHONE$94, 0);
            return target;
        }
    }
    
    /**
     * Sets the "WorkPhone" element
     */
    public void setWorkPhone(java.lang.String workPhone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(WORKPHONE$94, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(WORKPHONE$94);
            }
            target.setStringValue(workPhone);
        }
    }
    
    /**
     * Sets (as xml) the "WorkPhone" element
     */
    public void xsetWorkPhone(com.idanalytics.products.idscore.request.Phone workPhone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().find_element_user(WORKPHONE$94, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Phone)get_store().add_element_user(WORKPHONE$94);
            }
            target.set(workPhone);
        }
    }
    
    /**
     * Gets the "PrevHomePhone" element
     */
    public java.lang.String getPrevHomePhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVHOMEPHONE$96, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrevHomePhone" element
     */
    public com.idanalytics.products.idscore.request.Phone xgetPrevHomePhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().find_element_user(PREVHOMEPHONE$96, 0);
            return target;
        }
    }
    
    /**
     * True if has "PrevHomePhone" element
     */
    public boolean isSetPrevHomePhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREVHOMEPHONE$96) != 0;
        }
    }
    
    /**
     * Sets the "PrevHomePhone" element
     */
    public void setPrevHomePhone(java.lang.String prevHomePhone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVHOMEPHONE$96, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREVHOMEPHONE$96);
            }
            target.setStringValue(prevHomePhone);
        }
    }
    
    /**
     * Sets (as xml) the "PrevHomePhone" element
     */
    public void xsetPrevHomePhone(com.idanalytics.products.idscore.request.Phone prevHomePhone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().find_element_user(PREVHOMEPHONE$96, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Phone)get_store().add_element_user(PREVHOMEPHONE$96);
            }
            target.set(prevHomePhone);
        }
    }
    
    /**
     * Unsets the "PrevHomePhone" element
     */
    public void unsetPrevHomePhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREVHOMEPHONE$96, 0);
        }
    }
    
    /**
     * Gets the "PrevHomePhone2" element
     */
    public java.lang.String getPrevHomePhone2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVHOMEPHONE2$98, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrevHomePhone2" element
     */
    public com.idanalytics.products.idscore.request.Phone xgetPrevHomePhone2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().find_element_user(PREVHOMEPHONE2$98, 0);
            return target;
        }
    }
    
    /**
     * True if has "PrevHomePhone2" element
     */
    public boolean isSetPrevHomePhone2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREVHOMEPHONE2$98) != 0;
        }
    }
    
    /**
     * Sets the "PrevHomePhone2" element
     */
    public void setPrevHomePhone2(java.lang.String prevHomePhone2)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVHOMEPHONE2$98, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREVHOMEPHONE2$98);
            }
            target.setStringValue(prevHomePhone2);
        }
    }
    
    /**
     * Sets (as xml) the "PrevHomePhone2" element
     */
    public void xsetPrevHomePhone2(com.idanalytics.products.idscore.request.Phone prevHomePhone2)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().find_element_user(PREVHOMEPHONE2$98, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Phone)get_store().add_element_user(PREVHOMEPHONE2$98);
            }
            target.set(prevHomePhone2);
        }
    }
    
    /**
     * Unsets the "PrevHomePhone2" element
     */
    public void unsetPrevHomePhone2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREVHOMEPHONE2$98, 0);
        }
    }
    
    /**
     * Gets the "PrevMobilePhone" element
     */
    public java.lang.String getPrevMobilePhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVMOBILEPHONE$100, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrevMobilePhone" element
     */
    public com.idanalytics.products.idscore.request.Phone xgetPrevMobilePhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().find_element_user(PREVMOBILEPHONE$100, 0);
            return target;
        }
    }
    
    /**
     * True if has "PrevMobilePhone" element
     */
    public boolean isSetPrevMobilePhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREVMOBILEPHONE$100) != 0;
        }
    }
    
    /**
     * Sets the "PrevMobilePhone" element
     */
    public void setPrevMobilePhone(java.lang.String prevMobilePhone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVMOBILEPHONE$100, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREVMOBILEPHONE$100);
            }
            target.setStringValue(prevMobilePhone);
        }
    }
    
    /**
     * Sets (as xml) the "PrevMobilePhone" element
     */
    public void xsetPrevMobilePhone(com.idanalytics.products.idscore.request.Phone prevMobilePhone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().find_element_user(PREVMOBILEPHONE$100, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Phone)get_store().add_element_user(PREVMOBILEPHONE$100);
            }
            target.set(prevMobilePhone);
        }
    }
    
    /**
     * Unsets the "PrevMobilePhone" element
     */
    public void unsetPrevMobilePhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREVMOBILEPHONE$100, 0);
        }
    }
    
    /**
     * Gets the "PrevWorkPhone" element
     */
    public java.lang.String getPrevWorkPhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVWORKPHONE$102, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrevWorkPhone" element
     */
    public com.idanalytics.products.idscore.request.Phone xgetPrevWorkPhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().find_element_user(PREVWORKPHONE$102, 0);
            return target;
        }
    }
    
    /**
     * True if has "PrevWorkPhone" element
     */
    public boolean isSetPrevWorkPhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREVWORKPHONE$102) != 0;
        }
    }
    
    /**
     * Sets the "PrevWorkPhone" element
     */
    public void setPrevWorkPhone(java.lang.String prevWorkPhone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVWORKPHONE$102, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREVWORKPHONE$102);
            }
            target.setStringValue(prevWorkPhone);
        }
    }
    
    /**
     * Sets (as xml) the "PrevWorkPhone" element
     */
    public void xsetPrevWorkPhone(com.idanalytics.products.idscore.request.Phone prevWorkPhone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().find_element_user(PREVWORKPHONE$102, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Phone)get_store().add_element_user(PREVWORKPHONE$102);
            }
            target.set(prevWorkPhone);
        }
    }
    
    /**
     * Unsets the "PrevWorkPhone" element
     */
    public void unsetPrevWorkPhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREVWORKPHONE$102, 0);
        }
    }
    
    /**
     * Gets the "Email" element
     */
    public java.lang.String getEmail()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EMAIL$104, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Email" element
     */
    public com.idanalytics.products.idscore.request.Email xgetEmail()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Email target = null;
            target = (com.idanalytics.products.idscore.request.Email)get_store().find_element_user(EMAIL$104, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Email" element
     */
    public void setEmail(java.lang.String email)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EMAIL$104, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(EMAIL$104);
            }
            target.setStringValue(email);
        }
    }
    
    /**
     * Sets (as xml) the "Email" element
     */
    public void xsetEmail(com.idanalytics.products.idscore.request.Email email)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Email target = null;
            target = (com.idanalytics.products.idscore.request.Email)get_store().find_element_user(EMAIL$104, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Email)get_store().add_element_user(EMAIL$104);
            }
            target.set(email);
        }
    }
    
    /**
     * Gets the "PrevEmail" element
     */
    public java.lang.String getPrevEmail()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVEMAIL$106, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrevEmail" element
     */
    public com.idanalytics.products.idscore.request.Email xgetPrevEmail()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Email target = null;
            target = (com.idanalytics.products.idscore.request.Email)get_store().find_element_user(PREVEMAIL$106, 0);
            return target;
        }
    }
    
    /**
     * True if has "PrevEmail" element
     */
    public boolean isSetPrevEmail()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREVEMAIL$106) != 0;
        }
    }
    
    /**
     * Sets the "PrevEmail" element
     */
    public void setPrevEmail(java.lang.String prevEmail)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVEMAIL$106, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREVEMAIL$106);
            }
            target.setStringValue(prevEmail);
        }
    }
    
    /**
     * Sets (as xml) the "PrevEmail" element
     */
    public void xsetPrevEmail(com.idanalytics.products.idscore.request.Email prevEmail)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Email target = null;
            target = (com.idanalytics.products.idscore.request.Email)get_store().find_element_user(PREVEMAIL$106, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Email)get_store().add_element_user(PREVEMAIL$106);
            }
            target.set(prevEmail);
        }
    }
    
    /**
     * Unsets the "PrevEmail" element
     */
    public void unsetPrevEmail()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREVEMAIL$106, 0);
        }
    }
    
    /**
     * Gets the "IDType" element
     */
    public java.lang.String getIDType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDTYPE$108, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IDType" element
     */
    public com.idanalytics.products.idscore.request.IDType xgetIDType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.IDType target = null;
            target = (com.idanalytics.products.idscore.request.IDType)get_store().find_element_user(IDTYPE$108, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IDType" element
     */
    public void setIDType(java.lang.String idType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDTYPE$108, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDTYPE$108);
            }
            target.setStringValue(idType);
        }
    }
    
    /**
     * Sets (as xml) the "IDType" element
     */
    public void xsetIDType(com.idanalytics.products.idscore.request.IDType idType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.IDType target = null;
            target = (com.idanalytics.products.idscore.request.IDType)get_store().find_element_user(IDTYPE$108, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.IDType)get_store().add_element_user(IDTYPE$108);
            }
            target.set(idType);
        }
    }
    
    /**
     * Gets the "IDOrigin" element
     */
    public java.lang.String getIDOrigin()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDORIGIN$110, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IDOrigin" element
     */
    public com.idanalytics.products.idscore.request.IDOrigin xgetIDOrigin()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.IDOrigin target = null;
            target = (com.idanalytics.products.idscore.request.IDOrigin)get_store().find_element_user(IDORIGIN$110, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IDOrigin" element
     */
    public void setIDOrigin(java.lang.String idOrigin)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDORIGIN$110, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDORIGIN$110);
            }
            target.setStringValue(idOrigin);
        }
    }
    
    /**
     * Sets (as xml) the "IDOrigin" element
     */
    public void xsetIDOrigin(com.idanalytics.products.idscore.request.IDOrigin idOrigin)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.IDOrigin target = null;
            target = (com.idanalytics.products.idscore.request.IDOrigin)get_store().find_element_user(IDORIGIN$110, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.IDOrigin)get_store().add_element_user(IDORIGIN$110);
            }
            target.set(idOrigin);
        }
    }
    
    /**
     * Gets the "IDNumber" element
     */
    public java.lang.String getIDNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDNUMBER$112, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IDNumber" element
     */
    public com.idanalytics.products.idscore.request.IDNumber xgetIDNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.IDNumber target = null;
            target = (com.idanalytics.products.idscore.request.IDNumber)get_store().find_element_user(IDNUMBER$112, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IDNumber" element
     */
    public void setIDNumber(java.lang.String idNumber)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDNUMBER$112, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDNUMBER$112);
            }
            target.setStringValue(idNumber);
        }
    }
    
    /**
     * Sets (as xml) the "IDNumber" element
     */
    public void xsetIDNumber(com.idanalytics.products.idscore.request.IDNumber idNumber)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.IDNumber target = null;
            target = (com.idanalytics.products.idscore.request.IDNumber)get_store().find_element_user(IDNUMBER$112, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.IDNumber)get_store().add_element_user(IDNUMBER$112);
            }
            target.set(idNumber);
        }
    }
    
    /**
     * Gets the "Employment" element
     */
    public com.idanalytics.products.idscore.request.Employment getEmployment()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Employment target = null;
            target = (com.idanalytics.products.idscore.request.Employment)get_store().find_element_user(EMPLOYMENT$114, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "Employment" element
     */
    public boolean isSetEmployment()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(EMPLOYMENT$114) != 0;
        }
    }
    
    /**
     * Sets the "Employment" element
     */
    public void setEmployment(com.idanalytics.products.idscore.request.Employment employment)
    {
        generatedSetterHelperImpl(employment, EMPLOYMENT$114, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "Employment" element
     */
    public com.idanalytics.products.idscore.request.Employment addNewEmployment()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Employment target = null;
            target = (com.idanalytics.products.idscore.request.Employment)get_store().add_element_user(EMPLOYMENT$114);
            return target;
        }
    }
    
    /**
     * Unsets the "Employment" element
     */
    public void unsetEmployment()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(EMPLOYMENT$114, 0);
        }
    }
}
