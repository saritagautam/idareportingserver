/*
 * An XML document type.
 * Localname: EventType
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.EventTypeDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * A document containing one EventType(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public class EventTypeDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.EventTypeDocument
{
    private static final long serialVersionUID = 1L;
    
    public EventTypeDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName EVENTTYPE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "EventType");
    
    
    /**
     * Gets the "EventType" element
     */
    public java.lang.String getEventType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EVENTTYPE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "EventType" element
     */
    public com.idanalytics.products.idscore.request.EventTypeDocument.EventType xgetEventType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.EventTypeDocument.EventType target = null;
            target = (com.idanalytics.products.idscore.request.EventTypeDocument.EventType)get_store().find_element_user(EVENTTYPE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "EventType" element
     */
    public void setEventType(java.lang.String eventType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EVENTTYPE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(EVENTTYPE$0);
            }
            target.setStringValue(eventType);
        }
    }
    
    /**
     * Sets (as xml) the "EventType" element
     */
    public void xsetEventType(com.idanalytics.products.idscore.request.EventTypeDocument.EventType eventType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.EventTypeDocument.EventType target = null;
            target = (com.idanalytics.products.idscore.request.EventTypeDocument.EventType)get_store().find_element_user(EVENTTYPE$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.EventTypeDocument.EventType)get_store().add_element_user(EVENTTYPE$0);
            }
            target.set(eventType);
        }
    }
    /**
     * An XML EventType(@http://idanalytics.com/products/idscore/request).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.EventTypeDocument$EventType.
     */
    public static class EventTypeImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.request.EventTypeDocument.EventType
    {
        private static final long serialVersionUID = 1L;
        
        public EventTypeImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected EventTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
