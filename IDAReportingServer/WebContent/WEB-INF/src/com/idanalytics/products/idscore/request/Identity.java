/*
 * XML Type:  Identity
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.Identity
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request;


/**
 * An XML Identity(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public interface Identity extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Identity.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("identitycb56type");
    
    /**
     * Gets the "SSN" element
     */
    java.lang.String getSSN();
    
    /**
     * Gets (as xml) the "SSN" element
     */
    com.idanalytics.products.idscore.request.SSN xgetSSN();
    
    /**
     * Sets the "SSN" element
     */
    void setSSN(java.lang.String ssn);
    
    /**
     * Sets (as xml) the "SSN" element
     */
    void xsetSSN(com.idanalytics.products.idscore.request.SSN ssn);
    
    /**
     * Gets the "SSNLast4" element
     */
    java.lang.String getSSNLast4();
    
    /**
     * Gets (as xml) the "SSNLast4" element
     */
    com.idanalytics.products.idscore.request.SSNLast4 xgetSSNLast4();
    
    /**
     * True if has "SSNLast4" element
     */
    boolean isSetSSNLast4();
    
    /**
     * Sets the "SSNLast4" element
     */
    void setSSNLast4(java.lang.String ssnLast4);
    
    /**
     * Sets (as xml) the "SSNLast4" element
     */
    void xsetSSNLast4(com.idanalytics.products.idscore.request.SSNLast4 ssnLast4);
    
    /**
     * Unsets the "SSNLast4" element
     */
    void unsetSSNLast4();
    
    /**
     * Gets the "SSNFirst8" element
     */
    java.lang.String getSSNFirst8();
    
    /**
     * Gets (as xml) the "SSNFirst8" element
     */
    com.idanalytics.products.common_v1.SSN8 xgetSSNFirst8();
    
    /**
     * True if has "SSNFirst8" element
     */
    boolean isSetSSNFirst8();
    
    /**
     * Sets the "SSNFirst8" element
     */
    void setSSNFirst8(java.lang.String ssnFirst8);
    
    /**
     * Sets (as xml) the "SSNFirst8" element
     */
    void xsetSSNFirst8(com.idanalytics.products.common_v1.SSN8 ssnFirst8);
    
    /**
     * Unsets the "SSNFirst8" element
     */
    void unsetSSNFirst8();
    
    /**
     * Gets the "SSNLast8" element
     */
    java.lang.String getSSNLast8();
    
    /**
     * Gets (as xml) the "SSNLast8" element
     */
    com.idanalytics.products.common_v1.SSN8 xgetSSNLast8();
    
    /**
     * True if has "SSNLast8" element
     */
    boolean isSetSSNLast8();
    
    /**
     * Sets the "SSNLast8" element
     */
    void setSSNLast8(java.lang.String ssnLast8);
    
    /**
     * Sets (as xml) the "SSNLast8" element
     */
    void xsetSSNLast8(com.idanalytics.products.common_v1.SSN8 ssnLast8);
    
    /**
     * Unsets the "SSNLast8" element
     */
    void unsetSSNLast8();
    
    /**
     * Gets the "Title" element
     */
    java.lang.String getTitle();
    
    /**
     * Gets (as xml) the "Title" element
     */
    com.idanalytics.products.idscore.request.Title xgetTitle();
    
    /**
     * Sets the "Title" element
     */
    void setTitle(java.lang.String title);
    
    /**
     * Sets (as xml) the "Title" element
     */
    void xsetTitle(com.idanalytics.products.idscore.request.Title title);
    
    /**
     * Gets the "FirstName" element
     */
    java.lang.String getFirstName();
    
    /**
     * Gets (as xml) the "FirstName" element
     */
    com.idanalytics.products.idscore.request.Name xgetFirstName();
    
    /**
     * Sets the "FirstName" element
     */
    void setFirstName(java.lang.String firstName);
    
    /**
     * Sets (as xml) the "FirstName" element
     */
    void xsetFirstName(com.idanalytics.products.idscore.request.Name firstName);
    
    /**
     * Gets the "MiddleName" element
     */
    java.lang.String getMiddleName();
    
    /**
     * Gets (as xml) the "MiddleName" element
     */
    com.idanalytics.products.idscore.request.Name xgetMiddleName();
    
    /**
     * Sets the "MiddleName" element
     */
    void setMiddleName(java.lang.String middleName);
    
    /**
     * Sets (as xml) the "MiddleName" element
     */
    void xsetMiddleName(com.idanalytics.products.idscore.request.Name middleName);
    
    /**
     * Gets the "LastName" element
     */
    java.lang.String getLastName();
    
    /**
     * Gets (as xml) the "LastName" element
     */
    com.idanalytics.products.idscore.request.Name xgetLastName();
    
    /**
     * Sets the "LastName" element
     */
    void setLastName(java.lang.String lastName);
    
    /**
     * Sets (as xml) the "LastName" element
     */
    void xsetLastName(com.idanalytics.products.idscore.request.Name lastName);
    
    /**
     * Gets the "Suffix" element
     */
    java.lang.String getSuffix();
    
    /**
     * Gets (as xml) the "Suffix" element
     */
    com.idanalytics.products.idscore.request.Name xgetSuffix();
    
    /**
     * Sets the "Suffix" element
     */
    void setSuffix(java.lang.String suffix);
    
    /**
     * Sets (as xml) the "Suffix" element
     */
    void xsetSuffix(com.idanalytics.products.idscore.request.Name suffix);
    
    /**
     * Gets the "DOB" element
     */
    java.lang.Object getDOB();
    
    /**
     * Gets (as xml) the "DOB" element
     */
    com.idanalytics.products.idscore.request.Date xgetDOB();
    
    /**
     * Sets the "DOB" element
     */
    void setDOB(java.lang.Object dob);
    
    /**
     * Sets (as xml) the "DOB" element
     */
    void xsetDOB(com.idanalytics.products.idscore.request.Date dob);
    
    /**
     * Gets the "Gender" element
     */
    java.lang.String getGender();
    
    /**
     * Gets (as xml) the "Gender" element
     */
    com.idanalytics.products.idscore.request.Gender xgetGender();
    
    /**
     * True if has "Gender" element
     */
    boolean isSetGender();
    
    /**
     * Sets the "Gender" element
     */
    void setGender(java.lang.String gender);
    
    /**
     * Sets (as xml) the "Gender" element
     */
    void xsetGender(com.idanalytics.products.idscore.request.Gender gender);
    
    /**
     * Unsets the "Gender" element
     */
    void unsetGender();
    
    /**
     * Gets the "PlaceOfBirth" element
     */
    java.lang.String getPlaceOfBirth();
    
    /**
     * Gets (as xml) the "PlaceOfBirth" element
     */
    com.idanalytics.products.idscore.request.PlaceOfBirth xgetPlaceOfBirth();
    
    /**
     * True if has "PlaceOfBirth" element
     */
    boolean isSetPlaceOfBirth();
    
    /**
     * Sets the "PlaceOfBirth" element
     */
    void setPlaceOfBirth(java.lang.String placeOfBirth);
    
    /**
     * Sets (as xml) the "PlaceOfBirth" element
     */
    void xsetPlaceOfBirth(com.idanalytics.products.idscore.request.PlaceOfBirth placeOfBirth);
    
    /**
     * Unsets the "PlaceOfBirth" element
     */
    void unsetPlaceOfBirth();
    
    /**
     * Gets the "MothersMaiden" element
     */
    java.lang.String getMothersMaiden();
    
    /**
     * Gets (as xml) the "MothersMaiden" element
     */
    com.idanalytics.products.idscore.request.MothersMaiden xgetMothersMaiden();
    
    /**
     * True if has "MothersMaiden" element
     */
    boolean isSetMothersMaiden();
    
    /**
     * Sets the "MothersMaiden" element
     */
    void setMothersMaiden(java.lang.String mothersMaiden);
    
    /**
     * Sets (as xml) the "MothersMaiden" element
     */
    void xsetMothersMaiden(com.idanalytics.products.idscore.request.MothersMaiden mothersMaiden);
    
    /**
     * Unsets the "MothersMaiden" element
     */
    void unsetMothersMaiden();
    
    /**
     * Gets the "Company" element
     */
    java.lang.String getCompany();
    
    /**
     * Gets (as xml) the "Company" element
     */
    com.idanalytics.products.common_v1.NormalizedString xgetCompany();
    
    /**
     * True if has "Company" element
     */
    boolean isSetCompany();
    
    /**
     * Sets the "Company" element
     */
    void setCompany(java.lang.String company);
    
    /**
     * Sets (as xml) the "Company" element
     */
    void xsetCompany(com.idanalytics.products.common_v1.NormalizedString company);
    
    /**
     * Unsets the "Company" element
     */
    void unsetCompany();
    
    /**
     * Gets the "Address" element
     */
    com.idanalytics.products.idscore.request.Address getAddress();
    
    /**
     * True if has "Address" element
     */
    boolean isSetAddress();
    
    /**
     * Sets the "Address" element
     */
    void setAddress(com.idanalytics.products.idscore.request.Address address);
    
    /**
     * Appends and returns a new empty "Address" element
     */
    com.idanalytics.products.idscore.request.Address addNewAddress();
    
    /**
     * Unsets the "Address" element
     */
    void unsetAddress();
    
    /**
     * Gets the "ParsedAddress" element
     */
    com.idanalytics.products.idscore.request.ParsedAddress getParsedAddress();
    
    /**
     * True if has "ParsedAddress" element
     */
    boolean isSetParsedAddress();
    
    /**
     * Sets the "ParsedAddress" element
     */
    void setParsedAddress(com.idanalytics.products.idscore.request.ParsedAddress parsedAddress);
    
    /**
     * Appends and returns a new empty "ParsedAddress" element
     */
    com.idanalytics.products.idscore.request.ParsedAddress addNewParsedAddress();
    
    /**
     * Unsets the "ParsedAddress" element
     */
    void unsetParsedAddress();
    
    /**
     * Gets the "City" element
     */
    java.lang.String getCity();
    
    /**
     * Gets (as xml) the "City" element
     */
    com.idanalytics.products.idscore.request.City xgetCity();
    
    /**
     * Sets the "City" element
     */
    void setCity(java.lang.String city);
    
    /**
     * Sets (as xml) the "City" element
     */
    void xsetCity(com.idanalytics.products.idscore.request.City city);
    
    /**
     * Gets the "State" element
     */
    java.lang.String getState();
    
    /**
     * Gets (as xml) the "State" element
     */
    com.idanalytics.products.idscore.request.State xgetState();
    
    /**
     * Sets the "State" element
     */
    void setState(java.lang.String state);
    
    /**
     * Sets (as xml) the "State" element
     */
    void xsetState(com.idanalytics.products.idscore.request.State state);
    
    /**
     * Gets the "Zip" element
     */
    java.lang.String getZip();
    
    /**
     * Gets (as xml) the "Zip" element
     */
    com.idanalytics.products.idscore.request.Zip xgetZip();
    
    /**
     * Sets the "Zip" element
     */
    void setZip(java.lang.String zip);
    
    /**
     * Sets (as xml) the "Zip" element
     */
    void xsetZip(com.idanalytics.products.idscore.request.Zip zip);
    
    /**
     * Gets the "Country" element
     */
    java.lang.String getCountry();
    
    /**
     * Gets (as xml) the "Country" element
     */
    com.idanalytics.products.idscore.request.Country xgetCountry();
    
    /**
     * True if has "Country" element
     */
    boolean isSetCountry();
    
    /**
     * Sets the "Country" element
     */
    void setCountry(java.lang.String country);
    
    /**
     * Sets (as xml) the "Country" element
     */
    void xsetCountry(com.idanalytics.products.idscore.request.Country country);
    
    /**
     * Unsets the "Country" element
     */
    void unsetCountry();
    
    /**
     * Gets the "TimeAtResidence" element
     */
    java.lang.String getTimeAtResidence();
    
    /**
     * Gets (as xml) the "TimeAtResidence" element
     */
    com.idanalytics.products.idscore.request.TimeAt xgetTimeAtResidence();
    
    /**
     * True if has "TimeAtResidence" element
     */
    boolean isSetTimeAtResidence();
    
    /**
     * Sets the "TimeAtResidence" element
     */
    void setTimeAtResidence(java.lang.String timeAtResidence);
    
    /**
     * Sets (as xml) the "TimeAtResidence" element
     */
    void xsetTimeAtResidence(com.idanalytics.products.idscore.request.TimeAt timeAtResidence);
    
    /**
     * Unsets the "TimeAtResidence" element
     */
    void unsetTimeAtResidence();
    
    /**
     * Gets the "AddressSince" element
     */
    java.lang.Object getAddressSince();
    
    /**
     * Gets (as xml) the "AddressSince" element
     */
    com.idanalytics.products.idscore.request.Date xgetAddressSince();
    
    /**
     * True if has "AddressSince" element
     */
    boolean isSetAddressSince();
    
    /**
     * Sets the "AddressSince" element
     */
    void setAddressSince(java.lang.Object addressSince);
    
    /**
     * Sets (as xml) the "AddressSince" element
     */
    void xsetAddressSince(com.idanalytics.products.idscore.request.Date addressSince);
    
    /**
     * Unsets the "AddressSince" element
     */
    void unsetAddressSince();
    
    /**
     * Gets the "Occupancy" element
     */
    com.idanalytics.products.idscore.request.Occupancy.Enum getOccupancy();
    
    /**
     * Gets (as xml) the "Occupancy" element
     */
    com.idanalytics.products.idscore.request.Occupancy xgetOccupancy();
    
    /**
     * True if has "Occupancy" element
     */
    boolean isSetOccupancy();
    
    /**
     * Sets the "Occupancy" element
     */
    void setOccupancy(com.idanalytics.products.idscore.request.Occupancy.Enum occupancy);
    
    /**
     * Sets (as xml) the "Occupancy" element
     */
    void xsetOccupancy(com.idanalytics.products.idscore.request.Occupancy occupancy);
    
    /**
     * Unsets the "Occupancy" element
     */
    void unsetOccupancy();
    
    /**
     * Gets the "PhysicalAddress" element
     */
    com.idanalytics.products.idscore.request.Address getPhysicalAddress();
    
    /**
     * True if has "PhysicalAddress" element
     */
    boolean isSetPhysicalAddress();
    
    /**
     * Sets the "PhysicalAddress" element
     */
    void setPhysicalAddress(com.idanalytics.products.idscore.request.Address physicalAddress);
    
    /**
     * Appends and returns a new empty "PhysicalAddress" element
     */
    com.idanalytics.products.idscore.request.Address addNewPhysicalAddress();
    
    /**
     * Unsets the "PhysicalAddress" element
     */
    void unsetPhysicalAddress();
    
    /**
     * Gets the "ParsedPhysicalAddress" element
     */
    com.idanalytics.products.idscore.request.ParsedAddress getParsedPhysicalAddress();
    
    /**
     * True if has "ParsedPhysicalAddress" element
     */
    boolean isSetParsedPhysicalAddress();
    
    /**
     * Sets the "ParsedPhysicalAddress" element
     */
    void setParsedPhysicalAddress(com.idanalytics.products.idscore.request.ParsedAddress parsedPhysicalAddress);
    
    /**
     * Appends and returns a new empty "ParsedPhysicalAddress" element
     */
    com.idanalytics.products.idscore.request.ParsedAddress addNewParsedPhysicalAddress();
    
    /**
     * Unsets the "ParsedPhysicalAddress" element
     */
    void unsetParsedPhysicalAddress();
    
    /**
     * Gets the "PhysicalCity" element
     */
    java.lang.String getPhysicalCity();
    
    /**
     * Gets (as xml) the "PhysicalCity" element
     */
    com.idanalytics.products.idscore.request.City xgetPhysicalCity();
    
    /**
     * True if has "PhysicalCity" element
     */
    boolean isSetPhysicalCity();
    
    /**
     * Sets the "PhysicalCity" element
     */
    void setPhysicalCity(java.lang.String physicalCity);
    
    /**
     * Sets (as xml) the "PhysicalCity" element
     */
    void xsetPhysicalCity(com.idanalytics.products.idscore.request.City physicalCity);
    
    /**
     * Unsets the "PhysicalCity" element
     */
    void unsetPhysicalCity();
    
    /**
     * Gets the "PhysicalState" element
     */
    java.lang.String getPhysicalState();
    
    /**
     * Gets (as xml) the "PhysicalState" element
     */
    com.idanalytics.products.idscore.request.State xgetPhysicalState();
    
    /**
     * True if has "PhysicalState" element
     */
    boolean isSetPhysicalState();
    
    /**
     * Sets the "PhysicalState" element
     */
    void setPhysicalState(java.lang.String physicalState);
    
    /**
     * Sets (as xml) the "PhysicalState" element
     */
    void xsetPhysicalState(com.idanalytics.products.idscore.request.State physicalState);
    
    /**
     * Unsets the "PhysicalState" element
     */
    void unsetPhysicalState();
    
    /**
     * Gets the "PhysicalZip" element
     */
    java.lang.String getPhysicalZip();
    
    /**
     * Gets (as xml) the "PhysicalZip" element
     */
    com.idanalytics.products.idscore.request.Zip xgetPhysicalZip();
    
    /**
     * True if has "PhysicalZip" element
     */
    boolean isSetPhysicalZip();
    
    /**
     * Sets the "PhysicalZip" element
     */
    void setPhysicalZip(java.lang.String physicalZip);
    
    /**
     * Sets (as xml) the "PhysicalZip" element
     */
    void xsetPhysicalZip(com.idanalytics.products.idscore.request.Zip physicalZip);
    
    /**
     * Unsets the "PhysicalZip" element
     */
    void unsetPhysicalZip();
    
    /**
     * Gets the "PhysicalCountry" element
     */
    java.lang.String getPhysicalCountry();
    
    /**
     * Gets (as xml) the "PhysicalCountry" element
     */
    com.idanalytics.products.idscore.request.Country xgetPhysicalCountry();
    
    /**
     * True if has "PhysicalCountry" element
     */
    boolean isSetPhysicalCountry();
    
    /**
     * Sets the "PhysicalCountry" element
     */
    void setPhysicalCountry(java.lang.String physicalCountry);
    
    /**
     * Sets (as xml) the "PhysicalCountry" element
     */
    void xsetPhysicalCountry(com.idanalytics.products.idscore.request.Country physicalCountry);
    
    /**
     * Unsets the "PhysicalCountry" element
     */
    void unsetPhysicalCountry();
    
    /**
     * Gets the "PrevSSN" element
     */
    java.lang.String getPrevSSN();
    
    /**
     * Gets (as xml) the "PrevSSN" element
     */
    com.idanalytics.products.idscore.request.SSN xgetPrevSSN();
    
    /**
     * True if has "PrevSSN" element
     */
    boolean isSetPrevSSN();
    
    /**
     * Sets the "PrevSSN" element
     */
    void setPrevSSN(java.lang.String prevSSN);
    
    /**
     * Sets (as xml) the "PrevSSN" element
     */
    void xsetPrevSSN(com.idanalytics.products.idscore.request.SSN prevSSN);
    
    /**
     * Unsets the "PrevSSN" element
     */
    void unsetPrevSSN();
    
    /**
     * Gets the "PrevTitle" element
     */
    java.lang.String getPrevTitle();
    
    /**
     * Gets (as xml) the "PrevTitle" element
     */
    com.idanalytics.products.idscore.request.Title xgetPrevTitle();
    
    /**
     * True if has "PrevTitle" element
     */
    boolean isSetPrevTitle();
    
    /**
     * Sets the "PrevTitle" element
     */
    void setPrevTitle(java.lang.String prevTitle);
    
    /**
     * Sets (as xml) the "PrevTitle" element
     */
    void xsetPrevTitle(com.idanalytics.products.idscore.request.Title prevTitle);
    
    /**
     * Unsets the "PrevTitle" element
     */
    void unsetPrevTitle();
    
    /**
     * Gets the "PrevFirstName" element
     */
    java.lang.String getPrevFirstName();
    
    /**
     * Gets (as xml) the "PrevFirstName" element
     */
    com.idanalytics.products.idscore.request.Name xgetPrevFirstName();
    
    /**
     * True if has "PrevFirstName" element
     */
    boolean isSetPrevFirstName();
    
    /**
     * Sets the "PrevFirstName" element
     */
    void setPrevFirstName(java.lang.String prevFirstName);
    
    /**
     * Sets (as xml) the "PrevFirstName" element
     */
    void xsetPrevFirstName(com.idanalytics.products.idscore.request.Name prevFirstName);
    
    /**
     * Unsets the "PrevFirstName" element
     */
    void unsetPrevFirstName();
    
    /**
     * Gets the "PrevMiddleName" element
     */
    java.lang.String getPrevMiddleName();
    
    /**
     * Gets (as xml) the "PrevMiddleName" element
     */
    com.idanalytics.products.idscore.request.Name xgetPrevMiddleName();
    
    /**
     * True if has "PrevMiddleName" element
     */
    boolean isSetPrevMiddleName();
    
    /**
     * Sets the "PrevMiddleName" element
     */
    void setPrevMiddleName(java.lang.String prevMiddleName);
    
    /**
     * Sets (as xml) the "PrevMiddleName" element
     */
    void xsetPrevMiddleName(com.idanalytics.products.idscore.request.Name prevMiddleName);
    
    /**
     * Unsets the "PrevMiddleName" element
     */
    void unsetPrevMiddleName();
    
    /**
     * Gets the "PrevLastName" element
     */
    java.lang.String getPrevLastName();
    
    /**
     * Gets (as xml) the "PrevLastName" element
     */
    com.idanalytics.products.idscore.request.Name xgetPrevLastName();
    
    /**
     * True if has "PrevLastName" element
     */
    boolean isSetPrevLastName();
    
    /**
     * Sets the "PrevLastName" element
     */
    void setPrevLastName(java.lang.String prevLastName);
    
    /**
     * Sets (as xml) the "PrevLastName" element
     */
    void xsetPrevLastName(com.idanalytics.products.idscore.request.Name prevLastName);
    
    /**
     * Unsets the "PrevLastName" element
     */
    void unsetPrevLastName();
    
    /**
     * Gets the "PrevSuffix" element
     */
    java.lang.String getPrevSuffix();
    
    /**
     * Gets (as xml) the "PrevSuffix" element
     */
    com.idanalytics.products.idscore.request.Name xgetPrevSuffix();
    
    /**
     * True if has "PrevSuffix" element
     */
    boolean isSetPrevSuffix();
    
    /**
     * Sets the "PrevSuffix" element
     */
    void setPrevSuffix(java.lang.String prevSuffix);
    
    /**
     * Sets (as xml) the "PrevSuffix" element
     */
    void xsetPrevSuffix(com.idanalytics.products.idscore.request.Name prevSuffix);
    
    /**
     * Unsets the "PrevSuffix" element
     */
    void unsetPrevSuffix();
    
    /**
     * Gets the "PrevDOB" element
     */
    java.lang.Object getPrevDOB();
    
    /**
     * Gets (as xml) the "PrevDOB" element
     */
    com.idanalytics.products.idscore.request.Date xgetPrevDOB();
    
    /**
     * True if has "PrevDOB" element
     */
    boolean isSetPrevDOB();
    
    /**
     * Sets the "PrevDOB" element
     */
    void setPrevDOB(java.lang.Object prevDOB);
    
    /**
     * Sets (as xml) the "PrevDOB" element
     */
    void xsetPrevDOB(com.idanalytics.products.idscore.request.Date prevDOB);
    
    /**
     * Unsets the "PrevDOB" element
     */
    void unsetPrevDOB();
    
    /**
     * Gets the "PrevAddress" element
     */
    com.idanalytics.products.idscore.request.Address getPrevAddress();
    
    /**
     * True if has "PrevAddress" element
     */
    boolean isSetPrevAddress();
    
    /**
     * Sets the "PrevAddress" element
     */
    void setPrevAddress(com.idanalytics.products.idscore.request.Address prevAddress);
    
    /**
     * Appends and returns a new empty "PrevAddress" element
     */
    com.idanalytics.products.idscore.request.Address addNewPrevAddress();
    
    /**
     * Unsets the "PrevAddress" element
     */
    void unsetPrevAddress();
    
    /**
     * Gets the "ParsedPrevAddress" element
     */
    com.idanalytics.products.idscore.request.ParsedAddress getParsedPrevAddress();
    
    /**
     * True if has "ParsedPrevAddress" element
     */
    boolean isSetParsedPrevAddress();
    
    /**
     * Sets the "ParsedPrevAddress" element
     */
    void setParsedPrevAddress(com.idanalytics.products.idscore.request.ParsedAddress parsedPrevAddress);
    
    /**
     * Appends and returns a new empty "ParsedPrevAddress" element
     */
    com.idanalytics.products.idscore.request.ParsedAddress addNewParsedPrevAddress();
    
    /**
     * Unsets the "ParsedPrevAddress" element
     */
    void unsetParsedPrevAddress();
    
    /**
     * Gets the "PrevCity" element
     */
    java.lang.String getPrevCity();
    
    /**
     * Gets (as xml) the "PrevCity" element
     */
    com.idanalytics.products.idscore.request.City xgetPrevCity();
    
    /**
     * True if has "PrevCity" element
     */
    boolean isSetPrevCity();
    
    /**
     * Sets the "PrevCity" element
     */
    void setPrevCity(java.lang.String prevCity);
    
    /**
     * Sets (as xml) the "PrevCity" element
     */
    void xsetPrevCity(com.idanalytics.products.idscore.request.City prevCity);
    
    /**
     * Unsets the "PrevCity" element
     */
    void unsetPrevCity();
    
    /**
     * Gets the "PrevState" element
     */
    java.lang.String getPrevState();
    
    /**
     * Gets (as xml) the "PrevState" element
     */
    com.idanalytics.products.idscore.request.State xgetPrevState();
    
    /**
     * True if has "PrevState" element
     */
    boolean isSetPrevState();
    
    /**
     * Sets the "PrevState" element
     */
    void setPrevState(java.lang.String prevState);
    
    /**
     * Sets (as xml) the "PrevState" element
     */
    void xsetPrevState(com.idanalytics.products.idscore.request.State prevState);
    
    /**
     * Unsets the "PrevState" element
     */
    void unsetPrevState();
    
    /**
     * Gets the "PrevZip" element
     */
    java.lang.String getPrevZip();
    
    /**
     * Gets (as xml) the "PrevZip" element
     */
    com.idanalytics.products.idscore.request.Zip xgetPrevZip();
    
    /**
     * True if has "PrevZip" element
     */
    boolean isSetPrevZip();
    
    /**
     * Sets the "PrevZip" element
     */
    void setPrevZip(java.lang.String prevZip);
    
    /**
     * Sets (as xml) the "PrevZip" element
     */
    void xsetPrevZip(com.idanalytics.products.idscore.request.Zip prevZip);
    
    /**
     * Unsets the "PrevZip" element
     */
    void unsetPrevZip();
    
    /**
     * Gets the "PrevCountry" element
     */
    java.lang.String getPrevCountry();
    
    /**
     * Gets (as xml) the "PrevCountry" element
     */
    com.idanalytics.products.idscore.request.Country xgetPrevCountry();
    
    /**
     * True if has "PrevCountry" element
     */
    boolean isSetPrevCountry();
    
    /**
     * Sets the "PrevCountry" element
     */
    void setPrevCountry(java.lang.String prevCountry);
    
    /**
     * Sets (as xml) the "PrevCountry" element
     */
    void xsetPrevCountry(com.idanalytics.products.idscore.request.Country prevCountry);
    
    /**
     * Unsets the "PrevCountry" element
     */
    void unsetPrevCountry();
    
    /**
     * Gets the "PrevAddressSince" element
     */
    java.lang.Object getPrevAddressSince();
    
    /**
     * Gets (as xml) the "PrevAddressSince" element
     */
    com.idanalytics.products.idscore.request.Date xgetPrevAddressSince();
    
    /**
     * True if has "PrevAddressSince" element
     */
    boolean isSetPrevAddressSince();
    
    /**
     * Sets the "PrevAddressSince" element
     */
    void setPrevAddressSince(java.lang.Object prevAddressSince);
    
    /**
     * Sets (as xml) the "PrevAddressSince" element
     */
    void xsetPrevAddressSince(com.idanalytics.products.idscore.request.Date prevAddressSince);
    
    /**
     * Unsets the "PrevAddressSince" element
     */
    void unsetPrevAddressSince();
    
    /**
     * Gets the "PrevOccupancy" element
     */
    com.idanalytics.products.idscore.request.Occupancy.Enum getPrevOccupancy();
    
    /**
     * Gets (as xml) the "PrevOccupancy" element
     */
    com.idanalytics.products.idscore.request.Occupancy xgetPrevOccupancy();
    
    /**
     * True if has "PrevOccupancy" element
     */
    boolean isSetPrevOccupancy();
    
    /**
     * Sets the "PrevOccupancy" element
     */
    void setPrevOccupancy(com.idanalytics.products.idscore.request.Occupancy.Enum prevOccupancy);
    
    /**
     * Sets (as xml) the "PrevOccupancy" element
     */
    void xsetPrevOccupancy(com.idanalytics.products.idscore.request.Occupancy prevOccupancy);
    
    /**
     * Unsets the "PrevOccupancy" element
     */
    void unsetPrevOccupancy();
    
    /**
     * Gets the "HomePhone" element
     */
    java.lang.String getHomePhone();
    
    /**
     * Gets (as xml) the "HomePhone" element
     */
    com.idanalytics.products.idscore.request.Phone xgetHomePhone();
    
    /**
     * Sets the "HomePhone" element
     */
    void setHomePhone(java.lang.String homePhone);
    
    /**
     * Sets (as xml) the "HomePhone" element
     */
    void xsetHomePhone(com.idanalytics.products.idscore.request.Phone homePhone);
    
    /**
     * Gets the "HomePhone2" element
     */
    java.lang.String getHomePhone2();
    
    /**
     * Gets (as xml) the "HomePhone2" element
     */
    com.idanalytics.products.idscore.request.Phone xgetHomePhone2();
    
    /**
     * True if has "HomePhone2" element
     */
    boolean isSetHomePhone2();
    
    /**
     * Sets the "HomePhone2" element
     */
    void setHomePhone2(java.lang.String homePhone2);
    
    /**
     * Sets (as xml) the "HomePhone2" element
     */
    void xsetHomePhone2(com.idanalytics.products.idscore.request.Phone homePhone2);
    
    /**
     * Unsets the "HomePhone2" element
     */
    void unsetHomePhone2();
    
    /**
     * Gets the "MobilePhone" element
     */
    java.lang.String getMobilePhone();
    
    /**
     * Gets (as xml) the "MobilePhone" element
     */
    com.idanalytics.products.idscore.request.Phone xgetMobilePhone();
    
    /**
     * Sets the "MobilePhone" element
     */
    void setMobilePhone(java.lang.String mobilePhone);
    
    /**
     * Sets (as xml) the "MobilePhone" element
     */
    void xsetMobilePhone(com.idanalytics.products.idscore.request.Phone mobilePhone);
    
    /**
     * Gets the "WorkPhone" element
     */
    java.lang.String getWorkPhone();
    
    /**
     * Gets (as xml) the "WorkPhone" element
     */
    com.idanalytics.products.idscore.request.Phone xgetWorkPhone();
    
    /**
     * Sets the "WorkPhone" element
     */
    void setWorkPhone(java.lang.String workPhone);
    
    /**
     * Sets (as xml) the "WorkPhone" element
     */
    void xsetWorkPhone(com.idanalytics.products.idscore.request.Phone workPhone);
    
    /**
     * Gets the "PrevHomePhone" element
     */
    java.lang.String getPrevHomePhone();
    
    /**
     * Gets (as xml) the "PrevHomePhone" element
     */
    com.idanalytics.products.idscore.request.Phone xgetPrevHomePhone();
    
    /**
     * True if has "PrevHomePhone" element
     */
    boolean isSetPrevHomePhone();
    
    /**
     * Sets the "PrevHomePhone" element
     */
    void setPrevHomePhone(java.lang.String prevHomePhone);
    
    /**
     * Sets (as xml) the "PrevHomePhone" element
     */
    void xsetPrevHomePhone(com.idanalytics.products.idscore.request.Phone prevHomePhone);
    
    /**
     * Unsets the "PrevHomePhone" element
     */
    void unsetPrevHomePhone();
    
    /**
     * Gets the "PrevHomePhone2" element
     */
    java.lang.String getPrevHomePhone2();
    
    /**
     * Gets (as xml) the "PrevHomePhone2" element
     */
    com.idanalytics.products.idscore.request.Phone xgetPrevHomePhone2();
    
    /**
     * True if has "PrevHomePhone2" element
     */
    boolean isSetPrevHomePhone2();
    
    /**
     * Sets the "PrevHomePhone2" element
     */
    void setPrevHomePhone2(java.lang.String prevHomePhone2);
    
    /**
     * Sets (as xml) the "PrevHomePhone2" element
     */
    void xsetPrevHomePhone2(com.idanalytics.products.idscore.request.Phone prevHomePhone2);
    
    /**
     * Unsets the "PrevHomePhone2" element
     */
    void unsetPrevHomePhone2();
    
    /**
     * Gets the "PrevMobilePhone" element
     */
    java.lang.String getPrevMobilePhone();
    
    /**
     * Gets (as xml) the "PrevMobilePhone" element
     */
    com.idanalytics.products.idscore.request.Phone xgetPrevMobilePhone();
    
    /**
     * True if has "PrevMobilePhone" element
     */
    boolean isSetPrevMobilePhone();
    
    /**
     * Sets the "PrevMobilePhone" element
     */
    void setPrevMobilePhone(java.lang.String prevMobilePhone);
    
    /**
     * Sets (as xml) the "PrevMobilePhone" element
     */
    void xsetPrevMobilePhone(com.idanalytics.products.idscore.request.Phone prevMobilePhone);
    
    /**
     * Unsets the "PrevMobilePhone" element
     */
    void unsetPrevMobilePhone();
    
    /**
     * Gets the "PrevWorkPhone" element
     */
    java.lang.String getPrevWorkPhone();
    
    /**
     * Gets (as xml) the "PrevWorkPhone" element
     */
    com.idanalytics.products.idscore.request.Phone xgetPrevWorkPhone();
    
    /**
     * True if has "PrevWorkPhone" element
     */
    boolean isSetPrevWorkPhone();
    
    /**
     * Sets the "PrevWorkPhone" element
     */
    void setPrevWorkPhone(java.lang.String prevWorkPhone);
    
    /**
     * Sets (as xml) the "PrevWorkPhone" element
     */
    void xsetPrevWorkPhone(com.idanalytics.products.idscore.request.Phone prevWorkPhone);
    
    /**
     * Unsets the "PrevWorkPhone" element
     */
    void unsetPrevWorkPhone();
    
    /**
     * Gets the "Email" element
     */
    java.lang.String getEmail();
    
    /**
     * Gets (as xml) the "Email" element
     */
    com.idanalytics.products.idscore.request.Email xgetEmail();
    
    /**
     * Sets the "Email" element
     */
    void setEmail(java.lang.String email);
    
    /**
     * Sets (as xml) the "Email" element
     */
    void xsetEmail(com.idanalytics.products.idscore.request.Email email);
    
    /**
     * Gets the "PrevEmail" element
     */
    java.lang.String getPrevEmail();
    
    /**
     * Gets (as xml) the "PrevEmail" element
     */
    com.idanalytics.products.idscore.request.Email xgetPrevEmail();
    
    /**
     * True if has "PrevEmail" element
     */
    boolean isSetPrevEmail();
    
    /**
     * Sets the "PrevEmail" element
     */
    void setPrevEmail(java.lang.String prevEmail);
    
    /**
     * Sets (as xml) the "PrevEmail" element
     */
    void xsetPrevEmail(com.idanalytics.products.idscore.request.Email prevEmail);
    
    /**
     * Unsets the "PrevEmail" element
     */
    void unsetPrevEmail();
    
    /**
     * Gets the "IDType" element
     */
    java.lang.String getIDType();
    
    /**
     * Gets (as xml) the "IDType" element
     */
    com.idanalytics.products.idscore.request.IDType xgetIDType();
    
    /**
     * Sets the "IDType" element
     */
    void setIDType(java.lang.String idType);
    
    /**
     * Sets (as xml) the "IDType" element
     */
    void xsetIDType(com.idanalytics.products.idscore.request.IDType idType);
    
    /**
     * Gets the "IDOrigin" element
     */
    java.lang.String getIDOrigin();
    
    /**
     * Gets (as xml) the "IDOrigin" element
     */
    com.idanalytics.products.idscore.request.IDOrigin xgetIDOrigin();
    
    /**
     * Sets the "IDOrigin" element
     */
    void setIDOrigin(java.lang.String idOrigin);
    
    /**
     * Sets (as xml) the "IDOrigin" element
     */
    void xsetIDOrigin(com.idanalytics.products.idscore.request.IDOrigin idOrigin);
    
    /**
     * Gets the "IDNumber" element
     */
    java.lang.String getIDNumber();
    
    /**
     * Gets (as xml) the "IDNumber" element
     */
    com.idanalytics.products.idscore.request.IDNumber xgetIDNumber();
    
    /**
     * Sets the "IDNumber" element
     */
    void setIDNumber(java.lang.String idNumber);
    
    /**
     * Sets (as xml) the "IDNumber" element
     */
    void xsetIDNumber(com.idanalytics.products.idscore.request.IDNumber idNumber);
    
    /**
     * Gets the "Employment" element
     */
    com.idanalytics.products.idscore.request.Employment getEmployment();
    
    /**
     * True if has "Employment" element
     */
    boolean isSetEmployment();
    
    /**
     * Sets the "Employment" element
     */
    void setEmployment(com.idanalytics.products.idscore.request.Employment employment);
    
    /**
     * Appends and returns a new empty "Employment" element
     */
    com.idanalytics.products.idscore.request.Employment addNewEmployment();
    
    /**
     * Unsets the "Employment" element
     */
    void unsetEmployment();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.idscore.request.Identity newInstance() {
          return (com.idanalytics.products.idscore.request.Identity) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.idscore.request.Identity newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.idscore.request.Identity) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.idscore.request.Identity parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.Identity) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.idscore.request.Identity parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.Identity) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.idscore.request.Identity parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.Identity) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.idscore.request.Identity parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.Identity) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.idscore.request.Identity parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.Identity) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.idscore.request.Identity parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.Identity) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.idscore.request.Identity parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.Identity) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.idscore.request.Identity parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.Identity) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.idscore.request.Identity parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.Identity) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.idscore.request.Identity parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.Identity) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.idscore.request.Identity parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.Identity) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.idscore.request.Identity parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.Identity) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.idscore.request.Identity parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.Identity) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.idscore.request.Identity parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.Identity) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.request.Identity parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.request.Identity) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.request.Identity parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.request.Identity) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
