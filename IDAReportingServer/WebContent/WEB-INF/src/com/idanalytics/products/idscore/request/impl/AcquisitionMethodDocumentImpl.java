/*
 * An XML document type.
 * Localname: AcquisitionMethod
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.AcquisitionMethodDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * A document containing one AcquisitionMethod(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public class AcquisitionMethodDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.AcquisitionMethodDocument
{
    private static final long serialVersionUID = 1L;
    
    public AcquisitionMethodDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ACQUISITIONMETHOD$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "AcquisitionMethod");
    
    
    /**
     * Gets the "AcquisitionMethod" element
     */
    public java.lang.String getAcquisitionMethod()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ACQUISITIONMETHOD$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "AcquisitionMethod" element
     */
    public com.idanalytics.products.idscore.request.AcquisitionMethodDocument.AcquisitionMethod xgetAcquisitionMethod()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.AcquisitionMethodDocument.AcquisitionMethod target = null;
            target = (com.idanalytics.products.idscore.request.AcquisitionMethodDocument.AcquisitionMethod)get_store().find_element_user(ACQUISITIONMETHOD$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "AcquisitionMethod" element
     */
    public void setAcquisitionMethod(java.lang.String acquisitionMethod)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ACQUISITIONMETHOD$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ACQUISITIONMETHOD$0);
            }
            target.setStringValue(acquisitionMethod);
        }
    }
    
    /**
     * Sets (as xml) the "AcquisitionMethod" element
     */
    public void xsetAcquisitionMethod(com.idanalytics.products.idscore.request.AcquisitionMethodDocument.AcquisitionMethod acquisitionMethod)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.AcquisitionMethodDocument.AcquisitionMethod target = null;
            target = (com.idanalytics.products.idscore.request.AcquisitionMethodDocument.AcquisitionMethod)get_store().find_element_user(ACQUISITIONMETHOD$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.AcquisitionMethodDocument.AcquisitionMethod)get_store().add_element_user(ACQUISITIONMETHOD$0);
            }
            target.set(acquisitionMethod);
        }
    }
    /**
     * An XML AcquisitionMethod(@http://idanalytics.com/products/idscore/request).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.AcquisitionMethodDocument$AcquisitionMethod.
     */
    public static class AcquisitionMethodImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.request.AcquisitionMethodDocument.AcquisitionMethod
    {
        private static final long serialVersionUID = 1L;
        
        public AcquisitionMethodImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected AcquisitionMethodImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
