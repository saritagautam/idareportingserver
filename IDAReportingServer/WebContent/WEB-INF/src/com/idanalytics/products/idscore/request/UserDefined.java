/*
 * XML Type:  UserDefined
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.UserDefined
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request;


/**
 * An XML UserDefined(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public interface UserDefined extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(UserDefined.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("userdefinedc2a4type");
    
    /**
     * Gets the "udf1" element
     */
    java.lang.String getUdf1();
    
    /**
     * Gets (as xml) the "udf1" element
     */
    com.idanalytics.products.idscore.request.UserDefinedField xgetUdf1();
    
    /**
     * True if has "udf1" element
     */
    boolean isSetUdf1();
    
    /**
     * Sets the "udf1" element
     */
    void setUdf1(java.lang.String udf1);
    
    /**
     * Sets (as xml) the "udf1" element
     */
    void xsetUdf1(com.idanalytics.products.idscore.request.UserDefinedField udf1);
    
    /**
     * Unsets the "udf1" element
     */
    void unsetUdf1();
    
    /**
     * Gets the "udf2" element
     */
    java.lang.String getUdf2();
    
    /**
     * Gets (as xml) the "udf2" element
     */
    com.idanalytics.products.idscore.request.UserDefinedField xgetUdf2();
    
    /**
     * True if has "udf2" element
     */
    boolean isSetUdf2();
    
    /**
     * Sets the "udf2" element
     */
    void setUdf2(java.lang.String udf2);
    
    /**
     * Sets (as xml) the "udf2" element
     */
    void xsetUdf2(com.idanalytics.products.idscore.request.UserDefinedField udf2);
    
    /**
     * Unsets the "udf2" element
     */
    void unsetUdf2();
    
    /**
     * Gets the "udf3" element
     */
    java.lang.String getUdf3();
    
    /**
     * Gets (as xml) the "udf3" element
     */
    com.idanalytics.products.idscore.request.UserDefinedField xgetUdf3();
    
    /**
     * True if has "udf3" element
     */
    boolean isSetUdf3();
    
    /**
     * Sets the "udf3" element
     */
    void setUdf3(java.lang.String udf3);
    
    /**
     * Sets (as xml) the "udf3" element
     */
    void xsetUdf3(com.idanalytics.products.idscore.request.UserDefinedField udf3);
    
    /**
     * Unsets the "udf3" element
     */
    void unsetUdf3();
    
    /**
     * Gets the "udf4" element
     */
    java.lang.String getUdf4();
    
    /**
     * Gets (as xml) the "udf4" element
     */
    com.idanalytics.products.idscore.request.UserDefinedField xgetUdf4();
    
    /**
     * True if has "udf4" element
     */
    boolean isSetUdf4();
    
    /**
     * Sets the "udf4" element
     */
    void setUdf4(java.lang.String udf4);
    
    /**
     * Sets (as xml) the "udf4" element
     */
    void xsetUdf4(com.idanalytics.products.idscore.request.UserDefinedField udf4);
    
    /**
     * Unsets the "udf4" element
     */
    void unsetUdf4();
    
    /**
     * Gets the "udf5" element
     */
    java.lang.String getUdf5();
    
    /**
     * Gets (as xml) the "udf5" element
     */
    com.idanalytics.products.idscore.request.UserDefinedField xgetUdf5();
    
    /**
     * True if has "udf5" element
     */
    boolean isSetUdf5();
    
    /**
     * Sets the "udf5" element
     */
    void setUdf5(java.lang.String udf5);
    
    /**
     * Sets (as xml) the "udf5" element
     */
    void xsetUdf5(com.idanalytics.products.idscore.request.UserDefinedField udf5);
    
    /**
     * Unsets the "udf5" element
     */
    void unsetUdf5();
    
    /**
     * Gets the "udf6" element
     */
    java.lang.String getUdf6();
    
    /**
     * Gets (as xml) the "udf6" element
     */
    com.idanalytics.products.idscore.request.UserDefinedField xgetUdf6();
    
    /**
     * True if has "udf6" element
     */
    boolean isSetUdf6();
    
    /**
     * Sets the "udf6" element
     */
    void setUdf6(java.lang.String udf6);
    
    /**
     * Sets (as xml) the "udf6" element
     */
    void xsetUdf6(com.idanalytics.products.idscore.request.UserDefinedField udf6);
    
    /**
     * Unsets the "udf6" element
     */
    void unsetUdf6();
    
    /**
     * Gets the "udf7" element
     */
    java.lang.String getUdf7();
    
    /**
     * Gets (as xml) the "udf7" element
     */
    com.idanalytics.products.idscore.request.UserDefinedField xgetUdf7();
    
    /**
     * True if has "udf7" element
     */
    boolean isSetUdf7();
    
    /**
     * Sets the "udf7" element
     */
    void setUdf7(java.lang.String udf7);
    
    /**
     * Sets (as xml) the "udf7" element
     */
    void xsetUdf7(com.idanalytics.products.idscore.request.UserDefinedField udf7);
    
    /**
     * Unsets the "udf7" element
     */
    void unsetUdf7();
    
    /**
     * Gets the "udf8" element
     */
    java.lang.String getUdf8();
    
    /**
     * Gets (as xml) the "udf8" element
     */
    com.idanalytics.products.idscore.request.UserDefinedField xgetUdf8();
    
    /**
     * True if has "udf8" element
     */
    boolean isSetUdf8();
    
    /**
     * Sets the "udf8" element
     */
    void setUdf8(java.lang.String udf8);
    
    /**
     * Sets (as xml) the "udf8" element
     */
    void xsetUdf8(com.idanalytics.products.idscore.request.UserDefinedField udf8);
    
    /**
     * Unsets the "udf8" element
     */
    void unsetUdf8();
    
    /**
     * Gets the "udf9" element
     */
    java.lang.String getUdf9();
    
    /**
     * Gets (as xml) the "udf9" element
     */
    com.idanalytics.products.idscore.request.UserDefinedField xgetUdf9();
    
    /**
     * True if has "udf9" element
     */
    boolean isSetUdf9();
    
    /**
     * Sets the "udf9" element
     */
    void setUdf9(java.lang.String udf9);
    
    /**
     * Sets (as xml) the "udf9" element
     */
    void xsetUdf9(com.idanalytics.products.idscore.request.UserDefinedField udf9);
    
    /**
     * Unsets the "udf9" element
     */
    void unsetUdf9();
    
    /**
     * Gets the "udf10" element
     */
    java.lang.String getUdf10();
    
    /**
     * Gets (as xml) the "udf10" element
     */
    com.idanalytics.products.idscore.request.UserDefinedField xgetUdf10();
    
    /**
     * True if has "udf10" element
     */
    boolean isSetUdf10();
    
    /**
     * Sets the "udf10" element
     */
    void setUdf10(java.lang.String udf10);
    
    /**
     * Sets (as xml) the "udf10" element
     */
    void xsetUdf10(com.idanalytics.products.idscore.request.UserDefinedField udf10);
    
    /**
     * Unsets the "udf10" element
     */
    void unsetUdf10();
    
    /**
     * Gets the "udf11" element
     */
    java.lang.String getUdf11();
    
    /**
     * Gets (as xml) the "udf11" element
     */
    com.idanalytics.products.idscore.request.UserDefinedField xgetUdf11();
    
    /**
     * True if has "udf11" element
     */
    boolean isSetUdf11();
    
    /**
     * Sets the "udf11" element
     */
    void setUdf11(java.lang.String udf11);
    
    /**
     * Sets (as xml) the "udf11" element
     */
    void xsetUdf11(com.idanalytics.products.idscore.request.UserDefinedField udf11);
    
    /**
     * Unsets the "udf11" element
     */
    void unsetUdf11();
    
    /**
     * Gets the "udf12" element
     */
    java.lang.String getUdf12();
    
    /**
     * Gets (as xml) the "udf12" element
     */
    com.idanalytics.products.idscore.request.UserDefinedField xgetUdf12();
    
    /**
     * True if has "udf12" element
     */
    boolean isSetUdf12();
    
    /**
     * Sets the "udf12" element
     */
    void setUdf12(java.lang.String udf12);
    
    /**
     * Sets (as xml) the "udf12" element
     */
    void xsetUdf12(com.idanalytics.products.idscore.request.UserDefinedField udf12);
    
    /**
     * Unsets the "udf12" element
     */
    void unsetUdf12();
    
    /**
     * Gets the "udf13" element
     */
    java.lang.String getUdf13();
    
    /**
     * Gets (as xml) the "udf13" element
     */
    com.idanalytics.products.idscore.request.UserDefinedField xgetUdf13();
    
    /**
     * True if has "udf13" element
     */
    boolean isSetUdf13();
    
    /**
     * Sets the "udf13" element
     */
    void setUdf13(java.lang.String udf13);
    
    /**
     * Sets (as xml) the "udf13" element
     */
    void xsetUdf13(com.idanalytics.products.idscore.request.UserDefinedField udf13);
    
    /**
     * Unsets the "udf13" element
     */
    void unsetUdf13();
    
    /**
     * Gets the "udf14" element
     */
    java.lang.String getUdf14();
    
    /**
     * Gets (as xml) the "udf14" element
     */
    com.idanalytics.products.idscore.request.UserDefinedField xgetUdf14();
    
    /**
     * True if has "udf14" element
     */
    boolean isSetUdf14();
    
    /**
     * Sets the "udf14" element
     */
    void setUdf14(java.lang.String udf14);
    
    /**
     * Sets (as xml) the "udf14" element
     */
    void xsetUdf14(com.idanalytics.products.idscore.request.UserDefinedField udf14);
    
    /**
     * Unsets the "udf14" element
     */
    void unsetUdf14();
    
    /**
     * Gets the "udf15" element
     */
    java.lang.String getUdf15();
    
    /**
     * Gets (as xml) the "udf15" element
     */
    com.idanalytics.products.idscore.request.UserDefinedField xgetUdf15();
    
    /**
     * True if has "udf15" element
     */
    boolean isSetUdf15();
    
    /**
     * Sets the "udf15" element
     */
    void setUdf15(java.lang.String udf15);
    
    /**
     * Sets (as xml) the "udf15" element
     */
    void xsetUdf15(com.idanalytics.products.idscore.request.UserDefinedField udf15);
    
    /**
     * Unsets the "udf15" element
     */
    void unsetUdf15();
    
    /**
     * Gets the "udf16" element
     */
    java.lang.String getUdf16();
    
    /**
     * Gets (as xml) the "udf16" element
     */
    com.idanalytics.products.idscore.request.UserDefinedField xgetUdf16();
    
    /**
     * True if has "udf16" element
     */
    boolean isSetUdf16();
    
    /**
     * Sets the "udf16" element
     */
    void setUdf16(java.lang.String udf16);
    
    /**
     * Sets (as xml) the "udf16" element
     */
    void xsetUdf16(com.idanalytics.products.idscore.request.UserDefinedField udf16);
    
    /**
     * Unsets the "udf16" element
     */
    void unsetUdf16();
    
    /**
     * Gets the "udf17" element
     */
    java.lang.String getUdf17();
    
    /**
     * Gets (as xml) the "udf17" element
     */
    com.idanalytics.products.idscore.request.UserDefinedField xgetUdf17();
    
    /**
     * True if has "udf17" element
     */
    boolean isSetUdf17();
    
    /**
     * Sets the "udf17" element
     */
    void setUdf17(java.lang.String udf17);
    
    /**
     * Sets (as xml) the "udf17" element
     */
    void xsetUdf17(com.idanalytics.products.idscore.request.UserDefinedField udf17);
    
    /**
     * Unsets the "udf17" element
     */
    void unsetUdf17();
    
    /**
     * Gets the "udf18" element
     */
    java.lang.String getUdf18();
    
    /**
     * Gets (as xml) the "udf18" element
     */
    com.idanalytics.products.idscore.request.UserDefinedField xgetUdf18();
    
    /**
     * True if has "udf18" element
     */
    boolean isSetUdf18();
    
    /**
     * Sets the "udf18" element
     */
    void setUdf18(java.lang.String udf18);
    
    /**
     * Sets (as xml) the "udf18" element
     */
    void xsetUdf18(com.idanalytics.products.idscore.request.UserDefinedField udf18);
    
    /**
     * Unsets the "udf18" element
     */
    void unsetUdf18();
    
    /**
     * Gets the "udf19" element
     */
    java.lang.String getUdf19();
    
    /**
     * Gets (as xml) the "udf19" element
     */
    com.idanalytics.products.idscore.request.UserDefinedField xgetUdf19();
    
    /**
     * True if has "udf19" element
     */
    boolean isSetUdf19();
    
    /**
     * Sets the "udf19" element
     */
    void setUdf19(java.lang.String udf19);
    
    /**
     * Sets (as xml) the "udf19" element
     */
    void xsetUdf19(com.idanalytics.products.idscore.request.UserDefinedField udf19);
    
    /**
     * Unsets the "udf19" element
     */
    void unsetUdf19();
    
    /**
     * Gets the "udf20" element
     */
    java.lang.String getUdf20();
    
    /**
     * Gets (as xml) the "udf20" element
     */
    com.idanalytics.products.idscore.request.UserDefinedField xgetUdf20();
    
    /**
     * True if has "udf20" element
     */
    boolean isSetUdf20();
    
    /**
     * Sets the "udf20" element
     */
    void setUdf20(java.lang.String udf20);
    
    /**
     * Sets (as xml) the "udf20" element
     */
    void xsetUdf20(com.idanalytics.products.idscore.request.UserDefinedField udf20);
    
    /**
     * Unsets the "udf20" element
     */
    void unsetUdf20();
    
    /**
     * Gets the "udf21" element
     */
    java.lang.String getUdf21();
    
    /**
     * Gets (as xml) the "udf21" element
     */
    com.idanalytics.products.idscore.request.UserDefinedField xgetUdf21();
    
    /**
     * True if has "udf21" element
     */
    boolean isSetUdf21();
    
    /**
     * Sets the "udf21" element
     */
    void setUdf21(java.lang.String udf21);
    
    /**
     * Sets (as xml) the "udf21" element
     */
    void xsetUdf21(com.idanalytics.products.idscore.request.UserDefinedField udf21);
    
    /**
     * Unsets the "udf21" element
     */
    void unsetUdf21();
    
    /**
     * Gets the "udf22" element
     */
    java.lang.String getUdf22();
    
    /**
     * Gets (as xml) the "udf22" element
     */
    com.idanalytics.products.idscore.request.UserDefinedField xgetUdf22();
    
    /**
     * True if has "udf22" element
     */
    boolean isSetUdf22();
    
    /**
     * Sets the "udf22" element
     */
    void setUdf22(java.lang.String udf22);
    
    /**
     * Sets (as xml) the "udf22" element
     */
    void xsetUdf22(com.idanalytics.products.idscore.request.UserDefinedField udf22);
    
    /**
     * Unsets the "udf22" element
     */
    void unsetUdf22();
    
    /**
     * Gets the "udf23" element
     */
    java.lang.String getUdf23();
    
    /**
     * Gets (as xml) the "udf23" element
     */
    com.idanalytics.products.idscore.request.UserDefinedField xgetUdf23();
    
    /**
     * True if has "udf23" element
     */
    boolean isSetUdf23();
    
    /**
     * Sets the "udf23" element
     */
    void setUdf23(java.lang.String udf23);
    
    /**
     * Sets (as xml) the "udf23" element
     */
    void xsetUdf23(com.idanalytics.products.idscore.request.UserDefinedField udf23);
    
    /**
     * Unsets the "udf23" element
     */
    void unsetUdf23();
    
    /**
     * Gets the "udf24" element
     */
    java.lang.String getUdf24();
    
    /**
     * Gets (as xml) the "udf24" element
     */
    com.idanalytics.products.idscore.request.UserDefinedField xgetUdf24();
    
    /**
     * True if has "udf24" element
     */
    boolean isSetUdf24();
    
    /**
     * Sets the "udf24" element
     */
    void setUdf24(java.lang.String udf24);
    
    /**
     * Sets (as xml) the "udf24" element
     */
    void xsetUdf24(com.idanalytics.products.idscore.request.UserDefinedField udf24);
    
    /**
     * Unsets the "udf24" element
     */
    void unsetUdf24();
    
    /**
     * Gets the "udf25" element
     */
    java.lang.String getUdf25();
    
    /**
     * Gets (as xml) the "udf25" element
     */
    com.idanalytics.products.idscore.request.UserDefinedField xgetUdf25();
    
    /**
     * True if has "udf25" element
     */
    boolean isSetUdf25();
    
    /**
     * Sets the "udf25" element
     */
    void setUdf25(java.lang.String udf25);
    
    /**
     * Sets (as xml) the "udf25" element
     */
    void xsetUdf25(com.idanalytics.products.idscore.request.UserDefinedField udf25);
    
    /**
     * Unsets the "udf25" element
     */
    void unsetUdf25();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.idscore.request.UserDefined newInstance() {
          return (com.idanalytics.products.idscore.request.UserDefined) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.idscore.request.UserDefined newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.idscore.request.UserDefined) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.idscore.request.UserDefined parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.UserDefined) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.idscore.request.UserDefined parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.UserDefined) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.idscore.request.UserDefined parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.UserDefined) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.idscore.request.UserDefined parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.UserDefined) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.idscore.request.UserDefined parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.UserDefined) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.idscore.request.UserDefined parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.UserDefined) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.idscore.request.UserDefined parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.UserDefined) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.idscore.request.UserDefined parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.UserDefined) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.idscore.request.UserDefined parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.UserDefined) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.idscore.request.UserDefined parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.UserDefined) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.idscore.request.UserDefined parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.UserDefined) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.idscore.request.UserDefined parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.UserDefined) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.idscore.request.UserDefined parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.UserDefined) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.idscore.request.UserDefined parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.UserDefined) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.request.UserDefined parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.request.UserDefined) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.request.UserDefined parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.request.UserDefined) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
