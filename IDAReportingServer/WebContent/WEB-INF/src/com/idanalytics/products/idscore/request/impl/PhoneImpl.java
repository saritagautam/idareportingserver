/*
 * XML Type:  Phone
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.Phone
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML Phone(@http://idanalytics.com/products/idscore/request).
 *
 * This is a union type. Instances are of one of the following types:
 *     com.idanalytics.products.idscore.request.Phone$Member
 *     com.idanalytics.products.idscore.request.Phone$Member2
 */
public class PhoneImpl extends org.apache.xmlbeans.impl.values.XmlUnionImpl implements com.idanalytics.products.idscore.request.Phone, com.idanalytics.products.idscore.request.Phone.Member, com.idanalytics.products.idscore.request.Phone.Member2
{
    private static final long serialVersionUID = 1L;
    
    public PhoneImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected PhoneImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
    /**
     * An anonymous inner XML type.
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.Phone$Member.
     */
    public static class MemberImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.request.Phone.Member
    {
        private static final long serialVersionUID = 1L;
        
        public MemberImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected MemberImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
    /**
     * An anonymous inner XML type.
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.Phone$Member2.
     */
    public static class MemberImpl2 extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.request.Phone.Member2
    {
        private static final long serialVersionUID = 1L;
        
        public MemberImpl2(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected MemberImpl2(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
