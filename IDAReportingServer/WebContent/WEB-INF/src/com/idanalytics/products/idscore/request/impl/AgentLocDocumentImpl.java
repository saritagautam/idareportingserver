/*
 * An XML document type.
 * Localname: AgentLoc
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.AgentLocDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * A document containing one AgentLoc(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public class AgentLocDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.AgentLocDocument
{
    private static final long serialVersionUID = 1L;
    
    public AgentLocDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName AGENTLOC$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "AgentLoc");
    
    
    /**
     * Gets the "AgentLoc" element
     */
    public java.lang.String getAgentLoc()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(AGENTLOC$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "AgentLoc" element
     */
    public com.idanalytics.products.idscore.request.AgentLocDocument.AgentLoc xgetAgentLoc()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.AgentLocDocument.AgentLoc target = null;
            target = (com.idanalytics.products.idscore.request.AgentLocDocument.AgentLoc)get_store().find_element_user(AGENTLOC$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "AgentLoc" element
     */
    public void setAgentLoc(java.lang.String agentLoc)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(AGENTLOC$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(AGENTLOC$0);
            }
            target.setStringValue(agentLoc);
        }
    }
    
    /**
     * Sets (as xml) the "AgentLoc" element
     */
    public void xsetAgentLoc(com.idanalytics.products.idscore.request.AgentLocDocument.AgentLoc agentLoc)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.AgentLocDocument.AgentLoc target = null;
            target = (com.idanalytics.products.idscore.request.AgentLocDocument.AgentLoc)get_store().find_element_user(AGENTLOC$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.AgentLocDocument.AgentLoc)get_store().add_element_user(AGENTLOC$0);
            }
            target.set(agentLoc);
        }
    }
    /**
     * An XML AgentLoc(@http://idanalytics.com/products/idscore/request).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.AgentLocDocument$AgentLoc.
     */
    public static class AgentLocImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.request.AgentLocDocument.AgentLoc
    {
        private static final long serialVersionUID = 1L;
        
        public AgentLocImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected AgentLocImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
