/*
 * An XML document type.
 * Localname: MerchantID
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.MerchantIDDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * A document containing one MerchantID(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public class MerchantIDDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.MerchantIDDocument
{
    private static final long serialVersionUID = 1L;
    
    public MerchantIDDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName MERCHANTID$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "MerchantID");
    
    
    /**
     * Gets the "MerchantID" element
     */
    public java.lang.String getMerchantID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MERCHANTID$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "MerchantID" element
     */
    public com.idanalytics.products.idscore.request.MerchantIDDocument.MerchantID xgetMerchantID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.MerchantIDDocument.MerchantID target = null;
            target = (com.idanalytics.products.idscore.request.MerchantIDDocument.MerchantID)get_store().find_element_user(MERCHANTID$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "MerchantID" element
     */
    public void setMerchantID(java.lang.String merchantID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MERCHANTID$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(MERCHANTID$0);
            }
            target.setStringValue(merchantID);
        }
    }
    
    /**
     * Sets (as xml) the "MerchantID" element
     */
    public void xsetMerchantID(com.idanalytics.products.idscore.request.MerchantIDDocument.MerchantID merchantID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.MerchantIDDocument.MerchantID target = null;
            target = (com.idanalytics.products.idscore.request.MerchantIDDocument.MerchantID)get_store().find_element_user(MERCHANTID$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.MerchantIDDocument.MerchantID)get_store().add_element_user(MERCHANTID$0);
            }
            target.set(merchantID);
        }
    }
    /**
     * An XML MerchantID(@http://idanalytics.com/products/idscore/request).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.MerchantIDDocument$MerchantID.
     */
    public static class MerchantIDImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.request.MerchantIDDocument.MerchantID
    {
        private static final long serialVersionUID = 1L;
        
        public MerchantIDImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected MerchantIDImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
