/*
 * XML Type:  GenericPayment
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.GenericPayment
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML GenericPayment(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public class GenericPaymentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.GenericPayment
{
    private static final long serialVersionUID = 1L;
    
    public GenericPaymentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName PAYMENTCOMPANY$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PaymentCompany");
    private static final javax.xml.namespace.QName ACCOUNTID$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "AccountID");
    private static final javax.xml.namespace.QName TRANSACTIONID$4 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "TransactionID");
    private static final javax.xml.namespace.QName BANKROUTING$6 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "BankRouting");
    
    
    /**
     * Gets the "PaymentCompany" element
     */
    public java.lang.String getPaymentCompany()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PAYMENTCOMPANY$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PaymentCompany" element
     */
    public com.idanalytics.products.common_v1.NormalizedString xgetPaymentCompany()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.NormalizedString target = null;
            target = (com.idanalytics.products.common_v1.NormalizedString)get_store().find_element_user(PAYMENTCOMPANY$0, 0);
            return target;
        }
    }
    
    /**
     * True if has "PaymentCompany" element
     */
    public boolean isSetPaymentCompany()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PAYMENTCOMPANY$0) != 0;
        }
    }
    
    /**
     * Sets the "PaymentCompany" element
     */
    public void setPaymentCompany(java.lang.String paymentCompany)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PAYMENTCOMPANY$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PAYMENTCOMPANY$0);
            }
            target.setStringValue(paymentCompany);
        }
    }
    
    /**
     * Sets (as xml) the "PaymentCompany" element
     */
    public void xsetPaymentCompany(com.idanalytics.products.common_v1.NormalizedString paymentCompany)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.NormalizedString target = null;
            target = (com.idanalytics.products.common_v1.NormalizedString)get_store().find_element_user(PAYMENTCOMPANY$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.NormalizedString)get_store().add_element_user(PAYMENTCOMPANY$0);
            }
            target.set(paymentCompany);
        }
    }
    
    /**
     * Unsets the "PaymentCompany" element
     */
    public void unsetPaymentCompany()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PAYMENTCOMPANY$0, 0);
        }
    }
    
    /**
     * Gets the "AccountID" element
     */
    public java.lang.String getAccountID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ACCOUNTID$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "AccountID" element
     */
    public com.idanalytics.products.common_v1.NormalizedString xgetAccountID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.NormalizedString target = null;
            target = (com.idanalytics.products.common_v1.NormalizedString)get_store().find_element_user(ACCOUNTID$2, 0);
            return target;
        }
    }
    
    /**
     * True if has "AccountID" element
     */
    public boolean isSetAccountID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ACCOUNTID$2) != 0;
        }
    }
    
    /**
     * Sets the "AccountID" element
     */
    public void setAccountID(java.lang.String accountID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ACCOUNTID$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ACCOUNTID$2);
            }
            target.setStringValue(accountID);
        }
    }
    
    /**
     * Sets (as xml) the "AccountID" element
     */
    public void xsetAccountID(com.idanalytics.products.common_v1.NormalizedString accountID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.NormalizedString target = null;
            target = (com.idanalytics.products.common_v1.NormalizedString)get_store().find_element_user(ACCOUNTID$2, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.NormalizedString)get_store().add_element_user(ACCOUNTID$2);
            }
            target.set(accountID);
        }
    }
    
    /**
     * Unsets the "AccountID" element
     */
    public void unsetAccountID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ACCOUNTID$2, 0);
        }
    }
    
    /**
     * Gets the "TransactionID" element
     */
    public java.lang.String getTransactionID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TRANSACTIONID$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "TransactionID" element
     */
    public com.idanalytics.products.common_v1.NormalizedString xgetTransactionID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.NormalizedString target = null;
            target = (com.idanalytics.products.common_v1.NormalizedString)get_store().find_element_user(TRANSACTIONID$4, 0);
            return target;
        }
    }
    
    /**
     * True if has "TransactionID" element
     */
    public boolean isSetTransactionID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(TRANSACTIONID$4) != 0;
        }
    }
    
    /**
     * Sets the "TransactionID" element
     */
    public void setTransactionID(java.lang.String transactionID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TRANSACTIONID$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TRANSACTIONID$4);
            }
            target.setStringValue(transactionID);
        }
    }
    
    /**
     * Sets (as xml) the "TransactionID" element
     */
    public void xsetTransactionID(com.idanalytics.products.common_v1.NormalizedString transactionID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.NormalizedString target = null;
            target = (com.idanalytics.products.common_v1.NormalizedString)get_store().find_element_user(TRANSACTIONID$4, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.NormalizedString)get_store().add_element_user(TRANSACTIONID$4);
            }
            target.set(transactionID);
        }
    }
    
    /**
     * Unsets the "TransactionID" element
     */
    public void unsetTransactionID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(TRANSACTIONID$4, 0);
        }
    }
    
    /**
     * Gets the "BankRouting" element
     */
    public java.lang.String getBankRouting()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(BANKROUTING$6, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "BankRouting" element
     */
    public com.idanalytics.products.common_v1.NormalizedString xgetBankRouting()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.NormalizedString target = null;
            target = (com.idanalytics.products.common_v1.NormalizedString)get_store().find_element_user(BANKROUTING$6, 0);
            return target;
        }
    }
    
    /**
     * True if has "BankRouting" element
     */
    public boolean isSetBankRouting()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(BANKROUTING$6) != 0;
        }
    }
    
    /**
     * Sets the "BankRouting" element
     */
    public void setBankRouting(java.lang.String bankRouting)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(BANKROUTING$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(BANKROUTING$6);
            }
            target.setStringValue(bankRouting);
        }
    }
    
    /**
     * Sets (as xml) the "BankRouting" element
     */
    public void xsetBankRouting(com.idanalytics.products.common_v1.NormalizedString bankRouting)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.NormalizedString target = null;
            target = (com.idanalytics.products.common_v1.NormalizedString)get_store().find_element_user(BANKROUTING$6, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.NormalizedString)get_store().add_element_user(BANKROUTING$6);
            }
            target.set(bankRouting);
        }
    }
    
    /**
     * Unsets the "BankRouting" element
     */
    public void unsetBankRouting()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(BANKROUTING$6, 0);
        }
    }
}
