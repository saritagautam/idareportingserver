/*
 * XML Type:  Application
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.Application
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML Application(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public class ApplicationImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.Application
{
    private static final long serialVersionUID = 1L;
    
    public ApplicationImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CHANNEL$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Channel");
    private static final javax.xml.namespace.QName ACQUISITIONMETHOD$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "AcquisitionMethod");
    private static final javax.xml.namespace.QName AGENTLOC$4 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "AgentLoc");
    private static final javax.xml.namespace.QName AGENTID$6 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "AgentID");
    private static final javax.xml.namespace.QName SOURCEIP$8 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "SourceIP");
    private static final javax.xml.namespace.QName DEVICEID$10 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "DeviceID");
    private static final javax.xml.namespace.QName ENCODEDDEVICEINFO$12 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "EncodedDeviceInfo");
    private static final javax.xml.namespace.QName DEVICEIDSOURCE$14 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "DeviceIDSource");
    private static final javax.xml.namespace.QName PRIMARYDECISIONCODE$16 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrimaryDecisionCode");
    private static final javax.xml.namespace.QName SECONDARYDECISIONCODE$18 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "SecondaryDecisionCode");
    private static final javax.xml.namespace.QName PRIMARYPORTFOLIO$20 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrimaryPortfolio");
    private static final javax.xml.namespace.QName SECONDARYPORTFOLIO$22 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "SecondaryPortfolio");
    private static final javax.xml.namespace.QName CONFIRMEDFRAUD$24 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ConfirmedFraud");
    private static final javax.xml.namespace.QName PRIMARYFRAUDCODE$26 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrimaryFraudCode");
    private static final javax.xml.namespace.QName SECONDARYFRAUDCODE$28 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "SecondaryFraudCode");
    private static final javax.xml.namespace.QName CARETAILFLAG$30 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "CARetailFlag");
    private static final javax.xml.namespace.QName PRIMARYINCOME$32 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrimaryIncome");
    private static final javax.xml.namespace.QName OTHERINCOME$34 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "OtherIncome");
    private static final javax.xml.namespace.QName INFERREDINCOME$36 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "InferredIncome");
    private static final javax.xml.namespace.QName INFERREDINCOMEIND$38 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "InferredIncomeInd");
    private static final javax.xml.namespace.QName RECOMMENDEDCREDITLINE$40 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "RecommendedCreditLine");
    
    
    /**
     * Gets the "Channel" element
     */
    public java.lang.String getChannel()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CHANNEL$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Channel" element
     */
    public com.idanalytics.products.idscore.request.ChannelDocument.Channel xgetChannel()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ChannelDocument.Channel target = null;
            target = (com.idanalytics.products.idscore.request.ChannelDocument.Channel)get_store().find_element_user(CHANNEL$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Channel" element
     */
    public void setChannel(java.lang.String channel)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CHANNEL$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CHANNEL$0);
            }
            target.setStringValue(channel);
        }
    }
    
    /**
     * Sets (as xml) the "Channel" element
     */
    public void xsetChannel(com.idanalytics.products.idscore.request.ChannelDocument.Channel channel)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ChannelDocument.Channel target = null;
            target = (com.idanalytics.products.idscore.request.ChannelDocument.Channel)get_store().find_element_user(CHANNEL$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.ChannelDocument.Channel)get_store().add_element_user(CHANNEL$0);
            }
            target.set(channel);
        }
    }
    
    /**
     * Gets the "AcquisitionMethod" element
     */
    public java.lang.String getAcquisitionMethod()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ACQUISITIONMETHOD$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "AcquisitionMethod" element
     */
    public com.idanalytics.products.idscore.request.AcquisitionMethodDocument.AcquisitionMethod xgetAcquisitionMethod()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.AcquisitionMethodDocument.AcquisitionMethod target = null;
            target = (com.idanalytics.products.idscore.request.AcquisitionMethodDocument.AcquisitionMethod)get_store().find_element_user(ACQUISITIONMETHOD$2, 0);
            return target;
        }
    }
    
    /**
     * True if has "AcquisitionMethod" element
     */
    public boolean isSetAcquisitionMethod()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ACQUISITIONMETHOD$2) != 0;
        }
    }
    
    /**
     * Sets the "AcquisitionMethod" element
     */
    public void setAcquisitionMethod(java.lang.String acquisitionMethod)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ACQUISITIONMETHOD$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ACQUISITIONMETHOD$2);
            }
            target.setStringValue(acquisitionMethod);
        }
    }
    
    /**
     * Sets (as xml) the "AcquisitionMethod" element
     */
    public void xsetAcquisitionMethod(com.idanalytics.products.idscore.request.AcquisitionMethodDocument.AcquisitionMethod acquisitionMethod)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.AcquisitionMethodDocument.AcquisitionMethod target = null;
            target = (com.idanalytics.products.idscore.request.AcquisitionMethodDocument.AcquisitionMethod)get_store().find_element_user(ACQUISITIONMETHOD$2, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.AcquisitionMethodDocument.AcquisitionMethod)get_store().add_element_user(ACQUISITIONMETHOD$2);
            }
            target.set(acquisitionMethod);
        }
    }
    
    /**
     * Unsets the "AcquisitionMethod" element
     */
    public void unsetAcquisitionMethod()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ACQUISITIONMETHOD$2, 0);
        }
    }
    
    /**
     * Gets the "AgentLoc" element
     */
    public java.lang.String getAgentLoc()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(AGENTLOC$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "AgentLoc" element
     */
    public com.idanalytics.products.idscore.request.AgentLocDocument.AgentLoc xgetAgentLoc()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.AgentLocDocument.AgentLoc target = null;
            target = (com.idanalytics.products.idscore.request.AgentLocDocument.AgentLoc)get_store().find_element_user(AGENTLOC$4, 0);
            return target;
        }
    }
    
    /**
     * True if has "AgentLoc" element
     */
    public boolean isSetAgentLoc()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(AGENTLOC$4) != 0;
        }
    }
    
    /**
     * Sets the "AgentLoc" element
     */
    public void setAgentLoc(java.lang.String agentLoc)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(AGENTLOC$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(AGENTLOC$4);
            }
            target.setStringValue(agentLoc);
        }
    }
    
    /**
     * Sets (as xml) the "AgentLoc" element
     */
    public void xsetAgentLoc(com.idanalytics.products.idscore.request.AgentLocDocument.AgentLoc agentLoc)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.AgentLocDocument.AgentLoc target = null;
            target = (com.idanalytics.products.idscore.request.AgentLocDocument.AgentLoc)get_store().find_element_user(AGENTLOC$4, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.AgentLocDocument.AgentLoc)get_store().add_element_user(AGENTLOC$4);
            }
            target.set(agentLoc);
        }
    }
    
    /**
     * Unsets the "AgentLoc" element
     */
    public void unsetAgentLoc()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(AGENTLOC$4, 0);
        }
    }
    
    /**
     * Gets the "AgentID" element
     */
    public java.lang.String getAgentID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(AGENTID$6, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "AgentID" element
     */
    public com.idanalytics.products.idscore.request.AgentIDDocument.AgentID xgetAgentID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.AgentIDDocument.AgentID target = null;
            target = (com.idanalytics.products.idscore.request.AgentIDDocument.AgentID)get_store().find_element_user(AGENTID$6, 0);
            return target;
        }
    }
    
    /**
     * True if has "AgentID" element
     */
    public boolean isSetAgentID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(AGENTID$6) != 0;
        }
    }
    
    /**
     * Sets the "AgentID" element
     */
    public void setAgentID(java.lang.String agentID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(AGENTID$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(AGENTID$6);
            }
            target.setStringValue(agentID);
        }
    }
    
    /**
     * Sets (as xml) the "AgentID" element
     */
    public void xsetAgentID(com.idanalytics.products.idscore.request.AgentIDDocument.AgentID agentID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.AgentIDDocument.AgentID target = null;
            target = (com.idanalytics.products.idscore.request.AgentIDDocument.AgentID)get_store().find_element_user(AGENTID$6, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.AgentIDDocument.AgentID)get_store().add_element_user(AGENTID$6);
            }
            target.set(agentID);
        }
    }
    
    /**
     * Unsets the "AgentID" element
     */
    public void unsetAgentID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(AGENTID$6, 0);
        }
    }
    
    /**
     * Gets the "SourceIP" element
     */
    public java.lang.String getSourceIP()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SOURCEIP$8, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "SourceIP" element
     */
    public com.idanalytics.products.idscore.request.SourceIPDocument.SourceIP xgetSourceIP()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.SourceIPDocument.SourceIP target = null;
            target = (com.idanalytics.products.idscore.request.SourceIPDocument.SourceIP)get_store().find_element_user(SOURCEIP$8, 0);
            return target;
        }
    }
    
    /**
     * True if has "SourceIP" element
     */
    public boolean isSetSourceIP()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SOURCEIP$8) != 0;
        }
    }
    
    /**
     * Sets the "SourceIP" element
     */
    public void setSourceIP(java.lang.String sourceIP)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SOURCEIP$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SOURCEIP$8);
            }
            target.setStringValue(sourceIP);
        }
    }
    
    /**
     * Sets (as xml) the "SourceIP" element
     */
    public void xsetSourceIP(com.idanalytics.products.idscore.request.SourceIPDocument.SourceIP sourceIP)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.SourceIPDocument.SourceIP target = null;
            target = (com.idanalytics.products.idscore.request.SourceIPDocument.SourceIP)get_store().find_element_user(SOURCEIP$8, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.SourceIPDocument.SourceIP)get_store().add_element_user(SOURCEIP$8);
            }
            target.set(sourceIP);
        }
    }
    
    /**
     * Unsets the "SourceIP" element
     */
    public void unsetSourceIP()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SOURCEIP$8, 0);
        }
    }
    
    /**
     * Gets the "DeviceID" element
     */
    public java.lang.String getDeviceID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DEVICEID$10, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "DeviceID" element
     */
    public com.idanalytics.products.idscore.request.DeviceIDDocument.DeviceID xgetDeviceID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.DeviceIDDocument.DeviceID target = null;
            target = (com.idanalytics.products.idscore.request.DeviceIDDocument.DeviceID)get_store().find_element_user(DEVICEID$10, 0);
            return target;
        }
    }
    
    /**
     * True if has "DeviceID" element
     */
    public boolean isSetDeviceID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(DEVICEID$10) != 0;
        }
    }
    
    /**
     * Sets the "DeviceID" element
     */
    public void setDeviceID(java.lang.String deviceID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DEVICEID$10, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DEVICEID$10);
            }
            target.setStringValue(deviceID);
        }
    }
    
    /**
     * Sets (as xml) the "DeviceID" element
     */
    public void xsetDeviceID(com.idanalytics.products.idscore.request.DeviceIDDocument.DeviceID deviceID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.DeviceIDDocument.DeviceID target = null;
            target = (com.idanalytics.products.idscore.request.DeviceIDDocument.DeviceID)get_store().find_element_user(DEVICEID$10, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.DeviceIDDocument.DeviceID)get_store().add_element_user(DEVICEID$10);
            }
            target.set(deviceID);
        }
    }
    
    /**
     * Unsets the "DeviceID" element
     */
    public void unsetDeviceID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(DEVICEID$10, 0);
        }
    }
    
    /**
     * Gets the "EncodedDeviceInfo" element
     */
    public java.lang.String getEncodedDeviceInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ENCODEDDEVICEINFO$12, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "EncodedDeviceInfo" element
     */
    public com.idanalytics.products.idscore.request.EncodedDeviceInfoDocument.EncodedDeviceInfo xgetEncodedDeviceInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.EncodedDeviceInfoDocument.EncodedDeviceInfo target = null;
            target = (com.idanalytics.products.idscore.request.EncodedDeviceInfoDocument.EncodedDeviceInfo)get_store().find_element_user(ENCODEDDEVICEINFO$12, 0);
            return target;
        }
    }
    
    /**
     * True if has "EncodedDeviceInfo" element
     */
    public boolean isSetEncodedDeviceInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ENCODEDDEVICEINFO$12) != 0;
        }
    }
    
    /**
     * Sets the "EncodedDeviceInfo" element
     */
    public void setEncodedDeviceInfo(java.lang.String encodedDeviceInfo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ENCODEDDEVICEINFO$12, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ENCODEDDEVICEINFO$12);
            }
            target.setStringValue(encodedDeviceInfo);
        }
    }
    
    /**
     * Sets (as xml) the "EncodedDeviceInfo" element
     */
    public void xsetEncodedDeviceInfo(com.idanalytics.products.idscore.request.EncodedDeviceInfoDocument.EncodedDeviceInfo encodedDeviceInfo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.EncodedDeviceInfoDocument.EncodedDeviceInfo target = null;
            target = (com.idanalytics.products.idscore.request.EncodedDeviceInfoDocument.EncodedDeviceInfo)get_store().find_element_user(ENCODEDDEVICEINFO$12, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.EncodedDeviceInfoDocument.EncodedDeviceInfo)get_store().add_element_user(ENCODEDDEVICEINFO$12);
            }
            target.set(encodedDeviceInfo);
        }
    }
    
    /**
     * Unsets the "EncodedDeviceInfo" element
     */
    public void unsetEncodedDeviceInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ENCODEDDEVICEINFO$12, 0);
        }
    }
    
    /**
     * Gets the "DeviceIDSource" element
     */
    public java.lang.String getDeviceIDSource()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DEVICEIDSOURCE$14, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "DeviceIDSource" element
     */
    public com.idanalytics.products.idscore.request.DeviceIDSourceDocument.DeviceIDSource xgetDeviceIDSource()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.DeviceIDSourceDocument.DeviceIDSource target = null;
            target = (com.idanalytics.products.idscore.request.DeviceIDSourceDocument.DeviceIDSource)get_store().find_element_user(DEVICEIDSOURCE$14, 0);
            return target;
        }
    }
    
    /**
     * True if has "DeviceIDSource" element
     */
    public boolean isSetDeviceIDSource()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(DEVICEIDSOURCE$14) != 0;
        }
    }
    
    /**
     * Sets the "DeviceIDSource" element
     */
    public void setDeviceIDSource(java.lang.String deviceIDSource)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DEVICEIDSOURCE$14, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DEVICEIDSOURCE$14);
            }
            target.setStringValue(deviceIDSource);
        }
    }
    
    /**
     * Sets (as xml) the "DeviceIDSource" element
     */
    public void xsetDeviceIDSource(com.idanalytics.products.idscore.request.DeviceIDSourceDocument.DeviceIDSource deviceIDSource)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.DeviceIDSourceDocument.DeviceIDSource target = null;
            target = (com.idanalytics.products.idscore.request.DeviceIDSourceDocument.DeviceIDSource)get_store().find_element_user(DEVICEIDSOURCE$14, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.DeviceIDSourceDocument.DeviceIDSource)get_store().add_element_user(DEVICEIDSOURCE$14);
            }
            target.set(deviceIDSource);
        }
    }
    
    /**
     * Unsets the "DeviceIDSource" element
     */
    public void unsetDeviceIDSource()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(DEVICEIDSOURCE$14, 0);
        }
    }
    
    /**
     * Gets the "PrimaryDecisionCode" element
     */
    public java.lang.String getPrimaryDecisionCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PRIMARYDECISIONCODE$16, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrimaryDecisionCode" element
     */
    public com.idanalytics.products.idscore.request.PrimaryDecisionCodeDocument.PrimaryDecisionCode xgetPrimaryDecisionCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.PrimaryDecisionCodeDocument.PrimaryDecisionCode target = null;
            target = (com.idanalytics.products.idscore.request.PrimaryDecisionCodeDocument.PrimaryDecisionCode)get_store().find_element_user(PRIMARYDECISIONCODE$16, 0);
            return target;
        }
    }
    
    /**
     * True if has "PrimaryDecisionCode" element
     */
    public boolean isSetPrimaryDecisionCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PRIMARYDECISIONCODE$16) != 0;
        }
    }
    
    /**
     * Sets the "PrimaryDecisionCode" element
     */
    public void setPrimaryDecisionCode(java.lang.String primaryDecisionCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PRIMARYDECISIONCODE$16, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PRIMARYDECISIONCODE$16);
            }
            target.setStringValue(primaryDecisionCode);
        }
    }
    
    /**
     * Sets (as xml) the "PrimaryDecisionCode" element
     */
    public void xsetPrimaryDecisionCode(com.idanalytics.products.idscore.request.PrimaryDecisionCodeDocument.PrimaryDecisionCode primaryDecisionCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.PrimaryDecisionCodeDocument.PrimaryDecisionCode target = null;
            target = (com.idanalytics.products.idscore.request.PrimaryDecisionCodeDocument.PrimaryDecisionCode)get_store().find_element_user(PRIMARYDECISIONCODE$16, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.PrimaryDecisionCodeDocument.PrimaryDecisionCode)get_store().add_element_user(PRIMARYDECISIONCODE$16);
            }
            target.set(primaryDecisionCode);
        }
    }
    
    /**
     * Unsets the "PrimaryDecisionCode" element
     */
    public void unsetPrimaryDecisionCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PRIMARYDECISIONCODE$16, 0);
        }
    }
    
    /**
     * Gets the "SecondaryDecisionCode" element
     */
    public java.lang.String getSecondaryDecisionCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SECONDARYDECISIONCODE$18, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "SecondaryDecisionCode" element
     */
    public com.idanalytics.products.idscore.request.SecondaryDecisionCodeDocument.SecondaryDecisionCode xgetSecondaryDecisionCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.SecondaryDecisionCodeDocument.SecondaryDecisionCode target = null;
            target = (com.idanalytics.products.idscore.request.SecondaryDecisionCodeDocument.SecondaryDecisionCode)get_store().find_element_user(SECONDARYDECISIONCODE$18, 0);
            return target;
        }
    }
    
    /**
     * True if has "SecondaryDecisionCode" element
     */
    public boolean isSetSecondaryDecisionCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SECONDARYDECISIONCODE$18) != 0;
        }
    }
    
    /**
     * Sets the "SecondaryDecisionCode" element
     */
    public void setSecondaryDecisionCode(java.lang.String secondaryDecisionCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SECONDARYDECISIONCODE$18, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SECONDARYDECISIONCODE$18);
            }
            target.setStringValue(secondaryDecisionCode);
        }
    }
    
    /**
     * Sets (as xml) the "SecondaryDecisionCode" element
     */
    public void xsetSecondaryDecisionCode(com.idanalytics.products.idscore.request.SecondaryDecisionCodeDocument.SecondaryDecisionCode secondaryDecisionCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.SecondaryDecisionCodeDocument.SecondaryDecisionCode target = null;
            target = (com.idanalytics.products.idscore.request.SecondaryDecisionCodeDocument.SecondaryDecisionCode)get_store().find_element_user(SECONDARYDECISIONCODE$18, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.SecondaryDecisionCodeDocument.SecondaryDecisionCode)get_store().add_element_user(SECONDARYDECISIONCODE$18);
            }
            target.set(secondaryDecisionCode);
        }
    }
    
    /**
     * Unsets the "SecondaryDecisionCode" element
     */
    public void unsetSecondaryDecisionCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SECONDARYDECISIONCODE$18, 0);
        }
    }
    
    /**
     * Gets the "PrimaryPortfolio" element
     */
    public java.lang.String getPrimaryPortfolio()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PRIMARYPORTFOLIO$20, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrimaryPortfolio" element
     */
    public com.idanalytics.products.idscore.request.PrimaryPortfolioDocument.PrimaryPortfolio xgetPrimaryPortfolio()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.PrimaryPortfolioDocument.PrimaryPortfolio target = null;
            target = (com.idanalytics.products.idscore.request.PrimaryPortfolioDocument.PrimaryPortfolio)get_store().find_element_user(PRIMARYPORTFOLIO$20, 0);
            return target;
        }
    }
    
    /**
     * True if has "PrimaryPortfolio" element
     */
    public boolean isSetPrimaryPortfolio()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PRIMARYPORTFOLIO$20) != 0;
        }
    }
    
    /**
     * Sets the "PrimaryPortfolio" element
     */
    public void setPrimaryPortfolio(java.lang.String primaryPortfolio)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PRIMARYPORTFOLIO$20, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PRIMARYPORTFOLIO$20);
            }
            target.setStringValue(primaryPortfolio);
        }
    }
    
    /**
     * Sets (as xml) the "PrimaryPortfolio" element
     */
    public void xsetPrimaryPortfolio(com.idanalytics.products.idscore.request.PrimaryPortfolioDocument.PrimaryPortfolio primaryPortfolio)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.PrimaryPortfolioDocument.PrimaryPortfolio target = null;
            target = (com.idanalytics.products.idscore.request.PrimaryPortfolioDocument.PrimaryPortfolio)get_store().find_element_user(PRIMARYPORTFOLIO$20, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.PrimaryPortfolioDocument.PrimaryPortfolio)get_store().add_element_user(PRIMARYPORTFOLIO$20);
            }
            target.set(primaryPortfolio);
        }
    }
    
    /**
     * Unsets the "PrimaryPortfolio" element
     */
    public void unsetPrimaryPortfolio()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PRIMARYPORTFOLIO$20, 0);
        }
    }
    
    /**
     * Gets the "SecondaryPortfolio" element
     */
    public java.lang.String getSecondaryPortfolio()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SECONDARYPORTFOLIO$22, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "SecondaryPortfolio" element
     */
    public com.idanalytics.products.idscore.request.SecondaryPortfolioDocument.SecondaryPortfolio xgetSecondaryPortfolio()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.SecondaryPortfolioDocument.SecondaryPortfolio target = null;
            target = (com.idanalytics.products.idscore.request.SecondaryPortfolioDocument.SecondaryPortfolio)get_store().find_element_user(SECONDARYPORTFOLIO$22, 0);
            return target;
        }
    }
    
    /**
     * True if has "SecondaryPortfolio" element
     */
    public boolean isSetSecondaryPortfolio()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SECONDARYPORTFOLIO$22) != 0;
        }
    }
    
    /**
     * Sets the "SecondaryPortfolio" element
     */
    public void setSecondaryPortfolio(java.lang.String secondaryPortfolio)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SECONDARYPORTFOLIO$22, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SECONDARYPORTFOLIO$22);
            }
            target.setStringValue(secondaryPortfolio);
        }
    }
    
    /**
     * Sets (as xml) the "SecondaryPortfolio" element
     */
    public void xsetSecondaryPortfolio(com.idanalytics.products.idscore.request.SecondaryPortfolioDocument.SecondaryPortfolio secondaryPortfolio)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.SecondaryPortfolioDocument.SecondaryPortfolio target = null;
            target = (com.idanalytics.products.idscore.request.SecondaryPortfolioDocument.SecondaryPortfolio)get_store().find_element_user(SECONDARYPORTFOLIO$22, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.SecondaryPortfolioDocument.SecondaryPortfolio)get_store().add_element_user(SECONDARYPORTFOLIO$22);
            }
            target.set(secondaryPortfolio);
        }
    }
    
    /**
     * Unsets the "SecondaryPortfolio" element
     */
    public void unsetSecondaryPortfolio()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SECONDARYPORTFOLIO$22, 0);
        }
    }
    
    /**
     * Gets the "ConfirmedFraud" element
     */
    public com.idanalytics.products.idscore.request.ConfirmedFraudDocument.ConfirmedFraud.Enum getConfirmedFraud()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CONFIRMEDFRAUD$24, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.idscore.request.ConfirmedFraudDocument.ConfirmedFraud.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "ConfirmedFraud" element
     */
    public com.idanalytics.products.idscore.request.ConfirmedFraudDocument.ConfirmedFraud xgetConfirmedFraud()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ConfirmedFraudDocument.ConfirmedFraud target = null;
            target = (com.idanalytics.products.idscore.request.ConfirmedFraudDocument.ConfirmedFraud)get_store().find_element_user(CONFIRMEDFRAUD$24, 0);
            return target;
        }
    }
    
    /**
     * True if has "ConfirmedFraud" element
     */
    public boolean isSetConfirmedFraud()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CONFIRMEDFRAUD$24) != 0;
        }
    }
    
    /**
     * Sets the "ConfirmedFraud" element
     */
    public void setConfirmedFraud(com.idanalytics.products.idscore.request.ConfirmedFraudDocument.ConfirmedFraud.Enum confirmedFraud)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CONFIRMEDFRAUD$24, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CONFIRMEDFRAUD$24);
            }
            target.setEnumValue(confirmedFraud);
        }
    }
    
    /**
     * Sets (as xml) the "ConfirmedFraud" element
     */
    public void xsetConfirmedFraud(com.idanalytics.products.idscore.request.ConfirmedFraudDocument.ConfirmedFraud confirmedFraud)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ConfirmedFraudDocument.ConfirmedFraud target = null;
            target = (com.idanalytics.products.idscore.request.ConfirmedFraudDocument.ConfirmedFraud)get_store().find_element_user(CONFIRMEDFRAUD$24, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.ConfirmedFraudDocument.ConfirmedFraud)get_store().add_element_user(CONFIRMEDFRAUD$24);
            }
            target.set(confirmedFraud);
        }
    }
    
    /**
     * Unsets the "ConfirmedFraud" element
     */
    public void unsetConfirmedFraud()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CONFIRMEDFRAUD$24, 0);
        }
    }
    
    /**
     * Gets the "PrimaryFraudCode" element
     */
    public java.lang.String getPrimaryFraudCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PRIMARYFRAUDCODE$26, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrimaryFraudCode" element
     */
    public com.idanalytics.products.idscore.request.PrimaryFraudCodeDocument.PrimaryFraudCode xgetPrimaryFraudCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.PrimaryFraudCodeDocument.PrimaryFraudCode target = null;
            target = (com.idanalytics.products.idscore.request.PrimaryFraudCodeDocument.PrimaryFraudCode)get_store().find_element_user(PRIMARYFRAUDCODE$26, 0);
            return target;
        }
    }
    
    /**
     * True if has "PrimaryFraudCode" element
     */
    public boolean isSetPrimaryFraudCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PRIMARYFRAUDCODE$26) != 0;
        }
    }
    
    /**
     * Sets the "PrimaryFraudCode" element
     */
    public void setPrimaryFraudCode(java.lang.String primaryFraudCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PRIMARYFRAUDCODE$26, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PRIMARYFRAUDCODE$26);
            }
            target.setStringValue(primaryFraudCode);
        }
    }
    
    /**
     * Sets (as xml) the "PrimaryFraudCode" element
     */
    public void xsetPrimaryFraudCode(com.idanalytics.products.idscore.request.PrimaryFraudCodeDocument.PrimaryFraudCode primaryFraudCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.PrimaryFraudCodeDocument.PrimaryFraudCode target = null;
            target = (com.idanalytics.products.idscore.request.PrimaryFraudCodeDocument.PrimaryFraudCode)get_store().find_element_user(PRIMARYFRAUDCODE$26, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.PrimaryFraudCodeDocument.PrimaryFraudCode)get_store().add_element_user(PRIMARYFRAUDCODE$26);
            }
            target.set(primaryFraudCode);
        }
    }
    
    /**
     * Unsets the "PrimaryFraudCode" element
     */
    public void unsetPrimaryFraudCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PRIMARYFRAUDCODE$26, 0);
        }
    }
    
    /**
     * Gets the "SecondaryFraudCode" element
     */
    public java.lang.String getSecondaryFraudCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SECONDARYFRAUDCODE$28, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "SecondaryFraudCode" element
     */
    public com.idanalytics.products.idscore.request.SecondaryFraudCodeDocument.SecondaryFraudCode xgetSecondaryFraudCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.SecondaryFraudCodeDocument.SecondaryFraudCode target = null;
            target = (com.idanalytics.products.idscore.request.SecondaryFraudCodeDocument.SecondaryFraudCode)get_store().find_element_user(SECONDARYFRAUDCODE$28, 0);
            return target;
        }
    }
    
    /**
     * True if has "SecondaryFraudCode" element
     */
    public boolean isSetSecondaryFraudCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SECONDARYFRAUDCODE$28) != 0;
        }
    }
    
    /**
     * Sets the "SecondaryFraudCode" element
     */
    public void setSecondaryFraudCode(java.lang.String secondaryFraudCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SECONDARYFRAUDCODE$28, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SECONDARYFRAUDCODE$28);
            }
            target.setStringValue(secondaryFraudCode);
        }
    }
    
    /**
     * Sets (as xml) the "SecondaryFraudCode" element
     */
    public void xsetSecondaryFraudCode(com.idanalytics.products.idscore.request.SecondaryFraudCodeDocument.SecondaryFraudCode secondaryFraudCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.SecondaryFraudCodeDocument.SecondaryFraudCode target = null;
            target = (com.idanalytics.products.idscore.request.SecondaryFraudCodeDocument.SecondaryFraudCode)get_store().find_element_user(SECONDARYFRAUDCODE$28, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.SecondaryFraudCodeDocument.SecondaryFraudCode)get_store().add_element_user(SECONDARYFRAUDCODE$28);
            }
            target.set(secondaryFraudCode);
        }
    }
    
    /**
     * Unsets the "SecondaryFraudCode" element
     */
    public void unsetSecondaryFraudCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SECONDARYFRAUDCODE$28, 0);
        }
    }
    
    /**
     * Gets the "CARetailFlag" element
     */
    public com.idanalytics.products.idscore.request.CARetailFlagDocument.CARetailFlag.Enum getCARetailFlag()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CARETAILFLAG$30, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.idscore.request.CARetailFlagDocument.CARetailFlag.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "CARetailFlag" element
     */
    public com.idanalytics.products.idscore.request.CARetailFlagDocument.CARetailFlag xgetCARetailFlag()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.CARetailFlagDocument.CARetailFlag target = null;
            target = (com.idanalytics.products.idscore.request.CARetailFlagDocument.CARetailFlag)get_store().find_element_user(CARETAILFLAG$30, 0);
            return target;
        }
    }
    
    /**
     * True if has "CARetailFlag" element
     */
    public boolean isSetCARetailFlag()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CARETAILFLAG$30) != 0;
        }
    }
    
    /**
     * Sets the "CARetailFlag" element
     */
    public void setCARetailFlag(com.idanalytics.products.idscore.request.CARetailFlagDocument.CARetailFlag.Enum caRetailFlag)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CARETAILFLAG$30, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CARETAILFLAG$30);
            }
            target.setEnumValue(caRetailFlag);
        }
    }
    
    /**
     * Sets (as xml) the "CARetailFlag" element
     */
    public void xsetCARetailFlag(com.idanalytics.products.idscore.request.CARetailFlagDocument.CARetailFlag caRetailFlag)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.CARetailFlagDocument.CARetailFlag target = null;
            target = (com.idanalytics.products.idscore.request.CARetailFlagDocument.CARetailFlag)get_store().find_element_user(CARETAILFLAG$30, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.CARetailFlagDocument.CARetailFlag)get_store().add_element_user(CARETAILFLAG$30);
            }
            target.set(caRetailFlag);
        }
    }
    
    /**
     * Unsets the "CARetailFlag" element
     */
    public void unsetCARetailFlag()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CARETAILFLAG$30, 0);
        }
    }
    
    /**
     * Gets the "PrimaryIncome" element
     */
    public java.lang.Object getPrimaryIncome()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PRIMARYINCOME$32, 0);
            if (target == null)
            {
                return null;
            }
            return target.getObjectValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrimaryIncome" element
     */
    public com.idanalytics.products.idscore.request.Income xgetPrimaryIncome()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Income target = null;
            target = (com.idanalytics.products.idscore.request.Income)get_store().find_element_user(PRIMARYINCOME$32, 0);
            return target;
        }
    }
    
    /**
     * True if has "PrimaryIncome" element
     */
    public boolean isSetPrimaryIncome()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PRIMARYINCOME$32) != 0;
        }
    }
    
    /**
     * Sets the "PrimaryIncome" element
     */
    public void setPrimaryIncome(java.lang.Object primaryIncome)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PRIMARYINCOME$32, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PRIMARYINCOME$32);
            }
            target.setObjectValue(primaryIncome);
        }
    }
    
    /**
     * Sets (as xml) the "PrimaryIncome" element
     */
    public void xsetPrimaryIncome(com.idanalytics.products.idscore.request.Income primaryIncome)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Income target = null;
            target = (com.idanalytics.products.idscore.request.Income)get_store().find_element_user(PRIMARYINCOME$32, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Income)get_store().add_element_user(PRIMARYINCOME$32);
            }
            target.set(primaryIncome);
        }
    }
    
    /**
     * Unsets the "PrimaryIncome" element
     */
    public void unsetPrimaryIncome()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PRIMARYINCOME$32, 0);
        }
    }
    
    /**
     * Gets the "OtherIncome" element
     */
    public java.lang.Object getOtherIncome()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(OTHERINCOME$34, 0);
            if (target == null)
            {
                return null;
            }
            return target.getObjectValue();
        }
    }
    
    /**
     * Gets (as xml) the "OtherIncome" element
     */
    public com.idanalytics.products.idscore.request.Income xgetOtherIncome()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Income target = null;
            target = (com.idanalytics.products.idscore.request.Income)get_store().find_element_user(OTHERINCOME$34, 0);
            return target;
        }
    }
    
    /**
     * True if has "OtherIncome" element
     */
    public boolean isSetOtherIncome()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(OTHERINCOME$34) != 0;
        }
    }
    
    /**
     * Sets the "OtherIncome" element
     */
    public void setOtherIncome(java.lang.Object otherIncome)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(OTHERINCOME$34, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(OTHERINCOME$34);
            }
            target.setObjectValue(otherIncome);
        }
    }
    
    /**
     * Sets (as xml) the "OtherIncome" element
     */
    public void xsetOtherIncome(com.idanalytics.products.idscore.request.Income otherIncome)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Income target = null;
            target = (com.idanalytics.products.idscore.request.Income)get_store().find_element_user(OTHERINCOME$34, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Income)get_store().add_element_user(OTHERINCOME$34);
            }
            target.set(otherIncome);
        }
    }
    
    /**
     * Unsets the "OtherIncome" element
     */
    public void unsetOtherIncome()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(OTHERINCOME$34, 0);
        }
    }
    
    /**
     * Gets the "InferredIncome" element
     */
    public java.lang.Object getInferredIncome()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(INFERREDINCOME$36, 0);
            if (target == null)
            {
                return null;
            }
            return target.getObjectValue();
        }
    }
    
    /**
     * Gets (as xml) the "InferredIncome" element
     */
    public com.idanalytics.products.idscore.request.Income xgetInferredIncome()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Income target = null;
            target = (com.idanalytics.products.idscore.request.Income)get_store().find_element_user(INFERREDINCOME$36, 0);
            return target;
        }
    }
    
    /**
     * True if has "InferredIncome" element
     */
    public boolean isSetInferredIncome()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(INFERREDINCOME$36) != 0;
        }
    }
    
    /**
     * Sets the "InferredIncome" element
     */
    public void setInferredIncome(java.lang.Object inferredIncome)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(INFERREDINCOME$36, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(INFERREDINCOME$36);
            }
            target.setObjectValue(inferredIncome);
        }
    }
    
    /**
     * Sets (as xml) the "InferredIncome" element
     */
    public void xsetInferredIncome(com.idanalytics.products.idscore.request.Income inferredIncome)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Income target = null;
            target = (com.idanalytics.products.idscore.request.Income)get_store().find_element_user(INFERREDINCOME$36, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Income)get_store().add_element_user(INFERREDINCOME$36);
            }
            target.set(inferredIncome);
        }
    }
    
    /**
     * Unsets the "InferredIncome" element
     */
    public void unsetInferredIncome()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(INFERREDINCOME$36, 0);
        }
    }
    
    /**
     * Gets the "InferredIncomeInd" element
     */
    public com.idanalytics.products.idscore.request.Bool.Enum getInferredIncomeInd()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(INFERREDINCOMEIND$38, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.idscore.request.Bool.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "InferredIncomeInd" element
     */
    public com.idanalytics.products.idscore.request.Bool xgetInferredIncomeInd()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Bool target = null;
            target = (com.idanalytics.products.idscore.request.Bool)get_store().find_element_user(INFERREDINCOMEIND$38, 0);
            return target;
        }
    }
    
    /**
     * True if has "InferredIncomeInd" element
     */
    public boolean isSetInferredIncomeInd()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(INFERREDINCOMEIND$38) != 0;
        }
    }
    
    /**
     * Sets the "InferredIncomeInd" element
     */
    public void setInferredIncomeInd(com.idanalytics.products.idscore.request.Bool.Enum inferredIncomeInd)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(INFERREDINCOMEIND$38, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(INFERREDINCOMEIND$38);
            }
            target.setEnumValue(inferredIncomeInd);
        }
    }
    
    /**
     * Sets (as xml) the "InferredIncomeInd" element
     */
    public void xsetInferredIncomeInd(com.idanalytics.products.idscore.request.Bool inferredIncomeInd)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Bool target = null;
            target = (com.idanalytics.products.idscore.request.Bool)get_store().find_element_user(INFERREDINCOMEIND$38, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Bool)get_store().add_element_user(INFERREDINCOMEIND$38);
            }
            target.set(inferredIncomeInd);
        }
    }
    
    /**
     * Unsets the "InferredIncomeInd" element
     */
    public void unsetInferredIncomeInd()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(INFERREDINCOMEIND$38, 0);
        }
    }
    
    /**
     * Gets the "RecommendedCreditLine" element
     */
    public java.lang.Object getRecommendedCreditLine()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RECOMMENDEDCREDITLINE$40, 0);
            if (target == null)
            {
                return null;
            }
            return target.getObjectValue();
        }
    }
    
    /**
     * Gets (as xml) the "RecommendedCreditLine" element
     */
    public com.idanalytics.products.idscore.request.Income xgetRecommendedCreditLine()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Income target = null;
            target = (com.idanalytics.products.idscore.request.Income)get_store().find_element_user(RECOMMENDEDCREDITLINE$40, 0);
            return target;
        }
    }
    
    /**
     * True if has "RecommendedCreditLine" element
     */
    public boolean isSetRecommendedCreditLine()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(RECOMMENDEDCREDITLINE$40) != 0;
        }
    }
    
    /**
     * Sets the "RecommendedCreditLine" element
     */
    public void setRecommendedCreditLine(java.lang.Object recommendedCreditLine)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RECOMMENDEDCREDITLINE$40, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(RECOMMENDEDCREDITLINE$40);
            }
            target.setObjectValue(recommendedCreditLine);
        }
    }
    
    /**
     * Sets (as xml) the "RecommendedCreditLine" element
     */
    public void xsetRecommendedCreditLine(com.idanalytics.products.idscore.request.Income recommendedCreditLine)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Income target = null;
            target = (com.idanalytics.products.idscore.request.Income)get_store().find_element_user(RECOMMENDEDCREDITLINE$40, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Income)get_store().add_element_user(RECOMMENDEDCREDITLINE$40);
            }
            target.set(recommendedCreditLine);
        }
    }
    
    /**
     * Unsets the "RecommendedCreditLine" element
     */
    public void unsetRecommendedCreditLine()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(RECOMMENDEDCREDITLINE$40, 0);
        }
    }
}
