/*
 * XML Type:  MessageType
 * Namespace: http://idanalytics.com/products/idscore/result
 * Java type: com.idanalytics.products.idscore.result.MessageType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.result.impl;
/**
 * An XML MessageType(@http://idanalytics.com/products/idscore/result).
 *
 * This is a complex type.
 */
public class MessageTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.result.MessageType
{
    private static final long serialVersionUID = 1L;
    
    public MessageTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName MESSAGETYPE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "MessageType");
    private static final javax.xml.namespace.QName MESSAGESUBTYPE$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "MessageSubType");
    private static final javax.xml.namespace.QName MESSAGEID$4 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "MessageID");
    
    
    /**
     * Gets the "MessageType" element
     */
    public java.lang.String getMessageType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MESSAGETYPE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "MessageType" element
     */
    public com.idanalytics.products.idscore.result.MessageType.MessageType2 xgetMessageType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.MessageType.MessageType2 target = null;
            target = (com.idanalytics.products.idscore.result.MessageType.MessageType2)get_store().find_element_user(MESSAGETYPE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "MessageType" element
     */
    public void setMessageType(java.lang.String messageType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MESSAGETYPE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(MESSAGETYPE$0);
            }
            target.setStringValue(messageType);
        }
    }
    
    /**
     * Sets (as xml) the "MessageType" element
     */
    public void xsetMessageType(com.idanalytics.products.idscore.result.MessageType.MessageType2 messageType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.MessageType.MessageType2 target = null;
            target = (com.idanalytics.products.idscore.result.MessageType.MessageType2)get_store().find_element_user(MESSAGETYPE$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.result.MessageType.MessageType2)get_store().add_element_user(MESSAGETYPE$0);
            }
            target.set(messageType);
        }
    }
    
    /**
     * Gets the "MessageSubType" element
     */
    public java.lang.String getMessageSubType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MESSAGESUBTYPE$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "MessageSubType" element
     */
    public com.idanalytics.products.idscore.result.MessageType.MessageSubType xgetMessageSubType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.MessageType.MessageSubType target = null;
            target = (com.idanalytics.products.idscore.result.MessageType.MessageSubType)get_store().find_element_user(MESSAGESUBTYPE$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "MessageSubType" element
     */
    public void setMessageSubType(java.lang.String messageSubType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MESSAGESUBTYPE$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(MESSAGESUBTYPE$2);
            }
            target.setStringValue(messageSubType);
        }
    }
    
    /**
     * Sets (as xml) the "MessageSubType" element
     */
    public void xsetMessageSubType(com.idanalytics.products.idscore.result.MessageType.MessageSubType messageSubType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.MessageType.MessageSubType target = null;
            target = (com.idanalytics.products.idscore.result.MessageType.MessageSubType)get_store().find_element_user(MESSAGESUBTYPE$2, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.result.MessageType.MessageSubType)get_store().add_element_user(MESSAGESUBTYPE$2);
            }
            target.set(messageSubType);
        }
    }
    
    /**
     * Gets the "MessageID" element
     */
    public java.lang.String getMessageID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MESSAGEID$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "MessageID" element
     */
    public com.idanalytics.products.idscore.result.MessageType.MessageID xgetMessageID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.MessageType.MessageID target = null;
            target = (com.idanalytics.products.idscore.result.MessageType.MessageID)get_store().find_element_user(MESSAGEID$4, 0);
            return target;
        }
    }
    
    /**
     * Sets the "MessageID" element
     */
    public void setMessageID(java.lang.String messageID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MESSAGEID$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(MESSAGEID$4);
            }
            target.setStringValue(messageID);
        }
    }
    
    /**
     * Sets (as xml) the "MessageID" element
     */
    public void xsetMessageID(com.idanalytics.products.idscore.result.MessageType.MessageID messageID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.MessageType.MessageID target = null;
            target = (com.idanalytics.products.idscore.result.MessageType.MessageID)get_store().find_element_user(MESSAGEID$4, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.result.MessageType.MessageID)get_store().add_element_user(MESSAGEID$4);
            }
            target.set(messageID);
        }
    }
    /**
     * An XML MessageType(@http://idanalytics.com/products/idscore/result).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result.MessageType$MessageType2.
     */
    public static class MessageTypeImpl2 extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.result.MessageType.MessageType2
    {
        private static final long serialVersionUID = 1L;
        
        public MessageTypeImpl2(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected MessageTypeImpl2(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
    /**
     * An XML MessageSubType(@http://idanalytics.com/products/idscore/result).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result.MessageType$MessageSubType.
     */
    public static class MessageSubTypeImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.result.MessageType.MessageSubType
    {
        private static final long serialVersionUID = 1L;
        
        public MessageSubTypeImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected MessageSubTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
    /**
     * An XML MessageID(@http://idanalytics.com/products/idscore/result).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result.MessageType$MessageID.
     */
    public static class MessageIDImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.result.MessageType.MessageID
    {
        private static final long serialVersionUID = 1L;
        
        public MessageIDImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected MessageIDImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
