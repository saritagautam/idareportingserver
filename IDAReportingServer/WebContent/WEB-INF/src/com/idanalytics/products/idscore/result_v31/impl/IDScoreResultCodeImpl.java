/*
 * XML Type:  IDScoreResultCode
 * Namespace: http://idanalytics.com/products/idscore/result.v31
 * Java type: com.idanalytics.products.idscore.result_v31.IDScoreResultCode
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.result_v31.impl;
/**
 * An XML IDScoreResultCode(@http://idanalytics.com/products/idscore/result.v31).
 *
 * This is a union type. Instances are of one of the following types:
 *     com.idanalytics.products.idscore.result_v31.ResultCode$Member
 *     com.idanalytics.products.idscore.result_v31.ResultCode$Member2
 */
public class IDScoreResultCodeImpl extends org.apache.xmlbeans.impl.values.XmlUnionImpl implements com.idanalytics.products.idscore.result_v31.IDScoreResultCode, com.idanalytics.products.idscore.result_v31.ResultCode.Member, com.idanalytics.products.idscore.result_v31.ResultCode.Member2
{
    private static final long serialVersionUID = 1L;
    
    public IDScoreResultCodeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, true);
    }
    
    protected IDScoreResultCodeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
    
    private static final javax.xml.namespace.QName DESC$0 = 
        new javax.xml.namespace.QName("", "desc");
    
    
    /**
     * Gets the "desc" attribute
     */
    public org.apache.xmlbeans.XmlAnySimpleType getDesc()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlAnySimpleType target = null;
            target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().find_attribute_user(DESC$0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "desc" attribute
     */
    public boolean isSetDesc()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().find_attribute_user(DESC$0) != null;
        }
    }
    
    /**
     * Sets the "desc" attribute
     */
    public void setDesc(org.apache.xmlbeans.XmlAnySimpleType desc)
    {
        generatedSetterHelperImpl(desc, DESC$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "desc" attribute
     */
    public org.apache.xmlbeans.XmlAnySimpleType addNewDesc()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlAnySimpleType target = null;
            target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().add_attribute_user(DESC$0);
            return target;
        }
    }
    
    /**
     * Unsets the "desc" attribute
     */
    public void unsetDesc()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_attribute(DESC$0);
        }
    }
}
