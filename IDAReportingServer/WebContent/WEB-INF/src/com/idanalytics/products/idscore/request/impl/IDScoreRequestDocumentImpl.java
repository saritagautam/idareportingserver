/*
 * An XML document type.
 * Localname: IDScoreRequest
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.IDScoreRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * A document containing one IDScoreRequest(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public class IDScoreRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.IDScoreRequestDocument
{
    private static final long serialVersionUID = 1L;
    
    public IDScoreRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IDSCOREREQUEST$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "IDScoreRequest");
    
    
    /**
     * Gets the "IDScoreRequest" element
     */
    public com.idanalytics.products.idscore.request.IDScoreRequestDocument.IDScoreRequest getIDScoreRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.IDScoreRequestDocument.IDScoreRequest target = null;
            target = (com.idanalytics.products.idscore.request.IDScoreRequestDocument.IDScoreRequest)get_store().find_element_user(IDSCOREREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "IDScoreRequest" element
     */
    public void setIDScoreRequest(com.idanalytics.products.idscore.request.IDScoreRequestDocument.IDScoreRequest idScoreRequest)
    {
        generatedSetterHelperImpl(idScoreRequest, IDSCOREREQUEST$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "IDScoreRequest" element
     */
    public com.idanalytics.products.idscore.request.IDScoreRequestDocument.IDScoreRequest addNewIDScoreRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.IDScoreRequestDocument.IDScoreRequest target = null;
            target = (com.idanalytics.products.idscore.request.IDScoreRequestDocument.IDScoreRequest)get_store().add_element_user(IDSCOREREQUEST$0);
            return target;
        }
    }
    /**
     * An XML IDScoreRequest(@http://idanalytics.com/products/idscore/request).
     *
     * This is a complex type.
     */
    public static class IDScoreRequestImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.IDScoreRequestDocument.IDScoreRequest
    {
        private static final long serialVersionUID = 1L;
        
        public IDScoreRequestImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName SERVICETYPE$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ServiceType");
        private static final javax.xml.namespace.QName ORIGINATION$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Origination");
        private static final javax.xml.namespace.QName RECONSTRUCTREQUEST$4 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ReconstructRequest");
        private static final javax.xml.namespace.QName IDENTITY$6 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Identity");
        private static final javax.xml.namespace.QName BILLINGIDENTITY$8 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "BillingIdentity");
        private static final javax.xml.namespace.QName BILLINGINFO$10 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "BillingInfo");
        private static final javax.xml.namespace.QName SHIPPINGINFO$12 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ShippingInfo");
        private static final javax.xml.namespace.QName APPLICATION$14 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Application");
        private static final javax.xml.namespace.QName AUTHORIZEDUSER$16 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "AuthorizedUser");
        private static final javax.xml.namespace.QName CREDITREPORT$18 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "CreditReport");
        private static final javax.xml.namespace.QName EXISTINGACCOUNT$20 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ExistingAccount");
        private static final javax.xml.namespace.QName USERDEFINED$22 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "UserDefined");
        private static final javax.xml.namespace.QName EXTERNALDATA$24 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ExternalData");
        private static final javax.xml.namespace.QName ECOMMERCE$26 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ECommerce");
        private static final javax.xml.namespace.QName IDAINTERNAL$28 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "IDAInternal");
        private static final javax.xml.namespace.QName SCHEMAVERSION$30 = 
            new javax.xml.namespace.QName("", "schemaVersion");
        
        
        /**
         * Gets the "ServiceType" element
         */
        public java.lang.String getServiceType()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SERVICETYPE$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "ServiceType" element
         */
        public com.idanalytics.products.idscore.request.ServiceType xgetServiceType()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.ServiceType target = null;
                target = (com.idanalytics.products.idscore.request.ServiceType)get_store().find_element_user(SERVICETYPE$0, 0);
                return target;
            }
        }
        
        /**
         * True if has "ServiceType" element
         */
        public boolean isSetServiceType()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(SERVICETYPE$0) != 0;
            }
        }
        
        /**
         * Sets the "ServiceType" element
         */
        public void setServiceType(java.lang.String serviceType)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SERVICETYPE$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SERVICETYPE$0);
                }
                target.setStringValue(serviceType);
            }
        }
        
        /**
         * Sets (as xml) the "ServiceType" element
         */
        public void xsetServiceType(com.idanalytics.products.idscore.request.ServiceType serviceType)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.ServiceType target = null;
                target = (com.idanalytics.products.idscore.request.ServiceType)get_store().find_element_user(SERVICETYPE$0, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.request.ServiceType)get_store().add_element_user(SERVICETYPE$0);
                }
                target.set(serviceType);
            }
        }
        
        /**
         * Unsets the "ServiceType" element
         */
        public void unsetServiceType()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(SERVICETYPE$0, 0);
            }
        }
        
        /**
         * Gets the "Origination" element
         */
        public com.idanalytics.products.idscore.request.Origination getOrigination()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.Origination target = null;
                target = (com.idanalytics.products.idscore.request.Origination)get_store().find_element_user(ORIGINATION$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * Sets the "Origination" element
         */
        public void setOrigination(com.idanalytics.products.idscore.request.Origination origination)
        {
            generatedSetterHelperImpl(origination, ORIGINATION$2, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "Origination" element
         */
        public com.idanalytics.products.idscore.request.Origination addNewOrigination()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.Origination target = null;
                target = (com.idanalytics.products.idscore.request.Origination)get_store().add_element_user(ORIGINATION$2);
                return target;
            }
        }
        
        /**
         * Gets the "ReconstructRequest" element
         */
        public com.idanalytics.products.idscore.request.ReconstructRequestType getReconstructRequest()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.ReconstructRequestType target = null;
                target = (com.idanalytics.products.idscore.request.ReconstructRequestType)get_store().find_element_user(RECONSTRUCTREQUEST$4, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "ReconstructRequest" element
         */
        public boolean isSetReconstructRequest()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(RECONSTRUCTREQUEST$4) != 0;
            }
        }
        
        /**
         * Sets the "ReconstructRequest" element
         */
        public void setReconstructRequest(com.idanalytics.products.idscore.request.ReconstructRequestType reconstructRequest)
        {
            generatedSetterHelperImpl(reconstructRequest, RECONSTRUCTREQUEST$4, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "ReconstructRequest" element
         */
        public com.idanalytics.products.idscore.request.ReconstructRequestType addNewReconstructRequest()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.ReconstructRequestType target = null;
                target = (com.idanalytics.products.idscore.request.ReconstructRequestType)get_store().add_element_user(RECONSTRUCTREQUEST$4);
                return target;
            }
        }
        
        /**
         * Unsets the "ReconstructRequest" element
         */
        public void unsetReconstructRequest()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(RECONSTRUCTREQUEST$4, 0);
            }
        }
        
        /**
         * Gets the "Identity" element
         */
        public com.idanalytics.products.idscore.request.Identity getIdentity()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.Identity target = null;
                target = (com.idanalytics.products.idscore.request.Identity)get_store().find_element_user(IDENTITY$6, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * Sets the "Identity" element
         */
        public void setIdentity(com.idanalytics.products.idscore.request.Identity identity)
        {
            generatedSetterHelperImpl(identity, IDENTITY$6, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "Identity" element
         */
        public com.idanalytics.products.idscore.request.Identity addNewIdentity()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.Identity target = null;
                target = (com.idanalytics.products.idscore.request.Identity)get_store().add_element_user(IDENTITY$6);
                return target;
            }
        }
        
        /**
         * Gets the "BillingIdentity" element
         */
        public com.idanalytics.products.idscore.request.Identity getBillingIdentity()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.Identity target = null;
                target = (com.idanalytics.products.idscore.request.Identity)get_store().find_element_user(BILLINGIDENTITY$8, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "BillingIdentity" element
         */
        public boolean isSetBillingIdentity()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(BILLINGIDENTITY$8) != 0;
            }
        }
        
        /**
         * Sets the "BillingIdentity" element
         */
        public void setBillingIdentity(com.idanalytics.products.idscore.request.Identity billingIdentity)
        {
            generatedSetterHelperImpl(billingIdentity, BILLINGIDENTITY$8, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "BillingIdentity" element
         */
        public com.idanalytics.products.idscore.request.Identity addNewBillingIdentity()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.Identity target = null;
                target = (com.idanalytics.products.idscore.request.Identity)get_store().add_element_user(BILLINGIDENTITY$8);
                return target;
            }
        }
        
        /**
         * Unsets the "BillingIdentity" element
         */
        public void unsetBillingIdentity()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(BILLINGIDENTITY$8, 0);
            }
        }
        
        /**
         * Gets array of all "BillingInfo" elements
         */
        public com.idanalytics.products.idscore.request.BillingInfo[] getBillingInfoArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(BILLINGINFO$10, targetList);
                com.idanalytics.products.idscore.request.BillingInfo[] result = new com.idanalytics.products.idscore.request.BillingInfo[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "BillingInfo" element
         */
        public com.idanalytics.products.idscore.request.BillingInfo getBillingInfoArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.BillingInfo target = null;
                target = (com.idanalytics.products.idscore.request.BillingInfo)get_store().find_element_user(BILLINGINFO$10, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "BillingInfo" element
         */
        public int sizeOfBillingInfoArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(BILLINGINFO$10);
            }
        }
        
        /**
         * Sets array of all "BillingInfo" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setBillingInfoArray(com.idanalytics.products.idscore.request.BillingInfo[] billingInfoArray)
        {
            check_orphaned();
            arraySetterHelper(billingInfoArray, BILLINGINFO$10);
        }
        
        /**
         * Sets ith "BillingInfo" element
         */
        public void setBillingInfoArray(int i, com.idanalytics.products.idscore.request.BillingInfo billingInfo)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.BillingInfo target = null;
                target = (com.idanalytics.products.idscore.request.BillingInfo)get_store().find_element_user(BILLINGINFO$10, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(billingInfo);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "BillingInfo" element
         */
        public com.idanalytics.products.idscore.request.BillingInfo insertNewBillingInfo(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.BillingInfo target = null;
                target = (com.idanalytics.products.idscore.request.BillingInfo)get_store().insert_element_user(BILLINGINFO$10, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "BillingInfo" element
         */
        public com.idanalytics.products.idscore.request.BillingInfo addNewBillingInfo()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.BillingInfo target = null;
                target = (com.idanalytics.products.idscore.request.BillingInfo)get_store().add_element_user(BILLINGINFO$10);
                return target;
            }
        }
        
        /**
         * Removes the ith "BillingInfo" element
         */
        public void removeBillingInfo(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(BILLINGINFO$10, i);
            }
        }
        
        /**
         * Gets array of all "ShippingInfo" elements
         */
        public com.idanalytics.products.idscore.request.ShippingInfo[] getShippingInfoArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(SHIPPINGINFO$12, targetList);
                com.idanalytics.products.idscore.request.ShippingInfo[] result = new com.idanalytics.products.idscore.request.ShippingInfo[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "ShippingInfo" element
         */
        public com.idanalytics.products.idscore.request.ShippingInfo getShippingInfoArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.ShippingInfo target = null;
                target = (com.idanalytics.products.idscore.request.ShippingInfo)get_store().find_element_user(SHIPPINGINFO$12, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "ShippingInfo" element
         */
        public int sizeOfShippingInfoArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(SHIPPINGINFO$12);
            }
        }
        
        /**
         * Sets array of all "ShippingInfo" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setShippingInfoArray(com.idanalytics.products.idscore.request.ShippingInfo[] shippingInfoArray)
        {
            check_orphaned();
            arraySetterHelper(shippingInfoArray, SHIPPINGINFO$12);
        }
        
        /**
         * Sets ith "ShippingInfo" element
         */
        public void setShippingInfoArray(int i, com.idanalytics.products.idscore.request.ShippingInfo shippingInfo)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.ShippingInfo target = null;
                target = (com.idanalytics.products.idscore.request.ShippingInfo)get_store().find_element_user(SHIPPINGINFO$12, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(shippingInfo);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "ShippingInfo" element
         */
        public com.idanalytics.products.idscore.request.ShippingInfo insertNewShippingInfo(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.ShippingInfo target = null;
                target = (com.idanalytics.products.idscore.request.ShippingInfo)get_store().insert_element_user(SHIPPINGINFO$12, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "ShippingInfo" element
         */
        public com.idanalytics.products.idscore.request.ShippingInfo addNewShippingInfo()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.ShippingInfo target = null;
                target = (com.idanalytics.products.idscore.request.ShippingInfo)get_store().add_element_user(SHIPPINGINFO$12);
                return target;
            }
        }
        
        /**
         * Removes the ith "ShippingInfo" element
         */
        public void removeShippingInfo(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(SHIPPINGINFO$12, i);
            }
        }
        
        /**
         * Gets the "Application" element
         */
        public com.idanalytics.products.idscore.request.Application getApplication()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.Application target = null;
                target = (com.idanalytics.products.idscore.request.Application)get_store().find_element_user(APPLICATION$14, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * Sets the "Application" element
         */
        public void setApplication(com.idanalytics.products.idscore.request.Application application)
        {
            generatedSetterHelperImpl(application, APPLICATION$14, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "Application" element
         */
        public com.idanalytics.products.idscore.request.Application addNewApplication()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.Application target = null;
                target = (com.idanalytics.products.idscore.request.Application)get_store().add_element_user(APPLICATION$14);
                return target;
            }
        }
        
        /**
         * Gets array of all "AuthorizedUser" elements
         */
        public com.idanalytics.products.idscore.request.AuthorizedUser[] getAuthorizedUserArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(AUTHORIZEDUSER$16, targetList);
                com.idanalytics.products.idscore.request.AuthorizedUser[] result = new com.idanalytics.products.idscore.request.AuthorizedUser[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "AuthorizedUser" element
         */
        public com.idanalytics.products.idscore.request.AuthorizedUser getAuthorizedUserArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.AuthorizedUser target = null;
                target = (com.idanalytics.products.idscore.request.AuthorizedUser)get_store().find_element_user(AUTHORIZEDUSER$16, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "AuthorizedUser" element
         */
        public int sizeOfAuthorizedUserArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(AUTHORIZEDUSER$16);
            }
        }
        
        /**
         * Sets array of all "AuthorizedUser" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setAuthorizedUserArray(com.idanalytics.products.idscore.request.AuthorizedUser[] authorizedUserArray)
        {
            check_orphaned();
            arraySetterHelper(authorizedUserArray, AUTHORIZEDUSER$16);
        }
        
        /**
         * Sets ith "AuthorizedUser" element
         */
        public void setAuthorizedUserArray(int i, com.idanalytics.products.idscore.request.AuthorizedUser authorizedUser)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.AuthorizedUser target = null;
                target = (com.idanalytics.products.idscore.request.AuthorizedUser)get_store().find_element_user(AUTHORIZEDUSER$16, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(authorizedUser);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "AuthorizedUser" element
         */
        public com.idanalytics.products.idscore.request.AuthorizedUser insertNewAuthorizedUser(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.AuthorizedUser target = null;
                target = (com.idanalytics.products.idscore.request.AuthorizedUser)get_store().insert_element_user(AUTHORIZEDUSER$16, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "AuthorizedUser" element
         */
        public com.idanalytics.products.idscore.request.AuthorizedUser addNewAuthorizedUser()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.AuthorizedUser target = null;
                target = (com.idanalytics.products.idscore.request.AuthorizedUser)get_store().add_element_user(AUTHORIZEDUSER$16);
                return target;
            }
        }
        
        /**
         * Removes the ith "AuthorizedUser" element
         */
        public void removeAuthorizedUser(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(AUTHORIZEDUSER$16, i);
            }
        }
        
        /**
         * Gets array of all "CreditReport" elements
         */
        public com.idanalytics.products.idscore.request.CreditReport[] getCreditReportArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(CREDITREPORT$18, targetList);
                com.idanalytics.products.idscore.request.CreditReport[] result = new com.idanalytics.products.idscore.request.CreditReport[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "CreditReport" element
         */
        public com.idanalytics.products.idscore.request.CreditReport getCreditReportArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.CreditReport target = null;
                target = (com.idanalytics.products.idscore.request.CreditReport)get_store().find_element_user(CREDITREPORT$18, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "CreditReport" element
         */
        public int sizeOfCreditReportArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(CREDITREPORT$18);
            }
        }
        
        /**
         * Sets array of all "CreditReport" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setCreditReportArray(com.idanalytics.products.idscore.request.CreditReport[] creditReportArray)
        {
            check_orphaned();
            arraySetterHelper(creditReportArray, CREDITREPORT$18);
        }
        
        /**
         * Sets ith "CreditReport" element
         */
        public void setCreditReportArray(int i, com.idanalytics.products.idscore.request.CreditReport creditReport)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.CreditReport target = null;
                target = (com.idanalytics.products.idscore.request.CreditReport)get_store().find_element_user(CREDITREPORT$18, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(creditReport);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "CreditReport" element
         */
        public com.idanalytics.products.idscore.request.CreditReport insertNewCreditReport(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.CreditReport target = null;
                target = (com.idanalytics.products.idscore.request.CreditReport)get_store().insert_element_user(CREDITREPORT$18, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "CreditReport" element
         */
        public com.idanalytics.products.idscore.request.CreditReport addNewCreditReport()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.CreditReport target = null;
                target = (com.idanalytics.products.idscore.request.CreditReport)get_store().add_element_user(CREDITREPORT$18);
                return target;
            }
        }
        
        /**
         * Removes the ith "CreditReport" element
         */
        public void removeCreditReport(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(CREDITREPORT$18, i);
            }
        }
        
        /**
         * Gets array of all "ExistingAccount" elements
         */
        public com.idanalytics.products.idscore.request.ExistingAccount[] getExistingAccountArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(EXISTINGACCOUNT$20, targetList);
                com.idanalytics.products.idscore.request.ExistingAccount[] result = new com.idanalytics.products.idscore.request.ExistingAccount[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "ExistingAccount" element
         */
        public com.idanalytics.products.idscore.request.ExistingAccount getExistingAccountArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.ExistingAccount target = null;
                target = (com.idanalytics.products.idscore.request.ExistingAccount)get_store().find_element_user(EXISTINGACCOUNT$20, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "ExistingAccount" element
         */
        public int sizeOfExistingAccountArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(EXISTINGACCOUNT$20);
            }
        }
        
        /**
         * Sets array of all "ExistingAccount" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setExistingAccountArray(com.idanalytics.products.idscore.request.ExistingAccount[] existingAccountArray)
        {
            check_orphaned();
            arraySetterHelper(existingAccountArray, EXISTINGACCOUNT$20);
        }
        
        /**
         * Sets ith "ExistingAccount" element
         */
        public void setExistingAccountArray(int i, com.idanalytics.products.idscore.request.ExistingAccount existingAccount)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.ExistingAccount target = null;
                target = (com.idanalytics.products.idscore.request.ExistingAccount)get_store().find_element_user(EXISTINGACCOUNT$20, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(existingAccount);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "ExistingAccount" element
         */
        public com.idanalytics.products.idscore.request.ExistingAccount insertNewExistingAccount(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.ExistingAccount target = null;
                target = (com.idanalytics.products.idscore.request.ExistingAccount)get_store().insert_element_user(EXISTINGACCOUNT$20, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "ExistingAccount" element
         */
        public com.idanalytics.products.idscore.request.ExistingAccount addNewExistingAccount()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.ExistingAccount target = null;
                target = (com.idanalytics.products.idscore.request.ExistingAccount)get_store().add_element_user(EXISTINGACCOUNT$20);
                return target;
            }
        }
        
        /**
         * Removes the ith "ExistingAccount" element
         */
        public void removeExistingAccount(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(EXISTINGACCOUNT$20, i);
            }
        }
        
        /**
         * Gets array of all "UserDefined" elements
         */
        public com.idanalytics.products.idscore.request.UserDefined[] getUserDefinedArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(USERDEFINED$22, targetList);
                com.idanalytics.products.idscore.request.UserDefined[] result = new com.idanalytics.products.idscore.request.UserDefined[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "UserDefined" element
         */
        public com.idanalytics.products.idscore.request.UserDefined getUserDefinedArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.UserDefined target = null;
                target = (com.idanalytics.products.idscore.request.UserDefined)get_store().find_element_user(USERDEFINED$22, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "UserDefined" element
         */
        public int sizeOfUserDefinedArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(USERDEFINED$22);
            }
        }
        
        /**
         * Sets array of all "UserDefined" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setUserDefinedArray(com.idanalytics.products.idscore.request.UserDefined[] userDefinedArray)
        {
            check_orphaned();
            arraySetterHelper(userDefinedArray, USERDEFINED$22);
        }
        
        /**
         * Sets ith "UserDefined" element
         */
        public void setUserDefinedArray(int i, com.idanalytics.products.idscore.request.UserDefined userDefined)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.UserDefined target = null;
                target = (com.idanalytics.products.idscore.request.UserDefined)get_store().find_element_user(USERDEFINED$22, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(userDefined);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "UserDefined" element
         */
        public com.idanalytics.products.idscore.request.UserDefined insertNewUserDefined(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.UserDefined target = null;
                target = (com.idanalytics.products.idscore.request.UserDefined)get_store().insert_element_user(USERDEFINED$22, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "UserDefined" element
         */
        public com.idanalytics.products.idscore.request.UserDefined addNewUserDefined()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.UserDefined target = null;
                target = (com.idanalytics.products.idscore.request.UserDefined)get_store().add_element_user(USERDEFINED$22);
                return target;
            }
        }
        
        /**
         * Removes the ith "UserDefined" element
         */
        public void removeUserDefined(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(USERDEFINED$22, i);
            }
        }
        
        /**
         * Gets the "ExternalData" element
         */
        public com.idanalytics.products.idscore.request.ExternalData getExternalData()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.ExternalData target = null;
                target = (com.idanalytics.products.idscore.request.ExternalData)get_store().find_element_user(EXTERNALDATA$24, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "ExternalData" element
         */
        public boolean isSetExternalData()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(EXTERNALDATA$24) != 0;
            }
        }
        
        /**
         * Sets the "ExternalData" element
         */
        public void setExternalData(com.idanalytics.products.idscore.request.ExternalData externalData)
        {
            generatedSetterHelperImpl(externalData, EXTERNALDATA$24, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "ExternalData" element
         */
        public com.idanalytics.products.idscore.request.ExternalData addNewExternalData()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.ExternalData target = null;
                target = (com.idanalytics.products.idscore.request.ExternalData)get_store().add_element_user(EXTERNALDATA$24);
                return target;
            }
        }
        
        /**
         * Unsets the "ExternalData" element
         */
        public void unsetExternalData()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(EXTERNALDATA$24, 0);
            }
        }
        
        /**
         * Gets the "ECommerce" element
         */
        public com.idanalytics.products.idscore.request.ECommerce getECommerce()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.ECommerce target = null;
                target = (com.idanalytics.products.idscore.request.ECommerce)get_store().find_element_user(ECOMMERCE$26, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "ECommerce" element
         */
        public boolean isSetECommerce()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(ECOMMERCE$26) != 0;
            }
        }
        
        /**
         * Sets the "ECommerce" element
         */
        public void setECommerce(com.idanalytics.products.idscore.request.ECommerce eCommerce)
        {
            generatedSetterHelperImpl(eCommerce, ECOMMERCE$26, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "ECommerce" element
         */
        public com.idanalytics.products.idscore.request.ECommerce addNewECommerce()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.ECommerce target = null;
                target = (com.idanalytics.products.idscore.request.ECommerce)get_store().add_element_user(ECOMMERCE$26);
                return target;
            }
        }
        
        /**
         * Unsets the "ECommerce" element
         */
        public void unsetECommerce()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(ECOMMERCE$26, 0);
            }
        }
        
        /**
         * Gets the "IDAInternal" element
         */
        public com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal getIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal target = null;
                target = (com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal)get_store().find_element_user(IDAINTERNAL$28, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "IDAInternal" element
         */
        public boolean isSetIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(IDAINTERNAL$28) != 0;
            }
        }
        
        /**
         * Sets the "IDAInternal" element
         */
        public void setIDAInternal(com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal idaInternal)
        {
            generatedSetterHelperImpl(idaInternal, IDAINTERNAL$28, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "IDAInternal" element
         */
        public com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal addNewIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal target = null;
                target = (com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal)get_store().add_element_user(IDAINTERNAL$28);
                return target;
            }
        }
        
        /**
         * Unsets the "IDAInternal" element
         */
        public void unsetIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(IDAINTERNAL$28, 0);
            }
        }
        
        /**
         * Gets the "schemaVersion" attribute
         */
        public java.math.BigDecimal getSchemaVersion()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(SCHEMAVERSION$30);
                if (target == null)
                {
                    return null;
                }
                return target.getBigDecimalValue();
            }
        }
        
        /**
         * Gets (as xml) the "schemaVersion" attribute
         */
        public org.apache.xmlbeans.XmlDecimal xgetSchemaVersion()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlDecimal target = null;
                target = (org.apache.xmlbeans.XmlDecimal)get_store().find_attribute_user(SCHEMAVERSION$30);
                return target;
            }
        }
        
        /**
         * True if has "schemaVersion" attribute
         */
        public boolean isSetSchemaVersion()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().find_attribute_user(SCHEMAVERSION$30) != null;
            }
        }
        
        /**
         * Sets the "schemaVersion" attribute
         */
        public void setSchemaVersion(java.math.BigDecimal schemaVersion)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(SCHEMAVERSION$30);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(SCHEMAVERSION$30);
                }
                target.setBigDecimalValue(schemaVersion);
            }
        }
        
        /**
         * Sets (as xml) the "schemaVersion" attribute
         */
        public void xsetSchemaVersion(org.apache.xmlbeans.XmlDecimal schemaVersion)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlDecimal target = null;
                target = (org.apache.xmlbeans.XmlDecimal)get_store().find_attribute_user(SCHEMAVERSION$30);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlDecimal)get_store().add_attribute_user(SCHEMAVERSION$30);
                }
                target.set(schemaVersion);
            }
        }
        
        /**
         * Unsets the "schemaVersion" attribute
         */
        public void unsetSchemaVersion()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_attribute(SCHEMAVERSION$30);
            }
        }
    }
}
