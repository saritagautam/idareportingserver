/*
 * An XML document type.
 * Localname: MiddleName
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.MiddleNameDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * A document containing one MiddleName(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public class MiddleNameDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.MiddleNameDocument
{
    private static final long serialVersionUID = 1L;
    
    public MiddleNameDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName MIDDLENAME$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "MiddleName");
    
    
    /**
     * Gets the "MiddleName" element
     */
    public java.lang.String getMiddleName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MIDDLENAME$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "MiddleName" element
     */
    public com.idanalytics.products.idscore.request.MiddleNameDocument.MiddleName xgetMiddleName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.MiddleNameDocument.MiddleName target = null;
            target = (com.idanalytics.products.idscore.request.MiddleNameDocument.MiddleName)get_store().find_element_user(MIDDLENAME$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "MiddleName" element
     */
    public void setMiddleName(java.lang.String middleName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MIDDLENAME$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(MIDDLENAME$0);
            }
            target.setStringValue(middleName);
        }
    }
    
    /**
     * Sets (as xml) the "MiddleName" element
     */
    public void xsetMiddleName(com.idanalytics.products.idscore.request.MiddleNameDocument.MiddleName middleName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.MiddleNameDocument.MiddleName target = null;
            target = (com.idanalytics.products.idscore.request.MiddleNameDocument.MiddleName)get_store().find_element_user(MIDDLENAME$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.MiddleNameDocument.MiddleName)get_store().add_element_user(MIDDLENAME$0);
            }
            target.set(middleName);
        }
    }
    /**
     * An XML MiddleName(@http://idanalytics.com/products/idscore/request).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.MiddleNameDocument$MiddleName.
     */
    public static class MiddleNameImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.request.MiddleNameDocument.MiddleName
    {
        private static final long serialVersionUID = 1L;
        
        public MiddleNameImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected MiddleNameImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
