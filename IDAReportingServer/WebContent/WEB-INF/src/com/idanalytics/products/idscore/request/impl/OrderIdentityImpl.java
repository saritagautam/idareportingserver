/*
 * XML Type:  Order_Identity
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.OrderIdentity
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML Order_Identity(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public class OrderIdentityImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.OrderIdentity
{
    private static final long serialVersionUID = 1L;
    
    public OrderIdentityImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName TITLE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Title");
    private static final javax.xml.namespace.QName FIRSTNAME$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "FirstName");
    private static final javax.xml.namespace.QName MIDDLENAME$4 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "MiddleName");
    private static final javax.xml.namespace.QName LASTNAME$6 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "LastName");
    private static final javax.xml.namespace.QName SUFFIX$8 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Suffix");
    private static final javax.xml.namespace.QName ADDRESS$10 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Address");
    private static final javax.xml.namespace.QName CITY$12 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "City");
    private static final javax.xml.namespace.QName STATE$14 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "State");
    private static final javax.xml.namespace.QName ZIP$16 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Zip");
    private static final javax.xml.namespace.QName COUNTRY$18 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Country");
    private static final javax.xml.namespace.QName HOMEPHONE$20 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "HomePhone");
    private static final javax.xml.namespace.QName MOBILEPHONE$22 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "MobilePhone");
    private static final javax.xml.namespace.QName WORKPHONE$24 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "WorkPhone");
    private static final javax.xml.namespace.QName EMAIL$26 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Email");
    
    
    /**
     * Gets the "Title" element
     */
    public java.lang.String getTitle()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TITLE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Title" element
     */
    public com.idanalytics.products.idscore.request.Title xgetTitle()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Title target = null;
            target = (com.idanalytics.products.idscore.request.Title)get_store().find_element_user(TITLE$0, 0);
            return target;
        }
    }
    
    /**
     * True if has "Title" element
     */
    public boolean isSetTitle()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(TITLE$0) != 0;
        }
    }
    
    /**
     * Sets the "Title" element
     */
    public void setTitle(java.lang.String title)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TITLE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TITLE$0);
            }
            target.setStringValue(title);
        }
    }
    
    /**
     * Sets (as xml) the "Title" element
     */
    public void xsetTitle(com.idanalytics.products.idscore.request.Title title)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Title target = null;
            target = (com.idanalytics.products.idscore.request.Title)get_store().find_element_user(TITLE$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Title)get_store().add_element_user(TITLE$0);
            }
            target.set(title);
        }
    }
    
    /**
     * Unsets the "Title" element
     */
    public void unsetTitle()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(TITLE$0, 0);
        }
    }
    
    /**
     * Gets the "FirstName" element
     */
    public java.lang.String getFirstName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FIRSTNAME$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "FirstName" element
     */
    public com.idanalytics.products.idscore.request.Name xgetFirstName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Name target = null;
            target = (com.idanalytics.products.idscore.request.Name)get_store().find_element_user(FIRSTNAME$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "FirstName" element
     */
    public void setFirstName(java.lang.String firstName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FIRSTNAME$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(FIRSTNAME$2);
            }
            target.setStringValue(firstName);
        }
    }
    
    /**
     * Sets (as xml) the "FirstName" element
     */
    public void xsetFirstName(com.idanalytics.products.idscore.request.Name firstName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Name target = null;
            target = (com.idanalytics.products.idscore.request.Name)get_store().find_element_user(FIRSTNAME$2, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Name)get_store().add_element_user(FIRSTNAME$2);
            }
            target.set(firstName);
        }
    }
    
    /**
     * Gets the "MiddleName" element
     */
    public java.lang.String getMiddleName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MIDDLENAME$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "MiddleName" element
     */
    public com.idanalytics.products.idscore.request.Name xgetMiddleName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Name target = null;
            target = (com.idanalytics.products.idscore.request.Name)get_store().find_element_user(MIDDLENAME$4, 0);
            return target;
        }
    }
    
    /**
     * True if has "MiddleName" element
     */
    public boolean isSetMiddleName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(MIDDLENAME$4) != 0;
        }
    }
    
    /**
     * Sets the "MiddleName" element
     */
    public void setMiddleName(java.lang.String middleName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MIDDLENAME$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(MIDDLENAME$4);
            }
            target.setStringValue(middleName);
        }
    }
    
    /**
     * Sets (as xml) the "MiddleName" element
     */
    public void xsetMiddleName(com.idanalytics.products.idscore.request.Name middleName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Name target = null;
            target = (com.idanalytics.products.idscore.request.Name)get_store().find_element_user(MIDDLENAME$4, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Name)get_store().add_element_user(MIDDLENAME$4);
            }
            target.set(middleName);
        }
    }
    
    /**
     * Unsets the "MiddleName" element
     */
    public void unsetMiddleName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(MIDDLENAME$4, 0);
        }
    }
    
    /**
     * Gets the "LastName" element
     */
    public java.lang.String getLastName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LASTNAME$6, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "LastName" element
     */
    public com.idanalytics.products.idscore.request.Name xgetLastName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Name target = null;
            target = (com.idanalytics.products.idscore.request.Name)get_store().find_element_user(LASTNAME$6, 0);
            return target;
        }
    }
    
    /**
     * Sets the "LastName" element
     */
    public void setLastName(java.lang.String lastName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LASTNAME$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(LASTNAME$6);
            }
            target.setStringValue(lastName);
        }
    }
    
    /**
     * Sets (as xml) the "LastName" element
     */
    public void xsetLastName(com.idanalytics.products.idscore.request.Name lastName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Name target = null;
            target = (com.idanalytics.products.idscore.request.Name)get_store().find_element_user(LASTNAME$6, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Name)get_store().add_element_user(LASTNAME$6);
            }
            target.set(lastName);
        }
    }
    
    /**
     * Gets the "Suffix" element
     */
    public java.lang.String getSuffix()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SUFFIX$8, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Suffix" element
     */
    public com.idanalytics.products.idscore.request.Name xgetSuffix()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Name target = null;
            target = (com.idanalytics.products.idscore.request.Name)get_store().find_element_user(SUFFIX$8, 0);
            return target;
        }
    }
    
    /**
     * True if has "Suffix" element
     */
    public boolean isSetSuffix()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SUFFIX$8) != 0;
        }
    }
    
    /**
     * Sets the "Suffix" element
     */
    public void setSuffix(java.lang.String suffix)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SUFFIX$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SUFFIX$8);
            }
            target.setStringValue(suffix);
        }
    }
    
    /**
     * Sets (as xml) the "Suffix" element
     */
    public void xsetSuffix(com.idanalytics.products.idscore.request.Name suffix)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Name target = null;
            target = (com.idanalytics.products.idscore.request.Name)get_store().find_element_user(SUFFIX$8, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Name)get_store().add_element_user(SUFFIX$8);
            }
            target.set(suffix);
        }
    }
    
    /**
     * Unsets the "Suffix" element
     */
    public void unsetSuffix()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SUFFIX$8, 0);
        }
    }
    
    /**
     * Gets the "Address" element
     */
    public java.lang.String getAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ADDRESS$10, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Address" element
     */
    public com.idanalytics.products.idscore.request.AddressLine xgetAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.AddressLine target = null;
            target = (com.idanalytics.products.idscore.request.AddressLine)get_store().find_element_user(ADDRESS$10, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Address" element
     */
    public void setAddress(java.lang.String address)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ADDRESS$10, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ADDRESS$10);
            }
            target.setStringValue(address);
        }
    }
    
    /**
     * Sets (as xml) the "Address" element
     */
    public void xsetAddress(com.idanalytics.products.idscore.request.AddressLine address)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.AddressLine target = null;
            target = (com.idanalytics.products.idscore.request.AddressLine)get_store().find_element_user(ADDRESS$10, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.AddressLine)get_store().add_element_user(ADDRESS$10);
            }
            target.set(address);
        }
    }
    
    /**
     * Gets the "City" element
     */
    public java.lang.String getCity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CITY$12, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "City" element
     */
    public com.idanalytics.products.idscore.request.City xgetCity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.City target = null;
            target = (com.idanalytics.products.idscore.request.City)get_store().find_element_user(CITY$12, 0);
            return target;
        }
    }
    
    /**
     * Sets the "City" element
     */
    public void setCity(java.lang.String city)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CITY$12, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CITY$12);
            }
            target.setStringValue(city);
        }
    }
    
    /**
     * Sets (as xml) the "City" element
     */
    public void xsetCity(com.idanalytics.products.idscore.request.City city)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.City target = null;
            target = (com.idanalytics.products.idscore.request.City)get_store().find_element_user(CITY$12, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.City)get_store().add_element_user(CITY$12);
            }
            target.set(city);
        }
    }
    
    /**
     * Gets the "State" element
     */
    public java.lang.String getState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STATE$14, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "State" element
     */
    public com.idanalytics.products.idscore.request.State xgetState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.State target = null;
            target = (com.idanalytics.products.idscore.request.State)get_store().find_element_user(STATE$14, 0);
            return target;
        }
    }
    
    /**
     * Sets the "State" element
     */
    public void setState(java.lang.String state)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STATE$14, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(STATE$14);
            }
            target.setStringValue(state);
        }
    }
    
    /**
     * Sets (as xml) the "State" element
     */
    public void xsetState(com.idanalytics.products.idscore.request.State state)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.State target = null;
            target = (com.idanalytics.products.idscore.request.State)get_store().find_element_user(STATE$14, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.State)get_store().add_element_user(STATE$14);
            }
            target.set(state);
        }
    }
    
    /**
     * Gets the "Zip" element
     */
    public java.lang.String getZip()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ZIP$16, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Zip" element
     */
    public com.idanalytics.products.idscore.request.Zip xgetZip()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Zip target = null;
            target = (com.idanalytics.products.idscore.request.Zip)get_store().find_element_user(ZIP$16, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Zip" element
     */
    public void setZip(java.lang.String zip)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ZIP$16, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ZIP$16);
            }
            target.setStringValue(zip);
        }
    }
    
    /**
     * Sets (as xml) the "Zip" element
     */
    public void xsetZip(com.idanalytics.products.idscore.request.Zip zip)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Zip target = null;
            target = (com.idanalytics.products.idscore.request.Zip)get_store().find_element_user(ZIP$16, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Zip)get_store().add_element_user(ZIP$16);
            }
            target.set(zip);
        }
    }
    
    /**
     * Gets the "Country" element
     */
    public java.lang.String getCountry()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(COUNTRY$18, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Country" element
     */
    public com.idanalytics.products.idscore.request.Country xgetCountry()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Country target = null;
            target = (com.idanalytics.products.idscore.request.Country)get_store().find_element_user(COUNTRY$18, 0);
            return target;
        }
    }
    
    /**
     * True if has "Country" element
     */
    public boolean isSetCountry()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(COUNTRY$18) != 0;
        }
    }
    
    /**
     * Sets the "Country" element
     */
    public void setCountry(java.lang.String country)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(COUNTRY$18, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(COUNTRY$18);
            }
            target.setStringValue(country);
        }
    }
    
    /**
     * Sets (as xml) the "Country" element
     */
    public void xsetCountry(com.idanalytics.products.idscore.request.Country country)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Country target = null;
            target = (com.idanalytics.products.idscore.request.Country)get_store().find_element_user(COUNTRY$18, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Country)get_store().add_element_user(COUNTRY$18);
            }
            target.set(country);
        }
    }
    
    /**
     * Unsets the "Country" element
     */
    public void unsetCountry()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(COUNTRY$18, 0);
        }
    }
    
    /**
     * Gets the "HomePhone" element
     */
    public java.lang.String getHomePhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(HOMEPHONE$20, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "HomePhone" element
     */
    public com.idanalytics.products.idscore.request.Phone xgetHomePhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().find_element_user(HOMEPHONE$20, 0);
            return target;
        }
    }
    
    /**
     * Sets the "HomePhone" element
     */
    public void setHomePhone(java.lang.String homePhone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(HOMEPHONE$20, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(HOMEPHONE$20);
            }
            target.setStringValue(homePhone);
        }
    }
    
    /**
     * Sets (as xml) the "HomePhone" element
     */
    public void xsetHomePhone(com.idanalytics.products.idscore.request.Phone homePhone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().find_element_user(HOMEPHONE$20, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Phone)get_store().add_element_user(HOMEPHONE$20);
            }
            target.set(homePhone);
        }
    }
    
    /**
     * Gets the "MobilePhone" element
     */
    public java.lang.String getMobilePhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MOBILEPHONE$22, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "MobilePhone" element
     */
    public com.idanalytics.products.idscore.request.Phone xgetMobilePhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().find_element_user(MOBILEPHONE$22, 0);
            return target;
        }
    }
    
    /**
     * True if has "MobilePhone" element
     */
    public boolean isSetMobilePhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(MOBILEPHONE$22) != 0;
        }
    }
    
    /**
     * Sets the "MobilePhone" element
     */
    public void setMobilePhone(java.lang.String mobilePhone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MOBILEPHONE$22, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(MOBILEPHONE$22);
            }
            target.setStringValue(mobilePhone);
        }
    }
    
    /**
     * Sets (as xml) the "MobilePhone" element
     */
    public void xsetMobilePhone(com.idanalytics.products.idscore.request.Phone mobilePhone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().find_element_user(MOBILEPHONE$22, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Phone)get_store().add_element_user(MOBILEPHONE$22);
            }
            target.set(mobilePhone);
        }
    }
    
    /**
     * Unsets the "MobilePhone" element
     */
    public void unsetMobilePhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(MOBILEPHONE$22, 0);
        }
    }
    
    /**
     * Gets the "WorkPhone" element
     */
    public java.lang.String getWorkPhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(WORKPHONE$24, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "WorkPhone" element
     */
    public com.idanalytics.products.idscore.request.Phone xgetWorkPhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().find_element_user(WORKPHONE$24, 0);
            return target;
        }
    }
    
    /**
     * True if has "WorkPhone" element
     */
    public boolean isSetWorkPhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(WORKPHONE$24) != 0;
        }
    }
    
    /**
     * Sets the "WorkPhone" element
     */
    public void setWorkPhone(java.lang.String workPhone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(WORKPHONE$24, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(WORKPHONE$24);
            }
            target.setStringValue(workPhone);
        }
    }
    
    /**
     * Sets (as xml) the "WorkPhone" element
     */
    public void xsetWorkPhone(com.idanalytics.products.idscore.request.Phone workPhone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().find_element_user(WORKPHONE$24, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Phone)get_store().add_element_user(WORKPHONE$24);
            }
            target.set(workPhone);
        }
    }
    
    /**
     * Unsets the "WorkPhone" element
     */
    public void unsetWorkPhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(WORKPHONE$24, 0);
        }
    }
    
    /**
     * Gets the "Email" element
     */
    public java.lang.String getEmail()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EMAIL$26, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Email" element
     */
    public com.idanalytics.products.idscore.request.Email xgetEmail()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Email target = null;
            target = (com.idanalytics.products.idscore.request.Email)get_store().find_element_user(EMAIL$26, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Email" element
     */
    public void setEmail(java.lang.String email)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EMAIL$26, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(EMAIL$26);
            }
            target.setStringValue(email);
        }
    }
    
    /**
     * Sets (as xml) the "Email" element
     */
    public void xsetEmail(com.idanalytics.products.idscore.request.Email email)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Email target = null;
            target = (com.idanalytics.products.idscore.request.Email)get_store().find_element_user(EMAIL$26, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Email)get_store().add_element_user(EMAIL$26);
            }
            target.set(email);
        }
    }
}
