/*
 * XML Type:  Employment
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.Employment
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML Employment(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public class EmploymentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.Employment
{
    private static final long serialVersionUID = 1L;
    
    public EmploymentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName EMPLOYERTYPE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "EmployerType");
    private static final javax.xml.namespace.QName EMPLOYMENTTYPE$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "EmploymentType");
    private static final javax.xml.namespace.QName NAME$4 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Name");
    private static final javax.xml.namespace.QName ADDRESS$6 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Address");
    private static final javax.xml.namespace.QName PARSEDADDRESS$8 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ParsedAddress");
    private static final javax.xml.namespace.QName CITY$10 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "City");
    private static final javax.xml.namespace.QName STATE$12 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "State");
    private static final javax.xml.namespace.QName ZIP$14 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Zip");
    private static final javax.xml.namespace.QName COUNTRY$16 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Country");
    private static final javax.xml.namespace.QName PHONE$18 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Phone");
    private static final javax.xml.namespace.QName TIMEATEMPLOYER$20 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "TimeAtEmployer");
    private static final javax.xml.namespace.QName STARTDATE$22 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "StartDate");
    private static final javax.xml.namespace.QName TITLE$24 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Title");
    private static final javax.xml.namespace.QName SALARY$26 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Salary");
    private static final javax.xml.namespace.QName SALARYPERIODICITY$28 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "SalaryPeriodicity");
    private static final javax.xml.namespace.QName PREVEMPLOYERTYPE$30 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrevEmployerType");
    private static final javax.xml.namespace.QName PREVEMPLOYMENTTYPE$32 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrevEmploymentType");
    private static final javax.xml.namespace.QName PREVNAME$34 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrevName");
    private static final javax.xml.namespace.QName PREVADDRESS$36 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrevAddress");
    private static final javax.xml.namespace.QName PARSEDPREVADDRESS$38 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ParsedPrevAddress");
    private static final javax.xml.namespace.QName PREVCITY$40 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrevCity");
    private static final javax.xml.namespace.QName PREVSTATE$42 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrevState");
    private static final javax.xml.namespace.QName PREVZIP$44 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrevZip");
    private static final javax.xml.namespace.QName PREVCOUNTRY$46 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrevCountry");
    private static final javax.xml.namespace.QName PREVPHONE$48 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrevPhone");
    private static final javax.xml.namespace.QName PREVTIMEATEMPLOYER$50 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrevTimeAtEmployer");
    private static final javax.xml.namespace.QName PREVSTARTDATE$52 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrevStartDate");
    private static final javax.xml.namespace.QName PREVTITLE$54 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrevTitle");
    private static final javax.xml.namespace.QName PREVSALARY$56 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrevSalary");
    private static final javax.xml.namespace.QName PREVSALARYPERIODICITY$58 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrevSalaryPeriodicity");
    
    
    /**
     * Gets the "EmployerType" element
     */
    public java.lang.String getEmployerType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EMPLOYERTYPE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "EmployerType" element
     */
    public org.apache.xmlbeans.XmlString xgetEmployerType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(EMPLOYERTYPE$0, 0);
            return target;
        }
    }
    
    /**
     * True if has "EmployerType" element
     */
    public boolean isSetEmployerType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(EMPLOYERTYPE$0) != 0;
        }
    }
    
    /**
     * Sets the "EmployerType" element
     */
    public void setEmployerType(java.lang.String employerType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EMPLOYERTYPE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(EMPLOYERTYPE$0);
            }
            target.setStringValue(employerType);
        }
    }
    
    /**
     * Sets (as xml) the "EmployerType" element
     */
    public void xsetEmployerType(org.apache.xmlbeans.XmlString employerType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(EMPLOYERTYPE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(EMPLOYERTYPE$0);
            }
            target.set(employerType);
        }
    }
    
    /**
     * Unsets the "EmployerType" element
     */
    public void unsetEmployerType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(EMPLOYERTYPE$0, 0);
        }
    }
    
    /**
     * Gets the "EmploymentType" element
     */
    public java.lang.String getEmploymentType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EMPLOYMENTTYPE$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "EmploymentType" element
     */
    public com.idanalytics.products.idscore.request.EmploymentType xgetEmploymentType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.EmploymentType target = null;
            target = (com.idanalytics.products.idscore.request.EmploymentType)get_store().find_element_user(EMPLOYMENTTYPE$2, 0);
            return target;
        }
    }
    
    /**
     * True if has "EmploymentType" element
     */
    public boolean isSetEmploymentType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(EMPLOYMENTTYPE$2) != 0;
        }
    }
    
    /**
     * Sets the "EmploymentType" element
     */
    public void setEmploymentType(java.lang.String employmentType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EMPLOYMENTTYPE$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(EMPLOYMENTTYPE$2);
            }
            target.setStringValue(employmentType);
        }
    }
    
    /**
     * Sets (as xml) the "EmploymentType" element
     */
    public void xsetEmploymentType(com.idanalytics.products.idscore.request.EmploymentType employmentType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.EmploymentType target = null;
            target = (com.idanalytics.products.idscore.request.EmploymentType)get_store().find_element_user(EMPLOYMENTTYPE$2, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.EmploymentType)get_store().add_element_user(EMPLOYMENTTYPE$2);
            }
            target.set(employmentType);
        }
    }
    
    /**
     * Unsets the "EmploymentType" element
     */
    public void unsetEmploymentType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(EMPLOYMENTTYPE$2, 0);
        }
    }
    
    /**
     * Gets the "Name" element
     */
    public java.lang.String getName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NAME$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Name" element
     */
    public com.idanalytics.products.idscore.request.EmployerName xgetName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.EmployerName target = null;
            target = (com.idanalytics.products.idscore.request.EmployerName)get_store().find_element_user(NAME$4, 0);
            return target;
        }
    }
    
    /**
     * True if has "Name" element
     */
    public boolean isSetName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(NAME$4) != 0;
        }
    }
    
    /**
     * Sets the "Name" element
     */
    public void setName(java.lang.String name)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NAME$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(NAME$4);
            }
            target.setStringValue(name);
        }
    }
    
    /**
     * Sets (as xml) the "Name" element
     */
    public void xsetName(com.idanalytics.products.idscore.request.EmployerName name)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.EmployerName target = null;
            target = (com.idanalytics.products.idscore.request.EmployerName)get_store().find_element_user(NAME$4, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.EmployerName)get_store().add_element_user(NAME$4);
            }
            target.set(name);
        }
    }
    
    /**
     * Unsets the "Name" element
     */
    public void unsetName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(NAME$4, 0);
        }
    }
    
    /**
     * Gets the "Address" element
     */
    public com.idanalytics.products.idscore.request.Address getAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Address target = null;
            target = (com.idanalytics.products.idscore.request.Address)get_store().find_element_user(ADDRESS$6, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "Address" element
     */
    public boolean isSetAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ADDRESS$6) != 0;
        }
    }
    
    /**
     * Sets the "Address" element
     */
    public void setAddress(com.idanalytics.products.idscore.request.Address address)
    {
        generatedSetterHelperImpl(address, ADDRESS$6, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "Address" element
     */
    public com.idanalytics.products.idscore.request.Address addNewAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Address target = null;
            target = (com.idanalytics.products.idscore.request.Address)get_store().add_element_user(ADDRESS$6);
            return target;
        }
    }
    
    /**
     * Unsets the "Address" element
     */
    public void unsetAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ADDRESS$6, 0);
        }
    }
    
    /**
     * Gets the "ParsedAddress" element
     */
    public com.idanalytics.products.idscore.request.ParsedAddress getParsedAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ParsedAddress target = null;
            target = (com.idanalytics.products.idscore.request.ParsedAddress)get_store().find_element_user(PARSEDADDRESS$8, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "ParsedAddress" element
     */
    public boolean isSetParsedAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PARSEDADDRESS$8) != 0;
        }
    }
    
    /**
     * Sets the "ParsedAddress" element
     */
    public void setParsedAddress(com.idanalytics.products.idscore.request.ParsedAddress parsedAddress)
    {
        generatedSetterHelperImpl(parsedAddress, PARSEDADDRESS$8, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "ParsedAddress" element
     */
    public com.idanalytics.products.idscore.request.ParsedAddress addNewParsedAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ParsedAddress target = null;
            target = (com.idanalytics.products.idscore.request.ParsedAddress)get_store().add_element_user(PARSEDADDRESS$8);
            return target;
        }
    }
    
    /**
     * Unsets the "ParsedAddress" element
     */
    public void unsetParsedAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PARSEDADDRESS$8, 0);
        }
    }
    
    /**
     * Gets the "City" element
     */
    public java.lang.String getCity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CITY$10, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "City" element
     */
    public com.idanalytics.products.idscore.request.City xgetCity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.City target = null;
            target = (com.idanalytics.products.idscore.request.City)get_store().find_element_user(CITY$10, 0);
            return target;
        }
    }
    
    /**
     * True if has "City" element
     */
    public boolean isSetCity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CITY$10) != 0;
        }
    }
    
    /**
     * Sets the "City" element
     */
    public void setCity(java.lang.String city)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CITY$10, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CITY$10);
            }
            target.setStringValue(city);
        }
    }
    
    /**
     * Sets (as xml) the "City" element
     */
    public void xsetCity(com.idanalytics.products.idscore.request.City city)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.City target = null;
            target = (com.idanalytics.products.idscore.request.City)get_store().find_element_user(CITY$10, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.City)get_store().add_element_user(CITY$10);
            }
            target.set(city);
        }
    }
    
    /**
     * Unsets the "City" element
     */
    public void unsetCity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CITY$10, 0);
        }
    }
    
    /**
     * Gets the "State" element
     */
    public java.lang.String getState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STATE$12, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "State" element
     */
    public com.idanalytics.products.idscore.request.State xgetState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.State target = null;
            target = (com.idanalytics.products.idscore.request.State)get_store().find_element_user(STATE$12, 0);
            return target;
        }
    }
    
    /**
     * True if has "State" element
     */
    public boolean isSetState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(STATE$12) != 0;
        }
    }
    
    /**
     * Sets the "State" element
     */
    public void setState(java.lang.String state)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STATE$12, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(STATE$12);
            }
            target.setStringValue(state);
        }
    }
    
    /**
     * Sets (as xml) the "State" element
     */
    public void xsetState(com.idanalytics.products.idscore.request.State state)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.State target = null;
            target = (com.idanalytics.products.idscore.request.State)get_store().find_element_user(STATE$12, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.State)get_store().add_element_user(STATE$12);
            }
            target.set(state);
        }
    }
    
    /**
     * Unsets the "State" element
     */
    public void unsetState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(STATE$12, 0);
        }
    }
    
    /**
     * Gets the "Zip" element
     */
    public java.lang.String getZip()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ZIP$14, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Zip" element
     */
    public com.idanalytics.products.idscore.request.Zip xgetZip()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Zip target = null;
            target = (com.idanalytics.products.idscore.request.Zip)get_store().find_element_user(ZIP$14, 0);
            return target;
        }
    }
    
    /**
     * True if has "Zip" element
     */
    public boolean isSetZip()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ZIP$14) != 0;
        }
    }
    
    /**
     * Sets the "Zip" element
     */
    public void setZip(java.lang.String zip)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ZIP$14, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ZIP$14);
            }
            target.setStringValue(zip);
        }
    }
    
    /**
     * Sets (as xml) the "Zip" element
     */
    public void xsetZip(com.idanalytics.products.idscore.request.Zip zip)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Zip target = null;
            target = (com.idanalytics.products.idscore.request.Zip)get_store().find_element_user(ZIP$14, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Zip)get_store().add_element_user(ZIP$14);
            }
            target.set(zip);
        }
    }
    
    /**
     * Unsets the "Zip" element
     */
    public void unsetZip()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ZIP$14, 0);
        }
    }
    
    /**
     * Gets the "Country" element
     */
    public java.lang.String getCountry()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(COUNTRY$16, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Country" element
     */
    public com.idanalytics.products.idscore.request.Country xgetCountry()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Country target = null;
            target = (com.idanalytics.products.idscore.request.Country)get_store().find_element_user(COUNTRY$16, 0);
            return target;
        }
    }
    
    /**
     * True if has "Country" element
     */
    public boolean isSetCountry()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(COUNTRY$16) != 0;
        }
    }
    
    /**
     * Sets the "Country" element
     */
    public void setCountry(java.lang.String country)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(COUNTRY$16, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(COUNTRY$16);
            }
            target.setStringValue(country);
        }
    }
    
    /**
     * Sets (as xml) the "Country" element
     */
    public void xsetCountry(com.idanalytics.products.idscore.request.Country country)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Country target = null;
            target = (com.idanalytics.products.idscore.request.Country)get_store().find_element_user(COUNTRY$16, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Country)get_store().add_element_user(COUNTRY$16);
            }
            target.set(country);
        }
    }
    
    /**
     * Unsets the "Country" element
     */
    public void unsetCountry()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(COUNTRY$16, 0);
        }
    }
    
    /**
     * Gets the "Phone" element
     */
    public java.lang.String getPhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PHONE$18, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Phone" element
     */
    public com.idanalytics.products.idscore.request.Phone xgetPhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().find_element_user(PHONE$18, 0);
            return target;
        }
    }
    
    /**
     * True if has "Phone" element
     */
    public boolean isSetPhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PHONE$18) != 0;
        }
    }
    
    /**
     * Sets the "Phone" element
     */
    public void setPhone(java.lang.String phone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PHONE$18, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PHONE$18);
            }
            target.setStringValue(phone);
        }
    }
    
    /**
     * Sets (as xml) the "Phone" element
     */
    public void xsetPhone(com.idanalytics.products.idscore.request.Phone phone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().find_element_user(PHONE$18, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Phone)get_store().add_element_user(PHONE$18);
            }
            target.set(phone);
        }
    }
    
    /**
     * Unsets the "Phone" element
     */
    public void unsetPhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PHONE$18, 0);
        }
    }
    
    /**
     * Gets the "TimeAtEmployer" element
     */
    public java.lang.String getTimeAtEmployer()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TIMEATEMPLOYER$20, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "TimeAtEmployer" element
     */
    public com.idanalytics.products.idscore.request.TimeAt xgetTimeAtEmployer()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.TimeAt target = null;
            target = (com.idanalytics.products.idscore.request.TimeAt)get_store().find_element_user(TIMEATEMPLOYER$20, 0);
            return target;
        }
    }
    
    /**
     * True if has "TimeAtEmployer" element
     */
    public boolean isSetTimeAtEmployer()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(TIMEATEMPLOYER$20) != 0;
        }
    }
    
    /**
     * Sets the "TimeAtEmployer" element
     */
    public void setTimeAtEmployer(java.lang.String timeAtEmployer)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TIMEATEMPLOYER$20, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TIMEATEMPLOYER$20);
            }
            target.setStringValue(timeAtEmployer);
        }
    }
    
    /**
     * Sets (as xml) the "TimeAtEmployer" element
     */
    public void xsetTimeAtEmployer(com.idanalytics.products.idscore.request.TimeAt timeAtEmployer)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.TimeAt target = null;
            target = (com.idanalytics.products.idscore.request.TimeAt)get_store().find_element_user(TIMEATEMPLOYER$20, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.TimeAt)get_store().add_element_user(TIMEATEMPLOYER$20);
            }
            target.set(timeAtEmployer);
        }
    }
    
    /**
     * Unsets the "TimeAtEmployer" element
     */
    public void unsetTimeAtEmployer()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(TIMEATEMPLOYER$20, 0);
        }
    }
    
    /**
     * Gets the "StartDate" element
     */
    public java.lang.Object getStartDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STARTDATE$22, 0);
            if (target == null)
            {
                return null;
            }
            return target.getObjectValue();
        }
    }
    
    /**
     * Gets (as xml) the "StartDate" element
     */
    public com.idanalytics.products.idscore.request.Date xgetStartDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Date target = null;
            target = (com.idanalytics.products.idscore.request.Date)get_store().find_element_user(STARTDATE$22, 0);
            return target;
        }
    }
    
    /**
     * True if has "StartDate" element
     */
    public boolean isSetStartDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(STARTDATE$22) != 0;
        }
    }
    
    /**
     * Sets the "StartDate" element
     */
    public void setStartDate(java.lang.Object startDate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STARTDATE$22, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(STARTDATE$22);
            }
            target.setObjectValue(startDate);
        }
    }
    
    /**
     * Sets (as xml) the "StartDate" element
     */
    public void xsetStartDate(com.idanalytics.products.idscore.request.Date startDate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Date target = null;
            target = (com.idanalytics.products.idscore.request.Date)get_store().find_element_user(STARTDATE$22, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Date)get_store().add_element_user(STARTDATE$22);
            }
            target.set(startDate);
        }
    }
    
    /**
     * Unsets the "StartDate" element
     */
    public void unsetStartDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(STARTDATE$22, 0);
        }
    }
    
    /**
     * Gets the "Title" element
     */
    public java.lang.String getTitle()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TITLE$24, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Title" element
     */
    public com.idanalytics.products.idscore.request.EmploymentTitle xgetTitle()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.EmploymentTitle target = null;
            target = (com.idanalytics.products.idscore.request.EmploymentTitle)get_store().find_element_user(TITLE$24, 0);
            return target;
        }
    }
    
    /**
     * True if has "Title" element
     */
    public boolean isSetTitle()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(TITLE$24) != 0;
        }
    }
    
    /**
     * Sets the "Title" element
     */
    public void setTitle(java.lang.String title)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TITLE$24, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TITLE$24);
            }
            target.setStringValue(title);
        }
    }
    
    /**
     * Sets (as xml) the "Title" element
     */
    public void xsetTitle(com.idanalytics.products.idscore.request.EmploymentTitle title)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.EmploymentTitle target = null;
            target = (com.idanalytics.products.idscore.request.EmploymentTitle)get_store().find_element_user(TITLE$24, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.EmploymentTitle)get_store().add_element_user(TITLE$24);
            }
            target.set(title);
        }
    }
    
    /**
     * Unsets the "Title" element
     */
    public void unsetTitle()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(TITLE$24, 0);
        }
    }
    
    /**
     * Gets the "Salary" element
     */
    public java.lang.Object getSalary()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SALARY$26, 0);
            if (target == null)
            {
                return null;
            }
            return target.getObjectValue();
        }
    }
    
    /**
     * Gets (as xml) the "Salary" element
     */
    public com.idanalytics.products.idscore.request.Income xgetSalary()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Income target = null;
            target = (com.idanalytics.products.idscore.request.Income)get_store().find_element_user(SALARY$26, 0);
            return target;
        }
    }
    
    /**
     * True if has "Salary" element
     */
    public boolean isSetSalary()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SALARY$26) != 0;
        }
    }
    
    /**
     * Sets the "Salary" element
     */
    public void setSalary(java.lang.Object salary)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SALARY$26, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SALARY$26);
            }
            target.setObjectValue(salary);
        }
    }
    
    /**
     * Sets (as xml) the "Salary" element
     */
    public void xsetSalary(com.idanalytics.products.idscore.request.Income salary)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Income target = null;
            target = (com.idanalytics.products.idscore.request.Income)get_store().find_element_user(SALARY$26, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Income)get_store().add_element_user(SALARY$26);
            }
            target.set(salary);
        }
    }
    
    /**
     * Unsets the "Salary" element
     */
    public void unsetSalary()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SALARY$26, 0);
        }
    }
    
    /**
     * Gets the "SalaryPeriodicity" element
     */
    public com.idanalytics.products.idscore.request.Periodicity.Enum getSalaryPeriodicity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SALARYPERIODICITY$28, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.idscore.request.Periodicity.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "SalaryPeriodicity" element
     */
    public com.idanalytics.products.idscore.request.Periodicity xgetSalaryPeriodicity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Periodicity target = null;
            target = (com.idanalytics.products.idscore.request.Periodicity)get_store().find_element_user(SALARYPERIODICITY$28, 0);
            return target;
        }
    }
    
    /**
     * True if has "SalaryPeriodicity" element
     */
    public boolean isSetSalaryPeriodicity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SALARYPERIODICITY$28) != 0;
        }
    }
    
    /**
     * Sets the "SalaryPeriodicity" element
     */
    public void setSalaryPeriodicity(com.idanalytics.products.idscore.request.Periodicity.Enum salaryPeriodicity)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SALARYPERIODICITY$28, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SALARYPERIODICITY$28);
            }
            target.setEnumValue(salaryPeriodicity);
        }
    }
    
    /**
     * Sets (as xml) the "SalaryPeriodicity" element
     */
    public void xsetSalaryPeriodicity(com.idanalytics.products.idscore.request.Periodicity salaryPeriodicity)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Periodicity target = null;
            target = (com.idanalytics.products.idscore.request.Periodicity)get_store().find_element_user(SALARYPERIODICITY$28, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Periodicity)get_store().add_element_user(SALARYPERIODICITY$28);
            }
            target.set(salaryPeriodicity);
        }
    }
    
    /**
     * Unsets the "SalaryPeriodicity" element
     */
    public void unsetSalaryPeriodicity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SALARYPERIODICITY$28, 0);
        }
    }
    
    /**
     * Gets the "PrevEmployerType" element
     */
    public java.lang.String getPrevEmployerType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVEMPLOYERTYPE$30, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrevEmployerType" element
     */
    public com.idanalytics.products.idscore.request.EmployerType xgetPrevEmployerType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.EmployerType target = null;
            target = (com.idanalytics.products.idscore.request.EmployerType)get_store().find_element_user(PREVEMPLOYERTYPE$30, 0);
            return target;
        }
    }
    
    /**
     * True if has "PrevEmployerType" element
     */
    public boolean isSetPrevEmployerType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREVEMPLOYERTYPE$30) != 0;
        }
    }
    
    /**
     * Sets the "PrevEmployerType" element
     */
    public void setPrevEmployerType(java.lang.String prevEmployerType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVEMPLOYERTYPE$30, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREVEMPLOYERTYPE$30);
            }
            target.setStringValue(prevEmployerType);
        }
    }
    
    /**
     * Sets (as xml) the "PrevEmployerType" element
     */
    public void xsetPrevEmployerType(com.idanalytics.products.idscore.request.EmployerType prevEmployerType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.EmployerType target = null;
            target = (com.idanalytics.products.idscore.request.EmployerType)get_store().find_element_user(PREVEMPLOYERTYPE$30, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.EmployerType)get_store().add_element_user(PREVEMPLOYERTYPE$30);
            }
            target.set(prevEmployerType);
        }
    }
    
    /**
     * Unsets the "PrevEmployerType" element
     */
    public void unsetPrevEmployerType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREVEMPLOYERTYPE$30, 0);
        }
    }
    
    /**
     * Gets the "PrevEmploymentType" element
     */
    public java.lang.String getPrevEmploymentType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVEMPLOYMENTTYPE$32, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrevEmploymentType" element
     */
    public com.idanalytics.products.idscore.request.EmploymentType xgetPrevEmploymentType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.EmploymentType target = null;
            target = (com.idanalytics.products.idscore.request.EmploymentType)get_store().find_element_user(PREVEMPLOYMENTTYPE$32, 0);
            return target;
        }
    }
    
    /**
     * True if has "PrevEmploymentType" element
     */
    public boolean isSetPrevEmploymentType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREVEMPLOYMENTTYPE$32) != 0;
        }
    }
    
    /**
     * Sets the "PrevEmploymentType" element
     */
    public void setPrevEmploymentType(java.lang.String prevEmploymentType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVEMPLOYMENTTYPE$32, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREVEMPLOYMENTTYPE$32);
            }
            target.setStringValue(prevEmploymentType);
        }
    }
    
    /**
     * Sets (as xml) the "PrevEmploymentType" element
     */
    public void xsetPrevEmploymentType(com.idanalytics.products.idscore.request.EmploymentType prevEmploymentType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.EmploymentType target = null;
            target = (com.idanalytics.products.idscore.request.EmploymentType)get_store().find_element_user(PREVEMPLOYMENTTYPE$32, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.EmploymentType)get_store().add_element_user(PREVEMPLOYMENTTYPE$32);
            }
            target.set(prevEmploymentType);
        }
    }
    
    /**
     * Unsets the "PrevEmploymentType" element
     */
    public void unsetPrevEmploymentType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREVEMPLOYMENTTYPE$32, 0);
        }
    }
    
    /**
     * Gets the "PrevName" element
     */
    public java.lang.String getPrevName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVNAME$34, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrevName" element
     */
    public com.idanalytics.products.idscore.request.EmployerName xgetPrevName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.EmployerName target = null;
            target = (com.idanalytics.products.idscore.request.EmployerName)get_store().find_element_user(PREVNAME$34, 0);
            return target;
        }
    }
    
    /**
     * True if has "PrevName" element
     */
    public boolean isSetPrevName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREVNAME$34) != 0;
        }
    }
    
    /**
     * Sets the "PrevName" element
     */
    public void setPrevName(java.lang.String prevName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVNAME$34, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREVNAME$34);
            }
            target.setStringValue(prevName);
        }
    }
    
    /**
     * Sets (as xml) the "PrevName" element
     */
    public void xsetPrevName(com.idanalytics.products.idscore.request.EmployerName prevName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.EmployerName target = null;
            target = (com.idanalytics.products.idscore.request.EmployerName)get_store().find_element_user(PREVNAME$34, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.EmployerName)get_store().add_element_user(PREVNAME$34);
            }
            target.set(prevName);
        }
    }
    
    /**
     * Unsets the "PrevName" element
     */
    public void unsetPrevName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREVNAME$34, 0);
        }
    }
    
    /**
     * Gets the "PrevAddress" element
     */
    public com.idanalytics.products.idscore.request.Address getPrevAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Address target = null;
            target = (com.idanalytics.products.idscore.request.Address)get_store().find_element_user(PREVADDRESS$36, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "PrevAddress" element
     */
    public boolean isSetPrevAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREVADDRESS$36) != 0;
        }
    }
    
    /**
     * Sets the "PrevAddress" element
     */
    public void setPrevAddress(com.idanalytics.products.idscore.request.Address prevAddress)
    {
        generatedSetterHelperImpl(prevAddress, PREVADDRESS$36, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "PrevAddress" element
     */
    public com.idanalytics.products.idscore.request.Address addNewPrevAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Address target = null;
            target = (com.idanalytics.products.idscore.request.Address)get_store().add_element_user(PREVADDRESS$36);
            return target;
        }
    }
    
    /**
     * Unsets the "PrevAddress" element
     */
    public void unsetPrevAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREVADDRESS$36, 0);
        }
    }
    
    /**
     * Gets the "ParsedPrevAddress" element
     */
    public com.idanalytics.products.idscore.request.ParsedAddress getParsedPrevAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ParsedAddress target = null;
            target = (com.idanalytics.products.idscore.request.ParsedAddress)get_store().find_element_user(PARSEDPREVADDRESS$38, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "ParsedPrevAddress" element
     */
    public boolean isSetParsedPrevAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PARSEDPREVADDRESS$38) != 0;
        }
    }
    
    /**
     * Sets the "ParsedPrevAddress" element
     */
    public void setParsedPrevAddress(com.idanalytics.products.idscore.request.ParsedAddress parsedPrevAddress)
    {
        generatedSetterHelperImpl(parsedPrevAddress, PARSEDPREVADDRESS$38, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "ParsedPrevAddress" element
     */
    public com.idanalytics.products.idscore.request.ParsedAddress addNewParsedPrevAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ParsedAddress target = null;
            target = (com.idanalytics.products.idscore.request.ParsedAddress)get_store().add_element_user(PARSEDPREVADDRESS$38);
            return target;
        }
    }
    
    /**
     * Unsets the "ParsedPrevAddress" element
     */
    public void unsetParsedPrevAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PARSEDPREVADDRESS$38, 0);
        }
    }
    
    /**
     * Gets the "PrevCity" element
     */
    public java.lang.String getPrevCity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVCITY$40, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrevCity" element
     */
    public com.idanalytics.products.idscore.request.City xgetPrevCity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.City target = null;
            target = (com.idanalytics.products.idscore.request.City)get_store().find_element_user(PREVCITY$40, 0);
            return target;
        }
    }
    
    /**
     * True if has "PrevCity" element
     */
    public boolean isSetPrevCity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREVCITY$40) != 0;
        }
    }
    
    /**
     * Sets the "PrevCity" element
     */
    public void setPrevCity(java.lang.String prevCity)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVCITY$40, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREVCITY$40);
            }
            target.setStringValue(prevCity);
        }
    }
    
    /**
     * Sets (as xml) the "PrevCity" element
     */
    public void xsetPrevCity(com.idanalytics.products.idscore.request.City prevCity)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.City target = null;
            target = (com.idanalytics.products.idscore.request.City)get_store().find_element_user(PREVCITY$40, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.City)get_store().add_element_user(PREVCITY$40);
            }
            target.set(prevCity);
        }
    }
    
    /**
     * Unsets the "PrevCity" element
     */
    public void unsetPrevCity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREVCITY$40, 0);
        }
    }
    
    /**
     * Gets the "PrevState" element
     */
    public java.lang.String getPrevState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVSTATE$42, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrevState" element
     */
    public com.idanalytics.products.idscore.request.State xgetPrevState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.State target = null;
            target = (com.idanalytics.products.idscore.request.State)get_store().find_element_user(PREVSTATE$42, 0);
            return target;
        }
    }
    
    /**
     * True if has "PrevState" element
     */
    public boolean isSetPrevState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREVSTATE$42) != 0;
        }
    }
    
    /**
     * Sets the "PrevState" element
     */
    public void setPrevState(java.lang.String prevState)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVSTATE$42, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREVSTATE$42);
            }
            target.setStringValue(prevState);
        }
    }
    
    /**
     * Sets (as xml) the "PrevState" element
     */
    public void xsetPrevState(com.idanalytics.products.idscore.request.State prevState)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.State target = null;
            target = (com.idanalytics.products.idscore.request.State)get_store().find_element_user(PREVSTATE$42, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.State)get_store().add_element_user(PREVSTATE$42);
            }
            target.set(prevState);
        }
    }
    
    /**
     * Unsets the "PrevState" element
     */
    public void unsetPrevState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREVSTATE$42, 0);
        }
    }
    
    /**
     * Gets the "PrevZip" element
     */
    public java.lang.String getPrevZip()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVZIP$44, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrevZip" element
     */
    public com.idanalytics.products.idscore.request.Zip xgetPrevZip()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Zip target = null;
            target = (com.idanalytics.products.idscore.request.Zip)get_store().find_element_user(PREVZIP$44, 0);
            return target;
        }
    }
    
    /**
     * True if has "PrevZip" element
     */
    public boolean isSetPrevZip()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREVZIP$44) != 0;
        }
    }
    
    /**
     * Sets the "PrevZip" element
     */
    public void setPrevZip(java.lang.String prevZip)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVZIP$44, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREVZIP$44);
            }
            target.setStringValue(prevZip);
        }
    }
    
    /**
     * Sets (as xml) the "PrevZip" element
     */
    public void xsetPrevZip(com.idanalytics.products.idscore.request.Zip prevZip)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Zip target = null;
            target = (com.idanalytics.products.idscore.request.Zip)get_store().find_element_user(PREVZIP$44, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Zip)get_store().add_element_user(PREVZIP$44);
            }
            target.set(prevZip);
        }
    }
    
    /**
     * Unsets the "PrevZip" element
     */
    public void unsetPrevZip()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREVZIP$44, 0);
        }
    }
    
    /**
     * Gets the "PrevCountry" element
     */
    public java.lang.String getPrevCountry()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVCOUNTRY$46, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrevCountry" element
     */
    public com.idanalytics.products.idscore.request.Country xgetPrevCountry()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Country target = null;
            target = (com.idanalytics.products.idscore.request.Country)get_store().find_element_user(PREVCOUNTRY$46, 0);
            return target;
        }
    }
    
    /**
     * True if has "PrevCountry" element
     */
    public boolean isSetPrevCountry()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREVCOUNTRY$46) != 0;
        }
    }
    
    /**
     * Sets the "PrevCountry" element
     */
    public void setPrevCountry(java.lang.String prevCountry)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVCOUNTRY$46, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREVCOUNTRY$46);
            }
            target.setStringValue(prevCountry);
        }
    }
    
    /**
     * Sets (as xml) the "PrevCountry" element
     */
    public void xsetPrevCountry(com.idanalytics.products.idscore.request.Country prevCountry)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Country target = null;
            target = (com.idanalytics.products.idscore.request.Country)get_store().find_element_user(PREVCOUNTRY$46, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Country)get_store().add_element_user(PREVCOUNTRY$46);
            }
            target.set(prevCountry);
        }
    }
    
    /**
     * Unsets the "PrevCountry" element
     */
    public void unsetPrevCountry()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREVCOUNTRY$46, 0);
        }
    }
    
    /**
     * Gets the "PrevPhone" element
     */
    public java.lang.String getPrevPhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVPHONE$48, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrevPhone" element
     */
    public com.idanalytics.products.idscore.request.Phone xgetPrevPhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().find_element_user(PREVPHONE$48, 0);
            return target;
        }
    }
    
    /**
     * True if has "PrevPhone" element
     */
    public boolean isSetPrevPhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREVPHONE$48) != 0;
        }
    }
    
    /**
     * Sets the "PrevPhone" element
     */
    public void setPrevPhone(java.lang.String prevPhone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVPHONE$48, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREVPHONE$48);
            }
            target.setStringValue(prevPhone);
        }
    }
    
    /**
     * Sets (as xml) the "PrevPhone" element
     */
    public void xsetPrevPhone(com.idanalytics.products.idscore.request.Phone prevPhone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().find_element_user(PREVPHONE$48, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Phone)get_store().add_element_user(PREVPHONE$48);
            }
            target.set(prevPhone);
        }
    }
    
    /**
     * Unsets the "PrevPhone" element
     */
    public void unsetPrevPhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREVPHONE$48, 0);
        }
    }
    
    /**
     * Gets the "PrevTimeAtEmployer" element
     */
    public java.lang.String getPrevTimeAtEmployer()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVTIMEATEMPLOYER$50, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrevTimeAtEmployer" element
     */
    public com.idanalytics.products.idscore.request.TimeAt xgetPrevTimeAtEmployer()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.TimeAt target = null;
            target = (com.idanalytics.products.idscore.request.TimeAt)get_store().find_element_user(PREVTIMEATEMPLOYER$50, 0);
            return target;
        }
    }
    
    /**
     * True if has "PrevTimeAtEmployer" element
     */
    public boolean isSetPrevTimeAtEmployer()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREVTIMEATEMPLOYER$50) != 0;
        }
    }
    
    /**
     * Sets the "PrevTimeAtEmployer" element
     */
    public void setPrevTimeAtEmployer(java.lang.String prevTimeAtEmployer)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVTIMEATEMPLOYER$50, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREVTIMEATEMPLOYER$50);
            }
            target.setStringValue(prevTimeAtEmployer);
        }
    }
    
    /**
     * Sets (as xml) the "PrevTimeAtEmployer" element
     */
    public void xsetPrevTimeAtEmployer(com.idanalytics.products.idscore.request.TimeAt prevTimeAtEmployer)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.TimeAt target = null;
            target = (com.idanalytics.products.idscore.request.TimeAt)get_store().find_element_user(PREVTIMEATEMPLOYER$50, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.TimeAt)get_store().add_element_user(PREVTIMEATEMPLOYER$50);
            }
            target.set(prevTimeAtEmployer);
        }
    }
    
    /**
     * Unsets the "PrevTimeAtEmployer" element
     */
    public void unsetPrevTimeAtEmployer()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREVTIMEATEMPLOYER$50, 0);
        }
    }
    
    /**
     * Gets the "PrevStartDate" element
     */
    public java.lang.Object getPrevStartDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVSTARTDATE$52, 0);
            if (target == null)
            {
                return null;
            }
            return target.getObjectValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrevStartDate" element
     */
    public com.idanalytics.products.idscore.request.Date xgetPrevStartDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Date target = null;
            target = (com.idanalytics.products.idscore.request.Date)get_store().find_element_user(PREVSTARTDATE$52, 0);
            return target;
        }
    }
    
    /**
     * True if has "PrevStartDate" element
     */
    public boolean isSetPrevStartDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREVSTARTDATE$52) != 0;
        }
    }
    
    /**
     * Sets the "PrevStartDate" element
     */
    public void setPrevStartDate(java.lang.Object prevStartDate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVSTARTDATE$52, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREVSTARTDATE$52);
            }
            target.setObjectValue(prevStartDate);
        }
    }
    
    /**
     * Sets (as xml) the "PrevStartDate" element
     */
    public void xsetPrevStartDate(com.idanalytics.products.idscore.request.Date prevStartDate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Date target = null;
            target = (com.idanalytics.products.idscore.request.Date)get_store().find_element_user(PREVSTARTDATE$52, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Date)get_store().add_element_user(PREVSTARTDATE$52);
            }
            target.set(prevStartDate);
        }
    }
    
    /**
     * Unsets the "PrevStartDate" element
     */
    public void unsetPrevStartDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREVSTARTDATE$52, 0);
        }
    }
    
    /**
     * Gets the "PrevTitle" element
     */
    public java.lang.String getPrevTitle()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVTITLE$54, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrevTitle" element
     */
    public com.idanalytics.products.idscore.request.EmploymentTitle xgetPrevTitle()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.EmploymentTitle target = null;
            target = (com.idanalytics.products.idscore.request.EmploymentTitle)get_store().find_element_user(PREVTITLE$54, 0);
            return target;
        }
    }
    
    /**
     * True if has "PrevTitle" element
     */
    public boolean isSetPrevTitle()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREVTITLE$54) != 0;
        }
    }
    
    /**
     * Sets the "PrevTitle" element
     */
    public void setPrevTitle(java.lang.String prevTitle)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVTITLE$54, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREVTITLE$54);
            }
            target.setStringValue(prevTitle);
        }
    }
    
    /**
     * Sets (as xml) the "PrevTitle" element
     */
    public void xsetPrevTitle(com.idanalytics.products.idscore.request.EmploymentTitle prevTitle)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.EmploymentTitle target = null;
            target = (com.idanalytics.products.idscore.request.EmploymentTitle)get_store().find_element_user(PREVTITLE$54, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.EmploymentTitle)get_store().add_element_user(PREVTITLE$54);
            }
            target.set(prevTitle);
        }
    }
    
    /**
     * Unsets the "PrevTitle" element
     */
    public void unsetPrevTitle()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREVTITLE$54, 0);
        }
    }
    
    /**
     * Gets the "PrevSalary" element
     */
    public java.lang.Object getPrevSalary()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVSALARY$56, 0);
            if (target == null)
            {
                return null;
            }
            return target.getObjectValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrevSalary" element
     */
    public com.idanalytics.products.idscore.request.Income xgetPrevSalary()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Income target = null;
            target = (com.idanalytics.products.idscore.request.Income)get_store().find_element_user(PREVSALARY$56, 0);
            return target;
        }
    }
    
    /**
     * True if has "PrevSalary" element
     */
    public boolean isSetPrevSalary()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREVSALARY$56) != 0;
        }
    }
    
    /**
     * Sets the "PrevSalary" element
     */
    public void setPrevSalary(java.lang.Object prevSalary)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVSALARY$56, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREVSALARY$56);
            }
            target.setObjectValue(prevSalary);
        }
    }
    
    /**
     * Sets (as xml) the "PrevSalary" element
     */
    public void xsetPrevSalary(com.idanalytics.products.idscore.request.Income prevSalary)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Income target = null;
            target = (com.idanalytics.products.idscore.request.Income)get_store().find_element_user(PREVSALARY$56, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Income)get_store().add_element_user(PREVSALARY$56);
            }
            target.set(prevSalary);
        }
    }
    
    /**
     * Unsets the "PrevSalary" element
     */
    public void unsetPrevSalary()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREVSALARY$56, 0);
        }
    }
    
    /**
     * Gets the "PrevSalaryPeriodicity" element
     */
    public com.idanalytics.products.idscore.request.Periodicity.Enum getPrevSalaryPeriodicity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVSALARYPERIODICITY$58, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.idscore.request.Periodicity.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "PrevSalaryPeriodicity" element
     */
    public com.idanalytics.products.idscore.request.Periodicity xgetPrevSalaryPeriodicity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Periodicity target = null;
            target = (com.idanalytics.products.idscore.request.Periodicity)get_store().find_element_user(PREVSALARYPERIODICITY$58, 0);
            return target;
        }
    }
    
    /**
     * True if has "PrevSalaryPeriodicity" element
     */
    public boolean isSetPrevSalaryPeriodicity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREVSALARYPERIODICITY$58) != 0;
        }
    }
    
    /**
     * Sets the "PrevSalaryPeriodicity" element
     */
    public void setPrevSalaryPeriodicity(com.idanalytics.products.idscore.request.Periodicity.Enum prevSalaryPeriodicity)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVSALARYPERIODICITY$58, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREVSALARYPERIODICITY$58);
            }
            target.setEnumValue(prevSalaryPeriodicity);
        }
    }
    
    /**
     * Sets (as xml) the "PrevSalaryPeriodicity" element
     */
    public void xsetPrevSalaryPeriodicity(com.idanalytics.products.idscore.request.Periodicity prevSalaryPeriodicity)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Periodicity target = null;
            target = (com.idanalytics.products.idscore.request.Periodicity)get_store().find_element_user(PREVSALARYPERIODICITY$58, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Periodicity)get_store().add_element_user(PREVSALARYPERIODICITY$58);
            }
            target.set(prevSalaryPeriodicity);
        }
    }
    
    /**
     * Unsets the "PrevSalaryPeriodicity" element
     */
    public void unsetPrevSalaryPeriodicity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREVSALARYPERIODICITY$58, 0);
        }
    }
}
