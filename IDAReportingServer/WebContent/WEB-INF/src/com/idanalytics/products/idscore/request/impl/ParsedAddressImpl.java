/*
 * XML Type:  ParsedAddress
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.ParsedAddress
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML ParsedAddress(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public class ParsedAddressImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.ParsedAddress
{
    private static final long serialVersionUID = 1L;
    
    public ParsedAddressImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName STREETDIR$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "StreetDir");
    private static final javax.xml.namespace.QName STREET$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Street");
    private static final javax.xml.namespace.QName STREETNUMBER$4 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "StreetNumber");
    private static final javax.xml.namespace.QName STREETTYPE$6 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "StreetType");
    private static final javax.xml.namespace.QName APTNUMBER$8 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "AptNumber");
    private static final javax.xml.namespace.QName POBOX$10 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "POBox");
    private static final javax.xml.namespace.QName RURALROUTE$12 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "RuralRoute");
    
    
    /**
     * Gets the "StreetDir" element
     */
    public java.lang.String getStreetDir()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STREETDIR$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "StreetDir" element
     */
    public com.idanalytics.products.idscore.request.Collapsed xgetStreetDir()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Collapsed target = null;
            target = (com.idanalytics.products.idscore.request.Collapsed)get_store().find_element_user(STREETDIR$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "StreetDir" element
     */
    public void setStreetDir(java.lang.String streetDir)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STREETDIR$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(STREETDIR$0);
            }
            target.setStringValue(streetDir);
        }
    }
    
    /**
     * Sets (as xml) the "StreetDir" element
     */
    public void xsetStreetDir(com.idanalytics.products.idscore.request.Collapsed streetDir)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Collapsed target = null;
            target = (com.idanalytics.products.idscore.request.Collapsed)get_store().find_element_user(STREETDIR$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Collapsed)get_store().add_element_user(STREETDIR$0);
            }
            target.set(streetDir);
        }
    }
    
    /**
     * Gets the "Street" element
     */
    public java.lang.String getStreet()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STREET$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Street" element
     */
    public com.idanalytics.products.idscore.request.Collapsed xgetStreet()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Collapsed target = null;
            target = (com.idanalytics.products.idscore.request.Collapsed)get_store().find_element_user(STREET$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Street" element
     */
    public void setStreet(java.lang.String street)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STREET$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(STREET$2);
            }
            target.setStringValue(street);
        }
    }
    
    /**
     * Sets (as xml) the "Street" element
     */
    public void xsetStreet(com.idanalytics.products.idscore.request.Collapsed street)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Collapsed target = null;
            target = (com.idanalytics.products.idscore.request.Collapsed)get_store().find_element_user(STREET$2, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Collapsed)get_store().add_element_user(STREET$2);
            }
            target.set(street);
        }
    }
    
    /**
     * Gets the "StreetNumber" element
     */
    public java.lang.String getStreetNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STREETNUMBER$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "StreetNumber" element
     */
    public com.idanalytics.products.idscore.request.Collapsed xgetStreetNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Collapsed target = null;
            target = (com.idanalytics.products.idscore.request.Collapsed)get_store().find_element_user(STREETNUMBER$4, 0);
            return target;
        }
    }
    
    /**
     * Sets the "StreetNumber" element
     */
    public void setStreetNumber(java.lang.String streetNumber)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STREETNUMBER$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(STREETNUMBER$4);
            }
            target.setStringValue(streetNumber);
        }
    }
    
    /**
     * Sets (as xml) the "StreetNumber" element
     */
    public void xsetStreetNumber(com.idanalytics.products.idscore.request.Collapsed streetNumber)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Collapsed target = null;
            target = (com.idanalytics.products.idscore.request.Collapsed)get_store().find_element_user(STREETNUMBER$4, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Collapsed)get_store().add_element_user(STREETNUMBER$4);
            }
            target.set(streetNumber);
        }
    }
    
    /**
     * Gets the "StreetType" element
     */
    public java.lang.String getStreetType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STREETTYPE$6, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "StreetType" element
     */
    public com.idanalytics.products.idscore.request.Collapsed xgetStreetType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Collapsed target = null;
            target = (com.idanalytics.products.idscore.request.Collapsed)get_store().find_element_user(STREETTYPE$6, 0);
            return target;
        }
    }
    
    /**
     * Sets the "StreetType" element
     */
    public void setStreetType(java.lang.String streetType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STREETTYPE$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(STREETTYPE$6);
            }
            target.setStringValue(streetType);
        }
    }
    
    /**
     * Sets (as xml) the "StreetType" element
     */
    public void xsetStreetType(com.idanalytics.products.idscore.request.Collapsed streetType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Collapsed target = null;
            target = (com.idanalytics.products.idscore.request.Collapsed)get_store().find_element_user(STREETTYPE$6, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Collapsed)get_store().add_element_user(STREETTYPE$6);
            }
            target.set(streetType);
        }
    }
    
    /**
     * Gets the "AptNumber" element
     */
    public java.lang.String getAptNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(APTNUMBER$8, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "AptNumber" element
     */
    public com.idanalytics.products.idscore.request.Collapsed xgetAptNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Collapsed target = null;
            target = (com.idanalytics.products.idscore.request.Collapsed)get_store().find_element_user(APTNUMBER$8, 0);
            return target;
        }
    }
    
    /**
     * True if has "AptNumber" element
     */
    public boolean isSetAptNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(APTNUMBER$8) != 0;
        }
    }
    
    /**
     * Sets the "AptNumber" element
     */
    public void setAptNumber(java.lang.String aptNumber)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(APTNUMBER$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(APTNUMBER$8);
            }
            target.setStringValue(aptNumber);
        }
    }
    
    /**
     * Sets (as xml) the "AptNumber" element
     */
    public void xsetAptNumber(com.idanalytics.products.idscore.request.Collapsed aptNumber)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Collapsed target = null;
            target = (com.idanalytics.products.idscore.request.Collapsed)get_store().find_element_user(APTNUMBER$8, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Collapsed)get_store().add_element_user(APTNUMBER$8);
            }
            target.set(aptNumber);
        }
    }
    
    /**
     * Unsets the "AptNumber" element
     */
    public void unsetAptNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(APTNUMBER$8, 0);
        }
    }
    
    /**
     * Gets the "POBox" element
     */
    public java.lang.String getPOBox()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(POBOX$10, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "POBox" element
     */
    public com.idanalytics.products.idscore.request.Collapsed xgetPOBox()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Collapsed target = null;
            target = (com.idanalytics.products.idscore.request.Collapsed)get_store().find_element_user(POBOX$10, 0);
            return target;
        }
    }
    
    /**
     * True if has "POBox" element
     */
    public boolean isSetPOBox()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(POBOX$10) != 0;
        }
    }
    
    /**
     * Sets the "POBox" element
     */
    public void setPOBox(java.lang.String poBox)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(POBOX$10, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(POBOX$10);
            }
            target.setStringValue(poBox);
        }
    }
    
    /**
     * Sets (as xml) the "POBox" element
     */
    public void xsetPOBox(com.idanalytics.products.idscore.request.Collapsed poBox)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Collapsed target = null;
            target = (com.idanalytics.products.idscore.request.Collapsed)get_store().find_element_user(POBOX$10, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Collapsed)get_store().add_element_user(POBOX$10);
            }
            target.set(poBox);
        }
    }
    
    /**
     * Unsets the "POBox" element
     */
    public void unsetPOBox()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(POBOX$10, 0);
        }
    }
    
    /**
     * Gets the "RuralRoute" element
     */
    public java.lang.String getRuralRoute()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RURALROUTE$12, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "RuralRoute" element
     */
    public com.idanalytics.products.idscore.request.Collapsed xgetRuralRoute()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Collapsed target = null;
            target = (com.idanalytics.products.idscore.request.Collapsed)get_store().find_element_user(RURALROUTE$12, 0);
            return target;
        }
    }
    
    /**
     * True if has "RuralRoute" element
     */
    public boolean isSetRuralRoute()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(RURALROUTE$12) != 0;
        }
    }
    
    /**
     * Sets the "RuralRoute" element
     */
    public void setRuralRoute(java.lang.String ruralRoute)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RURALROUTE$12, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(RURALROUTE$12);
            }
            target.setStringValue(ruralRoute);
        }
    }
    
    /**
     * Sets (as xml) the "RuralRoute" element
     */
    public void xsetRuralRoute(com.idanalytics.products.idscore.request.Collapsed ruralRoute)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Collapsed target = null;
            target = (com.idanalytics.products.idscore.request.Collapsed)get_store().find_element_user(RURALROUTE$12, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Collapsed)get_store().add_element_user(RURALROUTE$12);
            }
            target.set(ruralRoute);
        }
    }
    
    /**
     * Unsets the "RuralRoute" element
     */
    public void unsetRuralRoute()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(RURALROUTE$12, 0);
        }
    }
}
