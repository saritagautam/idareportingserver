/*
 * An XML document type.
 * Localname: ScoreBandIndicator
 * Namespace: http://idanalytics.com/products/idscore/result.v31
 * Java type: com.idanalytics.products.idscore.result_v31.ScoreBandIndicatorDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.result_v31.impl;
/**
 * A document containing one ScoreBandIndicator(@http://idanalytics.com/products/idscore/result.v31) element.
 *
 * This is a complex type.
 */
public class ScoreBandIndicatorDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.result_v31.ScoreBandIndicatorDocument
{
    private static final long serialVersionUID = 1L;
    
    public ScoreBandIndicatorDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SCOREBANDINDICATOR$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result.v31", "ScoreBandIndicator");
    
    
    /**
     * Gets the "ScoreBandIndicator" element
     */
    public com.idanalytics.products.idscore.result_v31.ScoreBandIndicatorDocument.ScoreBandIndicator.Enum getScoreBandIndicator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SCOREBANDINDICATOR$0, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.idscore.result_v31.ScoreBandIndicatorDocument.ScoreBandIndicator.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "ScoreBandIndicator" element
     */
    public com.idanalytics.products.idscore.result_v31.ScoreBandIndicatorDocument.ScoreBandIndicator xgetScoreBandIndicator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result_v31.ScoreBandIndicatorDocument.ScoreBandIndicator target = null;
            target = (com.idanalytics.products.idscore.result_v31.ScoreBandIndicatorDocument.ScoreBandIndicator)get_store().find_element_user(SCOREBANDINDICATOR$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "ScoreBandIndicator" element
     */
    public void setScoreBandIndicator(com.idanalytics.products.idscore.result_v31.ScoreBandIndicatorDocument.ScoreBandIndicator.Enum scoreBandIndicator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SCOREBANDINDICATOR$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SCOREBANDINDICATOR$0);
            }
            target.setEnumValue(scoreBandIndicator);
        }
    }
    
    /**
     * Sets (as xml) the "ScoreBandIndicator" element
     */
    public void xsetScoreBandIndicator(com.idanalytics.products.idscore.result_v31.ScoreBandIndicatorDocument.ScoreBandIndicator scoreBandIndicator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result_v31.ScoreBandIndicatorDocument.ScoreBandIndicator target = null;
            target = (com.idanalytics.products.idscore.result_v31.ScoreBandIndicatorDocument.ScoreBandIndicator)get_store().find_element_user(SCOREBANDINDICATOR$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.result_v31.ScoreBandIndicatorDocument.ScoreBandIndicator)get_store().add_element_user(SCOREBANDINDICATOR$0);
            }
            target.set(scoreBandIndicator);
        }
    }
    /**
     * An XML ScoreBandIndicator(@http://idanalytics.com/products/idscore/result.v31).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result_v31.ScoreBandIndicatorDocument$ScoreBandIndicator.
     */
    public static class ScoreBandIndicatorImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.idanalytics.products.idscore.result_v31.ScoreBandIndicatorDocument.ScoreBandIndicator
    {
        private static final long serialVersionUID = 1L;
        
        public ScoreBandIndicatorImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected ScoreBandIndicatorImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
