/*
 * An XML document type.
 * Localname: ConsumerHeader
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.ConsumerHeaderDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * A document containing one ConsumerHeader(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public class ConsumerHeaderDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.ConsumerHeaderDocument
{
    private static final long serialVersionUID = 1L;
    
    public ConsumerHeaderDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CONSUMERHEADER$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ConsumerHeader");
    
    
    /**
     * Gets the "ConsumerHeader" element
     */
    public com.idanalytics.products.idscore.request.ConsumerHeaderDocument.ConsumerHeader getConsumerHeader()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ConsumerHeaderDocument.ConsumerHeader target = null;
            target = (com.idanalytics.products.idscore.request.ConsumerHeaderDocument.ConsumerHeader)get_store().find_element_user(CONSUMERHEADER$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "ConsumerHeader" element
     */
    public void setConsumerHeader(com.idanalytics.products.idscore.request.ConsumerHeaderDocument.ConsumerHeader consumerHeader)
    {
        generatedSetterHelperImpl(consumerHeader, CONSUMERHEADER$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "ConsumerHeader" element
     */
    public com.idanalytics.products.idscore.request.ConsumerHeaderDocument.ConsumerHeader addNewConsumerHeader()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ConsumerHeaderDocument.ConsumerHeader target = null;
            target = (com.idanalytics.products.idscore.request.ConsumerHeaderDocument.ConsumerHeader)get_store().add_element_user(CONSUMERHEADER$0);
            return target;
        }
    }
    /**
     * An XML ConsumerHeader(@http://idanalytics.com/products/idscore/request).
     *
     * This is a complex type.
     */
    public static class ConsumerHeaderImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.ConsumerHeaderDocument.ConsumerHeader
    {
        private static final long serialVersionUID = 1L;
        
        public ConsumerHeaderImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName CPC$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "CPC");
        private static final javax.xml.namespace.QName REQUESTTYPE$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "RequestType");
        private static final javax.xml.namespace.QName EFFECTIVEDATE$4 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "EffectiveDate");
        private static final javax.xml.namespace.QName CONSUMERID$6 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ConsumerID");
        private static final javax.xml.namespace.QName IPADDRESS$8 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "IPAddress");
        private static final javax.xml.namespace.QName CREDITALERTFLAG$10 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "CreditAlertFlag");
        private static final javax.xml.namespace.QName CREDITINACTIVEFLAG$12 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "CreditInactiveFlag");
        private static final javax.xml.namespace.QName PASSTHRU1$14 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PassThru1");
        private static final javax.xml.namespace.QName PASSTHRU2$16 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PassThru2");
        private static final javax.xml.namespace.QName PASSTHRU3$18 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PassThru3");
        private static final javax.xml.namespace.QName PASSTHRU4$20 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PassThru4");
        
        
        /**
         * Gets the "CPC" element
         */
        public java.lang.String getCPC()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CPC$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "CPC" element
         */
        public com.idanalytics.products.idscore.request.CPCDocument.CPC xgetCPC()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.CPCDocument.CPC target = null;
                target = (com.idanalytics.products.idscore.request.CPCDocument.CPC)get_store().find_element_user(CPC$0, 0);
                return target;
            }
        }
        
        /**
         * True if has "CPC" element
         */
        public boolean isSetCPC()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(CPC$0) != 0;
            }
        }
        
        /**
         * Sets the "CPC" element
         */
        public void setCPC(java.lang.String cpc)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CPC$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CPC$0);
                }
                target.setStringValue(cpc);
            }
        }
        
        /**
         * Sets (as xml) the "CPC" element
         */
        public void xsetCPC(com.idanalytics.products.idscore.request.CPCDocument.CPC cpc)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.CPCDocument.CPC target = null;
                target = (com.idanalytics.products.idscore.request.CPCDocument.CPC)get_store().find_element_user(CPC$0, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.request.CPCDocument.CPC)get_store().add_element_user(CPC$0);
                }
                target.set(cpc);
            }
        }
        
        /**
         * Unsets the "CPC" element
         */
        public void unsetCPC()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(CPC$0, 0);
            }
        }
        
        /**
         * Gets the "RequestType" element
         */
        public java.lang.Object getRequestType()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(REQUESTTYPE$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getObjectValue();
            }
        }
        
        /**
         * Gets (as xml) the "RequestType" element
         */
        public com.idanalytics.products.idscore.request.RequestTypeDocument.RequestType xgetRequestType()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.RequestTypeDocument.RequestType target = null;
                target = (com.idanalytics.products.idscore.request.RequestTypeDocument.RequestType)get_store().find_element_user(REQUESTTYPE$2, 0);
                return target;
            }
        }
        
        /**
         * Sets the "RequestType" element
         */
        public void setRequestType(java.lang.Object requestType)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(REQUESTTYPE$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(REQUESTTYPE$2);
                }
                target.setObjectValue(requestType);
            }
        }
        
        /**
         * Sets (as xml) the "RequestType" element
         */
        public void xsetRequestType(com.idanalytics.products.idscore.request.RequestTypeDocument.RequestType requestType)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.RequestTypeDocument.RequestType target = null;
                target = (com.idanalytics.products.idscore.request.RequestTypeDocument.RequestType)get_store().find_element_user(REQUESTTYPE$2, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.request.RequestTypeDocument.RequestType)get_store().add_element_user(REQUESTTYPE$2);
                }
                target.set(requestType);
            }
        }
        
        /**
         * Gets the "EffectiveDate" element
         */
        public java.util.Calendar getEffectiveDate()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EFFECTIVEDATE$4, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getCalendarValue();
            }
        }
        
        /**
         * Gets (as xml) the "EffectiveDate" element
         */
        public org.apache.xmlbeans.XmlDateTime xgetEffectiveDate()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlDateTime target = null;
                target = (org.apache.xmlbeans.XmlDateTime)get_store().find_element_user(EFFECTIVEDATE$4, 0);
                return target;
            }
        }
        
        /**
         * True if has "EffectiveDate" element
         */
        public boolean isSetEffectiveDate()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(EFFECTIVEDATE$4) != 0;
            }
        }
        
        /**
         * Sets the "EffectiveDate" element
         */
        public void setEffectiveDate(java.util.Calendar effectiveDate)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EFFECTIVEDATE$4, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(EFFECTIVEDATE$4);
                }
                target.setCalendarValue(effectiveDate);
            }
        }
        
        /**
         * Sets (as xml) the "EffectiveDate" element
         */
        public void xsetEffectiveDate(org.apache.xmlbeans.XmlDateTime effectiveDate)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlDateTime target = null;
                target = (org.apache.xmlbeans.XmlDateTime)get_store().find_element_user(EFFECTIVEDATE$4, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlDateTime)get_store().add_element_user(EFFECTIVEDATE$4);
                }
                target.set(effectiveDate);
            }
        }
        
        /**
         * Unsets the "EffectiveDate" element
         */
        public void unsetEffectiveDate()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(EFFECTIVEDATE$4, 0);
            }
        }
        
        /**
         * Gets the "ConsumerID" element
         */
        public java.lang.String getConsumerID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CONSUMERID$6, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "ConsumerID" element
         */
        public com.idanalytics.products.idscore.request.ConsumerIDDocument.ConsumerID xgetConsumerID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.ConsumerIDDocument.ConsumerID target = null;
                target = (com.idanalytics.products.idscore.request.ConsumerIDDocument.ConsumerID)get_store().find_element_user(CONSUMERID$6, 0);
                return target;
            }
        }
        
        /**
         * Sets the "ConsumerID" element
         */
        public void setConsumerID(java.lang.String consumerID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CONSUMERID$6, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CONSUMERID$6);
                }
                target.setStringValue(consumerID);
            }
        }
        
        /**
         * Sets (as xml) the "ConsumerID" element
         */
        public void xsetConsumerID(com.idanalytics.products.idscore.request.ConsumerIDDocument.ConsumerID consumerID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.ConsumerIDDocument.ConsumerID target = null;
                target = (com.idanalytics.products.idscore.request.ConsumerIDDocument.ConsumerID)get_store().find_element_user(CONSUMERID$6, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.request.ConsumerIDDocument.ConsumerID)get_store().add_element_user(CONSUMERID$6);
                }
                target.set(consumerID);
            }
        }
        
        /**
         * Gets the "IPAddress" element
         */
        public java.lang.String getIPAddress()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IPADDRESS$8, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "IPAddress" element
         */
        public com.idanalytics.products.idscore.request.IPAddressDocument.IPAddress xgetIPAddress()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.IPAddressDocument.IPAddress target = null;
                target = (com.idanalytics.products.idscore.request.IPAddressDocument.IPAddress)get_store().find_element_user(IPADDRESS$8, 0);
                return target;
            }
        }
        
        /**
         * True if has "IPAddress" element
         */
        public boolean isSetIPAddress()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(IPADDRESS$8) != 0;
            }
        }
        
        /**
         * Sets the "IPAddress" element
         */
        public void setIPAddress(java.lang.String ipAddress)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IPADDRESS$8, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IPADDRESS$8);
                }
                target.setStringValue(ipAddress);
            }
        }
        
        /**
         * Sets (as xml) the "IPAddress" element
         */
        public void xsetIPAddress(com.idanalytics.products.idscore.request.IPAddressDocument.IPAddress ipAddress)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.IPAddressDocument.IPAddress target = null;
                target = (com.idanalytics.products.idscore.request.IPAddressDocument.IPAddress)get_store().find_element_user(IPADDRESS$8, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.request.IPAddressDocument.IPAddress)get_store().add_element_user(IPADDRESS$8);
                }
                target.set(ipAddress);
            }
        }
        
        /**
         * Unsets the "IPAddress" element
         */
        public void unsetIPAddress()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(IPADDRESS$8, 0);
            }
        }
        
        /**
         * Gets the "CreditAlertFlag" element
         */
        public com.idanalytics.products.idscore.request.Bool.Enum getCreditAlertFlag()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CREDITALERTFLAG$10, 0);
                if (target == null)
                {
                    return null;
                }
                return (com.idanalytics.products.idscore.request.Bool.Enum)target.getEnumValue();
            }
        }
        
        /**
         * Gets (as xml) the "CreditAlertFlag" element
         */
        public com.idanalytics.products.idscore.request.Bool xgetCreditAlertFlag()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.Bool target = null;
                target = (com.idanalytics.products.idscore.request.Bool)get_store().find_element_user(CREDITALERTFLAG$10, 0);
                return target;
            }
        }
        
        /**
         * True if has "CreditAlertFlag" element
         */
        public boolean isSetCreditAlertFlag()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(CREDITALERTFLAG$10) != 0;
            }
        }
        
        /**
         * Sets the "CreditAlertFlag" element
         */
        public void setCreditAlertFlag(com.idanalytics.products.idscore.request.Bool.Enum creditAlertFlag)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CREDITALERTFLAG$10, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CREDITALERTFLAG$10);
                }
                target.setEnumValue(creditAlertFlag);
            }
        }
        
        /**
         * Sets (as xml) the "CreditAlertFlag" element
         */
        public void xsetCreditAlertFlag(com.idanalytics.products.idscore.request.Bool creditAlertFlag)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.Bool target = null;
                target = (com.idanalytics.products.idscore.request.Bool)get_store().find_element_user(CREDITALERTFLAG$10, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.request.Bool)get_store().add_element_user(CREDITALERTFLAG$10);
                }
                target.set(creditAlertFlag);
            }
        }
        
        /**
         * Unsets the "CreditAlertFlag" element
         */
        public void unsetCreditAlertFlag()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(CREDITALERTFLAG$10, 0);
            }
        }
        
        /**
         * Gets the "CreditInactiveFlag" element
         */
        public com.idanalytics.products.idscore.request.Bool.Enum getCreditInactiveFlag()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CREDITINACTIVEFLAG$12, 0);
                if (target == null)
                {
                    return null;
                }
                return (com.idanalytics.products.idscore.request.Bool.Enum)target.getEnumValue();
            }
        }
        
        /**
         * Gets (as xml) the "CreditInactiveFlag" element
         */
        public com.idanalytics.products.idscore.request.Bool xgetCreditInactiveFlag()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.Bool target = null;
                target = (com.idanalytics.products.idscore.request.Bool)get_store().find_element_user(CREDITINACTIVEFLAG$12, 0);
                return target;
            }
        }
        
        /**
         * True if has "CreditInactiveFlag" element
         */
        public boolean isSetCreditInactiveFlag()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(CREDITINACTIVEFLAG$12) != 0;
            }
        }
        
        /**
         * Sets the "CreditInactiveFlag" element
         */
        public void setCreditInactiveFlag(com.idanalytics.products.idscore.request.Bool.Enum creditInactiveFlag)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CREDITINACTIVEFLAG$12, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CREDITINACTIVEFLAG$12);
                }
                target.setEnumValue(creditInactiveFlag);
            }
        }
        
        /**
         * Sets (as xml) the "CreditInactiveFlag" element
         */
        public void xsetCreditInactiveFlag(com.idanalytics.products.idscore.request.Bool creditInactiveFlag)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.Bool target = null;
                target = (com.idanalytics.products.idscore.request.Bool)get_store().find_element_user(CREDITINACTIVEFLAG$12, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.request.Bool)get_store().add_element_user(CREDITINACTIVEFLAG$12);
                }
                target.set(creditInactiveFlag);
            }
        }
        
        /**
         * Unsets the "CreditInactiveFlag" element
         */
        public void unsetCreditInactiveFlag()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(CREDITINACTIVEFLAG$12, 0);
            }
        }
        
        /**
         * Gets the "PassThru1" element
         */
        public java.lang.String getPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU1$14, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru1" element
         */
        public com.idanalytics.products.common_v1.PassThru xgetPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU1$14, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru1" element
         */
        public boolean isSetPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU1$14) != 0;
            }
        }
        
        /**
         * Sets the "PassThru1" element
         */
        public void setPassThru1(java.lang.String passThru1)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU1$14, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU1$14);
                }
                target.setStringValue(passThru1);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru1" element
         */
        public void xsetPassThru1(com.idanalytics.products.common_v1.PassThru passThru1)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU1$14, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.PassThru)get_store().add_element_user(PASSTHRU1$14);
                }
                target.set(passThru1);
            }
        }
        
        /**
         * Unsets the "PassThru1" element
         */
        public void unsetPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU1$14, 0);
            }
        }
        
        /**
         * Gets the "PassThru2" element
         */
        public java.lang.String getPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU2$16, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru2" element
         */
        public com.idanalytics.products.common_v1.PassThru xgetPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU2$16, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru2" element
         */
        public boolean isSetPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU2$16) != 0;
            }
        }
        
        /**
         * Sets the "PassThru2" element
         */
        public void setPassThru2(java.lang.String passThru2)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU2$16, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU2$16);
                }
                target.setStringValue(passThru2);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru2" element
         */
        public void xsetPassThru2(com.idanalytics.products.common_v1.PassThru passThru2)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU2$16, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.PassThru)get_store().add_element_user(PASSTHRU2$16);
                }
                target.set(passThru2);
            }
        }
        
        /**
         * Unsets the "PassThru2" element
         */
        public void unsetPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU2$16, 0);
            }
        }
        
        /**
         * Gets the "PassThru3" element
         */
        public java.lang.String getPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU3$18, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru3" element
         */
        public com.idanalytics.products.common_v1.PassThru xgetPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU3$18, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru3" element
         */
        public boolean isSetPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU3$18) != 0;
            }
        }
        
        /**
         * Sets the "PassThru3" element
         */
        public void setPassThru3(java.lang.String passThru3)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU3$18, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU3$18);
                }
                target.setStringValue(passThru3);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru3" element
         */
        public void xsetPassThru3(com.idanalytics.products.common_v1.PassThru passThru3)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU3$18, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.PassThru)get_store().add_element_user(PASSTHRU3$18);
                }
                target.set(passThru3);
            }
        }
        
        /**
         * Unsets the "PassThru3" element
         */
        public void unsetPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU3$18, 0);
            }
        }
        
        /**
         * Gets the "PassThru4" element
         */
        public java.lang.String getPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU4$20, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru4" element
         */
        public com.idanalytics.products.common_v1.PassThru xgetPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU4$20, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru4" element
         */
        public boolean isSetPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU4$20) != 0;
            }
        }
        
        /**
         * Sets the "PassThru4" element
         */
        public void setPassThru4(java.lang.String passThru4)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU4$20, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU4$20);
                }
                target.setStringValue(passThru4);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru4" element
         */
        public void xsetPassThru4(com.idanalytics.products.common_v1.PassThru passThru4)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU4$20, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.PassThru)get_store().add_element_user(PASSTHRU4$20);
                }
                target.set(passThru4);
            }
        }
        
        /**
         * Unsets the "PassThru4" element
         */
        public void unsetPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU4$20, 0);
            }
        }
    }
}
