/*
 * An XML document type.
 * Localname: PassThru3
 * Namespace: http://idanalytics.com/products/idscore/result
 * Java type: com.idanalytics.products.idscore.result.PassThru3Document
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.result.impl;
/**
 * A document containing one PassThru3(@http://idanalytics.com/products/idscore/result) element.
 *
 * This is a complex type.
 */
public class PassThru3DocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.result.PassThru3Document
{
    private static final long serialVersionUID = 1L;
    
    public PassThru3DocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName PASSTHRU3$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "PassThru3");
    
    
    /**
     * Gets the "PassThru3" element
     */
    public java.lang.String getPassThru3()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU3$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PassThru3" element
     */
    public com.idanalytics.products.idscore.result.PassThru3Document.PassThru3 xgetPassThru3()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.PassThru3Document.PassThru3 target = null;
            target = (com.idanalytics.products.idscore.result.PassThru3Document.PassThru3)get_store().find_element_user(PASSTHRU3$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "PassThru3" element
     */
    public void setPassThru3(java.lang.String passThru3)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU3$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU3$0);
            }
            target.setStringValue(passThru3);
        }
    }
    
    /**
     * Sets (as xml) the "PassThru3" element
     */
    public void xsetPassThru3(com.idanalytics.products.idscore.result.PassThru3Document.PassThru3 passThru3)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.PassThru3Document.PassThru3 target = null;
            target = (com.idanalytics.products.idscore.result.PassThru3Document.PassThru3)get_store().find_element_user(PASSTHRU3$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.result.PassThru3Document.PassThru3)get_store().add_element_user(PASSTHRU3$0);
            }
            target.set(passThru3);
        }
    }
    /**
     * An XML PassThru3(@http://idanalytics.com/products/idscore/result).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result.PassThru3Document$PassThru3.
     */
    public static class PassThru3Impl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.result.PassThru3Document.PassThru3
    {
        private static final long serialVersionUID = 1L;
        
        public PassThru3Impl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected PassThru3Impl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
