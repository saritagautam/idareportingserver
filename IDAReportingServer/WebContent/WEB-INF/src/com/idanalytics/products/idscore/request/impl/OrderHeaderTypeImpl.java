/*
 * XML Type:  OrderHeader_type
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.OrderHeaderType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML OrderHeader_type(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public class OrderHeaderTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.OrderHeaderType
{
    private static final long serialVersionUID = 1L;
    
    public OrderHeaderTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CPC$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "CPC");
    private static final javax.xml.namespace.QName REQUESTTYPE$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "RequestType");
    private static final javax.xml.namespace.QName ORDERDATE$4 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "OrderDate");
    private static final javax.xml.namespace.QName CLIENTKEY$6 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ClientKey");
    private static final javax.xml.namespace.QName ORDERAMOUNT$8 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "OrderAmount");
    private static final javax.xml.namespace.QName IPADDRESS$10 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "IPAddress");
    private static final javax.xml.namespace.QName MERCHANTID$12 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "MerchantID");
    private static final javax.xml.namespace.QName PASSTHRU1$14 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PassThru1");
    private static final javax.xml.namespace.QName PASSTHRU2$16 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PassThru2");
    private static final javax.xml.namespace.QName PASSTHRU3$18 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PassThru3");
    private static final javax.xml.namespace.QName PASSTHRU4$20 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PassThru4");
    
    
    /**
     * Gets the "CPC" element
     */
    public java.lang.String getCPC()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CPC$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "CPC" element
     */
    public com.idanalytics.products.idscore.request.CPCDocument.CPC xgetCPC()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.CPCDocument.CPC target = null;
            target = (com.idanalytics.products.idscore.request.CPCDocument.CPC)get_store().find_element_user(CPC$0, 0);
            return target;
        }
    }
    
    /**
     * True if has "CPC" element
     */
    public boolean isSetCPC()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CPC$0) != 0;
        }
    }
    
    /**
     * Sets the "CPC" element
     */
    public void setCPC(java.lang.String cpc)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CPC$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CPC$0);
            }
            target.setStringValue(cpc);
        }
    }
    
    /**
     * Sets (as xml) the "CPC" element
     */
    public void xsetCPC(com.idanalytics.products.idscore.request.CPCDocument.CPC cpc)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.CPCDocument.CPC target = null;
            target = (com.idanalytics.products.idscore.request.CPCDocument.CPC)get_store().find_element_user(CPC$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.CPCDocument.CPC)get_store().add_element_user(CPC$0);
            }
            target.set(cpc);
        }
    }
    
    /**
     * Unsets the "CPC" element
     */
    public void unsetCPC()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CPC$0, 0);
        }
    }
    
    /**
     * Gets the "RequestType" element
     */
    public java.lang.Object getRequestType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(REQUESTTYPE$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getObjectValue();
        }
    }
    
    /**
     * Gets (as xml) the "RequestType" element
     */
    public com.idanalytics.products.idscore.request.RequestTypeDocument.RequestType xgetRequestType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.RequestTypeDocument.RequestType target = null;
            target = (com.idanalytics.products.idscore.request.RequestTypeDocument.RequestType)get_store().find_element_user(REQUESTTYPE$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "RequestType" element
     */
    public void setRequestType(java.lang.Object requestType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(REQUESTTYPE$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(REQUESTTYPE$2);
            }
            target.setObjectValue(requestType);
        }
    }
    
    /**
     * Sets (as xml) the "RequestType" element
     */
    public void xsetRequestType(com.idanalytics.products.idscore.request.RequestTypeDocument.RequestType requestType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.RequestTypeDocument.RequestType target = null;
            target = (com.idanalytics.products.idscore.request.RequestTypeDocument.RequestType)get_store().find_element_user(REQUESTTYPE$2, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.RequestTypeDocument.RequestType)get_store().add_element_user(REQUESTTYPE$2);
            }
            target.set(requestType);
        }
    }
    
    /**
     * Gets the "OrderDate" element
     */
    public java.util.Calendar getOrderDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ORDERDATE$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getCalendarValue();
        }
    }
    
    /**
     * Gets (as xml) the "OrderDate" element
     */
    public com.idanalytics.products.idscore.request.OrderDateDocument.OrderDate xgetOrderDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.OrderDateDocument.OrderDate target = null;
            target = (com.idanalytics.products.idscore.request.OrderDateDocument.OrderDate)get_store().find_element_user(ORDERDATE$4, 0);
            return target;
        }
    }
    
    /**
     * Sets the "OrderDate" element
     */
    public void setOrderDate(java.util.Calendar orderDate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ORDERDATE$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ORDERDATE$4);
            }
            target.setCalendarValue(orderDate);
        }
    }
    
    /**
     * Sets (as xml) the "OrderDate" element
     */
    public void xsetOrderDate(com.idanalytics.products.idscore.request.OrderDateDocument.OrderDate orderDate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.OrderDateDocument.OrderDate target = null;
            target = (com.idanalytics.products.idscore.request.OrderDateDocument.OrderDate)get_store().find_element_user(ORDERDATE$4, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.OrderDateDocument.OrderDate)get_store().add_element_user(ORDERDATE$4);
            }
            target.set(orderDate);
        }
    }
    
    /**
     * Gets the "ClientKey" element
     */
    public java.lang.String getClientKey()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CLIENTKEY$6, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "ClientKey" element
     */
    public com.idanalytics.products.idscore.request.ClientKeyDocument.ClientKey xgetClientKey()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ClientKeyDocument.ClientKey target = null;
            target = (com.idanalytics.products.idscore.request.ClientKeyDocument.ClientKey)get_store().find_element_user(CLIENTKEY$6, 0);
            return target;
        }
    }
    
    /**
     * Sets the "ClientKey" element
     */
    public void setClientKey(java.lang.String clientKey)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CLIENTKEY$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CLIENTKEY$6);
            }
            target.setStringValue(clientKey);
        }
    }
    
    /**
     * Sets (as xml) the "ClientKey" element
     */
    public void xsetClientKey(com.idanalytics.products.idscore.request.ClientKeyDocument.ClientKey clientKey)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ClientKeyDocument.ClientKey target = null;
            target = (com.idanalytics.products.idscore.request.ClientKeyDocument.ClientKey)get_store().find_element_user(CLIENTKEY$6, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.ClientKeyDocument.ClientKey)get_store().add_element_user(CLIENTKEY$6);
            }
            target.set(clientKey);
        }
    }
    
    /**
     * Gets the "OrderAmount" element
     */
    public java.math.BigDecimal getOrderAmount()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ORDERAMOUNT$8, 0);
            if (target == null)
            {
                return null;
            }
            return target.getBigDecimalValue();
        }
    }
    
    /**
     * Gets (as xml) the "OrderAmount" element
     */
    public com.idanalytics.products.idscore.request.OrderAmountDocument.OrderAmount xgetOrderAmount()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.OrderAmountDocument.OrderAmount target = null;
            target = (com.idanalytics.products.idscore.request.OrderAmountDocument.OrderAmount)get_store().find_element_user(ORDERAMOUNT$8, 0);
            return target;
        }
    }
    
    /**
     * Sets the "OrderAmount" element
     */
    public void setOrderAmount(java.math.BigDecimal orderAmount)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ORDERAMOUNT$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ORDERAMOUNT$8);
            }
            target.setBigDecimalValue(orderAmount);
        }
    }
    
    /**
     * Sets (as xml) the "OrderAmount" element
     */
    public void xsetOrderAmount(com.idanalytics.products.idscore.request.OrderAmountDocument.OrderAmount orderAmount)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.OrderAmountDocument.OrderAmount target = null;
            target = (com.idanalytics.products.idscore.request.OrderAmountDocument.OrderAmount)get_store().find_element_user(ORDERAMOUNT$8, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.OrderAmountDocument.OrderAmount)get_store().add_element_user(ORDERAMOUNT$8);
            }
            target.set(orderAmount);
        }
    }
    
    /**
     * Gets the "IPAddress" element
     */
    public java.lang.String getIPAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IPADDRESS$10, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IPAddress" element
     */
    public com.idanalytics.products.idscore.request.IPAddressDocument.IPAddress xgetIPAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.IPAddressDocument.IPAddress target = null;
            target = (com.idanalytics.products.idscore.request.IPAddressDocument.IPAddress)get_store().find_element_user(IPADDRESS$10, 0);
            return target;
        }
    }
    
    /**
     * True if has "IPAddress" element
     */
    public boolean isSetIPAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(IPADDRESS$10) != 0;
        }
    }
    
    /**
     * Sets the "IPAddress" element
     */
    public void setIPAddress(java.lang.String ipAddress)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IPADDRESS$10, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IPADDRESS$10);
            }
            target.setStringValue(ipAddress);
        }
    }
    
    /**
     * Sets (as xml) the "IPAddress" element
     */
    public void xsetIPAddress(com.idanalytics.products.idscore.request.IPAddressDocument.IPAddress ipAddress)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.IPAddressDocument.IPAddress target = null;
            target = (com.idanalytics.products.idscore.request.IPAddressDocument.IPAddress)get_store().find_element_user(IPADDRESS$10, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.IPAddressDocument.IPAddress)get_store().add_element_user(IPADDRESS$10);
            }
            target.set(ipAddress);
        }
    }
    
    /**
     * Unsets the "IPAddress" element
     */
    public void unsetIPAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(IPADDRESS$10, 0);
        }
    }
    
    /**
     * Gets the "MerchantID" element
     */
    public java.lang.String getMerchantID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MERCHANTID$12, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "MerchantID" element
     */
    public com.idanalytics.products.idscore.request.MerchantIDDocument.MerchantID xgetMerchantID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.MerchantIDDocument.MerchantID target = null;
            target = (com.idanalytics.products.idscore.request.MerchantIDDocument.MerchantID)get_store().find_element_user(MERCHANTID$12, 0);
            return target;
        }
    }
    
    /**
     * Sets the "MerchantID" element
     */
    public void setMerchantID(java.lang.String merchantID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MERCHANTID$12, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(MERCHANTID$12);
            }
            target.setStringValue(merchantID);
        }
    }
    
    /**
     * Sets (as xml) the "MerchantID" element
     */
    public void xsetMerchantID(com.idanalytics.products.idscore.request.MerchantIDDocument.MerchantID merchantID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.MerchantIDDocument.MerchantID target = null;
            target = (com.idanalytics.products.idscore.request.MerchantIDDocument.MerchantID)get_store().find_element_user(MERCHANTID$12, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.MerchantIDDocument.MerchantID)get_store().add_element_user(MERCHANTID$12);
            }
            target.set(merchantID);
        }
    }
    
    /**
     * Gets the "PassThru1" element
     */
    public java.lang.String getPassThru1()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU1$14, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PassThru1" element
     */
    public com.idanalytics.products.common_v1.PassThru xgetPassThru1()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.PassThru target = null;
            target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU1$14, 0);
            return target;
        }
    }
    
    /**
     * True if has "PassThru1" element
     */
    public boolean isSetPassThru1()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PASSTHRU1$14) != 0;
        }
    }
    
    /**
     * Sets the "PassThru1" element
     */
    public void setPassThru1(java.lang.String passThru1)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU1$14, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU1$14);
            }
            target.setStringValue(passThru1);
        }
    }
    
    /**
     * Sets (as xml) the "PassThru1" element
     */
    public void xsetPassThru1(com.idanalytics.products.common_v1.PassThru passThru1)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.PassThru target = null;
            target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU1$14, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.PassThru)get_store().add_element_user(PASSTHRU1$14);
            }
            target.set(passThru1);
        }
    }
    
    /**
     * Unsets the "PassThru1" element
     */
    public void unsetPassThru1()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PASSTHRU1$14, 0);
        }
    }
    
    /**
     * Gets the "PassThru2" element
     */
    public java.lang.String getPassThru2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU2$16, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PassThru2" element
     */
    public com.idanalytics.products.common_v1.PassThru xgetPassThru2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.PassThru target = null;
            target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU2$16, 0);
            return target;
        }
    }
    
    /**
     * True if has "PassThru2" element
     */
    public boolean isSetPassThru2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PASSTHRU2$16) != 0;
        }
    }
    
    /**
     * Sets the "PassThru2" element
     */
    public void setPassThru2(java.lang.String passThru2)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU2$16, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU2$16);
            }
            target.setStringValue(passThru2);
        }
    }
    
    /**
     * Sets (as xml) the "PassThru2" element
     */
    public void xsetPassThru2(com.idanalytics.products.common_v1.PassThru passThru2)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.PassThru target = null;
            target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU2$16, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.PassThru)get_store().add_element_user(PASSTHRU2$16);
            }
            target.set(passThru2);
        }
    }
    
    /**
     * Unsets the "PassThru2" element
     */
    public void unsetPassThru2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PASSTHRU2$16, 0);
        }
    }
    
    /**
     * Gets the "PassThru3" element
     */
    public java.lang.String getPassThru3()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU3$18, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PassThru3" element
     */
    public com.idanalytics.products.common_v1.PassThru xgetPassThru3()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.PassThru target = null;
            target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU3$18, 0);
            return target;
        }
    }
    
    /**
     * True if has "PassThru3" element
     */
    public boolean isSetPassThru3()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PASSTHRU3$18) != 0;
        }
    }
    
    /**
     * Sets the "PassThru3" element
     */
    public void setPassThru3(java.lang.String passThru3)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU3$18, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU3$18);
            }
            target.setStringValue(passThru3);
        }
    }
    
    /**
     * Sets (as xml) the "PassThru3" element
     */
    public void xsetPassThru3(com.idanalytics.products.common_v1.PassThru passThru3)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.PassThru target = null;
            target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU3$18, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.PassThru)get_store().add_element_user(PASSTHRU3$18);
            }
            target.set(passThru3);
        }
    }
    
    /**
     * Unsets the "PassThru3" element
     */
    public void unsetPassThru3()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PASSTHRU3$18, 0);
        }
    }
    
    /**
     * Gets the "PassThru4" element
     */
    public java.lang.String getPassThru4()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU4$20, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PassThru4" element
     */
    public com.idanalytics.products.common_v1.PassThru xgetPassThru4()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.PassThru target = null;
            target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU4$20, 0);
            return target;
        }
    }
    
    /**
     * True if has "PassThru4" element
     */
    public boolean isSetPassThru4()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PASSTHRU4$20) != 0;
        }
    }
    
    /**
     * Sets the "PassThru4" element
     */
    public void setPassThru4(java.lang.String passThru4)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU4$20, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU4$20);
            }
            target.setStringValue(passThru4);
        }
    }
    
    /**
     * Sets (as xml) the "PassThru4" element
     */
    public void xsetPassThru4(com.idanalytics.products.common_v1.PassThru passThru4)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.PassThru target = null;
            target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU4$20, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.PassThru)get_store().add_element_user(PASSTHRU4$20);
            }
            target.set(passThru4);
        }
    }
    
    /**
     * Unsets the "PassThru4" element
     */
    public void unsetPassThru4()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PASSTHRU4$20, 0);
        }
    }
}
