/*
 * XML Type:  Name
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.Name
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML Name(@http://idanalytics.com/products/idscore/request).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.Name.
 */
public class NameImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.request.Name
{
    private static final long serialVersionUID = 1L;
    
    public NameImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected NameImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
