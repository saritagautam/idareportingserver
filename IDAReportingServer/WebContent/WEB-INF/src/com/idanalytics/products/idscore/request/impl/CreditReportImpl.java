/*
 * XML Type:  CreditReport
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.CreditReport
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML CreditReport(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public class CreditReportImpl extends com.idanalytics.products.idscore.request.impl.IdentityImpl implements com.idanalytics.products.idscore.request.CreditReport
{
    private static final long serialVersionUID = 1L;
    
    public CreditReportImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName PREVHOMEPHONE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PrevHomePhone");
    private static final javax.xml.namespace.QName SOURCE$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Source");
    private static final javax.xml.namespace.QName PREV2ADDRESSSINCE$4 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Prev2AddressSince");
    private static final javax.xml.namespace.QName PREV2ADDRESS$6 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Prev2Address");
    private static final javax.xml.namespace.QName PARSEDPREV2ADDRESS$8 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ParsedPrev2Address");
    private static final javax.xml.namespace.QName PREV2CITY$10 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Prev2City");
    private static final javax.xml.namespace.QName PREV2STATE$12 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Prev2State");
    private static final javax.xml.namespace.QName PREV2ZIP$14 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Prev2Zip");
    private static final javax.xml.namespace.QName PREV2COUNTRY$16 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Prev2Country");
    private static final javax.xml.namespace.QName TIMEONFILE$18 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "TimeOnFile");
    private static final javax.xml.namespace.QName OLDESTTRADEMONTHS$20 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "OldestTradeMonths");
    private static final javax.xml.namespace.QName MORTGAGEPAYMENT$22 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "MortgagePayment");
    private static final javax.xml.namespace.QName FRAUDFLAGS$24 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "FraudFlags");
    private static final javax.xml.namespace.QName CONSUMERSTATEMENT$26 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ConsumerStatement");
    private static final javax.xml.namespace.QName FICOSCORE$28 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "FicoScore");
    
    
    /**
     * Gets first "PrevHomePhone" element
     */
    public java.lang.String getPrevHomePhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVHOMEPHONE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) first "PrevHomePhone" element
     */
    public com.idanalytics.products.idscore.request.Phone xgetPrevHomePhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().find_element_user(PREVHOMEPHONE$0, 0);
            return target;
        }
    }
    
    /**
     * True if has at least one "PrevHomePhone" element
     */
    public boolean isSetPrevHomePhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREVHOMEPHONE$0) != 0;
        }
    }
    
    /**
     * Gets array of all "PrevHomePhone" elements
     */
    public java.lang.String[] getPrevHomePhoneArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(PREVHOMEPHONE$0, targetList);
            java.lang.String[] result = new java.lang.String[targetList.size()];
            for (int i = 0, len = targetList.size() ; i < len ; i++)
                result[i] = ((org.apache.xmlbeans.SimpleValue)targetList.get(i)).getStringValue();
            return result;
        }
    }
    
    /**
     * Gets ith "PrevHomePhone" element
     */
    public java.lang.String getPrevHomePhoneArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVHOMEPHONE$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) array of all "PrevHomePhone" elements
     */
    public com.idanalytics.products.idscore.request.Phone[] xgetPrevHomePhoneArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(PREVHOMEPHONE$0, targetList);
            com.idanalytics.products.idscore.request.Phone[] result = new com.idanalytics.products.idscore.request.Phone[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets (as xml) ith "PrevHomePhone" element
     */
    public com.idanalytics.products.idscore.request.Phone xgetPrevHomePhoneArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().find_element_user(PREVHOMEPHONE$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "PrevHomePhone" element
     */
    public int sizeOfPrevHomePhoneArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREVHOMEPHONE$0);
        }
    }
    
    /**
     * Sets first "PrevHomePhone" element
     */
    public void setPrevHomePhone(java.lang.String prevHomePhone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVHOMEPHONE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREVHOMEPHONE$0);
            }
            target.setStringValue(prevHomePhone);
        }
    }
    
    /**
     * Sets (as xml) first "PrevHomePhone" element
     */
    public void xsetPrevHomePhone(com.idanalytics.products.idscore.request.Phone prevHomePhone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().find_element_user(PREVHOMEPHONE$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Phone)get_store().add_element_user(PREVHOMEPHONE$0);
            }
            target.set(prevHomePhone);
        }
    }
    
    /**
     * Removes first "PrevHomePhone" element
     */
    public void unsetPrevHomePhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREVHOMEPHONE$0, 0);
        }
    }
    
    /**
     * Sets array of all "PrevHomePhone" element
     */
    public void setPrevHomePhoneArray(java.lang.String[] prevHomePhoneArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(prevHomePhoneArray, PREVHOMEPHONE$0);
        }
    }
    
    /**
     * Sets ith "PrevHomePhone" element
     */
    public void setPrevHomePhoneArray(int i, java.lang.String prevHomePhone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREVHOMEPHONE$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.setStringValue(prevHomePhone);
        }
    }
    
    /**
     * Sets (as xml) array of all "PrevHomePhone" element
     */
    public void xsetPrevHomePhoneArray(com.idanalytics.products.idscore.request.Phone[]prevHomePhoneArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(prevHomePhoneArray, PREVHOMEPHONE$0);
        }
    }
    
    /**
     * Sets (as xml) ith "PrevHomePhone" element
     */
    public void xsetPrevHomePhoneArray(int i, com.idanalytics.products.idscore.request.Phone prevHomePhone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().find_element_user(PREVHOMEPHONE$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(prevHomePhone);
        }
    }
    
    /**
     * Inserts the value as the ith "PrevHomePhone" element
     */
    public void insertPrevHomePhone(int i, java.lang.String prevHomePhone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = 
                (org.apache.xmlbeans.SimpleValue)get_store().insert_element_user(PREVHOMEPHONE$0, i);
            target.setStringValue(prevHomePhone);
        }
    }
    
    /**
     * Appends the value as the last "PrevHomePhone" element
     */
    public void addPrevHomePhone(java.lang.String prevHomePhone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREVHOMEPHONE$0);
            target.setStringValue(prevHomePhone);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "PrevHomePhone" element
     */
    public com.idanalytics.products.idscore.request.Phone insertNewPrevHomePhone(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().insert_element_user(PREVHOMEPHONE$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "PrevHomePhone" element
     */
    public com.idanalytics.products.idscore.request.Phone addNewPrevHomePhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Phone target = null;
            target = (com.idanalytics.products.idscore.request.Phone)get_store().add_element_user(PREVHOMEPHONE$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "PrevHomePhone" element
     */
    public void removePrevHomePhone(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREVHOMEPHONE$0, i);
        }
    }
    
    /**
     * Gets the "Source" element
     */
    public com.idanalytics.products.idscore.request.Source.Enum getSource()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SOURCE$2, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.idscore.request.Source.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "Source" element
     */
    public com.idanalytics.products.idscore.request.Source xgetSource()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Source target = null;
            target = (com.idanalytics.products.idscore.request.Source)get_store().find_element_user(SOURCE$2, 0);
            return target;
        }
    }
    
    /**
     * True if has "Source" element
     */
    public boolean isSetSource()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SOURCE$2) != 0;
        }
    }
    
    /**
     * Sets the "Source" element
     */
    public void setSource(com.idanalytics.products.idscore.request.Source.Enum source)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SOURCE$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SOURCE$2);
            }
            target.setEnumValue(source);
        }
    }
    
    /**
     * Sets (as xml) the "Source" element
     */
    public void xsetSource(com.idanalytics.products.idscore.request.Source source)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Source target = null;
            target = (com.idanalytics.products.idscore.request.Source)get_store().find_element_user(SOURCE$2, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Source)get_store().add_element_user(SOURCE$2);
            }
            target.set(source);
        }
    }
    
    /**
     * Unsets the "Source" element
     */
    public void unsetSource()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SOURCE$2, 0);
        }
    }
    
    /**
     * Gets the "Prev2AddressSince" element
     */
    public java.lang.Object getPrev2AddressSince()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREV2ADDRESSSINCE$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getObjectValue();
        }
    }
    
    /**
     * Gets (as xml) the "Prev2AddressSince" element
     */
    public com.idanalytics.products.idscore.request.Date xgetPrev2AddressSince()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Date target = null;
            target = (com.idanalytics.products.idscore.request.Date)get_store().find_element_user(PREV2ADDRESSSINCE$4, 0);
            return target;
        }
    }
    
    /**
     * True if has "Prev2AddressSince" element
     */
    public boolean isSetPrev2AddressSince()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREV2ADDRESSSINCE$4) != 0;
        }
    }
    
    /**
     * Sets the "Prev2AddressSince" element
     */
    public void setPrev2AddressSince(java.lang.Object prev2AddressSince)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREV2ADDRESSSINCE$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREV2ADDRESSSINCE$4);
            }
            target.setObjectValue(prev2AddressSince);
        }
    }
    
    /**
     * Sets (as xml) the "Prev2AddressSince" element
     */
    public void xsetPrev2AddressSince(com.idanalytics.products.idscore.request.Date prev2AddressSince)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Date target = null;
            target = (com.idanalytics.products.idscore.request.Date)get_store().find_element_user(PREV2ADDRESSSINCE$4, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Date)get_store().add_element_user(PREV2ADDRESSSINCE$4);
            }
            target.set(prev2AddressSince);
        }
    }
    
    /**
     * Unsets the "Prev2AddressSince" element
     */
    public void unsetPrev2AddressSince()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREV2ADDRESSSINCE$4, 0);
        }
    }
    
    /**
     * Gets the "Prev2Address" element
     */
    public com.idanalytics.products.idscore.request.Address getPrev2Address()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Address target = null;
            target = (com.idanalytics.products.idscore.request.Address)get_store().find_element_user(PREV2ADDRESS$6, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "Prev2Address" element
     */
    public boolean isSetPrev2Address()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREV2ADDRESS$6) != 0;
        }
    }
    
    /**
     * Sets the "Prev2Address" element
     */
    public void setPrev2Address(com.idanalytics.products.idscore.request.Address prev2Address)
    {
        generatedSetterHelperImpl(prev2Address, PREV2ADDRESS$6, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "Prev2Address" element
     */
    public com.idanalytics.products.idscore.request.Address addNewPrev2Address()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Address target = null;
            target = (com.idanalytics.products.idscore.request.Address)get_store().add_element_user(PREV2ADDRESS$6);
            return target;
        }
    }
    
    /**
     * Unsets the "Prev2Address" element
     */
    public void unsetPrev2Address()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREV2ADDRESS$6, 0);
        }
    }
    
    /**
     * Gets the "ParsedPrev2Address" element
     */
    public com.idanalytics.products.idscore.request.ParsedAddress getParsedPrev2Address()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ParsedAddress target = null;
            target = (com.idanalytics.products.idscore.request.ParsedAddress)get_store().find_element_user(PARSEDPREV2ADDRESS$8, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "ParsedPrev2Address" element
     */
    public boolean isSetParsedPrev2Address()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PARSEDPREV2ADDRESS$8) != 0;
        }
    }
    
    /**
     * Sets the "ParsedPrev2Address" element
     */
    public void setParsedPrev2Address(com.idanalytics.products.idscore.request.ParsedAddress parsedPrev2Address)
    {
        generatedSetterHelperImpl(parsedPrev2Address, PARSEDPREV2ADDRESS$8, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "ParsedPrev2Address" element
     */
    public com.idanalytics.products.idscore.request.ParsedAddress addNewParsedPrev2Address()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ParsedAddress target = null;
            target = (com.idanalytics.products.idscore.request.ParsedAddress)get_store().add_element_user(PARSEDPREV2ADDRESS$8);
            return target;
        }
    }
    
    /**
     * Unsets the "ParsedPrev2Address" element
     */
    public void unsetParsedPrev2Address()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PARSEDPREV2ADDRESS$8, 0);
        }
    }
    
    /**
     * Gets the "Prev2City" element
     */
    public java.lang.String getPrev2City()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREV2CITY$10, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Prev2City" element
     */
    public com.idanalytics.products.idscore.request.City xgetPrev2City()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.City target = null;
            target = (com.idanalytics.products.idscore.request.City)get_store().find_element_user(PREV2CITY$10, 0);
            return target;
        }
    }
    
    /**
     * True if has "Prev2City" element
     */
    public boolean isSetPrev2City()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREV2CITY$10) != 0;
        }
    }
    
    /**
     * Sets the "Prev2City" element
     */
    public void setPrev2City(java.lang.String prev2City)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREV2CITY$10, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREV2CITY$10);
            }
            target.setStringValue(prev2City);
        }
    }
    
    /**
     * Sets (as xml) the "Prev2City" element
     */
    public void xsetPrev2City(com.idanalytics.products.idscore.request.City prev2City)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.City target = null;
            target = (com.idanalytics.products.idscore.request.City)get_store().find_element_user(PREV2CITY$10, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.City)get_store().add_element_user(PREV2CITY$10);
            }
            target.set(prev2City);
        }
    }
    
    /**
     * Unsets the "Prev2City" element
     */
    public void unsetPrev2City()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREV2CITY$10, 0);
        }
    }
    
    /**
     * Gets the "Prev2State" element
     */
    public java.lang.String getPrev2State()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREV2STATE$12, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Prev2State" element
     */
    public com.idanalytics.products.idscore.request.State xgetPrev2State()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.State target = null;
            target = (com.idanalytics.products.idscore.request.State)get_store().find_element_user(PREV2STATE$12, 0);
            return target;
        }
    }
    
    /**
     * True if has "Prev2State" element
     */
    public boolean isSetPrev2State()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREV2STATE$12) != 0;
        }
    }
    
    /**
     * Sets the "Prev2State" element
     */
    public void setPrev2State(java.lang.String prev2State)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREV2STATE$12, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREV2STATE$12);
            }
            target.setStringValue(prev2State);
        }
    }
    
    /**
     * Sets (as xml) the "Prev2State" element
     */
    public void xsetPrev2State(com.idanalytics.products.idscore.request.State prev2State)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.State target = null;
            target = (com.idanalytics.products.idscore.request.State)get_store().find_element_user(PREV2STATE$12, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.State)get_store().add_element_user(PREV2STATE$12);
            }
            target.set(prev2State);
        }
    }
    
    /**
     * Unsets the "Prev2State" element
     */
    public void unsetPrev2State()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREV2STATE$12, 0);
        }
    }
    
    /**
     * Gets the "Prev2Zip" element
     */
    public java.lang.String getPrev2Zip()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREV2ZIP$14, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Prev2Zip" element
     */
    public com.idanalytics.products.idscore.request.Zip xgetPrev2Zip()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Zip target = null;
            target = (com.idanalytics.products.idscore.request.Zip)get_store().find_element_user(PREV2ZIP$14, 0);
            return target;
        }
    }
    
    /**
     * True if has "Prev2Zip" element
     */
    public boolean isSetPrev2Zip()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREV2ZIP$14) != 0;
        }
    }
    
    /**
     * Sets the "Prev2Zip" element
     */
    public void setPrev2Zip(java.lang.String prev2Zip)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREV2ZIP$14, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREV2ZIP$14);
            }
            target.setStringValue(prev2Zip);
        }
    }
    
    /**
     * Sets (as xml) the "Prev2Zip" element
     */
    public void xsetPrev2Zip(com.idanalytics.products.idscore.request.Zip prev2Zip)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Zip target = null;
            target = (com.idanalytics.products.idscore.request.Zip)get_store().find_element_user(PREV2ZIP$14, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Zip)get_store().add_element_user(PREV2ZIP$14);
            }
            target.set(prev2Zip);
        }
    }
    
    /**
     * Unsets the "Prev2Zip" element
     */
    public void unsetPrev2Zip()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREV2ZIP$14, 0);
        }
    }
    
    /**
     * Gets the "Prev2Country" element
     */
    public java.lang.String getPrev2Country()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREV2COUNTRY$16, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Prev2Country" element
     */
    public com.idanalytics.products.idscore.request.Country xgetPrev2Country()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Country target = null;
            target = (com.idanalytics.products.idscore.request.Country)get_store().find_element_user(PREV2COUNTRY$16, 0);
            return target;
        }
    }
    
    /**
     * True if has "Prev2Country" element
     */
    public boolean isSetPrev2Country()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREV2COUNTRY$16) != 0;
        }
    }
    
    /**
     * Sets the "Prev2Country" element
     */
    public void setPrev2Country(java.lang.String prev2Country)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PREV2COUNTRY$16, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PREV2COUNTRY$16);
            }
            target.setStringValue(prev2Country);
        }
    }
    
    /**
     * Sets (as xml) the "Prev2Country" element
     */
    public void xsetPrev2Country(com.idanalytics.products.idscore.request.Country prev2Country)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Country target = null;
            target = (com.idanalytics.products.idscore.request.Country)get_store().find_element_user(PREV2COUNTRY$16, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Country)get_store().add_element_user(PREV2COUNTRY$16);
            }
            target.set(prev2Country);
        }
    }
    
    /**
     * Unsets the "Prev2Country" element
     */
    public void unsetPrev2Country()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREV2COUNTRY$16, 0);
        }
    }
    
    /**
     * Gets the "TimeOnFile" element
     */
    public java.lang.String getTimeOnFile()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TIMEONFILE$18, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "TimeOnFile" element
     */
    public com.idanalytics.products.idscore.request.TimeAt xgetTimeOnFile()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.TimeAt target = null;
            target = (com.idanalytics.products.idscore.request.TimeAt)get_store().find_element_user(TIMEONFILE$18, 0);
            return target;
        }
    }
    
    /**
     * True if has "TimeOnFile" element
     */
    public boolean isSetTimeOnFile()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(TIMEONFILE$18) != 0;
        }
    }
    
    /**
     * Sets the "TimeOnFile" element
     */
    public void setTimeOnFile(java.lang.String timeOnFile)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TIMEONFILE$18, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TIMEONFILE$18);
            }
            target.setStringValue(timeOnFile);
        }
    }
    
    /**
     * Sets (as xml) the "TimeOnFile" element
     */
    public void xsetTimeOnFile(com.idanalytics.products.idscore.request.TimeAt timeOnFile)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.TimeAt target = null;
            target = (com.idanalytics.products.idscore.request.TimeAt)get_store().find_element_user(TIMEONFILE$18, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.TimeAt)get_store().add_element_user(TIMEONFILE$18);
            }
            target.set(timeOnFile);
        }
    }
    
    /**
     * Unsets the "TimeOnFile" element
     */
    public void unsetTimeOnFile()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(TIMEONFILE$18, 0);
        }
    }
    
    /**
     * Gets the "OldestTradeMonths" element
     */
    public java.lang.String getOldestTradeMonths()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(OLDESTTRADEMONTHS$20, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "OldestTradeMonths" element
     */
    public com.idanalytics.products.idscore.request.TimeAt xgetOldestTradeMonths()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.TimeAt target = null;
            target = (com.idanalytics.products.idscore.request.TimeAt)get_store().find_element_user(OLDESTTRADEMONTHS$20, 0);
            return target;
        }
    }
    
    /**
     * True if has "OldestTradeMonths" element
     */
    public boolean isSetOldestTradeMonths()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(OLDESTTRADEMONTHS$20) != 0;
        }
    }
    
    /**
     * Sets the "OldestTradeMonths" element
     */
    public void setOldestTradeMonths(java.lang.String oldestTradeMonths)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(OLDESTTRADEMONTHS$20, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(OLDESTTRADEMONTHS$20);
            }
            target.setStringValue(oldestTradeMonths);
        }
    }
    
    /**
     * Sets (as xml) the "OldestTradeMonths" element
     */
    public void xsetOldestTradeMonths(com.idanalytics.products.idscore.request.TimeAt oldestTradeMonths)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.TimeAt target = null;
            target = (com.idanalytics.products.idscore.request.TimeAt)get_store().find_element_user(OLDESTTRADEMONTHS$20, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.TimeAt)get_store().add_element_user(OLDESTTRADEMONTHS$20);
            }
            target.set(oldestTradeMonths);
        }
    }
    
    /**
     * Unsets the "OldestTradeMonths" element
     */
    public void unsetOldestTradeMonths()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(OLDESTTRADEMONTHS$20, 0);
        }
    }
    
    /**
     * Gets the "MortgagePayment" element
     */
    public java.lang.Object getMortgagePayment()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MORTGAGEPAYMENT$22, 0);
            if (target == null)
            {
                return null;
            }
            return target.getObjectValue();
        }
    }
    
    /**
     * Gets (as xml) the "MortgagePayment" element
     */
    public com.idanalytics.products.idscore.request.MortgagePayment xgetMortgagePayment()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.MortgagePayment target = null;
            target = (com.idanalytics.products.idscore.request.MortgagePayment)get_store().find_element_user(MORTGAGEPAYMENT$22, 0);
            return target;
        }
    }
    
    /**
     * True if has "MortgagePayment" element
     */
    public boolean isSetMortgagePayment()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(MORTGAGEPAYMENT$22) != 0;
        }
    }
    
    /**
     * Sets the "MortgagePayment" element
     */
    public void setMortgagePayment(java.lang.Object mortgagePayment)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MORTGAGEPAYMENT$22, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(MORTGAGEPAYMENT$22);
            }
            target.setObjectValue(mortgagePayment);
        }
    }
    
    /**
     * Sets (as xml) the "MortgagePayment" element
     */
    public void xsetMortgagePayment(com.idanalytics.products.idscore.request.MortgagePayment mortgagePayment)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.MortgagePayment target = null;
            target = (com.idanalytics.products.idscore.request.MortgagePayment)get_store().find_element_user(MORTGAGEPAYMENT$22, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.MortgagePayment)get_store().add_element_user(MORTGAGEPAYMENT$22);
            }
            target.set(mortgagePayment);
        }
    }
    
    /**
     * Unsets the "MortgagePayment" element
     */
    public void unsetMortgagePayment()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(MORTGAGEPAYMENT$22, 0);
        }
    }
    
    /**
     * Gets the "FraudFlags" element
     */
    public com.idanalytics.products.idscore.request.Bool.Enum getFraudFlags()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FRAUDFLAGS$24, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.idscore.request.Bool.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "FraudFlags" element
     */
    public com.idanalytics.products.idscore.request.Bool xgetFraudFlags()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Bool target = null;
            target = (com.idanalytics.products.idscore.request.Bool)get_store().find_element_user(FRAUDFLAGS$24, 0);
            return target;
        }
    }
    
    /**
     * True if has "FraudFlags" element
     */
    public boolean isSetFraudFlags()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(FRAUDFLAGS$24) != 0;
        }
    }
    
    /**
     * Sets the "FraudFlags" element
     */
    public void setFraudFlags(com.idanalytics.products.idscore.request.Bool.Enum fraudFlags)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FRAUDFLAGS$24, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(FRAUDFLAGS$24);
            }
            target.setEnumValue(fraudFlags);
        }
    }
    
    /**
     * Sets (as xml) the "FraudFlags" element
     */
    public void xsetFraudFlags(com.idanalytics.products.idscore.request.Bool fraudFlags)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Bool target = null;
            target = (com.idanalytics.products.idscore.request.Bool)get_store().find_element_user(FRAUDFLAGS$24, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Bool)get_store().add_element_user(FRAUDFLAGS$24);
            }
            target.set(fraudFlags);
        }
    }
    
    /**
     * Unsets the "FraudFlags" element
     */
    public void unsetFraudFlags()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(FRAUDFLAGS$24, 0);
        }
    }
    
    /**
     * Gets the "ConsumerStatement" element
     */
    public com.idanalytics.products.idscore.request.Bool.Enum getConsumerStatement()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CONSUMERSTATEMENT$26, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.idscore.request.Bool.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "ConsumerStatement" element
     */
    public com.idanalytics.products.idscore.request.Bool xgetConsumerStatement()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Bool target = null;
            target = (com.idanalytics.products.idscore.request.Bool)get_store().find_element_user(CONSUMERSTATEMENT$26, 0);
            return target;
        }
    }
    
    /**
     * True if has "ConsumerStatement" element
     */
    public boolean isSetConsumerStatement()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CONSUMERSTATEMENT$26) != 0;
        }
    }
    
    /**
     * Sets the "ConsumerStatement" element
     */
    public void setConsumerStatement(com.idanalytics.products.idscore.request.Bool.Enum consumerStatement)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CONSUMERSTATEMENT$26, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CONSUMERSTATEMENT$26);
            }
            target.setEnumValue(consumerStatement);
        }
    }
    
    /**
     * Sets (as xml) the "ConsumerStatement" element
     */
    public void xsetConsumerStatement(com.idanalytics.products.idscore.request.Bool consumerStatement)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Bool target = null;
            target = (com.idanalytics.products.idscore.request.Bool)get_store().find_element_user(CONSUMERSTATEMENT$26, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Bool)get_store().add_element_user(CONSUMERSTATEMENT$26);
            }
            target.set(consumerStatement);
        }
    }
    
    /**
     * Unsets the "ConsumerStatement" element
     */
    public void unsetConsumerStatement()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CONSUMERSTATEMENT$26, 0);
        }
    }
    
    /**
     * Gets the "FicoScore" element
     */
    public java.math.BigInteger getFicoScore()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FICOSCORE$28, 0);
            if (target == null)
            {
                return null;
            }
            return target.getBigIntegerValue();
        }
    }
    
    /**
     * Gets (as xml) the "FicoScore" element
     */
    public com.idanalytics.products.idscore.request.FicoScore xgetFicoScore()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.FicoScore target = null;
            target = (com.idanalytics.products.idscore.request.FicoScore)get_store().find_element_user(FICOSCORE$28, 0);
            return target;
        }
    }
    
    /**
     * True if has "FicoScore" element
     */
    public boolean isSetFicoScore()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(FICOSCORE$28) != 0;
        }
    }
    
    /**
     * Sets the "FicoScore" element
     */
    public void setFicoScore(java.math.BigInteger ficoScore)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FICOSCORE$28, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(FICOSCORE$28);
            }
            target.setBigIntegerValue(ficoScore);
        }
    }
    
    /**
     * Sets (as xml) the "FicoScore" element
     */
    public void xsetFicoScore(com.idanalytics.products.idscore.request.FicoScore ficoScore)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.FicoScore target = null;
            target = (com.idanalytics.products.idscore.request.FicoScore)get_store().find_element_user(FICOSCORE$28, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.FicoScore)get_store().add_element_user(FICOSCORE$28);
            }
            target.set(ficoScore);
        }
    }
    
    /**
     * Unsets the "FicoScore" element
     */
    public void unsetFicoScore()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(FICOSCORE$28, 0);
        }
    }
}
