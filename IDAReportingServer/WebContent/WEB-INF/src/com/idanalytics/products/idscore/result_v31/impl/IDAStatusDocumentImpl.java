/*
 * An XML document type.
 * Localname: IDAStatus
 * Namespace: http://idanalytics.com/products/idscore/result.v31
 * Java type: com.idanalytics.products.idscore.result_v31.IDAStatusDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.result_v31.impl;
/**
 * A document containing one IDAStatus(@http://idanalytics.com/products/idscore/result.v31) element.
 *
 * This is a complex type.
 */
public class IDAStatusDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.result_v31.IDAStatusDocument
{
    private static final long serialVersionUID = 1L;
    
    public IDAStatusDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IDASTATUS$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result.v31", "IDAStatus");
    
    
    /**
     * Gets the "IDAStatus" element
     */
    public int getIDAStatus()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASTATUS$0, 0);
            if (target == null)
            {
                return 0;
            }
            return target.getIntValue();
        }
    }
    
    /**
     * Gets (as xml) the "IDAStatus" element
     */
    public com.idanalytics.products.idscore.result_v31.IDAStatusDocument.IDAStatus xgetIDAStatus()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result_v31.IDAStatusDocument.IDAStatus target = null;
            target = (com.idanalytics.products.idscore.result_v31.IDAStatusDocument.IDAStatus)get_store().find_element_user(IDASTATUS$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IDAStatus" element
     */
    public void setIDAStatus(int idaStatus)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASTATUS$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDASTATUS$0);
            }
            target.setIntValue(idaStatus);
        }
    }
    
    /**
     * Sets (as xml) the "IDAStatus" element
     */
    public void xsetIDAStatus(com.idanalytics.products.idscore.result_v31.IDAStatusDocument.IDAStatus idaStatus)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result_v31.IDAStatusDocument.IDAStatus target = null;
            target = (com.idanalytics.products.idscore.result_v31.IDAStatusDocument.IDAStatus)get_store().find_element_user(IDASTATUS$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.result_v31.IDAStatusDocument.IDAStatus)get_store().add_element_user(IDASTATUS$0);
            }
            target.set(idaStatus);
        }
    }
    /**
     * An XML IDAStatus(@http://idanalytics.com/products/idscore/result.v31).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result_v31.IDAStatusDocument$IDAStatus.
     */
    public static class IDAStatusImpl extends org.apache.xmlbeans.impl.values.JavaIntHolderEx implements com.idanalytics.products.idscore.result_v31.IDAStatusDocument.IDAStatus
    {
        private static final long serialVersionUID = 1L;
        
        public IDAStatusImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected IDAStatusImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
