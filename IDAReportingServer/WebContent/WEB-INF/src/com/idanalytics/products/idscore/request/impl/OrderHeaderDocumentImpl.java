/*
 * An XML document type.
 * Localname: OrderHeader
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.OrderHeaderDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * A document containing one OrderHeader(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public class OrderHeaderDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.OrderHeaderDocument
{
    private static final long serialVersionUID = 1L;
    
    public OrderHeaderDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ORDERHEADER$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "OrderHeader");
    
    
    /**
     * Gets the "OrderHeader" element
     */
    public com.idanalytics.products.idscore.request.OrderHeaderType getOrderHeader()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.OrderHeaderType target = null;
            target = (com.idanalytics.products.idscore.request.OrderHeaderType)get_store().find_element_user(ORDERHEADER$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "OrderHeader" element
     */
    public void setOrderHeader(com.idanalytics.products.idscore.request.OrderHeaderType orderHeader)
    {
        generatedSetterHelperImpl(orderHeader, ORDERHEADER$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "OrderHeader" element
     */
    public com.idanalytics.products.idscore.request.OrderHeaderType addNewOrderHeader()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.OrderHeaderType target = null;
            target = (com.idanalytics.products.idscore.request.OrderHeaderType)get_store().add_element_user(ORDERHEADER$0);
            return target;
        }
    }
}
