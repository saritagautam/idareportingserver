/*
 * An XML document type.
 * Localname: ClientKey
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.ClientKeyDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * A document containing one ClientKey(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public class ClientKeyDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.ClientKeyDocument
{
    private static final long serialVersionUID = 1L;
    
    public ClientKeyDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CLIENTKEY$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ClientKey");
    
    
    /**
     * Gets the "ClientKey" element
     */
    public java.lang.String getClientKey()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CLIENTKEY$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "ClientKey" element
     */
    public com.idanalytics.products.idscore.request.ClientKeyDocument.ClientKey xgetClientKey()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ClientKeyDocument.ClientKey target = null;
            target = (com.idanalytics.products.idscore.request.ClientKeyDocument.ClientKey)get_store().find_element_user(CLIENTKEY$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "ClientKey" element
     */
    public void setClientKey(java.lang.String clientKey)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CLIENTKEY$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CLIENTKEY$0);
            }
            target.setStringValue(clientKey);
        }
    }
    
    /**
     * Sets (as xml) the "ClientKey" element
     */
    public void xsetClientKey(com.idanalytics.products.idscore.request.ClientKeyDocument.ClientKey clientKey)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ClientKeyDocument.ClientKey target = null;
            target = (com.idanalytics.products.idscore.request.ClientKeyDocument.ClientKey)get_store().find_element_user(CLIENTKEY$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.ClientKeyDocument.ClientKey)get_store().add_element_user(CLIENTKEY$0);
            }
            target.set(clientKey);
        }
    }
    /**
     * An XML ClientKey(@http://idanalytics.com/products/idscore/request).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.ClientKeyDocument$ClientKey.
     */
    public static class ClientKeyImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.request.ClientKeyDocument.ClientKey
    {
        private static final long serialVersionUID = 1L;
        
        public ClientKeyImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected ClientKeyImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
