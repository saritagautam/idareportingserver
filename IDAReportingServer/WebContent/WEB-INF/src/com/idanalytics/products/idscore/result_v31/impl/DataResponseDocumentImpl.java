/*
 * An XML document type.
 * Localname: DataResponse
 * Namespace: http://idanalytics.com/products/idscore/result.v31
 * Java type: com.idanalytics.products.idscore.result_v31.DataResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.result_v31.impl;
/**
 * A document containing one DataResponse(@http://idanalytics.com/products/idscore/result.v31) element.
 *
 * This is a complex type.
 */
public class DataResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.result_v31.DataResponseDocument
{
    private static final long serialVersionUID = 1L;
    
    public DataResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName DATARESPONSE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result.v31", "DataResponse");
    
    
    /**
     * Gets the "DataResponse" element
     */
    public com.idanalytics.products.idscore.result_v31.DataResponseDocument.DataResponse getDataResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result_v31.DataResponseDocument.DataResponse target = null;
            target = (com.idanalytics.products.idscore.result_v31.DataResponseDocument.DataResponse)get_store().find_element_user(DATARESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "DataResponse" element
     */
    public void setDataResponse(com.idanalytics.products.idscore.result_v31.DataResponseDocument.DataResponse dataResponse)
    {
        generatedSetterHelperImpl(dataResponse, DATARESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "DataResponse" element
     */
    public com.idanalytics.products.idscore.result_v31.DataResponseDocument.DataResponse addNewDataResponse()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result_v31.DataResponseDocument.DataResponse target = null;
            target = (com.idanalytics.products.idscore.result_v31.DataResponseDocument.DataResponse)get_store().add_element_user(DATARESPONSE$0);
            return target;
        }
    }
    /**
     * An XML DataResponse(@http://idanalytics.com/products/idscore/result.v31).
     *
     * This is a complex type.
     */
    public static class DataResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.result_v31.DataResponseDocument.DataResponse
    {
        private static final long serialVersionUID = 1L;
        
        public DataResponseImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName IDAINTERNAL$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result.v31", "IDAInternal");
        
        
        /**
         * Gets the "IDAInternal" element
         */
        public com.idanalytics.products.idscore.result_v31.IDAInternalDocument.IDAInternal getIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.IDAInternalDocument.IDAInternal target = null;
                target = (com.idanalytics.products.idscore.result_v31.IDAInternalDocument.IDAInternal)get_store().find_element_user(IDAINTERNAL$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "IDAInternal" element
         */
        public boolean isSetIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(IDAINTERNAL$0) != 0;
            }
        }
        
        /**
         * Sets the "IDAInternal" element
         */
        public void setIDAInternal(com.idanalytics.products.idscore.result_v31.IDAInternalDocument.IDAInternal idaInternal)
        {
            generatedSetterHelperImpl(idaInternal, IDAINTERNAL$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "IDAInternal" element
         */
        public com.idanalytics.products.idscore.result_v31.IDAInternalDocument.IDAInternal addNewIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.IDAInternalDocument.IDAInternal target = null;
                target = (com.idanalytics.products.idscore.result_v31.IDAInternalDocument.IDAInternal)get_store().add_element_user(IDAINTERNAL$0);
                return target;
            }
        }
        
        /**
         * Unsets the "IDAInternal" element
         */
        public void unsetIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(IDAINTERNAL$0, 0);
            }
        }
    }
}
