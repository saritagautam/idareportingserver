/*
 * An XML document type.
 * Localname: BillingIdentity
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.BillingIdentityDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * A document containing one BillingIdentity(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public class BillingIdentityDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.BillingIdentityDocument
{
    private static final long serialVersionUID = 1L;
    
    public BillingIdentityDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName BILLINGIDENTITY$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "BillingIdentity");
    
    
    /**
     * Gets the "BillingIdentity" element
     */
    public com.idanalytics.products.idscore.request.OrderIdentity getBillingIdentity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.OrderIdentity target = null;
            target = (com.idanalytics.products.idscore.request.OrderIdentity)get_store().find_element_user(BILLINGIDENTITY$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "BillingIdentity" element
     */
    public void setBillingIdentity(com.idanalytics.products.idscore.request.OrderIdentity billingIdentity)
    {
        generatedSetterHelperImpl(billingIdentity, BILLINGIDENTITY$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "BillingIdentity" element
     */
    public com.idanalytics.products.idscore.request.OrderIdentity addNewBillingIdentity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.OrderIdentity target = null;
            target = (com.idanalytics.products.idscore.request.OrderIdentity)get_store().add_element_user(BILLINGIDENTITY$0);
            return target;
        }
    }
}
