/*
 * XML Type:  Percent
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.Percent
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML Percent(@http://idanalytics.com/products/idscore/request).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.Percent.
 */
public class PercentImpl extends org.apache.xmlbeans.impl.values.JavaDecimalHolderEx implements com.idanalytics.products.idscore.request.Percent
{
    private static final long serialVersionUID = 1L;
    
    public PercentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected PercentImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
