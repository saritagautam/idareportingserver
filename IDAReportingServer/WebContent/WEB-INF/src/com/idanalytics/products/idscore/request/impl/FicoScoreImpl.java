/*
 * XML Type:  FicoScore
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.FicoScore
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML FicoScore(@http://idanalytics.com/products/idscore/request).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.FicoScore.
 */
public class FicoScoreImpl extends org.apache.xmlbeans.impl.values.JavaIntegerHolderEx implements com.idanalytics.products.idscore.request.FicoScore
{
    private static final long serialVersionUID = 1L;
    
    public FicoScoreImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected FicoScoreImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
