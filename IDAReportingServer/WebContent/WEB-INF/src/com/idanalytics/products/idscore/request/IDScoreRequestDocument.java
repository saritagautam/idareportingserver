/*
 * An XML document type.
 * Localname: IDScoreRequest
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.IDScoreRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request;


/**
 * A document containing one IDScoreRequest(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public interface IDScoreRequestDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(IDScoreRequestDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("idscorerequestda20doctype");
    
    /**
     * Gets the "IDScoreRequest" element
     */
    com.idanalytics.products.idscore.request.IDScoreRequestDocument.IDScoreRequest getIDScoreRequest();
    
    /**
     * Sets the "IDScoreRequest" element
     */
    void setIDScoreRequest(com.idanalytics.products.idscore.request.IDScoreRequestDocument.IDScoreRequest idScoreRequest);
    
    /**
     * Appends and returns a new empty "IDScoreRequest" element
     */
    com.idanalytics.products.idscore.request.IDScoreRequestDocument.IDScoreRequest addNewIDScoreRequest();
    
    /**
     * An XML IDScoreRequest(@http://idanalytics.com/products/idscore/request).
     *
     * This is a complex type.
     */
    public interface IDScoreRequest extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(IDScoreRequest.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("idscorerequest89acelemtype");
        
        /**
         * Gets the "ServiceType" element
         */
        java.lang.String getServiceType();
        
        /**
         * Gets (as xml) the "ServiceType" element
         */
        com.idanalytics.products.idscore.request.ServiceType xgetServiceType();
        
        /**
         * True if has "ServiceType" element
         */
        boolean isSetServiceType();
        
        /**
         * Sets the "ServiceType" element
         */
        void setServiceType(java.lang.String serviceType);
        
        /**
         * Sets (as xml) the "ServiceType" element
         */
        void xsetServiceType(com.idanalytics.products.idscore.request.ServiceType serviceType);
        
        /**
         * Unsets the "ServiceType" element
         */
        void unsetServiceType();
        
        /**
         * Gets the "Origination" element
         */
        com.idanalytics.products.idscore.request.Origination getOrigination();
        
        /**
         * Sets the "Origination" element
         */
        void setOrigination(com.idanalytics.products.idscore.request.Origination origination);
        
        /**
         * Appends and returns a new empty "Origination" element
         */
        com.idanalytics.products.idscore.request.Origination addNewOrigination();
        
        /**
         * Gets the "ReconstructRequest" element
         */
        com.idanalytics.products.idscore.request.ReconstructRequestType getReconstructRequest();
        
        /**
         * True if has "ReconstructRequest" element
         */
        boolean isSetReconstructRequest();
        
        /**
         * Sets the "ReconstructRequest" element
         */
        void setReconstructRequest(com.idanalytics.products.idscore.request.ReconstructRequestType reconstructRequest);
        
        /**
         * Appends and returns a new empty "ReconstructRequest" element
         */
        com.idanalytics.products.idscore.request.ReconstructRequestType addNewReconstructRequest();
        
        /**
         * Unsets the "ReconstructRequest" element
         */
        void unsetReconstructRequest();
        
        /**
         * Gets the "Identity" element
         */
        com.idanalytics.products.idscore.request.Identity getIdentity();
        
        /**
         * Sets the "Identity" element
         */
        void setIdentity(com.idanalytics.products.idscore.request.Identity identity);
        
        /**
         * Appends and returns a new empty "Identity" element
         */
        com.idanalytics.products.idscore.request.Identity addNewIdentity();
        
        /**
         * Gets the "BillingIdentity" element
         */
        com.idanalytics.products.idscore.request.Identity getBillingIdentity();
        
        /**
         * True if has "BillingIdentity" element
         */
        boolean isSetBillingIdentity();
        
        /**
         * Sets the "BillingIdentity" element
         */
        void setBillingIdentity(com.idanalytics.products.idscore.request.Identity billingIdentity);
        
        /**
         * Appends and returns a new empty "BillingIdentity" element
         */
        com.idanalytics.products.idscore.request.Identity addNewBillingIdentity();
        
        /**
         * Unsets the "BillingIdentity" element
         */
        void unsetBillingIdentity();
        
        /**
         * Gets array of all "BillingInfo" elements
         */
        com.idanalytics.products.idscore.request.BillingInfo[] getBillingInfoArray();
        
        /**
         * Gets ith "BillingInfo" element
         */
        com.idanalytics.products.idscore.request.BillingInfo getBillingInfoArray(int i);
        
        /**
         * Returns number of "BillingInfo" element
         */
        int sizeOfBillingInfoArray();
        
        /**
         * Sets array of all "BillingInfo" element
         */
        void setBillingInfoArray(com.idanalytics.products.idscore.request.BillingInfo[] billingInfoArray);
        
        /**
         * Sets ith "BillingInfo" element
         */
        void setBillingInfoArray(int i, com.idanalytics.products.idscore.request.BillingInfo billingInfo);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "BillingInfo" element
         */
        com.idanalytics.products.idscore.request.BillingInfo insertNewBillingInfo(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "BillingInfo" element
         */
        com.idanalytics.products.idscore.request.BillingInfo addNewBillingInfo();
        
        /**
         * Removes the ith "BillingInfo" element
         */
        void removeBillingInfo(int i);
        
        /**
         * Gets array of all "ShippingInfo" elements
         */
        com.idanalytics.products.idscore.request.ShippingInfo[] getShippingInfoArray();
        
        /**
         * Gets ith "ShippingInfo" element
         */
        com.idanalytics.products.idscore.request.ShippingInfo getShippingInfoArray(int i);
        
        /**
         * Returns number of "ShippingInfo" element
         */
        int sizeOfShippingInfoArray();
        
        /**
         * Sets array of all "ShippingInfo" element
         */
        void setShippingInfoArray(com.idanalytics.products.idscore.request.ShippingInfo[] shippingInfoArray);
        
        /**
         * Sets ith "ShippingInfo" element
         */
        void setShippingInfoArray(int i, com.idanalytics.products.idscore.request.ShippingInfo shippingInfo);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "ShippingInfo" element
         */
        com.idanalytics.products.idscore.request.ShippingInfo insertNewShippingInfo(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "ShippingInfo" element
         */
        com.idanalytics.products.idscore.request.ShippingInfo addNewShippingInfo();
        
        /**
         * Removes the ith "ShippingInfo" element
         */
        void removeShippingInfo(int i);
        
        /**
         * Gets the "Application" element
         */
        com.idanalytics.products.idscore.request.Application getApplication();
        
        /**
         * Sets the "Application" element
         */
        void setApplication(com.idanalytics.products.idscore.request.Application application);
        
        /**
         * Appends and returns a new empty "Application" element
         */
        com.idanalytics.products.idscore.request.Application addNewApplication();
        
        /**
         * Gets array of all "AuthorizedUser" elements
         */
        com.idanalytics.products.idscore.request.AuthorizedUser[] getAuthorizedUserArray();
        
        /**
         * Gets ith "AuthorizedUser" element
         */
        com.idanalytics.products.idscore.request.AuthorizedUser getAuthorizedUserArray(int i);
        
        /**
         * Returns number of "AuthorizedUser" element
         */
        int sizeOfAuthorizedUserArray();
        
        /**
         * Sets array of all "AuthorizedUser" element
         */
        void setAuthorizedUserArray(com.idanalytics.products.idscore.request.AuthorizedUser[] authorizedUserArray);
        
        /**
         * Sets ith "AuthorizedUser" element
         */
        void setAuthorizedUserArray(int i, com.idanalytics.products.idscore.request.AuthorizedUser authorizedUser);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "AuthorizedUser" element
         */
        com.idanalytics.products.idscore.request.AuthorizedUser insertNewAuthorizedUser(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "AuthorizedUser" element
         */
        com.idanalytics.products.idscore.request.AuthorizedUser addNewAuthorizedUser();
        
        /**
         * Removes the ith "AuthorizedUser" element
         */
        void removeAuthorizedUser(int i);
        
        /**
         * Gets array of all "CreditReport" elements
         */
        com.idanalytics.products.idscore.request.CreditReport[] getCreditReportArray();
        
        /**
         * Gets ith "CreditReport" element
         */
        com.idanalytics.products.idscore.request.CreditReport getCreditReportArray(int i);
        
        /**
         * Returns number of "CreditReport" element
         */
        int sizeOfCreditReportArray();
        
        /**
         * Sets array of all "CreditReport" element
         */
        void setCreditReportArray(com.idanalytics.products.idscore.request.CreditReport[] creditReportArray);
        
        /**
         * Sets ith "CreditReport" element
         */
        void setCreditReportArray(int i, com.idanalytics.products.idscore.request.CreditReport creditReport);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "CreditReport" element
         */
        com.idanalytics.products.idscore.request.CreditReport insertNewCreditReport(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "CreditReport" element
         */
        com.idanalytics.products.idscore.request.CreditReport addNewCreditReport();
        
        /**
         * Removes the ith "CreditReport" element
         */
        void removeCreditReport(int i);
        
        /**
         * Gets array of all "ExistingAccount" elements
         */
        com.idanalytics.products.idscore.request.ExistingAccount[] getExistingAccountArray();
        
        /**
         * Gets ith "ExistingAccount" element
         */
        com.idanalytics.products.idscore.request.ExistingAccount getExistingAccountArray(int i);
        
        /**
         * Returns number of "ExistingAccount" element
         */
        int sizeOfExistingAccountArray();
        
        /**
         * Sets array of all "ExistingAccount" element
         */
        void setExistingAccountArray(com.idanalytics.products.idscore.request.ExistingAccount[] existingAccountArray);
        
        /**
         * Sets ith "ExistingAccount" element
         */
        void setExistingAccountArray(int i, com.idanalytics.products.idscore.request.ExistingAccount existingAccount);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "ExistingAccount" element
         */
        com.idanalytics.products.idscore.request.ExistingAccount insertNewExistingAccount(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "ExistingAccount" element
         */
        com.idanalytics.products.idscore.request.ExistingAccount addNewExistingAccount();
        
        /**
         * Removes the ith "ExistingAccount" element
         */
        void removeExistingAccount(int i);
        
        /**
         * Gets array of all "UserDefined" elements
         */
        com.idanalytics.products.idscore.request.UserDefined[] getUserDefinedArray();
        
        /**
         * Gets ith "UserDefined" element
         */
        com.idanalytics.products.idscore.request.UserDefined getUserDefinedArray(int i);
        
        /**
         * Returns number of "UserDefined" element
         */
        int sizeOfUserDefinedArray();
        
        /**
         * Sets array of all "UserDefined" element
         */
        void setUserDefinedArray(com.idanalytics.products.idscore.request.UserDefined[] userDefinedArray);
        
        /**
         * Sets ith "UserDefined" element
         */
        void setUserDefinedArray(int i, com.idanalytics.products.idscore.request.UserDefined userDefined);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "UserDefined" element
         */
        com.idanalytics.products.idscore.request.UserDefined insertNewUserDefined(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "UserDefined" element
         */
        com.idanalytics.products.idscore.request.UserDefined addNewUserDefined();
        
        /**
         * Removes the ith "UserDefined" element
         */
        void removeUserDefined(int i);
        
        /**
         * Gets the "ExternalData" element
         */
        com.idanalytics.products.idscore.request.ExternalData getExternalData();
        
        /**
         * True if has "ExternalData" element
         */
        boolean isSetExternalData();
        
        /**
         * Sets the "ExternalData" element
         */
        void setExternalData(com.idanalytics.products.idscore.request.ExternalData externalData);
        
        /**
         * Appends and returns a new empty "ExternalData" element
         */
        com.idanalytics.products.idscore.request.ExternalData addNewExternalData();
        
        /**
         * Unsets the "ExternalData" element
         */
        void unsetExternalData();
        
        /**
         * Gets the "ECommerce" element
         */
        com.idanalytics.products.idscore.request.ECommerce getECommerce();
        
        /**
         * True if has "ECommerce" element
         */
        boolean isSetECommerce();
        
        /**
         * Sets the "ECommerce" element
         */
        void setECommerce(com.idanalytics.products.idscore.request.ECommerce eCommerce);
        
        /**
         * Appends and returns a new empty "ECommerce" element
         */
        com.idanalytics.products.idscore.request.ECommerce addNewECommerce();
        
        /**
         * Unsets the "ECommerce" element
         */
        void unsetECommerce();
        
        /**
         * Gets the "IDAInternal" element
         */
        com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal getIDAInternal();
        
        /**
         * True if has "IDAInternal" element
         */
        boolean isSetIDAInternal();
        
        /**
         * Sets the "IDAInternal" element
         */
        void setIDAInternal(com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal idaInternal);
        
        /**
         * Appends and returns a new empty "IDAInternal" element
         */
        com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal addNewIDAInternal();
        
        /**
         * Unsets the "IDAInternal" element
         */
        void unsetIDAInternal();
        
        /**
         * Gets the "schemaVersion" attribute
         */
        java.math.BigDecimal getSchemaVersion();
        
        /**
         * Gets (as xml) the "schemaVersion" attribute
         */
        org.apache.xmlbeans.XmlDecimal xgetSchemaVersion();
        
        /**
         * True if has "schemaVersion" attribute
         */
        boolean isSetSchemaVersion();
        
        /**
         * Sets the "schemaVersion" attribute
         */
        void setSchemaVersion(java.math.BigDecimal schemaVersion);
        
        /**
         * Sets (as xml) the "schemaVersion" attribute
         */
        void xsetSchemaVersion(org.apache.xmlbeans.XmlDecimal schemaVersion);
        
        /**
         * Unsets the "schemaVersion" attribute
         */
        void unsetSchemaVersion();
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.idscore.request.IDScoreRequestDocument.IDScoreRequest newInstance() {
              return (com.idanalytics.products.idscore.request.IDScoreRequestDocument.IDScoreRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.idscore.request.IDScoreRequestDocument.IDScoreRequest newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.idscore.request.IDScoreRequestDocument.IDScoreRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.idscore.request.IDScoreRequestDocument newInstance() {
          return (com.idanalytics.products.idscore.request.IDScoreRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.idscore.request.IDScoreRequestDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.idscore.request.IDScoreRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.idscore.request.IDScoreRequestDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.IDScoreRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.idscore.request.IDScoreRequestDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.IDScoreRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.idscore.request.IDScoreRequestDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.IDScoreRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.idscore.request.IDScoreRequestDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.IDScoreRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.idscore.request.IDScoreRequestDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.IDScoreRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.idscore.request.IDScoreRequestDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.IDScoreRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.idscore.request.IDScoreRequestDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.IDScoreRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.idscore.request.IDScoreRequestDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.IDScoreRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.idscore.request.IDScoreRequestDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.IDScoreRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.idscore.request.IDScoreRequestDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.IDScoreRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.idscore.request.IDScoreRequestDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.IDScoreRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.idscore.request.IDScoreRequestDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.IDScoreRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.idscore.request.IDScoreRequestDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.IDScoreRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.idscore.request.IDScoreRequestDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.IDScoreRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.request.IDScoreRequestDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.request.IDScoreRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.request.IDScoreRequestDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.request.IDScoreRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
