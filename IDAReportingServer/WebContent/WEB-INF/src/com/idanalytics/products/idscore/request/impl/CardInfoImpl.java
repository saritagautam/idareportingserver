/*
 * XML Type:  CardInfo
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.CardInfo
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML CardInfo(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public class CardInfoImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.CardInfo
{
    private static final long serialVersionUID = 1L;
    
    public CardInfoImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CARDBRAND$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "CardBrand");
    private static final javax.xml.namespace.QName CARDNUMBER$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "CardNumber");
    private static final javax.xml.namespace.QName CARDTOKEN$4 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "CardToken");
    private static final javax.xml.namespace.QName CARDEXPIRATION$6 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "CardExpiration");
    
    
    /**
     * Gets the "CardBrand" element
     */
    public java.lang.String getCardBrand()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CARDBRAND$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "CardBrand" element
     */
    public com.idanalytics.products.idscore.request.CardBrandDocument.CardBrand xgetCardBrand()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.CardBrandDocument.CardBrand target = null;
            target = (com.idanalytics.products.idscore.request.CardBrandDocument.CardBrand)get_store().find_element_user(CARDBRAND$0, 0);
            return target;
        }
    }
    
    /**
     * True if has "CardBrand" element
     */
    public boolean isSetCardBrand()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CARDBRAND$0) != 0;
        }
    }
    
    /**
     * Sets the "CardBrand" element
     */
    public void setCardBrand(java.lang.String cardBrand)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CARDBRAND$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CARDBRAND$0);
            }
            target.setStringValue(cardBrand);
        }
    }
    
    /**
     * Sets (as xml) the "CardBrand" element
     */
    public void xsetCardBrand(com.idanalytics.products.idscore.request.CardBrandDocument.CardBrand cardBrand)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.CardBrandDocument.CardBrand target = null;
            target = (com.idanalytics.products.idscore.request.CardBrandDocument.CardBrand)get_store().find_element_user(CARDBRAND$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.CardBrandDocument.CardBrand)get_store().add_element_user(CARDBRAND$0);
            }
            target.set(cardBrand);
        }
    }
    
    /**
     * Unsets the "CardBrand" element
     */
    public void unsetCardBrand()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CARDBRAND$0, 0);
        }
    }
    
    /**
     * Gets the "CardNumber" element
     */
    public java.lang.String getCardNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CARDNUMBER$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "CardNumber" element
     */
    public com.idanalytics.products.idscore.request.CardNumberDocument.CardNumber xgetCardNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.CardNumberDocument.CardNumber target = null;
            target = (com.idanalytics.products.idscore.request.CardNumberDocument.CardNumber)get_store().find_element_user(CARDNUMBER$2, 0);
            return target;
        }
    }
    
    /**
     * True if has "CardNumber" element
     */
    public boolean isSetCardNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CARDNUMBER$2) != 0;
        }
    }
    
    /**
     * Sets the "CardNumber" element
     */
    public void setCardNumber(java.lang.String cardNumber)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CARDNUMBER$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CARDNUMBER$2);
            }
            target.setStringValue(cardNumber);
        }
    }
    
    /**
     * Sets (as xml) the "CardNumber" element
     */
    public void xsetCardNumber(com.idanalytics.products.idscore.request.CardNumberDocument.CardNumber cardNumber)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.CardNumberDocument.CardNumber target = null;
            target = (com.idanalytics.products.idscore.request.CardNumberDocument.CardNumber)get_store().find_element_user(CARDNUMBER$2, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.CardNumberDocument.CardNumber)get_store().add_element_user(CARDNUMBER$2);
            }
            target.set(cardNumber);
        }
    }
    
    /**
     * Unsets the "CardNumber" element
     */
    public void unsetCardNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CARDNUMBER$2, 0);
        }
    }
    
    /**
     * Gets the "CardToken" element
     */
    public java.lang.String getCardToken()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CARDTOKEN$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "CardToken" element
     */
    public com.idanalytics.products.common_v1.NormalizedString xgetCardToken()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.NormalizedString target = null;
            target = (com.idanalytics.products.common_v1.NormalizedString)get_store().find_element_user(CARDTOKEN$4, 0);
            return target;
        }
    }
    
    /**
     * True if has "CardToken" element
     */
    public boolean isSetCardToken()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CARDTOKEN$4) != 0;
        }
    }
    
    /**
     * Sets the "CardToken" element
     */
    public void setCardToken(java.lang.String cardToken)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CARDTOKEN$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CARDTOKEN$4);
            }
            target.setStringValue(cardToken);
        }
    }
    
    /**
     * Sets (as xml) the "CardToken" element
     */
    public void xsetCardToken(com.idanalytics.products.common_v1.NormalizedString cardToken)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.NormalizedString target = null;
            target = (com.idanalytics.products.common_v1.NormalizedString)get_store().find_element_user(CARDTOKEN$4, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.NormalizedString)get_store().add_element_user(CARDTOKEN$4);
            }
            target.set(cardToken);
        }
    }
    
    /**
     * Unsets the "CardToken" element
     */
    public void unsetCardToken()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CARDTOKEN$4, 0);
        }
    }
    
    /**
     * Gets the "CardExpiration" element
     */
    public java.lang.String getCardExpiration()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CARDEXPIRATION$6, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "CardExpiration" element
     */
    public com.idanalytics.products.idscore.request.ExpirationDate xgetCardExpiration()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ExpirationDate target = null;
            target = (com.idanalytics.products.idscore.request.ExpirationDate)get_store().find_element_user(CARDEXPIRATION$6, 0);
            return target;
        }
    }
    
    /**
     * True if has "CardExpiration" element
     */
    public boolean isSetCardExpiration()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CARDEXPIRATION$6) != 0;
        }
    }
    
    /**
     * Sets the "CardExpiration" element
     */
    public void setCardExpiration(java.lang.String cardExpiration)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CARDEXPIRATION$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CARDEXPIRATION$6);
            }
            target.setStringValue(cardExpiration);
        }
    }
    
    /**
     * Sets (as xml) the "CardExpiration" element
     */
    public void xsetCardExpiration(com.idanalytics.products.idscore.request.ExpirationDate cardExpiration)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ExpirationDate target = null;
            target = (com.idanalytics.products.idscore.request.ExpirationDate)get_store().find_element_user(CARDEXPIRATION$6, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.ExpirationDate)get_store().add_element_user(CARDEXPIRATION$6);
            }
            target.set(cardExpiration);
        }
    }
    
    /**
     * Unsets the "CardExpiration" element
     */
    public void unsetCardExpiration()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CARDEXPIRATION$6, 0);
        }
    }
}
