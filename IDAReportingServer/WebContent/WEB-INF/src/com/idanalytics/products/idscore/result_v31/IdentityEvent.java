/*
 * XML Type:  IdentityEvent
 * Namespace: http://idanalytics.com/products/idscore/result.v31
 * Java type: com.idanalytics.products.idscore.result_v31.IdentityEvent
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.result_v31;


/**
 * An XML IdentityEvent(@http://idanalytics.com/products/idscore/result.v31).
 *
 * This is a complex type.
 */
public interface IdentityEvent extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(IdentityEvent.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("identityeventc410type");
    
    /**
     * Gets the "EventID" element
     */
    java.lang.String getEventID();
    
    /**
     * Gets (as xml) the "EventID" element
     */
    com.idanalytics.products.idscore.result_v31.String30 xgetEventID();
    
    /**
     * True if has "EventID" element
     */
    boolean isSetEventID();
    
    /**
     * Sets the "EventID" element
     */
    void setEventID(java.lang.String eventID);
    
    /**
     * Sets (as xml) the "EventID" element
     */
    void xsetEventID(com.idanalytics.products.idscore.result_v31.String30 eventID);
    
    /**
     * Unsets the "EventID" element
     */
    void unsetEventID();
    
    /**
     * Gets the "CreditInactive" element
     */
    boolean getCreditInactive();
    
    /**
     * Gets (as xml) the "CreditInactive" element
     */
    org.apache.xmlbeans.XmlBoolean xgetCreditInactive();
    
    /**
     * True if has "CreditInactive" element
     */
    boolean isSetCreditInactive();
    
    /**
     * Sets the "CreditInactive" element
     */
    void setCreditInactive(boolean creditInactive);
    
    /**
     * Sets (as xml) the "CreditInactive" element
     */
    void xsetCreditInactive(org.apache.xmlbeans.XmlBoolean creditInactive);
    
    /**
     * Unsets the "CreditInactive" element
     */
    void unsetCreditInactive();
    
    /**
     * Gets the "CompanyName" element
     */
    java.lang.String getCompanyName();
    
    /**
     * Gets (as xml) the "CompanyName" element
     */
    com.idanalytics.products.idscore.result_v31.String50 xgetCompanyName();
    
    /**
     * Sets the "CompanyName" element
     */
    void setCompanyName(java.lang.String companyName);
    
    /**
     * Sets (as xml) the "CompanyName" element
     */
    void xsetCompanyName(com.idanalytics.products.idscore.result_v31.String50 companyName);
    
    /**
     * Gets the "EventType" element
     */
    java.lang.String getEventType();
    
    /**
     * Gets (as xml) the "EventType" element
     */
    com.idanalytics.products.idscore.result_v31.String50 xgetEventType();
    
    /**
     * True if has "EventType" element
     */
    boolean isSetEventType();
    
    /**
     * Sets the "EventType" element
     */
    void setEventType(java.lang.String eventType);
    
    /**
     * Sets (as xml) the "EventType" element
     */
    void xsetEventType(com.idanalytics.products.idscore.result_v31.String50 eventType);
    
    /**
     * Unsets the "EventType" element
     */
    void unsetEventType();
    
    /**
     * Gets the "IndustryType" element
     */
    java.lang.String getIndustryType();
    
    /**
     * Gets (as xml) the "IndustryType" element
     */
    com.idanalytics.products.idscore.result_v31.String50 xgetIndustryType();
    
    /**
     * True if has "IndustryType" element
     */
    boolean isSetIndustryType();
    
    /**
     * Sets the "IndustryType" element
     */
    void setIndustryType(java.lang.String industryType);
    
    /**
     * Sets (as xml) the "IndustryType" element
     */
    void xsetIndustryType(com.idanalytics.products.idscore.result_v31.String50 industryType);
    
    /**
     * Unsets the "IndustryType" element
     */
    void unsetIndustryType();
    
    /**
     * Gets the "Date" element
     */
    java.util.Calendar getDate();
    
    /**
     * Gets (as xml) the "Date" element
     */
    org.apache.xmlbeans.XmlDate xgetDate();
    
    /**
     * Sets the "Date" element
     */
    void setDate(java.util.Calendar date);
    
    /**
     * Sets (as xml) the "Date" element
     */
    void xsetDate(org.apache.xmlbeans.XmlDate date);
    
    /**
     * Gets the "SSNMatchIndicator" element
     */
    com.idanalytics.products.idscore.result_v31.MatchIndicator.Enum getSSNMatchIndicator();
    
    /**
     * Gets (as xml) the "SSNMatchIndicator" element
     */
    com.idanalytics.products.idscore.result_v31.MatchIndicator xgetSSNMatchIndicator();
    
    /**
     * Sets the "SSNMatchIndicator" element
     */
    void setSSNMatchIndicator(com.idanalytics.products.idscore.result_v31.MatchIndicator.Enum ssnMatchIndicator);
    
    /**
     * Sets (as xml) the "SSNMatchIndicator" element
     */
    void xsetSSNMatchIndicator(com.idanalytics.products.idscore.result_v31.MatchIndicator ssnMatchIndicator);
    
    /**
     * Gets the "NameMatchIndicator" element
     */
    com.idanalytics.products.idscore.result_v31.MatchIndicator.Enum getNameMatchIndicator();
    
    /**
     * Gets (as xml) the "NameMatchIndicator" element
     */
    com.idanalytics.products.idscore.result_v31.MatchIndicator xgetNameMatchIndicator();
    
    /**
     * Sets the "NameMatchIndicator" element
     */
    void setNameMatchIndicator(com.idanalytics.products.idscore.result_v31.MatchIndicator.Enum nameMatchIndicator);
    
    /**
     * Sets (as xml) the "NameMatchIndicator" element
     */
    void xsetNameMatchIndicator(com.idanalytics.products.idscore.result_v31.MatchIndicator nameMatchIndicator);
    
    /**
     * Gets the "DOBMatchIndicator" element
     */
    com.idanalytics.products.idscore.result_v31.MatchIndicator.Enum getDOBMatchIndicator();
    
    /**
     * Gets (as xml) the "DOBMatchIndicator" element
     */
    com.idanalytics.products.idscore.result_v31.MatchIndicator xgetDOBMatchIndicator();
    
    /**
     * Sets the "DOBMatchIndicator" element
     */
    void setDOBMatchIndicator(com.idanalytics.products.idscore.result_v31.MatchIndicator.Enum dobMatchIndicator);
    
    /**
     * Sets (as xml) the "DOBMatchIndicator" element
     */
    void xsetDOBMatchIndicator(com.idanalytics.products.idscore.result_v31.MatchIndicator dobMatchIndicator);
    
    /**
     * Gets the "PartialAddress" element
     */
    com.idanalytics.products.idscore.result_v31.IdentityEvent.PartialAddress getPartialAddress();
    
    /**
     * Sets the "PartialAddress" element
     */
    void setPartialAddress(com.idanalytics.products.idscore.result_v31.IdentityEvent.PartialAddress partialAddress);
    
    /**
     * Appends and returns a new empty "PartialAddress" element
     */
    com.idanalytics.products.idscore.result_v31.IdentityEvent.PartialAddress addNewPartialAddress();
    
    /**
     * Gets the "PartialHomePhone" element
     */
    com.idanalytics.products.idscore.result_v31.IdentityEvent.PartialHomePhone getPartialHomePhone();
    
    /**
     * Sets the "PartialHomePhone" element
     */
    void setPartialHomePhone(com.idanalytics.products.idscore.result_v31.IdentityEvent.PartialHomePhone partialHomePhone);
    
    /**
     * Appends and returns a new empty "PartialHomePhone" element
     */
    com.idanalytics.products.idscore.result_v31.IdentityEvent.PartialHomePhone addNewPartialHomePhone();
    
    /**
     * Gets the "ReportedFraud" element
     */
    com.idanalytics.products.idscore.result_v31.MatchIndicator.Enum getReportedFraud();
    
    /**
     * Gets (as xml) the "ReportedFraud" element
     */
    com.idanalytics.products.idscore.result_v31.MatchIndicator xgetReportedFraud();
    
    /**
     * Sets the "ReportedFraud" element
     */
    void setReportedFraud(com.idanalytics.products.idscore.result_v31.MatchIndicator.Enum reportedFraud);
    
    /**
     * Sets (as xml) the "ReportedFraud" element
     */
    void xsetReportedFraud(com.idanalytics.products.idscore.result_v31.MatchIndicator reportedFraud);
    
    /**
     * An XML PartialAddress(@http://idanalytics.com/products/idscore/result.v31).
     *
     * This is a complex type.
     */
    public interface PartialAddress extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(PartialAddress.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("partialaddressefc1elemtype");
        
        /**
         * Gets the "Address" element
         */
        java.lang.String getAddress();
        
        /**
         * Gets (as xml) the "Address" element
         */
        com.idanalytics.products.idscore.result_v31.String50 xgetAddress();
        
        /**
         * Sets the "Address" element
         */
        void setAddress(java.lang.String address);
        
        /**
         * Sets (as xml) the "Address" element
         */
        void xsetAddress(com.idanalytics.products.idscore.result_v31.String50 address);
        
        /**
         * Gets the "City" element
         */
        java.lang.String getCity();
        
        /**
         * Gets (as xml) the "City" element
         */
        com.idanalytics.products.idscore.result_v31.String50 xgetCity();
        
        /**
         * Sets the "City" element
         */
        void setCity(java.lang.String city);
        
        /**
         * Sets (as xml) the "City" element
         */
        void xsetCity(com.idanalytics.products.idscore.result_v31.String50 city);
        
        /**
         * Gets the "Zip" element
         */
        java.lang.String getZip();
        
        /**
         * Gets (as xml) the "Zip" element
         */
        com.idanalytics.products.idscore.result_v31.String5Z xgetZip();
        
        /**
         * Sets the "Zip" element
         */
        void setZip(java.lang.String zip);
        
        /**
         * Sets (as xml) the "Zip" element
         */
        void xsetZip(com.idanalytics.products.idscore.result_v31.String5Z zip);
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.idscore.result_v31.IdentityEvent.PartialAddress newInstance() {
              return (com.idanalytics.products.idscore.result_v31.IdentityEvent.PartialAddress) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.idscore.result_v31.IdentityEvent.PartialAddress newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.idscore.result_v31.IdentityEvent.PartialAddress) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * An XML PartialHomePhone(@http://idanalytics.com/products/idscore/result.v31).
     *
     * This is a complex type.
     */
    public interface PartialHomePhone extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(PartialHomePhone.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("partialhomephone7fc6elemtype");
        
        /**
         * Gets the "NPA" element
         */
        java.lang.String getNPA();
        
        /**
         * Gets (as xml) the "NPA" element
         */
        com.idanalytics.products.idscore.result_v31.String3Z xgetNPA();
        
        /**
         * Sets the "NPA" element
         */
        void setNPA(java.lang.String npa);
        
        /**
         * Sets (as xml) the "NPA" element
         */
        void xsetNPA(com.idanalytics.products.idscore.result_v31.String3Z npa);
        
        /**
         * Gets the "NXX" element
         */
        java.lang.String getNXX();
        
        /**
         * Gets (as xml) the "NXX" element
         */
        com.idanalytics.products.idscore.result_v31.String3Z xgetNXX();
        
        /**
         * Sets the "NXX" element
         */
        void setNXX(java.lang.String nxx);
        
        /**
         * Sets (as xml) the "NXX" element
         */
        void xsetNXX(com.idanalytics.products.idscore.result_v31.String3Z nxx);
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.idscore.result_v31.IdentityEvent.PartialHomePhone newInstance() {
              return (com.idanalytics.products.idscore.result_v31.IdentityEvent.PartialHomePhone) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.idscore.result_v31.IdentityEvent.PartialHomePhone newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.idscore.result_v31.IdentityEvent.PartialHomePhone) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.idscore.result_v31.IdentityEvent newInstance() {
          return (com.idanalytics.products.idscore.result_v31.IdentityEvent) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.idscore.result_v31.IdentityEvent newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.idscore.result_v31.IdentityEvent) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.idscore.result_v31.IdentityEvent parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result_v31.IdentityEvent) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.idscore.result_v31.IdentityEvent parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result_v31.IdentityEvent) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.idscore.result_v31.IdentityEvent parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result_v31.IdentityEvent) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.idscore.result_v31.IdentityEvent parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result_v31.IdentityEvent) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.idscore.result_v31.IdentityEvent parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result_v31.IdentityEvent) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.idscore.result_v31.IdentityEvent parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result_v31.IdentityEvent) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.idscore.result_v31.IdentityEvent parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result_v31.IdentityEvent) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.idscore.result_v31.IdentityEvent parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result_v31.IdentityEvent) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.idscore.result_v31.IdentityEvent parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result_v31.IdentityEvent) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.idscore.result_v31.IdentityEvent parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result_v31.IdentityEvent) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.idscore.result_v31.IdentityEvent parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result_v31.IdentityEvent) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.idscore.result_v31.IdentityEvent parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result_v31.IdentityEvent) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.idscore.result_v31.IdentityEvent parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result_v31.IdentityEvent) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.idscore.result_v31.IdentityEvent parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result_v31.IdentityEvent) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.result_v31.IdentityEvent parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.result_v31.IdentityEvent) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.result_v31.IdentityEvent parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.result_v31.IdentityEvent) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
