/*
 * XML Type:  ChangeRequestSource
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.ChangeRequestSource
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML ChangeRequestSource(@http://idanalytics.com/products/idscore/request).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.ChangeRequestSource.
 */
public class ChangeRequestSourceImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.idanalytics.products.idscore.request.ChangeRequestSource
{
    private static final long serialVersionUID = 1L;
    
    public ChangeRequestSourceImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected ChangeRequestSourceImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
