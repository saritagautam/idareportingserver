/*
 * An XML document type.
 * Localname: AgentID
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.AgentIDDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * A document containing one AgentID(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public class AgentIDDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.AgentIDDocument
{
    private static final long serialVersionUID = 1L;
    
    public AgentIDDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName AGENTID$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "AgentID");
    
    
    /**
     * Gets the "AgentID" element
     */
    public java.lang.String getAgentID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(AGENTID$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "AgentID" element
     */
    public com.idanalytics.products.idscore.request.AgentIDDocument.AgentID xgetAgentID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.AgentIDDocument.AgentID target = null;
            target = (com.idanalytics.products.idscore.request.AgentIDDocument.AgentID)get_store().find_element_user(AGENTID$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "AgentID" element
     */
    public void setAgentID(java.lang.String agentID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(AGENTID$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(AGENTID$0);
            }
            target.setStringValue(agentID);
        }
    }
    
    /**
     * Sets (as xml) the "AgentID" element
     */
    public void xsetAgentID(com.idanalytics.products.idscore.request.AgentIDDocument.AgentID agentID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.AgentIDDocument.AgentID target = null;
            target = (com.idanalytics.products.idscore.request.AgentIDDocument.AgentID)get_store().find_element_user(AGENTID$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.AgentIDDocument.AgentID)get_store().add_element_user(AGENTID$0);
            }
            target.set(agentID);
        }
    }
    /**
     * An XML AgentID(@http://idanalytics.com/products/idscore/request).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.AgentIDDocument$AgentID.
     */
    public static class AgentIDImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.request.AgentIDDocument.AgentID
    {
        private static final long serialVersionUID = 1L;
        
        public AgentIDImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected AgentIDImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
