/*
 * XML Type:  UserDefined
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.UserDefined
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML UserDefined(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public class UserDefinedImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.UserDefined
{
    private static final long serialVersionUID = 1L;
    
    public UserDefinedImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName UDF1$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "udf1");
    private static final javax.xml.namespace.QName UDF2$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "udf2");
    private static final javax.xml.namespace.QName UDF3$4 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "udf3");
    private static final javax.xml.namespace.QName UDF4$6 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "udf4");
    private static final javax.xml.namespace.QName UDF5$8 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "udf5");
    private static final javax.xml.namespace.QName UDF6$10 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "udf6");
    private static final javax.xml.namespace.QName UDF7$12 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "udf7");
    private static final javax.xml.namespace.QName UDF8$14 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "udf8");
    private static final javax.xml.namespace.QName UDF9$16 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "udf9");
    private static final javax.xml.namespace.QName UDF10$18 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "udf10");
    private static final javax.xml.namespace.QName UDF11$20 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "udf11");
    private static final javax.xml.namespace.QName UDF12$22 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "udf12");
    private static final javax.xml.namespace.QName UDF13$24 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "udf13");
    private static final javax.xml.namespace.QName UDF14$26 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "udf14");
    private static final javax.xml.namespace.QName UDF15$28 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "udf15");
    private static final javax.xml.namespace.QName UDF16$30 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "udf16");
    private static final javax.xml.namespace.QName UDF17$32 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "udf17");
    private static final javax.xml.namespace.QName UDF18$34 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "udf18");
    private static final javax.xml.namespace.QName UDF19$36 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "udf19");
    private static final javax.xml.namespace.QName UDF20$38 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "udf20");
    private static final javax.xml.namespace.QName UDF21$40 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "udf21");
    private static final javax.xml.namespace.QName UDF22$42 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "udf22");
    private static final javax.xml.namespace.QName UDF23$44 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "udf23");
    private static final javax.xml.namespace.QName UDF24$46 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "udf24");
    private static final javax.xml.namespace.QName UDF25$48 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "udf25");
    
    
    /**
     * Gets the "udf1" element
     */
    public java.lang.String getUdf1()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF1$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "udf1" element
     */
    public com.idanalytics.products.idscore.request.UserDefinedField xgetUdf1()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF1$0, 0);
            return target;
        }
    }
    
    /**
     * True if has "udf1" element
     */
    public boolean isSetUdf1()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(UDF1$0) != 0;
        }
    }
    
    /**
     * Sets the "udf1" element
     */
    public void setUdf1(java.lang.String udf1)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF1$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(UDF1$0);
            }
            target.setStringValue(udf1);
        }
    }
    
    /**
     * Sets (as xml) the "udf1" element
     */
    public void xsetUdf1(com.idanalytics.products.idscore.request.UserDefinedField udf1)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF1$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().add_element_user(UDF1$0);
            }
            target.set(udf1);
        }
    }
    
    /**
     * Unsets the "udf1" element
     */
    public void unsetUdf1()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(UDF1$0, 0);
        }
    }
    
    /**
     * Gets the "udf2" element
     */
    public java.lang.String getUdf2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF2$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "udf2" element
     */
    public com.idanalytics.products.idscore.request.UserDefinedField xgetUdf2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF2$2, 0);
            return target;
        }
    }
    
    /**
     * True if has "udf2" element
     */
    public boolean isSetUdf2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(UDF2$2) != 0;
        }
    }
    
    /**
     * Sets the "udf2" element
     */
    public void setUdf2(java.lang.String udf2)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF2$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(UDF2$2);
            }
            target.setStringValue(udf2);
        }
    }
    
    /**
     * Sets (as xml) the "udf2" element
     */
    public void xsetUdf2(com.idanalytics.products.idscore.request.UserDefinedField udf2)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF2$2, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().add_element_user(UDF2$2);
            }
            target.set(udf2);
        }
    }
    
    /**
     * Unsets the "udf2" element
     */
    public void unsetUdf2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(UDF2$2, 0);
        }
    }
    
    /**
     * Gets the "udf3" element
     */
    public java.lang.String getUdf3()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF3$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "udf3" element
     */
    public com.idanalytics.products.idscore.request.UserDefinedField xgetUdf3()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF3$4, 0);
            return target;
        }
    }
    
    /**
     * True if has "udf3" element
     */
    public boolean isSetUdf3()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(UDF3$4) != 0;
        }
    }
    
    /**
     * Sets the "udf3" element
     */
    public void setUdf3(java.lang.String udf3)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF3$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(UDF3$4);
            }
            target.setStringValue(udf3);
        }
    }
    
    /**
     * Sets (as xml) the "udf3" element
     */
    public void xsetUdf3(com.idanalytics.products.idscore.request.UserDefinedField udf3)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF3$4, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().add_element_user(UDF3$4);
            }
            target.set(udf3);
        }
    }
    
    /**
     * Unsets the "udf3" element
     */
    public void unsetUdf3()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(UDF3$4, 0);
        }
    }
    
    /**
     * Gets the "udf4" element
     */
    public java.lang.String getUdf4()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF4$6, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "udf4" element
     */
    public com.idanalytics.products.idscore.request.UserDefinedField xgetUdf4()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF4$6, 0);
            return target;
        }
    }
    
    /**
     * True if has "udf4" element
     */
    public boolean isSetUdf4()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(UDF4$6) != 0;
        }
    }
    
    /**
     * Sets the "udf4" element
     */
    public void setUdf4(java.lang.String udf4)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF4$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(UDF4$6);
            }
            target.setStringValue(udf4);
        }
    }
    
    /**
     * Sets (as xml) the "udf4" element
     */
    public void xsetUdf4(com.idanalytics.products.idscore.request.UserDefinedField udf4)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF4$6, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().add_element_user(UDF4$6);
            }
            target.set(udf4);
        }
    }
    
    /**
     * Unsets the "udf4" element
     */
    public void unsetUdf4()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(UDF4$6, 0);
        }
    }
    
    /**
     * Gets the "udf5" element
     */
    public java.lang.String getUdf5()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF5$8, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "udf5" element
     */
    public com.idanalytics.products.idscore.request.UserDefinedField xgetUdf5()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF5$8, 0);
            return target;
        }
    }
    
    /**
     * True if has "udf5" element
     */
    public boolean isSetUdf5()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(UDF5$8) != 0;
        }
    }
    
    /**
     * Sets the "udf5" element
     */
    public void setUdf5(java.lang.String udf5)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF5$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(UDF5$8);
            }
            target.setStringValue(udf5);
        }
    }
    
    /**
     * Sets (as xml) the "udf5" element
     */
    public void xsetUdf5(com.idanalytics.products.idscore.request.UserDefinedField udf5)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF5$8, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().add_element_user(UDF5$8);
            }
            target.set(udf5);
        }
    }
    
    /**
     * Unsets the "udf5" element
     */
    public void unsetUdf5()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(UDF5$8, 0);
        }
    }
    
    /**
     * Gets the "udf6" element
     */
    public java.lang.String getUdf6()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF6$10, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "udf6" element
     */
    public com.idanalytics.products.idscore.request.UserDefinedField xgetUdf6()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF6$10, 0);
            return target;
        }
    }
    
    /**
     * True if has "udf6" element
     */
    public boolean isSetUdf6()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(UDF6$10) != 0;
        }
    }
    
    /**
     * Sets the "udf6" element
     */
    public void setUdf6(java.lang.String udf6)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF6$10, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(UDF6$10);
            }
            target.setStringValue(udf6);
        }
    }
    
    /**
     * Sets (as xml) the "udf6" element
     */
    public void xsetUdf6(com.idanalytics.products.idscore.request.UserDefinedField udf6)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF6$10, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().add_element_user(UDF6$10);
            }
            target.set(udf6);
        }
    }
    
    /**
     * Unsets the "udf6" element
     */
    public void unsetUdf6()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(UDF6$10, 0);
        }
    }
    
    /**
     * Gets the "udf7" element
     */
    public java.lang.String getUdf7()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF7$12, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "udf7" element
     */
    public com.idanalytics.products.idscore.request.UserDefinedField xgetUdf7()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF7$12, 0);
            return target;
        }
    }
    
    /**
     * True if has "udf7" element
     */
    public boolean isSetUdf7()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(UDF7$12) != 0;
        }
    }
    
    /**
     * Sets the "udf7" element
     */
    public void setUdf7(java.lang.String udf7)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF7$12, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(UDF7$12);
            }
            target.setStringValue(udf7);
        }
    }
    
    /**
     * Sets (as xml) the "udf7" element
     */
    public void xsetUdf7(com.idanalytics.products.idscore.request.UserDefinedField udf7)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF7$12, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().add_element_user(UDF7$12);
            }
            target.set(udf7);
        }
    }
    
    /**
     * Unsets the "udf7" element
     */
    public void unsetUdf7()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(UDF7$12, 0);
        }
    }
    
    /**
     * Gets the "udf8" element
     */
    public java.lang.String getUdf8()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF8$14, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "udf8" element
     */
    public com.idanalytics.products.idscore.request.UserDefinedField xgetUdf8()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF8$14, 0);
            return target;
        }
    }
    
    /**
     * True if has "udf8" element
     */
    public boolean isSetUdf8()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(UDF8$14) != 0;
        }
    }
    
    /**
     * Sets the "udf8" element
     */
    public void setUdf8(java.lang.String udf8)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF8$14, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(UDF8$14);
            }
            target.setStringValue(udf8);
        }
    }
    
    /**
     * Sets (as xml) the "udf8" element
     */
    public void xsetUdf8(com.idanalytics.products.idscore.request.UserDefinedField udf8)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF8$14, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().add_element_user(UDF8$14);
            }
            target.set(udf8);
        }
    }
    
    /**
     * Unsets the "udf8" element
     */
    public void unsetUdf8()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(UDF8$14, 0);
        }
    }
    
    /**
     * Gets the "udf9" element
     */
    public java.lang.String getUdf9()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF9$16, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "udf9" element
     */
    public com.idanalytics.products.idscore.request.UserDefinedField xgetUdf9()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF9$16, 0);
            return target;
        }
    }
    
    /**
     * True if has "udf9" element
     */
    public boolean isSetUdf9()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(UDF9$16) != 0;
        }
    }
    
    /**
     * Sets the "udf9" element
     */
    public void setUdf9(java.lang.String udf9)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF9$16, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(UDF9$16);
            }
            target.setStringValue(udf9);
        }
    }
    
    /**
     * Sets (as xml) the "udf9" element
     */
    public void xsetUdf9(com.idanalytics.products.idscore.request.UserDefinedField udf9)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF9$16, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().add_element_user(UDF9$16);
            }
            target.set(udf9);
        }
    }
    
    /**
     * Unsets the "udf9" element
     */
    public void unsetUdf9()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(UDF9$16, 0);
        }
    }
    
    /**
     * Gets the "udf10" element
     */
    public java.lang.String getUdf10()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF10$18, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "udf10" element
     */
    public com.idanalytics.products.idscore.request.UserDefinedField xgetUdf10()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF10$18, 0);
            return target;
        }
    }
    
    /**
     * True if has "udf10" element
     */
    public boolean isSetUdf10()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(UDF10$18) != 0;
        }
    }
    
    /**
     * Sets the "udf10" element
     */
    public void setUdf10(java.lang.String udf10)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF10$18, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(UDF10$18);
            }
            target.setStringValue(udf10);
        }
    }
    
    /**
     * Sets (as xml) the "udf10" element
     */
    public void xsetUdf10(com.idanalytics.products.idscore.request.UserDefinedField udf10)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF10$18, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().add_element_user(UDF10$18);
            }
            target.set(udf10);
        }
    }
    
    /**
     * Unsets the "udf10" element
     */
    public void unsetUdf10()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(UDF10$18, 0);
        }
    }
    
    /**
     * Gets the "udf11" element
     */
    public java.lang.String getUdf11()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF11$20, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "udf11" element
     */
    public com.idanalytics.products.idscore.request.UserDefinedField xgetUdf11()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF11$20, 0);
            return target;
        }
    }
    
    /**
     * True if has "udf11" element
     */
    public boolean isSetUdf11()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(UDF11$20) != 0;
        }
    }
    
    /**
     * Sets the "udf11" element
     */
    public void setUdf11(java.lang.String udf11)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF11$20, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(UDF11$20);
            }
            target.setStringValue(udf11);
        }
    }
    
    /**
     * Sets (as xml) the "udf11" element
     */
    public void xsetUdf11(com.idanalytics.products.idscore.request.UserDefinedField udf11)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF11$20, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().add_element_user(UDF11$20);
            }
            target.set(udf11);
        }
    }
    
    /**
     * Unsets the "udf11" element
     */
    public void unsetUdf11()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(UDF11$20, 0);
        }
    }
    
    /**
     * Gets the "udf12" element
     */
    public java.lang.String getUdf12()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF12$22, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "udf12" element
     */
    public com.idanalytics.products.idscore.request.UserDefinedField xgetUdf12()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF12$22, 0);
            return target;
        }
    }
    
    /**
     * True if has "udf12" element
     */
    public boolean isSetUdf12()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(UDF12$22) != 0;
        }
    }
    
    /**
     * Sets the "udf12" element
     */
    public void setUdf12(java.lang.String udf12)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF12$22, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(UDF12$22);
            }
            target.setStringValue(udf12);
        }
    }
    
    /**
     * Sets (as xml) the "udf12" element
     */
    public void xsetUdf12(com.idanalytics.products.idscore.request.UserDefinedField udf12)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF12$22, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().add_element_user(UDF12$22);
            }
            target.set(udf12);
        }
    }
    
    /**
     * Unsets the "udf12" element
     */
    public void unsetUdf12()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(UDF12$22, 0);
        }
    }
    
    /**
     * Gets the "udf13" element
     */
    public java.lang.String getUdf13()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF13$24, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "udf13" element
     */
    public com.idanalytics.products.idscore.request.UserDefinedField xgetUdf13()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF13$24, 0);
            return target;
        }
    }
    
    /**
     * True if has "udf13" element
     */
    public boolean isSetUdf13()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(UDF13$24) != 0;
        }
    }
    
    /**
     * Sets the "udf13" element
     */
    public void setUdf13(java.lang.String udf13)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF13$24, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(UDF13$24);
            }
            target.setStringValue(udf13);
        }
    }
    
    /**
     * Sets (as xml) the "udf13" element
     */
    public void xsetUdf13(com.idanalytics.products.idscore.request.UserDefinedField udf13)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF13$24, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().add_element_user(UDF13$24);
            }
            target.set(udf13);
        }
    }
    
    /**
     * Unsets the "udf13" element
     */
    public void unsetUdf13()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(UDF13$24, 0);
        }
    }
    
    /**
     * Gets the "udf14" element
     */
    public java.lang.String getUdf14()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF14$26, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "udf14" element
     */
    public com.idanalytics.products.idscore.request.UserDefinedField xgetUdf14()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF14$26, 0);
            return target;
        }
    }
    
    /**
     * True if has "udf14" element
     */
    public boolean isSetUdf14()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(UDF14$26) != 0;
        }
    }
    
    /**
     * Sets the "udf14" element
     */
    public void setUdf14(java.lang.String udf14)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF14$26, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(UDF14$26);
            }
            target.setStringValue(udf14);
        }
    }
    
    /**
     * Sets (as xml) the "udf14" element
     */
    public void xsetUdf14(com.idanalytics.products.idscore.request.UserDefinedField udf14)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF14$26, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().add_element_user(UDF14$26);
            }
            target.set(udf14);
        }
    }
    
    /**
     * Unsets the "udf14" element
     */
    public void unsetUdf14()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(UDF14$26, 0);
        }
    }
    
    /**
     * Gets the "udf15" element
     */
    public java.lang.String getUdf15()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF15$28, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "udf15" element
     */
    public com.idanalytics.products.idscore.request.UserDefinedField xgetUdf15()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF15$28, 0);
            return target;
        }
    }
    
    /**
     * True if has "udf15" element
     */
    public boolean isSetUdf15()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(UDF15$28) != 0;
        }
    }
    
    /**
     * Sets the "udf15" element
     */
    public void setUdf15(java.lang.String udf15)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF15$28, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(UDF15$28);
            }
            target.setStringValue(udf15);
        }
    }
    
    /**
     * Sets (as xml) the "udf15" element
     */
    public void xsetUdf15(com.idanalytics.products.idscore.request.UserDefinedField udf15)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF15$28, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().add_element_user(UDF15$28);
            }
            target.set(udf15);
        }
    }
    
    /**
     * Unsets the "udf15" element
     */
    public void unsetUdf15()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(UDF15$28, 0);
        }
    }
    
    /**
     * Gets the "udf16" element
     */
    public java.lang.String getUdf16()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF16$30, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "udf16" element
     */
    public com.idanalytics.products.idscore.request.UserDefinedField xgetUdf16()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF16$30, 0);
            return target;
        }
    }
    
    /**
     * True if has "udf16" element
     */
    public boolean isSetUdf16()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(UDF16$30) != 0;
        }
    }
    
    /**
     * Sets the "udf16" element
     */
    public void setUdf16(java.lang.String udf16)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF16$30, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(UDF16$30);
            }
            target.setStringValue(udf16);
        }
    }
    
    /**
     * Sets (as xml) the "udf16" element
     */
    public void xsetUdf16(com.idanalytics.products.idscore.request.UserDefinedField udf16)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF16$30, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().add_element_user(UDF16$30);
            }
            target.set(udf16);
        }
    }
    
    /**
     * Unsets the "udf16" element
     */
    public void unsetUdf16()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(UDF16$30, 0);
        }
    }
    
    /**
     * Gets the "udf17" element
     */
    public java.lang.String getUdf17()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF17$32, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "udf17" element
     */
    public com.idanalytics.products.idscore.request.UserDefinedField xgetUdf17()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF17$32, 0);
            return target;
        }
    }
    
    /**
     * True if has "udf17" element
     */
    public boolean isSetUdf17()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(UDF17$32) != 0;
        }
    }
    
    /**
     * Sets the "udf17" element
     */
    public void setUdf17(java.lang.String udf17)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF17$32, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(UDF17$32);
            }
            target.setStringValue(udf17);
        }
    }
    
    /**
     * Sets (as xml) the "udf17" element
     */
    public void xsetUdf17(com.idanalytics.products.idscore.request.UserDefinedField udf17)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF17$32, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().add_element_user(UDF17$32);
            }
            target.set(udf17);
        }
    }
    
    /**
     * Unsets the "udf17" element
     */
    public void unsetUdf17()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(UDF17$32, 0);
        }
    }
    
    /**
     * Gets the "udf18" element
     */
    public java.lang.String getUdf18()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF18$34, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "udf18" element
     */
    public com.idanalytics.products.idscore.request.UserDefinedField xgetUdf18()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF18$34, 0);
            return target;
        }
    }
    
    /**
     * True if has "udf18" element
     */
    public boolean isSetUdf18()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(UDF18$34) != 0;
        }
    }
    
    /**
     * Sets the "udf18" element
     */
    public void setUdf18(java.lang.String udf18)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF18$34, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(UDF18$34);
            }
            target.setStringValue(udf18);
        }
    }
    
    /**
     * Sets (as xml) the "udf18" element
     */
    public void xsetUdf18(com.idanalytics.products.idscore.request.UserDefinedField udf18)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF18$34, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().add_element_user(UDF18$34);
            }
            target.set(udf18);
        }
    }
    
    /**
     * Unsets the "udf18" element
     */
    public void unsetUdf18()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(UDF18$34, 0);
        }
    }
    
    /**
     * Gets the "udf19" element
     */
    public java.lang.String getUdf19()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF19$36, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "udf19" element
     */
    public com.idanalytics.products.idscore.request.UserDefinedField xgetUdf19()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF19$36, 0);
            return target;
        }
    }
    
    /**
     * True if has "udf19" element
     */
    public boolean isSetUdf19()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(UDF19$36) != 0;
        }
    }
    
    /**
     * Sets the "udf19" element
     */
    public void setUdf19(java.lang.String udf19)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF19$36, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(UDF19$36);
            }
            target.setStringValue(udf19);
        }
    }
    
    /**
     * Sets (as xml) the "udf19" element
     */
    public void xsetUdf19(com.idanalytics.products.idscore.request.UserDefinedField udf19)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF19$36, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().add_element_user(UDF19$36);
            }
            target.set(udf19);
        }
    }
    
    /**
     * Unsets the "udf19" element
     */
    public void unsetUdf19()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(UDF19$36, 0);
        }
    }
    
    /**
     * Gets the "udf20" element
     */
    public java.lang.String getUdf20()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF20$38, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "udf20" element
     */
    public com.idanalytics.products.idscore.request.UserDefinedField xgetUdf20()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF20$38, 0);
            return target;
        }
    }
    
    /**
     * True if has "udf20" element
     */
    public boolean isSetUdf20()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(UDF20$38) != 0;
        }
    }
    
    /**
     * Sets the "udf20" element
     */
    public void setUdf20(java.lang.String udf20)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF20$38, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(UDF20$38);
            }
            target.setStringValue(udf20);
        }
    }
    
    /**
     * Sets (as xml) the "udf20" element
     */
    public void xsetUdf20(com.idanalytics.products.idscore.request.UserDefinedField udf20)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF20$38, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().add_element_user(UDF20$38);
            }
            target.set(udf20);
        }
    }
    
    /**
     * Unsets the "udf20" element
     */
    public void unsetUdf20()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(UDF20$38, 0);
        }
    }
    
    /**
     * Gets the "udf21" element
     */
    public java.lang.String getUdf21()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF21$40, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "udf21" element
     */
    public com.idanalytics.products.idscore.request.UserDefinedField xgetUdf21()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF21$40, 0);
            return target;
        }
    }
    
    /**
     * True if has "udf21" element
     */
    public boolean isSetUdf21()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(UDF21$40) != 0;
        }
    }
    
    /**
     * Sets the "udf21" element
     */
    public void setUdf21(java.lang.String udf21)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF21$40, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(UDF21$40);
            }
            target.setStringValue(udf21);
        }
    }
    
    /**
     * Sets (as xml) the "udf21" element
     */
    public void xsetUdf21(com.idanalytics.products.idscore.request.UserDefinedField udf21)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF21$40, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().add_element_user(UDF21$40);
            }
            target.set(udf21);
        }
    }
    
    /**
     * Unsets the "udf21" element
     */
    public void unsetUdf21()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(UDF21$40, 0);
        }
    }
    
    /**
     * Gets the "udf22" element
     */
    public java.lang.String getUdf22()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF22$42, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "udf22" element
     */
    public com.idanalytics.products.idscore.request.UserDefinedField xgetUdf22()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF22$42, 0);
            return target;
        }
    }
    
    /**
     * True if has "udf22" element
     */
    public boolean isSetUdf22()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(UDF22$42) != 0;
        }
    }
    
    /**
     * Sets the "udf22" element
     */
    public void setUdf22(java.lang.String udf22)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF22$42, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(UDF22$42);
            }
            target.setStringValue(udf22);
        }
    }
    
    /**
     * Sets (as xml) the "udf22" element
     */
    public void xsetUdf22(com.idanalytics.products.idscore.request.UserDefinedField udf22)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF22$42, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().add_element_user(UDF22$42);
            }
            target.set(udf22);
        }
    }
    
    /**
     * Unsets the "udf22" element
     */
    public void unsetUdf22()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(UDF22$42, 0);
        }
    }
    
    /**
     * Gets the "udf23" element
     */
    public java.lang.String getUdf23()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF23$44, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "udf23" element
     */
    public com.idanalytics.products.idscore.request.UserDefinedField xgetUdf23()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF23$44, 0);
            return target;
        }
    }
    
    /**
     * True if has "udf23" element
     */
    public boolean isSetUdf23()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(UDF23$44) != 0;
        }
    }
    
    /**
     * Sets the "udf23" element
     */
    public void setUdf23(java.lang.String udf23)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF23$44, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(UDF23$44);
            }
            target.setStringValue(udf23);
        }
    }
    
    /**
     * Sets (as xml) the "udf23" element
     */
    public void xsetUdf23(com.idanalytics.products.idscore.request.UserDefinedField udf23)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF23$44, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().add_element_user(UDF23$44);
            }
            target.set(udf23);
        }
    }
    
    /**
     * Unsets the "udf23" element
     */
    public void unsetUdf23()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(UDF23$44, 0);
        }
    }
    
    /**
     * Gets the "udf24" element
     */
    public java.lang.String getUdf24()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF24$46, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "udf24" element
     */
    public com.idanalytics.products.idscore.request.UserDefinedField xgetUdf24()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF24$46, 0);
            return target;
        }
    }
    
    /**
     * True if has "udf24" element
     */
    public boolean isSetUdf24()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(UDF24$46) != 0;
        }
    }
    
    /**
     * Sets the "udf24" element
     */
    public void setUdf24(java.lang.String udf24)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF24$46, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(UDF24$46);
            }
            target.setStringValue(udf24);
        }
    }
    
    /**
     * Sets (as xml) the "udf24" element
     */
    public void xsetUdf24(com.idanalytics.products.idscore.request.UserDefinedField udf24)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF24$46, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().add_element_user(UDF24$46);
            }
            target.set(udf24);
        }
    }
    
    /**
     * Unsets the "udf24" element
     */
    public void unsetUdf24()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(UDF24$46, 0);
        }
    }
    
    /**
     * Gets the "udf25" element
     */
    public java.lang.String getUdf25()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF25$48, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "udf25" element
     */
    public com.idanalytics.products.idscore.request.UserDefinedField xgetUdf25()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF25$48, 0);
            return target;
        }
    }
    
    /**
     * True if has "udf25" element
     */
    public boolean isSetUdf25()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(UDF25$48) != 0;
        }
    }
    
    /**
     * Sets the "udf25" element
     */
    public void setUdf25(java.lang.String udf25)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(UDF25$48, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(UDF25$48);
            }
            target.setStringValue(udf25);
        }
    }
    
    /**
     * Sets (as xml) the "udf25" element
     */
    public void xsetUdf25(com.idanalytics.products.idscore.request.UserDefinedField udf25)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.UserDefinedField target = null;
            target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().find_element_user(UDF25$48, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.UserDefinedField)get_store().add_element_user(UDF25$48);
            }
            target.set(udf25);
        }
    }
    
    /**
     * Unsets the "udf25" element
     */
    public void unsetUdf25()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(UDF25$48, 0);
        }
    }
}
