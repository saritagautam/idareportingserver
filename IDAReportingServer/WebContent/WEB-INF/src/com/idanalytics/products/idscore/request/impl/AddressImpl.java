/*
 * XML Type:  Address
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.Address
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML Address(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public class AddressImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.Address
{
    private static final long serialVersionUID = 1L;
    
    public AddressImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName LINE1$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Line1");
    private static final javax.xml.namespace.QName LINE2$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Line2");
    
    
    /**
     * Gets the "Line1" element
     */
    public java.lang.String getLine1()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LINE1$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Line1" element
     */
    public com.idanalytics.products.idscore.request.AddressLine xgetLine1()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.AddressLine target = null;
            target = (com.idanalytics.products.idscore.request.AddressLine)get_store().find_element_user(LINE1$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Line1" element
     */
    public void setLine1(java.lang.String line1)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LINE1$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(LINE1$0);
            }
            target.setStringValue(line1);
        }
    }
    
    /**
     * Sets (as xml) the "Line1" element
     */
    public void xsetLine1(com.idanalytics.products.idscore.request.AddressLine line1)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.AddressLine target = null;
            target = (com.idanalytics.products.idscore.request.AddressLine)get_store().find_element_user(LINE1$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.AddressLine)get_store().add_element_user(LINE1$0);
            }
            target.set(line1);
        }
    }
    
    /**
     * Gets the "Line2" element
     */
    public java.lang.String getLine2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LINE2$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Line2" element
     */
    public com.idanalytics.products.idscore.request.AddressLine xgetLine2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.AddressLine target = null;
            target = (com.idanalytics.products.idscore.request.AddressLine)get_store().find_element_user(LINE2$2, 0);
            return target;
        }
    }
    
    /**
     * True if has "Line2" element
     */
    public boolean isSetLine2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(LINE2$2) != 0;
        }
    }
    
    /**
     * Sets the "Line2" element
     */
    public void setLine2(java.lang.String line2)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LINE2$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(LINE2$2);
            }
            target.setStringValue(line2);
        }
    }
    
    /**
     * Sets (as xml) the "Line2" element
     */
    public void xsetLine2(com.idanalytics.products.idscore.request.AddressLine line2)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.AddressLine target = null;
            target = (com.idanalytics.products.idscore.request.AddressLine)get_store().find_element_user(LINE2$2, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.AddressLine)get_store().add_element_user(LINE2$2);
            }
            target.set(line2);
        }
    }
    
    /**
     * Unsets the "Line2" element
     */
    public void unsetLine2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(LINE2$2, 0);
        }
    }
}
