/*
 * An XML document type.
 * Localname: EnrollmentRequest
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.EnrollmentRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * A document containing one EnrollmentRequest(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public class EnrollmentRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.EnrollmentRequestDocument
{
    private static final long serialVersionUID = 1L;
    
    public EnrollmentRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ENROLLMENTREQUEST$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "EnrollmentRequest");
    
    
    /**
     * Gets the "EnrollmentRequest" element
     */
    public com.idanalytics.products.idscore.request.EnrollmentRequestDocument.EnrollmentRequest getEnrollmentRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.EnrollmentRequestDocument.EnrollmentRequest target = null;
            target = (com.idanalytics.products.idscore.request.EnrollmentRequestDocument.EnrollmentRequest)get_store().find_element_user(ENROLLMENTREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "EnrollmentRequest" element
     */
    public void setEnrollmentRequest(com.idanalytics.products.idscore.request.EnrollmentRequestDocument.EnrollmentRequest enrollmentRequest)
    {
        generatedSetterHelperImpl(enrollmentRequest, ENROLLMENTREQUEST$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "EnrollmentRequest" element
     */
    public com.idanalytics.products.idscore.request.EnrollmentRequestDocument.EnrollmentRequest addNewEnrollmentRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.EnrollmentRequestDocument.EnrollmentRequest target = null;
            target = (com.idanalytics.products.idscore.request.EnrollmentRequestDocument.EnrollmentRequest)get_store().add_element_user(ENROLLMENTREQUEST$0);
            return target;
        }
    }
    /**
     * An XML EnrollmentRequest(@http://idanalytics.com/products/idscore/request).
     *
     * This is a complex type.
     */
    public static class EnrollmentRequestImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.EnrollmentRequestDocument.EnrollmentRequest
    {
        private static final long serialVersionUID = 1L;
        
        public EnrollmentRequestImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName CONSUMERHEADER$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ConsumerHeader");
        private static final javax.xml.namespace.QName IDENTITY$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Identity");
        private static final javax.xml.namespace.QName IDAINTERNAL$4 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "IDAInternal");
        private static final javax.xml.namespace.QName SCHEMAVERSION$6 = 
            new javax.xml.namespace.QName("", "schemaVersion");
        
        
        /**
         * Gets the "ConsumerHeader" element
         */
        public com.idanalytics.products.idscore.request.ConsumerHeaderDocument.ConsumerHeader getConsumerHeader()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.ConsumerHeaderDocument.ConsumerHeader target = null;
                target = (com.idanalytics.products.idscore.request.ConsumerHeaderDocument.ConsumerHeader)get_store().find_element_user(CONSUMERHEADER$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * Sets the "ConsumerHeader" element
         */
        public void setConsumerHeader(com.idanalytics.products.idscore.request.ConsumerHeaderDocument.ConsumerHeader consumerHeader)
        {
            generatedSetterHelperImpl(consumerHeader, CONSUMERHEADER$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "ConsumerHeader" element
         */
        public com.idanalytics.products.idscore.request.ConsumerHeaderDocument.ConsumerHeader addNewConsumerHeader()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.ConsumerHeaderDocument.ConsumerHeader target = null;
                target = (com.idanalytics.products.idscore.request.ConsumerHeaderDocument.ConsumerHeader)get_store().add_element_user(CONSUMERHEADER$0);
                return target;
            }
        }
        
        /**
         * Gets the "Identity" element
         */
        public com.idanalytics.products.idscore.request.Identity getIdentity()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.Identity target = null;
                target = (com.idanalytics.products.idscore.request.Identity)get_store().find_element_user(IDENTITY$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "Identity" element
         */
        public boolean isSetIdentity()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(IDENTITY$2) != 0;
            }
        }
        
        /**
         * Sets the "Identity" element
         */
        public void setIdentity(com.idanalytics.products.idscore.request.Identity identity)
        {
            generatedSetterHelperImpl(identity, IDENTITY$2, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "Identity" element
         */
        public com.idanalytics.products.idscore.request.Identity addNewIdentity()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.Identity target = null;
                target = (com.idanalytics.products.idscore.request.Identity)get_store().add_element_user(IDENTITY$2);
                return target;
            }
        }
        
        /**
         * Unsets the "Identity" element
         */
        public void unsetIdentity()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(IDENTITY$2, 0);
            }
        }
        
        /**
         * Gets the "IDAInternal" element
         */
        public com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal getIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal target = null;
                target = (com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal)get_store().find_element_user(IDAINTERNAL$4, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "IDAInternal" element
         */
        public boolean isSetIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(IDAINTERNAL$4) != 0;
            }
        }
        
        /**
         * Sets the "IDAInternal" element
         */
        public void setIDAInternal(com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal idaInternal)
        {
            generatedSetterHelperImpl(idaInternal, IDAINTERNAL$4, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "IDAInternal" element
         */
        public com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal addNewIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal target = null;
                target = (com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal)get_store().add_element_user(IDAINTERNAL$4);
                return target;
            }
        }
        
        /**
         * Unsets the "IDAInternal" element
         */
        public void unsetIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(IDAINTERNAL$4, 0);
            }
        }
        
        /**
         * Gets the "schemaVersion" attribute
         */
        public java.math.BigDecimal getSchemaVersion()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(SCHEMAVERSION$6);
                if (target == null)
                {
                    return null;
                }
                return target.getBigDecimalValue();
            }
        }
        
        /**
         * Gets (as xml) the "schemaVersion" attribute
         */
        public org.apache.xmlbeans.XmlDecimal xgetSchemaVersion()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlDecimal target = null;
                target = (org.apache.xmlbeans.XmlDecimal)get_store().find_attribute_user(SCHEMAVERSION$6);
                return target;
            }
        }
        
        /**
         * True if has "schemaVersion" attribute
         */
        public boolean isSetSchemaVersion()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().find_attribute_user(SCHEMAVERSION$6) != null;
            }
        }
        
        /**
         * Sets the "schemaVersion" attribute
         */
        public void setSchemaVersion(java.math.BigDecimal schemaVersion)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(SCHEMAVERSION$6);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(SCHEMAVERSION$6);
                }
                target.setBigDecimalValue(schemaVersion);
            }
        }
        
        /**
         * Sets (as xml) the "schemaVersion" attribute
         */
        public void xsetSchemaVersion(org.apache.xmlbeans.XmlDecimal schemaVersion)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlDecimal target = null;
                target = (org.apache.xmlbeans.XmlDecimal)get_store().find_attribute_user(SCHEMAVERSION$6);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlDecimal)get_store().add_attribute_user(SCHEMAVERSION$6);
                }
                target.set(schemaVersion);
            }
        }
        
        /**
         * Unsets the "schemaVersion" attribute
         */
        public void unsetSchemaVersion()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_attribute(SCHEMAVERSION$6);
            }
        }
    }
}
