/*
 * XML Type:  Diff
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.Diff
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML Diff(@http://idanalytics.com/products/idscore/request).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.Diff.
 */
public class DiffImpl extends org.apache.xmlbeans.impl.values.JavaIntegerHolderEx implements com.idanalytics.products.idscore.request.Diff
{
    private static final long serialVersionUID = 1L;
    
    public DiffImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected DiffImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
