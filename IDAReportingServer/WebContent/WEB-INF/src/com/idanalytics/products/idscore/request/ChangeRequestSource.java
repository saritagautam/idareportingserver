/*
 * XML Type:  ChangeRequestSource
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.ChangeRequestSource
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request;


/**
 * An XML ChangeRequestSource(@http://idanalytics.com/products/idscore/request).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.ChangeRequestSource.
 */
public interface ChangeRequestSource extends org.apache.xmlbeans.XmlString
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ChangeRequestSource.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("changerequestsourcecda0type");
    
    org.apache.xmlbeans.StringEnumAbstractBase enumValue();
    void set(org.apache.xmlbeans.StringEnumAbstractBase e);
    
    static final Enum CUSTOMER_PHONE = Enum.forString("CUSTOMER_PHONE");
    static final Enum CUSTOMER_FAX = Enum.forString("CUSTOMER_FAX");
    static final Enum CUSTOMER_MAIL = Enum.forString("CUSTOMER_MAIL");
    static final Enum CUSTOMER_INTERNET = Enum.forString("CUSTOMER_INTERNET");
    static final Enum CUSTOMER_IN_PERSON = Enum.forString("CUSTOMER_IN_PERSON");
    static final Enum CUSTOMER_OTHER = Enum.forString("CUSTOMER_OTHER");
    static final Enum USPS = Enum.forString("USPS");
    
    static final int INT_CUSTOMER_PHONE = Enum.INT_CUSTOMER_PHONE;
    static final int INT_CUSTOMER_FAX = Enum.INT_CUSTOMER_FAX;
    static final int INT_CUSTOMER_MAIL = Enum.INT_CUSTOMER_MAIL;
    static final int INT_CUSTOMER_INTERNET = Enum.INT_CUSTOMER_INTERNET;
    static final int INT_CUSTOMER_IN_PERSON = Enum.INT_CUSTOMER_IN_PERSON;
    static final int INT_CUSTOMER_OTHER = Enum.INT_CUSTOMER_OTHER;
    static final int INT_USPS = Enum.INT_USPS;
    
    /**
     * Enumeration value class for com.idanalytics.products.idscore.request.ChangeRequestSource.
     * These enum values can be used as follows:
     * <pre>
     * enum.toString(); // returns the string value of the enum
     * enum.intValue(); // returns an int value, useful for switches
     * // e.g., case Enum.INT_CUSTOMER_PHONE
     * Enum.forString(s); // returns the enum value for a string
     * Enum.forInt(i); // returns the enum value for an int
     * </pre>
     * Enumeration objects are immutable singleton objects that
     * can be compared using == object equality. They have no
     * public constructor. See the constants defined within this
     * class for all the valid values.
     */
    static final class Enum extends org.apache.xmlbeans.StringEnumAbstractBase
    {
        /**
         * Returns the enum value for a string, or null if none.
         */
        public static Enum forString(java.lang.String s)
            { return (Enum)table.forString(s); }
        /**
         * Returns the enum value corresponding to an int, or null if none.
         */
        public static Enum forInt(int i)
            { return (Enum)table.forInt(i); }
        
        private Enum(java.lang.String s, int i)
            { super(s, i); }
        
        static final int INT_CUSTOMER_PHONE = 1;
        static final int INT_CUSTOMER_FAX = 2;
        static final int INT_CUSTOMER_MAIL = 3;
        static final int INT_CUSTOMER_INTERNET = 4;
        static final int INT_CUSTOMER_IN_PERSON = 5;
        static final int INT_CUSTOMER_OTHER = 6;
        static final int INT_USPS = 7;
        
        public static final org.apache.xmlbeans.StringEnumAbstractBase.Table table =
            new org.apache.xmlbeans.StringEnumAbstractBase.Table
        (
            new Enum[]
            {
                new Enum("CUSTOMER_PHONE", INT_CUSTOMER_PHONE),
                new Enum("CUSTOMER_FAX", INT_CUSTOMER_FAX),
                new Enum("CUSTOMER_MAIL", INT_CUSTOMER_MAIL),
                new Enum("CUSTOMER_INTERNET", INT_CUSTOMER_INTERNET),
                new Enum("CUSTOMER_IN_PERSON", INT_CUSTOMER_IN_PERSON),
                new Enum("CUSTOMER_OTHER", INT_CUSTOMER_OTHER),
                new Enum("USPS", INT_USPS),
            }
        );
        private static final long serialVersionUID = 1L;
        private java.lang.Object readResolve() { return forInt(intValue()); } 
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.idscore.request.ChangeRequestSource newValue(java.lang.Object obj) {
          return (com.idanalytics.products.idscore.request.ChangeRequestSource) type.newValue( obj ); }
        
        public static com.idanalytics.products.idscore.request.ChangeRequestSource newInstance() {
          return (com.idanalytics.products.idscore.request.ChangeRequestSource) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.idscore.request.ChangeRequestSource newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.idscore.request.ChangeRequestSource) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.idscore.request.ChangeRequestSource parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.ChangeRequestSource) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.idscore.request.ChangeRequestSource parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.ChangeRequestSource) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.idscore.request.ChangeRequestSource parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.ChangeRequestSource) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.idscore.request.ChangeRequestSource parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.ChangeRequestSource) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.idscore.request.ChangeRequestSource parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.ChangeRequestSource) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.idscore.request.ChangeRequestSource parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.ChangeRequestSource) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.idscore.request.ChangeRequestSource parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.ChangeRequestSource) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.idscore.request.ChangeRequestSource parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.ChangeRequestSource) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.idscore.request.ChangeRequestSource parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.ChangeRequestSource) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.idscore.request.ChangeRequestSource parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.ChangeRequestSource) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.idscore.request.ChangeRequestSource parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.ChangeRequestSource) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.idscore.request.ChangeRequestSource parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.ChangeRequestSource) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.idscore.request.ChangeRequestSource parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.ChangeRequestSource) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.idscore.request.ChangeRequestSource parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.ChangeRequestSource) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.request.ChangeRequestSource parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.request.ChangeRequestSource) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.request.ChangeRequestSource parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.request.ChangeRequestSource) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
