/*
 * XML Type:  string30
 * Namespace: http://idanalytics.com/products/idscore/result
 * Java type: com.idanalytics.products.idscore.result.String30
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.result.impl;
/**
 * An XML string30(@http://idanalytics.com/products/idscore/result).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result.String30.
 */
public class String30Impl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.result.String30
{
    private static final long serialVersionUID = 1L;
    
    public String30Impl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected String30Impl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
