/*
 * XML Type:  ExistingAccount
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.ExistingAccount
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML ExistingAccount(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public class ExistingAccountImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.ExistingAccount
{
    private static final long serialVersionUID = 1L;
    
    public ExistingAccountImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ACCOUNTNUMBER$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "AccountNumber");
    private static final javax.xml.namespace.QName ACCOUNTTYPE$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "AccountType");
    private static final javax.xml.namespace.QName OPENDATE$4 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "OpenDate");
    private static final javax.xml.namespace.QName ACTIVATEDATE$6 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ActivateDate");
    private static final javax.xml.namespace.QName LASTPAYMENT$8 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "LastPayment");
    private static final javax.xml.namespace.QName LASTCHARGE$10 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "LastCharge");
    private static final javax.xml.namespace.QName LASTNONMONETARY$12 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "LastNonMonetary");
    private static final javax.xml.namespace.QName LASTADDRESSCHANGE$14 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "LastAddressChange");
    private static final javax.xml.namespace.QName LASTPHONECHANGE$16 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "LastPhoneChange");
    private static final javax.xml.namespace.QName USER$18 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "User");
    
    
    /**
     * Gets the "AccountNumber" element
     */
    public java.lang.String getAccountNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ACCOUNTNUMBER$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "AccountNumber" element
     */
    public com.idanalytics.products.idscore.request.AccountNumber xgetAccountNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.AccountNumber target = null;
            target = (com.idanalytics.products.idscore.request.AccountNumber)get_store().find_element_user(ACCOUNTNUMBER$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "AccountNumber" element
     */
    public void setAccountNumber(java.lang.String accountNumber)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ACCOUNTNUMBER$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ACCOUNTNUMBER$0);
            }
            target.setStringValue(accountNumber);
        }
    }
    
    /**
     * Sets (as xml) the "AccountNumber" element
     */
    public void xsetAccountNumber(com.idanalytics.products.idscore.request.AccountNumber accountNumber)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.AccountNumber target = null;
            target = (com.idanalytics.products.idscore.request.AccountNumber)get_store().find_element_user(ACCOUNTNUMBER$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.AccountNumber)get_store().add_element_user(ACCOUNTNUMBER$0);
            }
            target.set(accountNumber);
        }
    }
    
    /**
     * Gets the "AccountType" element
     */
    public java.lang.String getAccountType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ACCOUNTTYPE$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "AccountType" element
     */
    public com.idanalytics.products.idscore.request.AccountType xgetAccountType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.AccountType target = null;
            target = (com.idanalytics.products.idscore.request.AccountType)get_store().find_element_user(ACCOUNTTYPE$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "AccountType" element
     */
    public void setAccountType(java.lang.String accountType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ACCOUNTTYPE$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ACCOUNTTYPE$2);
            }
            target.setStringValue(accountType);
        }
    }
    
    /**
     * Sets (as xml) the "AccountType" element
     */
    public void xsetAccountType(com.idanalytics.products.idscore.request.AccountType accountType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.AccountType target = null;
            target = (com.idanalytics.products.idscore.request.AccountType)get_store().find_element_user(ACCOUNTTYPE$2, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.AccountType)get_store().add_element_user(ACCOUNTTYPE$2);
            }
            target.set(accountType);
        }
    }
    
    /**
     * Gets the "OpenDate" element
     */
    public java.lang.Object getOpenDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(OPENDATE$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getObjectValue();
        }
    }
    
    /**
     * Gets (as xml) the "OpenDate" element
     */
    public com.idanalytics.products.idscore.request.Date xgetOpenDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Date target = null;
            target = (com.idanalytics.products.idscore.request.Date)get_store().find_element_user(OPENDATE$4, 0);
            return target;
        }
    }
    
    /**
     * Sets the "OpenDate" element
     */
    public void setOpenDate(java.lang.Object openDate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(OPENDATE$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(OPENDATE$4);
            }
            target.setObjectValue(openDate);
        }
    }
    
    /**
     * Sets (as xml) the "OpenDate" element
     */
    public void xsetOpenDate(com.idanalytics.products.idscore.request.Date openDate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Date target = null;
            target = (com.idanalytics.products.idscore.request.Date)get_store().find_element_user(OPENDATE$4, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Date)get_store().add_element_user(OPENDATE$4);
            }
            target.set(openDate);
        }
    }
    
    /**
     * Gets the "ActivateDate" element
     */
    public java.lang.Object getActivateDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ACTIVATEDATE$6, 0);
            if (target == null)
            {
                return null;
            }
            return target.getObjectValue();
        }
    }
    
    /**
     * Gets (as xml) the "ActivateDate" element
     */
    public com.idanalytics.products.idscore.request.Date xgetActivateDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Date target = null;
            target = (com.idanalytics.products.idscore.request.Date)get_store().find_element_user(ACTIVATEDATE$6, 0);
            return target;
        }
    }
    
    /**
     * Sets the "ActivateDate" element
     */
    public void setActivateDate(java.lang.Object activateDate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ACTIVATEDATE$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ACTIVATEDATE$6);
            }
            target.setObjectValue(activateDate);
        }
    }
    
    /**
     * Sets (as xml) the "ActivateDate" element
     */
    public void xsetActivateDate(com.idanalytics.products.idscore.request.Date activateDate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Date target = null;
            target = (com.idanalytics.products.idscore.request.Date)get_store().find_element_user(ACTIVATEDATE$6, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Date)get_store().add_element_user(ACTIVATEDATE$6);
            }
            target.set(activateDate);
        }
    }
    
    /**
     * Gets the "LastPayment" element
     */
    public java.lang.Object getLastPayment()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LASTPAYMENT$8, 0);
            if (target == null)
            {
                return null;
            }
            return target.getObjectValue();
        }
    }
    
    /**
     * Gets (as xml) the "LastPayment" element
     */
    public com.idanalytics.products.idscore.request.Date xgetLastPayment()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Date target = null;
            target = (com.idanalytics.products.idscore.request.Date)get_store().find_element_user(LASTPAYMENT$8, 0);
            return target;
        }
    }
    
    /**
     * Sets the "LastPayment" element
     */
    public void setLastPayment(java.lang.Object lastPayment)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LASTPAYMENT$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(LASTPAYMENT$8);
            }
            target.setObjectValue(lastPayment);
        }
    }
    
    /**
     * Sets (as xml) the "LastPayment" element
     */
    public void xsetLastPayment(com.idanalytics.products.idscore.request.Date lastPayment)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Date target = null;
            target = (com.idanalytics.products.idscore.request.Date)get_store().find_element_user(LASTPAYMENT$8, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Date)get_store().add_element_user(LASTPAYMENT$8);
            }
            target.set(lastPayment);
        }
    }
    
    /**
     * Gets the "LastCharge" element
     */
    public java.lang.Object getLastCharge()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LASTCHARGE$10, 0);
            if (target == null)
            {
                return null;
            }
            return target.getObjectValue();
        }
    }
    
    /**
     * Gets (as xml) the "LastCharge" element
     */
    public com.idanalytics.products.idscore.request.Date xgetLastCharge()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Date target = null;
            target = (com.idanalytics.products.idscore.request.Date)get_store().find_element_user(LASTCHARGE$10, 0);
            return target;
        }
    }
    
    /**
     * Sets the "LastCharge" element
     */
    public void setLastCharge(java.lang.Object lastCharge)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LASTCHARGE$10, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(LASTCHARGE$10);
            }
            target.setObjectValue(lastCharge);
        }
    }
    
    /**
     * Sets (as xml) the "LastCharge" element
     */
    public void xsetLastCharge(com.idanalytics.products.idscore.request.Date lastCharge)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Date target = null;
            target = (com.idanalytics.products.idscore.request.Date)get_store().find_element_user(LASTCHARGE$10, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Date)get_store().add_element_user(LASTCHARGE$10);
            }
            target.set(lastCharge);
        }
    }
    
    /**
     * Gets the "LastNonMonetary" element
     */
    public java.lang.Object getLastNonMonetary()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LASTNONMONETARY$12, 0);
            if (target == null)
            {
                return null;
            }
            return target.getObjectValue();
        }
    }
    
    /**
     * Gets (as xml) the "LastNonMonetary" element
     */
    public com.idanalytics.products.idscore.request.Date xgetLastNonMonetary()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Date target = null;
            target = (com.idanalytics.products.idscore.request.Date)get_store().find_element_user(LASTNONMONETARY$12, 0);
            return target;
        }
    }
    
    /**
     * Sets the "LastNonMonetary" element
     */
    public void setLastNonMonetary(java.lang.Object lastNonMonetary)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LASTNONMONETARY$12, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(LASTNONMONETARY$12);
            }
            target.setObjectValue(lastNonMonetary);
        }
    }
    
    /**
     * Sets (as xml) the "LastNonMonetary" element
     */
    public void xsetLastNonMonetary(com.idanalytics.products.idscore.request.Date lastNonMonetary)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Date target = null;
            target = (com.idanalytics.products.idscore.request.Date)get_store().find_element_user(LASTNONMONETARY$12, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Date)get_store().add_element_user(LASTNONMONETARY$12);
            }
            target.set(lastNonMonetary);
        }
    }
    
    /**
     * Gets the "LastAddressChange" element
     */
    public java.lang.Object getLastAddressChange()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LASTADDRESSCHANGE$14, 0);
            if (target == null)
            {
                return null;
            }
            return target.getObjectValue();
        }
    }
    
    /**
     * Gets (as xml) the "LastAddressChange" element
     */
    public com.idanalytics.products.idscore.request.Date xgetLastAddressChange()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Date target = null;
            target = (com.idanalytics.products.idscore.request.Date)get_store().find_element_user(LASTADDRESSCHANGE$14, 0);
            return target;
        }
    }
    
    /**
     * Sets the "LastAddressChange" element
     */
    public void setLastAddressChange(java.lang.Object lastAddressChange)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LASTADDRESSCHANGE$14, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(LASTADDRESSCHANGE$14);
            }
            target.setObjectValue(lastAddressChange);
        }
    }
    
    /**
     * Sets (as xml) the "LastAddressChange" element
     */
    public void xsetLastAddressChange(com.idanalytics.products.idscore.request.Date lastAddressChange)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Date target = null;
            target = (com.idanalytics.products.idscore.request.Date)get_store().find_element_user(LASTADDRESSCHANGE$14, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Date)get_store().add_element_user(LASTADDRESSCHANGE$14);
            }
            target.set(lastAddressChange);
        }
    }
    
    /**
     * Gets the "LastPhoneChange" element
     */
    public java.lang.Object getLastPhoneChange()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LASTPHONECHANGE$16, 0);
            if (target == null)
            {
                return null;
            }
            return target.getObjectValue();
        }
    }
    
    /**
     * Gets (as xml) the "LastPhoneChange" element
     */
    public com.idanalytics.products.idscore.request.Date xgetLastPhoneChange()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Date target = null;
            target = (com.idanalytics.products.idscore.request.Date)get_store().find_element_user(LASTPHONECHANGE$16, 0);
            return target;
        }
    }
    
    /**
     * Sets the "LastPhoneChange" element
     */
    public void setLastPhoneChange(java.lang.Object lastPhoneChange)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(LASTPHONECHANGE$16, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(LASTPHONECHANGE$16);
            }
            target.setObjectValue(lastPhoneChange);
        }
    }
    
    /**
     * Sets (as xml) the "LastPhoneChange" element
     */
    public void xsetLastPhoneChange(com.idanalytics.products.idscore.request.Date lastPhoneChange)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Date target = null;
            target = (com.idanalytics.products.idscore.request.Date)get_store().find_element_user(LASTPHONECHANGE$16, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Date)get_store().add_element_user(LASTPHONECHANGE$16);
            }
            target.set(lastPhoneChange);
        }
    }
    
    /**
     * Gets array of all "User" elements
     */
    public com.idanalytics.products.idscore.request.Identity[] getUserArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(USER$18, targetList);
            com.idanalytics.products.idscore.request.Identity[] result = new com.idanalytics.products.idscore.request.Identity[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "User" element
     */
    public com.idanalytics.products.idscore.request.Identity getUserArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Identity target = null;
            target = (com.idanalytics.products.idscore.request.Identity)get_store().find_element_user(USER$18, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "User" element
     */
    public int sizeOfUserArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(USER$18);
        }
    }
    
    /**
     * Sets array of all "User" element  WARNING: This method is not atomicaly synchronized.
     */
    public void setUserArray(com.idanalytics.products.idscore.request.Identity[] userArray)
    {
        check_orphaned();
        arraySetterHelper(userArray, USER$18);
    }
    
    /**
     * Sets ith "User" element
     */
    public void setUserArray(int i, com.idanalytics.products.idscore.request.Identity user)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Identity target = null;
            target = (com.idanalytics.products.idscore.request.Identity)get_store().find_element_user(USER$18, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(user);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "User" element
     */
    public com.idanalytics.products.idscore.request.Identity insertNewUser(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Identity target = null;
            target = (com.idanalytics.products.idscore.request.Identity)get_store().insert_element_user(USER$18, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "User" element
     */
    public com.idanalytics.products.idscore.request.Identity addNewUser()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Identity target = null;
            target = (com.idanalytics.products.idscore.request.Identity)get_store().add_element_user(USER$18);
            return target;
        }
    }
    
    /**
     * Removes the ith "User" element
     */
    public void removeUser(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(USER$18, i);
        }
    }
}
