/*
 * An XML document type.
 * Localname: CardNumber
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.CardNumberDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * A document containing one CardNumber(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public class CardNumberDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.CardNumberDocument
{
    private static final long serialVersionUID = 1L;
    
    public CardNumberDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CARDNUMBER$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "CardNumber");
    
    
    /**
     * Gets the "CardNumber" element
     */
    public java.lang.String getCardNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CARDNUMBER$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "CardNumber" element
     */
    public com.idanalytics.products.idscore.request.CardNumberDocument.CardNumber xgetCardNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.CardNumberDocument.CardNumber target = null;
            target = (com.idanalytics.products.idscore.request.CardNumberDocument.CardNumber)get_store().find_element_user(CARDNUMBER$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "CardNumber" element
     */
    public void setCardNumber(java.lang.String cardNumber)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CARDNUMBER$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CARDNUMBER$0);
            }
            target.setStringValue(cardNumber);
        }
    }
    
    /**
     * Sets (as xml) the "CardNumber" element
     */
    public void xsetCardNumber(com.idanalytics.products.idscore.request.CardNumberDocument.CardNumber cardNumber)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.CardNumberDocument.CardNumber target = null;
            target = (com.idanalytics.products.idscore.request.CardNumberDocument.CardNumber)get_store().find_element_user(CARDNUMBER$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.CardNumberDocument.CardNumber)get_store().add_element_user(CARDNUMBER$0);
            }
            target.set(cardNumber);
        }
    }
    /**
     * An XML CardNumber(@http://idanalytics.com/products/idscore/request).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.CardNumberDocument$CardNumber.
     */
    public static class CardNumberImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.request.CardNumberDocument.CardNumber
    {
        private static final long serialVersionUID = 1L;
        
        public CardNumberImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected CardNumberImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
