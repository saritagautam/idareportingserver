/*
 * An XML document type.
 * Localname: IndustryType
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.IndustryTypeDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * A document containing one IndustryType(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public class IndustryTypeDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.IndustryTypeDocument
{
    private static final long serialVersionUID = 1L;
    
    public IndustryTypeDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName INDUSTRYTYPE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "IndustryType");
    
    
    /**
     * Gets the "IndustryType" element
     */
    public java.lang.String getIndustryType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(INDUSTRYTYPE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IndustryType" element
     */
    public com.idanalytics.products.idscore.request.IndustryTypeDocument.IndustryType xgetIndustryType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.IndustryTypeDocument.IndustryType target = null;
            target = (com.idanalytics.products.idscore.request.IndustryTypeDocument.IndustryType)get_store().find_element_user(INDUSTRYTYPE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IndustryType" element
     */
    public void setIndustryType(java.lang.String industryType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(INDUSTRYTYPE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(INDUSTRYTYPE$0);
            }
            target.setStringValue(industryType);
        }
    }
    
    /**
     * Sets (as xml) the "IndustryType" element
     */
    public void xsetIndustryType(com.idanalytics.products.idscore.request.IndustryTypeDocument.IndustryType industryType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.IndustryTypeDocument.IndustryType target = null;
            target = (com.idanalytics.products.idscore.request.IndustryTypeDocument.IndustryType)get_store().find_element_user(INDUSTRYTYPE$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.IndustryTypeDocument.IndustryType)get_store().add_element_user(INDUSTRYTYPE$0);
            }
            target.set(industryType);
        }
    }
    /**
     * An XML IndustryType(@http://idanalytics.com/products/idscore/request).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.IndustryTypeDocument$IndustryType.
     */
    public static class IndustryTypeImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.request.IndustryTypeDocument.IndustryType
    {
        private static final long serialVersionUID = 1L;
        
        public IndustryTypeImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected IndustryTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
