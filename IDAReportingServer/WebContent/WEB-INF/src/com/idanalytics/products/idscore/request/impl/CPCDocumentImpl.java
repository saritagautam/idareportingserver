/*
 * An XML document type.
 * Localname: CPC
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.CPCDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * A document containing one CPC(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public class CPCDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.CPCDocument
{
    private static final long serialVersionUID = 1L;
    
    public CPCDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CPC$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "CPC");
    
    
    /**
     * Gets the "CPC" element
     */
    public java.lang.String getCPC()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CPC$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "CPC" element
     */
    public com.idanalytics.products.idscore.request.CPCDocument.CPC xgetCPC()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.CPCDocument.CPC target = null;
            target = (com.idanalytics.products.idscore.request.CPCDocument.CPC)get_store().find_element_user(CPC$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "CPC" element
     */
    public void setCPC(java.lang.String cpc)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CPC$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CPC$0);
            }
            target.setStringValue(cpc);
        }
    }
    
    /**
     * Sets (as xml) the "CPC" element
     */
    public void xsetCPC(com.idanalytics.products.idscore.request.CPCDocument.CPC cpc)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.CPCDocument.CPC target = null;
            target = (com.idanalytics.products.idscore.request.CPCDocument.CPC)get_store().find_element_user(CPC$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.CPCDocument.CPC)get_store().add_element_user(CPC$0);
            }
            target.set(cpc);
        }
    }
    /**
     * An XML CPC(@http://idanalytics.com/products/idscore/request).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.CPCDocument$CPC.
     */
    public static class CPCImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.request.CPCDocument.CPC
    {
        private static final long serialVersionUID = 1L;
        
        public CPCImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected CPCImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
