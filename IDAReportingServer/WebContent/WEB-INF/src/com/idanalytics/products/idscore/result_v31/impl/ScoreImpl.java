/*
 * XML Type:  Score
 * Namespace: http://idanalytics.com/products/idscore/result.v31
 * Java type: com.idanalytics.products.idscore.result_v31.Score
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.result_v31.impl;
/**
 * An XML Score(@http://idanalytics.com/products/idscore/result.v31).
 *
 * This is a complex type.
 */
public class ScoreImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.result_v31.Score
{
    private static final long serialVersionUID = 1L;
    
    public ScoreImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IDSCORE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result.v31", "IDScore");
    private static final javax.xml.namespace.QName SCOREBANDINDICATOR$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result.v31", "ScoreBandIndicator");
    private static final javax.xml.namespace.QName SCOREDELTAINDICATOR$4 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result.v31", "ScoreDeltaIndicator");
    private static final javax.xml.namespace.QName IDSCORERESULTCODE$6 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result.v31", "IDScoreResultCode");
    private static final javax.xml.namespace.QName SCORETYPE$8 = 
        new javax.xml.namespace.QName("", "scoreType");
    
    
    /**
     * Gets the "IDScore" element
     */
    public java.lang.String getIDScore()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDSCORE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IDScore" element
     */
    public com.idanalytics.products.idscore.result_v31.IDScoreDocument.IDScore xgetIDScore()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result_v31.IDScoreDocument.IDScore target = null;
            target = (com.idanalytics.products.idscore.result_v31.IDScoreDocument.IDScore)get_store().find_element_user(IDSCORE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IDScore" element
     */
    public void setIDScore(java.lang.String idScore)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDSCORE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDSCORE$0);
            }
            target.setStringValue(idScore);
        }
    }
    
    /**
     * Sets (as xml) the "IDScore" element
     */
    public void xsetIDScore(com.idanalytics.products.idscore.result_v31.IDScoreDocument.IDScore idScore)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result_v31.IDScoreDocument.IDScore target = null;
            target = (com.idanalytics.products.idscore.result_v31.IDScoreDocument.IDScore)get_store().find_element_user(IDSCORE$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.result_v31.IDScoreDocument.IDScore)get_store().add_element_user(IDSCORE$0);
            }
            target.set(idScore);
        }
    }
    
    /**
     * Gets the "ScoreBandIndicator" element
     */
    public com.idanalytics.products.idscore.result_v31.ScoreBandIndicatorDocument.ScoreBandIndicator.Enum getScoreBandIndicator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SCOREBANDINDICATOR$2, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.idscore.result_v31.ScoreBandIndicatorDocument.ScoreBandIndicator.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "ScoreBandIndicator" element
     */
    public com.idanalytics.products.idscore.result_v31.ScoreBandIndicatorDocument.ScoreBandIndicator xgetScoreBandIndicator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result_v31.ScoreBandIndicatorDocument.ScoreBandIndicator target = null;
            target = (com.idanalytics.products.idscore.result_v31.ScoreBandIndicatorDocument.ScoreBandIndicator)get_store().find_element_user(SCOREBANDINDICATOR$2, 0);
            return target;
        }
    }
    
    /**
     * True if has "ScoreBandIndicator" element
     */
    public boolean isSetScoreBandIndicator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SCOREBANDINDICATOR$2) != 0;
        }
    }
    
    /**
     * Sets the "ScoreBandIndicator" element
     */
    public void setScoreBandIndicator(com.idanalytics.products.idscore.result_v31.ScoreBandIndicatorDocument.ScoreBandIndicator.Enum scoreBandIndicator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SCOREBANDINDICATOR$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SCOREBANDINDICATOR$2);
            }
            target.setEnumValue(scoreBandIndicator);
        }
    }
    
    /**
     * Sets (as xml) the "ScoreBandIndicator" element
     */
    public void xsetScoreBandIndicator(com.idanalytics.products.idscore.result_v31.ScoreBandIndicatorDocument.ScoreBandIndicator scoreBandIndicator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result_v31.ScoreBandIndicatorDocument.ScoreBandIndicator target = null;
            target = (com.idanalytics.products.idscore.result_v31.ScoreBandIndicatorDocument.ScoreBandIndicator)get_store().find_element_user(SCOREBANDINDICATOR$2, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.result_v31.ScoreBandIndicatorDocument.ScoreBandIndicator)get_store().add_element_user(SCOREBANDINDICATOR$2);
            }
            target.set(scoreBandIndicator);
        }
    }
    
    /**
     * Unsets the "ScoreBandIndicator" element
     */
    public void unsetScoreBandIndicator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SCOREBANDINDICATOR$2, 0);
        }
    }
    
    /**
     * Gets the "ScoreDeltaIndicator" element
     */
    public com.idanalytics.products.idscore.result_v31.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator.Enum getScoreDeltaIndicator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SCOREDELTAINDICATOR$4, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.idscore.result_v31.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "ScoreDeltaIndicator" element
     */
    public com.idanalytics.products.idscore.result_v31.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator xgetScoreDeltaIndicator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result_v31.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator target = null;
            target = (com.idanalytics.products.idscore.result_v31.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator)get_store().find_element_user(SCOREDELTAINDICATOR$4, 0);
            return target;
        }
    }
    
    /**
     * True if has "ScoreDeltaIndicator" element
     */
    public boolean isSetScoreDeltaIndicator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SCOREDELTAINDICATOR$4) != 0;
        }
    }
    
    /**
     * Sets the "ScoreDeltaIndicator" element
     */
    public void setScoreDeltaIndicator(com.idanalytics.products.idscore.result_v31.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator.Enum scoreDeltaIndicator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SCOREDELTAINDICATOR$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SCOREDELTAINDICATOR$4);
            }
            target.setEnumValue(scoreDeltaIndicator);
        }
    }
    
    /**
     * Sets (as xml) the "ScoreDeltaIndicator" element
     */
    public void xsetScoreDeltaIndicator(com.idanalytics.products.idscore.result_v31.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator scoreDeltaIndicator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result_v31.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator target = null;
            target = (com.idanalytics.products.idscore.result_v31.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator)get_store().find_element_user(SCOREDELTAINDICATOR$4, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.result_v31.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator)get_store().add_element_user(SCOREDELTAINDICATOR$4);
            }
            target.set(scoreDeltaIndicator);
        }
    }
    
    /**
     * Unsets the "ScoreDeltaIndicator" element
     */
    public void unsetScoreDeltaIndicator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SCOREDELTAINDICATOR$4, 0);
        }
    }
    
    /**
     * Gets array of all "IDScoreResultCode" elements
     */
    public com.idanalytics.products.idscore.result_v31.IDScoreResultCode[] getIDScoreResultCodeArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(IDSCORERESULTCODE$6, targetList);
            com.idanalytics.products.idscore.result_v31.IDScoreResultCode[] result = new com.idanalytics.products.idscore.result_v31.IDScoreResultCode[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "IDScoreResultCode" element
     */
    public com.idanalytics.products.idscore.result_v31.IDScoreResultCode getIDScoreResultCodeArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result_v31.IDScoreResultCode target = null;
            target = (com.idanalytics.products.idscore.result_v31.IDScoreResultCode)get_store().find_element_user(IDSCORERESULTCODE$6, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "IDScoreResultCode" element
     */
    public int sizeOfIDScoreResultCodeArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(IDSCORERESULTCODE$6);
        }
    }
    
    /**
     * Sets array of all "IDScoreResultCode" element  WARNING: This method is not atomicaly synchronized.
     */
    public void setIDScoreResultCodeArray(com.idanalytics.products.idscore.result_v31.IDScoreResultCode[] idScoreResultCodeArray)
    {
        check_orphaned();
        arraySetterHelper(idScoreResultCodeArray, IDSCORERESULTCODE$6);
    }
    
    /**
     * Sets ith "IDScoreResultCode" element
     */
    public void setIDScoreResultCodeArray(int i, com.idanalytics.products.idscore.result_v31.IDScoreResultCode idScoreResultCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result_v31.IDScoreResultCode target = null;
            target = (com.idanalytics.products.idscore.result_v31.IDScoreResultCode)get_store().find_element_user(IDSCORERESULTCODE$6, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(idScoreResultCode);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "IDScoreResultCode" element
     */
    public com.idanalytics.products.idscore.result_v31.IDScoreResultCode insertNewIDScoreResultCode(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result_v31.IDScoreResultCode target = null;
            target = (com.idanalytics.products.idscore.result_v31.IDScoreResultCode)get_store().insert_element_user(IDSCORERESULTCODE$6, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "IDScoreResultCode" element
     */
    public com.idanalytics.products.idscore.result_v31.IDScoreResultCode addNewIDScoreResultCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result_v31.IDScoreResultCode target = null;
            target = (com.idanalytics.products.idscore.result_v31.IDScoreResultCode)get_store().add_element_user(IDSCORERESULTCODE$6);
            return target;
        }
    }
    
    /**
     * Removes the ith "IDScoreResultCode" element
     */
    public void removeIDScoreResultCode(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(IDSCORERESULTCODE$6, i);
        }
    }
    
    /**
     * Gets the "scoreType" attribute
     */
    public org.apache.xmlbeans.XmlAnySimpleType getScoreType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlAnySimpleType target = null;
            target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().find_attribute_user(SCORETYPE$8);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "scoreType" attribute
     */
    public void setScoreType(org.apache.xmlbeans.XmlAnySimpleType scoreType)
    {
        generatedSetterHelperImpl(scoreType, SCORETYPE$8, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "scoreType" attribute
     */
    public org.apache.xmlbeans.XmlAnySimpleType addNewScoreType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlAnySimpleType target = null;
            target = (org.apache.xmlbeans.XmlAnySimpleType)get_store().add_attribute_user(SCORETYPE$8);
            return target;
        }
    }
}
