/*
 * XML Type:  ItemInfo
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.ItemInfo
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request;


/**
 * An XML ItemInfo(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public interface ItemInfo extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ItemInfo.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("iteminfofaf9type");
    
    /**
     * Gets the "ShippingCost" element
     */
    java.lang.Object getShippingCost();
    
    /**
     * Gets (as xml) the "ShippingCost" element
     */
    com.idanalytics.products.common_v1.OptionalDecimal xgetShippingCost();
    
    /**
     * True if has "ShippingCost" element
     */
    boolean isSetShippingCost();
    
    /**
     * Sets the "ShippingCost" element
     */
    void setShippingCost(java.lang.Object shippingCost);
    
    /**
     * Sets (as xml) the "ShippingCost" element
     */
    void xsetShippingCost(com.idanalytics.products.common_v1.OptionalDecimal shippingCost);
    
    /**
     * Unsets the "ShippingCost" element
     */
    void unsetShippingCost();
    
    /**
     * Gets the "ItemID" element
     */
    java.lang.String getItemID();
    
    /**
     * Gets (as xml) the "ItemID" element
     */
    com.idanalytics.products.common_v1.NormalizedString xgetItemID();
    
    /**
     * True if has "ItemID" element
     */
    boolean isSetItemID();
    
    /**
     * Sets the "ItemID" element
     */
    void setItemID(java.lang.String itemID);
    
    /**
     * Sets (as xml) the "ItemID" element
     */
    void xsetItemID(com.idanalytics.products.common_v1.NormalizedString itemID);
    
    /**
     * Unsets the "ItemID" element
     */
    void unsetItemID();
    
    /**
     * Gets the "ItemName" element
     */
    java.lang.String getItemName();
    
    /**
     * Gets (as xml) the "ItemName" element
     */
    com.idanalytics.products.common_v1.NormalizedString xgetItemName();
    
    /**
     * True if has "ItemName" element
     */
    boolean isSetItemName();
    
    /**
     * Sets the "ItemName" element
     */
    void setItemName(java.lang.String itemName);
    
    /**
     * Sets (as xml) the "ItemName" element
     */
    void xsetItemName(com.idanalytics.products.common_v1.NormalizedString itemName);
    
    /**
     * Unsets the "ItemName" element
     */
    void unsetItemName();
    
    /**
     * Gets the "ItemDescription" element
     */
    java.lang.String getItemDescription();
    
    /**
     * Gets (as xml) the "ItemDescription" element
     */
    com.idanalytics.products.common_v1.NormalizedString xgetItemDescription();
    
    /**
     * True if has "ItemDescription" element
     */
    boolean isSetItemDescription();
    
    /**
     * Sets the "ItemDescription" element
     */
    void setItemDescription(java.lang.String itemDescription);
    
    /**
     * Sets (as xml) the "ItemDescription" element
     */
    void xsetItemDescription(com.idanalytics.products.common_v1.NormalizedString itemDescription);
    
    /**
     * Unsets the "ItemDescription" element
     */
    void unsetItemDescription();
    
    /**
     * Gets the "ItemQuantity" element
     */
    java.lang.Object getItemQuantity();
    
    /**
     * Gets (as xml) the "ItemQuantity" element
     */
    com.idanalytics.products.common_v1.OptionalDecimal xgetItemQuantity();
    
    /**
     * True if has "ItemQuantity" element
     */
    boolean isSetItemQuantity();
    
    /**
     * Sets the "ItemQuantity" element
     */
    void setItemQuantity(java.lang.Object itemQuantity);
    
    /**
     * Sets (as xml) the "ItemQuantity" element
     */
    void xsetItemQuantity(com.idanalytics.products.common_v1.OptionalDecimal itemQuantity);
    
    /**
     * Unsets the "ItemQuantity" element
     */
    void unsetItemQuantity();
    
    /**
     * Gets the "ItemPrice" element
     */
    java.lang.Object getItemPrice();
    
    /**
     * Gets (as xml) the "ItemPrice" element
     */
    com.idanalytics.products.common_v1.OptionalDecimal xgetItemPrice();
    
    /**
     * True if has "ItemPrice" element
     */
    boolean isSetItemPrice();
    
    /**
     * Sets the "ItemPrice" element
     */
    void setItemPrice(java.lang.Object itemPrice);
    
    /**
     * Sets (as xml) the "ItemPrice" element
     */
    void xsetItemPrice(com.idanalytics.products.common_v1.OptionalDecimal itemPrice);
    
    /**
     * Unsets the "ItemPrice" element
     */
    void unsetItemPrice();
    
    /**
     * Gets the "ItemCategory" element
     */
    java.lang.String getItemCategory();
    
    /**
     * Gets (as xml) the "ItemCategory" element
     */
    com.idanalytics.products.common_v1.NormalizedString xgetItemCategory();
    
    /**
     * True if has "ItemCategory" element
     */
    boolean isSetItemCategory();
    
    /**
     * Sets the "ItemCategory" element
     */
    void setItemCategory(java.lang.String itemCategory);
    
    /**
     * Sets (as xml) the "ItemCategory" element
     */
    void xsetItemCategory(com.idanalytics.products.common_v1.NormalizedString itemCategory);
    
    /**
     * Unsets the "ItemCategory" element
     */
    void unsetItemCategory();
    
    /**
     * Gets the "Accessory" element
     */
    java.lang.String getAccessory();
    
    /**
     * Gets (as xml) the "Accessory" element
     */
    com.idanalytics.products.idscore.request.Accessory xgetAccessory();
    
    /**
     * True if has "Accessory" element
     */
    boolean isSetAccessory();
    
    /**
     * Sets the "Accessory" element
     */
    void setAccessory(java.lang.String accessory);
    
    /**
     * Sets (as xml) the "Accessory" element
     */
    void xsetAccessory(com.idanalytics.products.idscore.request.Accessory accessory);
    
    /**
     * Unsets the "Accessory" element
     */
    void unsetAccessory();
    
    /**
     * Gets the "ShippingDate" element
     */
    java.lang.Object getShippingDate();
    
    /**
     * Gets (as xml) the "ShippingDate" element
     */
    com.idanalytics.products.common_v1.OptionalDate xgetShippingDate();
    
    /**
     * True if has "ShippingDate" element
     */
    boolean isSetShippingDate();
    
    /**
     * Sets the "ShippingDate" element
     */
    void setShippingDate(java.lang.Object shippingDate);
    
    /**
     * Sets (as xml) the "ShippingDate" element
     */
    void xsetShippingDate(com.idanalytics.products.common_v1.OptionalDate shippingDate);
    
    /**
     * Unsets the "ShippingDate" element
     */
    void unsetShippingDate();
    
    /**
     * Gets the "DeliveryDate" element
     */
    java.lang.Object getDeliveryDate();
    
    /**
     * Gets (as xml) the "DeliveryDate" element
     */
    com.idanalytics.products.common_v1.OptionalDate xgetDeliveryDate();
    
    /**
     * True if has "DeliveryDate" element
     */
    boolean isSetDeliveryDate();
    
    /**
     * Sets the "DeliveryDate" element
     */
    void setDeliveryDate(java.lang.Object deliveryDate);
    
    /**
     * Sets (as xml) the "DeliveryDate" element
     */
    void xsetDeliveryDate(com.idanalytics.products.common_v1.OptionalDate deliveryDate);
    
    /**
     * Unsets the "DeliveryDate" element
     */
    void unsetDeliveryDate();
    
    /**
     * Gets the "CardMessage" element
     */
    java.lang.String getCardMessage();
    
    /**
     * Gets (as xml) the "CardMessage" element
     */
    org.apache.xmlbeans.XmlString xgetCardMessage();
    
    /**
     * True if has "CardMessage" element
     */
    boolean isSetCardMessage();
    
    /**
     * Sets the "CardMessage" element
     */
    void setCardMessage(java.lang.String cardMessage);
    
    /**
     * Sets (as xml) the "CardMessage" element
     */
    void xsetCardMessage(org.apache.xmlbeans.XmlString cardMessage);
    
    /**
     * Unsets the "CardMessage" element
     */
    void unsetCardMessage();
    
    /**
     * Gets the "Occasion" element
     */
    java.lang.String getOccasion();
    
    /**
     * Gets (as xml) the "Occasion" element
     */
    org.apache.xmlbeans.XmlString xgetOccasion();
    
    /**
     * True if has "Occasion" element
     */
    boolean isSetOccasion();
    
    /**
     * Sets the "Occasion" element
     */
    void setOccasion(java.lang.String occasion);
    
    /**
     * Sets (as xml) the "Occasion" element
     */
    void xsetOccasion(org.apache.xmlbeans.XmlString occasion);
    
    /**
     * Unsets the "Occasion" element
     */
    void unsetOccasion();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.idscore.request.ItemInfo newInstance() {
          return (com.idanalytics.products.idscore.request.ItemInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.idscore.request.ItemInfo newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.idscore.request.ItemInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.idscore.request.ItemInfo parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.ItemInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.idscore.request.ItemInfo parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.ItemInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.idscore.request.ItemInfo parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.ItemInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.idscore.request.ItemInfo parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.ItemInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.idscore.request.ItemInfo parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.ItemInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.idscore.request.ItemInfo parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.ItemInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.idscore.request.ItemInfo parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.ItemInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.idscore.request.ItemInfo parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.ItemInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.idscore.request.ItemInfo parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.ItemInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.idscore.request.ItemInfo parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.ItemInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.idscore.request.ItemInfo parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.ItemInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.idscore.request.ItemInfo parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.ItemInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.idscore.request.ItemInfo parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.ItemInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.idscore.request.ItemInfo parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.ItemInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.request.ItemInfo parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.request.ItemInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.request.ItemInfo parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.request.ItemInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
