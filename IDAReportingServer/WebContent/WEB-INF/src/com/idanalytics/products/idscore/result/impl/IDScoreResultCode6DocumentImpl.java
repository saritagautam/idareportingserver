/*
 * An XML document type.
 * Localname: IDScoreResultCode6
 * Namespace: http://idanalytics.com/products/idscore/result
 * Java type: com.idanalytics.products.idscore.result.IDScoreResultCode6Document
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.result.impl;
/**
 * A document containing one IDScoreResultCode6(@http://idanalytics.com/products/idscore/result) element.
 *
 * This is a complex type.
 */
public class IDScoreResultCode6DocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.result.IDScoreResultCode6Document
{
    private static final long serialVersionUID = 1L;
    
    public IDScoreResultCode6DocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IDSCORERESULTCODE6$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "IDScoreResultCode6");
    
    
    /**
     * Gets the "IDScoreResultCode6" element
     */
    public java.lang.String getIDScoreResultCode6()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDSCORERESULTCODE6$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IDScoreResultCode6" element
     */
    public com.idanalytics.products.idscore.result.IDScoreResultCode6Document.IDScoreResultCode6 xgetIDScoreResultCode6()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.IDScoreResultCode6Document.IDScoreResultCode6 target = null;
            target = (com.idanalytics.products.idscore.result.IDScoreResultCode6Document.IDScoreResultCode6)get_store().find_element_user(IDSCORERESULTCODE6$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IDScoreResultCode6" element
     */
    public void setIDScoreResultCode6(java.lang.String idScoreResultCode6)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDSCORERESULTCODE6$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDSCORERESULTCODE6$0);
            }
            target.setStringValue(idScoreResultCode6);
        }
    }
    
    /**
     * Sets (as xml) the "IDScoreResultCode6" element
     */
    public void xsetIDScoreResultCode6(com.idanalytics.products.idscore.result.IDScoreResultCode6Document.IDScoreResultCode6 idScoreResultCode6)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.IDScoreResultCode6Document.IDScoreResultCode6 target = null;
            target = (com.idanalytics.products.idscore.result.IDScoreResultCode6Document.IDScoreResultCode6)get_store().find_element_user(IDSCORERESULTCODE6$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.result.IDScoreResultCode6Document.IDScoreResultCode6)get_store().add_element_user(IDSCORERESULTCODE6$0);
            }
            target.set(idScoreResultCode6);
        }
    }
    /**
     * An XML IDScoreResultCode6(@http://idanalytics.com/products/idscore/result).
     *
     * This is a union type. Instances are of one of the following types:
     *     com.idanalytics.products.idscore.result.IDScoreResultCode6Document$IDScoreResultCode6$Member
     *     com.idanalytics.products.idscore.result.IDScoreResultCode6Document$IDScoreResultCode6$Member2
     */
    public static class IDScoreResultCode6Impl extends org.apache.xmlbeans.impl.values.XmlUnionImpl implements com.idanalytics.products.idscore.result.IDScoreResultCode6Document.IDScoreResultCode6, com.idanalytics.products.idscore.result.IDScoreResultCode6Document.IDScoreResultCode6.Member, com.idanalytics.products.idscore.result.IDScoreResultCode6Document.IDScoreResultCode6.Member2
    {
        private static final long serialVersionUID = 1L;
        
        public IDScoreResultCode6Impl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected IDScoreResultCode6Impl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
        /**
         * An anonymous inner XML type.
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result.IDScoreResultCode6Document$IDScoreResultCode6$Member.
         */
        public static class MemberImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.result.IDScoreResultCode6Document.IDScoreResultCode6.Member
        {
            private static final long serialVersionUID = 1L;
            
            public MemberImpl(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected MemberImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
        /**
         * An anonymous inner XML type.
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result.IDScoreResultCode6Document$IDScoreResultCode6$Member2.
         */
        public static class MemberImpl2 extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.result.IDScoreResultCode6Document.IDScoreResultCode6.Member2
        {
            private static final long serialVersionUID = 1L;
            
            public MemberImpl2(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected MemberImpl2(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
    }
}
