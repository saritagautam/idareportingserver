/*
 * An XML document type.
 * Localname: CardBrand
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.CardBrandDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * A document containing one CardBrand(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public class CardBrandDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.CardBrandDocument
{
    private static final long serialVersionUID = 1L;
    
    public CardBrandDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CARDBRAND$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "CardBrand");
    
    
    /**
     * Gets the "CardBrand" element
     */
    public java.lang.String getCardBrand()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CARDBRAND$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "CardBrand" element
     */
    public com.idanalytics.products.idscore.request.CardBrandDocument.CardBrand xgetCardBrand()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.CardBrandDocument.CardBrand target = null;
            target = (com.idanalytics.products.idscore.request.CardBrandDocument.CardBrand)get_store().find_element_user(CARDBRAND$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "CardBrand" element
     */
    public void setCardBrand(java.lang.String cardBrand)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CARDBRAND$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CARDBRAND$0);
            }
            target.setStringValue(cardBrand);
        }
    }
    
    /**
     * Sets (as xml) the "CardBrand" element
     */
    public void xsetCardBrand(com.idanalytics.products.idscore.request.CardBrandDocument.CardBrand cardBrand)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.CardBrandDocument.CardBrand target = null;
            target = (com.idanalytics.products.idscore.request.CardBrandDocument.CardBrand)get_store().find_element_user(CARDBRAND$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.CardBrandDocument.CardBrand)get_store().add_element_user(CARDBRAND$0);
            }
            target.set(cardBrand);
        }
    }
    /**
     * An XML CardBrand(@http://idanalytics.com/products/idscore/request).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.CardBrandDocument$CardBrand.
     */
    public static class CardBrandImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.request.CardBrandDocument.CardBrand
    {
        private static final long serialVersionUID = 1L;
        
        public CardBrandImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected CardBrandImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
