/*
 * XML Type:  ParsedAddress
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.ParsedAddress
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request;


/**
 * An XML ParsedAddress(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public interface ParsedAddress extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ParsedAddress.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("parsedaddress1dc9type");
    
    /**
     * Gets the "StreetDir" element
     */
    java.lang.String getStreetDir();
    
    /**
     * Gets (as xml) the "StreetDir" element
     */
    com.idanalytics.products.idscore.request.Collapsed xgetStreetDir();
    
    /**
     * Sets the "StreetDir" element
     */
    void setStreetDir(java.lang.String streetDir);
    
    /**
     * Sets (as xml) the "StreetDir" element
     */
    void xsetStreetDir(com.idanalytics.products.idscore.request.Collapsed streetDir);
    
    /**
     * Gets the "Street" element
     */
    java.lang.String getStreet();
    
    /**
     * Gets (as xml) the "Street" element
     */
    com.idanalytics.products.idscore.request.Collapsed xgetStreet();
    
    /**
     * Sets the "Street" element
     */
    void setStreet(java.lang.String street);
    
    /**
     * Sets (as xml) the "Street" element
     */
    void xsetStreet(com.idanalytics.products.idscore.request.Collapsed street);
    
    /**
     * Gets the "StreetNumber" element
     */
    java.lang.String getStreetNumber();
    
    /**
     * Gets (as xml) the "StreetNumber" element
     */
    com.idanalytics.products.idscore.request.Collapsed xgetStreetNumber();
    
    /**
     * Sets the "StreetNumber" element
     */
    void setStreetNumber(java.lang.String streetNumber);
    
    /**
     * Sets (as xml) the "StreetNumber" element
     */
    void xsetStreetNumber(com.idanalytics.products.idscore.request.Collapsed streetNumber);
    
    /**
     * Gets the "StreetType" element
     */
    java.lang.String getStreetType();
    
    /**
     * Gets (as xml) the "StreetType" element
     */
    com.idanalytics.products.idscore.request.Collapsed xgetStreetType();
    
    /**
     * Sets the "StreetType" element
     */
    void setStreetType(java.lang.String streetType);
    
    /**
     * Sets (as xml) the "StreetType" element
     */
    void xsetStreetType(com.idanalytics.products.idscore.request.Collapsed streetType);
    
    /**
     * Gets the "AptNumber" element
     */
    java.lang.String getAptNumber();
    
    /**
     * Gets (as xml) the "AptNumber" element
     */
    com.idanalytics.products.idscore.request.Collapsed xgetAptNumber();
    
    /**
     * True if has "AptNumber" element
     */
    boolean isSetAptNumber();
    
    /**
     * Sets the "AptNumber" element
     */
    void setAptNumber(java.lang.String aptNumber);
    
    /**
     * Sets (as xml) the "AptNumber" element
     */
    void xsetAptNumber(com.idanalytics.products.idscore.request.Collapsed aptNumber);
    
    /**
     * Unsets the "AptNumber" element
     */
    void unsetAptNumber();
    
    /**
     * Gets the "POBox" element
     */
    java.lang.String getPOBox();
    
    /**
     * Gets (as xml) the "POBox" element
     */
    com.idanalytics.products.idscore.request.Collapsed xgetPOBox();
    
    /**
     * True if has "POBox" element
     */
    boolean isSetPOBox();
    
    /**
     * Sets the "POBox" element
     */
    void setPOBox(java.lang.String poBox);
    
    /**
     * Sets (as xml) the "POBox" element
     */
    void xsetPOBox(com.idanalytics.products.idscore.request.Collapsed poBox);
    
    /**
     * Unsets the "POBox" element
     */
    void unsetPOBox();
    
    /**
     * Gets the "RuralRoute" element
     */
    java.lang.String getRuralRoute();
    
    /**
     * Gets (as xml) the "RuralRoute" element
     */
    com.idanalytics.products.idscore.request.Collapsed xgetRuralRoute();
    
    /**
     * True if has "RuralRoute" element
     */
    boolean isSetRuralRoute();
    
    /**
     * Sets the "RuralRoute" element
     */
    void setRuralRoute(java.lang.String ruralRoute);
    
    /**
     * Sets (as xml) the "RuralRoute" element
     */
    void xsetRuralRoute(com.idanalytics.products.idscore.request.Collapsed ruralRoute);
    
    /**
     * Unsets the "RuralRoute" element
     */
    void unsetRuralRoute();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.idscore.request.ParsedAddress newInstance() {
          return (com.idanalytics.products.idscore.request.ParsedAddress) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.idscore.request.ParsedAddress newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.idscore.request.ParsedAddress) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.idscore.request.ParsedAddress parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.ParsedAddress) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.idscore.request.ParsedAddress parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.ParsedAddress) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.idscore.request.ParsedAddress parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.ParsedAddress) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.idscore.request.ParsedAddress parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.ParsedAddress) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.idscore.request.ParsedAddress parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.ParsedAddress) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.idscore.request.ParsedAddress parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.ParsedAddress) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.idscore.request.ParsedAddress parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.ParsedAddress) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.idscore.request.ParsedAddress parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.ParsedAddress) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.idscore.request.ParsedAddress parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.ParsedAddress) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.idscore.request.ParsedAddress parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.ParsedAddress) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.idscore.request.ParsedAddress parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.ParsedAddress) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.idscore.request.ParsedAddress parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.ParsedAddress) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.idscore.request.ParsedAddress parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.ParsedAddress) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.idscore.request.ParsedAddress parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.ParsedAddress) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.request.ParsedAddress parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.request.ParsedAddress) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.request.ParsedAddress parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.request.ParsedAddress) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
