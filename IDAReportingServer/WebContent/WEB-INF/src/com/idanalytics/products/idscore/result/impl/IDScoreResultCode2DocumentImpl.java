/*
 * An XML document type.
 * Localname: IDScoreResultCode2
 * Namespace: http://idanalytics.com/products/idscore/result
 * Java type: com.idanalytics.products.idscore.result.IDScoreResultCode2Document
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.result.impl;
/**
 * A document containing one IDScoreResultCode2(@http://idanalytics.com/products/idscore/result) element.
 *
 * This is a complex type.
 */
public class IDScoreResultCode2DocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.result.IDScoreResultCode2Document
{
    private static final long serialVersionUID = 1L;
    
    public IDScoreResultCode2DocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IDSCORERESULTCODE2$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "IDScoreResultCode2");
    
    
    /**
     * Gets the "IDScoreResultCode2" element
     */
    public java.lang.String getIDScoreResultCode2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDSCORERESULTCODE2$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IDScoreResultCode2" element
     */
    public com.idanalytics.products.idscore.result.IDScoreResultCode2Document.IDScoreResultCode2 xgetIDScoreResultCode2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.IDScoreResultCode2Document.IDScoreResultCode2 target = null;
            target = (com.idanalytics.products.idscore.result.IDScoreResultCode2Document.IDScoreResultCode2)get_store().find_element_user(IDSCORERESULTCODE2$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IDScoreResultCode2" element
     */
    public void setIDScoreResultCode2(java.lang.String idScoreResultCode2)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDSCORERESULTCODE2$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDSCORERESULTCODE2$0);
            }
            target.setStringValue(idScoreResultCode2);
        }
    }
    
    /**
     * Sets (as xml) the "IDScoreResultCode2" element
     */
    public void xsetIDScoreResultCode2(com.idanalytics.products.idscore.result.IDScoreResultCode2Document.IDScoreResultCode2 idScoreResultCode2)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.IDScoreResultCode2Document.IDScoreResultCode2 target = null;
            target = (com.idanalytics.products.idscore.result.IDScoreResultCode2Document.IDScoreResultCode2)get_store().find_element_user(IDSCORERESULTCODE2$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.result.IDScoreResultCode2Document.IDScoreResultCode2)get_store().add_element_user(IDSCORERESULTCODE2$0);
            }
            target.set(idScoreResultCode2);
        }
    }
    /**
     * An XML IDScoreResultCode2(@http://idanalytics.com/products/idscore/result).
     *
     * This is a union type. Instances are of one of the following types:
     *     com.idanalytics.products.idscore.result.IDScoreResultCode2Document$IDScoreResultCode2$Member
     *     com.idanalytics.products.idscore.result.IDScoreResultCode2Document$IDScoreResultCode2$Member2
     */
    public static class IDScoreResultCode2Impl extends org.apache.xmlbeans.impl.values.XmlUnionImpl implements com.idanalytics.products.idscore.result.IDScoreResultCode2Document.IDScoreResultCode2, com.idanalytics.products.idscore.result.IDScoreResultCode2Document.IDScoreResultCode2.Member, com.idanalytics.products.idscore.result.IDScoreResultCode2Document.IDScoreResultCode2.Member2
    {
        private static final long serialVersionUID = 1L;
        
        public IDScoreResultCode2Impl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected IDScoreResultCode2Impl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
        /**
         * An anonymous inner XML type.
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result.IDScoreResultCode2Document$IDScoreResultCode2$Member.
         */
        public static class MemberImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.result.IDScoreResultCode2Document.IDScoreResultCode2.Member
        {
            private static final long serialVersionUID = 1L;
            
            public MemberImpl(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected MemberImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
        /**
         * An anonymous inner XML type.
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result.IDScoreResultCode2Document$IDScoreResultCode2$Member2.
         */
        public static class MemberImpl2 extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.result.IDScoreResultCode2Document.IDScoreResultCode2.Member2
        {
            private static final long serialVersionUID = 1L;
            
            public MemberImpl2(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected MemberImpl2(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
    }
}
