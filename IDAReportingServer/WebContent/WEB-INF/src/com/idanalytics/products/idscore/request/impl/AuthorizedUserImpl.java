/*
 * XML Type:  AuthorizedUser
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.AuthorizedUser
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML AuthorizedUser(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public class AuthorizedUserImpl extends com.idanalytics.products.idscore.request.impl.IdentityImpl implements com.idanalytics.products.idscore.request.AuthorizedUser
{
    private static final long serialVersionUID = 1L;
    
    public AuthorizedUserImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName RELATIONSHIP$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Relationship");
    
    
    /**
     * Gets the "Relationship" element
     */
    public java.lang.String getRelationship()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RELATIONSHIP$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Relationship" element
     */
    public com.idanalytics.products.idscore.request.Relationship xgetRelationship()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Relationship target = null;
            target = (com.idanalytics.products.idscore.request.Relationship)get_store().find_element_user(RELATIONSHIP$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Relationship" element
     */
    public void setRelationship(java.lang.String relationship)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(RELATIONSHIP$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(RELATIONSHIP$0);
            }
            target.setStringValue(relationship);
        }
    }
    
    /**
     * Sets (as xml) the "Relationship" element
     */
    public void xsetRelationship(com.idanalytics.products.idscore.request.Relationship relationship)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Relationship target = null;
            target = (com.idanalytics.products.idscore.request.Relationship)get_store().find_element_user(RELATIONSHIP$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.Relationship)get_store().add_element_user(RELATIONSHIP$0);
            }
            target.set(relationship);
        }
    }
}
