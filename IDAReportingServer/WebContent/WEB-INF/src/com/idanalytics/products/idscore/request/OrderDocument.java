/*
 * An XML document type.
 * Localname: Order
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.OrderDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request;


/**
 * A document containing one Order(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public interface OrderDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(OrderDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("orderc364doctype");
    
    /**
     * Gets the "Order" element
     */
    com.idanalytics.products.idscore.request.OrderDocument.Order getOrder();
    
    /**
     * Sets the "Order" element
     */
    void setOrder(com.idanalytics.products.idscore.request.OrderDocument.Order order);
    
    /**
     * Appends and returns a new empty "Order" element
     */
    com.idanalytics.products.idscore.request.OrderDocument.Order addNewOrder();
    
    /**
     * An XML Order(@http://idanalytics.com/products/idscore/request).
     *
     * This is a complex type.
     */
    public interface Order extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Order.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("order5afeelemtype");
        
        /**
         * Gets the "OrderHeader" element
         */
        com.idanalytics.products.idscore.request.OrderHeaderType getOrderHeader();
        
        /**
         * Sets the "OrderHeader" element
         */
        void setOrderHeader(com.idanalytics.products.idscore.request.OrderHeaderType orderHeader);
        
        /**
         * Appends and returns a new empty "OrderHeader" element
         */
        com.idanalytics.products.idscore.request.OrderHeaderType addNewOrderHeader();
        
        /**
         * Gets the "BillingIdentity" element
         */
        com.idanalytics.products.idscore.request.OrderIdentity getBillingIdentity();
        
        /**
         * Sets the "BillingIdentity" element
         */
        void setBillingIdentity(com.idanalytics.products.idscore.request.OrderIdentity billingIdentity);
        
        /**
         * Appends and returns a new empty "BillingIdentity" element
         */
        com.idanalytics.products.idscore.request.OrderIdentity addNewBillingIdentity();
        
        /**
         * Gets array of all "ShippingIdentity" elements
         */
        com.idanalytics.products.idscore.request.OrderIdentity[] getShippingIdentityArray();
        
        /**
         * Gets ith "ShippingIdentity" element
         */
        com.idanalytics.products.idscore.request.OrderIdentity getShippingIdentityArray(int i);
        
        /**
         * Returns number of "ShippingIdentity" element
         */
        int sizeOfShippingIdentityArray();
        
        /**
         * Sets array of all "ShippingIdentity" element
         */
        void setShippingIdentityArray(com.idanalytics.products.idscore.request.OrderIdentity[] shippingIdentityArray);
        
        /**
         * Sets ith "ShippingIdentity" element
         */
        void setShippingIdentityArray(int i, com.idanalytics.products.idscore.request.OrderIdentity shippingIdentity);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "ShippingIdentity" element
         */
        com.idanalytics.products.idscore.request.OrderIdentity insertNewShippingIdentity(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "ShippingIdentity" element
         */
        com.idanalytics.products.idscore.request.OrderIdentity addNewShippingIdentity();
        
        /**
         * Removes the ith "ShippingIdentity" element
         */
        void removeShippingIdentity(int i);
        
        /**
         * Gets the "IDAInternal" element
         */
        com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal getIDAInternal();
        
        /**
         * True if has "IDAInternal" element
         */
        boolean isSetIDAInternal();
        
        /**
         * Sets the "IDAInternal" element
         */
        void setIDAInternal(com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal idaInternal);
        
        /**
         * Appends and returns a new empty "IDAInternal" element
         */
        com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal addNewIDAInternal();
        
        /**
         * Unsets the "IDAInternal" element
         */
        void unsetIDAInternal();
        
        /**
         * Gets the "schemaVersion" attribute
         */
        java.math.BigDecimal getSchemaVersion();
        
        /**
         * Gets (as xml) the "schemaVersion" attribute
         */
        org.apache.xmlbeans.XmlDecimal xgetSchemaVersion();
        
        /**
         * True if has "schemaVersion" attribute
         */
        boolean isSetSchemaVersion();
        
        /**
         * Sets the "schemaVersion" attribute
         */
        void setSchemaVersion(java.math.BigDecimal schemaVersion);
        
        /**
         * Sets (as xml) the "schemaVersion" attribute
         */
        void xsetSchemaVersion(org.apache.xmlbeans.XmlDecimal schemaVersion);
        
        /**
         * Unsets the "schemaVersion" attribute
         */
        void unsetSchemaVersion();
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.idscore.request.OrderDocument.Order newInstance() {
              return (com.idanalytics.products.idscore.request.OrderDocument.Order) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.idscore.request.OrderDocument.Order newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.idscore.request.OrderDocument.Order) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.idscore.request.OrderDocument newInstance() {
          return (com.idanalytics.products.idscore.request.OrderDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.idscore.request.OrderDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.idscore.request.OrderDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.idscore.request.OrderDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.OrderDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.idscore.request.OrderDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.OrderDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.idscore.request.OrderDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.OrderDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.idscore.request.OrderDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.OrderDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.idscore.request.OrderDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.OrderDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.idscore.request.OrderDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.OrderDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.idscore.request.OrderDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.OrderDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.idscore.request.OrderDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.OrderDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.idscore.request.OrderDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.OrderDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.idscore.request.OrderDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.OrderDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.idscore.request.OrderDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.OrderDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.idscore.request.OrderDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.OrderDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.idscore.request.OrderDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.OrderDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.idscore.request.OrderDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.OrderDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.request.OrderDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.request.OrderDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.request.OrderDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.request.OrderDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
