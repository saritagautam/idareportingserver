/*
 * XML Type:  VariableGroup
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.VariableGroup
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML VariableGroup(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public class VariableGroupImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.VariableGroup
{
    private static final long serialVersionUID = 1L;
    
    public VariableGroupImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName VARIABLE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Variable");
    private static final javax.xml.namespace.QName NAME$2 = 
        new javax.xml.namespace.QName("", "name");
    
    
    /**
     * Gets array of all "Variable" elements
     */
    public com.idanalytics.products.idscore.request.Variable[] getVariableArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(VARIABLE$0, targetList);
            com.idanalytics.products.idscore.request.Variable[] result = new com.idanalytics.products.idscore.request.Variable[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "Variable" element
     */
    public com.idanalytics.products.idscore.request.Variable getVariableArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Variable target = null;
            target = (com.idanalytics.products.idscore.request.Variable)get_store().find_element_user(VARIABLE$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "Variable" element
     */
    public int sizeOfVariableArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(VARIABLE$0);
        }
    }
    
    /**
     * Sets array of all "Variable" element  WARNING: This method is not atomicaly synchronized.
     */
    public void setVariableArray(com.idanalytics.products.idscore.request.Variable[] variableArray)
    {
        check_orphaned();
        arraySetterHelper(variableArray, VARIABLE$0);
    }
    
    /**
     * Sets ith "Variable" element
     */
    public void setVariableArray(int i, com.idanalytics.products.idscore.request.Variable variable)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Variable target = null;
            target = (com.idanalytics.products.idscore.request.Variable)get_store().find_element_user(VARIABLE$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(variable);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "Variable" element
     */
    public com.idanalytics.products.idscore.request.Variable insertNewVariable(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Variable target = null;
            target = (com.idanalytics.products.idscore.request.Variable)get_store().insert_element_user(VARIABLE$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "Variable" element
     */
    public com.idanalytics.products.idscore.request.Variable addNewVariable()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.Variable target = null;
            target = (com.idanalytics.products.idscore.request.Variable)get_store().add_element_user(VARIABLE$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "Variable" element
     */
    public void removeVariable(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(VARIABLE$0, i);
        }
    }
    
    /**
     * Gets the "name" attribute
     */
    public java.lang.String getName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(NAME$2);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "name" attribute
     */
    public org.apache.xmlbeans.XmlString xgetName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_attribute_user(NAME$2);
            return target;
        }
    }
    
    /**
     * Sets the "name" attribute
     */
    public void setName(java.lang.String name)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(NAME$2);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(NAME$2);
            }
            target.setStringValue(name);
        }
    }
    
    /**
     * Sets (as xml) the "name" attribute
     */
    public void xsetName(org.apache.xmlbeans.XmlString name)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_attribute_user(NAME$2);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_attribute_user(NAME$2);
            }
            target.set(name);
        }
    }
}
