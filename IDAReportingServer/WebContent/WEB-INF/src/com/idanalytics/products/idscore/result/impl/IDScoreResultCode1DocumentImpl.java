/*
 * An XML document type.
 * Localname: IDScoreResultCode1
 * Namespace: http://idanalytics.com/products/idscore/result
 * Java type: com.idanalytics.products.idscore.result.IDScoreResultCode1Document
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.result.impl;
/**
 * A document containing one IDScoreResultCode1(@http://idanalytics.com/products/idscore/result) element.
 *
 * This is a complex type.
 */
public class IDScoreResultCode1DocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.result.IDScoreResultCode1Document
{
    private static final long serialVersionUID = 1L;
    
    public IDScoreResultCode1DocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IDSCORERESULTCODE1$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "IDScoreResultCode1");
    
    
    /**
     * Gets the "IDScoreResultCode1" element
     */
    public java.lang.String getIDScoreResultCode1()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDSCORERESULTCODE1$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IDScoreResultCode1" element
     */
    public com.idanalytics.products.idscore.result.IDScoreResultCode1Document.IDScoreResultCode1 xgetIDScoreResultCode1()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.IDScoreResultCode1Document.IDScoreResultCode1 target = null;
            target = (com.idanalytics.products.idscore.result.IDScoreResultCode1Document.IDScoreResultCode1)get_store().find_element_user(IDSCORERESULTCODE1$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IDScoreResultCode1" element
     */
    public void setIDScoreResultCode1(java.lang.String idScoreResultCode1)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDSCORERESULTCODE1$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDSCORERESULTCODE1$0);
            }
            target.setStringValue(idScoreResultCode1);
        }
    }
    
    /**
     * Sets (as xml) the "IDScoreResultCode1" element
     */
    public void xsetIDScoreResultCode1(com.idanalytics.products.idscore.result.IDScoreResultCode1Document.IDScoreResultCode1 idScoreResultCode1)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.IDScoreResultCode1Document.IDScoreResultCode1 target = null;
            target = (com.idanalytics.products.idscore.result.IDScoreResultCode1Document.IDScoreResultCode1)get_store().find_element_user(IDSCORERESULTCODE1$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.result.IDScoreResultCode1Document.IDScoreResultCode1)get_store().add_element_user(IDSCORERESULTCODE1$0);
            }
            target.set(idScoreResultCode1);
        }
    }
    /**
     * An XML IDScoreResultCode1(@http://idanalytics.com/products/idscore/result).
     *
     * This is a union type. Instances are of one of the following types:
     *     com.idanalytics.products.idscore.result.IDScoreResultCode1Document$IDScoreResultCode1$Member
     *     com.idanalytics.products.idscore.result.IDScoreResultCode1Document$IDScoreResultCode1$Member2
     */
    public static class IDScoreResultCode1Impl extends org.apache.xmlbeans.impl.values.XmlUnionImpl implements com.idanalytics.products.idscore.result.IDScoreResultCode1Document.IDScoreResultCode1, com.idanalytics.products.idscore.result.IDScoreResultCode1Document.IDScoreResultCode1.Member, com.idanalytics.products.idscore.result.IDScoreResultCode1Document.IDScoreResultCode1.Member2
    {
        private static final long serialVersionUID = 1L;
        
        public IDScoreResultCode1Impl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected IDScoreResultCode1Impl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
        /**
         * An anonymous inner XML type.
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result.IDScoreResultCode1Document$IDScoreResultCode1$Member.
         */
        public static class MemberImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.result.IDScoreResultCode1Document.IDScoreResultCode1.Member
        {
            private static final long serialVersionUID = 1L;
            
            public MemberImpl(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected MemberImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
        /**
         * An anonymous inner XML type.
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result.IDScoreResultCode1Document$IDScoreResultCode1$Member2.
         */
        public static class MemberImpl2 extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.result.IDScoreResultCode1Document.IDScoreResultCode1.Member2
        {
            private static final long serialVersionUID = 1L;
            
            public MemberImpl2(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected MemberImpl2(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
    }
}
