/*
 * An XML document type.
 * Localname: ConsumerID
 * Namespace: http://idanalytics.com/products/idscore/result
 * Java type: com.idanalytics.products.idscore.result.ConsumerIDDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.result.impl;
/**
 * A document containing one ConsumerID(@http://idanalytics.com/products/idscore/result) element.
 *
 * This is a complex type.
 */
public class ConsumerIDDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.result.ConsumerIDDocument
{
    private static final long serialVersionUID = 1L;
    
    public ConsumerIDDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CONSUMERID$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "ConsumerID");
    
    
    /**
     * Gets the "ConsumerID" element
     */
    public java.lang.String getConsumerID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CONSUMERID$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "ConsumerID" element
     */
    public com.idanalytics.products.idscore.result.ConsumerIDDocument.ConsumerID xgetConsumerID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.ConsumerIDDocument.ConsumerID target = null;
            target = (com.idanalytics.products.idscore.result.ConsumerIDDocument.ConsumerID)get_store().find_element_user(CONSUMERID$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "ConsumerID" element
     */
    public void setConsumerID(java.lang.String consumerID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CONSUMERID$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CONSUMERID$0);
            }
            target.setStringValue(consumerID);
        }
    }
    
    /**
     * Sets (as xml) the "ConsumerID" element
     */
    public void xsetConsumerID(com.idanalytics.products.idscore.result.ConsumerIDDocument.ConsumerID consumerID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.ConsumerIDDocument.ConsumerID target = null;
            target = (com.idanalytics.products.idscore.result.ConsumerIDDocument.ConsumerID)get_store().find_element_user(CONSUMERID$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.result.ConsumerIDDocument.ConsumerID)get_store().add_element_user(CONSUMERID$0);
            }
            target.set(consumerID);
        }
    }
    /**
     * An XML ConsumerID(@http://idanalytics.com/products/idscore/result).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result.ConsumerIDDocument$ConsumerID.
     */
    public static class ConsumerIDImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.result.ConsumerIDDocument.ConsumerID
    {
        private static final long serialVersionUID = 1L;
        
        public ConsumerIDImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected ConsumerIDImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
