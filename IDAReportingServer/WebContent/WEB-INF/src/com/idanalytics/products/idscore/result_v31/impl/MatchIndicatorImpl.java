/*
 * XML Type:  MatchIndicator
 * Namespace: http://idanalytics.com/products/idscore/result.v31
 * Java type: com.idanalytics.products.idscore.result_v31.MatchIndicator
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.result_v31.impl;
/**
 * An XML MatchIndicator(@http://idanalytics.com/products/idscore/result.v31).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result_v31.MatchIndicator.
 */
public class MatchIndicatorImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.idanalytics.products.idscore.result_v31.MatchIndicator
{
    private static final long serialVersionUID = 1L;
    
    public MatchIndicatorImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected MatchIndicatorImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
