/*
 * An XML document type.
 * Localname: OutputRecord
 * Namespace: http://idanalytics.com/products/idscore/result.v31
 * Java type: com.idanalytics.products.idscore.result_v31.OutputRecordDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.result_v31.impl;
/**
 * A document containing one OutputRecord(@http://idanalytics.com/products/idscore/result.v31) element.
 *
 * This is a complex type.
 */
public class OutputRecordDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.result_v31.OutputRecordDocument
{
    private static final long serialVersionUID = 1L;
    
    public OutputRecordDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName OUTPUTRECORD$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result.v31", "OutputRecord");
    
    
    /**
     * Gets the "OutputRecord" element
     */
    public com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord getOutputRecord()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord target = null;
            target = (com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord)get_store().find_element_user(OUTPUTRECORD$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "OutputRecord" element
     */
    public void setOutputRecord(com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord outputRecord)
    {
        generatedSetterHelperImpl(outputRecord, OUTPUTRECORD$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "OutputRecord" element
     */
    public com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord addNewOutputRecord()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord target = null;
            target = (com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord)get_store().add_element_user(OUTPUTRECORD$0);
            return target;
        }
    }
    /**
     * An XML OutputRecord(@http://idanalytics.com/products/idscore/result.v31).
     *
     * This is a complex type.
     */
    public static class OutputRecordImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord
    {
        private static final long serialVersionUID = 1L;
        
        public OutputRecordImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName IDASTATUS$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result.v31", "IDAStatus");
        private static final javax.xml.namespace.QName APPID$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result.v31", "AppID");
        private static final javax.xml.namespace.QName CLIENTKEY$4 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result.v31", "ClientKey");
        private static final javax.xml.namespace.QName CONSUMERID$6 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result.v31", "ConsumerID");
        private static final javax.xml.namespace.QName DESIGNATION$8 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result.v31", "Designation");
        private static final javax.xml.namespace.QName ACCTLINKKEY$10 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result.v31", "AcctLinkKey");
        private static final javax.xml.namespace.QName IDASEQUENCE$12 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result.v31", "IDASequence");
        private static final javax.xml.namespace.QName IDATIMESTAMP$14 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result.v31", "IDATimeStamp");
        private static final javax.xml.namespace.QName SCORE$16 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result.v31", "Score");
        private static final javax.xml.namespace.QName CONSUMERSTATEMENTS$18 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result.v31", "ConsumerStatements");
        private static final javax.xml.namespace.QName MESSAGES$20 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result.v31", "Messages");
        private static final javax.xml.namespace.QName PASSTHRU1$22 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result.v31", "PassThru1");
        private static final javax.xml.namespace.QName PASSTHRU2$24 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result.v31", "PassThru2");
        private static final javax.xml.namespace.QName PASSTHRU3$26 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result.v31", "PassThru3");
        private static final javax.xml.namespace.QName PASSTHRU4$28 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result.v31", "PassThru4");
        private static final javax.xml.namespace.QName INDICATORS$30 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result.v31", "Indicators");
        private static final javax.xml.namespace.QName IDAINTERNAL$32 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result.v31", "IDAInternal");
        private static final javax.xml.namespace.QName PREVIOUSEVENTS$34 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result.v31", "PreviousEvents");
        private static final javax.xml.namespace.QName SCHEMAVERSION$36 = 
            new javax.xml.namespace.QName("", "schemaVersion");
        
        
        /**
         * Gets the "IDAStatus" element
         */
        public int getIDAStatus()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASTATUS$0, 0);
                if (target == null)
                {
                    return 0;
                }
                return target.getIntValue();
            }
        }
        
        /**
         * Gets (as xml) the "IDAStatus" element
         */
        public com.idanalytics.products.idscore.result_v31.IDAStatusDocument.IDAStatus xgetIDAStatus()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.IDAStatusDocument.IDAStatus target = null;
                target = (com.idanalytics.products.idscore.result_v31.IDAStatusDocument.IDAStatus)get_store().find_element_user(IDASTATUS$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "IDAStatus" element
         */
        public void setIDAStatus(int idaStatus)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASTATUS$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDASTATUS$0);
                }
                target.setIntValue(idaStatus);
            }
        }
        
        /**
         * Sets (as xml) the "IDAStatus" element
         */
        public void xsetIDAStatus(com.idanalytics.products.idscore.result_v31.IDAStatusDocument.IDAStatus idaStatus)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.IDAStatusDocument.IDAStatus target = null;
                target = (com.idanalytics.products.idscore.result_v31.IDAStatusDocument.IDAStatus)get_store().find_element_user(IDASTATUS$0, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result_v31.IDAStatusDocument.IDAStatus)get_store().add_element_user(IDASTATUS$0);
                }
                target.set(idaStatus);
            }
        }
        
        /**
         * Gets the "AppID" element
         */
        public java.lang.String getAppID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(APPID$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "AppID" element
         */
        public com.idanalytics.products.common_v1.AppID xgetAppID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.AppID target = null;
                target = (com.idanalytics.products.common_v1.AppID)get_store().find_element_user(APPID$2, 0);
                return target;
            }
        }
        
        /**
         * True if has "AppID" element
         */
        public boolean isSetAppID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(APPID$2) != 0;
            }
        }
        
        /**
         * Sets the "AppID" element
         */
        public void setAppID(java.lang.String appID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(APPID$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(APPID$2);
                }
                target.setStringValue(appID);
            }
        }
        
        /**
         * Sets (as xml) the "AppID" element
         */
        public void xsetAppID(com.idanalytics.products.common_v1.AppID appID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.AppID target = null;
                target = (com.idanalytics.products.common_v1.AppID)get_store().find_element_user(APPID$2, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.AppID)get_store().add_element_user(APPID$2);
                }
                target.set(appID);
            }
        }
        
        /**
         * Unsets the "AppID" element
         */
        public void unsetAppID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(APPID$2, 0);
            }
        }
        
        /**
         * Gets the "ClientKey" element
         */
        public java.lang.String getClientKey()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CLIENTKEY$4, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "ClientKey" element
         */
        public com.idanalytics.products.idscore.result_v31.ClientKeyDocument.ClientKey xgetClientKey()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.ClientKeyDocument.ClientKey target = null;
                target = (com.idanalytics.products.idscore.result_v31.ClientKeyDocument.ClientKey)get_store().find_element_user(CLIENTKEY$4, 0);
                return target;
            }
        }
        
        /**
         * True if has "ClientKey" element
         */
        public boolean isSetClientKey()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(CLIENTKEY$4) != 0;
            }
        }
        
        /**
         * Sets the "ClientKey" element
         */
        public void setClientKey(java.lang.String clientKey)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CLIENTKEY$4, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CLIENTKEY$4);
                }
                target.setStringValue(clientKey);
            }
        }
        
        /**
         * Sets (as xml) the "ClientKey" element
         */
        public void xsetClientKey(com.idanalytics.products.idscore.result_v31.ClientKeyDocument.ClientKey clientKey)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.ClientKeyDocument.ClientKey target = null;
                target = (com.idanalytics.products.idscore.result_v31.ClientKeyDocument.ClientKey)get_store().find_element_user(CLIENTKEY$4, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result_v31.ClientKeyDocument.ClientKey)get_store().add_element_user(CLIENTKEY$4);
                }
                target.set(clientKey);
            }
        }
        
        /**
         * Unsets the "ClientKey" element
         */
        public void unsetClientKey()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(CLIENTKEY$4, 0);
            }
        }
        
        /**
         * Gets the "ConsumerID" element
         */
        public java.lang.String getConsumerID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CONSUMERID$6, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "ConsumerID" element
         */
        public com.idanalytics.products.idscore.result_v31.ConsumerIDDocument.ConsumerID xgetConsumerID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.ConsumerIDDocument.ConsumerID target = null;
                target = (com.idanalytics.products.idscore.result_v31.ConsumerIDDocument.ConsumerID)get_store().find_element_user(CONSUMERID$6, 0);
                return target;
            }
        }
        
        /**
         * True if has "ConsumerID" element
         */
        public boolean isSetConsumerID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(CONSUMERID$6) != 0;
            }
        }
        
        /**
         * Sets the "ConsumerID" element
         */
        public void setConsumerID(java.lang.String consumerID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CONSUMERID$6, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CONSUMERID$6);
                }
                target.setStringValue(consumerID);
            }
        }
        
        /**
         * Sets (as xml) the "ConsumerID" element
         */
        public void xsetConsumerID(com.idanalytics.products.idscore.result_v31.ConsumerIDDocument.ConsumerID consumerID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.ConsumerIDDocument.ConsumerID target = null;
                target = (com.idanalytics.products.idscore.result_v31.ConsumerIDDocument.ConsumerID)get_store().find_element_user(CONSUMERID$6, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result_v31.ConsumerIDDocument.ConsumerID)get_store().add_element_user(CONSUMERID$6);
                }
                target.set(consumerID);
            }
        }
        
        /**
         * Unsets the "ConsumerID" element
         */
        public void unsetConsumerID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(CONSUMERID$6, 0);
            }
        }
        
        /**
         * Gets the "Designation" element
         */
        public com.idanalytics.products.idscore.result_v31.DesignationDocument.Designation.Enum getDesignation()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESIGNATION$8, 0);
                if (target == null)
                {
                    return null;
                }
                return (com.idanalytics.products.idscore.result_v31.DesignationDocument.Designation.Enum)target.getEnumValue();
            }
        }
        
        /**
         * Gets (as xml) the "Designation" element
         */
        public com.idanalytics.products.idscore.result_v31.DesignationDocument.Designation xgetDesignation()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.DesignationDocument.Designation target = null;
                target = (com.idanalytics.products.idscore.result_v31.DesignationDocument.Designation)get_store().find_element_user(DESIGNATION$8, 0);
                return target;
            }
        }
        
        /**
         * True if has "Designation" element
         */
        public boolean isSetDesignation()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(DESIGNATION$8) != 0;
            }
        }
        
        /**
         * Sets the "Designation" element
         */
        public void setDesignation(com.idanalytics.products.idscore.result_v31.DesignationDocument.Designation.Enum designation)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESIGNATION$8, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DESIGNATION$8);
                }
                target.setEnumValue(designation);
            }
        }
        
        /**
         * Sets (as xml) the "Designation" element
         */
        public void xsetDesignation(com.idanalytics.products.idscore.result_v31.DesignationDocument.Designation designation)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.DesignationDocument.Designation target = null;
                target = (com.idanalytics.products.idscore.result_v31.DesignationDocument.Designation)get_store().find_element_user(DESIGNATION$8, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result_v31.DesignationDocument.Designation)get_store().add_element_user(DESIGNATION$8);
                }
                target.set(designation);
            }
        }
        
        /**
         * Unsets the "Designation" element
         */
        public void unsetDesignation()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(DESIGNATION$8, 0);
            }
        }
        
        /**
         * Gets the "AcctLinkKey" element
         */
        public java.lang.String getAcctLinkKey()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ACCTLINKKEY$10, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "AcctLinkKey" element
         */
        public com.idanalytics.products.idscore.result_v31.AcctLinkKeyDocument.AcctLinkKey xgetAcctLinkKey()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.AcctLinkKeyDocument.AcctLinkKey target = null;
                target = (com.idanalytics.products.idscore.result_v31.AcctLinkKeyDocument.AcctLinkKey)get_store().find_element_user(ACCTLINKKEY$10, 0);
                return target;
            }
        }
        
        /**
         * True if has "AcctLinkKey" element
         */
        public boolean isSetAcctLinkKey()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(ACCTLINKKEY$10) != 0;
            }
        }
        
        /**
         * Sets the "AcctLinkKey" element
         */
        public void setAcctLinkKey(java.lang.String acctLinkKey)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ACCTLINKKEY$10, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ACCTLINKKEY$10);
                }
                target.setStringValue(acctLinkKey);
            }
        }
        
        /**
         * Sets (as xml) the "AcctLinkKey" element
         */
        public void xsetAcctLinkKey(com.idanalytics.products.idscore.result_v31.AcctLinkKeyDocument.AcctLinkKey acctLinkKey)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.AcctLinkKeyDocument.AcctLinkKey target = null;
                target = (com.idanalytics.products.idscore.result_v31.AcctLinkKeyDocument.AcctLinkKey)get_store().find_element_user(ACCTLINKKEY$10, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result_v31.AcctLinkKeyDocument.AcctLinkKey)get_store().add_element_user(ACCTLINKKEY$10);
                }
                target.set(acctLinkKey);
            }
        }
        
        /**
         * Unsets the "AcctLinkKey" element
         */
        public void unsetAcctLinkKey()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(ACCTLINKKEY$10, 0);
            }
        }
        
        /**
         * Gets the "IDASequence" element
         */
        public java.lang.String getIDASequence()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASEQUENCE$12, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "IDASequence" element
         */
        public com.idanalytics.products.idscore.result_v31.IDASequenceDocument.IDASequence xgetIDASequence()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.IDASequenceDocument.IDASequence target = null;
                target = (com.idanalytics.products.idscore.result_v31.IDASequenceDocument.IDASequence)get_store().find_element_user(IDASEQUENCE$12, 0);
                return target;
            }
        }
        
        /**
         * Sets the "IDASequence" element
         */
        public void setIDASequence(java.lang.String idaSequence)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASEQUENCE$12, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDASEQUENCE$12);
                }
                target.setStringValue(idaSequence);
            }
        }
        
        /**
         * Sets (as xml) the "IDASequence" element
         */
        public void xsetIDASequence(com.idanalytics.products.idscore.result_v31.IDASequenceDocument.IDASequence idaSequence)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.IDASequenceDocument.IDASequence target = null;
                target = (com.idanalytics.products.idscore.result_v31.IDASequenceDocument.IDASequence)get_store().find_element_user(IDASEQUENCE$12, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result_v31.IDASequenceDocument.IDASequence)get_store().add_element_user(IDASEQUENCE$12);
                }
                target.set(idaSequence);
            }
        }
        
        /**
         * Gets the "IDATimeStamp" element
         */
        public java.util.Calendar getIDATimeStamp()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDATIMESTAMP$14, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getCalendarValue();
            }
        }
        
        /**
         * Gets (as xml) the "IDATimeStamp" element
         */
        public com.idanalytics.products.idscore.result_v31.IDATimeStampDocument.IDATimeStamp xgetIDATimeStamp()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.IDATimeStampDocument.IDATimeStamp target = null;
                target = (com.idanalytics.products.idscore.result_v31.IDATimeStampDocument.IDATimeStamp)get_store().find_element_user(IDATIMESTAMP$14, 0);
                return target;
            }
        }
        
        /**
         * Sets the "IDATimeStamp" element
         */
        public void setIDATimeStamp(java.util.Calendar idaTimeStamp)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDATIMESTAMP$14, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDATIMESTAMP$14);
                }
                target.setCalendarValue(idaTimeStamp);
            }
        }
        
        /**
         * Sets (as xml) the "IDATimeStamp" element
         */
        public void xsetIDATimeStamp(com.idanalytics.products.idscore.result_v31.IDATimeStampDocument.IDATimeStamp idaTimeStamp)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.IDATimeStampDocument.IDATimeStamp target = null;
                target = (com.idanalytics.products.idscore.result_v31.IDATimeStampDocument.IDATimeStamp)get_store().find_element_user(IDATIMESTAMP$14, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result_v31.IDATimeStampDocument.IDATimeStamp)get_store().add_element_user(IDATIMESTAMP$14);
                }
                target.set(idaTimeStamp);
            }
        }
        
        /**
         * Gets array of all "Score" elements
         */
        public com.idanalytics.products.idscore.result_v31.Score[] getScoreArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(SCORE$16, targetList);
                com.idanalytics.products.idscore.result_v31.Score[] result = new com.idanalytics.products.idscore.result_v31.Score[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "Score" element
         */
        public com.idanalytics.products.idscore.result_v31.Score getScoreArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.Score target = null;
                target = (com.idanalytics.products.idscore.result_v31.Score)get_store().find_element_user(SCORE$16, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "Score" element
         */
        public int sizeOfScoreArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(SCORE$16);
            }
        }
        
        /**
         * Sets array of all "Score" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setScoreArray(com.idanalytics.products.idscore.result_v31.Score[] scoreArray)
        {
            check_orphaned();
            arraySetterHelper(scoreArray, SCORE$16);
        }
        
        /**
         * Sets ith "Score" element
         */
        public void setScoreArray(int i, com.idanalytics.products.idscore.result_v31.Score score)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.Score target = null;
                target = (com.idanalytics.products.idscore.result_v31.Score)get_store().find_element_user(SCORE$16, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(score);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "Score" element
         */
        public com.idanalytics.products.idscore.result_v31.Score insertNewScore(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.Score target = null;
                target = (com.idanalytics.products.idscore.result_v31.Score)get_store().insert_element_user(SCORE$16, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "Score" element
         */
        public com.idanalytics.products.idscore.result_v31.Score addNewScore()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.Score target = null;
                target = (com.idanalytics.products.idscore.result_v31.Score)get_store().add_element_user(SCORE$16);
                return target;
            }
        }
        
        /**
         * Removes the ith "Score" element
         */
        public void removeScore(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(SCORE$16, i);
            }
        }
        
        /**
         * Gets the "ConsumerStatements" element
         */
        public com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.ConsumerStatements getConsumerStatements()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.ConsumerStatements target = null;
                target = (com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.ConsumerStatements)get_store().find_element_user(CONSUMERSTATEMENTS$18, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "ConsumerStatements" element
         */
        public boolean isSetConsumerStatements()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(CONSUMERSTATEMENTS$18) != 0;
            }
        }
        
        /**
         * Sets the "ConsumerStatements" element
         */
        public void setConsumerStatements(com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.ConsumerStatements consumerStatements)
        {
            generatedSetterHelperImpl(consumerStatements, CONSUMERSTATEMENTS$18, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "ConsumerStatements" element
         */
        public com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.ConsumerStatements addNewConsumerStatements()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.ConsumerStatements target = null;
                target = (com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.ConsumerStatements)get_store().add_element_user(CONSUMERSTATEMENTS$18);
                return target;
            }
        }
        
        /**
         * Unsets the "ConsumerStatements" element
         */
        public void unsetConsumerStatements()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(CONSUMERSTATEMENTS$18, 0);
            }
        }
        
        /**
         * Gets the "Messages" element
         */
        public com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.Messages getMessages()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.Messages target = null;
                target = (com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.Messages)get_store().find_element_user(MESSAGES$20, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "Messages" element
         */
        public boolean isSetMessages()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(MESSAGES$20) != 0;
            }
        }
        
        /**
         * Sets the "Messages" element
         */
        public void setMessages(com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.Messages messages)
        {
            generatedSetterHelperImpl(messages, MESSAGES$20, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "Messages" element
         */
        public com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.Messages addNewMessages()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.Messages target = null;
                target = (com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.Messages)get_store().add_element_user(MESSAGES$20);
                return target;
            }
        }
        
        /**
         * Unsets the "Messages" element
         */
        public void unsetMessages()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(MESSAGES$20, 0);
            }
        }
        
        /**
         * Gets the "PassThru1" element
         */
        public java.lang.String getPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU1$22, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru1" element
         */
        public com.idanalytics.products.idscore.result_v31.PassThru1Document.PassThru1 xgetPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.PassThru1Document.PassThru1 target = null;
                target = (com.idanalytics.products.idscore.result_v31.PassThru1Document.PassThru1)get_store().find_element_user(PASSTHRU1$22, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru1" element
         */
        public boolean isSetPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU1$22) != 0;
            }
        }
        
        /**
         * Sets the "PassThru1" element
         */
        public void setPassThru1(java.lang.String passThru1)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU1$22, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU1$22);
                }
                target.setStringValue(passThru1);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru1" element
         */
        public void xsetPassThru1(com.idanalytics.products.idscore.result_v31.PassThru1Document.PassThru1 passThru1)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.PassThru1Document.PassThru1 target = null;
                target = (com.idanalytics.products.idscore.result_v31.PassThru1Document.PassThru1)get_store().find_element_user(PASSTHRU1$22, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result_v31.PassThru1Document.PassThru1)get_store().add_element_user(PASSTHRU1$22);
                }
                target.set(passThru1);
            }
        }
        
        /**
         * Unsets the "PassThru1" element
         */
        public void unsetPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU1$22, 0);
            }
        }
        
        /**
         * Gets the "PassThru2" element
         */
        public java.lang.String getPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU2$24, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru2" element
         */
        public com.idanalytics.products.idscore.result_v31.PassThru2Document.PassThru2 xgetPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.PassThru2Document.PassThru2 target = null;
                target = (com.idanalytics.products.idscore.result_v31.PassThru2Document.PassThru2)get_store().find_element_user(PASSTHRU2$24, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru2" element
         */
        public boolean isSetPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU2$24) != 0;
            }
        }
        
        /**
         * Sets the "PassThru2" element
         */
        public void setPassThru2(java.lang.String passThru2)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU2$24, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU2$24);
                }
                target.setStringValue(passThru2);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru2" element
         */
        public void xsetPassThru2(com.idanalytics.products.idscore.result_v31.PassThru2Document.PassThru2 passThru2)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.PassThru2Document.PassThru2 target = null;
                target = (com.idanalytics.products.idscore.result_v31.PassThru2Document.PassThru2)get_store().find_element_user(PASSTHRU2$24, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result_v31.PassThru2Document.PassThru2)get_store().add_element_user(PASSTHRU2$24);
                }
                target.set(passThru2);
            }
        }
        
        /**
         * Unsets the "PassThru2" element
         */
        public void unsetPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU2$24, 0);
            }
        }
        
        /**
         * Gets the "PassThru3" element
         */
        public java.lang.String getPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU3$26, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru3" element
         */
        public com.idanalytics.products.idscore.result_v31.PassThru3Document.PassThru3 xgetPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.PassThru3Document.PassThru3 target = null;
                target = (com.idanalytics.products.idscore.result_v31.PassThru3Document.PassThru3)get_store().find_element_user(PASSTHRU3$26, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru3" element
         */
        public boolean isSetPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU3$26) != 0;
            }
        }
        
        /**
         * Sets the "PassThru3" element
         */
        public void setPassThru3(java.lang.String passThru3)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU3$26, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU3$26);
                }
                target.setStringValue(passThru3);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru3" element
         */
        public void xsetPassThru3(com.idanalytics.products.idscore.result_v31.PassThru3Document.PassThru3 passThru3)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.PassThru3Document.PassThru3 target = null;
                target = (com.idanalytics.products.idscore.result_v31.PassThru3Document.PassThru3)get_store().find_element_user(PASSTHRU3$26, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result_v31.PassThru3Document.PassThru3)get_store().add_element_user(PASSTHRU3$26);
                }
                target.set(passThru3);
            }
        }
        
        /**
         * Unsets the "PassThru3" element
         */
        public void unsetPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU3$26, 0);
            }
        }
        
        /**
         * Gets the "PassThru4" element
         */
        public java.lang.String getPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU4$28, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru4" element
         */
        public com.idanalytics.products.idscore.result_v31.PassThru4Document.PassThru4 xgetPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.PassThru4Document.PassThru4 target = null;
                target = (com.idanalytics.products.idscore.result_v31.PassThru4Document.PassThru4)get_store().find_element_user(PASSTHRU4$28, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru4" element
         */
        public boolean isSetPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU4$28) != 0;
            }
        }
        
        /**
         * Sets the "PassThru4" element
         */
        public void setPassThru4(java.lang.String passThru4)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU4$28, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU4$28);
                }
                target.setStringValue(passThru4);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru4" element
         */
        public void xsetPassThru4(com.idanalytics.products.idscore.result_v31.PassThru4Document.PassThru4 passThru4)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.PassThru4Document.PassThru4 target = null;
                target = (com.idanalytics.products.idscore.result_v31.PassThru4Document.PassThru4)get_store().find_element_user(PASSTHRU4$28, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result_v31.PassThru4Document.PassThru4)get_store().add_element_user(PASSTHRU4$28);
                }
                target.set(passThru4);
            }
        }
        
        /**
         * Unsets the "PassThru4" element
         */
        public void unsetPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU4$28, 0);
            }
        }
        
        /**
         * Gets the "Indicators" element
         */
        public com.idanalytics.products.idscore.result_v31.IndicatorsDocument.Indicators getIndicators()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.IndicatorsDocument.Indicators target = null;
                target = (com.idanalytics.products.idscore.result_v31.IndicatorsDocument.Indicators)get_store().find_element_user(INDICATORS$30, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "Indicators" element
         */
        public boolean isSetIndicators()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(INDICATORS$30) != 0;
            }
        }
        
        /**
         * Sets the "Indicators" element
         */
        public void setIndicators(com.idanalytics.products.idscore.result_v31.IndicatorsDocument.Indicators indicators)
        {
            generatedSetterHelperImpl(indicators, INDICATORS$30, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "Indicators" element
         */
        public com.idanalytics.products.idscore.result_v31.IndicatorsDocument.Indicators addNewIndicators()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.IndicatorsDocument.Indicators target = null;
                target = (com.idanalytics.products.idscore.result_v31.IndicatorsDocument.Indicators)get_store().add_element_user(INDICATORS$30);
                return target;
            }
        }
        
        /**
         * Unsets the "Indicators" element
         */
        public void unsetIndicators()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(INDICATORS$30, 0);
            }
        }
        
        /**
         * Gets the "IDAInternal" element
         */
        public com.idanalytics.products.idscore.result_v31.IDAInternalDocument.IDAInternal getIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.IDAInternalDocument.IDAInternal target = null;
                target = (com.idanalytics.products.idscore.result_v31.IDAInternalDocument.IDAInternal)get_store().find_element_user(IDAINTERNAL$32, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "IDAInternal" element
         */
        public boolean isSetIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(IDAINTERNAL$32) != 0;
            }
        }
        
        /**
         * Sets the "IDAInternal" element
         */
        public void setIDAInternal(com.idanalytics.products.idscore.result_v31.IDAInternalDocument.IDAInternal idaInternal)
        {
            generatedSetterHelperImpl(idaInternal, IDAINTERNAL$32, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "IDAInternal" element
         */
        public com.idanalytics.products.idscore.result_v31.IDAInternalDocument.IDAInternal addNewIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.IDAInternalDocument.IDAInternal target = null;
                target = (com.idanalytics.products.idscore.result_v31.IDAInternalDocument.IDAInternal)get_store().add_element_user(IDAINTERNAL$32);
                return target;
            }
        }
        
        /**
         * Unsets the "IDAInternal" element
         */
        public void unsetIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(IDAINTERNAL$32, 0);
            }
        }
        
        /**
         * Gets the "PreviousEvents" element
         */
        public com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.PreviousEvents getPreviousEvents()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.PreviousEvents target = null;
                target = (com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.PreviousEvents)get_store().find_element_user(PREVIOUSEVENTS$34, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "PreviousEvents" element
         */
        public boolean isSetPreviousEvents()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PREVIOUSEVENTS$34) != 0;
            }
        }
        
        /**
         * Sets the "PreviousEvents" element
         */
        public void setPreviousEvents(com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.PreviousEvents previousEvents)
        {
            generatedSetterHelperImpl(previousEvents, PREVIOUSEVENTS$34, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "PreviousEvents" element
         */
        public com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.PreviousEvents addNewPreviousEvents()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.PreviousEvents target = null;
                target = (com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.PreviousEvents)get_store().add_element_user(PREVIOUSEVENTS$34);
                return target;
            }
        }
        
        /**
         * Unsets the "PreviousEvents" element
         */
        public void unsetPreviousEvents()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PREVIOUSEVENTS$34, 0);
            }
        }
        
        /**
         * Gets the "schemaVersion" attribute
         */
        public java.math.BigDecimal getSchemaVersion()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(SCHEMAVERSION$36);
                if (target == null)
                {
                    return null;
                }
                return target.getBigDecimalValue();
            }
        }
        
        /**
         * Gets (as xml) the "schemaVersion" attribute
         */
        public org.apache.xmlbeans.XmlDecimal xgetSchemaVersion()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlDecimal target = null;
                target = (org.apache.xmlbeans.XmlDecimal)get_store().find_attribute_user(SCHEMAVERSION$36);
                return target;
            }
        }
        
        /**
         * True if has "schemaVersion" attribute
         */
        public boolean isSetSchemaVersion()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().find_attribute_user(SCHEMAVERSION$36) != null;
            }
        }
        
        /**
         * Sets the "schemaVersion" attribute
         */
        public void setSchemaVersion(java.math.BigDecimal schemaVersion)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(SCHEMAVERSION$36);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(SCHEMAVERSION$36);
                }
                target.setBigDecimalValue(schemaVersion);
            }
        }
        
        /**
         * Sets (as xml) the "schemaVersion" attribute
         */
        public void xsetSchemaVersion(org.apache.xmlbeans.XmlDecimal schemaVersion)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlDecimal target = null;
                target = (org.apache.xmlbeans.XmlDecimal)get_store().find_attribute_user(SCHEMAVERSION$36);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlDecimal)get_store().add_attribute_user(SCHEMAVERSION$36);
                }
                target.set(schemaVersion);
            }
        }
        
        /**
         * Unsets the "schemaVersion" attribute
         */
        public void unsetSchemaVersion()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_attribute(SCHEMAVERSION$36);
            }
        }
        /**
         * An XML ConsumerStatements(@http://idanalytics.com/products/idscore/result.v31).
         *
         * This is a complex type.
         */
        public static class ConsumerStatementsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.ConsumerStatements
        {
            private static final long serialVersionUID = 1L;
            
            public ConsumerStatementsImpl(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType);
            }
            
            private static final javax.xml.namespace.QName CONSUMERSTATEMENT$0 = 
                new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result.v31", "ConsumerStatement");
            
            
            /**
             * Gets array of all "ConsumerStatement" elements
             */
            public com.idanalytics.products.idscore.result_v31.ConsumerStatementType[] getConsumerStatementArray()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    java.util.List targetList = new java.util.ArrayList();
                    get_store().find_all_element_users(CONSUMERSTATEMENT$0, targetList);
                    com.idanalytics.products.idscore.result_v31.ConsumerStatementType[] result = new com.idanalytics.products.idscore.result_v31.ConsumerStatementType[targetList.size()];
                    targetList.toArray(result);
                    return result;
                }
            }
            
            /**
             * Gets ith "ConsumerStatement" element
             */
            public com.idanalytics.products.idscore.result_v31.ConsumerStatementType getConsumerStatementArray(int i)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    com.idanalytics.products.idscore.result_v31.ConsumerStatementType target = null;
                    target = (com.idanalytics.products.idscore.result_v31.ConsumerStatementType)get_store().find_element_user(CONSUMERSTATEMENT$0, i);
                    if (target == null)
                    {
                      throw new IndexOutOfBoundsException();
                    }
                    return target;
                }
            }
            
            /**
             * Returns number of "ConsumerStatement" element
             */
            public int sizeOfConsumerStatementArray()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    return get_store().count_elements(CONSUMERSTATEMENT$0);
                }
            }
            
            /**
             * Sets array of all "ConsumerStatement" element  WARNING: This method is not atomicaly synchronized.
             */
            public void setConsumerStatementArray(com.idanalytics.products.idscore.result_v31.ConsumerStatementType[] consumerStatementArray)
            {
                check_orphaned();
                arraySetterHelper(consumerStatementArray, CONSUMERSTATEMENT$0);
            }
            
            /**
             * Sets ith "ConsumerStatement" element
             */
            public void setConsumerStatementArray(int i, com.idanalytics.products.idscore.result_v31.ConsumerStatementType consumerStatement)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    com.idanalytics.products.idscore.result_v31.ConsumerStatementType target = null;
                    target = (com.idanalytics.products.idscore.result_v31.ConsumerStatementType)get_store().find_element_user(CONSUMERSTATEMENT$0, i);
                    if (target == null)
                    {
                      throw new IndexOutOfBoundsException();
                    }
                    target.set(consumerStatement);
                }
            }
            
            /**
             * Inserts and returns a new empty value (as xml) as the ith "ConsumerStatement" element
             */
            public com.idanalytics.products.idscore.result_v31.ConsumerStatementType insertNewConsumerStatement(int i)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    com.idanalytics.products.idscore.result_v31.ConsumerStatementType target = null;
                    target = (com.idanalytics.products.idscore.result_v31.ConsumerStatementType)get_store().insert_element_user(CONSUMERSTATEMENT$0, i);
                    return target;
                }
            }
            
            /**
             * Appends and returns a new empty value (as xml) as the last "ConsumerStatement" element
             */
            public com.idanalytics.products.idscore.result_v31.ConsumerStatementType addNewConsumerStatement()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    com.idanalytics.products.idscore.result_v31.ConsumerStatementType target = null;
                    target = (com.idanalytics.products.idscore.result_v31.ConsumerStatementType)get_store().add_element_user(CONSUMERSTATEMENT$0);
                    return target;
                }
            }
            
            /**
             * Removes the ith "ConsumerStatement" element
             */
            public void removeConsumerStatement(int i)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    get_store().remove_element(CONSUMERSTATEMENT$0, i);
                }
            }
        }
        /**
         * An XML Messages(@http://idanalytics.com/products/idscore/result.v31).
         *
         * This is a complex type.
         */
        public static class MessagesImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.Messages
        {
            private static final long serialVersionUID = 1L;
            
            public MessagesImpl(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType);
            }
            
            private static final javax.xml.namespace.QName MESSAGE$0 = 
                new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result.v31", "Message");
            
            
            /**
             * Gets array of all "Message" elements
             */
            public com.idanalytics.products.idscore.result_v31.MessageType[] getMessageArray()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    java.util.List targetList = new java.util.ArrayList();
                    get_store().find_all_element_users(MESSAGE$0, targetList);
                    com.idanalytics.products.idscore.result_v31.MessageType[] result = new com.idanalytics.products.idscore.result_v31.MessageType[targetList.size()];
                    targetList.toArray(result);
                    return result;
                }
            }
            
            /**
             * Gets ith "Message" element
             */
            public com.idanalytics.products.idscore.result_v31.MessageType getMessageArray(int i)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    com.idanalytics.products.idscore.result_v31.MessageType target = null;
                    target = (com.idanalytics.products.idscore.result_v31.MessageType)get_store().find_element_user(MESSAGE$0, i);
                    if (target == null)
                    {
                      throw new IndexOutOfBoundsException();
                    }
                    return target;
                }
            }
            
            /**
             * Returns number of "Message" element
             */
            public int sizeOfMessageArray()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    return get_store().count_elements(MESSAGE$0);
                }
            }
            
            /**
             * Sets array of all "Message" element  WARNING: This method is not atomicaly synchronized.
             */
            public void setMessageArray(com.idanalytics.products.idscore.result_v31.MessageType[] messageArray)
            {
                check_orphaned();
                arraySetterHelper(messageArray, MESSAGE$0);
            }
            
            /**
             * Sets ith "Message" element
             */
            public void setMessageArray(int i, com.idanalytics.products.idscore.result_v31.MessageType message)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    com.idanalytics.products.idscore.result_v31.MessageType target = null;
                    target = (com.idanalytics.products.idscore.result_v31.MessageType)get_store().find_element_user(MESSAGE$0, i);
                    if (target == null)
                    {
                      throw new IndexOutOfBoundsException();
                    }
                    target.set(message);
                }
            }
            
            /**
             * Inserts and returns a new empty value (as xml) as the ith "Message" element
             */
            public com.idanalytics.products.idscore.result_v31.MessageType insertNewMessage(int i)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    com.idanalytics.products.idscore.result_v31.MessageType target = null;
                    target = (com.idanalytics.products.idscore.result_v31.MessageType)get_store().insert_element_user(MESSAGE$0, i);
                    return target;
                }
            }
            
            /**
             * Appends and returns a new empty value (as xml) as the last "Message" element
             */
            public com.idanalytics.products.idscore.result_v31.MessageType addNewMessage()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    com.idanalytics.products.idscore.result_v31.MessageType target = null;
                    target = (com.idanalytics.products.idscore.result_v31.MessageType)get_store().add_element_user(MESSAGE$0);
                    return target;
                }
            }
            
            /**
             * Removes the ith "Message" element
             */
            public void removeMessage(int i)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    get_store().remove_element(MESSAGE$0, i);
                }
            }
        }
        /**
         * An XML PreviousEvents(@http://idanalytics.com/products/idscore/result.v31).
         *
         * This is a complex type.
         */
        public static class PreviousEventsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.PreviousEvents
        {
            private static final long serialVersionUID = 1L;
            
            public PreviousEventsImpl(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType);
            }
            
            private static final javax.xml.namespace.QName PREVIOUSEVENT$0 = 
                new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result.v31", "PreviousEvent");
            
            
            /**
             * Gets array of all "PreviousEvent" elements
             */
            public com.idanalytics.products.idscore.result_v31.IdentityEvent[] getPreviousEventArray()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    java.util.List targetList = new java.util.ArrayList();
                    get_store().find_all_element_users(PREVIOUSEVENT$0, targetList);
                    com.idanalytics.products.idscore.result_v31.IdentityEvent[] result = new com.idanalytics.products.idscore.result_v31.IdentityEvent[targetList.size()];
                    targetList.toArray(result);
                    return result;
                }
            }
            
            /**
             * Gets ith "PreviousEvent" element
             */
            public com.idanalytics.products.idscore.result_v31.IdentityEvent getPreviousEventArray(int i)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    com.idanalytics.products.idscore.result_v31.IdentityEvent target = null;
                    target = (com.idanalytics.products.idscore.result_v31.IdentityEvent)get_store().find_element_user(PREVIOUSEVENT$0, i);
                    if (target == null)
                    {
                      throw new IndexOutOfBoundsException();
                    }
                    return target;
                }
            }
            
            /**
             * Returns number of "PreviousEvent" element
             */
            public int sizeOfPreviousEventArray()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    return get_store().count_elements(PREVIOUSEVENT$0);
                }
            }
            
            /**
             * Sets array of all "PreviousEvent" element  WARNING: This method is not atomicaly synchronized.
             */
            public void setPreviousEventArray(com.idanalytics.products.idscore.result_v31.IdentityEvent[] previousEventArray)
            {
                check_orphaned();
                arraySetterHelper(previousEventArray, PREVIOUSEVENT$0);
            }
            
            /**
             * Sets ith "PreviousEvent" element
             */
            public void setPreviousEventArray(int i, com.idanalytics.products.idscore.result_v31.IdentityEvent previousEvent)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    com.idanalytics.products.idscore.result_v31.IdentityEvent target = null;
                    target = (com.idanalytics.products.idscore.result_v31.IdentityEvent)get_store().find_element_user(PREVIOUSEVENT$0, i);
                    if (target == null)
                    {
                      throw new IndexOutOfBoundsException();
                    }
                    target.set(previousEvent);
                }
            }
            
            /**
             * Inserts and returns a new empty value (as xml) as the ith "PreviousEvent" element
             */
            public com.idanalytics.products.idscore.result_v31.IdentityEvent insertNewPreviousEvent(int i)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    com.idanalytics.products.idscore.result_v31.IdentityEvent target = null;
                    target = (com.idanalytics.products.idscore.result_v31.IdentityEvent)get_store().insert_element_user(PREVIOUSEVENT$0, i);
                    return target;
                }
            }
            
            /**
             * Appends and returns a new empty value (as xml) as the last "PreviousEvent" element
             */
            public com.idanalytics.products.idscore.result_v31.IdentityEvent addNewPreviousEvent()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    com.idanalytics.products.idscore.result_v31.IdentityEvent target = null;
                    target = (com.idanalytics.products.idscore.result_v31.IdentityEvent)get_store().add_element_user(PREVIOUSEVENT$0);
                    return target;
                }
            }
            
            /**
             * Removes the ith "PreviousEvent" element
             */
            public void removePreviousEvent(int i)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    get_store().remove_element(PREVIOUSEVENT$0, i);
                }
            }
        }
    }
}
