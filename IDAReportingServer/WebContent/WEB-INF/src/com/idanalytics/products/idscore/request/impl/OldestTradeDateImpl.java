/*
 * XML Type:  OldestTradeDate
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.OldestTradeDate
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML OldestTradeDate(@http://idanalytics.com/products/idscore/request).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.OldestTradeDate.
 */
public class OldestTradeDateImpl extends org.apache.xmlbeans.impl.values.JavaGDateHolderEx implements com.idanalytics.products.idscore.request.OldestTradeDate
{
    private static final long serialVersionUID = 1L;
    
    public OldestTradeDateImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected OldestTradeDateImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
