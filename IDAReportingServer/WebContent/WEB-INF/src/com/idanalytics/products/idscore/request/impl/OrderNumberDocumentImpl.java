/*
 * An XML document type.
 * Localname: OrderNumber
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.OrderNumberDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * A document containing one OrderNumber(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public class OrderNumberDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.OrderNumberDocument
{
    private static final long serialVersionUID = 1L;
    
    public OrderNumberDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ORDERNUMBER$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "OrderNumber");
    
    
    /**
     * Gets the "OrderNumber" element
     */
    public java.lang.String getOrderNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ORDERNUMBER$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "OrderNumber" element
     */
    public com.idanalytics.products.idscore.request.OrderNumberDocument.OrderNumber xgetOrderNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.OrderNumberDocument.OrderNumber target = null;
            target = (com.idanalytics.products.idscore.request.OrderNumberDocument.OrderNumber)get_store().find_element_user(ORDERNUMBER$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "OrderNumber" element
     */
    public void setOrderNumber(java.lang.String orderNumber)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ORDERNUMBER$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ORDERNUMBER$0);
            }
            target.setStringValue(orderNumber);
        }
    }
    
    /**
     * Sets (as xml) the "OrderNumber" element
     */
    public void xsetOrderNumber(com.idanalytics.products.idscore.request.OrderNumberDocument.OrderNumber orderNumber)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.OrderNumberDocument.OrderNumber target = null;
            target = (com.idanalytics.products.idscore.request.OrderNumberDocument.OrderNumber)get_store().find_element_user(ORDERNUMBER$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.OrderNumberDocument.OrderNumber)get_store().add_element_user(ORDERNUMBER$0);
            }
            target.set(orderNumber);
        }
    }
    /**
     * An XML OrderNumber(@http://idanalytics.com/products/idscore/request).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.OrderNumberDocument$OrderNumber.
     */
    public static class OrderNumberImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.request.OrderNumberDocument.OrderNumber
    {
        private static final long serialVersionUID = 1L;
        
        public OrderNumberImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected OrderNumberImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
