/*
 * XML Type:  string50
 * Namespace: http://idanalytics.com/products/idscore/result
 * Java type: com.idanalytics.products.idscore.result.String50
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.result.impl;
/**
 * An XML string50(@http://idanalytics.com/products/idscore/result).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result.String50.
 */
public class String50Impl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.result.String50
{
    private static final long serialVersionUID = 1L;
    
    public String50Impl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected String50Impl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
