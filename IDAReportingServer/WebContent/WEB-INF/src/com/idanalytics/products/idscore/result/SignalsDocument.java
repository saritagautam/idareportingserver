/*
 * An XML document type.
 * Localname: Signals
 * Namespace: http://idanalytics.com/products/idscore/result
 * Java type: com.idanalytics.products.idscore.result.SignalsDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.result;


/**
 * A document containing one Signals(@http://idanalytics.com/products/idscore/result) element.
 *
 * This is a complex type.
 */
public interface SignalsDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(SignalsDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("signals314bdoctype");
    
    /**
     * Gets the "Signals" element
     */
    com.idanalytics.products.idscore.result.SignalsDocument.Signals getSignals();
    
    /**
     * Sets the "Signals" element
     */
    void setSignals(com.idanalytics.products.idscore.result.SignalsDocument.Signals signals);
    
    /**
     * Appends and returns a new empty "Signals" element
     */
    com.idanalytics.products.idscore.result.SignalsDocument.Signals addNewSignals();
    
    /**
     * An XML Signals(@http://idanalytics.com/products/idscore/result).
     *
     * This is a complex type.
     */
    public interface Signals extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Signals.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("signals0a14elemtype");
        
        /**
         * Gets the "AddressRisk" element
         */
        java.lang.String getAddressRisk();
        
        /**
         * Gets (as xml) the "AddressRisk" element
         */
        com.idanalytics.products.idscore.result.SignalsDocument.Signals.AddressRisk xgetAddressRisk();
        
        /**
         * Sets the "AddressRisk" element
         */
        void setAddressRisk(java.lang.String addressRisk);
        
        /**
         * Sets (as xml) the "AddressRisk" element
         */
        void xsetAddressRisk(com.idanalytics.products.idscore.result.SignalsDocument.Signals.AddressRisk addressRisk);
        
        /**
         * Gets the "PhoneRisk" element
         */
        java.lang.String getPhoneRisk();
        
        /**
         * Gets (as xml) the "PhoneRisk" element
         */
        com.idanalytics.products.idscore.result.SignalsDocument.Signals.PhoneRisk xgetPhoneRisk();
        
        /**
         * Sets the "PhoneRisk" element
         */
        void setPhoneRisk(java.lang.String phoneRisk);
        
        /**
         * Sets (as xml) the "PhoneRisk" element
         */
        void xsetPhoneRisk(com.idanalytics.products.idscore.result.SignalsDocument.Signals.PhoneRisk phoneRisk);
        
        /**
         * Gets the "EmailRisk" element
         */
        java.lang.String getEmailRisk();
        
        /**
         * Gets (as xml) the "EmailRisk" element
         */
        com.idanalytics.products.idscore.result.SignalsDocument.Signals.EmailRisk xgetEmailRisk();
        
        /**
         * Sets the "EmailRisk" element
         */
        void setEmailRisk(java.lang.String emailRisk);
        
        /**
         * Sets (as xml) the "EmailRisk" element
         */
        void xsetEmailRisk(com.idanalytics.products.idscore.result.SignalsDocument.Signals.EmailRisk emailRisk);
        
        /**
         * Gets the "SSNInsight" element
         */
        java.lang.String getSSNInsight();
        
        /**
         * Gets (as xml) the "SSNInsight" element
         */
        com.idanalytics.products.idscore.result.SignalsDocument.Signals.SSNInsight xgetSSNInsight();
        
        /**
         * Sets the "SSNInsight" element
         */
        void setSSNInsight(java.lang.String ssnInsight);
        
        /**
         * Sets (as xml) the "SSNInsight" element
         */
        void xsetSSNInsight(com.idanalytics.products.idscore.result.SignalsDocument.Signals.SSNInsight ssnInsight);
        
        /**
         * Gets the "FraudRiskType" element
         */
        java.lang.String getFraudRiskType();
        
        /**
         * Gets (as xml) the "FraudRiskType" element
         */
        com.idanalytics.products.idscore.result.SignalsDocument.Signals.FraudRiskType xgetFraudRiskType();
        
        /**
         * Sets the "FraudRiskType" element
         */
        void setFraudRiskType(java.lang.String fraudRiskType);
        
        /**
         * Sets (as xml) the "FraudRiskType" element
         */
        void xsetFraudRiskType(com.idanalytics.products.idscore.result.SignalsDocument.Signals.FraudRiskType fraudRiskType);
        
        /**
         * Gets the "FraudRing" element
         */
        java.lang.String getFraudRing();
        
        /**
         * Gets (as xml) the "FraudRing" element
         */
        com.idanalytics.products.idscore.result.SignalsDocument.Signals.FraudRing xgetFraudRing();
        
        /**
         * Sets the "FraudRing" element
         */
        void setFraudRing(java.lang.String fraudRing);
        
        /**
         * Sets (as xml) the "FraudRing" element
         */
        void xsetFraudRing(com.idanalytics.products.idscore.result.SignalsDocument.Signals.FraudRing fraudRing);
        
        /**
         * Gets the "PIN" element
         */
        java.lang.String getPIN();
        
        /**
         * Gets (as xml) the "PIN" element
         */
        com.idanalytics.products.idscore.result.SignalsDocument.Signals.PIN xgetPIN();
        
        /**
         * True if has "PIN" element
         */
        boolean isSetPIN();
        
        /**
         * Sets the "PIN" element
         */
        void setPIN(java.lang.String pin);
        
        /**
         * Sets (as xml) the "PIN" element
         */
        void xsetPIN(com.idanalytics.products.idscore.result.SignalsDocument.Signals.PIN pin);
        
        /**
         * Unsets the "PIN" element
         */
        void unsetPIN();
        
        /**
         * An XML AddressRisk(@http://idanalytics.com/products/idscore/result).
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result.SignalsDocument$Signals$AddressRisk.
         */
        public interface AddressRisk extends org.apache.xmlbeans.XmlString
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(AddressRisk.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("addressrisk8e85elemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static com.idanalytics.products.idscore.result.SignalsDocument.Signals.AddressRisk newValue(java.lang.Object obj) {
                  return (com.idanalytics.products.idscore.result.SignalsDocument.Signals.AddressRisk) type.newValue( obj ); }
                
                public static com.idanalytics.products.idscore.result.SignalsDocument.Signals.AddressRisk newInstance() {
                  return (com.idanalytics.products.idscore.result.SignalsDocument.Signals.AddressRisk) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static com.idanalytics.products.idscore.result.SignalsDocument.Signals.AddressRisk newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (com.idanalytics.products.idscore.result.SignalsDocument.Signals.AddressRisk) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * An XML PhoneRisk(@http://idanalytics.com/products/idscore/result).
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result.SignalsDocument$Signals$PhoneRisk.
         */
        public interface PhoneRisk extends org.apache.xmlbeans.XmlString
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(PhoneRisk.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("phonerisk4dcbelemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static com.idanalytics.products.idscore.result.SignalsDocument.Signals.PhoneRisk newValue(java.lang.Object obj) {
                  return (com.idanalytics.products.idscore.result.SignalsDocument.Signals.PhoneRisk) type.newValue( obj ); }
                
                public static com.idanalytics.products.idscore.result.SignalsDocument.Signals.PhoneRisk newInstance() {
                  return (com.idanalytics.products.idscore.result.SignalsDocument.Signals.PhoneRisk) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static com.idanalytics.products.idscore.result.SignalsDocument.Signals.PhoneRisk newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (com.idanalytics.products.idscore.result.SignalsDocument.Signals.PhoneRisk) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * An XML EmailRisk(@http://idanalytics.com/products/idscore/result).
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result.SignalsDocument$Signals$EmailRisk.
         */
        public interface EmailRisk extends org.apache.xmlbeans.XmlString
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(EmailRisk.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("emailriskffddelemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static com.idanalytics.products.idscore.result.SignalsDocument.Signals.EmailRisk newValue(java.lang.Object obj) {
                  return (com.idanalytics.products.idscore.result.SignalsDocument.Signals.EmailRisk) type.newValue( obj ); }
                
                public static com.idanalytics.products.idscore.result.SignalsDocument.Signals.EmailRisk newInstance() {
                  return (com.idanalytics.products.idscore.result.SignalsDocument.Signals.EmailRisk) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static com.idanalytics.products.idscore.result.SignalsDocument.Signals.EmailRisk newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (com.idanalytics.products.idscore.result.SignalsDocument.Signals.EmailRisk) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * An XML SSNInsight(@http://idanalytics.com/products/idscore/result).
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result.SignalsDocument$Signals$SSNInsight.
         */
        public interface SSNInsight extends org.apache.xmlbeans.XmlString
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(SSNInsight.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("ssninsightc3eeelemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static com.idanalytics.products.idscore.result.SignalsDocument.Signals.SSNInsight newValue(java.lang.Object obj) {
                  return (com.idanalytics.products.idscore.result.SignalsDocument.Signals.SSNInsight) type.newValue( obj ); }
                
                public static com.idanalytics.products.idscore.result.SignalsDocument.Signals.SSNInsight newInstance() {
                  return (com.idanalytics.products.idscore.result.SignalsDocument.Signals.SSNInsight) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static com.idanalytics.products.idscore.result.SignalsDocument.Signals.SSNInsight newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (com.idanalytics.products.idscore.result.SignalsDocument.Signals.SSNInsight) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * An XML FraudRiskType(@http://idanalytics.com/products/idscore/result).
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result.SignalsDocument$Signals$FraudRiskType.
         */
        public interface FraudRiskType extends org.apache.xmlbeans.XmlString
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(FraudRiskType.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("fraudrisktype3bfbelemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static com.idanalytics.products.idscore.result.SignalsDocument.Signals.FraudRiskType newValue(java.lang.Object obj) {
                  return (com.idanalytics.products.idscore.result.SignalsDocument.Signals.FraudRiskType) type.newValue( obj ); }
                
                public static com.idanalytics.products.idscore.result.SignalsDocument.Signals.FraudRiskType newInstance() {
                  return (com.idanalytics.products.idscore.result.SignalsDocument.Signals.FraudRiskType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static com.idanalytics.products.idscore.result.SignalsDocument.Signals.FraudRiskType newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (com.idanalytics.products.idscore.result.SignalsDocument.Signals.FraudRiskType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * An XML FraudRing(@http://idanalytics.com/products/idscore/result).
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result.SignalsDocument$Signals$FraudRing.
         */
        public interface FraudRing extends org.apache.xmlbeans.XmlString
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(FraudRing.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("fraudring7114elemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static com.idanalytics.products.idscore.result.SignalsDocument.Signals.FraudRing newValue(java.lang.Object obj) {
                  return (com.idanalytics.products.idscore.result.SignalsDocument.Signals.FraudRing) type.newValue( obj ); }
                
                public static com.idanalytics.products.idscore.result.SignalsDocument.Signals.FraudRing newInstance() {
                  return (com.idanalytics.products.idscore.result.SignalsDocument.Signals.FraudRing) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static com.idanalytics.products.idscore.result.SignalsDocument.Signals.FraudRing newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (com.idanalytics.products.idscore.result.SignalsDocument.Signals.FraudRing) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * An XML PIN(@http://idanalytics.com/products/idscore/result).
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result.SignalsDocument$Signals$PIN.
         */
        public interface PIN extends org.apache.xmlbeans.XmlString
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(PIN.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("pin7493elemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static com.idanalytics.products.idscore.result.SignalsDocument.Signals.PIN newValue(java.lang.Object obj) {
                  return (com.idanalytics.products.idscore.result.SignalsDocument.Signals.PIN) type.newValue( obj ); }
                
                public static com.idanalytics.products.idscore.result.SignalsDocument.Signals.PIN newInstance() {
                  return (com.idanalytics.products.idscore.result.SignalsDocument.Signals.PIN) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static com.idanalytics.products.idscore.result.SignalsDocument.Signals.PIN newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (com.idanalytics.products.idscore.result.SignalsDocument.Signals.PIN) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.idscore.result.SignalsDocument.Signals newInstance() {
              return (com.idanalytics.products.idscore.result.SignalsDocument.Signals) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.idscore.result.SignalsDocument.Signals newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.idscore.result.SignalsDocument.Signals) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.idscore.result.SignalsDocument newInstance() {
          return (com.idanalytics.products.idscore.result.SignalsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.idscore.result.SignalsDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.idscore.result.SignalsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.idscore.result.SignalsDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result.SignalsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.idscore.result.SignalsDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result.SignalsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.idscore.result.SignalsDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result.SignalsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.idscore.result.SignalsDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result.SignalsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.idscore.result.SignalsDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result.SignalsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.idscore.result.SignalsDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result.SignalsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.idscore.result.SignalsDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result.SignalsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.idscore.result.SignalsDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result.SignalsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.idscore.result.SignalsDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result.SignalsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.idscore.result.SignalsDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result.SignalsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.idscore.result.SignalsDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result.SignalsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.idscore.result.SignalsDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result.SignalsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.idscore.result.SignalsDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result.SignalsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.idscore.result.SignalsDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result.SignalsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.result.SignalsDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.result.SignalsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.result.SignalsDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.result.SignalsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
