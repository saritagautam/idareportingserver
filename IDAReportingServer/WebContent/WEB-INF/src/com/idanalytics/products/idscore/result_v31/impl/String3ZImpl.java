/*
 * XML Type:  string3z
 * Namespace: http://idanalytics.com/products/idscore/result.v31
 * Java type: com.idanalytics.products.idscore.result_v31.String3Z
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.result_v31.impl;
/**
 * An XML string3z(@http://idanalytics.com/products/idscore/result.v31).
 *
 * This is a union type. Instances are of one of the following types:
 *     com.idanalytics.products.idscore.result_v31.String3Z$Member
 *     com.idanalytics.products.idscore.result_v31.String3Z$Member2
 */
public class String3ZImpl extends org.apache.xmlbeans.impl.values.XmlUnionImpl implements com.idanalytics.products.idscore.result_v31.String3Z, com.idanalytics.products.idscore.result_v31.String3Z.Member, com.idanalytics.products.idscore.result_v31.String3Z.Member2
{
    private static final long serialVersionUID = 1L;
    
    public String3ZImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected String3ZImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
    /**
     * An anonymous inner XML type.
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result_v31.String3Z$Member.
     */
    public static class MemberImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.result_v31.String3Z.Member
    {
        private static final long serialVersionUID = 1L;
        
        public MemberImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected MemberImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
    /**
     * An anonymous inner XML type.
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result_v31.String3Z$Member2.
     */
    public static class MemberImpl2 extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.result_v31.String3Z.Member2
    {
        private static final long serialVersionUID = 1L;
        
        public MemberImpl2(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected MemberImpl2(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
