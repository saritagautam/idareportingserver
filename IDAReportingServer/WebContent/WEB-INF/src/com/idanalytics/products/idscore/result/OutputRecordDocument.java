/*
 * An XML document type.
 * Localname: OutputRecord
 * Namespace: http://idanalytics.com/products/idscore/result
 * Java type: com.idanalytics.products.idscore.result.OutputRecordDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.result;


/**
 * A document containing one OutputRecord(@http://idanalytics.com/products/idscore/result) element.
 *
 * This is a complex type.
 */
public interface OutputRecordDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(OutputRecordDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("outputrecord1c72doctype");
    
    /**
     * Gets the "OutputRecord" element
     */
    com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord getOutputRecord();
    
    /**
     * Sets the "OutputRecord" element
     */
    void setOutputRecord(com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord outputRecord);
    
    /**
     * Appends and returns a new empty "OutputRecord" element
     */
    com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord addNewOutputRecord();
    
    /**
     * An XML OutputRecord(@http://idanalytics.com/products/idscore/result).
     *
     * This is a complex type.
     */
    public interface OutputRecord extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(OutputRecord.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("outputrecord5560elemtype");
        
        /**
         * Gets the "IDAStatus" element
         */
        int getIDAStatus();
        
        /**
         * Gets (as xml) the "IDAStatus" element
         */
        com.idanalytics.products.idscore.result.IDAStatusDocument.IDAStatus xgetIDAStatus();
        
        /**
         * Sets the "IDAStatus" element
         */
        void setIDAStatus(int idaStatus);
        
        /**
         * Sets (as xml) the "IDAStatus" element
         */
        void xsetIDAStatus(com.idanalytics.products.idscore.result.IDAStatusDocument.IDAStatus idaStatus);
        
        /**
         * Gets the "AppID" element
         */
        java.lang.String getAppID();
        
        /**
         * Gets (as xml) the "AppID" element
         */
        com.idanalytics.products.common_v1.AppID xgetAppID();
        
        /**
         * True if has "AppID" element
         */
        boolean isSetAppID();
        
        /**
         * Sets the "AppID" element
         */
        void setAppID(java.lang.String appID);
        
        /**
         * Sets (as xml) the "AppID" element
         */
        void xsetAppID(com.idanalytics.products.common_v1.AppID appID);
        
        /**
         * Unsets the "AppID" element
         */
        void unsetAppID();
        
        /**
         * Gets the "ClientKey" element
         */
        java.lang.String getClientKey();
        
        /**
         * Gets (as xml) the "ClientKey" element
         */
        com.idanalytics.products.idscore.result.ClientKeyDocument.ClientKey xgetClientKey();
        
        /**
         * True if has "ClientKey" element
         */
        boolean isSetClientKey();
        
        /**
         * Sets the "ClientKey" element
         */
        void setClientKey(java.lang.String clientKey);
        
        /**
         * Sets (as xml) the "ClientKey" element
         */
        void xsetClientKey(com.idanalytics.products.idscore.result.ClientKeyDocument.ClientKey clientKey);
        
        /**
         * Unsets the "ClientKey" element
         */
        void unsetClientKey();
        
        /**
         * Gets the "ConsumerID" element
         */
        java.lang.String getConsumerID();
        
        /**
         * Gets (as xml) the "ConsumerID" element
         */
        com.idanalytics.products.idscore.result.ConsumerIDDocument.ConsumerID xgetConsumerID();
        
        /**
         * True if has "ConsumerID" element
         */
        boolean isSetConsumerID();
        
        /**
         * Sets the "ConsumerID" element
         */
        void setConsumerID(java.lang.String consumerID);
        
        /**
         * Sets (as xml) the "ConsumerID" element
         */
        void xsetConsumerID(com.idanalytics.products.idscore.result.ConsumerIDDocument.ConsumerID consumerID);
        
        /**
         * Unsets the "ConsumerID" element
         */
        void unsetConsumerID();
        
        /**
         * Gets the "Designation" element
         */
        com.idanalytics.products.idscore.result.DesignationDocument.Designation.Enum getDesignation();
        
        /**
         * Gets (as xml) the "Designation" element
         */
        com.idanalytics.products.idscore.result.DesignationDocument.Designation xgetDesignation();
        
        /**
         * True if has "Designation" element
         */
        boolean isSetDesignation();
        
        /**
         * Sets the "Designation" element
         */
        void setDesignation(com.idanalytics.products.idscore.result.DesignationDocument.Designation.Enum designation);
        
        /**
         * Sets (as xml) the "Designation" element
         */
        void xsetDesignation(com.idanalytics.products.idscore.result.DesignationDocument.Designation designation);
        
        /**
         * Unsets the "Designation" element
         */
        void unsetDesignation();
        
        /**
         * Gets the "AcctLinkKey" element
         */
        java.lang.String getAcctLinkKey();
        
        /**
         * Gets (as xml) the "AcctLinkKey" element
         */
        com.idanalytics.products.idscore.result.AcctLinkKeyDocument.AcctLinkKey xgetAcctLinkKey();
        
        /**
         * True if has "AcctLinkKey" element
         */
        boolean isSetAcctLinkKey();
        
        /**
         * Sets the "AcctLinkKey" element
         */
        void setAcctLinkKey(java.lang.String acctLinkKey);
        
        /**
         * Sets (as xml) the "AcctLinkKey" element
         */
        void xsetAcctLinkKey(com.idanalytics.products.idscore.result.AcctLinkKeyDocument.AcctLinkKey acctLinkKey);
        
        /**
         * Unsets the "AcctLinkKey" element
         */
        void unsetAcctLinkKey();
        
        /**
         * Gets the "IDASequence" element
         */
        java.lang.String getIDASequence();
        
        /**
         * Gets (as xml) the "IDASequence" element
         */
        com.idanalytics.products.idscore.result.IDASequenceDocument.IDASequence xgetIDASequence();
        
        /**
         * Sets the "IDASequence" element
         */
        void setIDASequence(java.lang.String idaSequence);
        
        /**
         * Sets (as xml) the "IDASequence" element
         */
        void xsetIDASequence(com.idanalytics.products.idscore.result.IDASequenceDocument.IDASequence idaSequence);
        
        /**
         * Gets the "IDATimeStamp" element
         */
        java.util.Calendar getIDATimeStamp();
        
        /**
         * Gets (as xml) the "IDATimeStamp" element
         */
        com.idanalytics.products.idscore.result.IDATimeStampDocument.IDATimeStamp xgetIDATimeStamp();
        
        /**
         * Sets the "IDATimeStamp" element
         */
        void setIDATimeStamp(java.util.Calendar idaTimeStamp);
        
        /**
         * Sets (as xml) the "IDATimeStamp" element
         */
        void xsetIDATimeStamp(com.idanalytics.products.idscore.result.IDATimeStampDocument.IDATimeStamp idaTimeStamp);
        
        /**
         * Gets the "PrescreenDate" element
         */
        java.lang.String getPrescreenDate();
        
        /**
         * Gets (as xml) the "PrescreenDate" element
         */
        com.idanalytics.products.common_v1.CampaignIdentifier xgetPrescreenDate();
        
        /**
         * True if has "PrescreenDate" element
         */
        boolean isSetPrescreenDate();
        
        /**
         * Sets the "PrescreenDate" element
         */
        void setPrescreenDate(java.lang.String prescreenDate);
        
        /**
         * Sets (as xml) the "PrescreenDate" element
         */
        void xsetPrescreenDate(com.idanalytics.products.common_v1.CampaignIdentifier prescreenDate);
        
        /**
         * Unsets the "PrescreenDate" element
         */
        void unsetPrescreenDate();
        
        /**
         * Gets the "IDScore" element
         */
        java.lang.String getIDScore();
        
        /**
         * Gets (as xml) the "IDScore" element
         */
        com.idanalytics.products.idscore.result.IDScoreDocument.IDScore xgetIDScore();
        
        /**
         * True if has "IDScore" element
         */
        boolean isSetIDScore();
        
        /**
         * Sets the "IDScore" element
         */
        void setIDScore(java.lang.String idScore);
        
        /**
         * Sets (as xml) the "IDScore" element
         */
        void xsetIDScore(com.idanalytics.products.idscore.result.IDScoreDocument.IDScore idScore);
        
        /**
         * Unsets the "IDScore" element
         */
        void unsetIDScore();
        
        /**
         * Gets the "ScoreBandIndicator" element
         */
        com.idanalytics.products.idscore.result.ScoreBandIndicatorDocument.ScoreBandIndicator.Enum getScoreBandIndicator();
        
        /**
         * Gets (as xml) the "ScoreBandIndicator" element
         */
        com.idanalytics.products.idscore.result.ScoreBandIndicatorDocument.ScoreBandIndicator xgetScoreBandIndicator();
        
        /**
         * True if has "ScoreBandIndicator" element
         */
        boolean isSetScoreBandIndicator();
        
        /**
         * Sets the "ScoreBandIndicator" element
         */
        void setScoreBandIndicator(com.idanalytics.products.idscore.result.ScoreBandIndicatorDocument.ScoreBandIndicator.Enum scoreBandIndicator);
        
        /**
         * Sets (as xml) the "ScoreBandIndicator" element
         */
        void xsetScoreBandIndicator(com.idanalytics.products.idscore.result.ScoreBandIndicatorDocument.ScoreBandIndicator scoreBandIndicator);
        
        /**
         * Unsets the "ScoreBandIndicator" element
         */
        void unsetScoreBandIndicator();
        
        /**
         * Gets the "ScoreDeltaIndicator" element
         */
        com.idanalytics.products.idscore.result.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator.Enum getScoreDeltaIndicator();
        
        /**
         * Gets (as xml) the "ScoreDeltaIndicator" element
         */
        com.idanalytics.products.idscore.result.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator xgetScoreDeltaIndicator();
        
        /**
         * True if has "ScoreDeltaIndicator" element
         */
        boolean isSetScoreDeltaIndicator();
        
        /**
         * Sets the "ScoreDeltaIndicator" element
         */
        void setScoreDeltaIndicator(com.idanalytics.products.idscore.result.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator.Enum scoreDeltaIndicator);
        
        /**
         * Sets (as xml) the "ScoreDeltaIndicator" element
         */
        void xsetScoreDeltaIndicator(com.idanalytics.products.idscore.result.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator scoreDeltaIndicator);
        
        /**
         * Unsets the "ScoreDeltaIndicator" element
         */
        void unsetScoreDeltaIndicator();
        
        /**
         * Gets the "IDScoreResultCode1" element
         */
        java.lang.String getIDScoreResultCode1();
        
        /**
         * Gets (as xml) the "IDScoreResultCode1" element
         */
        com.idanalytics.products.idscore.result.IDScoreResultCode1Document.IDScoreResultCode1 xgetIDScoreResultCode1();
        
        /**
         * True if has "IDScoreResultCode1" element
         */
        boolean isSetIDScoreResultCode1();
        
        /**
         * Sets the "IDScoreResultCode1" element
         */
        void setIDScoreResultCode1(java.lang.String idScoreResultCode1);
        
        /**
         * Sets (as xml) the "IDScoreResultCode1" element
         */
        void xsetIDScoreResultCode1(com.idanalytics.products.idscore.result.IDScoreResultCode1Document.IDScoreResultCode1 idScoreResultCode1);
        
        /**
         * Unsets the "IDScoreResultCode1" element
         */
        void unsetIDScoreResultCode1();
        
        /**
         * Gets the "IDScoreResultCode2" element
         */
        java.lang.String getIDScoreResultCode2();
        
        /**
         * Gets (as xml) the "IDScoreResultCode2" element
         */
        com.idanalytics.products.idscore.result.IDScoreResultCode2Document.IDScoreResultCode2 xgetIDScoreResultCode2();
        
        /**
         * True if has "IDScoreResultCode2" element
         */
        boolean isSetIDScoreResultCode2();
        
        /**
         * Sets the "IDScoreResultCode2" element
         */
        void setIDScoreResultCode2(java.lang.String idScoreResultCode2);
        
        /**
         * Sets (as xml) the "IDScoreResultCode2" element
         */
        void xsetIDScoreResultCode2(com.idanalytics.products.idscore.result.IDScoreResultCode2Document.IDScoreResultCode2 idScoreResultCode2);
        
        /**
         * Unsets the "IDScoreResultCode2" element
         */
        void unsetIDScoreResultCode2();
        
        /**
         * Gets the "IDScoreResultCode3" element
         */
        java.lang.String getIDScoreResultCode3();
        
        /**
         * Gets (as xml) the "IDScoreResultCode3" element
         */
        com.idanalytics.products.idscore.result.IDScoreResultCode3Document.IDScoreResultCode3 xgetIDScoreResultCode3();
        
        /**
         * True if has "IDScoreResultCode3" element
         */
        boolean isSetIDScoreResultCode3();
        
        /**
         * Sets the "IDScoreResultCode3" element
         */
        void setIDScoreResultCode3(java.lang.String idScoreResultCode3);
        
        /**
         * Sets (as xml) the "IDScoreResultCode3" element
         */
        void xsetIDScoreResultCode3(com.idanalytics.products.idscore.result.IDScoreResultCode3Document.IDScoreResultCode3 idScoreResultCode3);
        
        /**
         * Unsets the "IDScoreResultCode3" element
         */
        void unsetIDScoreResultCode3();
        
        /**
         * Gets the "IDScoreResultCode4" element
         */
        java.lang.String getIDScoreResultCode4();
        
        /**
         * Gets (as xml) the "IDScoreResultCode4" element
         */
        com.idanalytics.products.idscore.result.IDScoreResultCode4Document.IDScoreResultCode4 xgetIDScoreResultCode4();
        
        /**
         * True if has "IDScoreResultCode4" element
         */
        boolean isSetIDScoreResultCode4();
        
        /**
         * Sets the "IDScoreResultCode4" element
         */
        void setIDScoreResultCode4(java.lang.String idScoreResultCode4);
        
        /**
         * Sets (as xml) the "IDScoreResultCode4" element
         */
        void xsetIDScoreResultCode4(com.idanalytics.products.idscore.result.IDScoreResultCode4Document.IDScoreResultCode4 idScoreResultCode4);
        
        /**
         * Unsets the "IDScoreResultCode4" element
         */
        void unsetIDScoreResultCode4();
        
        /**
         * Gets the "IDScoreResultCode5" element
         */
        java.lang.String getIDScoreResultCode5();
        
        /**
         * Gets (as xml) the "IDScoreResultCode5" element
         */
        com.idanalytics.products.idscore.result.IDScoreResultCode5Document.IDScoreResultCode5 xgetIDScoreResultCode5();
        
        /**
         * True if has "IDScoreResultCode5" element
         */
        boolean isSetIDScoreResultCode5();
        
        /**
         * Sets the "IDScoreResultCode5" element
         */
        void setIDScoreResultCode5(java.lang.String idScoreResultCode5);
        
        /**
         * Sets (as xml) the "IDScoreResultCode5" element
         */
        void xsetIDScoreResultCode5(com.idanalytics.products.idscore.result.IDScoreResultCode5Document.IDScoreResultCode5 idScoreResultCode5);
        
        /**
         * Unsets the "IDScoreResultCode5" element
         */
        void unsetIDScoreResultCode5();
        
        /**
         * Gets the "IDScoreResultCode6" element
         */
        java.lang.String getIDScoreResultCode6();
        
        /**
         * Gets (as xml) the "IDScoreResultCode6" element
         */
        com.idanalytics.products.idscore.result.IDScoreResultCode6Document.IDScoreResultCode6 xgetIDScoreResultCode6();
        
        /**
         * True if has "IDScoreResultCode6" element
         */
        boolean isSetIDScoreResultCode6();
        
        /**
         * Sets the "IDScoreResultCode6" element
         */
        void setIDScoreResultCode6(java.lang.String idScoreResultCode6);
        
        /**
         * Sets (as xml) the "IDScoreResultCode6" element
         */
        void xsetIDScoreResultCode6(com.idanalytics.products.idscore.result.IDScoreResultCode6Document.IDScoreResultCode6 idScoreResultCode6);
        
        /**
         * Unsets the "IDScoreResultCode6" element
         */
        void unsetIDScoreResultCode6();
        
        /**
         * Gets the "ResultCode1" element
         */
        com.idanalytics.products.idscore.result.ResultCodeType getResultCode1();
        
        /**
         * True if has "ResultCode1" element
         */
        boolean isSetResultCode1();
        
        /**
         * Sets the "ResultCode1" element
         */
        void setResultCode1(com.idanalytics.products.idscore.result.ResultCodeType resultCode1);
        
        /**
         * Appends and returns a new empty "ResultCode1" element
         */
        com.idanalytics.products.idscore.result.ResultCodeType addNewResultCode1();
        
        /**
         * Unsets the "ResultCode1" element
         */
        void unsetResultCode1();
        
        /**
         * Gets the "ResultCode2" element
         */
        com.idanalytics.products.idscore.result.ResultCodeType getResultCode2();
        
        /**
         * True if has "ResultCode2" element
         */
        boolean isSetResultCode2();
        
        /**
         * Sets the "ResultCode2" element
         */
        void setResultCode2(com.idanalytics.products.idscore.result.ResultCodeType resultCode2);
        
        /**
         * Appends and returns a new empty "ResultCode2" element
         */
        com.idanalytics.products.idscore.result.ResultCodeType addNewResultCode2();
        
        /**
         * Unsets the "ResultCode2" element
         */
        void unsetResultCode2();
        
        /**
         * Gets the "ResultCode3" element
         */
        com.idanalytics.products.idscore.result.ResultCodeType getResultCode3();
        
        /**
         * True if has "ResultCode3" element
         */
        boolean isSetResultCode3();
        
        /**
         * Sets the "ResultCode3" element
         */
        void setResultCode3(com.idanalytics.products.idscore.result.ResultCodeType resultCode3);
        
        /**
         * Appends and returns a new empty "ResultCode3" element
         */
        com.idanalytics.products.idscore.result.ResultCodeType addNewResultCode3();
        
        /**
         * Unsets the "ResultCode3" element
         */
        void unsetResultCode3();
        
        /**
         * Gets the "ResultCode4" element
         */
        com.idanalytics.products.idscore.result.ResultCodeType getResultCode4();
        
        /**
         * True if has "ResultCode4" element
         */
        boolean isSetResultCode4();
        
        /**
         * Sets the "ResultCode4" element
         */
        void setResultCode4(com.idanalytics.products.idscore.result.ResultCodeType resultCode4);
        
        /**
         * Appends and returns a new empty "ResultCode4" element
         */
        com.idanalytics.products.idscore.result.ResultCodeType addNewResultCode4();
        
        /**
         * Unsets the "ResultCode4" element
         */
        void unsetResultCode4();
        
        /**
         * Gets the "ResultCode5" element
         */
        com.idanalytics.products.idscore.result.ResultCodeType getResultCode5();
        
        /**
         * True if has "ResultCode5" element
         */
        boolean isSetResultCode5();
        
        /**
         * Sets the "ResultCode5" element
         */
        void setResultCode5(com.idanalytics.products.idscore.result.ResultCodeType resultCode5);
        
        /**
         * Appends and returns a new empty "ResultCode5" element
         */
        com.idanalytics.products.idscore.result.ResultCodeType addNewResultCode5();
        
        /**
         * Unsets the "ResultCode5" element
         */
        void unsetResultCode5();
        
        /**
         * Gets the "ResultCode6" element
         */
        com.idanalytics.products.idscore.result.ResultCodeType getResultCode6();
        
        /**
         * True if has "ResultCode6" element
         */
        boolean isSetResultCode6();
        
        /**
         * Sets the "ResultCode6" element
         */
        void setResultCode6(com.idanalytics.products.idscore.result.ResultCodeType resultCode6);
        
        /**
         * Appends and returns a new empty "ResultCode6" element
         */
        com.idanalytics.products.idscore.result.ResultCodeType addNewResultCode6();
        
        /**
         * Unsets the "ResultCode6" element
         */
        void unsetResultCode6();
        
        /**
         * Gets the "ConsumerStatements" element
         */
        com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.ConsumerStatements getConsumerStatements();
        
        /**
         * True if has "ConsumerStatements" element
         */
        boolean isSetConsumerStatements();
        
        /**
         * Sets the "ConsumerStatements" element
         */
        void setConsumerStatements(com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.ConsumerStatements consumerStatements);
        
        /**
         * Appends and returns a new empty "ConsumerStatements" element
         */
        com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.ConsumerStatements addNewConsumerStatements();
        
        /**
         * Unsets the "ConsumerStatements" element
         */
        void unsetConsumerStatements();
        
        /**
         * Gets the "Messages" element
         */
        com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.Messages getMessages();
        
        /**
         * True if has "Messages" element
         */
        boolean isSetMessages();
        
        /**
         * Sets the "Messages" element
         */
        void setMessages(com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.Messages messages);
        
        /**
         * Appends and returns a new empty "Messages" element
         */
        com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.Messages addNewMessages();
        
        /**
         * Unsets the "Messages" element
         */
        void unsetMessages();
        
        /**
         * Gets the "Signals" element
         */
        com.idanalytics.products.idscore.result.SignalsDocument.Signals getSignals();
        
        /**
         * True if has "Signals" element
         */
        boolean isSetSignals();
        
        /**
         * Sets the "Signals" element
         */
        void setSignals(com.idanalytics.products.idscore.result.SignalsDocument.Signals signals);
        
        /**
         * Appends and returns a new empty "Signals" element
         */
        com.idanalytics.products.idscore.result.SignalsDocument.Signals addNewSignals();
        
        /**
         * Unsets the "Signals" element
         */
        void unsetSignals();
        
        /**
         * Gets the "PassThru1" element
         */
        java.lang.String getPassThru1();
        
        /**
         * Gets (as xml) the "PassThru1" element
         */
        com.idanalytics.products.idscore.result.PassThru1Document.PassThru1 xgetPassThru1();
        
        /**
         * True if has "PassThru1" element
         */
        boolean isSetPassThru1();
        
        /**
         * Sets the "PassThru1" element
         */
        void setPassThru1(java.lang.String passThru1);
        
        /**
         * Sets (as xml) the "PassThru1" element
         */
        void xsetPassThru1(com.idanalytics.products.idscore.result.PassThru1Document.PassThru1 passThru1);
        
        /**
         * Unsets the "PassThru1" element
         */
        void unsetPassThru1();
        
        /**
         * Gets the "PassThru2" element
         */
        java.lang.String getPassThru2();
        
        /**
         * Gets (as xml) the "PassThru2" element
         */
        com.idanalytics.products.idscore.result.PassThru2Document.PassThru2 xgetPassThru2();
        
        /**
         * True if has "PassThru2" element
         */
        boolean isSetPassThru2();
        
        /**
         * Sets the "PassThru2" element
         */
        void setPassThru2(java.lang.String passThru2);
        
        /**
         * Sets (as xml) the "PassThru2" element
         */
        void xsetPassThru2(com.idanalytics.products.idscore.result.PassThru2Document.PassThru2 passThru2);
        
        /**
         * Unsets the "PassThru2" element
         */
        void unsetPassThru2();
        
        /**
         * Gets the "PassThru3" element
         */
        java.lang.String getPassThru3();
        
        /**
         * Gets (as xml) the "PassThru3" element
         */
        com.idanalytics.products.idscore.result.PassThru3Document.PassThru3 xgetPassThru3();
        
        /**
         * True if has "PassThru3" element
         */
        boolean isSetPassThru3();
        
        /**
         * Sets the "PassThru3" element
         */
        void setPassThru3(java.lang.String passThru3);
        
        /**
         * Sets (as xml) the "PassThru3" element
         */
        void xsetPassThru3(com.idanalytics.products.idscore.result.PassThru3Document.PassThru3 passThru3);
        
        /**
         * Unsets the "PassThru3" element
         */
        void unsetPassThru3();
        
        /**
         * Gets the "PassThru4" element
         */
        java.lang.String getPassThru4();
        
        /**
         * Gets (as xml) the "PassThru4" element
         */
        com.idanalytics.products.idscore.result.PassThru4Document.PassThru4 xgetPassThru4();
        
        /**
         * True if has "PassThru4" element
         */
        boolean isSetPassThru4();
        
        /**
         * Sets the "PassThru4" element
         */
        void setPassThru4(java.lang.String passThru4);
        
        /**
         * Sets (as xml) the "PassThru4" element
         */
        void xsetPassThru4(com.idanalytics.products.idscore.result.PassThru4Document.PassThru4 passThru4);
        
        /**
         * Unsets the "PassThru4" element
         */
        void unsetPassThru4();
        
        /**
         * Gets the "Indicators" element
         */
        com.idanalytics.products.idscore.result.IndicatorsDocument.Indicators getIndicators();
        
        /**
         * True if has "Indicators" element
         */
        boolean isSetIndicators();
        
        /**
         * Sets the "Indicators" element
         */
        void setIndicators(com.idanalytics.products.idscore.result.IndicatorsDocument.Indicators indicators);
        
        /**
         * Appends and returns a new empty "Indicators" element
         */
        com.idanalytics.products.idscore.result.IndicatorsDocument.Indicators addNewIndicators();
        
        /**
         * Unsets the "Indicators" element
         */
        void unsetIndicators();
        
        /**
         * Gets the "IDAInternal" element
         */
        com.idanalytics.products.idscore.result.IDAInternalDocument.IDAInternal getIDAInternal();
        
        /**
         * True if has "IDAInternal" element
         */
        boolean isSetIDAInternal();
        
        /**
         * Sets the "IDAInternal" element
         */
        void setIDAInternal(com.idanalytics.products.idscore.result.IDAInternalDocument.IDAInternal idaInternal);
        
        /**
         * Appends and returns a new empty "IDAInternal" element
         */
        com.idanalytics.products.idscore.result.IDAInternalDocument.IDAInternal addNewIDAInternal();
        
        /**
         * Unsets the "IDAInternal" element
         */
        void unsetIDAInternal();
        
        /**
         * Gets the "PreviousEvents" element
         */
        com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.PreviousEvents getPreviousEvents();
        
        /**
         * True if has "PreviousEvents" element
         */
        boolean isSetPreviousEvents();
        
        /**
         * Sets the "PreviousEvents" element
         */
        void setPreviousEvents(com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.PreviousEvents previousEvents);
        
        /**
         * Appends and returns a new empty "PreviousEvents" element
         */
        com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.PreviousEvents addNewPreviousEvents();
        
        /**
         * Unsets the "PreviousEvents" element
         */
        void unsetPreviousEvents();
        
        /**
         * Gets the "schemaVersion" attribute
         */
        java.math.BigDecimal getSchemaVersion();
        
        /**
         * Gets (as xml) the "schemaVersion" attribute
         */
        org.apache.xmlbeans.XmlDecimal xgetSchemaVersion();
        
        /**
         * True if has "schemaVersion" attribute
         */
        boolean isSetSchemaVersion();
        
        /**
         * Sets the "schemaVersion" attribute
         */
        void setSchemaVersion(java.math.BigDecimal schemaVersion);
        
        /**
         * Sets (as xml) the "schemaVersion" attribute
         */
        void xsetSchemaVersion(org.apache.xmlbeans.XmlDecimal schemaVersion);
        
        /**
         * Unsets the "schemaVersion" attribute
         */
        void unsetSchemaVersion();
        
        /**
         * An XML ConsumerStatements(@http://idanalytics.com/products/idscore/result).
         *
         * This is a complex type.
         */
        public interface ConsumerStatements extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ConsumerStatements.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("consumerstatements54eaelemtype");
            
            /**
             * Gets array of all "ConsumerStatement" elements
             */
            com.idanalytics.products.idscore.result.ConsumerStatementType[] getConsumerStatementArray();
            
            /**
             * Gets ith "ConsumerStatement" element
             */
            com.idanalytics.products.idscore.result.ConsumerStatementType getConsumerStatementArray(int i);
            
            /**
             * Returns number of "ConsumerStatement" element
             */
            int sizeOfConsumerStatementArray();
            
            /**
             * Sets array of all "ConsumerStatement" element
             */
            void setConsumerStatementArray(com.idanalytics.products.idscore.result.ConsumerStatementType[] consumerStatementArray);
            
            /**
             * Sets ith "ConsumerStatement" element
             */
            void setConsumerStatementArray(int i, com.idanalytics.products.idscore.result.ConsumerStatementType consumerStatement);
            
            /**
             * Inserts and returns a new empty value (as xml) as the ith "ConsumerStatement" element
             */
            com.idanalytics.products.idscore.result.ConsumerStatementType insertNewConsumerStatement(int i);
            
            /**
             * Appends and returns a new empty value (as xml) as the last "ConsumerStatement" element
             */
            com.idanalytics.products.idscore.result.ConsumerStatementType addNewConsumerStatement();
            
            /**
             * Removes the ith "ConsumerStatement" element
             */
            void removeConsumerStatement(int i);
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.ConsumerStatements newInstance() {
                  return (com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.ConsumerStatements) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.ConsumerStatements newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.ConsumerStatements) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * An XML Messages(@http://idanalytics.com/products/idscore/result).
         *
         * This is a complex type.
         */
        public interface Messages extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Messages.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("messages4c58elemtype");
            
            /**
             * Gets array of all "Message" elements
             */
            com.idanalytics.products.idscore.result.MessageType[] getMessageArray();
            
            /**
             * Gets ith "Message" element
             */
            com.idanalytics.products.idscore.result.MessageType getMessageArray(int i);
            
            /**
             * Returns number of "Message" element
             */
            int sizeOfMessageArray();
            
            /**
             * Sets array of all "Message" element
             */
            void setMessageArray(com.idanalytics.products.idscore.result.MessageType[] messageArray);
            
            /**
             * Sets ith "Message" element
             */
            void setMessageArray(int i, com.idanalytics.products.idscore.result.MessageType message);
            
            /**
             * Inserts and returns a new empty value (as xml) as the ith "Message" element
             */
            com.idanalytics.products.idscore.result.MessageType insertNewMessage(int i);
            
            /**
             * Appends and returns a new empty value (as xml) as the last "Message" element
             */
            com.idanalytics.products.idscore.result.MessageType addNewMessage();
            
            /**
             * Removes the ith "Message" element
             */
            void removeMessage(int i);
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.Messages newInstance() {
                  return (com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.Messages) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.Messages newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.Messages) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * An XML PreviousEvents(@http://idanalytics.com/products/idscore/result).
         *
         * This is a complex type.
         */
        public interface PreviousEvents extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(PreviousEvents.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("previousevents2f34elemtype");
            
            /**
             * Gets array of all "PreviousEvent" elements
             */
            com.idanalytics.products.idscore.result.IdentityEvent[] getPreviousEventArray();
            
            /**
             * Gets ith "PreviousEvent" element
             */
            com.idanalytics.products.idscore.result.IdentityEvent getPreviousEventArray(int i);
            
            /**
             * Returns number of "PreviousEvent" element
             */
            int sizeOfPreviousEventArray();
            
            /**
             * Sets array of all "PreviousEvent" element
             */
            void setPreviousEventArray(com.idanalytics.products.idscore.result.IdentityEvent[] previousEventArray);
            
            /**
             * Sets ith "PreviousEvent" element
             */
            void setPreviousEventArray(int i, com.idanalytics.products.idscore.result.IdentityEvent previousEvent);
            
            /**
             * Inserts and returns a new empty value (as xml) as the ith "PreviousEvent" element
             */
            com.idanalytics.products.idscore.result.IdentityEvent insertNewPreviousEvent(int i);
            
            /**
             * Appends and returns a new empty value (as xml) as the last "PreviousEvent" element
             */
            com.idanalytics.products.idscore.result.IdentityEvent addNewPreviousEvent();
            
            /**
             * Removes the ith "PreviousEvent" element
             */
            void removePreviousEvent(int i);
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.PreviousEvents newInstance() {
                  return (com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.PreviousEvents) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.PreviousEvents newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.PreviousEvents) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord newInstance() {
              return (com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.idscore.result.OutputRecordDocument newInstance() {
          return (com.idanalytics.products.idscore.result.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.idscore.result.OutputRecordDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.idscore.result.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.idscore.result.OutputRecordDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.idscore.result.OutputRecordDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.idscore.result.OutputRecordDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.idscore.result.OutputRecordDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.idscore.result.OutputRecordDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.idscore.result.OutputRecordDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.idscore.result.OutputRecordDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.idscore.result.OutputRecordDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.idscore.result.OutputRecordDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.idscore.result.OutputRecordDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.idscore.result.OutputRecordDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.idscore.result.OutputRecordDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.idscore.result.OutputRecordDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.idscore.result.OutputRecordDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.result.OutputRecordDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.result.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.result.OutputRecordDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.result.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
