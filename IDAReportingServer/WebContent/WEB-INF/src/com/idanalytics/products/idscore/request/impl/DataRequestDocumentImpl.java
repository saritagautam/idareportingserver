/*
 * An XML document type.
 * Localname: DataRequest
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.DataRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * A document containing one DataRequest(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public class DataRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.DataRequestDocument
{
    private static final long serialVersionUID = 1L;
    
    public DataRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName DATAREQUEST$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "DataRequest");
    
    
    /**
     * Gets the "DataRequest" element
     */
    public com.idanalytics.products.idscore.request.DataRequestDocument.DataRequest getDataRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.DataRequestDocument.DataRequest target = null;
            target = (com.idanalytics.products.idscore.request.DataRequestDocument.DataRequest)get_store().find_element_user(DATAREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "DataRequest" element
     */
    public void setDataRequest(com.idanalytics.products.idscore.request.DataRequestDocument.DataRequest dataRequest)
    {
        generatedSetterHelperImpl(dataRequest, DATAREQUEST$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "DataRequest" element
     */
    public com.idanalytics.products.idscore.request.DataRequestDocument.DataRequest addNewDataRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.DataRequestDocument.DataRequest target = null;
            target = (com.idanalytics.products.idscore.request.DataRequestDocument.DataRequest)get_store().add_element_user(DATAREQUEST$0);
            return target;
        }
    }
    /**
     * An XML DataRequest(@http://idanalytics.com/products/idscore/request).
     *
     * This is a complex type.
     */
    public static class DataRequestImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.DataRequestDocument.DataRequest
    {
        private static final long serialVersionUID = 1L;
        
        public DataRequestImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName IDAINTERNAL$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "IDAInternal");
        
        
        /**
         * Gets the "IDAInternal" element
         */
        public com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal getIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal target = null;
                target = (com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal)get_store().find_element_user(IDAINTERNAL$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "IDAInternal" element
         */
        public boolean isSetIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(IDAINTERNAL$0) != 0;
            }
        }
        
        /**
         * Sets the "IDAInternal" element
         */
        public void setIDAInternal(com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal idaInternal)
        {
            generatedSetterHelperImpl(idaInternal, IDAINTERNAL$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "IDAInternal" element
         */
        public com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal addNewIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal target = null;
                target = (com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal)get_store().add_element_user(IDAINTERNAL$0);
                return target;
            }
        }
        
        /**
         * Unsets the "IDAInternal" element
         */
        public void unsetIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(IDAINTERNAL$0, 0);
            }
        }
    }
}
