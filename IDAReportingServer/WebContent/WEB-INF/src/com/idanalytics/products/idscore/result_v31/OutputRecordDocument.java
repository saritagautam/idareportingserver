/*
 * An XML document type.
 * Localname: OutputRecord
 * Namespace: http://idanalytics.com/products/idscore/result.v31
 * Java type: com.idanalytics.products.idscore.result_v31.OutputRecordDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.result_v31;


/**
 * A document containing one OutputRecord(@http://idanalytics.com/products/idscore/result.v31) element.
 *
 * This is a complex type.
 */
public interface OutputRecordDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(OutputRecordDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("outputrecord3dd8doctype");
    
    /**
     * Gets the "OutputRecord" element
     */
    com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord getOutputRecord();
    
    /**
     * Sets the "OutputRecord" element
     */
    void setOutputRecord(com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord outputRecord);
    
    /**
     * Appends and returns a new empty "OutputRecord" element
     */
    com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord addNewOutputRecord();
    
    /**
     * An XML OutputRecord(@http://idanalytics.com/products/idscore/result.v31).
     *
     * This is a complex type.
     */
    public interface OutputRecord extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(OutputRecord.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("outputrecord4fc6elemtype");
        
        /**
         * Gets the "IDAStatus" element
         */
        int getIDAStatus();
        
        /**
         * Gets (as xml) the "IDAStatus" element
         */
        com.idanalytics.products.idscore.result_v31.IDAStatusDocument.IDAStatus xgetIDAStatus();
        
        /**
         * Sets the "IDAStatus" element
         */
        void setIDAStatus(int idaStatus);
        
        /**
         * Sets (as xml) the "IDAStatus" element
         */
        void xsetIDAStatus(com.idanalytics.products.idscore.result_v31.IDAStatusDocument.IDAStatus idaStatus);
        
        /**
         * Gets the "AppID" element
         */
        java.lang.String getAppID();
        
        /**
         * Gets (as xml) the "AppID" element
         */
        com.idanalytics.products.common_v1.AppID xgetAppID();
        
        /**
         * True if has "AppID" element
         */
        boolean isSetAppID();
        
        /**
         * Sets the "AppID" element
         */
        void setAppID(java.lang.String appID);
        
        /**
         * Sets (as xml) the "AppID" element
         */
        void xsetAppID(com.idanalytics.products.common_v1.AppID appID);
        
        /**
         * Unsets the "AppID" element
         */
        void unsetAppID();
        
        /**
         * Gets the "ClientKey" element
         */
        java.lang.String getClientKey();
        
        /**
         * Gets (as xml) the "ClientKey" element
         */
        com.idanalytics.products.idscore.result_v31.ClientKeyDocument.ClientKey xgetClientKey();
        
        /**
         * True if has "ClientKey" element
         */
        boolean isSetClientKey();
        
        /**
         * Sets the "ClientKey" element
         */
        void setClientKey(java.lang.String clientKey);
        
        /**
         * Sets (as xml) the "ClientKey" element
         */
        void xsetClientKey(com.idanalytics.products.idscore.result_v31.ClientKeyDocument.ClientKey clientKey);
        
        /**
         * Unsets the "ClientKey" element
         */
        void unsetClientKey();
        
        /**
         * Gets the "ConsumerID" element
         */
        java.lang.String getConsumerID();
        
        /**
         * Gets (as xml) the "ConsumerID" element
         */
        com.idanalytics.products.idscore.result_v31.ConsumerIDDocument.ConsumerID xgetConsumerID();
        
        /**
         * True if has "ConsumerID" element
         */
        boolean isSetConsumerID();
        
        /**
         * Sets the "ConsumerID" element
         */
        void setConsumerID(java.lang.String consumerID);
        
        /**
         * Sets (as xml) the "ConsumerID" element
         */
        void xsetConsumerID(com.idanalytics.products.idscore.result_v31.ConsumerIDDocument.ConsumerID consumerID);
        
        /**
         * Unsets the "ConsumerID" element
         */
        void unsetConsumerID();
        
        /**
         * Gets the "Designation" element
         */
        com.idanalytics.products.idscore.result_v31.DesignationDocument.Designation.Enum getDesignation();
        
        /**
         * Gets (as xml) the "Designation" element
         */
        com.idanalytics.products.idscore.result_v31.DesignationDocument.Designation xgetDesignation();
        
        /**
         * True if has "Designation" element
         */
        boolean isSetDesignation();
        
        /**
         * Sets the "Designation" element
         */
        void setDesignation(com.idanalytics.products.idscore.result_v31.DesignationDocument.Designation.Enum designation);
        
        /**
         * Sets (as xml) the "Designation" element
         */
        void xsetDesignation(com.idanalytics.products.idscore.result_v31.DesignationDocument.Designation designation);
        
        /**
         * Unsets the "Designation" element
         */
        void unsetDesignation();
        
        /**
         * Gets the "AcctLinkKey" element
         */
        java.lang.String getAcctLinkKey();
        
        /**
         * Gets (as xml) the "AcctLinkKey" element
         */
        com.idanalytics.products.idscore.result_v31.AcctLinkKeyDocument.AcctLinkKey xgetAcctLinkKey();
        
        /**
         * True if has "AcctLinkKey" element
         */
        boolean isSetAcctLinkKey();
        
        /**
         * Sets the "AcctLinkKey" element
         */
        void setAcctLinkKey(java.lang.String acctLinkKey);
        
        /**
         * Sets (as xml) the "AcctLinkKey" element
         */
        void xsetAcctLinkKey(com.idanalytics.products.idscore.result_v31.AcctLinkKeyDocument.AcctLinkKey acctLinkKey);
        
        /**
         * Unsets the "AcctLinkKey" element
         */
        void unsetAcctLinkKey();
        
        /**
         * Gets the "IDASequence" element
         */
        java.lang.String getIDASequence();
        
        /**
         * Gets (as xml) the "IDASequence" element
         */
        com.idanalytics.products.idscore.result_v31.IDASequenceDocument.IDASequence xgetIDASequence();
        
        /**
         * Sets the "IDASequence" element
         */
        void setIDASequence(java.lang.String idaSequence);
        
        /**
         * Sets (as xml) the "IDASequence" element
         */
        void xsetIDASequence(com.idanalytics.products.idscore.result_v31.IDASequenceDocument.IDASequence idaSequence);
        
        /**
         * Gets the "IDATimeStamp" element
         */
        java.util.Calendar getIDATimeStamp();
        
        /**
         * Gets (as xml) the "IDATimeStamp" element
         */
        com.idanalytics.products.idscore.result_v31.IDATimeStampDocument.IDATimeStamp xgetIDATimeStamp();
        
        /**
         * Sets the "IDATimeStamp" element
         */
        void setIDATimeStamp(java.util.Calendar idaTimeStamp);
        
        /**
         * Sets (as xml) the "IDATimeStamp" element
         */
        void xsetIDATimeStamp(com.idanalytics.products.idscore.result_v31.IDATimeStampDocument.IDATimeStamp idaTimeStamp);
        
        /**
         * Gets array of all "Score" elements
         */
        com.idanalytics.products.idscore.result_v31.Score[] getScoreArray();
        
        /**
         * Gets ith "Score" element
         */
        com.idanalytics.products.idscore.result_v31.Score getScoreArray(int i);
        
        /**
         * Returns number of "Score" element
         */
        int sizeOfScoreArray();
        
        /**
         * Sets array of all "Score" element
         */
        void setScoreArray(com.idanalytics.products.idscore.result_v31.Score[] scoreArray);
        
        /**
         * Sets ith "Score" element
         */
        void setScoreArray(int i, com.idanalytics.products.idscore.result_v31.Score score);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "Score" element
         */
        com.idanalytics.products.idscore.result_v31.Score insertNewScore(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "Score" element
         */
        com.idanalytics.products.idscore.result_v31.Score addNewScore();
        
        /**
         * Removes the ith "Score" element
         */
        void removeScore(int i);
        
        /**
         * Gets the "ConsumerStatements" element
         */
        com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.ConsumerStatements getConsumerStatements();
        
        /**
         * True if has "ConsumerStatements" element
         */
        boolean isSetConsumerStatements();
        
        /**
         * Sets the "ConsumerStatements" element
         */
        void setConsumerStatements(com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.ConsumerStatements consumerStatements);
        
        /**
         * Appends and returns a new empty "ConsumerStatements" element
         */
        com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.ConsumerStatements addNewConsumerStatements();
        
        /**
         * Unsets the "ConsumerStatements" element
         */
        void unsetConsumerStatements();
        
        /**
         * Gets the "Messages" element
         */
        com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.Messages getMessages();
        
        /**
         * True if has "Messages" element
         */
        boolean isSetMessages();
        
        /**
         * Sets the "Messages" element
         */
        void setMessages(com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.Messages messages);
        
        /**
         * Appends and returns a new empty "Messages" element
         */
        com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.Messages addNewMessages();
        
        /**
         * Unsets the "Messages" element
         */
        void unsetMessages();
        
        /**
         * Gets the "PassThru1" element
         */
        java.lang.String getPassThru1();
        
        /**
         * Gets (as xml) the "PassThru1" element
         */
        com.idanalytics.products.idscore.result_v31.PassThru1Document.PassThru1 xgetPassThru1();
        
        /**
         * True if has "PassThru1" element
         */
        boolean isSetPassThru1();
        
        /**
         * Sets the "PassThru1" element
         */
        void setPassThru1(java.lang.String passThru1);
        
        /**
         * Sets (as xml) the "PassThru1" element
         */
        void xsetPassThru1(com.idanalytics.products.idscore.result_v31.PassThru1Document.PassThru1 passThru1);
        
        /**
         * Unsets the "PassThru1" element
         */
        void unsetPassThru1();
        
        /**
         * Gets the "PassThru2" element
         */
        java.lang.String getPassThru2();
        
        /**
         * Gets (as xml) the "PassThru2" element
         */
        com.idanalytics.products.idscore.result_v31.PassThru2Document.PassThru2 xgetPassThru2();
        
        /**
         * True if has "PassThru2" element
         */
        boolean isSetPassThru2();
        
        /**
         * Sets the "PassThru2" element
         */
        void setPassThru2(java.lang.String passThru2);
        
        /**
         * Sets (as xml) the "PassThru2" element
         */
        void xsetPassThru2(com.idanalytics.products.idscore.result_v31.PassThru2Document.PassThru2 passThru2);
        
        /**
         * Unsets the "PassThru2" element
         */
        void unsetPassThru2();
        
        /**
         * Gets the "PassThru3" element
         */
        java.lang.String getPassThru3();
        
        /**
         * Gets (as xml) the "PassThru3" element
         */
        com.idanalytics.products.idscore.result_v31.PassThru3Document.PassThru3 xgetPassThru3();
        
        /**
         * True if has "PassThru3" element
         */
        boolean isSetPassThru3();
        
        /**
         * Sets the "PassThru3" element
         */
        void setPassThru3(java.lang.String passThru3);
        
        /**
         * Sets (as xml) the "PassThru3" element
         */
        void xsetPassThru3(com.idanalytics.products.idscore.result_v31.PassThru3Document.PassThru3 passThru3);
        
        /**
         * Unsets the "PassThru3" element
         */
        void unsetPassThru3();
        
        /**
         * Gets the "PassThru4" element
         */
        java.lang.String getPassThru4();
        
        /**
         * Gets (as xml) the "PassThru4" element
         */
        com.idanalytics.products.idscore.result_v31.PassThru4Document.PassThru4 xgetPassThru4();
        
        /**
         * True if has "PassThru4" element
         */
        boolean isSetPassThru4();
        
        /**
         * Sets the "PassThru4" element
         */
        void setPassThru4(java.lang.String passThru4);
        
        /**
         * Sets (as xml) the "PassThru4" element
         */
        void xsetPassThru4(com.idanalytics.products.idscore.result_v31.PassThru4Document.PassThru4 passThru4);
        
        /**
         * Unsets the "PassThru4" element
         */
        void unsetPassThru4();
        
        /**
         * Gets the "Indicators" element
         */
        com.idanalytics.products.idscore.result_v31.IndicatorsDocument.Indicators getIndicators();
        
        /**
         * True if has "Indicators" element
         */
        boolean isSetIndicators();
        
        /**
         * Sets the "Indicators" element
         */
        void setIndicators(com.idanalytics.products.idscore.result_v31.IndicatorsDocument.Indicators indicators);
        
        /**
         * Appends and returns a new empty "Indicators" element
         */
        com.idanalytics.products.idscore.result_v31.IndicatorsDocument.Indicators addNewIndicators();
        
        /**
         * Unsets the "Indicators" element
         */
        void unsetIndicators();
        
        /**
         * Gets the "IDAInternal" element
         */
        com.idanalytics.products.idscore.result_v31.IDAInternalDocument.IDAInternal getIDAInternal();
        
        /**
         * True if has "IDAInternal" element
         */
        boolean isSetIDAInternal();
        
        /**
         * Sets the "IDAInternal" element
         */
        void setIDAInternal(com.idanalytics.products.idscore.result_v31.IDAInternalDocument.IDAInternal idaInternal);
        
        /**
         * Appends and returns a new empty "IDAInternal" element
         */
        com.idanalytics.products.idscore.result_v31.IDAInternalDocument.IDAInternal addNewIDAInternal();
        
        /**
         * Unsets the "IDAInternal" element
         */
        void unsetIDAInternal();
        
        /**
         * Gets the "PreviousEvents" element
         */
        com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.PreviousEvents getPreviousEvents();
        
        /**
         * True if has "PreviousEvents" element
         */
        boolean isSetPreviousEvents();
        
        /**
         * Sets the "PreviousEvents" element
         */
        void setPreviousEvents(com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.PreviousEvents previousEvents);
        
        /**
         * Appends and returns a new empty "PreviousEvents" element
         */
        com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.PreviousEvents addNewPreviousEvents();
        
        /**
         * Unsets the "PreviousEvents" element
         */
        void unsetPreviousEvents();
        
        /**
         * Gets the "schemaVersion" attribute
         */
        java.math.BigDecimal getSchemaVersion();
        
        /**
         * Gets (as xml) the "schemaVersion" attribute
         */
        org.apache.xmlbeans.XmlDecimal xgetSchemaVersion();
        
        /**
         * True if has "schemaVersion" attribute
         */
        boolean isSetSchemaVersion();
        
        /**
         * Sets the "schemaVersion" attribute
         */
        void setSchemaVersion(java.math.BigDecimal schemaVersion);
        
        /**
         * Sets (as xml) the "schemaVersion" attribute
         */
        void xsetSchemaVersion(org.apache.xmlbeans.XmlDecimal schemaVersion);
        
        /**
         * Unsets the "schemaVersion" attribute
         */
        void unsetSchemaVersion();
        
        /**
         * An XML ConsumerStatements(@http://idanalytics.com/products/idscore/result.v31).
         *
         * This is a complex type.
         */
        public interface ConsumerStatements extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ConsumerStatements.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("consumerstatements7a50elemtype");
            
            /**
             * Gets array of all "ConsumerStatement" elements
             */
            com.idanalytics.products.idscore.result_v31.ConsumerStatementType[] getConsumerStatementArray();
            
            /**
             * Gets ith "ConsumerStatement" element
             */
            com.idanalytics.products.idscore.result_v31.ConsumerStatementType getConsumerStatementArray(int i);
            
            /**
             * Returns number of "ConsumerStatement" element
             */
            int sizeOfConsumerStatementArray();
            
            /**
             * Sets array of all "ConsumerStatement" element
             */
            void setConsumerStatementArray(com.idanalytics.products.idscore.result_v31.ConsumerStatementType[] consumerStatementArray);
            
            /**
             * Sets ith "ConsumerStatement" element
             */
            void setConsumerStatementArray(int i, com.idanalytics.products.idscore.result_v31.ConsumerStatementType consumerStatement);
            
            /**
             * Inserts and returns a new empty value (as xml) as the ith "ConsumerStatement" element
             */
            com.idanalytics.products.idscore.result_v31.ConsumerStatementType insertNewConsumerStatement(int i);
            
            /**
             * Appends and returns a new empty value (as xml) as the last "ConsumerStatement" element
             */
            com.idanalytics.products.idscore.result_v31.ConsumerStatementType addNewConsumerStatement();
            
            /**
             * Removes the ith "ConsumerStatement" element
             */
            void removeConsumerStatement(int i);
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.ConsumerStatements newInstance() {
                  return (com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.ConsumerStatements) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.ConsumerStatements newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.ConsumerStatements) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * An XML Messages(@http://idanalytics.com/products/idscore/result.v31).
         *
         * This is a complex type.
         */
        public interface Messages extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Messages.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("messages0abeelemtype");
            
            /**
             * Gets array of all "Message" elements
             */
            com.idanalytics.products.idscore.result_v31.MessageType[] getMessageArray();
            
            /**
             * Gets ith "Message" element
             */
            com.idanalytics.products.idscore.result_v31.MessageType getMessageArray(int i);
            
            /**
             * Returns number of "Message" element
             */
            int sizeOfMessageArray();
            
            /**
             * Sets array of all "Message" element
             */
            void setMessageArray(com.idanalytics.products.idscore.result_v31.MessageType[] messageArray);
            
            /**
             * Sets ith "Message" element
             */
            void setMessageArray(int i, com.idanalytics.products.idscore.result_v31.MessageType message);
            
            /**
             * Inserts and returns a new empty value (as xml) as the ith "Message" element
             */
            com.idanalytics.products.idscore.result_v31.MessageType insertNewMessage(int i);
            
            /**
             * Appends and returns a new empty value (as xml) as the last "Message" element
             */
            com.idanalytics.products.idscore.result_v31.MessageType addNewMessage();
            
            /**
             * Removes the ith "Message" element
             */
            void removeMessage(int i);
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.Messages newInstance() {
                  return (com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.Messages) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.Messages newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.Messages) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * An XML PreviousEvents(@http://idanalytics.com/products/idscore/result.v31).
         *
         * This is a complex type.
         */
        public interface PreviousEvents extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(PreviousEvents.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("previousevents1f9aelemtype");
            
            /**
             * Gets array of all "PreviousEvent" elements
             */
            com.idanalytics.products.idscore.result_v31.IdentityEvent[] getPreviousEventArray();
            
            /**
             * Gets ith "PreviousEvent" element
             */
            com.idanalytics.products.idscore.result_v31.IdentityEvent getPreviousEventArray(int i);
            
            /**
             * Returns number of "PreviousEvent" element
             */
            int sizeOfPreviousEventArray();
            
            /**
             * Sets array of all "PreviousEvent" element
             */
            void setPreviousEventArray(com.idanalytics.products.idscore.result_v31.IdentityEvent[] previousEventArray);
            
            /**
             * Sets ith "PreviousEvent" element
             */
            void setPreviousEventArray(int i, com.idanalytics.products.idscore.result_v31.IdentityEvent previousEvent);
            
            /**
             * Inserts and returns a new empty value (as xml) as the ith "PreviousEvent" element
             */
            com.idanalytics.products.idscore.result_v31.IdentityEvent insertNewPreviousEvent(int i);
            
            /**
             * Appends and returns a new empty value (as xml) as the last "PreviousEvent" element
             */
            com.idanalytics.products.idscore.result_v31.IdentityEvent addNewPreviousEvent();
            
            /**
             * Removes the ith "PreviousEvent" element
             */
            void removePreviousEvent(int i);
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.PreviousEvents newInstance() {
                  return (com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.PreviousEvents) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.PreviousEvents newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord.PreviousEvents) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord newInstance() {
              return (com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.idscore.result_v31.OutputRecordDocument.OutputRecord) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.idscore.result_v31.OutputRecordDocument newInstance() {
          return (com.idanalytics.products.idscore.result_v31.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.idscore.result_v31.OutputRecordDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.idscore.result_v31.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.idscore.result_v31.OutputRecordDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result_v31.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.idscore.result_v31.OutputRecordDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result_v31.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.idscore.result_v31.OutputRecordDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result_v31.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.idscore.result_v31.OutputRecordDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result_v31.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.idscore.result_v31.OutputRecordDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result_v31.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.idscore.result_v31.OutputRecordDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result_v31.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.idscore.result_v31.OutputRecordDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result_v31.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.idscore.result_v31.OutputRecordDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result_v31.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.idscore.result_v31.OutputRecordDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result_v31.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.idscore.result_v31.OutputRecordDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result_v31.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.idscore.result_v31.OutputRecordDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result_v31.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.idscore.result_v31.OutputRecordDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result_v31.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.idscore.result_v31.OutputRecordDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result_v31.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.idscore.result_v31.OutputRecordDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result_v31.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.result_v31.OutputRecordDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.result_v31.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.result_v31.OutputRecordDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.result_v31.OutputRecordDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
