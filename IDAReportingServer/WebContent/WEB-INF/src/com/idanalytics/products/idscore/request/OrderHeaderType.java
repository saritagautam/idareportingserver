/*
 * XML Type:  OrderHeader_type
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.OrderHeaderType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request;


/**
 * An XML OrderHeader_type(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public interface OrderHeaderType extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(OrderHeaderType.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("orderheadertype5b16type");
    
    /**
     * Gets the "CPC" element
     */
    java.lang.String getCPC();
    
    /**
     * Gets (as xml) the "CPC" element
     */
    com.idanalytics.products.idscore.request.CPCDocument.CPC xgetCPC();
    
    /**
     * True if has "CPC" element
     */
    boolean isSetCPC();
    
    /**
     * Sets the "CPC" element
     */
    void setCPC(java.lang.String cpc);
    
    /**
     * Sets (as xml) the "CPC" element
     */
    void xsetCPC(com.idanalytics.products.idscore.request.CPCDocument.CPC cpc);
    
    /**
     * Unsets the "CPC" element
     */
    void unsetCPC();
    
    /**
     * Gets the "RequestType" element
     */
    java.lang.Object getRequestType();
    
    /**
     * Gets (as xml) the "RequestType" element
     */
    com.idanalytics.products.idscore.request.RequestTypeDocument.RequestType xgetRequestType();
    
    /**
     * Sets the "RequestType" element
     */
    void setRequestType(java.lang.Object requestType);
    
    /**
     * Sets (as xml) the "RequestType" element
     */
    void xsetRequestType(com.idanalytics.products.idscore.request.RequestTypeDocument.RequestType requestType);
    
    /**
     * Gets the "OrderDate" element
     */
    java.util.Calendar getOrderDate();
    
    /**
     * Gets (as xml) the "OrderDate" element
     */
    com.idanalytics.products.idscore.request.OrderDateDocument.OrderDate xgetOrderDate();
    
    /**
     * Sets the "OrderDate" element
     */
    void setOrderDate(java.util.Calendar orderDate);
    
    /**
     * Sets (as xml) the "OrderDate" element
     */
    void xsetOrderDate(com.idanalytics.products.idscore.request.OrderDateDocument.OrderDate orderDate);
    
    /**
     * Gets the "ClientKey" element
     */
    java.lang.String getClientKey();
    
    /**
     * Gets (as xml) the "ClientKey" element
     */
    com.idanalytics.products.idscore.request.ClientKeyDocument.ClientKey xgetClientKey();
    
    /**
     * Sets the "ClientKey" element
     */
    void setClientKey(java.lang.String clientKey);
    
    /**
     * Sets (as xml) the "ClientKey" element
     */
    void xsetClientKey(com.idanalytics.products.idscore.request.ClientKeyDocument.ClientKey clientKey);
    
    /**
     * Gets the "OrderAmount" element
     */
    java.math.BigDecimal getOrderAmount();
    
    /**
     * Gets (as xml) the "OrderAmount" element
     */
    com.idanalytics.products.idscore.request.OrderAmountDocument.OrderAmount xgetOrderAmount();
    
    /**
     * Sets the "OrderAmount" element
     */
    void setOrderAmount(java.math.BigDecimal orderAmount);
    
    /**
     * Sets (as xml) the "OrderAmount" element
     */
    void xsetOrderAmount(com.idanalytics.products.idscore.request.OrderAmountDocument.OrderAmount orderAmount);
    
    /**
     * Gets the "IPAddress" element
     */
    java.lang.String getIPAddress();
    
    /**
     * Gets (as xml) the "IPAddress" element
     */
    com.idanalytics.products.idscore.request.IPAddressDocument.IPAddress xgetIPAddress();
    
    /**
     * True if has "IPAddress" element
     */
    boolean isSetIPAddress();
    
    /**
     * Sets the "IPAddress" element
     */
    void setIPAddress(java.lang.String ipAddress);
    
    /**
     * Sets (as xml) the "IPAddress" element
     */
    void xsetIPAddress(com.idanalytics.products.idscore.request.IPAddressDocument.IPAddress ipAddress);
    
    /**
     * Unsets the "IPAddress" element
     */
    void unsetIPAddress();
    
    /**
     * Gets the "MerchantID" element
     */
    java.lang.String getMerchantID();
    
    /**
     * Gets (as xml) the "MerchantID" element
     */
    com.idanalytics.products.idscore.request.MerchantIDDocument.MerchantID xgetMerchantID();
    
    /**
     * Sets the "MerchantID" element
     */
    void setMerchantID(java.lang.String merchantID);
    
    /**
     * Sets (as xml) the "MerchantID" element
     */
    void xsetMerchantID(com.idanalytics.products.idscore.request.MerchantIDDocument.MerchantID merchantID);
    
    /**
     * Gets the "PassThru1" element
     */
    java.lang.String getPassThru1();
    
    /**
     * Gets (as xml) the "PassThru1" element
     */
    com.idanalytics.products.common_v1.PassThru xgetPassThru1();
    
    /**
     * True if has "PassThru1" element
     */
    boolean isSetPassThru1();
    
    /**
     * Sets the "PassThru1" element
     */
    void setPassThru1(java.lang.String passThru1);
    
    /**
     * Sets (as xml) the "PassThru1" element
     */
    void xsetPassThru1(com.idanalytics.products.common_v1.PassThru passThru1);
    
    /**
     * Unsets the "PassThru1" element
     */
    void unsetPassThru1();
    
    /**
     * Gets the "PassThru2" element
     */
    java.lang.String getPassThru2();
    
    /**
     * Gets (as xml) the "PassThru2" element
     */
    com.idanalytics.products.common_v1.PassThru xgetPassThru2();
    
    /**
     * True if has "PassThru2" element
     */
    boolean isSetPassThru2();
    
    /**
     * Sets the "PassThru2" element
     */
    void setPassThru2(java.lang.String passThru2);
    
    /**
     * Sets (as xml) the "PassThru2" element
     */
    void xsetPassThru2(com.idanalytics.products.common_v1.PassThru passThru2);
    
    /**
     * Unsets the "PassThru2" element
     */
    void unsetPassThru2();
    
    /**
     * Gets the "PassThru3" element
     */
    java.lang.String getPassThru3();
    
    /**
     * Gets (as xml) the "PassThru3" element
     */
    com.idanalytics.products.common_v1.PassThru xgetPassThru3();
    
    /**
     * True if has "PassThru3" element
     */
    boolean isSetPassThru3();
    
    /**
     * Sets the "PassThru3" element
     */
    void setPassThru3(java.lang.String passThru3);
    
    /**
     * Sets (as xml) the "PassThru3" element
     */
    void xsetPassThru3(com.idanalytics.products.common_v1.PassThru passThru3);
    
    /**
     * Unsets the "PassThru3" element
     */
    void unsetPassThru3();
    
    /**
     * Gets the "PassThru4" element
     */
    java.lang.String getPassThru4();
    
    /**
     * Gets (as xml) the "PassThru4" element
     */
    com.idanalytics.products.common_v1.PassThru xgetPassThru4();
    
    /**
     * True if has "PassThru4" element
     */
    boolean isSetPassThru4();
    
    /**
     * Sets the "PassThru4" element
     */
    void setPassThru4(java.lang.String passThru4);
    
    /**
     * Sets (as xml) the "PassThru4" element
     */
    void xsetPassThru4(com.idanalytics.products.common_v1.PassThru passThru4);
    
    /**
     * Unsets the "PassThru4" element
     */
    void unsetPassThru4();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.idscore.request.OrderHeaderType newInstance() {
          return (com.idanalytics.products.idscore.request.OrderHeaderType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.idscore.request.OrderHeaderType newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.idscore.request.OrderHeaderType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.idscore.request.OrderHeaderType parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.OrderHeaderType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.idscore.request.OrderHeaderType parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.OrderHeaderType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.idscore.request.OrderHeaderType parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.OrderHeaderType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.idscore.request.OrderHeaderType parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.OrderHeaderType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.idscore.request.OrderHeaderType parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.OrderHeaderType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.idscore.request.OrderHeaderType parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.OrderHeaderType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.idscore.request.OrderHeaderType parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.OrderHeaderType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.idscore.request.OrderHeaderType parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.OrderHeaderType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.idscore.request.OrderHeaderType parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.OrderHeaderType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.idscore.request.OrderHeaderType parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.OrderHeaderType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.idscore.request.OrderHeaderType parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.OrderHeaderType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.idscore.request.OrderHeaderType parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.OrderHeaderType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.idscore.request.OrderHeaderType parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.OrderHeaderType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.idscore.request.OrderHeaderType parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.OrderHeaderType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.request.OrderHeaderType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.request.OrderHeaderType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.request.OrderHeaderType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.request.OrderHeaderType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
