/*
 * XML Type:  IdentityEvent
 * Namespace: http://idanalytics.com/products/idscore/result
 * Java type: com.idanalytics.products.idscore.result.IdentityEvent
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.result.impl;
/**
 * An XML IdentityEvent(@http://idanalytics.com/products/idscore/result).
 *
 * This is a complex type.
 */
public class IdentityEventImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.result.IdentityEvent
{
    private static final long serialVersionUID = 1L;
    
    public IdentityEventImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName EVENTID$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "EventID");
    private static final javax.xml.namespace.QName CREDITINACTIVE$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "CreditInactive");
    private static final javax.xml.namespace.QName COMPANYNAME$4 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "CompanyName");
    private static final javax.xml.namespace.QName EVENTTYPE$6 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "EventType");
    private static final javax.xml.namespace.QName INDUSTRYTYPE$8 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "IndustryType");
    private static final javax.xml.namespace.QName DATE$10 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "Date");
    private static final javax.xml.namespace.QName SSNMATCHINDICATOR$12 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "SSNMatchIndicator");
    private static final javax.xml.namespace.QName NAMEMATCHINDICATOR$14 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "NameMatchIndicator");
    private static final javax.xml.namespace.QName DOBMATCHINDICATOR$16 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "DOBMatchIndicator");
    private static final javax.xml.namespace.QName PARTIALADDRESS$18 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "PartialAddress");
    private static final javax.xml.namespace.QName PARTIALHOMEPHONE$20 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "PartialHomePhone");
    private static final javax.xml.namespace.QName REPORTEDFRAUD$22 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "ReportedFraud");
    
    
    /**
     * Gets the "EventID" element
     */
    public java.lang.String getEventID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EVENTID$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "EventID" element
     */
    public com.idanalytics.products.idscore.result.String30 xgetEventID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.String30 target = null;
            target = (com.idanalytics.products.idscore.result.String30)get_store().find_element_user(EVENTID$0, 0);
            return target;
        }
    }
    
    /**
     * True if has "EventID" element
     */
    public boolean isSetEventID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(EVENTID$0) != 0;
        }
    }
    
    /**
     * Sets the "EventID" element
     */
    public void setEventID(java.lang.String eventID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EVENTID$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(EVENTID$0);
            }
            target.setStringValue(eventID);
        }
    }
    
    /**
     * Sets (as xml) the "EventID" element
     */
    public void xsetEventID(com.idanalytics.products.idscore.result.String30 eventID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.String30 target = null;
            target = (com.idanalytics.products.idscore.result.String30)get_store().find_element_user(EVENTID$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.result.String30)get_store().add_element_user(EVENTID$0);
            }
            target.set(eventID);
        }
    }
    
    /**
     * Unsets the "EventID" element
     */
    public void unsetEventID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(EVENTID$0, 0);
        }
    }
    
    /**
     * Gets the "CreditInactive" element
     */
    public boolean getCreditInactive()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CREDITINACTIVE$2, 0);
            if (target == null)
            {
                return false;
            }
            return target.getBooleanValue();
        }
    }
    
    /**
     * Gets (as xml) the "CreditInactive" element
     */
    public org.apache.xmlbeans.XmlBoolean xgetCreditInactive()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(CREDITINACTIVE$2, 0);
            return target;
        }
    }
    
    /**
     * True if has "CreditInactive" element
     */
    public boolean isSetCreditInactive()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CREDITINACTIVE$2) != 0;
        }
    }
    
    /**
     * Sets the "CreditInactive" element
     */
    public void setCreditInactive(boolean creditInactive)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CREDITINACTIVE$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CREDITINACTIVE$2);
            }
            target.setBooleanValue(creditInactive);
        }
    }
    
    /**
     * Sets (as xml) the "CreditInactive" element
     */
    public void xsetCreditInactive(org.apache.xmlbeans.XmlBoolean creditInactive)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(CREDITINACTIVE$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlBoolean)get_store().add_element_user(CREDITINACTIVE$2);
            }
            target.set(creditInactive);
        }
    }
    
    /**
     * Unsets the "CreditInactive" element
     */
    public void unsetCreditInactive()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CREDITINACTIVE$2, 0);
        }
    }
    
    /**
     * Gets the "CompanyName" element
     */
    public java.lang.String getCompanyName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(COMPANYNAME$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "CompanyName" element
     */
    public com.idanalytics.products.idscore.result.String50 xgetCompanyName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.String50 target = null;
            target = (com.idanalytics.products.idscore.result.String50)get_store().find_element_user(COMPANYNAME$4, 0);
            return target;
        }
    }
    
    /**
     * Sets the "CompanyName" element
     */
    public void setCompanyName(java.lang.String companyName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(COMPANYNAME$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(COMPANYNAME$4);
            }
            target.setStringValue(companyName);
        }
    }
    
    /**
     * Sets (as xml) the "CompanyName" element
     */
    public void xsetCompanyName(com.idanalytics.products.idscore.result.String50 companyName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.String50 target = null;
            target = (com.idanalytics.products.idscore.result.String50)get_store().find_element_user(COMPANYNAME$4, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.result.String50)get_store().add_element_user(COMPANYNAME$4);
            }
            target.set(companyName);
        }
    }
    
    /**
     * Gets the "EventType" element
     */
    public java.lang.String getEventType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EVENTTYPE$6, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "EventType" element
     */
    public com.idanalytics.products.idscore.result.String50 xgetEventType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.String50 target = null;
            target = (com.idanalytics.products.idscore.result.String50)get_store().find_element_user(EVENTTYPE$6, 0);
            return target;
        }
    }
    
    /**
     * True if has "EventType" element
     */
    public boolean isSetEventType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(EVENTTYPE$6) != 0;
        }
    }
    
    /**
     * Sets the "EventType" element
     */
    public void setEventType(java.lang.String eventType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EVENTTYPE$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(EVENTTYPE$6);
            }
            target.setStringValue(eventType);
        }
    }
    
    /**
     * Sets (as xml) the "EventType" element
     */
    public void xsetEventType(com.idanalytics.products.idscore.result.String50 eventType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.String50 target = null;
            target = (com.idanalytics.products.idscore.result.String50)get_store().find_element_user(EVENTTYPE$6, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.result.String50)get_store().add_element_user(EVENTTYPE$6);
            }
            target.set(eventType);
        }
    }
    
    /**
     * Unsets the "EventType" element
     */
    public void unsetEventType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(EVENTTYPE$6, 0);
        }
    }
    
    /**
     * Gets the "IndustryType" element
     */
    public java.lang.String getIndustryType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(INDUSTRYTYPE$8, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IndustryType" element
     */
    public com.idanalytics.products.idscore.result.String50 xgetIndustryType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.String50 target = null;
            target = (com.idanalytics.products.idscore.result.String50)get_store().find_element_user(INDUSTRYTYPE$8, 0);
            return target;
        }
    }
    
    /**
     * True if has "IndustryType" element
     */
    public boolean isSetIndustryType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(INDUSTRYTYPE$8) != 0;
        }
    }
    
    /**
     * Sets the "IndustryType" element
     */
    public void setIndustryType(java.lang.String industryType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(INDUSTRYTYPE$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(INDUSTRYTYPE$8);
            }
            target.setStringValue(industryType);
        }
    }
    
    /**
     * Sets (as xml) the "IndustryType" element
     */
    public void xsetIndustryType(com.idanalytics.products.idscore.result.String50 industryType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.String50 target = null;
            target = (com.idanalytics.products.idscore.result.String50)get_store().find_element_user(INDUSTRYTYPE$8, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.result.String50)get_store().add_element_user(INDUSTRYTYPE$8);
            }
            target.set(industryType);
        }
    }
    
    /**
     * Unsets the "IndustryType" element
     */
    public void unsetIndustryType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(INDUSTRYTYPE$8, 0);
        }
    }
    
    /**
     * Gets the "Date" element
     */
    public java.util.Calendar getDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DATE$10, 0);
            if (target == null)
            {
                return null;
            }
            return target.getCalendarValue();
        }
    }
    
    /**
     * Gets (as xml) the "Date" element
     */
    public org.apache.xmlbeans.XmlDate xgetDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlDate target = null;
            target = (org.apache.xmlbeans.XmlDate)get_store().find_element_user(DATE$10, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Date" element
     */
    public void setDate(java.util.Calendar date)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DATE$10, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DATE$10);
            }
            target.setCalendarValue(date);
        }
    }
    
    /**
     * Sets (as xml) the "Date" element
     */
    public void xsetDate(org.apache.xmlbeans.XmlDate date)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlDate target = null;
            target = (org.apache.xmlbeans.XmlDate)get_store().find_element_user(DATE$10, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlDate)get_store().add_element_user(DATE$10);
            }
            target.set(date);
        }
    }
    
    /**
     * Gets the "SSNMatchIndicator" element
     */
    public com.idanalytics.products.idscore.result.MatchIndicator.Enum getSSNMatchIndicator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SSNMATCHINDICATOR$12, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.idscore.result.MatchIndicator.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "SSNMatchIndicator" element
     */
    public com.idanalytics.products.idscore.result.MatchIndicator xgetSSNMatchIndicator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.MatchIndicator target = null;
            target = (com.idanalytics.products.idscore.result.MatchIndicator)get_store().find_element_user(SSNMATCHINDICATOR$12, 0);
            return target;
        }
    }
    
    /**
     * Sets the "SSNMatchIndicator" element
     */
    public void setSSNMatchIndicator(com.idanalytics.products.idscore.result.MatchIndicator.Enum ssnMatchIndicator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SSNMATCHINDICATOR$12, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SSNMATCHINDICATOR$12);
            }
            target.setEnumValue(ssnMatchIndicator);
        }
    }
    
    /**
     * Sets (as xml) the "SSNMatchIndicator" element
     */
    public void xsetSSNMatchIndicator(com.idanalytics.products.idscore.result.MatchIndicator ssnMatchIndicator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.MatchIndicator target = null;
            target = (com.idanalytics.products.idscore.result.MatchIndicator)get_store().find_element_user(SSNMATCHINDICATOR$12, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.result.MatchIndicator)get_store().add_element_user(SSNMATCHINDICATOR$12);
            }
            target.set(ssnMatchIndicator);
        }
    }
    
    /**
     * Gets the "NameMatchIndicator" element
     */
    public com.idanalytics.products.idscore.result.MatchIndicator.Enum getNameMatchIndicator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NAMEMATCHINDICATOR$14, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.idscore.result.MatchIndicator.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "NameMatchIndicator" element
     */
    public com.idanalytics.products.idscore.result.MatchIndicator xgetNameMatchIndicator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.MatchIndicator target = null;
            target = (com.idanalytics.products.idscore.result.MatchIndicator)get_store().find_element_user(NAMEMATCHINDICATOR$14, 0);
            return target;
        }
    }
    
    /**
     * Sets the "NameMatchIndicator" element
     */
    public void setNameMatchIndicator(com.idanalytics.products.idscore.result.MatchIndicator.Enum nameMatchIndicator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NAMEMATCHINDICATOR$14, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(NAMEMATCHINDICATOR$14);
            }
            target.setEnumValue(nameMatchIndicator);
        }
    }
    
    /**
     * Sets (as xml) the "NameMatchIndicator" element
     */
    public void xsetNameMatchIndicator(com.idanalytics.products.idscore.result.MatchIndicator nameMatchIndicator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.MatchIndicator target = null;
            target = (com.idanalytics.products.idscore.result.MatchIndicator)get_store().find_element_user(NAMEMATCHINDICATOR$14, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.result.MatchIndicator)get_store().add_element_user(NAMEMATCHINDICATOR$14);
            }
            target.set(nameMatchIndicator);
        }
    }
    
    /**
     * Gets the "DOBMatchIndicator" element
     */
    public com.idanalytics.products.idscore.result.MatchIndicator.Enum getDOBMatchIndicator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DOBMATCHINDICATOR$16, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.idscore.result.MatchIndicator.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "DOBMatchIndicator" element
     */
    public com.idanalytics.products.idscore.result.MatchIndicator xgetDOBMatchIndicator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.MatchIndicator target = null;
            target = (com.idanalytics.products.idscore.result.MatchIndicator)get_store().find_element_user(DOBMATCHINDICATOR$16, 0);
            return target;
        }
    }
    
    /**
     * Sets the "DOBMatchIndicator" element
     */
    public void setDOBMatchIndicator(com.idanalytics.products.idscore.result.MatchIndicator.Enum dobMatchIndicator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DOBMATCHINDICATOR$16, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DOBMATCHINDICATOR$16);
            }
            target.setEnumValue(dobMatchIndicator);
        }
    }
    
    /**
     * Sets (as xml) the "DOBMatchIndicator" element
     */
    public void xsetDOBMatchIndicator(com.idanalytics.products.idscore.result.MatchIndicator dobMatchIndicator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.MatchIndicator target = null;
            target = (com.idanalytics.products.idscore.result.MatchIndicator)get_store().find_element_user(DOBMATCHINDICATOR$16, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.result.MatchIndicator)get_store().add_element_user(DOBMATCHINDICATOR$16);
            }
            target.set(dobMatchIndicator);
        }
    }
    
    /**
     * Gets the "PartialAddress" element
     */
    public com.idanalytics.products.idscore.result.IdentityEvent.PartialAddress getPartialAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.IdentityEvent.PartialAddress target = null;
            target = (com.idanalytics.products.idscore.result.IdentityEvent.PartialAddress)get_store().find_element_user(PARTIALADDRESS$18, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "PartialAddress" element
     */
    public void setPartialAddress(com.idanalytics.products.idscore.result.IdentityEvent.PartialAddress partialAddress)
    {
        generatedSetterHelperImpl(partialAddress, PARTIALADDRESS$18, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "PartialAddress" element
     */
    public com.idanalytics.products.idscore.result.IdentityEvent.PartialAddress addNewPartialAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.IdentityEvent.PartialAddress target = null;
            target = (com.idanalytics.products.idscore.result.IdentityEvent.PartialAddress)get_store().add_element_user(PARTIALADDRESS$18);
            return target;
        }
    }
    
    /**
     * Gets the "PartialHomePhone" element
     */
    public com.idanalytics.products.idscore.result.IdentityEvent.PartialHomePhone getPartialHomePhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.IdentityEvent.PartialHomePhone target = null;
            target = (com.idanalytics.products.idscore.result.IdentityEvent.PartialHomePhone)get_store().find_element_user(PARTIALHOMEPHONE$20, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "PartialHomePhone" element
     */
    public void setPartialHomePhone(com.idanalytics.products.idscore.result.IdentityEvent.PartialHomePhone partialHomePhone)
    {
        generatedSetterHelperImpl(partialHomePhone, PARTIALHOMEPHONE$20, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "PartialHomePhone" element
     */
    public com.idanalytics.products.idscore.result.IdentityEvent.PartialHomePhone addNewPartialHomePhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.IdentityEvent.PartialHomePhone target = null;
            target = (com.idanalytics.products.idscore.result.IdentityEvent.PartialHomePhone)get_store().add_element_user(PARTIALHOMEPHONE$20);
            return target;
        }
    }
    
    /**
     * Gets the "ReportedFraud" element
     */
    public com.idanalytics.products.idscore.result.MatchIndicator.Enum getReportedFraud()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(REPORTEDFRAUD$22, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.idscore.result.MatchIndicator.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "ReportedFraud" element
     */
    public com.idanalytics.products.idscore.result.MatchIndicator xgetReportedFraud()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.MatchIndicator target = null;
            target = (com.idanalytics.products.idscore.result.MatchIndicator)get_store().find_element_user(REPORTEDFRAUD$22, 0);
            return target;
        }
    }
    
    /**
     * Sets the "ReportedFraud" element
     */
    public void setReportedFraud(com.idanalytics.products.idscore.result.MatchIndicator.Enum reportedFraud)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(REPORTEDFRAUD$22, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(REPORTEDFRAUD$22);
            }
            target.setEnumValue(reportedFraud);
        }
    }
    
    /**
     * Sets (as xml) the "ReportedFraud" element
     */
    public void xsetReportedFraud(com.idanalytics.products.idscore.result.MatchIndicator reportedFraud)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.MatchIndicator target = null;
            target = (com.idanalytics.products.idscore.result.MatchIndicator)get_store().find_element_user(REPORTEDFRAUD$22, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.result.MatchIndicator)get_store().add_element_user(REPORTEDFRAUD$22);
            }
            target.set(reportedFraud);
        }
    }
    /**
     * An XML PartialAddress(@http://idanalytics.com/products/idscore/result).
     *
     * This is a complex type.
     */
    public static class PartialAddressImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.result.IdentityEvent.PartialAddress
    {
        private static final long serialVersionUID = 1L;
        
        public PartialAddressImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName ADDRESS$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "Address");
        private static final javax.xml.namespace.QName CITY$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "City");
        private static final javax.xml.namespace.QName ZIP$4 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "Zip");
        
        
        /**
         * Gets the "Address" element
         */
        public java.lang.String getAddress()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ADDRESS$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "Address" element
         */
        public com.idanalytics.products.idscore.result.String50 xgetAddress()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.String50 target = null;
                target = (com.idanalytics.products.idscore.result.String50)get_store().find_element_user(ADDRESS$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "Address" element
         */
        public void setAddress(java.lang.String address)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ADDRESS$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ADDRESS$0);
                }
                target.setStringValue(address);
            }
        }
        
        /**
         * Sets (as xml) the "Address" element
         */
        public void xsetAddress(com.idanalytics.products.idscore.result.String50 address)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.String50 target = null;
                target = (com.idanalytics.products.idscore.result.String50)get_store().find_element_user(ADDRESS$0, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result.String50)get_store().add_element_user(ADDRESS$0);
                }
                target.set(address);
            }
        }
        
        /**
         * Gets the "City" element
         */
        public java.lang.String getCity()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CITY$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "City" element
         */
        public com.idanalytics.products.idscore.result.String50 xgetCity()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.String50 target = null;
                target = (com.idanalytics.products.idscore.result.String50)get_store().find_element_user(CITY$2, 0);
                return target;
            }
        }
        
        /**
         * Sets the "City" element
         */
        public void setCity(java.lang.String city)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CITY$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CITY$2);
                }
                target.setStringValue(city);
            }
        }
        
        /**
         * Sets (as xml) the "City" element
         */
        public void xsetCity(com.idanalytics.products.idscore.result.String50 city)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.String50 target = null;
                target = (com.idanalytics.products.idscore.result.String50)get_store().find_element_user(CITY$2, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result.String50)get_store().add_element_user(CITY$2);
                }
                target.set(city);
            }
        }
        
        /**
         * Gets the "Zip" element
         */
        public java.lang.String getZip()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ZIP$4, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "Zip" element
         */
        public com.idanalytics.products.idscore.result.String5Z xgetZip()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.String5Z target = null;
                target = (com.idanalytics.products.idscore.result.String5Z)get_store().find_element_user(ZIP$4, 0);
                return target;
            }
        }
        
        /**
         * Sets the "Zip" element
         */
        public void setZip(java.lang.String zip)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ZIP$4, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ZIP$4);
                }
                target.setStringValue(zip);
            }
        }
        
        /**
         * Sets (as xml) the "Zip" element
         */
        public void xsetZip(com.idanalytics.products.idscore.result.String5Z zip)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.String5Z target = null;
                target = (com.idanalytics.products.idscore.result.String5Z)get_store().find_element_user(ZIP$4, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result.String5Z)get_store().add_element_user(ZIP$4);
                }
                target.set(zip);
            }
        }
    }
    /**
     * An XML PartialHomePhone(@http://idanalytics.com/products/idscore/result).
     *
     * This is a complex type.
     */
    public static class PartialHomePhoneImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.result.IdentityEvent.PartialHomePhone
    {
        private static final long serialVersionUID = 1L;
        
        public PartialHomePhoneImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName NPA$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "NPA");
        private static final javax.xml.namespace.QName NXX$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "NXX");
        
        
        /**
         * Gets the "NPA" element
         */
        public java.lang.String getNPA()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NPA$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "NPA" element
         */
        public com.idanalytics.products.idscore.result.String3Z xgetNPA()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.String3Z target = null;
                target = (com.idanalytics.products.idscore.result.String3Z)get_store().find_element_user(NPA$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "NPA" element
         */
        public void setNPA(java.lang.String npa)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NPA$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(NPA$0);
                }
                target.setStringValue(npa);
            }
        }
        
        /**
         * Sets (as xml) the "NPA" element
         */
        public void xsetNPA(com.idanalytics.products.idscore.result.String3Z npa)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.String3Z target = null;
                target = (com.idanalytics.products.idscore.result.String3Z)get_store().find_element_user(NPA$0, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result.String3Z)get_store().add_element_user(NPA$0);
                }
                target.set(npa);
            }
        }
        
        /**
         * Gets the "NXX" element
         */
        public java.lang.String getNXX()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NXX$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "NXX" element
         */
        public com.idanalytics.products.idscore.result.String3Z xgetNXX()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.String3Z target = null;
                target = (com.idanalytics.products.idscore.result.String3Z)get_store().find_element_user(NXX$2, 0);
                return target;
            }
        }
        
        /**
         * Sets the "NXX" element
         */
        public void setNXX(java.lang.String nxx)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(NXX$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(NXX$2);
                }
                target.setStringValue(nxx);
            }
        }
        
        /**
         * Sets (as xml) the "NXX" element
         */
        public void xsetNXX(com.idanalytics.products.idscore.result.String3Z nxx)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.String3Z target = null;
                target = (com.idanalytics.products.idscore.result.String3Z)get_store().find_element_user(NXX$2, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result.String3Z)get_store().add_element_user(NXX$2);
                }
                target.set(nxx);
            }
        }
    }
}
