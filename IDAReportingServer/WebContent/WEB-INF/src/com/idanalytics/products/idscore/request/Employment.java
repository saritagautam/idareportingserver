/*
 * XML Type:  Employment
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.Employment
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request;


/**
 * An XML Employment(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public interface Employment extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Employment.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("employmentec44type");
    
    /**
     * Gets the "EmployerType" element
     */
    java.lang.String getEmployerType();
    
    /**
     * Gets (as xml) the "EmployerType" element
     */
    org.apache.xmlbeans.XmlString xgetEmployerType();
    
    /**
     * True if has "EmployerType" element
     */
    boolean isSetEmployerType();
    
    /**
     * Sets the "EmployerType" element
     */
    void setEmployerType(java.lang.String employerType);
    
    /**
     * Sets (as xml) the "EmployerType" element
     */
    void xsetEmployerType(org.apache.xmlbeans.XmlString employerType);
    
    /**
     * Unsets the "EmployerType" element
     */
    void unsetEmployerType();
    
    /**
     * Gets the "EmploymentType" element
     */
    java.lang.String getEmploymentType();
    
    /**
     * Gets (as xml) the "EmploymentType" element
     */
    com.idanalytics.products.idscore.request.EmploymentType xgetEmploymentType();
    
    /**
     * True if has "EmploymentType" element
     */
    boolean isSetEmploymentType();
    
    /**
     * Sets the "EmploymentType" element
     */
    void setEmploymentType(java.lang.String employmentType);
    
    /**
     * Sets (as xml) the "EmploymentType" element
     */
    void xsetEmploymentType(com.idanalytics.products.idscore.request.EmploymentType employmentType);
    
    /**
     * Unsets the "EmploymentType" element
     */
    void unsetEmploymentType();
    
    /**
     * Gets the "Name" element
     */
    java.lang.String getName();
    
    /**
     * Gets (as xml) the "Name" element
     */
    com.idanalytics.products.idscore.request.EmployerName xgetName();
    
    /**
     * True if has "Name" element
     */
    boolean isSetName();
    
    /**
     * Sets the "Name" element
     */
    void setName(java.lang.String name);
    
    /**
     * Sets (as xml) the "Name" element
     */
    void xsetName(com.idanalytics.products.idscore.request.EmployerName name);
    
    /**
     * Unsets the "Name" element
     */
    void unsetName();
    
    /**
     * Gets the "Address" element
     */
    com.idanalytics.products.idscore.request.Address getAddress();
    
    /**
     * True if has "Address" element
     */
    boolean isSetAddress();
    
    /**
     * Sets the "Address" element
     */
    void setAddress(com.idanalytics.products.idscore.request.Address address);
    
    /**
     * Appends and returns a new empty "Address" element
     */
    com.idanalytics.products.idscore.request.Address addNewAddress();
    
    /**
     * Unsets the "Address" element
     */
    void unsetAddress();
    
    /**
     * Gets the "ParsedAddress" element
     */
    com.idanalytics.products.idscore.request.ParsedAddress getParsedAddress();
    
    /**
     * True if has "ParsedAddress" element
     */
    boolean isSetParsedAddress();
    
    /**
     * Sets the "ParsedAddress" element
     */
    void setParsedAddress(com.idanalytics.products.idscore.request.ParsedAddress parsedAddress);
    
    /**
     * Appends and returns a new empty "ParsedAddress" element
     */
    com.idanalytics.products.idscore.request.ParsedAddress addNewParsedAddress();
    
    /**
     * Unsets the "ParsedAddress" element
     */
    void unsetParsedAddress();
    
    /**
     * Gets the "City" element
     */
    java.lang.String getCity();
    
    /**
     * Gets (as xml) the "City" element
     */
    com.idanalytics.products.idscore.request.City xgetCity();
    
    /**
     * True if has "City" element
     */
    boolean isSetCity();
    
    /**
     * Sets the "City" element
     */
    void setCity(java.lang.String city);
    
    /**
     * Sets (as xml) the "City" element
     */
    void xsetCity(com.idanalytics.products.idscore.request.City city);
    
    /**
     * Unsets the "City" element
     */
    void unsetCity();
    
    /**
     * Gets the "State" element
     */
    java.lang.String getState();
    
    /**
     * Gets (as xml) the "State" element
     */
    com.idanalytics.products.idscore.request.State xgetState();
    
    /**
     * True if has "State" element
     */
    boolean isSetState();
    
    /**
     * Sets the "State" element
     */
    void setState(java.lang.String state);
    
    /**
     * Sets (as xml) the "State" element
     */
    void xsetState(com.idanalytics.products.idscore.request.State state);
    
    /**
     * Unsets the "State" element
     */
    void unsetState();
    
    /**
     * Gets the "Zip" element
     */
    java.lang.String getZip();
    
    /**
     * Gets (as xml) the "Zip" element
     */
    com.idanalytics.products.idscore.request.Zip xgetZip();
    
    /**
     * True if has "Zip" element
     */
    boolean isSetZip();
    
    /**
     * Sets the "Zip" element
     */
    void setZip(java.lang.String zip);
    
    /**
     * Sets (as xml) the "Zip" element
     */
    void xsetZip(com.idanalytics.products.idscore.request.Zip zip);
    
    /**
     * Unsets the "Zip" element
     */
    void unsetZip();
    
    /**
     * Gets the "Country" element
     */
    java.lang.String getCountry();
    
    /**
     * Gets (as xml) the "Country" element
     */
    com.idanalytics.products.idscore.request.Country xgetCountry();
    
    /**
     * True if has "Country" element
     */
    boolean isSetCountry();
    
    /**
     * Sets the "Country" element
     */
    void setCountry(java.lang.String country);
    
    /**
     * Sets (as xml) the "Country" element
     */
    void xsetCountry(com.idanalytics.products.idscore.request.Country country);
    
    /**
     * Unsets the "Country" element
     */
    void unsetCountry();
    
    /**
     * Gets the "Phone" element
     */
    java.lang.String getPhone();
    
    /**
     * Gets (as xml) the "Phone" element
     */
    com.idanalytics.products.idscore.request.Phone xgetPhone();
    
    /**
     * True if has "Phone" element
     */
    boolean isSetPhone();
    
    /**
     * Sets the "Phone" element
     */
    void setPhone(java.lang.String phone);
    
    /**
     * Sets (as xml) the "Phone" element
     */
    void xsetPhone(com.idanalytics.products.idscore.request.Phone phone);
    
    /**
     * Unsets the "Phone" element
     */
    void unsetPhone();
    
    /**
     * Gets the "TimeAtEmployer" element
     */
    java.lang.String getTimeAtEmployer();
    
    /**
     * Gets (as xml) the "TimeAtEmployer" element
     */
    com.idanalytics.products.idscore.request.TimeAt xgetTimeAtEmployer();
    
    /**
     * True if has "TimeAtEmployer" element
     */
    boolean isSetTimeAtEmployer();
    
    /**
     * Sets the "TimeAtEmployer" element
     */
    void setTimeAtEmployer(java.lang.String timeAtEmployer);
    
    /**
     * Sets (as xml) the "TimeAtEmployer" element
     */
    void xsetTimeAtEmployer(com.idanalytics.products.idscore.request.TimeAt timeAtEmployer);
    
    /**
     * Unsets the "TimeAtEmployer" element
     */
    void unsetTimeAtEmployer();
    
    /**
     * Gets the "StartDate" element
     */
    java.lang.Object getStartDate();
    
    /**
     * Gets (as xml) the "StartDate" element
     */
    com.idanalytics.products.idscore.request.Date xgetStartDate();
    
    /**
     * True if has "StartDate" element
     */
    boolean isSetStartDate();
    
    /**
     * Sets the "StartDate" element
     */
    void setStartDate(java.lang.Object startDate);
    
    /**
     * Sets (as xml) the "StartDate" element
     */
    void xsetStartDate(com.idanalytics.products.idscore.request.Date startDate);
    
    /**
     * Unsets the "StartDate" element
     */
    void unsetStartDate();
    
    /**
     * Gets the "Title" element
     */
    java.lang.String getTitle();
    
    /**
     * Gets (as xml) the "Title" element
     */
    com.idanalytics.products.idscore.request.EmploymentTitle xgetTitle();
    
    /**
     * True if has "Title" element
     */
    boolean isSetTitle();
    
    /**
     * Sets the "Title" element
     */
    void setTitle(java.lang.String title);
    
    /**
     * Sets (as xml) the "Title" element
     */
    void xsetTitle(com.idanalytics.products.idscore.request.EmploymentTitle title);
    
    /**
     * Unsets the "Title" element
     */
    void unsetTitle();
    
    /**
     * Gets the "Salary" element
     */
    java.lang.Object getSalary();
    
    /**
     * Gets (as xml) the "Salary" element
     */
    com.idanalytics.products.idscore.request.Income xgetSalary();
    
    /**
     * True if has "Salary" element
     */
    boolean isSetSalary();
    
    /**
     * Sets the "Salary" element
     */
    void setSalary(java.lang.Object salary);
    
    /**
     * Sets (as xml) the "Salary" element
     */
    void xsetSalary(com.idanalytics.products.idscore.request.Income salary);
    
    /**
     * Unsets the "Salary" element
     */
    void unsetSalary();
    
    /**
     * Gets the "SalaryPeriodicity" element
     */
    com.idanalytics.products.idscore.request.Periodicity.Enum getSalaryPeriodicity();
    
    /**
     * Gets (as xml) the "SalaryPeriodicity" element
     */
    com.idanalytics.products.idscore.request.Periodicity xgetSalaryPeriodicity();
    
    /**
     * True if has "SalaryPeriodicity" element
     */
    boolean isSetSalaryPeriodicity();
    
    /**
     * Sets the "SalaryPeriodicity" element
     */
    void setSalaryPeriodicity(com.idanalytics.products.idscore.request.Periodicity.Enum salaryPeriodicity);
    
    /**
     * Sets (as xml) the "SalaryPeriodicity" element
     */
    void xsetSalaryPeriodicity(com.idanalytics.products.idscore.request.Periodicity salaryPeriodicity);
    
    /**
     * Unsets the "SalaryPeriodicity" element
     */
    void unsetSalaryPeriodicity();
    
    /**
     * Gets the "PrevEmployerType" element
     */
    java.lang.String getPrevEmployerType();
    
    /**
     * Gets (as xml) the "PrevEmployerType" element
     */
    com.idanalytics.products.idscore.request.EmployerType xgetPrevEmployerType();
    
    /**
     * True if has "PrevEmployerType" element
     */
    boolean isSetPrevEmployerType();
    
    /**
     * Sets the "PrevEmployerType" element
     */
    void setPrevEmployerType(java.lang.String prevEmployerType);
    
    /**
     * Sets (as xml) the "PrevEmployerType" element
     */
    void xsetPrevEmployerType(com.idanalytics.products.idscore.request.EmployerType prevEmployerType);
    
    /**
     * Unsets the "PrevEmployerType" element
     */
    void unsetPrevEmployerType();
    
    /**
     * Gets the "PrevEmploymentType" element
     */
    java.lang.String getPrevEmploymentType();
    
    /**
     * Gets (as xml) the "PrevEmploymentType" element
     */
    com.idanalytics.products.idscore.request.EmploymentType xgetPrevEmploymentType();
    
    /**
     * True if has "PrevEmploymentType" element
     */
    boolean isSetPrevEmploymentType();
    
    /**
     * Sets the "PrevEmploymentType" element
     */
    void setPrevEmploymentType(java.lang.String prevEmploymentType);
    
    /**
     * Sets (as xml) the "PrevEmploymentType" element
     */
    void xsetPrevEmploymentType(com.idanalytics.products.idscore.request.EmploymentType prevEmploymentType);
    
    /**
     * Unsets the "PrevEmploymentType" element
     */
    void unsetPrevEmploymentType();
    
    /**
     * Gets the "PrevName" element
     */
    java.lang.String getPrevName();
    
    /**
     * Gets (as xml) the "PrevName" element
     */
    com.idanalytics.products.idscore.request.EmployerName xgetPrevName();
    
    /**
     * True if has "PrevName" element
     */
    boolean isSetPrevName();
    
    /**
     * Sets the "PrevName" element
     */
    void setPrevName(java.lang.String prevName);
    
    /**
     * Sets (as xml) the "PrevName" element
     */
    void xsetPrevName(com.idanalytics.products.idscore.request.EmployerName prevName);
    
    /**
     * Unsets the "PrevName" element
     */
    void unsetPrevName();
    
    /**
     * Gets the "PrevAddress" element
     */
    com.idanalytics.products.idscore.request.Address getPrevAddress();
    
    /**
     * True if has "PrevAddress" element
     */
    boolean isSetPrevAddress();
    
    /**
     * Sets the "PrevAddress" element
     */
    void setPrevAddress(com.idanalytics.products.idscore.request.Address prevAddress);
    
    /**
     * Appends and returns a new empty "PrevAddress" element
     */
    com.idanalytics.products.idscore.request.Address addNewPrevAddress();
    
    /**
     * Unsets the "PrevAddress" element
     */
    void unsetPrevAddress();
    
    /**
     * Gets the "ParsedPrevAddress" element
     */
    com.idanalytics.products.idscore.request.ParsedAddress getParsedPrevAddress();
    
    /**
     * True if has "ParsedPrevAddress" element
     */
    boolean isSetParsedPrevAddress();
    
    /**
     * Sets the "ParsedPrevAddress" element
     */
    void setParsedPrevAddress(com.idanalytics.products.idscore.request.ParsedAddress parsedPrevAddress);
    
    /**
     * Appends and returns a new empty "ParsedPrevAddress" element
     */
    com.idanalytics.products.idscore.request.ParsedAddress addNewParsedPrevAddress();
    
    /**
     * Unsets the "ParsedPrevAddress" element
     */
    void unsetParsedPrevAddress();
    
    /**
     * Gets the "PrevCity" element
     */
    java.lang.String getPrevCity();
    
    /**
     * Gets (as xml) the "PrevCity" element
     */
    com.idanalytics.products.idscore.request.City xgetPrevCity();
    
    /**
     * True if has "PrevCity" element
     */
    boolean isSetPrevCity();
    
    /**
     * Sets the "PrevCity" element
     */
    void setPrevCity(java.lang.String prevCity);
    
    /**
     * Sets (as xml) the "PrevCity" element
     */
    void xsetPrevCity(com.idanalytics.products.idscore.request.City prevCity);
    
    /**
     * Unsets the "PrevCity" element
     */
    void unsetPrevCity();
    
    /**
     * Gets the "PrevState" element
     */
    java.lang.String getPrevState();
    
    /**
     * Gets (as xml) the "PrevState" element
     */
    com.idanalytics.products.idscore.request.State xgetPrevState();
    
    /**
     * True if has "PrevState" element
     */
    boolean isSetPrevState();
    
    /**
     * Sets the "PrevState" element
     */
    void setPrevState(java.lang.String prevState);
    
    /**
     * Sets (as xml) the "PrevState" element
     */
    void xsetPrevState(com.idanalytics.products.idscore.request.State prevState);
    
    /**
     * Unsets the "PrevState" element
     */
    void unsetPrevState();
    
    /**
     * Gets the "PrevZip" element
     */
    java.lang.String getPrevZip();
    
    /**
     * Gets (as xml) the "PrevZip" element
     */
    com.idanalytics.products.idscore.request.Zip xgetPrevZip();
    
    /**
     * True if has "PrevZip" element
     */
    boolean isSetPrevZip();
    
    /**
     * Sets the "PrevZip" element
     */
    void setPrevZip(java.lang.String prevZip);
    
    /**
     * Sets (as xml) the "PrevZip" element
     */
    void xsetPrevZip(com.idanalytics.products.idscore.request.Zip prevZip);
    
    /**
     * Unsets the "PrevZip" element
     */
    void unsetPrevZip();
    
    /**
     * Gets the "PrevCountry" element
     */
    java.lang.String getPrevCountry();
    
    /**
     * Gets (as xml) the "PrevCountry" element
     */
    com.idanalytics.products.idscore.request.Country xgetPrevCountry();
    
    /**
     * True if has "PrevCountry" element
     */
    boolean isSetPrevCountry();
    
    /**
     * Sets the "PrevCountry" element
     */
    void setPrevCountry(java.lang.String prevCountry);
    
    /**
     * Sets (as xml) the "PrevCountry" element
     */
    void xsetPrevCountry(com.idanalytics.products.idscore.request.Country prevCountry);
    
    /**
     * Unsets the "PrevCountry" element
     */
    void unsetPrevCountry();
    
    /**
     * Gets the "PrevPhone" element
     */
    java.lang.String getPrevPhone();
    
    /**
     * Gets (as xml) the "PrevPhone" element
     */
    com.idanalytics.products.idscore.request.Phone xgetPrevPhone();
    
    /**
     * True if has "PrevPhone" element
     */
    boolean isSetPrevPhone();
    
    /**
     * Sets the "PrevPhone" element
     */
    void setPrevPhone(java.lang.String prevPhone);
    
    /**
     * Sets (as xml) the "PrevPhone" element
     */
    void xsetPrevPhone(com.idanalytics.products.idscore.request.Phone prevPhone);
    
    /**
     * Unsets the "PrevPhone" element
     */
    void unsetPrevPhone();
    
    /**
     * Gets the "PrevTimeAtEmployer" element
     */
    java.lang.String getPrevTimeAtEmployer();
    
    /**
     * Gets (as xml) the "PrevTimeAtEmployer" element
     */
    com.idanalytics.products.idscore.request.TimeAt xgetPrevTimeAtEmployer();
    
    /**
     * True if has "PrevTimeAtEmployer" element
     */
    boolean isSetPrevTimeAtEmployer();
    
    /**
     * Sets the "PrevTimeAtEmployer" element
     */
    void setPrevTimeAtEmployer(java.lang.String prevTimeAtEmployer);
    
    /**
     * Sets (as xml) the "PrevTimeAtEmployer" element
     */
    void xsetPrevTimeAtEmployer(com.idanalytics.products.idscore.request.TimeAt prevTimeAtEmployer);
    
    /**
     * Unsets the "PrevTimeAtEmployer" element
     */
    void unsetPrevTimeAtEmployer();
    
    /**
     * Gets the "PrevStartDate" element
     */
    java.lang.Object getPrevStartDate();
    
    /**
     * Gets (as xml) the "PrevStartDate" element
     */
    com.idanalytics.products.idscore.request.Date xgetPrevStartDate();
    
    /**
     * True if has "PrevStartDate" element
     */
    boolean isSetPrevStartDate();
    
    /**
     * Sets the "PrevStartDate" element
     */
    void setPrevStartDate(java.lang.Object prevStartDate);
    
    /**
     * Sets (as xml) the "PrevStartDate" element
     */
    void xsetPrevStartDate(com.idanalytics.products.idscore.request.Date prevStartDate);
    
    /**
     * Unsets the "PrevStartDate" element
     */
    void unsetPrevStartDate();
    
    /**
     * Gets the "PrevTitle" element
     */
    java.lang.String getPrevTitle();
    
    /**
     * Gets (as xml) the "PrevTitle" element
     */
    com.idanalytics.products.idscore.request.EmploymentTitle xgetPrevTitle();
    
    /**
     * True if has "PrevTitle" element
     */
    boolean isSetPrevTitle();
    
    /**
     * Sets the "PrevTitle" element
     */
    void setPrevTitle(java.lang.String prevTitle);
    
    /**
     * Sets (as xml) the "PrevTitle" element
     */
    void xsetPrevTitle(com.idanalytics.products.idscore.request.EmploymentTitle prevTitle);
    
    /**
     * Unsets the "PrevTitle" element
     */
    void unsetPrevTitle();
    
    /**
     * Gets the "PrevSalary" element
     */
    java.lang.Object getPrevSalary();
    
    /**
     * Gets (as xml) the "PrevSalary" element
     */
    com.idanalytics.products.idscore.request.Income xgetPrevSalary();
    
    /**
     * True if has "PrevSalary" element
     */
    boolean isSetPrevSalary();
    
    /**
     * Sets the "PrevSalary" element
     */
    void setPrevSalary(java.lang.Object prevSalary);
    
    /**
     * Sets (as xml) the "PrevSalary" element
     */
    void xsetPrevSalary(com.idanalytics.products.idscore.request.Income prevSalary);
    
    /**
     * Unsets the "PrevSalary" element
     */
    void unsetPrevSalary();
    
    /**
     * Gets the "PrevSalaryPeriodicity" element
     */
    com.idanalytics.products.idscore.request.Periodicity.Enum getPrevSalaryPeriodicity();
    
    /**
     * Gets (as xml) the "PrevSalaryPeriodicity" element
     */
    com.idanalytics.products.idscore.request.Periodicity xgetPrevSalaryPeriodicity();
    
    /**
     * True if has "PrevSalaryPeriodicity" element
     */
    boolean isSetPrevSalaryPeriodicity();
    
    /**
     * Sets the "PrevSalaryPeriodicity" element
     */
    void setPrevSalaryPeriodicity(com.idanalytics.products.idscore.request.Periodicity.Enum prevSalaryPeriodicity);
    
    /**
     * Sets (as xml) the "PrevSalaryPeriodicity" element
     */
    void xsetPrevSalaryPeriodicity(com.idanalytics.products.idscore.request.Periodicity prevSalaryPeriodicity);
    
    /**
     * Unsets the "PrevSalaryPeriodicity" element
     */
    void unsetPrevSalaryPeriodicity();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.idscore.request.Employment newInstance() {
          return (com.idanalytics.products.idscore.request.Employment) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.idscore.request.Employment newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.idscore.request.Employment) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.idscore.request.Employment parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.Employment) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.idscore.request.Employment parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.Employment) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.idscore.request.Employment parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.Employment) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.idscore.request.Employment parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.Employment) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.idscore.request.Employment parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.Employment) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.idscore.request.Employment parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.Employment) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.idscore.request.Employment parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.Employment) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.idscore.request.Employment parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.Employment) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.idscore.request.Employment parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.Employment) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.idscore.request.Employment parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.Employment) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.idscore.request.Employment parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.Employment) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.idscore.request.Employment parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.Employment) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.idscore.request.Employment parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.Employment) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.idscore.request.Employment parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.Employment) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.request.Employment parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.request.Employment) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.request.Employment parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.request.Employment) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
