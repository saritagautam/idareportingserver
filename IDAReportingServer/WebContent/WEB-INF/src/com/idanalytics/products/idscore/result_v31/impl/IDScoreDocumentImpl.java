/*
 * An XML document type.
 * Localname: IDScore
 * Namespace: http://idanalytics.com/products/idscore/result.v31
 * Java type: com.idanalytics.products.idscore.result_v31.IDScoreDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.result_v31.impl;
/**
 * A document containing one IDScore(@http://idanalytics.com/products/idscore/result.v31) element.
 *
 * This is a complex type.
 */
public class IDScoreDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.result_v31.IDScoreDocument
{
    private static final long serialVersionUID = 1L;
    
    public IDScoreDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IDSCORE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result.v31", "IDScore");
    
    
    /**
     * Gets the "IDScore" element
     */
    public java.lang.String getIDScore()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDSCORE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IDScore" element
     */
    public com.idanalytics.products.idscore.result_v31.IDScoreDocument.IDScore xgetIDScore()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result_v31.IDScoreDocument.IDScore target = null;
            target = (com.idanalytics.products.idscore.result_v31.IDScoreDocument.IDScore)get_store().find_element_user(IDSCORE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IDScore" element
     */
    public void setIDScore(java.lang.String idScore)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDSCORE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDSCORE$0);
            }
            target.setStringValue(idScore);
        }
    }
    
    /**
     * Sets (as xml) the "IDScore" element
     */
    public void xsetIDScore(com.idanalytics.products.idscore.result_v31.IDScoreDocument.IDScore idScore)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result_v31.IDScoreDocument.IDScore target = null;
            target = (com.idanalytics.products.idscore.result_v31.IDScoreDocument.IDScore)get_store().find_element_user(IDSCORE$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.result_v31.IDScoreDocument.IDScore)get_store().add_element_user(IDSCORE$0);
            }
            target.set(idScore);
        }
    }
    /**
     * An XML IDScore(@http://idanalytics.com/products/idscore/result.v31).
     *
     * This is a union type. Instances are of one of the following types:
     *     com.idanalytics.products.idscore.result_v31.IDScoreDocument$IDScore$Member
     *     com.idanalytics.products.idscore.result_v31.IDScoreDocument$IDScore$Member2
     */
    public static class IDScoreImpl extends org.apache.xmlbeans.impl.values.XmlUnionImpl implements com.idanalytics.products.idscore.result_v31.IDScoreDocument.IDScore, com.idanalytics.products.idscore.result_v31.IDScoreDocument.IDScore.Member, com.idanalytics.products.idscore.result_v31.IDScoreDocument.IDScore.Member2
    {
        private static final long serialVersionUID = 1L;
        
        public IDScoreImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected IDScoreImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
        /**
         * An anonymous inner XML type.
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result_v31.IDScoreDocument$IDScore$Member.
         */
        public static class MemberImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.result_v31.IDScoreDocument.IDScore.Member
        {
            private static final long serialVersionUID = 1L;
            
            public MemberImpl(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected MemberImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
        /**
         * An anonymous inner XML type.
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result_v31.IDScoreDocument$IDScore$Member2.
         */
        public static class MemberImpl2 extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.result_v31.IDScoreDocument.IDScore.Member2
        {
            private static final long serialVersionUID = 1L;
            
            public MemberImpl2(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected MemberImpl2(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
    }
}
