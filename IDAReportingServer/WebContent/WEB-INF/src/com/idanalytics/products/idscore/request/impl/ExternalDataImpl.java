/*
 * XML Type:  ExternalData
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.ExternalData
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML ExternalData(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public class ExternalDataImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.ExternalData
{
    private static final long serialVersionUID = 1L;
    
    public ExternalDataImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName VARIABLEGROUP$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "VariableGroup");
    
    
    /**
     * Gets array of all "VariableGroup" elements
     */
    public com.idanalytics.products.idscore.request.VariableGroup[] getVariableGroupArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(VARIABLEGROUP$0, targetList);
            com.idanalytics.products.idscore.request.VariableGroup[] result = new com.idanalytics.products.idscore.request.VariableGroup[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "VariableGroup" element
     */
    public com.idanalytics.products.idscore.request.VariableGroup getVariableGroupArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.VariableGroup target = null;
            target = (com.idanalytics.products.idscore.request.VariableGroup)get_store().find_element_user(VARIABLEGROUP$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "VariableGroup" element
     */
    public int sizeOfVariableGroupArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(VARIABLEGROUP$0);
        }
    }
    
    /**
     * Sets array of all "VariableGroup" element  WARNING: This method is not atomicaly synchronized.
     */
    public void setVariableGroupArray(com.idanalytics.products.idscore.request.VariableGroup[] variableGroupArray)
    {
        check_orphaned();
        arraySetterHelper(variableGroupArray, VARIABLEGROUP$0);
    }
    
    /**
     * Sets ith "VariableGroup" element
     */
    public void setVariableGroupArray(int i, com.idanalytics.products.idscore.request.VariableGroup variableGroup)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.VariableGroup target = null;
            target = (com.idanalytics.products.idscore.request.VariableGroup)get_store().find_element_user(VARIABLEGROUP$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(variableGroup);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "VariableGroup" element
     */
    public com.idanalytics.products.idscore.request.VariableGroup insertNewVariableGroup(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.VariableGroup target = null;
            target = (com.idanalytics.products.idscore.request.VariableGroup)get_store().insert_element_user(VARIABLEGROUP$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "VariableGroup" element
     */
    public com.idanalytics.products.idscore.request.VariableGroup addNewVariableGroup()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.VariableGroup target = null;
            target = (com.idanalytics.products.idscore.request.VariableGroup)get_store().add_element_user(VARIABLEGROUP$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "VariableGroup" element
     */
    public void removeVariableGroup(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(VARIABLEGROUP$0, i);
        }
    }
}
