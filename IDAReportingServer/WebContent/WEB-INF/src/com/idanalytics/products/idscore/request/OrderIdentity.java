/*
 * XML Type:  Order_Identity
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.OrderIdentity
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request;


/**
 * An XML Order_Identity(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public interface OrderIdentity extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(OrderIdentity.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("orderidentitya087type");
    
    /**
     * Gets the "Title" element
     */
    java.lang.String getTitle();
    
    /**
     * Gets (as xml) the "Title" element
     */
    com.idanalytics.products.idscore.request.Title xgetTitle();
    
    /**
     * True if has "Title" element
     */
    boolean isSetTitle();
    
    /**
     * Sets the "Title" element
     */
    void setTitle(java.lang.String title);
    
    /**
     * Sets (as xml) the "Title" element
     */
    void xsetTitle(com.idanalytics.products.idscore.request.Title title);
    
    /**
     * Unsets the "Title" element
     */
    void unsetTitle();
    
    /**
     * Gets the "FirstName" element
     */
    java.lang.String getFirstName();
    
    /**
     * Gets (as xml) the "FirstName" element
     */
    com.idanalytics.products.idscore.request.Name xgetFirstName();
    
    /**
     * Sets the "FirstName" element
     */
    void setFirstName(java.lang.String firstName);
    
    /**
     * Sets (as xml) the "FirstName" element
     */
    void xsetFirstName(com.idanalytics.products.idscore.request.Name firstName);
    
    /**
     * Gets the "MiddleName" element
     */
    java.lang.String getMiddleName();
    
    /**
     * Gets (as xml) the "MiddleName" element
     */
    com.idanalytics.products.idscore.request.Name xgetMiddleName();
    
    /**
     * True if has "MiddleName" element
     */
    boolean isSetMiddleName();
    
    /**
     * Sets the "MiddleName" element
     */
    void setMiddleName(java.lang.String middleName);
    
    /**
     * Sets (as xml) the "MiddleName" element
     */
    void xsetMiddleName(com.idanalytics.products.idscore.request.Name middleName);
    
    /**
     * Unsets the "MiddleName" element
     */
    void unsetMiddleName();
    
    /**
     * Gets the "LastName" element
     */
    java.lang.String getLastName();
    
    /**
     * Gets (as xml) the "LastName" element
     */
    com.idanalytics.products.idscore.request.Name xgetLastName();
    
    /**
     * Sets the "LastName" element
     */
    void setLastName(java.lang.String lastName);
    
    /**
     * Sets (as xml) the "LastName" element
     */
    void xsetLastName(com.idanalytics.products.idscore.request.Name lastName);
    
    /**
     * Gets the "Suffix" element
     */
    java.lang.String getSuffix();
    
    /**
     * Gets (as xml) the "Suffix" element
     */
    com.idanalytics.products.idscore.request.Name xgetSuffix();
    
    /**
     * True if has "Suffix" element
     */
    boolean isSetSuffix();
    
    /**
     * Sets the "Suffix" element
     */
    void setSuffix(java.lang.String suffix);
    
    /**
     * Sets (as xml) the "Suffix" element
     */
    void xsetSuffix(com.idanalytics.products.idscore.request.Name suffix);
    
    /**
     * Unsets the "Suffix" element
     */
    void unsetSuffix();
    
    /**
     * Gets the "Address" element
     */
    java.lang.String getAddress();
    
    /**
     * Gets (as xml) the "Address" element
     */
    com.idanalytics.products.idscore.request.AddressLine xgetAddress();
    
    /**
     * Sets the "Address" element
     */
    void setAddress(java.lang.String address);
    
    /**
     * Sets (as xml) the "Address" element
     */
    void xsetAddress(com.idanalytics.products.idscore.request.AddressLine address);
    
    /**
     * Gets the "City" element
     */
    java.lang.String getCity();
    
    /**
     * Gets (as xml) the "City" element
     */
    com.idanalytics.products.idscore.request.City xgetCity();
    
    /**
     * Sets the "City" element
     */
    void setCity(java.lang.String city);
    
    /**
     * Sets (as xml) the "City" element
     */
    void xsetCity(com.idanalytics.products.idscore.request.City city);
    
    /**
     * Gets the "State" element
     */
    java.lang.String getState();
    
    /**
     * Gets (as xml) the "State" element
     */
    com.idanalytics.products.idscore.request.State xgetState();
    
    /**
     * Sets the "State" element
     */
    void setState(java.lang.String state);
    
    /**
     * Sets (as xml) the "State" element
     */
    void xsetState(com.idanalytics.products.idscore.request.State state);
    
    /**
     * Gets the "Zip" element
     */
    java.lang.String getZip();
    
    /**
     * Gets (as xml) the "Zip" element
     */
    com.idanalytics.products.idscore.request.Zip xgetZip();
    
    /**
     * Sets the "Zip" element
     */
    void setZip(java.lang.String zip);
    
    /**
     * Sets (as xml) the "Zip" element
     */
    void xsetZip(com.idanalytics.products.idscore.request.Zip zip);
    
    /**
     * Gets the "Country" element
     */
    java.lang.String getCountry();
    
    /**
     * Gets (as xml) the "Country" element
     */
    com.idanalytics.products.idscore.request.Country xgetCountry();
    
    /**
     * True if has "Country" element
     */
    boolean isSetCountry();
    
    /**
     * Sets the "Country" element
     */
    void setCountry(java.lang.String country);
    
    /**
     * Sets (as xml) the "Country" element
     */
    void xsetCountry(com.idanalytics.products.idscore.request.Country country);
    
    /**
     * Unsets the "Country" element
     */
    void unsetCountry();
    
    /**
     * Gets the "HomePhone" element
     */
    java.lang.String getHomePhone();
    
    /**
     * Gets (as xml) the "HomePhone" element
     */
    com.idanalytics.products.idscore.request.Phone xgetHomePhone();
    
    /**
     * Sets the "HomePhone" element
     */
    void setHomePhone(java.lang.String homePhone);
    
    /**
     * Sets (as xml) the "HomePhone" element
     */
    void xsetHomePhone(com.idanalytics.products.idscore.request.Phone homePhone);
    
    /**
     * Gets the "MobilePhone" element
     */
    java.lang.String getMobilePhone();
    
    /**
     * Gets (as xml) the "MobilePhone" element
     */
    com.idanalytics.products.idscore.request.Phone xgetMobilePhone();
    
    /**
     * True if has "MobilePhone" element
     */
    boolean isSetMobilePhone();
    
    /**
     * Sets the "MobilePhone" element
     */
    void setMobilePhone(java.lang.String mobilePhone);
    
    /**
     * Sets (as xml) the "MobilePhone" element
     */
    void xsetMobilePhone(com.idanalytics.products.idscore.request.Phone mobilePhone);
    
    /**
     * Unsets the "MobilePhone" element
     */
    void unsetMobilePhone();
    
    /**
     * Gets the "WorkPhone" element
     */
    java.lang.String getWorkPhone();
    
    /**
     * Gets (as xml) the "WorkPhone" element
     */
    com.idanalytics.products.idscore.request.Phone xgetWorkPhone();
    
    /**
     * True if has "WorkPhone" element
     */
    boolean isSetWorkPhone();
    
    /**
     * Sets the "WorkPhone" element
     */
    void setWorkPhone(java.lang.String workPhone);
    
    /**
     * Sets (as xml) the "WorkPhone" element
     */
    void xsetWorkPhone(com.idanalytics.products.idscore.request.Phone workPhone);
    
    /**
     * Unsets the "WorkPhone" element
     */
    void unsetWorkPhone();
    
    /**
     * Gets the "Email" element
     */
    java.lang.String getEmail();
    
    /**
     * Gets (as xml) the "Email" element
     */
    com.idanalytics.products.idscore.request.Email xgetEmail();
    
    /**
     * Sets the "Email" element
     */
    void setEmail(java.lang.String email);
    
    /**
     * Sets (as xml) the "Email" element
     */
    void xsetEmail(com.idanalytics.products.idscore.request.Email email);
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.idscore.request.OrderIdentity newInstance() {
          return (com.idanalytics.products.idscore.request.OrderIdentity) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.idscore.request.OrderIdentity newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.idscore.request.OrderIdentity) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.idscore.request.OrderIdentity parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.OrderIdentity) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.idscore.request.OrderIdentity parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.OrderIdentity) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.idscore.request.OrderIdentity parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.OrderIdentity) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.idscore.request.OrderIdentity parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.OrderIdentity) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.idscore.request.OrderIdentity parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.OrderIdentity) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.idscore.request.OrderIdentity parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.OrderIdentity) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.idscore.request.OrderIdentity parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.OrderIdentity) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.idscore.request.OrderIdentity parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.OrderIdentity) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.idscore.request.OrderIdentity parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.OrderIdentity) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.idscore.request.OrderIdentity parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.OrderIdentity) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.idscore.request.OrderIdentity parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.OrderIdentity) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.idscore.request.OrderIdentity parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.OrderIdentity) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.idscore.request.OrderIdentity parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.OrderIdentity) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.idscore.request.OrderIdentity parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.OrderIdentity) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.request.OrderIdentity parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.request.OrderIdentity) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.request.OrderIdentity parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.request.OrderIdentity) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
