/*
 * XML Type:  MessageType
 * Namespace: http://idanalytics.com/products/idscore/result.v31
 * Java type: com.idanalytics.products.idscore.result_v31.MessageType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.result_v31;


/**
 * An XML MessageType(@http://idanalytics.com/products/idscore/result.v31).
 *
 * This is a complex type.
 */
public interface MessageType extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(MessageType.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("messagetype8acbtype");
    
    /**
     * Gets the "MessageType" element
     */
    java.lang.String getMessageType();
    
    /**
     * Gets (as xml) the "MessageType" element
     */
    com.idanalytics.products.idscore.result_v31.MessageType.MessageType2 xgetMessageType();
    
    /**
     * Sets the "MessageType" element
     */
    void setMessageType(java.lang.String messageType);
    
    /**
     * Sets (as xml) the "MessageType" element
     */
    void xsetMessageType(com.idanalytics.products.idscore.result_v31.MessageType.MessageType2 messageType);
    
    /**
     * Gets the "MessageSubType" element
     */
    java.lang.String getMessageSubType();
    
    /**
     * Gets (as xml) the "MessageSubType" element
     */
    com.idanalytics.products.idscore.result_v31.MessageType.MessageSubType xgetMessageSubType();
    
    /**
     * Sets the "MessageSubType" element
     */
    void setMessageSubType(java.lang.String messageSubType);
    
    /**
     * Sets (as xml) the "MessageSubType" element
     */
    void xsetMessageSubType(com.idanalytics.products.idscore.result_v31.MessageType.MessageSubType messageSubType);
    
    /**
     * Gets the "MessageID" element
     */
    java.lang.String getMessageID();
    
    /**
     * Gets (as xml) the "MessageID" element
     */
    com.idanalytics.products.idscore.result_v31.MessageType.MessageID xgetMessageID();
    
    /**
     * Sets the "MessageID" element
     */
    void setMessageID(java.lang.String messageID);
    
    /**
     * Sets (as xml) the "MessageID" element
     */
    void xsetMessageID(com.idanalytics.products.idscore.result_v31.MessageType.MessageID messageID);
    
    /**
     * An XML MessageType(@http://idanalytics.com/products/idscore/result.v31).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result_v31.MessageType$MessageType2.
     */
    public interface MessageType2 extends org.apache.xmlbeans.XmlString
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(MessageType2.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("messagetype003eelemtype");
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.idscore.result_v31.MessageType.MessageType2 newValue(java.lang.Object obj) {
              return (com.idanalytics.products.idscore.result_v31.MessageType.MessageType2) type.newValue( obj ); }
            
            public static com.idanalytics.products.idscore.result_v31.MessageType.MessageType2 newInstance() {
              return (com.idanalytics.products.idscore.result_v31.MessageType.MessageType2) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.idscore.result_v31.MessageType.MessageType2 newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.idscore.result_v31.MessageType.MessageType2) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * An XML MessageSubType(@http://idanalytics.com/products/idscore/result.v31).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result_v31.MessageType$MessageSubType.
     */
    public interface MessageSubType extends org.apache.xmlbeans.XmlString
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(MessageSubType.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("messagesubtypeee5celemtype");
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.idscore.result_v31.MessageType.MessageSubType newValue(java.lang.Object obj) {
              return (com.idanalytics.products.idscore.result_v31.MessageType.MessageSubType) type.newValue( obj ); }
            
            public static com.idanalytics.products.idscore.result_v31.MessageType.MessageSubType newInstance() {
              return (com.idanalytics.products.idscore.result_v31.MessageType.MessageSubType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.idscore.result_v31.MessageType.MessageSubType newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.idscore.result_v31.MessageType.MessageSubType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * An XML MessageID(@http://idanalytics.com/products/idscore/result.v31).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result_v31.MessageType$MessageID.
     */
    public interface MessageID extends org.apache.xmlbeans.XmlString
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(MessageID.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("messageid4addelemtype");
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.idscore.result_v31.MessageType.MessageID newValue(java.lang.Object obj) {
              return (com.idanalytics.products.idscore.result_v31.MessageType.MessageID) type.newValue( obj ); }
            
            public static com.idanalytics.products.idscore.result_v31.MessageType.MessageID newInstance() {
              return (com.idanalytics.products.idscore.result_v31.MessageType.MessageID) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.idscore.result_v31.MessageType.MessageID newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.idscore.result_v31.MessageType.MessageID) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.idscore.result_v31.MessageType newInstance() {
          return (com.idanalytics.products.idscore.result_v31.MessageType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.idscore.result_v31.MessageType newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.idscore.result_v31.MessageType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.idscore.result_v31.MessageType parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result_v31.MessageType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.idscore.result_v31.MessageType parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result_v31.MessageType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.idscore.result_v31.MessageType parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result_v31.MessageType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.idscore.result_v31.MessageType parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result_v31.MessageType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.idscore.result_v31.MessageType parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result_v31.MessageType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.idscore.result_v31.MessageType parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result_v31.MessageType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.idscore.result_v31.MessageType parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result_v31.MessageType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.idscore.result_v31.MessageType parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result_v31.MessageType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.idscore.result_v31.MessageType parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result_v31.MessageType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.idscore.result_v31.MessageType parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result_v31.MessageType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.idscore.result_v31.MessageType parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result_v31.MessageType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.idscore.result_v31.MessageType parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result_v31.MessageType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.idscore.result_v31.MessageType parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result_v31.MessageType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.idscore.result_v31.MessageType parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result_v31.MessageType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.result_v31.MessageType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.result_v31.MessageType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.result_v31.MessageType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.result_v31.MessageType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
