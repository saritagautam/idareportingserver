/*
 * An XML document type.
 * Localname: CARetailFlag
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.CARetailFlagDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * A document containing one CARetailFlag(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public class CARetailFlagDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.CARetailFlagDocument
{
    private static final long serialVersionUID = 1L;
    
    public CARetailFlagDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CARETAILFLAG$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "CARetailFlag");
    
    
    /**
     * Gets the "CARetailFlag" element
     */
    public com.idanalytics.products.idscore.request.CARetailFlagDocument.CARetailFlag.Enum getCARetailFlag()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CARETAILFLAG$0, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.idscore.request.CARetailFlagDocument.CARetailFlag.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "CARetailFlag" element
     */
    public com.idanalytics.products.idscore.request.CARetailFlagDocument.CARetailFlag xgetCARetailFlag()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.CARetailFlagDocument.CARetailFlag target = null;
            target = (com.idanalytics.products.idscore.request.CARetailFlagDocument.CARetailFlag)get_store().find_element_user(CARETAILFLAG$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "CARetailFlag" element
     */
    public void setCARetailFlag(com.idanalytics.products.idscore.request.CARetailFlagDocument.CARetailFlag.Enum caRetailFlag)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CARETAILFLAG$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CARETAILFLAG$0);
            }
            target.setEnumValue(caRetailFlag);
        }
    }
    
    /**
     * Sets (as xml) the "CARetailFlag" element
     */
    public void xsetCARetailFlag(com.idanalytics.products.idscore.request.CARetailFlagDocument.CARetailFlag caRetailFlag)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.CARetailFlagDocument.CARetailFlag target = null;
            target = (com.idanalytics.products.idscore.request.CARetailFlagDocument.CARetailFlag)get_store().find_element_user(CARETAILFLAG$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.CARetailFlagDocument.CARetailFlag)get_store().add_element_user(CARETAILFLAG$0);
            }
            target.set(caRetailFlag);
        }
    }
    /**
     * An XML CARetailFlag(@http://idanalytics.com/products/idscore/request).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.CARetailFlagDocument$CARetailFlag.
     */
    public static class CARetailFlagImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.idanalytics.products.idscore.request.CARetailFlagDocument.CARetailFlag
    {
        private static final long serialVersionUID = 1L;
        
        public CARetailFlagImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected CARetailFlagImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
