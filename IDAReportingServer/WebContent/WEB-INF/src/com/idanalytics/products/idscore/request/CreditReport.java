/*
 * XML Type:  CreditReport
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.CreditReport
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request;


/**
 * An XML CreditReport(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public interface CreditReport extends com.idanalytics.products.idscore.request.Identity
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(CreditReport.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("creditreport01a5type");
    
    /**
     * Gets first "PrevHomePhone" element
     */
    java.lang.String getPrevHomePhone();
    
    /**
     * Gets (as xml) first "PrevHomePhone" element
     */
    com.idanalytics.products.idscore.request.Phone xgetPrevHomePhone();
    
    /**
     * True if has at least one "PrevHomePhone" element
     */
    boolean isSetPrevHomePhone();
    
    /**
     * Gets array of all "PrevHomePhone" elements
     */
    java.lang.String[] getPrevHomePhoneArray();
    
    /**
     * Gets ith "PrevHomePhone" element
     */
    java.lang.String getPrevHomePhoneArray(int i);
    
    /**
     * Gets (as xml) array of all "PrevHomePhone" elements
     */
    com.idanalytics.products.idscore.request.Phone[] xgetPrevHomePhoneArray();
    
    /**
     * Gets (as xml) ith "PrevHomePhone" element
     */
    com.idanalytics.products.idscore.request.Phone xgetPrevHomePhoneArray(int i);
    
    /**
     * Returns number of "PrevHomePhone" element
     */
    int sizeOfPrevHomePhoneArray();
    
    /**
     * Sets first "PrevHomePhone" element
     */
    void setPrevHomePhone(java.lang.String prevHomePhone);
    
    /**
     * Sets (as xml) first "PrevHomePhone" element
     */
    void xsetPrevHomePhone(com.idanalytics.products.idscore.request.Phone prevHomePhone);
    
    /**
     * Removes first "PrevHomePhone" element
     */
    void unsetPrevHomePhone();
    
    /**
     * Sets array of all "PrevHomePhone" element
     */
    void setPrevHomePhoneArray(java.lang.String[] prevHomePhoneArray);
    
    /**
     * Sets ith "PrevHomePhone" element
     */
    void setPrevHomePhoneArray(int i, java.lang.String prevHomePhone);
    
    /**
     * Sets (as xml) array of all "PrevHomePhone" element
     */
    void xsetPrevHomePhoneArray(com.idanalytics.products.idscore.request.Phone[] prevHomePhoneArray);
    
    /**
     * Sets (as xml) ith "PrevHomePhone" element
     */
    void xsetPrevHomePhoneArray(int i, com.idanalytics.products.idscore.request.Phone prevHomePhone);
    
    /**
     * Inserts the value as the ith "PrevHomePhone" element
     */
    void insertPrevHomePhone(int i, java.lang.String prevHomePhone);
    
    /**
     * Appends the value as the last "PrevHomePhone" element
     */
    void addPrevHomePhone(java.lang.String prevHomePhone);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "PrevHomePhone" element
     */
    com.idanalytics.products.idscore.request.Phone insertNewPrevHomePhone(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "PrevHomePhone" element
     */
    com.idanalytics.products.idscore.request.Phone addNewPrevHomePhone();
    
    /**
     * Removes the ith "PrevHomePhone" element
     */
    void removePrevHomePhone(int i);
    
    /**
     * Gets the "Source" element
     */
    com.idanalytics.products.idscore.request.Source.Enum getSource();
    
    /**
     * Gets (as xml) the "Source" element
     */
    com.idanalytics.products.idscore.request.Source xgetSource();
    
    /**
     * True if has "Source" element
     */
    boolean isSetSource();
    
    /**
     * Sets the "Source" element
     */
    void setSource(com.idanalytics.products.idscore.request.Source.Enum source);
    
    /**
     * Sets (as xml) the "Source" element
     */
    void xsetSource(com.idanalytics.products.idscore.request.Source source);
    
    /**
     * Unsets the "Source" element
     */
    void unsetSource();
    
    /**
     * Gets the "Prev2AddressSince" element
     */
    java.lang.Object getPrev2AddressSince();
    
    /**
     * Gets (as xml) the "Prev2AddressSince" element
     */
    com.idanalytics.products.idscore.request.Date xgetPrev2AddressSince();
    
    /**
     * True if has "Prev2AddressSince" element
     */
    boolean isSetPrev2AddressSince();
    
    /**
     * Sets the "Prev2AddressSince" element
     */
    void setPrev2AddressSince(java.lang.Object prev2AddressSince);
    
    /**
     * Sets (as xml) the "Prev2AddressSince" element
     */
    void xsetPrev2AddressSince(com.idanalytics.products.idscore.request.Date prev2AddressSince);
    
    /**
     * Unsets the "Prev2AddressSince" element
     */
    void unsetPrev2AddressSince();
    
    /**
     * Gets the "Prev2Address" element
     */
    com.idanalytics.products.idscore.request.Address getPrev2Address();
    
    /**
     * True if has "Prev2Address" element
     */
    boolean isSetPrev2Address();
    
    /**
     * Sets the "Prev2Address" element
     */
    void setPrev2Address(com.idanalytics.products.idscore.request.Address prev2Address);
    
    /**
     * Appends and returns a new empty "Prev2Address" element
     */
    com.idanalytics.products.idscore.request.Address addNewPrev2Address();
    
    /**
     * Unsets the "Prev2Address" element
     */
    void unsetPrev2Address();
    
    /**
     * Gets the "ParsedPrev2Address" element
     */
    com.idanalytics.products.idscore.request.ParsedAddress getParsedPrev2Address();
    
    /**
     * True if has "ParsedPrev2Address" element
     */
    boolean isSetParsedPrev2Address();
    
    /**
     * Sets the "ParsedPrev2Address" element
     */
    void setParsedPrev2Address(com.idanalytics.products.idscore.request.ParsedAddress parsedPrev2Address);
    
    /**
     * Appends and returns a new empty "ParsedPrev2Address" element
     */
    com.idanalytics.products.idscore.request.ParsedAddress addNewParsedPrev2Address();
    
    /**
     * Unsets the "ParsedPrev2Address" element
     */
    void unsetParsedPrev2Address();
    
    /**
     * Gets the "Prev2City" element
     */
    java.lang.String getPrev2City();
    
    /**
     * Gets (as xml) the "Prev2City" element
     */
    com.idanalytics.products.idscore.request.City xgetPrev2City();
    
    /**
     * True if has "Prev2City" element
     */
    boolean isSetPrev2City();
    
    /**
     * Sets the "Prev2City" element
     */
    void setPrev2City(java.lang.String prev2City);
    
    /**
     * Sets (as xml) the "Prev2City" element
     */
    void xsetPrev2City(com.idanalytics.products.idscore.request.City prev2City);
    
    /**
     * Unsets the "Prev2City" element
     */
    void unsetPrev2City();
    
    /**
     * Gets the "Prev2State" element
     */
    java.lang.String getPrev2State();
    
    /**
     * Gets (as xml) the "Prev2State" element
     */
    com.idanalytics.products.idscore.request.State xgetPrev2State();
    
    /**
     * True if has "Prev2State" element
     */
    boolean isSetPrev2State();
    
    /**
     * Sets the "Prev2State" element
     */
    void setPrev2State(java.lang.String prev2State);
    
    /**
     * Sets (as xml) the "Prev2State" element
     */
    void xsetPrev2State(com.idanalytics.products.idscore.request.State prev2State);
    
    /**
     * Unsets the "Prev2State" element
     */
    void unsetPrev2State();
    
    /**
     * Gets the "Prev2Zip" element
     */
    java.lang.String getPrev2Zip();
    
    /**
     * Gets (as xml) the "Prev2Zip" element
     */
    com.idanalytics.products.idscore.request.Zip xgetPrev2Zip();
    
    /**
     * True if has "Prev2Zip" element
     */
    boolean isSetPrev2Zip();
    
    /**
     * Sets the "Prev2Zip" element
     */
    void setPrev2Zip(java.lang.String prev2Zip);
    
    /**
     * Sets (as xml) the "Prev2Zip" element
     */
    void xsetPrev2Zip(com.idanalytics.products.idscore.request.Zip prev2Zip);
    
    /**
     * Unsets the "Prev2Zip" element
     */
    void unsetPrev2Zip();
    
    /**
     * Gets the "Prev2Country" element
     */
    java.lang.String getPrev2Country();
    
    /**
     * Gets (as xml) the "Prev2Country" element
     */
    com.idanalytics.products.idscore.request.Country xgetPrev2Country();
    
    /**
     * True if has "Prev2Country" element
     */
    boolean isSetPrev2Country();
    
    /**
     * Sets the "Prev2Country" element
     */
    void setPrev2Country(java.lang.String prev2Country);
    
    /**
     * Sets (as xml) the "Prev2Country" element
     */
    void xsetPrev2Country(com.idanalytics.products.idscore.request.Country prev2Country);
    
    /**
     * Unsets the "Prev2Country" element
     */
    void unsetPrev2Country();
    
    /**
     * Gets the "TimeOnFile" element
     */
    java.lang.String getTimeOnFile();
    
    /**
     * Gets (as xml) the "TimeOnFile" element
     */
    com.idanalytics.products.idscore.request.TimeAt xgetTimeOnFile();
    
    /**
     * True if has "TimeOnFile" element
     */
    boolean isSetTimeOnFile();
    
    /**
     * Sets the "TimeOnFile" element
     */
    void setTimeOnFile(java.lang.String timeOnFile);
    
    /**
     * Sets (as xml) the "TimeOnFile" element
     */
    void xsetTimeOnFile(com.idanalytics.products.idscore.request.TimeAt timeOnFile);
    
    /**
     * Unsets the "TimeOnFile" element
     */
    void unsetTimeOnFile();
    
    /**
     * Gets the "OldestTradeMonths" element
     */
    java.lang.String getOldestTradeMonths();
    
    /**
     * Gets (as xml) the "OldestTradeMonths" element
     */
    com.idanalytics.products.idscore.request.TimeAt xgetOldestTradeMonths();
    
    /**
     * True if has "OldestTradeMonths" element
     */
    boolean isSetOldestTradeMonths();
    
    /**
     * Sets the "OldestTradeMonths" element
     */
    void setOldestTradeMonths(java.lang.String oldestTradeMonths);
    
    /**
     * Sets (as xml) the "OldestTradeMonths" element
     */
    void xsetOldestTradeMonths(com.idanalytics.products.idscore.request.TimeAt oldestTradeMonths);
    
    /**
     * Unsets the "OldestTradeMonths" element
     */
    void unsetOldestTradeMonths();
    
    /**
     * Gets the "MortgagePayment" element
     */
    java.lang.Object getMortgagePayment();
    
    /**
     * Gets (as xml) the "MortgagePayment" element
     */
    com.idanalytics.products.idscore.request.MortgagePayment xgetMortgagePayment();
    
    /**
     * True if has "MortgagePayment" element
     */
    boolean isSetMortgagePayment();
    
    /**
     * Sets the "MortgagePayment" element
     */
    void setMortgagePayment(java.lang.Object mortgagePayment);
    
    /**
     * Sets (as xml) the "MortgagePayment" element
     */
    void xsetMortgagePayment(com.idanalytics.products.idscore.request.MortgagePayment mortgagePayment);
    
    /**
     * Unsets the "MortgagePayment" element
     */
    void unsetMortgagePayment();
    
    /**
     * Gets the "FraudFlags" element
     */
    com.idanalytics.products.idscore.request.Bool.Enum getFraudFlags();
    
    /**
     * Gets (as xml) the "FraudFlags" element
     */
    com.idanalytics.products.idscore.request.Bool xgetFraudFlags();
    
    /**
     * True if has "FraudFlags" element
     */
    boolean isSetFraudFlags();
    
    /**
     * Sets the "FraudFlags" element
     */
    void setFraudFlags(com.idanalytics.products.idscore.request.Bool.Enum fraudFlags);
    
    /**
     * Sets (as xml) the "FraudFlags" element
     */
    void xsetFraudFlags(com.idanalytics.products.idscore.request.Bool fraudFlags);
    
    /**
     * Unsets the "FraudFlags" element
     */
    void unsetFraudFlags();
    
    /**
     * Gets the "ConsumerStatement" element
     */
    com.idanalytics.products.idscore.request.Bool.Enum getConsumerStatement();
    
    /**
     * Gets (as xml) the "ConsumerStatement" element
     */
    com.idanalytics.products.idscore.request.Bool xgetConsumerStatement();
    
    /**
     * True if has "ConsumerStatement" element
     */
    boolean isSetConsumerStatement();
    
    /**
     * Sets the "ConsumerStatement" element
     */
    void setConsumerStatement(com.idanalytics.products.idscore.request.Bool.Enum consumerStatement);
    
    /**
     * Sets (as xml) the "ConsumerStatement" element
     */
    void xsetConsumerStatement(com.idanalytics.products.idscore.request.Bool consumerStatement);
    
    /**
     * Unsets the "ConsumerStatement" element
     */
    void unsetConsumerStatement();
    
    /**
     * Gets the "FicoScore" element
     */
    java.math.BigInteger getFicoScore();
    
    /**
     * Gets (as xml) the "FicoScore" element
     */
    com.idanalytics.products.idscore.request.FicoScore xgetFicoScore();
    
    /**
     * True if has "FicoScore" element
     */
    boolean isSetFicoScore();
    
    /**
     * Sets the "FicoScore" element
     */
    void setFicoScore(java.math.BigInteger ficoScore);
    
    /**
     * Sets (as xml) the "FicoScore" element
     */
    void xsetFicoScore(com.idanalytics.products.idscore.request.FicoScore ficoScore);
    
    /**
     * Unsets the "FicoScore" element
     */
    void unsetFicoScore();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.idscore.request.CreditReport newInstance() {
          return (com.idanalytics.products.idscore.request.CreditReport) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.idscore.request.CreditReport newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.idscore.request.CreditReport) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.idscore.request.CreditReport parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.CreditReport) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.idscore.request.CreditReport parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.CreditReport) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.idscore.request.CreditReport parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.CreditReport) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.idscore.request.CreditReport parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.CreditReport) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.idscore.request.CreditReport parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.CreditReport) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.idscore.request.CreditReport parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.CreditReport) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.idscore.request.CreditReport parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.CreditReport) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.idscore.request.CreditReport parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.CreditReport) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.idscore.request.CreditReport parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.CreditReport) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.idscore.request.CreditReport parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.CreditReport) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.idscore.request.CreditReport parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.CreditReport) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.idscore.request.CreditReport parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.CreditReport) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.idscore.request.CreditReport parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.CreditReport) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.idscore.request.CreditReport parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.CreditReport) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.request.CreditReport parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.request.CreditReport) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.request.CreditReport parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.request.CreditReport) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
