/*
 * XML Type:  ShippingInfo
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.ShippingInfo
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML ShippingInfo(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public class ShippingInfoImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.ShippingInfo
{
    private static final long serialVersionUID = 1L;
    
    public ShippingInfoImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IDENTITYINFO$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "IdentityInfo");
    private static final javax.xml.namespace.QName ITEM$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Item");
    
    
    /**
     * Gets the "IdentityInfo" element
     */
    public com.idanalytics.products.idscore.request.CommerceIdentity getIdentityInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.CommerceIdentity target = null;
            target = (com.idanalytics.products.idscore.request.CommerceIdentity)get_store().find_element_user(IDENTITYINFO$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "IdentityInfo" element
     */
    public boolean isSetIdentityInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(IDENTITYINFO$0) != 0;
        }
    }
    
    /**
     * Sets the "IdentityInfo" element
     */
    public void setIdentityInfo(com.idanalytics.products.idscore.request.CommerceIdentity identityInfo)
    {
        generatedSetterHelperImpl(identityInfo, IDENTITYINFO$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "IdentityInfo" element
     */
    public com.idanalytics.products.idscore.request.CommerceIdentity addNewIdentityInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.CommerceIdentity target = null;
            target = (com.idanalytics.products.idscore.request.CommerceIdentity)get_store().add_element_user(IDENTITYINFO$0);
            return target;
        }
    }
    
    /**
     * Unsets the "IdentityInfo" element
     */
    public void unsetIdentityInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(IDENTITYINFO$0, 0);
        }
    }
    
    /**
     * Gets array of all "Item" elements
     */
    public com.idanalytics.products.idscore.request.ItemInfo[] getItemArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(ITEM$2, targetList);
            com.idanalytics.products.idscore.request.ItemInfo[] result = new com.idanalytics.products.idscore.request.ItemInfo[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "Item" element
     */
    public com.idanalytics.products.idscore.request.ItemInfo getItemArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ItemInfo target = null;
            target = (com.idanalytics.products.idscore.request.ItemInfo)get_store().find_element_user(ITEM$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "Item" element
     */
    public int sizeOfItemArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ITEM$2);
        }
    }
    
    /**
     * Sets array of all "Item" element  WARNING: This method is not atomicaly synchronized.
     */
    public void setItemArray(com.idanalytics.products.idscore.request.ItemInfo[] itemArray)
    {
        check_orphaned();
        arraySetterHelper(itemArray, ITEM$2);
    }
    
    /**
     * Sets ith "Item" element
     */
    public void setItemArray(int i, com.idanalytics.products.idscore.request.ItemInfo item)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ItemInfo target = null;
            target = (com.idanalytics.products.idscore.request.ItemInfo)get_store().find_element_user(ITEM$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(item);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "Item" element
     */
    public com.idanalytics.products.idscore.request.ItemInfo insertNewItem(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ItemInfo target = null;
            target = (com.idanalytics.products.idscore.request.ItemInfo)get_store().insert_element_user(ITEM$2, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "Item" element
     */
    public com.idanalytics.products.idscore.request.ItemInfo addNewItem()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ItemInfo target = null;
            target = (com.idanalytics.products.idscore.request.ItemInfo)get_store().add_element_user(ITEM$2);
            return target;
        }
    }
    
    /**
     * Removes the ith "Item" element
     */
    public void removeItem(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ITEM$2, i);
        }
    }
}
