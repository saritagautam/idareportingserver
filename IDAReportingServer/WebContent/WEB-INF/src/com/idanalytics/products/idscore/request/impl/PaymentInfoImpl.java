/*
 * XML Type:  PaymentInfo
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.PaymentInfo
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML PaymentInfo(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public class PaymentInfoImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.PaymentInfo
{
    private static final long serialVersionUID = 1L;
    
    public PaymentInfoImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName PAYMENTTYPE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PaymentType");
    private static final javax.xml.namespace.QName PAYMENTAMOUNT$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PaymentAmount");
    private static final javax.xml.namespace.QName PAYMENTCARDINFO$4 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PaymentCardInfo");
    private static final javax.xml.namespace.QName PAYMENTGENERALINFO$6 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PaymentGeneralInfo");
    private static final javax.xml.namespace.QName PAYMENTDATE$8 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "PaymentDate");
    private static final javax.xml.namespace.QName AUTHORIZATIONDATETIME$10 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "AuthorizationDateTime");
    
    
    /**
     * Gets the "PaymentType" element
     */
    public java.lang.String getPaymentType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PAYMENTTYPE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PaymentType" element
     */
    public com.idanalytics.products.common_v1.NormalizedString xgetPaymentType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.NormalizedString target = null;
            target = (com.idanalytics.products.common_v1.NormalizedString)get_store().find_element_user(PAYMENTTYPE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "PaymentType" element
     */
    public void setPaymentType(java.lang.String paymentType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PAYMENTTYPE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PAYMENTTYPE$0);
            }
            target.setStringValue(paymentType);
        }
    }
    
    /**
     * Sets (as xml) the "PaymentType" element
     */
    public void xsetPaymentType(com.idanalytics.products.common_v1.NormalizedString paymentType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.NormalizedString target = null;
            target = (com.idanalytics.products.common_v1.NormalizedString)get_store().find_element_user(PAYMENTTYPE$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.NormalizedString)get_store().add_element_user(PAYMENTTYPE$0);
            }
            target.set(paymentType);
        }
    }
    
    /**
     * Gets the "PaymentAmount" element
     */
    public java.lang.Object getPaymentAmount()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PAYMENTAMOUNT$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getObjectValue();
        }
    }
    
    /**
     * Gets (as xml) the "PaymentAmount" element
     */
    public com.idanalytics.products.common_v1.OptionalDecimal xgetPaymentAmount()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.OptionalDecimal target = null;
            target = (com.idanalytics.products.common_v1.OptionalDecimal)get_store().find_element_user(PAYMENTAMOUNT$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "PaymentAmount" element
     */
    public void setPaymentAmount(java.lang.Object paymentAmount)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PAYMENTAMOUNT$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PAYMENTAMOUNT$2);
            }
            target.setObjectValue(paymentAmount);
        }
    }
    
    /**
     * Sets (as xml) the "PaymentAmount" element
     */
    public void xsetPaymentAmount(com.idanalytics.products.common_v1.OptionalDecimal paymentAmount)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.OptionalDecimal target = null;
            target = (com.idanalytics.products.common_v1.OptionalDecimal)get_store().find_element_user(PAYMENTAMOUNT$2, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.OptionalDecimal)get_store().add_element_user(PAYMENTAMOUNT$2);
            }
            target.set(paymentAmount);
        }
    }
    
    /**
     * Gets the "PaymentCardInfo" element
     */
    public com.idanalytics.products.idscore.request.CardInfo getPaymentCardInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.CardInfo target = null;
            target = (com.idanalytics.products.idscore.request.CardInfo)get_store().find_element_user(PAYMENTCARDINFO$4, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "PaymentCardInfo" element
     */
    public boolean isSetPaymentCardInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PAYMENTCARDINFO$4) != 0;
        }
    }
    
    /**
     * Sets the "PaymentCardInfo" element
     */
    public void setPaymentCardInfo(com.idanalytics.products.idscore.request.CardInfo paymentCardInfo)
    {
        generatedSetterHelperImpl(paymentCardInfo, PAYMENTCARDINFO$4, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "PaymentCardInfo" element
     */
    public com.idanalytics.products.idscore.request.CardInfo addNewPaymentCardInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.CardInfo target = null;
            target = (com.idanalytics.products.idscore.request.CardInfo)get_store().add_element_user(PAYMENTCARDINFO$4);
            return target;
        }
    }
    
    /**
     * Unsets the "PaymentCardInfo" element
     */
    public void unsetPaymentCardInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PAYMENTCARDINFO$4, 0);
        }
    }
    
    /**
     * Gets the "PaymentGeneralInfo" element
     */
    public com.idanalytics.products.idscore.request.GenericPayment getPaymentGeneralInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.GenericPayment target = null;
            target = (com.idanalytics.products.idscore.request.GenericPayment)get_store().find_element_user(PAYMENTGENERALINFO$6, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "PaymentGeneralInfo" element
     */
    public boolean isSetPaymentGeneralInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PAYMENTGENERALINFO$6) != 0;
        }
    }
    
    /**
     * Sets the "PaymentGeneralInfo" element
     */
    public void setPaymentGeneralInfo(com.idanalytics.products.idscore.request.GenericPayment paymentGeneralInfo)
    {
        generatedSetterHelperImpl(paymentGeneralInfo, PAYMENTGENERALINFO$6, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "PaymentGeneralInfo" element
     */
    public com.idanalytics.products.idscore.request.GenericPayment addNewPaymentGeneralInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.GenericPayment target = null;
            target = (com.idanalytics.products.idscore.request.GenericPayment)get_store().add_element_user(PAYMENTGENERALINFO$6);
            return target;
        }
    }
    
    /**
     * Unsets the "PaymentGeneralInfo" element
     */
    public void unsetPaymentGeneralInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PAYMENTGENERALINFO$6, 0);
        }
    }
    
    /**
     * Gets the "PaymentDate" element
     */
    public java.lang.Object getPaymentDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PAYMENTDATE$8, 0);
            if (target == null)
            {
                return null;
            }
            return target.getObjectValue();
        }
    }
    
    /**
     * Gets (as xml) the "PaymentDate" element
     */
    public com.idanalytics.products.common_v1.OptionalDate xgetPaymentDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.OptionalDate target = null;
            target = (com.idanalytics.products.common_v1.OptionalDate)get_store().find_element_user(PAYMENTDATE$8, 0);
            return target;
        }
    }
    
    /**
     * Sets the "PaymentDate" element
     */
    public void setPaymentDate(java.lang.Object paymentDate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PAYMENTDATE$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PAYMENTDATE$8);
            }
            target.setObjectValue(paymentDate);
        }
    }
    
    /**
     * Sets (as xml) the "PaymentDate" element
     */
    public void xsetPaymentDate(com.idanalytics.products.common_v1.OptionalDate paymentDate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.OptionalDate target = null;
            target = (com.idanalytics.products.common_v1.OptionalDate)get_store().find_element_user(PAYMENTDATE$8, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.OptionalDate)get_store().add_element_user(PAYMENTDATE$8);
            }
            target.set(paymentDate);
        }
    }
    
    /**
     * Gets the "AuthorizationDateTime" element
     */
    public java.lang.Object getAuthorizationDateTime()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(AUTHORIZATIONDATETIME$10, 0);
            if (target == null)
            {
                return null;
            }
            return target.getObjectValue();
        }
    }
    
    /**
     * Gets (as xml) the "AuthorizationDateTime" element
     */
    public com.idanalytics.products.common_v1.OptionalDateTime xgetAuthorizationDateTime()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.OptionalDateTime target = null;
            target = (com.idanalytics.products.common_v1.OptionalDateTime)get_store().find_element_user(AUTHORIZATIONDATETIME$10, 0);
            return target;
        }
    }
    
    /**
     * True if has "AuthorizationDateTime" element
     */
    public boolean isSetAuthorizationDateTime()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(AUTHORIZATIONDATETIME$10) != 0;
        }
    }
    
    /**
     * Sets the "AuthorizationDateTime" element
     */
    public void setAuthorizationDateTime(java.lang.Object authorizationDateTime)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(AUTHORIZATIONDATETIME$10, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(AUTHORIZATIONDATETIME$10);
            }
            target.setObjectValue(authorizationDateTime);
        }
    }
    
    /**
     * Sets (as xml) the "AuthorizationDateTime" element
     */
    public void xsetAuthorizationDateTime(com.idanalytics.products.common_v1.OptionalDateTime authorizationDateTime)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.OptionalDateTime target = null;
            target = (com.idanalytics.products.common_v1.OptionalDateTime)get_store().find_element_user(AUTHORIZATIONDATETIME$10, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.OptionalDateTime)get_store().add_element_user(AUTHORIZATIONDATETIME$10);
            }
            target.set(authorizationDateTime);
        }
    }
    
    /**
     * Unsets the "AuthorizationDateTime" element
     */
    public void unsetAuthorizationDateTime()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(AUTHORIZATIONDATETIME$10, 0);
        }
    }
}
