/*
 * XML Type:  QuestionResponse
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.QuestionResponse
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML QuestionResponse(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public class QuestionResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.QuestionResponse
{
    private static final long serialVersionUID = 1L;
    
    public QuestionResponseImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName QUESTIONID$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "QuestionID");
    private static final javax.xml.namespace.QName QUESTIONTEXT$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "QuestionText");
    private static final javax.xml.namespace.QName ANSWER$4 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Answer");
    
    
    /**
     * Gets the "QuestionID" element
     */
    public int getQuestionID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUESTIONID$0, 0);
            if (target == null)
            {
                return 0;
            }
            return target.getIntValue();
        }
    }
    
    /**
     * Gets (as xml) the "QuestionID" element
     */
    public org.apache.xmlbeans.XmlInt xgetQuestionID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlInt target = null;
            target = (org.apache.xmlbeans.XmlInt)get_store().find_element_user(QUESTIONID$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "QuestionID" element
     */
    public void setQuestionID(int questionID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUESTIONID$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(QUESTIONID$0);
            }
            target.setIntValue(questionID);
        }
    }
    
    /**
     * Sets (as xml) the "QuestionID" element
     */
    public void xsetQuestionID(org.apache.xmlbeans.XmlInt questionID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlInt target = null;
            target = (org.apache.xmlbeans.XmlInt)get_store().find_element_user(QUESTIONID$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlInt)get_store().add_element_user(QUESTIONID$0);
            }
            target.set(questionID);
        }
    }
    
    /**
     * Gets the "QuestionText" element
     */
    public java.lang.String getQuestionText()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUESTIONTEXT$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "QuestionText" element
     */
    public org.apache.xmlbeans.XmlString xgetQuestionText()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(QUESTIONTEXT$2, 0);
            return target;
        }
    }
    
    /**
     * True if has "QuestionText" element
     */
    public boolean isSetQuestionText()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(QUESTIONTEXT$2) != 0;
        }
    }
    
    /**
     * Sets the "QuestionText" element
     */
    public void setQuestionText(java.lang.String questionText)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(QUESTIONTEXT$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(QUESTIONTEXT$2);
            }
            target.setStringValue(questionText);
        }
    }
    
    /**
     * Sets (as xml) the "QuestionText" element
     */
    public void xsetQuestionText(org.apache.xmlbeans.XmlString questionText)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(QUESTIONTEXT$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(QUESTIONTEXT$2);
            }
            target.set(questionText);
        }
    }
    
    /**
     * Unsets the "QuestionText" element
     */
    public void unsetQuestionText()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(QUESTIONTEXT$2, 0);
        }
    }
    
    /**
     * Gets array of all "Answer" elements
     */
    public java.lang.String[] getAnswerArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(ANSWER$4, targetList);
            java.lang.String[] result = new java.lang.String[targetList.size()];
            for (int i = 0, len = targetList.size() ; i < len ; i++)
                result[i] = ((org.apache.xmlbeans.SimpleValue)targetList.get(i)).getStringValue();
            return result;
        }
    }
    
    /**
     * Gets ith "Answer" element
     */
    public java.lang.String getAnswerArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ANSWER$4, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) array of all "Answer" elements
     */
    public org.apache.xmlbeans.XmlString[] xgetAnswerArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(ANSWER$4, targetList);
            org.apache.xmlbeans.XmlString[] result = new org.apache.xmlbeans.XmlString[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets (as xml) ith "Answer" element
     */
    public org.apache.xmlbeans.XmlString xgetAnswerArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(ANSWER$4, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "Answer" element
     */
    public int sizeOfAnswerArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ANSWER$4);
        }
    }
    
    /**
     * Sets array of all "Answer" element
     */
    public void setAnswerArray(java.lang.String[] answerArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(answerArray, ANSWER$4);
        }
    }
    
    /**
     * Sets ith "Answer" element
     */
    public void setAnswerArray(int i, java.lang.String answer)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ANSWER$4, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.setStringValue(answer);
        }
    }
    
    /**
     * Sets (as xml) array of all "Answer" element
     */
    public void xsetAnswerArray(org.apache.xmlbeans.XmlString[]answerArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(answerArray, ANSWER$4);
        }
    }
    
    /**
     * Sets (as xml) ith "Answer" element
     */
    public void xsetAnswerArray(int i, org.apache.xmlbeans.XmlString answer)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(ANSWER$4, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(answer);
        }
    }
    
    /**
     * Inserts the value as the ith "Answer" element
     */
    public void insertAnswer(int i, java.lang.String answer)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = 
                (org.apache.xmlbeans.SimpleValue)get_store().insert_element_user(ANSWER$4, i);
            target.setStringValue(answer);
        }
    }
    
    /**
     * Appends the value as the last "Answer" element
     */
    public void addAnswer(java.lang.String answer)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ANSWER$4);
            target.setStringValue(answer);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "Answer" element
     */
    public org.apache.xmlbeans.XmlString insertNewAnswer(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().insert_element_user(ANSWER$4, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "Answer" element
     */
    public org.apache.xmlbeans.XmlString addNewAnswer()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(ANSWER$4);
            return target;
        }
    }
    
    /**
     * Removes the ith "Answer" element
     */
    public void removeAnswer(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ANSWER$4, i);
        }
    }
}
