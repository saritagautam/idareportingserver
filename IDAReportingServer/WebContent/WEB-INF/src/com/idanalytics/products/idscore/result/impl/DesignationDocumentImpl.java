/*
 * An XML document type.
 * Localname: Designation
 * Namespace: http://idanalytics.com/products/idscore/result
 * Java type: com.idanalytics.products.idscore.result.DesignationDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.result.impl;
/**
 * A document containing one Designation(@http://idanalytics.com/products/idscore/result) element.
 *
 * This is a complex type.
 */
public class DesignationDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.result.DesignationDocument
{
    private static final long serialVersionUID = 1L;
    
    public DesignationDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName DESIGNATION$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "Designation");
    
    
    /**
     * Gets the "Designation" element
     */
    public com.idanalytics.products.idscore.result.DesignationDocument.Designation.Enum getDesignation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESIGNATION$0, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.idscore.result.DesignationDocument.Designation.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "Designation" element
     */
    public com.idanalytics.products.idscore.result.DesignationDocument.Designation xgetDesignation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.DesignationDocument.Designation target = null;
            target = (com.idanalytics.products.idscore.result.DesignationDocument.Designation)get_store().find_element_user(DESIGNATION$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Designation" element
     */
    public void setDesignation(com.idanalytics.products.idscore.result.DesignationDocument.Designation.Enum designation)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESIGNATION$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DESIGNATION$0);
            }
            target.setEnumValue(designation);
        }
    }
    
    /**
     * Sets (as xml) the "Designation" element
     */
    public void xsetDesignation(com.idanalytics.products.idscore.result.DesignationDocument.Designation designation)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.DesignationDocument.Designation target = null;
            target = (com.idanalytics.products.idscore.result.DesignationDocument.Designation)get_store().find_element_user(DESIGNATION$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.result.DesignationDocument.Designation)get_store().add_element_user(DESIGNATION$0);
            }
            target.set(designation);
        }
    }
    /**
     * An XML Designation(@http://idanalytics.com/products/idscore/result).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result.DesignationDocument$Designation.
     */
    public static class DesignationImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.idanalytics.products.idscore.result.DesignationDocument.Designation
    {
        private static final long serialVersionUID = 1L;
        
        public DesignationImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected DesignationImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
