/*
 * An XML document type.
 * Localname: Channel
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.ChannelDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * A document containing one Channel(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public class ChannelDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.ChannelDocument
{
    private static final long serialVersionUID = 1L;
    
    public ChannelDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CHANNEL$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Channel");
    
    
    /**
     * Gets the "Channel" element
     */
    public java.lang.String getChannel()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CHANNEL$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Channel" element
     */
    public com.idanalytics.products.idscore.request.ChannelDocument.Channel xgetChannel()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ChannelDocument.Channel target = null;
            target = (com.idanalytics.products.idscore.request.ChannelDocument.Channel)get_store().find_element_user(CHANNEL$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Channel" element
     */
    public void setChannel(java.lang.String channel)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CHANNEL$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CHANNEL$0);
            }
            target.setStringValue(channel);
        }
    }
    
    /**
     * Sets (as xml) the "Channel" element
     */
    public void xsetChannel(com.idanalytics.products.idscore.request.ChannelDocument.Channel channel)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ChannelDocument.Channel target = null;
            target = (com.idanalytics.products.idscore.request.ChannelDocument.Channel)get_store().find_element_user(CHANNEL$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.ChannelDocument.Channel)get_store().add_element_user(CHANNEL$0);
            }
            target.set(channel);
        }
    }
    /**
     * An XML Channel(@http://idanalytics.com/products/idscore/request).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.ChannelDocument$Channel.
     */
    public static class ChannelImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.request.ChannelDocument.Channel
    {
        private static final long serialVersionUID = 1L;
        
        public ChannelImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected ChannelImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
