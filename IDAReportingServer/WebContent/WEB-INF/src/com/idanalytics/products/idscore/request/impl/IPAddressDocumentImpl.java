/*
 * An XML document type.
 * Localname: IPAddress
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.IPAddressDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * A document containing one IPAddress(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public class IPAddressDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.IPAddressDocument
{
    private static final long serialVersionUID = 1L;
    
    public IPAddressDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IPADDRESS$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "IPAddress");
    
    
    /**
     * Gets the "IPAddress" element
     */
    public java.lang.String getIPAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IPADDRESS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IPAddress" element
     */
    public com.idanalytics.products.idscore.request.IPAddressDocument.IPAddress xgetIPAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.IPAddressDocument.IPAddress target = null;
            target = (com.idanalytics.products.idscore.request.IPAddressDocument.IPAddress)get_store().find_element_user(IPADDRESS$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "IPAddress" element
     */
    public void setIPAddress(java.lang.String ipAddress)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IPADDRESS$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IPADDRESS$0);
            }
            target.setStringValue(ipAddress);
        }
    }
    
    /**
     * Sets (as xml) the "IPAddress" element
     */
    public void xsetIPAddress(com.idanalytics.products.idscore.request.IPAddressDocument.IPAddress ipAddress)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.IPAddressDocument.IPAddress target = null;
            target = (com.idanalytics.products.idscore.request.IPAddressDocument.IPAddress)get_store().find_element_user(IPADDRESS$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.IPAddressDocument.IPAddress)get_store().add_element_user(IPADDRESS$0);
            }
            target.set(ipAddress);
        }
    }
    /**
     * An XML IPAddress(@http://idanalytics.com/products/idscore/request).
     *
     * This is a union type. Instances are of one of the following types:
     *     com.idanalytics.products.idscore.request.IPAddressDocument$IPAddress$Member
     *     com.idanalytics.products.idscore.request.IPAddressDocument$IPAddress$Member2
     */
    public static class IPAddressImpl extends org.apache.xmlbeans.impl.values.XmlUnionImpl implements com.idanalytics.products.idscore.request.IPAddressDocument.IPAddress, com.idanalytics.products.idscore.request.IPAddressDocument.IPAddress.Member, com.idanalytics.products.idscore.request.IPAddressDocument.IPAddress.Member2
    {
        private static final long serialVersionUID = 1L;
        
        public IPAddressImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected IPAddressImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
        /**
         * An anonymous inner XML type.
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.IPAddressDocument$IPAddress$Member.
         */
        public static class MemberImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.request.IPAddressDocument.IPAddress.Member
        {
            private static final long serialVersionUID = 1L;
            
            public MemberImpl(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected MemberImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
        /**
         * An anonymous inner XML type.
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.IPAddressDocument$IPAddress$Member2.
         */
        public static class MemberImpl2 extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.request.IPAddressDocument.IPAddress.Member2
        {
            private static final long serialVersionUID = 1L;
            
            public MemberImpl2(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected MemberImpl2(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
    }
}
