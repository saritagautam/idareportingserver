/*
 * An XML document type.
 * Localname: OrderDate
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.OrderDateDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * A document containing one OrderDate(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public class OrderDateDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.OrderDateDocument
{
    private static final long serialVersionUID = 1L;
    
    public OrderDateDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ORDERDATE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "OrderDate");
    
    
    /**
     * Gets the "OrderDate" element
     */
    public java.util.Calendar getOrderDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ORDERDATE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getCalendarValue();
        }
    }
    
    /**
     * Gets (as xml) the "OrderDate" element
     */
    public com.idanalytics.products.idscore.request.OrderDateDocument.OrderDate xgetOrderDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.OrderDateDocument.OrderDate target = null;
            target = (com.idanalytics.products.idscore.request.OrderDateDocument.OrderDate)get_store().find_element_user(ORDERDATE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "OrderDate" element
     */
    public void setOrderDate(java.util.Calendar orderDate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ORDERDATE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ORDERDATE$0);
            }
            target.setCalendarValue(orderDate);
        }
    }
    
    /**
     * Sets (as xml) the "OrderDate" element
     */
    public void xsetOrderDate(com.idanalytics.products.idscore.request.OrderDateDocument.OrderDate orderDate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.OrderDateDocument.OrderDate target = null;
            target = (com.idanalytics.products.idscore.request.OrderDateDocument.OrderDate)get_store().find_element_user(ORDERDATE$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.OrderDateDocument.OrderDate)get_store().add_element_user(ORDERDATE$0);
            }
            target.set(orderDate);
        }
    }
    /**
     * An XML OrderDate(@http://idanalytics.com/products/idscore/request).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.OrderDateDocument$OrderDate.
     */
    public static class OrderDateImpl extends org.apache.xmlbeans.impl.values.JavaGDateHolderEx implements com.idanalytics.products.idscore.request.OrderDateDocument.OrderDate
    {
        private static final long serialVersionUID = 1L;
        
        public OrderDateImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected OrderDateImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
