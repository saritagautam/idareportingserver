/*
 * An XML document type.
 * Localname: EncodedDeviceInfo
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.EncodedDeviceInfoDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * A document containing one EncodedDeviceInfo(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public class EncodedDeviceInfoDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.EncodedDeviceInfoDocument
{
    private static final long serialVersionUID = 1L;
    
    public EncodedDeviceInfoDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ENCODEDDEVICEINFO$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "EncodedDeviceInfo");
    
    
    /**
     * Gets the "EncodedDeviceInfo" element
     */
    public java.lang.String getEncodedDeviceInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ENCODEDDEVICEINFO$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "EncodedDeviceInfo" element
     */
    public com.idanalytics.products.idscore.request.EncodedDeviceInfoDocument.EncodedDeviceInfo xgetEncodedDeviceInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.EncodedDeviceInfoDocument.EncodedDeviceInfo target = null;
            target = (com.idanalytics.products.idscore.request.EncodedDeviceInfoDocument.EncodedDeviceInfo)get_store().find_element_user(ENCODEDDEVICEINFO$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "EncodedDeviceInfo" element
     */
    public void setEncodedDeviceInfo(java.lang.String encodedDeviceInfo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ENCODEDDEVICEINFO$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ENCODEDDEVICEINFO$0);
            }
            target.setStringValue(encodedDeviceInfo);
        }
    }
    
    /**
     * Sets (as xml) the "EncodedDeviceInfo" element
     */
    public void xsetEncodedDeviceInfo(com.idanalytics.products.idscore.request.EncodedDeviceInfoDocument.EncodedDeviceInfo encodedDeviceInfo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.EncodedDeviceInfoDocument.EncodedDeviceInfo target = null;
            target = (com.idanalytics.products.idscore.request.EncodedDeviceInfoDocument.EncodedDeviceInfo)get_store().find_element_user(ENCODEDDEVICEINFO$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.EncodedDeviceInfoDocument.EncodedDeviceInfo)get_store().add_element_user(ENCODEDDEVICEINFO$0);
            }
            target.set(encodedDeviceInfo);
        }
    }
    /**
     * An XML EncodedDeviceInfo(@http://idanalytics.com/products/idscore/request).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.EncodedDeviceInfoDocument$EncodedDeviceInfo.
     */
    public static class EncodedDeviceInfoImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.request.EncodedDeviceInfoDocument.EncodedDeviceInfo
    {
        private static final long serialVersionUID = 1L;
        
        public EncodedDeviceInfoImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected EncodedDeviceInfoImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
