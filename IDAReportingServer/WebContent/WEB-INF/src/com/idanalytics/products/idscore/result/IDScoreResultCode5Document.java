/*
 * An XML document type.
 * Localname: IDScoreResultCode5
 * Namespace: http://idanalytics.com/products/idscore/result
 * Java type: com.idanalytics.products.idscore.result.IDScoreResultCode5Document
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.result;


/**
 * A document containing one IDScoreResultCode5(@http://idanalytics.com/products/idscore/result) element.
 *
 * This is a complex type.
 */
public interface IDScoreResultCode5Document extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(IDScoreResultCode5Document.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("idscoreresultcode57cb0doctype");
    
    /**
     * Gets the "IDScoreResultCode5" element
     */
    java.lang.String getIDScoreResultCode5();
    
    /**
     * Gets (as xml) the "IDScoreResultCode5" element
     */
    com.idanalytics.products.idscore.result.IDScoreResultCode5Document.IDScoreResultCode5 xgetIDScoreResultCode5();
    
    /**
     * Sets the "IDScoreResultCode5" element
     */
    void setIDScoreResultCode5(java.lang.String idScoreResultCode5);
    
    /**
     * Sets (as xml) the "IDScoreResultCode5" element
     */
    void xsetIDScoreResultCode5(com.idanalytics.products.idscore.result.IDScoreResultCode5Document.IDScoreResultCode5 idScoreResultCode5);
    
    /**
     * An XML IDScoreResultCode5(@http://idanalytics.com/products/idscore/result).
     *
     * This is a union type. Instances are of one of the following types:
     *     com.idanalytics.products.idscore.result.IDScoreResultCode5Document$IDScoreResultCode5$Member
     *     com.idanalytics.products.idscore.result.IDScoreResultCode5Document$IDScoreResultCode5$Member2
     */
    public interface IDScoreResultCode5 extends org.apache.xmlbeans.XmlAnySimpleType
    {
        java.lang.Object getObjectValue();
        void setObjectValue(java.lang.Object val);
        /** @deprecated */
        java.lang.Object objectValue();
        /** @deprecated */
        void objectSet(java.lang.Object val);
        org.apache.xmlbeans.SchemaType instanceType();
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(IDScoreResultCode5.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("idscoreresultcode54a60elemtype");
        
        /**
         * An anonymous inner XML type.
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result.IDScoreResultCode5Document$IDScoreResultCode5$Member.
         */
        public interface Member extends org.apache.xmlbeans.XmlString
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Member.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("anon5d5btype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static com.idanalytics.products.idscore.result.IDScoreResultCode5Document.IDScoreResultCode5.Member newValue(java.lang.Object obj) {
                  return (com.idanalytics.products.idscore.result.IDScoreResultCode5Document.IDScoreResultCode5.Member) type.newValue( obj ); }
                
                public static com.idanalytics.products.idscore.result.IDScoreResultCode5Document.IDScoreResultCode5.Member newInstance() {
                  return (com.idanalytics.products.idscore.result.IDScoreResultCode5Document.IDScoreResultCode5.Member) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static com.idanalytics.products.idscore.result.IDScoreResultCode5Document.IDScoreResultCode5.Member newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (com.idanalytics.products.idscore.result.IDScoreResultCode5Document.IDScoreResultCode5.Member) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * An anonymous inner XML type.
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result.IDScoreResultCode5Document$IDScoreResultCode5$Member2.
         */
        public interface Member2 extends org.apache.xmlbeans.XmlString
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Member2.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("anon387atype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static com.idanalytics.products.idscore.result.IDScoreResultCode5Document.IDScoreResultCode5.Member2 newValue(java.lang.Object obj) {
                  return (com.idanalytics.products.idscore.result.IDScoreResultCode5Document.IDScoreResultCode5.Member2) type.newValue( obj ); }
                
                public static com.idanalytics.products.idscore.result.IDScoreResultCode5Document.IDScoreResultCode5.Member2 newInstance() {
                  return (com.idanalytics.products.idscore.result.IDScoreResultCode5Document.IDScoreResultCode5.Member2) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static com.idanalytics.products.idscore.result.IDScoreResultCode5Document.IDScoreResultCode5.Member2 newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (com.idanalytics.products.idscore.result.IDScoreResultCode5Document.IDScoreResultCode5.Member2) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.idscore.result.IDScoreResultCode5Document.IDScoreResultCode5 newValue(java.lang.Object obj) {
              return (com.idanalytics.products.idscore.result.IDScoreResultCode5Document.IDScoreResultCode5) type.newValue( obj ); }
            
            public static com.idanalytics.products.idscore.result.IDScoreResultCode5Document.IDScoreResultCode5 newInstance() {
              return (com.idanalytics.products.idscore.result.IDScoreResultCode5Document.IDScoreResultCode5) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.idscore.result.IDScoreResultCode5Document.IDScoreResultCode5 newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.idscore.result.IDScoreResultCode5Document.IDScoreResultCode5) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.idscore.result.IDScoreResultCode5Document newInstance() {
          return (com.idanalytics.products.idscore.result.IDScoreResultCode5Document) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.idscore.result.IDScoreResultCode5Document newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.idscore.result.IDScoreResultCode5Document) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.idscore.result.IDScoreResultCode5Document parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result.IDScoreResultCode5Document) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.idscore.result.IDScoreResultCode5Document parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result.IDScoreResultCode5Document) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.idscore.result.IDScoreResultCode5Document parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result.IDScoreResultCode5Document) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.idscore.result.IDScoreResultCode5Document parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result.IDScoreResultCode5Document) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.idscore.result.IDScoreResultCode5Document parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result.IDScoreResultCode5Document) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.idscore.result.IDScoreResultCode5Document parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result.IDScoreResultCode5Document) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.idscore.result.IDScoreResultCode5Document parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result.IDScoreResultCode5Document) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.idscore.result.IDScoreResultCode5Document parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result.IDScoreResultCode5Document) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.idscore.result.IDScoreResultCode5Document parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result.IDScoreResultCode5Document) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.idscore.result.IDScoreResultCode5Document parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result.IDScoreResultCode5Document) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.idscore.result.IDScoreResultCode5Document parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result.IDScoreResultCode5Document) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.idscore.result.IDScoreResultCode5Document parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result.IDScoreResultCode5Document) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.idscore.result.IDScoreResultCode5Document parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result.IDScoreResultCode5Document) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.idscore.result.IDScoreResultCode5Document parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result.IDScoreResultCode5Document) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.result.IDScoreResultCode5Document parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.result.IDScoreResultCode5Document) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.result.IDScoreResultCode5Document parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.result.IDScoreResultCode5Document) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
