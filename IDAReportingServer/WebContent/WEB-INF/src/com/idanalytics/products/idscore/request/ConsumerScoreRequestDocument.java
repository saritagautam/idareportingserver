/*
 * An XML document type.
 * Localname: ConsumerScoreRequest
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request;


/**
 * A document containing one ConsumerScoreRequest(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public interface ConsumerScoreRequestDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ConsumerScoreRequestDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("consumerscorerequest67dbdoctype");
    
    /**
     * Gets the "ConsumerScoreRequest" element
     */
    com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument.ConsumerScoreRequest getConsumerScoreRequest();
    
    /**
     * Sets the "ConsumerScoreRequest" element
     */
    void setConsumerScoreRequest(com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument.ConsumerScoreRequest consumerScoreRequest);
    
    /**
     * Appends and returns a new empty "ConsumerScoreRequest" element
     */
    com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument.ConsumerScoreRequest addNewConsumerScoreRequest();
    
    /**
     * An XML ConsumerScoreRequest(@http://idanalytics.com/products/idscore/request).
     *
     * This is a complex type.
     */
    public interface ConsumerScoreRequest extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ConsumerScoreRequest.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("consumerscorerequestb98celemtype");
        
        /**
         * Gets the "ConsumerHeader" element
         */
        com.idanalytics.products.idscore.request.ConsumerHeaderDocument.ConsumerHeader getConsumerHeader();
        
        /**
         * Sets the "ConsumerHeader" element
         */
        void setConsumerHeader(com.idanalytics.products.idscore.request.ConsumerHeaderDocument.ConsumerHeader consumerHeader);
        
        /**
         * Appends and returns a new empty "ConsumerHeader" element
         */
        com.idanalytics.products.idscore.request.ConsumerHeaderDocument.ConsumerHeader addNewConsumerHeader();
        
        /**
         * Gets the "Identity" element
         */
        com.idanalytics.products.idscore.request.Identity getIdentity();
        
        /**
         * Sets the "Identity" element
         */
        void setIdentity(com.idanalytics.products.idscore.request.Identity identity);
        
        /**
         * Appends and returns a new empty "Identity" element
         */
        com.idanalytics.products.idscore.request.Identity addNewIdentity();
        
        /**
         * Gets the "SurveyResults" element
         */
        com.idanalytics.products.idscore.request.SurveyResults getSurveyResults();
        
        /**
         * True if has "SurveyResults" element
         */
        boolean isSetSurveyResults();
        
        /**
         * Sets the "SurveyResults" element
         */
        void setSurveyResults(com.idanalytics.products.idscore.request.SurveyResults surveyResults);
        
        /**
         * Appends and returns a new empty "SurveyResults" element
         */
        com.idanalytics.products.idscore.request.SurveyResults addNewSurveyResults();
        
        /**
         * Unsets the "SurveyResults" element
         */
        void unsetSurveyResults();
        
        /**
         * Gets the "IDAInternal" element
         */
        com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal getIDAInternal();
        
        /**
         * True if has "IDAInternal" element
         */
        boolean isSetIDAInternal();
        
        /**
         * Sets the "IDAInternal" element
         */
        void setIDAInternal(com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal idaInternal);
        
        /**
         * Appends and returns a new empty "IDAInternal" element
         */
        com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal addNewIDAInternal();
        
        /**
         * Unsets the "IDAInternal" element
         */
        void unsetIDAInternal();
        
        /**
         * Gets the "ExternalData" element
         */
        com.idanalytics.products.idscore.request.ExternalData getExternalData();
        
        /**
         * True if has "ExternalData" element
         */
        boolean isSetExternalData();
        
        /**
         * Sets the "ExternalData" element
         */
        void setExternalData(com.idanalytics.products.idscore.request.ExternalData externalData);
        
        /**
         * Appends and returns a new empty "ExternalData" element
         */
        com.idanalytics.products.idscore.request.ExternalData addNewExternalData();
        
        /**
         * Unsets the "ExternalData" element
         */
        void unsetExternalData();
        
        /**
         * Gets the "schemaVersion" attribute
         */
        java.math.BigDecimal getSchemaVersion();
        
        /**
         * Gets (as xml) the "schemaVersion" attribute
         */
        org.apache.xmlbeans.XmlDecimal xgetSchemaVersion();
        
        /**
         * True if has "schemaVersion" attribute
         */
        boolean isSetSchemaVersion();
        
        /**
         * Sets the "schemaVersion" attribute
         */
        void setSchemaVersion(java.math.BigDecimal schemaVersion);
        
        /**
         * Sets (as xml) the "schemaVersion" attribute
         */
        void xsetSchemaVersion(org.apache.xmlbeans.XmlDecimal schemaVersion);
        
        /**
         * Unsets the "schemaVersion" attribute
         */
        void unsetSchemaVersion();
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument.ConsumerScoreRequest newInstance() {
              return (com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument.ConsumerScoreRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument.ConsumerScoreRequest newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument.ConsumerScoreRequest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument newInstance() {
          return (com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
