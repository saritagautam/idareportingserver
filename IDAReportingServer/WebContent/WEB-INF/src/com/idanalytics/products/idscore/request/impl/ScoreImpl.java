/*
 * XML Type:  Score
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.Score
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML Score(@http://idanalytics.com/products/idscore/request).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.Score.
 */
public class ScoreImpl extends org.apache.xmlbeans.impl.values.JavaIntHolderEx implements com.idanalytics.products.idscore.request.Score
{
    private static final long serialVersionUID = 1L;
    
    public ScoreImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected ScoreImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
