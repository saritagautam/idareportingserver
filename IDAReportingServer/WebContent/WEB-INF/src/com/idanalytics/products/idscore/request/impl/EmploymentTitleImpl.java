/*
 * XML Type:  EmploymentTitle
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.EmploymentTitle
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML EmploymentTitle(@http://idanalytics.com/products/idscore/request).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.EmploymentTitle.
 */
public class EmploymentTitleImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.request.EmploymentTitle
{
    private static final long serialVersionUID = 1L;
    
    public EmploymentTitleImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected EmploymentTitleImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
