/*
 * An XML document type.
 * Localname: CARetailFlag
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.CARetailFlagDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request;


/**
 * A document containing one CARetailFlag(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public interface CARetailFlagDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(CARetailFlagDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("caretailflag1815doctype");
    
    /**
     * Gets the "CARetailFlag" element
     */
    com.idanalytics.products.idscore.request.CARetailFlagDocument.CARetailFlag.Enum getCARetailFlag();
    
    /**
     * Gets (as xml) the "CARetailFlag" element
     */
    com.idanalytics.products.idscore.request.CARetailFlagDocument.CARetailFlag xgetCARetailFlag();
    
    /**
     * Sets the "CARetailFlag" element
     */
    void setCARetailFlag(com.idanalytics.products.idscore.request.CARetailFlagDocument.CARetailFlag.Enum caRetailFlag);
    
    /**
     * Sets (as xml) the "CARetailFlag" element
     */
    void xsetCARetailFlag(com.idanalytics.products.idscore.request.CARetailFlagDocument.CARetailFlag caRetailFlag);
    
    /**
     * An XML CARetailFlag(@http://idanalytics.com/products/idscore/request).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.CARetailFlagDocument$CARetailFlag.
     */
    public interface CARetailFlag extends org.apache.xmlbeans.XmlString
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(CARetailFlag.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("caretailflag504celemtype");
        
        org.apache.xmlbeans.StringEnumAbstractBase enumValue();
        void set(org.apache.xmlbeans.StringEnumAbstractBase e);
        
        static final Enum X = Enum.forString("");
        static final Enum NOT_RETAIL = Enum.forString("NOT_RETAIL");
        static final Enum NOT_CA_RETAIL = Enum.forString("NOT_CA_RETAIL");
        static final Enum CA_RETAIL = Enum.forString("CA_RETAIL");
        
        static final int INT_X = Enum.INT_X;
        static final int INT_NOT_RETAIL = Enum.INT_NOT_RETAIL;
        static final int INT_NOT_CA_RETAIL = Enum.INT_NOT_CA_RETAIL;
        static final int INT_CA_RETAIL = Enum.INT_CA_RETAIL;
        
        /**
         * Enumeration value class for com.idanalytics.products.idscore.request.CARetailFlagDocument$CARetailFlag.
         * These enum values can be used as follows:
         * <pre>
         * enum.toString(); // returns the string value of the enum
         * enum.intValue(); // returns an int value, useful for switches
         * // e.g., case Enum.INT_X
         * Enum.forString(s); // returns the enum value for a string
         * Enum.forInt(i); // returns the enum value for an int
         * </pre>
         * Enumeration objects are immutable singleton objects that
         * can be compared using == object equality. They have no
         * public constructor. See the constants defined within this
         * class for all the valid values.
         */
        static final class Enum extends org.apache.xmlbeans.StringEnumAbstractBase
        {
            /**
             * Returns the enum value for a string, or null if none.
             */
            public static Enum forString(java.lang.String s)
                { return (Enum)table.forString(s); }
            /**
             * Returns the enum value corresponding to an int, or null if none.
             */
            public static Enum forInt(int i)
                { return (Enum)table.forInt(i); }
            
            private Enum(java.lang.String s, int i)
                { super(s, i); }
            
            static final int INT_X = 1;
            static final int INT_NOT_RETAIL = 2;
            static final int INT_NOT_CA_RETAIL = 3;
            static final int INT_CA_RETAIL = 4;
            
            public static final org.apache.xmlbeans.StringEnumAbstractBase.Table table =
                new org.apache.xmlbeans.StringEnumAbstractBase.Table
            (
                new Enum[]
                {
                    new Enum("", INT_X),
                    new Enum("NOT_RETAIL", INT_NOT_RETAIL),
                    new Enum("NOT_CA_RETAIL", INT_NOT_CA_RETAIL),
                    new Enum("CA_RETAIL", INT_CA_RETAIL),
                }
            );
            private static final long serialVersionUID = 1L;
            private java.lang.Object readResolve() { return forInt(intValue()); } 
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.idscore.request.CARetailFlagDocument.CARetailFlag newValue(java.lang.Object obj) {
              return (com.idanalytics.products.idscore.request.CARetailFlagDocument.CARetailFlag) type.newValue( obj ); }
            
            public static com.idanalytics.products.idscore.request.CARetailFlagDocument.CARetailFlag newInstance() {
              return (com.idanalytics.products.idscore.request.CARetailFlagDocument.CARetailFlag) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.idscore.request.CARetailFlagDocument.CARetailFlag newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.idscore.request.CARetailFlagDocument.CARetailFlag) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.idscore.request.CARetailFlagDocument newInstance() {
          return (com.idanalytics.products.idscore.request.CARetailFlagDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.idscore.request.CARetailFlagDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.idscore.request.CARetailFlagDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.idscore.request.CARetailFlagDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.CARetailFlagDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.idscore.request.CARetailFlagDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.CARetailFlagDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.idscore.request.CARetailFlagDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.CARetailFlagDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.idscore.request.CARetailFlagDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.CARetailFlagDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.idscore.request.CARetailFlagDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.CARetailFlagDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.idscore.request.CARetailFlagDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.CARetailFlagDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.idscore.request.CARetailFlagDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.CARetailFlagDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.idscore.request.CARetailFlagDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.CARetailFlagDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.idscore.request.CARetailFlagDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.CARetailFlagDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.idscore.request.CARetailFlagDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.CARetailFlagDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.idscore.request.CARetailFlagDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.CARetailFlagDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.idscore.request.CARetailFlagDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.CARetailFlagDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.idscore.request.CARetailFlagDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.CARetailFlagDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.idscore.request.CARetailFlagDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.CARetailFlagDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.request.CARetailFlagDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.request.CARetailFlagDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.request.CARetailFlagDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.request.CARetailFlagDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
