/*
 * An XML document type.
 * Localname: OrderAmount
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.OrderAmountDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * A document containing one OrderAmount(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public class OrderAmountDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.OrderAmountDocument
{
    private static final long serialVersionUID = 1L;
    
    public OrderAmountDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ORDERAMOUNT$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "OrderAmount");
    
    
    /**
     * Gets the "OrderAmount" element
     */
    public java.math.BigDecimal getOrderAmount()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ORDERAMOUNT$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getBigDecimalValue();
        }
    }
    
    /**
     * Gets (as xml) the "OrderAmount" element
     */
    public com.idanalytics.products.idscore.request.OrderAmountDocument.OrderAmount xgetOrderAmount()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.OrderAmountDocument.OrderAmount target = null;
            target = (com.idanalytics.products.idscore.request.OrderAmountDocument.OrderAmount)get_store().find_element_user(ORDERAMOUNT$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "OrderAmount" element
     */
    public void setOrderAmount(java.math.BigDecimal orderAmount)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ORDERAMOUNT$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ORDERAMOUNT$0);
            }
            target.setBigDecimalValue(orderAmount);
        }
    }
    
    /**
     * Sets (as xml) the "OrderAmount" element
     */
    public void xsetOrderAmount(com.idanalytics.products.idscore.request.OrderAmountDocument.OrderAmount orderAmount)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.OrderAmountDocument.OrderAmount target = null;
            target = (com.idanalytics.products.idscore.request.OrderAmountDocument.OrderAmount)get_store().find_element_user(ORDERAMOUNT$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.OrderAmountDocument.OrderAmount)get_store().add_element_user(ORDERAMOUNT$0);
            }
            target.set(orderAmount);
        }
    }
    /**
     * An XML OrderAmount(@http://idanalytics.com/products/idscore/request).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.OrderAmountDocument$OrderAmount.
     */
    public static class OrderAmountImpl extends org.apache.xmlbeans.impl.values.JavaDecimalHolderEx implements com.idanalytics.products.idscore.request.OrderAmountDocument.OrderAmount
    {
        private static final long serialVersionUID = 1L;
        
        public OrderAmountImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected OrderAmountImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
