/*
 * An XML document type.
 * Localname: ApplicationDate
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.ApplicationDateDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * A document containing one ApplicationDate(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public class ApplicationDateDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.ApplicationDateDocument
{
    private static final long serialVersionUID = 1L;
    
    public ApplicationDateDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName APPLICATIONDATE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ApplicationDate");
    
    
    /**
     * Gets the "ApplicationDate" element
     */
    public java.util.Calendar getApplicationDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(APPLICATIONDATE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getCalendarValue();
        }
    }
    
    /**
     * Gets (as xml) the "ApplicationDate" element
     */
    public com.idanalytics.products.idscore.request.ApplicationDateDocument.ApplicationDate xgetApplicationDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ApplicationDateDocument.ApplicationDate target = null;
            target = (com.idanalytics.products.idscore.request.ApplicationDateDocument.ApplicationDate)get_store().find_element_user(APPLICATIONDATE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "ApplicationDate" element
     */
    public void setApplicationDate(java.util.Calendar applicationDate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(APPLICATIONDATE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(APPLICATIONDATE$0);
            }
            target.setCalendarValue(applicationDate);
        }
    }
    
    /**
     * Sets (as xml) the "ApplicationDate" element
     */
    public void xsetApplicationDate(com.idanalytics.products.idscore.request.ApplicationDateDocument.ApplicationDate applicationDate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ApplicationDateDocument.ApplicationDate target = null;
            target = (com.idanalytics.products.idscore.request.ApplicationDateDocument.ApplicationDate)get_store().find_element_user(APPLICATIONDATE$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.ApplicationDateDocument.ApplicationDate)get_store().add_element_user(APPLICATIONDATE$0);
            }
            target.set(applicationDate);
        }
    }
    /**
     * An XML ApplicationDate(@http://idanalytics.com/products/idscore/request).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.ApplicationDateDocument$ApplicationDate.
     */
    public static class ApplicationDateImpl extends org.apache.xmlbeans.impl.values.JavaGDateHolderEx implements com.idanalytics.products.idscore.request.ApplicationDateDocument.ApplicationDate
    {
        private static final long serialVersionUID = 1L;
        
        public ApplicationDateImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected ApplicationDateImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
