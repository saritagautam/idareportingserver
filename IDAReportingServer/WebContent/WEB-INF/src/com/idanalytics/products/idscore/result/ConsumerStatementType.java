/*
 * XML Type:  ConsumerStatementType
 * Namespace: http://idanalytics.com/products/idscore/result
 * Java type: com.idanalytics.products.idscore.result.ConsumerStatementType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.result;


/**
 * An XML ConsumerStatementType(@http://idanalytics.com/products/idscore/result).
 *
 * This is a complex type.
 */
public interface ConsumerStatementType extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ConsumerStatementType.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("consumerstatementtype5453type");
    
    /**
     * Gets the "StatementDate" element
     */
    java.lang.String getStatementDate();
    
    /**
     * Gets (as xml) the "StatementDate" element
     */
    com.idanalytics.products.idscore.result.ConsumerStatementType.StatementDate xgetStatementDate();
    
    /**
     * Sets the "StatementDate" element
     */
    void setStatementDate(java.lang.String statementDate);
    
    /**
     * Sets (as xml) the "StatementDate" element
     */
    void xsetStatementDate(com.idanalytics.products.idscore.result.ConsumerStatementType.StatementDate statementDate);
    
    /**
     * Gets the "StatementType" element
     */
    java.lang.String getStatementType();
    
    /**
     * Gets (as xml) the "StatementType" element
     */
    com.idanalytics.products.idscore.result.ConsumerStatementType.StatementType xgetStatementType();
    
    /**
     * Sets the "StatementType" element
     */
    void setStatementType(java.lang.String statementType);
    
    /**
     * Sets (as xml) the "StatementType" element
     */
    void xsetStatementType(com.idanalytics.products.idscore.result.ConsumerStatementType.StatementType statementType);
    
    /**
     * Gets the "StatementSubtypeCode" element
     */
    java.lang.String getStatementSubtypeCode();
    
    /**
     * Gets (as xml) the "StatementSubtypeCode" element
     */
    com.idanalytics.products.idscore.result.ConsumerStatementType.StatementSubtypeCode xgetStatementSubtypeCode();
    
    /**
     * Sets the "StatementSubtypeCode" element
     */
    void setStatementSubtypeCode(java.lang.String statementSubtypeCode);
    
    /**
     * Sets (as xml) the "StatementSubtypeCode" element
     */
    void xsetStatementSubtypeCode(com.idanalytics.products.idscore.result.ConsumerStatementType.StatementSubtypeCode statementSubtypeCode);
    
    /**
     * Gets the "Statement" element
     */
    java.lang.String getStatement();
    
    /**
     * Gets (as xml) the "Statement" element
     */
    com.idanalytics.products.idscore.result.ConsumerStatementType.Statement xgetStatement();
    
    /**
     * Sets the "Statement" element
     */
    void setStatement(java.lang.String statement);
    
    /**
     * Sets (as xml) the "Statement" element
     */
    void xsetStatement(com.idanalytics.products.idscore.result.ConsumerStatementType.Statement statement);
    
    /**
     * An XML StatementDate(@http://idanalytics.com/products/idscore/result).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result.ConsumerStatementType$StatementDate.
     */
    public interface StatementDate extends org.apache.xmlbeans.XmlString
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(StatementDate.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("statementdate142aelemtype");
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.idscore.result.ConsumerStatementType.StatementDate newValue(java.lang.Object obj) {
              return (com.idanalytics.products.idscore.result.ConsumerStatementType.StatementDate) type.newValue( obj ); }
            
            public static com.idanalytics.products.idscore.result.ConsumerStatementType.StatementDate newInstance() {
              return (com.idanalytics.products.idscore.result.ConsumerStatementType.StatementDate) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.idscore.result.ConsumerStatementType.StatementDate newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.idscore.result.ConsumerStatementType.StatementDate) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * An XML StatementType(@http://idanalytics.com/products/idscore/result).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result.ConsumerStatementType$StatementType.
     */
    public interface StatementType extends org.apache.xmlbeans.XmlString
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(StatementType.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("statementtype7f1eelemtype");
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.idscore.result.ConsumerStatementType.StatementType newValue(java.lang.Object obj) {
              return (com.idanalytics.products.idscore.result.ConsumerStatementType.StatementType) type.newValue( obj ); }
            
            public static com.idanalytics.products.idscore.result.ConsumerStatementType.StatementType newInstance() {
              return (com.idanalytics.products.idscore.result.ConsumerStatementType.StatementType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.idscore.result.ConsumerStatementType.StatementType newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.idscore.result.ConsumerStatementType.StatementType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * An XML StatementSubtypeCode(@http://idanalytics.com/products/idscore/result).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result.ConsumerStatementType$StatementSubtypeCode.
     */
    public interface StatementSubtypeCode extends org.apache.xmlbeans.XmlString
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(StatementSubtypeCode.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("statementsubtypecode577felemtype");
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.idscore.result.ConsumerStatementType.StatementSubtypeCode newValue(java.lang.Object obj) {
              return (com.idanalytics.products.idscore.result.ConsumerStatementType.StatementSubtypeCode) type.newValue( obj ); }
            
            public static com.idanalytics.products.idscore.result.ConsumerStatementType.StatementSubtypeCode newInstance() {
              return (com.idanalytics.products.idscore.result.ConsumerStatementType.StatementSubtypeCode) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.idscore.result.ConsumerStatementType.StatementSubtypeCode newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.idscore.result.ConsumerStatementType.StatementSubtypeCode) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * An XML Statement(@http://idanalytics.com/products/idscore/result).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result.ConsumerStatementType$Statement.
     */
    public interface Statement extends org.apache.xmlbeans.XmlString
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Statement.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("statementa838elemtype");
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.idscore.result.ConsumerStatementType.Statement newValue(java.lang.Object obj) {
              return (com.idanalytics.products.idscore.result.ConsumerStatementType.Statement) type.newValue( obj ); }
            
            public static com.idanalytics.products.idscore.result.ConsumerStatementType.Statement newInstance() {
              return (com.idanalytics.products.idscore.result.ConsumerStatementType.Statement) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.idscore.result.ConsumerStatementType.Statement newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.idscore.result.ConsumerStatementType.Statement) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.idscore.result.ConsumerStatementType newInstance() {
          return (com.idanalytics.products.idscore.result.ConsumerStatementType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.idscore.result.ConsumerStatementType newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.idscore.result.ConsumerStatementType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.idscore.result.ConsumerStatementType parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result.ConsumerStatementType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.idscore.result.ConsumerStatementType parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result.ConsumerStatementType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.idscore.result.ConsumerStatementType parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result.ConsumerStatementType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.idscore.result.ConsumerStatementType parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result.ConsumerStatementType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.idscore.result.ConsumerStatementType parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result.ConsumerStatementType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.idscore.result.ConsumerStatementType parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result.ConsumerStatementType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.idscore.result.ConsumerStatementType parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result.ConsumerStatementType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.idscore.result.ConsumerStatementType parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result.ConsumerStatementType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.idscore.result.ConsumerStatementType parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result.ConsumerStatementType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.idscore.result.ConsumerStatementType parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.result.ConsumerStatementType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.idscore.result.ConsumerStatementType parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result.ConsumerStatementType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.idscore.result.ConsumerStatementType parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result.ConsumerStatementType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.idscore.result.ConsumerStatementType parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result.ConsumerStatementType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.idscore.result.ConsumerStatementType parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.result.ConsumerStatementType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.result.ConsumerStatementType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.result.ConsumerStatementType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.result.ConsumerStatementType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.result.ConsumerStatementType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
