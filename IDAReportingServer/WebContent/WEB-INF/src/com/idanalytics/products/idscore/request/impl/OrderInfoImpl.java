/*
 * XML Type:  OrderInfo
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.OrderInfo
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML OrderInfo(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public class OrderInfoImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.OrderInfo
{
    private static final long serialVersionUID = 1L;
    
    public OrderInfoImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName MERCHANTID$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "MerchantID");
    private static final javax.xml.namespace.QName ORDERDATE$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "OrderDate");
    private static final javax.xml.namespace.QName ORDERNUMBER$4 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "OrderNumber");
    private static final javax.xml.namespace.QName ORDERAMOUNT$6 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "OrderAmount");
    private static final javax.xml.namespace.QName ORDERTAX$8 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "OrderTax");
    
    
    /**
     * Gets the "MerchantID" element
     */
    public java.lang.String getMerchantID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MERCHANTID$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "MerchantID" element
     */
    public com.idanalytics.products.idscore.request.MerchantIDDocument.MerchantID xgetMerchantID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.MerchantIDDocument.MerchantID target = null;
            target = (com.idanalytics.products.idscore.request.MerchantIDDocument.MerchantID)get_store().find_element_user(MERCHANTID$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "MerchantID" element
     */
    public void setMerchantID(java.lang.String merchantID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MERCHANTID$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(MERCHANTID$0);
            }
            target.setStringValue(merchantID);
        }
    }
    
    /**
     * Sets (as xml) the "MerchantID" element
     */
    public void xsetMerchantID(com.idanalytics.products.idscore.request.MerchantIDDocument.MerchantID merchantID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.MerchantIDDocument.MerchantID target = null;
            target = (com.idanalytics.products.idscore.request.MerchantIDDocument.MerchantID)get_store().find_element_user(MERCHANTID$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.MerchantIDDocument.MerchantID)get_store().add_element_user(MERCHANTID$0);
            }
            target.set(merchantID);
        }
    }
    
    /**
     * Gets the "OrderDate" element
     */
    public java.util.Calendar getOrderDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ORDERDATE$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getCalendarValue();
        }
    }
    
    /**
     * Gets (as xml) the "OrderDate" element
     */
    public org.apache.xmlbeans.XmlDateTime xgetOrderDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlDateTime target = null;
            target = (org.apache.xmlbeans.XmlDateTime)get_store().find_element_user(ORDERDATE$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "OrderDate" element
     */
    public void setOrderDate(java.util.Calendar orderDate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ORDERDATE$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ORDERDATE$2);
            }
            target.setCalendarValue(orderDate);
        }
    }
    
    /**
     * Sets (as xml) the "OrderDate" element
     */
    public void xsetOrderDate(org.apache.xmlbeans.XmlDateTime orderDate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlDateTime target = null;
            target = (org.apache.xmlbeans.XmlDateTime)get_store().find_element_user(ORDERDATE$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlDateTime)get_store().add_element_user(ORDERDATE$2);
            }
            target.set(orderDate);
        }
    }
    
    /**
     * Gets the "OrderNumber" element
     */
    public java.lang.String getOrderNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ORDERNUMBER$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "OrderNumber" element
     */
    public com.idanalytics.products.idscore.request.OrderNumberDocument.OrderNumber xgetOrderNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.OrderNumberDocument.OrderNumber target = null;
            target = (com.idanalytics.products.idscore.request.OrderNumberDocument.OrderNumber)get_store().find_element_user(ORDERNUMBER$4, 0);
            return target;
        }
    }
    
    /**
     * True if has "OrderNumber" element
     */
    public boolean isSetOrderNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ORDERNUMBER$4) != 0;
        }
    }
    
    /**
     * Sets the "OrderNumber" element
     */
    public void setOrderNumber(java.lang.String orderNumber)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ORDERNUMBER$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ORDERNUMBER$4);
            }
            target.setStringValue(orderNumber);
        }
    }
    
    /**
     * Sets (as xml) the "OrderNumber" element
     */
    public void xsetOrderNumber(com.idanalytics.products.idscore.request.OrderNumberDocument.OrderNumber orderNumber)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.OrderNumberDocument.OrderNumber target = null;
            target = (com.idanalytics.products.idscore.request.OrderNumberDocument.OrderNumber)get_store().find_element_user(ORDERNUMBER$4, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.OrderNumberDocument.OrderNumber)get_store().add_element_user(ORDERNUMBER$4);
            }
            target.set(orderNumber);
        }
    }
    
    /**
     * Unsets the "OrderNumber" element
     */
    public void unsetOrderNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ORDERNUMBER$4, 0);
        }
    }
    
    /**
     * Gets the "OrderAmount" element
     */
    public java.math.BigDecimal getOrderAmount()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ORDERAMOUNT$6, 0);
            if (target == null)
            {
                return null;
            }
            return target.getBigDecimalValue();
        }
    }
    
    /**
     * Gets (as xml) the "OrderAmount" element
     */
    public com.idanalytics.products.idscore.request.OrderAmountDocument.OrderAmount xgetOrderAmount()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.OrderAmountDocument.OrderAmount target = null;
            target = (com.idanalytics.products.idscore.request.OrderAmountDocument.OrderAmount)get_store().find_element_user(ORDERAMOUNT$6, 0);
            return target;
        }
    }
    
    /**
     * Sets the "OrderAmount" element
     */
    public void setOrderAmount(java.math.BigDecimal orderAmount)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ORDERAMOUNT$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ORDERAMOUNT$6);
            }
            target.setBigDecimalValue(orderAmount);
        }
    }
    
    /**
     * Sets (as xml) the "OrderAmount" element
     */
    public void xsetOrderAmount(com.idanalytics.products.idscore.request.OrderAmountDocument.OrderAmount orderAmount)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.OrderAmountDocument.OrderAmount target = null;
            target = (com.idanalytics.products.idscore.request.OrderAmountDocument.OrderAmount)get_store().find_element_user(ORDERAMOUNT$6, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.OrderAmountDocument.OrderAmount)get_store().add_element_user(ORDERAMOUNT$6);
            }
            target.set(orderAmount);
        }
    }
    
    /**
     * Gets the "OrderTax" element
     */
    public java.lang.Object getOrderTax()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ORDERTAX$8, 0);
            if (target == null)
            {
                return null;
            }
            return target.getObjectValue();
        }
    }
    
    /**
     * Gets (as xml) the "OrderTax" element
     */
    public com.idanalytics.products.common_v1.OptionalDecimal xgetOrderTax()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.OptionalDecimal target = null;
            target = (com.idanalytics.products.common_v1.OptionalDecimal)get_store().find_element_user(ORDERTAX$8, 0);
            return target;
        }
    }
    
    /**
     * True if has "OrderTax" element
     */
    public boolean isSetOrderTax()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ORDERTAX$8) != 0;
        }
    }
    
    /**
     * Sets the "OrderTax" element
     */
    public void setOrderTax(java.lang.Object orderTax)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ORDERTAX$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ORDERTAX$8);
            }
            target.setObjectValue(orderTax);
        }
    }
    
    /**
     * Sets (as xml) the "OrderTax" element
     */
    public void xsetOrderTax(com.idanalytics.products.common_v1.OptionalDecimal orderTax)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.OptionalDecimal target = null;
            target = (com.idanalytics.products.common_v1.OptionalDecimal)get_store().find_element_user(ORDERTAX$8, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.OptionalDecimal)get_store().add_element_user(ORDERTAX$8);
            }
            target.set(orderTax);
        }
    }
    
    /**
     * Unsets the "OrderTax" element
     */
    public void unsetOrderTax()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ORDERTAX$8, 0);
        }
    }
}
