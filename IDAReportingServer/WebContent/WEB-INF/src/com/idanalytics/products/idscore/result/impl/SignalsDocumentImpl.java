/*
 * An XML document type.
 * Localname: Signals
 * Namespace: http://idanalytics.com/products/idscore/result
 * Java type: com.idanalytics.products.idscore.result.SignalsDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.result.impl;
/**
 * A document containing one Signals(@http://idanalytics.com/products/idscore/result) element.
 *
 * This is a complex type.
 */
public class SignalsDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.result.SignalsDocument
{
    private static final long serialVersionUID = 1L;
    
    public SignalsDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SIGNALS$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "Signals");
    
    
    /**
     * Gets the "Signals" element
     */
    public com.idanalytics.products.idscore.result.SignalsDocument.Signals getSignals()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.SignalsDocument.Signals target = null;
            target = (com.idanalytics.products.idscore.result.SignalsDocument.Signals)get_store().find_element_user(SIGNALS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "Signals" element
     */
    public void setSignals(com.idanalytics.products.idscore.result.SignalsDocument.Signals signals)
    {
        generatedSetterHelperImpl(signals, SIGNALS$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "Signals" element
     */
    public com.idanalytics.products.idscore.result.SignalsDocument.Signals addNewSignals()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.SignalsDocument.Signals target = null;
            target = (com.idanalytics.products.idscore.result.SignalsDocument.Signals)get_store().add_element_user(SIGNALS$0);
            return target;
        }
    }
    /**
     * An XML Signals(@http://idanalytics.com/products/idscore/result).
     *
     * This is a complex type.
     */
    public static class SignalsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.result.SignalsDocument.Signals
    {
        private static final long serialVersionUID = 1L;
        
        public SignalsImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName ADDRESSRISK$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "AddressRisk");
        private static final javax.xml.namespace.QName PHONERISK$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "PhoneRisk");
        private static final javax.xml.namespace.QName EMAILRISK$4 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "EmailRisk");
        private static final javax.xml.namespace.QName SSNINSIGHT$6 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "SSNInsight");
        private static final javax.xml.namespace.QName FRAUDRISKTYPE$8 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "FraudRiskType");
        private static final javax.xml.namespace.QName FRAUDRING$10 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "FraudRing");
        private static final javax.xml.namespace.QName PIN$12 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "PIN");
        
        
        /**
         * Gets the "AddressRisk" element
         */
        public java.lang.String getAddressRisk()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ADDRESSRISK$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "AddressRisk" element
         */
        public com.idanalytics.products.idscore.result.SignalsDocument.Signals.AddressRisk xgetAddressRisk()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.SignalsDocument.Signals.AddressRisk target = null;
                target = (com.idanalytics.products.idscore.result.SignalsDocument.Signals.AddressRisk)get_store().find_element_user(ADDRESSRISK$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "AddressRisk" element
         */
        public void setAddressRisk(java.lang.String addressRisk)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ADDRESSRISK$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ADDRESSRISK$0);
                }
                target.setStringValue(addressRisk);
            }
        }
        
        /**
         * Sets (as xml) the "AddressRisk" element
         */
        public void xsetAddressRisk(com.idanalytics.products.idscore.result.SignalsDocument.Signals.AddressRisk addressRisk)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.SignalsDocument.Signals.AddressRisk target = null;
                target = (com.idanalytics.products.idscore.result.SignalsDocument.Signals.AddressRisk)get_store().find_element_user(ADDRESSRISK$0, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result.SignalsDocument.Signals.AddressRisk)get_store().add_element_user(ADDRESSRISK$0);
                }
                target.set(addressRisk);
            }
        }
        
        /**
         * Gets the "PhoneRisk" element
         */
        public java.lang.String getPhoneRisk()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PHONERISK$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PhoneRisk" element
         */
        public com.idanalytics.products.idscore.result.SignalsDocument.Signals.PhoneRisk xgetPhoneRisk()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.SignalsDocument.Signals.PhoneRisk target = null;
                target = (com.idanalytics.products.idscore.result.SignalsDocument.Signals.PhoneRisk)get_store().find_element_user(PHONERISK$2, 0);
                return target;
            }
        }
        
        /**
         * Sets the "PhoneRisk" element
         */
        public void setPhoneRisk(java.lang.String phoneRisk)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PHONERISK$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PHONERISK$2);
                }
                target.setStringValue(phoneRisk);
            }
        }
        
        /**
         * Sets (as xml) the "PhoneRisk" element
         */
        public void xsetPhoneRisk(com.idanalytics.products.idscore.result.SignalsDocument.Signals.PhoneRisk phoneRisk)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.SignalsDocument.Signals.PhoneRisk target = null;
                target = (com.idanalytics.products.idscore.result.SignalsDocument.Signals.PhoneRisk)get_store().find_element_user(PHONERISK$2, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result.SignalsDocument.Signals.PhoneRisk)get_store().add_element_user(PHONERISK$2);
                }
                target.set(phoneRisk);
            }
        }
        
        /**
         * Gets the "EmailRisk" element
         */
        public java.lang.String getEmailRisk()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EMAILRISK$4, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "EmailRisk" element
         */
        public com.idanalytics.products.idscore.result.SignalsDocument.Signals.EmailRisk xgetEmailRisk()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.SignalsDocument.Signals.EmailRisk target = null;
                target = (com.idanalytics.products.idscore.result.SignalsDocument.Signals.EmailRisk)get_store().find_element_user(EMAILRISK$4, 0);
                return target;
            }
        }
        
        /**
         * Sets the "EmailRisk" element
         */
        public void setEmailRisk(java.lang.String emailRisk)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EMAILRISK$4, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(EMAILRISK$4);
                }
                target.setStringValue(emailRisk);
            }
        }
        
        /**
         * Sets (as xml) the "EmailRisk" element
         */
        public void xsetEmailRisk(com.idanalytics.products.idscore.result.SignalsDocument.Signals.EmailRisk emailRisk)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.SignalsDocument.Signals.EmailRisk target = null;
                target = (com.idanalytics.products.idscore.result.SignalsDocument.Signals.EmailRisk)get_store().find_element_user(EMAILRISK$4, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result.SignalsDocument.Signals.EmailRisk)get_store().add_element_user(EMAILRISK$4);
                }
                target.set(emailRisk);
            }
        }
        
        /**
         * Gets the "SSNInsight" element
         */
        public java.lang.String getSSNInsight()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SSNINSIGHT$6, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "SSNInsight" element
         */
        public com.idanalytics.products.idscore.result.SignalsDocument.Signals.SSNInsight xgetSSNInsight()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.SignalsDocument.Signals.SSNInsight target = null;
                target = (com.idanalytics.products.idscore.result.SignalsDocument.Signals.SSNInsight)get_store().find_element_user(SSNINSIGHT$6, 0);
                return target;
            }
        }
        
        /**
         * Sets the "SSNInsight" element
         */
        public void setSSNInsight(java.lang.String ssnInsight)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SSNINSIGHT$6, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SSNINSIGHT$6);
                }
                target.setStringValue(ssnInsight);
            }
        }
        
        /**
         * Sets (as xml) the "SSNInsight" element
         */
        public void xsetSSNInsight(com.idanalytics.products.idscore.result.SignalsDocument.Signals.SSNInsight ssnInsight)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.SignalsDocument.Signals.SSNInsight target = null;
                target = (com.idanalytics.products.idscore.result.SignalsDocument.Signals.SSNInsight)get_store().find_element_user(SSNINSIGHT$6, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result.SignalsDocument.Signals.SSNInsight)get_store().add_element_user(SSNINSIGHT$6);
                }
                target.set(ssnInsight);
            }
        }
        
        /**
         * Gets the "FraudRiskType" element
         */
        public java.lang.String getFraudRiskType()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FRAUDRISKTYPE$8, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "FraudRiskType" element
         */
        public com.idanalytics.products.idscore.result.SignalsDocument.Signals.FraudRiskType xgetFraudRiskType()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.SignalsDocument.Signals.FraudRiskType target = null;
                target = (com.idanalytics.products.idscore.result.SignalsDocument.Signals.FraudRiskType)get_store().find_element_user(FRAUDRISKTYPE$8, 0);
                return target;
            }
        }
        
        /**
         * Sets the "FraudRiskType" element
         */
        public void setFraudRiskType(java.lang.String fraudRiskType)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FRAUDRISKTYPE$8, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(FRAUDRISKTYPE$8);
                }
                target.setStringValue(fraudRiskType);
            }
        }
        
        /**
         * Sets (as xml) the "FraudRiskType" element
         */
        public void xsetFraudRiskType(com.idanalytics.products.idscore.result.SignalsDocument.Signals.FraudRiskType fraudRiskType)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.SignalsDocument.Signals.FraudRiskType target = null;
                target = (com.idanalytics.products.idscore.result.SignalsDocument.Signals.FraudRiskType)get_store().find_element_user(FRAUDRISKTYPE$8, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result.SignalsDocument.Signals.FraudRiskType)get_store().add_element_user(FRAUDRISKTYPE$8);
                }
                target.set(fraudRiskType);
            }
        }
        
        /**
         * Gets the "FraudRing" element
         */
        public java.lang.String getFraudRing()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FRAUDRING$10, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "FraudRing" element
         */
        public com.idanalytics.products.idscore.result.SignalsDocument.Signals.FraudRing xgetFraudRing()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.SignalsDocument.Signals.FraudRing target = null;
                target = (com.idanalytics.products.idscore.result.SignalsDocument.Signals.FraudRing)get_store().find_element_user(FRAUDRING$10, 0);
                return target;
            }
        }
        
        /**
         * Sets the "FraudRing" element
         */
        public void setFraudRing(java.lang.String fraudRing)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FRAUDRING$10, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(FRAUDRING$10);
                }
                target.setStringValue(fraudRing);
            }
        }
        
        /**
         * Sets (as xml) the "FraudRing" element
         */
        public void xsetFraudRing(com.idanalytics.products.idscore.result.SignalsDocument.Signals.FraudRing fraudRing)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.SignalsDocument.Signals.FraudRing target = null;
                target = (com.idanalytics.products.idscore.result.SignalsDocument.Signals.FraudRing)get_store().find_element_user(FRAUDRING$10, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result.SignalsDocument.Signals.FraudRing)get_store().add_element_user(FRAUDRING$10);
                }
                target.set(fraudRing);
            }
        }
        
        /**
         * Gets the "PIN" element
         */
        public java.lang.String getPIN()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PIN$12, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PIN" element
         */
        public com.idanalytics.products.idscore.result.SignalsDocument.Signals.PIN xgetPIN()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.SignalsDocument.Signals.PIN target = null;
                target = (com.idanalytics.products.idscore.result.SignalsDocument.Signals.PIN)get_store().find_element_user(PIN$12, 0);
                return target;
            }
        }
        
        /**
         * True if has "PIN" element
         */
        public boolean isSetPIN()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PIN$12) != 0;
            }
        }
        
        /**
         * Sets the "PIN" element
         */
        public void setPIN(java.lang.String pin)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PIN$12, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PIN$12);
                }
                target.setStringValue(pin);
            }
        }
        
        /**
         * Sets (as xml) the "PIN" element
         */
        public void xsetPIN(com.idanalytics.products.idscore.result.SignalsDocument.Signals.PIN pin)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.SignalsDocument.Signals.PIN target = null;
                target = (com.idanalytics.products.idscore.result.SignalsDocument.Signals.PIN)get_store().find_element_user(PIN$12, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result.SignalsDocument.Signals.PIN)get_store().add_element_user(PIN$12);
                }
                target.set(pin);
            }
        }
        
        /**
         * Unsets the "PIN" element
         */
        public void unsetPIN()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PIN$12, 0);
            }
        }
        /**
         * An XML AddressRisk(@http://idanalytics.com/products/idscore/result).
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result.SignalsDocument$Signals$AddressRisk.
         */
        public static class AddressRiskImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.result.SignalsDocument.Signals.AddressRisk
        {
            private static final long serialVersionUID = 1L;
            
            public AddressRiskImpl(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected AddressRiskImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
        /**
         * An XML PhoneRisk(@http://idanalytics.com/products/idscore/result).
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result.SignalsDocument$Signals$PhoneRisk.
         */
        public static class PhoneRiskImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.result.SignalsDocument.Signals.PhoneRisk
        {
            private static final long serialVersionUID = 1L;
            
            public PhoneRiskImpl(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected PhoneRiskImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
        /**
         * An XML EmailRisk(@http://idanalytics.com/products/idscore/result).
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result.SignalsDocument$Signals$EmailRisk.
         */
        public static class EmailRiskImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.result.SignalsDocument.Signals.EmailRisk
        {
            private static final long serialVersionUID = 1L;
            
            public EmailRiskImpl(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected EmailRiskImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
        /**
         * An XML SSNInsight(@http://idanalytics.com/products/idscore/result).
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result.SignalsDocument$Signals$SSNInsight.
         */
        public static class SSNInsightImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.result.SignalsDocument.Signals.SSNInsight
        {
            private static final long serialVersionUID = 1L;
            
            public SSNInsightImpl(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected SSNInsightImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
        /**
         * An XML FraudRiskType(@http://idanalytics.com/products/idscore/result).
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result.SignalsDocument$Signals$FraudRiskType.
         */
        public static class FraudRiskTypeImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.result.SignalsDocument.Signals.FraudRiskType
        {
            private static final long serialVersionUID = 1L;
            
            public FraudRiskTypeImpl(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected FraudRiskTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
        /**
         * An XML FraudRing(@http://idanalytics.com/products/idscore/result).
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result.SignalsDocument$Signals$FraudRing.
         */
        public static class FraudRingImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.result.SignalsDocument.Signals.FraudRing
        {
            private static final long serialVersionUID = 1L;
            
            public FraudRingImpl(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected FraudRingImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
        /**
         * An XML PIN(@http://idanalytics.com/products/idscore/result).
         *
         * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result.SignalsDocument$Signals$PIN.
         */
        public static class PINImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.result.SignalsDocument.Signals.PIN
        {
            private static final long serialVersionUID = 1L;
            
            public PINImpl(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType, false);
            }
            
            protected PINImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
            {
                super(sType, b);
            }
        }
    }
}
