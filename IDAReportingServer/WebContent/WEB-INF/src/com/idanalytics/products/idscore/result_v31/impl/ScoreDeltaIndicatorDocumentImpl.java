/*
 * An XML document type.
 * Localname: ScoreDeltaIndicator
 * Namespace: http://idanalytics.com/products/idscore/result.v31
 * Java type: com.idanalytics.products.idscore.result_v31.ScoreDeltaIndicatorDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.result_v31.impl;
/**
 * A document containing one ScoreDeltaIndicator(@http://idanalytics.com/products/idscore/result.v31) element.
 *
 * This is a complex type.
 */
public class ScoreDeltaIndicatorDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.result_v31.ScoreDeltaIndicatorDocument
{
    private static final long serialVersionUID = 1L;
    
    public ScoreDeltaIndicatorDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SCOREDELTAINDICATOR$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result.v31", "ScoreDeltaIndicator");
    
    
    /**
     * Gets the "ScoreDeltaIndicator" element
     */
    public com.idanalytics.products.idscore.result_v31.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator.Enum getScoreDeltaIndicator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SCOREDELTAINDICATOR$0, 0);
            if (target == null)
            {
                return null;
            }
            return (com.idanalytics.products.idscore.result_v31.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "ScoreDeltaIndicator" element
     */
    public com.idanalytics.products.idscore.result_v31.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator xgetScoreDeltaIndicator()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result_v31.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator target = null;
            target = (com.idanalytics.products.idscore.result_v31.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator)get_store().find_element_user(SCOREDELTAINDICATOR$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "ScoreDeltaIndicator" element
     */
    public void setScoreDeltaIndicator(com.idanalytics.products.idscore.result_v31.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator.Enum scoreDeltaIndicator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SCOREDELTAINDICATOR$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SCOREDELTAINDICATOR$0);
            }
            target.setEnumValue(scoreDeltaIndicator);
        }
    }
    
    /**
     * Sets (as xml) the "ScoreDeltaIndicator" element
     */
    public void xsetScoreDeltaIndicator(com.idanalytics.products.idscore.result_v31.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator scoreDeltaIndicator)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result_v31.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator target = null;
            target = (com.idanalytics.products.idscore.result_v31.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator)get_store().find_element_user(SCOREDELTAINDICATOR$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.result_v31.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator)get_store().add_element_user(SCOREDELTAINDICATOR$0);
            }
            target.set(scoreDeltaIndicator);
        }
    }
    /**
     * An XML ScoreDeltaIndicator(@http://idanalytics.com/products/idscore/result.v31).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result_v31.ScoreDeltaIndicatorDocument$ScoreDeltaIndicator.
     */
    public static class ScoreDeltaIndicatorImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.idanalytics.products.idscore.result_v31.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator
    {
        private static final long serialVersionUID = 1L;
        
        public ScoreDeltaIndicatorImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected ScoreDeltaIndicatorImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
