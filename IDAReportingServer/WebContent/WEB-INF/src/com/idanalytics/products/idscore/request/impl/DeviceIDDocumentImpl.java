/*
 * An XML document type.
 * Localname: DeviceID
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.DeviceIDDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * A document containing one DeviceID(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public class DeviceIDDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.DeviceIDDocument
{
    private static final long serialVersionUID = 1L;
    
    public DeviceIDDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName DEVICEID$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "DeviceID");
    
    
    /**
     * Gets the "DeviceID" element
     */
    public java.lang.String getDeviceID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DEVICEID$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "DeviceID" element
     */
    public com.idanalytics.products.idscore.request.DeviceIDDocument.DeviceID xgetDeviceID()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.DeviceIDDocument.DeviceID target = null;
            target = (com.idanalytics.products.idscore.request.DeviceIDDocument.DeviceID)get_store().find_element_user(DEVICEID$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "DeviceID" element
     */
    public void setDeviceID(java.lang.String deviceID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DEVICEID$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DEVICEID$0);
            }
            target.setStringValue(deviceID);
        }
    }
    
    /**
     * Sets (as xml) the "DeviceID" element
     */
    public void xsetDeviceID(com.idanalytics.products.idscore.request.DeviceIDDocument.DeviceID deviceID)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.DeviceIDDocument.DeviceID target = null;
            target = (com.idanalytics.products.idscore.request.DeviceIDDocument.DeviceID)get_store().find_element_user(DEVICEID$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.request.DeviceIDDocument.DeviceID)get_store().add_element_user(DEVICEID$0);
            }
            target.set(deviceID);
        }
    }
    /**
     * An XML DeviceID(@http://idanalytics.com/products/idscore/request).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.DeviceIDDocument$DeviceID.
     */
    public static class DeviceIDImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.request.DeviceIDDocument.DeviceID
    {
        private static final long serialVersionUID = 1L;
        
        public DeviceIDImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected DeviceIDImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
