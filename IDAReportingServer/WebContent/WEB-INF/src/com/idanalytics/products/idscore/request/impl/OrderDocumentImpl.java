/*
 * An XML document type.
 * Localname: Order
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.OrderDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * A document containing one Order(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public class OrderDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.OrderDocument
{
    private static final long serialVersionUID = 1L;
    
    public OrderDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ORDER$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Order");
    
    
    /**
     * Gets the "Order" element
     */
    public com.idanalytics.products.idscore.request.OrderDocument.Order getOrder()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.OrderDocument.Order target = null;
            target = (com.idanalytics.products.idscore.request.OrderDocument.Order)get_store().find_element_user(ORDER$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "Order" element
     */
    public void setOrder(com.idanalytics.products.idscore.request.OrderDocument.Order order)
    {
        generatedSetterHelperImpl(order, ORDER$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "Order" element
     */
    public com.idanalytics.products.idscore.request.OrderDocument.Order addNewOrder()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.OrderDocument.Order target = null;
            target = (com.idanalytics.products.idscore.request.OrderDocument.Order)get_store().add_element_user(ORDER$0);
            return target;
        }
    }
    /**
     * An XML Order(@http://idanalytics.com/products/idscore/request).
     *
     * This is a complex type.
     */
    public static class OrderImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.OrderDocument.Order
    {
        private static final long serialVersionUID = 1L;
        
        public OrderImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName ORDERHEADER$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "OrderHeader");
        private static final javax.xml.namespace.QName BILLINGIDENTITY$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "BillingIdentity");
        private static final javax.xml.namespace.QName SHIPPINGIDENTITY$4 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ShippingIdentity");
        private static final javax.xml.namespace.QName IDAINTERNAL$6 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "IDAInternal");
        private static final javax.xml.namespace.QName SCHEMAVERSION$8 = 
            new javax.xml.namespace.QName("", "schemaVersion");
        
        
        /**
         * Gets the "OrderHeader" element
         */
        public com.idanalytics.products.idscore.request.OrderHeaderType getOrderHeader()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.OrderHeaderType target = null;
                target = (com.idanalytics.products.idscore.request.OrderHeaderType)get_store().find_element_user(ORDERHEADER$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * Sets the "OrderHeader" element
         */
        public void setOrderHeader(com.idanalytics.products.idscore.request.OrderHeaderType orderHeader)
        {
            generatedSetterHelperImpl(orderHeader, ORDERHEADER$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "OrderHeader" element
         */
        public com.idanalytics.products.idscore.request.OrderHeaderType addNewOrderHeader()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.OrderHeaderType target = null;
                target = (com.idanalytics.products.idscore.request.OrderHeaderType)get_store().add_element_user(ORDERHEADER$0);
                return target;
            }
        }
        
        /**
         * Gets the "BillingIdentity" element
         */
        public com.idanalytics.products.idscore.request.OrderIdentity getBillingIdentity()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.OrderIdentity target = null;
                target = (com.idanalytics.products.idscore.request.OrderIdentity)get_store().find_element_user(BILLINGIDENTITY$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * Sets the "BillingIdentity" element
         */
        public void setBillingIdentity(com.idanalytics.products.idscore.request.OrderIdentity billingIdentity)
        {
            generatedSetterHelperImpl(billingIdentity, BILLINGIDENTITY$2, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "BillingIdentity" element
         */
        public com.idanalytics.products.idscore.request.OrderIdentity addNewBillingIdentity()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.OrderIdentity target = null;
                target = (com.idanalytics.products.idscore.request.OrderIdentity)get_store().add_element_user(BILLINGIDENTITY$2);
                return target;
            }
        }
        
        /**
         * Gets array of all "ShippingIdentity" elements
         */
        public com.idanalytics.products.idscore.request.OrderIdentity[] getShippingIdentityArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(SHIPPINGIDENTITY$4, targetList);
                com.idanalytics.products.idscore.request.OrderIdentity[] result = new com.idanalytics.products.idscore.request.OrderIdentity[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "ShippingIdentity" element
         */
        public com.idanalytics.products.idscore.request.OrderIdentity getShippingIdentityArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.OrderIdentity target = null;
                target = (com.idanalytics.products.idscore.request.OrderIdentity)get_store().find_element_user(SHIPPINGIDENTITY$4, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "ShippingIdentity" element
         */
        public int sizeOfShippingIdentityArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(SHIPPINGIDENTITY$4);
            }
        }
        
        /**
         * Sets array of all "ShippingIdentity" element  WARNING: This method is not atomicaly synchronized.
         */
        public void setShippingIdentityArray(com.idanalytics.products.idscore.request.OrderIdentity[] shippingIdentityArray)
        {
            check_orphaned();
            arraySetterHelper(shippingIdentityArray, SHIPPINGIDENTITY$4);
        }
        
        /**
         * Sets ith "ShippingIdentity" element
         */
        public void setShippingIdentityArray(int i, com.idanalytics.products.idscore.request.OrderIdentity shippingIdentity)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.OrderIdentity target = null;
                target = (com.idanalytics.products.idscore.request.OrderIdentity)get_store().find_element_user(SHIPPINGIDENTITY$4, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(shippingIdentity);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "ShippingIdentity" element
         */
        public com.idanalytics.products.idscore.request.OrderIdentity insertNewShippingIdentity(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.OrderIdentity target = null;
                target = (com.idanalytics.products.idscore.request.OrderIdentity)get_store().insert_element_user(SHIPPINGIDENTITY$4, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "ShippingIdentity" element
         */
        public com.idanalytics.products.idscore.request.OrderIdentity addNewShippingIdentity()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.OrderIdentity target = null;
                target = (com.idanalytics.products.idscore.request.OrderIdentity)get_store().add_element_user(SHIPPINGIDENTITY$4);
                return target;
            }
        }
        
        /**
         * Removes the ith "ShippingIdentity" element
         */
        public void removeShippingIdentity(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(SHIPPINGIDENTITY$4, i);
            }
        }
        
        /**
         * Gets the "IDAInternal" element
         */
        public com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal getIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal target = null;
                target = (com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal)get_store().find_element_user(IDAINTERNAL$6, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "IDAInternal" element
         */
        public boolean isSetIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(IDAINTERNAL$6) != 0;
            }
        }
        
        /**
         * Sets the "IDAInternal" element
         */
        public void setIDAInternal(com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal idaInternal)
        {
            generatedSetterHelperImpl(idaInternal, IDAINTERNAL$6, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "IDAInternal" element
         */
        public com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal addNewIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal target = null;
                target = (com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal)get_store().add_element_user(IDAINTERNAL$6);
                return target;
            }
        }
        
        /**
         * Unsets the "IDAInternal" element
         */
        public void unsetIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(IDAINTERNAL$6, 0);
            }
        }
        
        /**
         * Gets the "schemaVersion" attribute
         */
        public java.math.BigDecimal getSchemaVersion()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(SCHEMAVERSION$8);
                if (target == null)
                {
                    return null;
                }
                return target.getBigDecimalValue();
            }
        }
        
        /**
         * Gets (as xml) the "schemaVersion" attribute
         */
        public org.apache.xmlbeans.XmlDecimal xgetSchemaVersion()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlDecimal target = null;
                target = (org.apache.xmlbeans.XmlDecimal)get_store().find_attribute_user(SCHEMAVERSION$8);
                return target;
            }
        }
        
        /**
         * True if has "schemaVersion" attribute
         */
        public boolean isSetSchemaVersion()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().find_attribute_user(SCHEMAVERSION$8) != null;
            }
        }
        
        /**
         * Sets the "schemaVersion" attribute
         */
        public void setSchemaVersion(java.math.BigDecimal schemaVersion)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(SCHEMAVERSION$8);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(SCHEMAVERSION$8);
                }
                target.setBigDecimalValue(schemaVersion);
            }
        }
        
        /**
         * Sets (as xml) the "schemaVersion" attribute
         */
        public void xsetSchemaVersion(org.apache.xmlbeans.XmlDecimal schemaVersion)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlDecimal target = null;
                target = (org.apache.xmlbeans.XmlDecimal)get_store().find_attribute_user(SCHEMAVERSION$8);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlDecimal)get_store().add_attribute_user(SCHEMAVERSION$8);
                }
                target.set(schemaVersion);
            }
        }
        
        /**
         * Unsets the "schemaVersion" attribute
         */
        public void unsetSchemaVersion()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_attribute(SCHEMAVERSION$8);
            }
        }
    }
}
