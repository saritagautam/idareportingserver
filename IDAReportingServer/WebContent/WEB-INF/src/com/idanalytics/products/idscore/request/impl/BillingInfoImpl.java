/*
 * XML Type:  BillingInfo
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.BillingInfo
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML BillingInfo(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public class BillingInfoImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.BillingInfo
{
    private static final long serialVersionUID = 1L;
    
    public BillingInfoImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IDENTITYINFO$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "IdentityInfo");
    private static final javax.xml.namespace.QName PAYMENT$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Payment");
    
    
    /**
     * Gets the "IdentityInfo" element
     */
    public com.idanalytics.products.idscore.request.CommerceIdentity getIdentityInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.CommerceIdentity target = null;
            target = (com.idanalytics.products.idscore.request.CommerceIdentity)get_store().find_element_user(IDENTITYINFO$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "IdentityInfo" element
     */
    public boolean isSetIdentityInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(IDENTITYINFO$0) != 0;
        }
    }
    
    /**
     * Sets the "IdentityInfo" element
     */
    public void setIdentityInfo(com.idanalytics.products.idscore.request.CommerceIdentity identityInfo)
    {
        generatedSetterHelperImpl(identityInfo, IDENTITYINFO$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "IdentityInfo" element
     */
    public com.idanalytics.products.idscore.request.CommerceIdentity addNewIdentityInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.CommerceIdentity target = null;
            target = (com.idanalytics.products.idscore.request.CommerceIdentity)get_store().add_element_user(IDENTITYINFO$0);
            return target;
        }
    }
    
    /**
     * Unsets the "IdentityInfo" element
     */
    public void unsetIdentityInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(IDENTITYINFO$0, 0);
        }
    }
    
    /**
     * Gets array of all "Payment" elements
     */
    public com.idanalytics.products.idscore.request.PaymentInfo[] getPaymentArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(PAYMENT$2, targetList);
            com.idanalytics.products.idscore.request.PaymentInfo[] result = new com.idanalytics.products.idscore.request.PaymentInfo[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "Payment" element
     */
    public com.idanalytics.products.idscore.request.PaymentInfo getPaymentArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.PaymentInfo target = null;
            target = (com.idanalytics.products.idscore.request.PaymentInfo)get_store().find_element_user(PAYMENT$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "Payment" element
     */
    public int sizeOfPaymentArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PAYMENT$2);
        }
    }
    
    /**
     * Sets array of all "Payment" element  WARNING: This method is not atomicaly synchronized.
     */
    public void setPaymentArray(com.idanalytics.products.idscore.request.PaymentInfo[] paymentArray)
    {
        check_orphaned();
        arraySetterHelper(paymentArray, PAYMENT$2);
    }
    
    /**
     * Sets ith "Payment" element
     */
    public void setPaymentArray(int i, com.idanalytics.products.idscore.request.PaymentInfo payment)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.PaymentInfo target = null;
            target = (com.idanalytics.products.idscore.request.PaymentInfo)get_store().find_element_user(PAYMENT$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(payment);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "Payment" element
     */
    public com.idanalytics.products.idscore.request.PaymentInfo insertNewPayment(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.PaymentInfo target = null;
            target = (com.idanalytics.products.idscore.request.PaymentInfo)get_store().insert_element_user(PAYMENT$2, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "Payment" element
     */
    public com.idanalytics.products.idscore.request.PaymentInfo addNewPayment()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.PaymentInfo target = null;
            target = (com.idanalytics.products.idscore.request.PaymentInfo)get_store().add_element_user(PAYMENT$2);
            return target;
        }
    }
    
    /**
     * Removes the ith "Payment" element
     */
    public void removePayment(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PAYMENT$2, i);
        }
    }
}
