/*
 * An XML document type.
 * Localname: AcctLinkKey
 * Namespace: http://idanalytics.com/products/idscore/result
 * Java type: com.idanalytics.products.idscore.result.AcctLinkKeyDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.result.impl;
/**
 * A document containing one AcctLinkKey(@http://idanalytics.com/products/idscore/result) element.
 *
 * This is a complex type.
 */
public class AcctLinkKeyDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.result.AcctLinkKeyDocument
{
    private static final long serialVersionUID = 1L;
    
    public AcctLinkKeyDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ACCTLINKKEY$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "AcctLinkKey");
    
    
    /**
     * Gets the "AcctLinkKey" element
     */
    public java.lang.String getAcctLinkKey()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ACCTLINKKEY$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "AcctLinkKey" element
     */
    public com.idanalytics.products.idscore.result.AcctLinkKeyDocument.AcctLinkKey xgetAcctLinkKey()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.AcctLinkKeyDocument.AcctLinkKey target = null;
            target = (com.idanalytics.products.idscore.result.AcctLinkKeyDocument.AcctLinkKey)get_store().find_element_user(ACCTLINKKEY$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "AcctLinkKey" element
     */
    public void setAcctLinkKey(java.lang.String acctLinkKey)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ACCTLINKKEY$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ACCTLINKKEY$0);
            }
            target.setStringValue(acctLinkKey);
        }
    }
    
    /**
     * Sets (as xml) the "AcctLinkKey" element
     */
    public void xsetAcctLinkKey(com.idanalytics.products.idscore.result.AcctLinkKeyDocument.AcctLinkKey acctLinkKey)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.AcctLinkKeyDocument.AcctLinkKey target = null;
            target = (com.idanalytics.products.idscore.result.AcctLinkKeyDocument.AcctLinkKey)get_store().find_element_user(ACCTLINKKEY$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.result.AcctLinkKeyDocument.AcctLinkKey)get_store().add_element_user(ACCTLINKKEY$0);
            }
            target.set(acctLinkKey);
        }
    }
    /**
     * An XML AcctLinkKey(@http://idanalytics.com/products/idscore/result).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result.AcctLinkKeyDocument$AcctLinkKey.
     */
    public static class AcctLinkKeyImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.result.AcctLinkKeyDocument.AcctLinkKey
    {
        private static final long serialVersionUID = 1L;
        
        public AcctLinkKeyImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected AcctLinkKeyImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
