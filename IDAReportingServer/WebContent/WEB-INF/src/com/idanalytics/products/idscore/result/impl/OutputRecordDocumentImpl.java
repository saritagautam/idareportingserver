/*
 * An XML document type.
 * Localname: OutputRecord
 * Namespace: http://idanalytics.com/products/idscore/result
 * Java type: com.idanalytics.products.idscore.result.OutputRecordDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.result.impl;
/**
 * A document containing one OutputRecord(@http://idanalytics.com/products/idscore/result) element.
 *
 * This is a complex type.
 */
public class OutputRecordDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.result.OutputRecordDocument
{
    private static final long serialVersionUID = 1L;
    
    public OutputRecordDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName OUTPUTRECORD$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "OutputRecord");
    
    
    /**
     * Gets the "OutputRecord" element
     */
    public com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord getOutputRecord()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord target = null;
            target = (com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord)get_store().find_element_user(OUTPUTRECORD$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "OutputRecord" element
     */
    public void setOutputRecord(com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord outputRecord)
    {
        generatedSetterHelperImpl(outputRecord, OUTPUTRECORD$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "OutputRecord" element
     */
    public com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord addNewOutputRecord()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord target = null;
            target = (com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord)get_store().add_element_user(OUTPUTRECORD$0);
            return target;
        }
    }
    /**
     * An XML OutputRecord(@http://idanalytics.com/products/idscore/result).
     *
     * This is a complex type.
     */
    public static class OutputRecordImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord
    {
        private static final long serialVersionUID = 1L;
        
        public OutputRecordImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName IDASTATUS$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "IDAStatus");
        private static final javax.xml.namespace.QName APPID$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "AppID");
        private static final javax.xml.namespace.QName CLIENTKEY$4 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "ClientKey");
        private static final javax.xml.namespace.QName CONSUMERID$6 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "ConsumerID");
        private static final javax.xml.namespace.QName DESIGNATION$8 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "Designation");
        private static final javax.xml.namespace.QName ACCTLINKKEY$10 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "AcctLinkKey");
        private static final javax.xml.namespace.QName IDASEQUENCE$12 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "IDASequence");
        private static final javax.xml.namespace.QName IDATIMESTAMP$14 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "IDATimeStamp");
        private static final javax.xml.namespace.QName PRESCREENDATE$16 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "PrescreenDate");
        private static final javax.xml.namespace.QName IDSCORE$18 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "IDScore");
        private static final javax.xml.namespace.QName SCOREBANDINDICATOR$20 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "ScoreBandIndicator");
        private static final javax.xml.namespace.QName SCOREDELTAINDICATOR$22 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "ScoreDeltaIndicator");
        private static final javax.xml.namespace.QName IDSCORERESULTCODE1$24 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "IDScoreResultCode1");
        private static final javax.xml.namespace.QName IDSCORERESULTCODE2$26 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "IDScoreResultCode2");
        private static final javax.xml.namespace.QName IDSCORERESULTCODE3$28 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "IDScoreResultCode3");
        private static final javax.xml.namespace.QName IDSCORERESULTCODE4$30 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "IDScoreResultCode4");
        private static final javax.xml.namespace.QName IDSCORERESULTCODE5$32 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "IDScoreResultCode5");
        private static final javax.xml.namespace.QName IDSCORERESULTCODE6$34 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "IDScoreResultCode6");
        private static final javax.xml.namespace.QName RESULTCODE1$36 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "ResultCode1");
        private static final javax.xml.namespace.QName RESULTCODE2$38 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "ResultCode2");
        private static final javax.xml.namespace.QName RESULTCODE3$40 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "ResultCode3");
        private static final javax.xml.namespace.QName RESULTCODE4$42 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "ResultCode4");
        private static final javax.xml.namespace.QName RESULTCODE5$44 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "ResultCode5");
        private static final javax.xml.namespace.QName RESULTCODE6$46 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "ResultCode6");
        private static final javax.xml.namespace.QName CONSUMERSTATEMENTS$48 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "ConsumerStatements");
        private static final javax.xml.namespace.QName MESSAGES$50 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "Messages");
        private static final javax.xml.namespace.QName SIGNALS$52 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "Signals");
        private static final javax.xml.namespace.QName PASSTHRU1$54 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "PassThru1");
        private static final javax.xml.namespace.QName PASSTHRU2$56 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "PassThru2");
        private static final javax.xml.namespace.QName PASSTHRU3$58 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "PassThru3");
        private static final javax.xml.namespace.QName PASSTHRU4$60 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "PassThru4");
        private static final javax.xml.namespace.QName INDICATORS$62 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "Indicators");
        private static final javax.xml.namespace.QName IDAINTERNAL$64 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "IDAInternal");
        private static final javax.xml.namespace.QName PREVIOUSEVENTS$66 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "PreviousEvents");
        private static final javax.xml.namespace.QName SCHEMAVERSION$68 = 
            new javax.xml.namespace.QName("", "schemaVersion");
        
        
        /**
         * Gets the "IDAStatus" element
         */
        public int getIDAStatus()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASTATUS$0, 0);
                if (target == null)
                {
                    return 0;
                }
                return target.getIntValue();
            }
        }
        
        /**
         * Gets (as xml) the "IDAStatus" element
         */
        public com.idanalytics.products.idscore.result.IDAStatusDocument.IDAStatus xgetIDAStatus()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.IDAStatusDocument.IDAStatus target = null;
                target = (com.idanalytics.products.idscore.result.IDAStatusDocument.IDAStatus)get_store().find_element_user(IDASTATUS$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "IDAStatus" element
         */
        public void setIDAStatus(int idaStatus)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASTATUS$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDASTATUS$0);
                }
                target.setIntValue(idaStatus);
            }
        }
        
        /**
         * Sets (as xml) the "IDAStatus" element
         */
        public void xsetIDAStatus(com.idanalytics.products.idscore.result.IDAStatusDocument.IDAStatus idaStatus)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.IDAStatusDocument.IDAStatus target = null;
                target = (com.idanalytics.products.idscore.result.IDAStatusDocument.IDAStatus)get_store().find_element_user(IDASTATUS$0, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result.IDAStatusDocument.IDAStatus)get_store().add_element_user(IDASTATUS$0);
                }
                target.set(idaStatus);
            }
        }
        
        /**
         * Gets the "AppID" element
         */
        public java.lang.String getAppID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(APPID$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "AppID" element
         */
        public com.idanalytics.products.common_v1.AppID xgetAppID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.AppID target = null;
                target = (com.idanalytics.products.common_v1.AppID)get_store().find_element_user(APPID$2, 0);
                return target;
            }
        }
        
        /**
         * True if has "AppID" element
         */
        public boolean isSetAppID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(APPID$2) != 0;
            }
        }
        
        /**
         * Sets the "AppID" element
         */
        public void setAppID(java.lang.String appID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(APPID$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(APPID$2);
                }
                target.setStringValue(appID);
            }
        }
        
        /**
         * Sets (as xml) the "AppID" element
         */
        public void xsetAppID(com.idanalytics.products.common_v1.AppID appID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.AppID target = null;
                target = (com.idanalytics.products.common_v1.AppID)get_store().find_element_user(APPID$2, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.AppID)get_store().add_element_user(APPID$2);
                }
                target.set(appID);
            }
        }
        
        /**
         * Unsets the "AppID" element
         */
        public void unsetAppID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(APPID$2, 0);
            }
        }
        
        /**
         * Gets the "ClientKey" element
         */
        public java.lang.String getClientKey()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CLIENTKEY$4, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "ClientKey" element
         */
        public com.idanalytics.products.idscore.result.ClientKeyDocument.ClientKey xgetClientKey()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.ClientKeyDocument.ClientKey target = null;
                target = (com.idanalytics.products.idscore.result.ClientKeyDocument.ClientKey)get_store().find_element_user(CLIENTKEY$4, 0);
                return target;
            }
        }
        
        /**
         * True if has "ClientKey" element
         */
        public boolean isSetClientKey()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(CLIENTKEY$4) != 0;
            }
        }
        
        /**
         * Sets the "ClientKey" element
         */
        public void setClientKey(java.lang.String clientKey)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CLIENTKEY$4, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CLIENTKEY$4);
                }
                target.setStringValue(clientKey);
            }
        }
        
        /**
         * Sets (as xml) the "ClientKey" element
         */
        public void xsetClientKey(com.idanalytics.products.idscore.result.ClientKeyDocument.ClientKey clientKey)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.ClientKeyDocument.ClientKey target = null;
                target = (com.idanalytics.products.idscore.result.ClientKeyDocument.ClientKey)get_store().find_element_user(CLIENTKEY$4, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result.ClientKeyDocument.ClientKey)get_store().add_element_user(CLIENTKEY$4);
                }
                target.set(clientKey);
            }
        }
        
        /**
         * Unsets the "ClientKey" element
         */
        public void unsetClientKey()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(CLIENTKEY$4, 0);
            }
        }
        
        /**
         * Gets the "ConsumerID" element
         */
        public java.lang.String getConsumerID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CONSUMERID$6, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "ConsumerID" element
         */
        public com.idanalytics.products.idscore.result.ConsumerIDDocument.ConsumerID xgetConsumerID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.ConsumerIDDocument.ConsumerID target = null;
                target = (com.idanalytics.products.idscore.result.ConsumerIDDocument.ConsumerID)get_store().find_element_user(CONSUMERID$6, 0);
                return target;
            }
        }
        
        /**
         * True if has "ConsumerID" element
         */
        public boolean isSetConsumerID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(CONSUMERID$6) != 0;
            }
        }
        
        /**
         * Sets the "ConsumerID" element
         */
        public void setConsumerID(java.lang.String consumerID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CONSUMERID$6, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CONSUMERID$6);
                }
                target.setStringValue(consumerID);
            }
        }
        
        /**
         * Sets (as xml) the "ConsumerID" element
         */
        public void xsetConsumerID(com.idanalytics.products.idscore.result.ConsumerIDDocument.ConsumerID consumerID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.ConsumerIDDocument.ConsumerID target = null;
                target = (com.idanalytics.products.idscore.result.ConsumerIDDocument.ConsumerID)get_store().find_element_user(CONSUMERID$6, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result.ConsumerIDDocument.ConsumerID)get_store().add_element_user(CONSUMERID$6);
                }
                target.set(consumerID);
            }
        }
        
        /**
         * Unsets the "ConsumerID" element
         */
        public void unsetConsumerID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(CONSUMERID$6, 0);
            }
        }
        
        /**
         * Gets the "Designation" element
         */
        public com.idanalytics.products.idscore.result.DesignationDocument.Designation.Enum getDesignation()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESIGNATION$8, 0);
                if (target == null)
                {
                    return null;
                }
                return (com.idanalytics.products.idscore.result.DesignationDocument.Designation.Enum)target.getEnumValue();
            }
        }
        
        /**
         * Gets (as xml) the "Designation" element
         */
        public com.idanalytics.products.idscore.result.DesignationDocument.Designation xgetDesignation()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.DesignationDocument.Designation target = null;
                target = (com.idanalytics.products.idscore.result.DesignationDocument.Designation)get_store().find_element_user(DESIGNATION$8, 0);
                return target;
            }
        }
        
        /**
         * True if has "Designation" element
         */
        public boolean isSetDesignation()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(DESIGNATION$8) != 0;
            }
        }
        
        /**
         * Sets the "Designation" element
         */
        public void setDesignation(com.idanalytics.products.idscore.result.DesignationDocument.Designation.Enum designation)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESIGNATION$8, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DESIGNATION$8);
                }
                target.setEnumValue(designation);
            }
        }
        
        /**
         * Sets (as xml) the "Designation" element
         */
        public void xsetDesignation(com.idanalytics.products.idscore.result.DesignationDocument.Designation designation)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.DesignationDocument.Designation target = null;
                target = (com.idanalytics.products.idscore.result.DesignationDocument.Designation)get_store().find_element_user(DESIGNATION$8, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result.DesignationDocument.Designation)get_store().add_element_user(DESIGNATION$8);
                }
                target.set(designation);
            }
        }
        
        /**
         * Unsets the "Designation" element
         */
        public void unsetDesignation()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(DESIGNATION$8, 0);
            }
        }
        
        /**
         * Gets the "AcctLinkKey" element
         */
        public java.lang.String getAcctLinkKey()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ACCTLINKKEY$10, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "AcctLinkKey" element
         */
        public com.idanalytics.products.idscore.result.AcctLinkKeyDocument.AcctLinkKey xgetAcctLinkKey()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.AcctLinkKeyDocument.AcctLinkKey target = null;
                target = (com.idanalytics.products.idscore.result.AcctLinkKeyDocument.AcctLinkKey)get_store().find_element_user(ACCTLINKKEY$10, 0);
                return target;
            }
        }
        
        /**
         * True if has "AcctLinkKey" element
         */
        public boolean isSetAcctLinkKey()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(ACCTLINKKEY$10) != 0;
            }
        }
        
        /**
         * Sets the "AcctLinkKey" element
         */
        public void setAcctLinkKey(java.lang.String acctLinkKey)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ACCTLINKKEY$10, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ACCTLINKKEY$10);
                }
                target.setStringValue(acctLinkKey);
            }
        }
        
        /**
         * Sets (as xml) the "AcctLinkKey" element
         */
        public void xsetAcctLinkKey(com.idanalytics.products.idscore.result.AcctLinkKeyDocument.AcctLinkKey acctLinkKey)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.AcctLinkKeyDocument.AcctLinkKey target = null;
                target = (com.idanalytics.products.idscore.result.AcctLinkKeyDocument.AcctLinkKey)get_store().find_element_user(ACCTLINKKEY$10, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result.AcctLinkKeyDocument.AcctLinkKey)get_store().add_element_user(ACCTLINKKEY$10);
                }
                target.set(acctLinkKey);
            }
        }
        
        /**
         * Unsets the "AcctLinkKey" element
         */
        public void unsetAcctLinkKey()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(ACCTLINKKEY$10, 0);
            }
        }
        
        /**
         * Gets the "IDASequence" element
         */
        public java.lang.String getIDASequence()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASEQUENCE$12, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "IDASequence" element
         */
        public com.idanalytics.products.idscore.result.IDASequenceDocument.IDASequence xgetIDASequence()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.IDASequenceDocument.IDASequence target = null;
                target = (com.idanalytics.products.idscore.result.IDASequenceDocument.IDASequence)get_store().find_element_user(IDASEQUENCE$12, 0);
                return target;
            }
        }
        
        /**
         * Sets the "IDASequence" element
         */
        public void setIDASequence(java.lang.String idaSequence)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASEQUENCE$12, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDASEQUENCE$12);
                }
                target.setStringValue(idaSequence);
            }
        }
        
        /**
         * Sets (as xml) the "IDASequence" element
         */
        public void xsetIDASequence(com.idanalytics.products.idscore.result.IDASequenceDocument.IDASequence idaSequence)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.IDASequenceDocument.IDASequence target = null;
                target = (com.idanalytics.products.idscore.result.IDASequenceDocument.IDASequence)get_store().find_element_user(IDASEQUENCE$12, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result.IDASequenceDocument.IDASequence)get_store().add_element_user(IDASEQUENCE$12);
                }
                target.set(idaSequence);
            }
        }
        
        /**
         * Gets the "IDATimeStamp" element
         */
        public java.util.Calendar getIDATimeStamp()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDATIMESTAMP$14, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getCalendarValue();
            }
        }
        
        /**
         * Gets (as xml) the "IDATimeStamp" element
         */
        public com.idanalytics.products.idscore.result.IDATimeStampDocument.IDATimeStamp xgetIDATimeStamp()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.IDATimeStampDocument.IDATimeStamp target = null;
                target = (com.idanalytics.products.idscore.result.IDATimeStampDocument.IDATimeStamp)get_store().find_element_user(IDATIMESTAMP$14, 0);
                return target;
            }
        }
        
        /**
         * Sets the "IDATimeStamp" element
         */
        public void setIDATimeStamp(java.util.Calendar idaTimeStamp)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDATIMESTAMP$14, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDATIMESTAMP$14);
                }
                target.setCalendarValue(idaTimeStamp);
            }
        }
        
        /**
         * Sets (as xml) the "IDATimeStamp" element
         */
        public void xsetIDATimeStamp(com.idanalytics.products.idscore.result.IDATimeStampDocument.IDATimeStamp idaTimeStamp)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.IDATimeStampDocument.IDATimeStamp target = null;
                target = (com.idanalytics.products.idscore.result.IDATimeStampDocument.IDATimeStamp)get_store().find_element_user(IDATIMESTAMP$14, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result.IDATimeStampDocument.IDATimeStamp)get_store().add_element_user(IDATIMESTAMP$14);
                }
                target.set(idaTimeStamp);
            }
        }
        
        /**
         * Gets the "PrescreenDate" element
         */
        public java.lang.String getPrescreenDate()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PRESCREENDATE$16, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PrescreenDate" element
         */
        public com.idanalytics.products.common_v1.CampaignIdentifier xgetPrescreenDate()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.CampaignIdentifier target = null;
                target = (com.idanalytics.products.common_v1.CampaignIdentifier)get_store().find_element_user(PRESCREENDATE$16, 0);
                return target;
            }
        }
        
        /**
         * True if has "PrescreenDate" element
         */
        public boolean isSetPrescreenDate()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PRESCREENDATE$16) != 0;
            }
        }
        
        /**
         * Sets the "PrescreenDate" element
         */
        public void setPrescreenDate(java.lang.String prescreenDate)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PRESCREENDATE$16, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PRESCREENDATE$16);
                }
                target.setStringValue(prescreenDate);
            }
        }
        
        /**
         * Sets (as xml) the "PrescreenDate" element
         */
        public void xsetPrescreenDate(com.idanalytics.products.common_v1.CampaignIdentifier prescreenDate)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.CampaignIdentifier target = null;
                target = (com.idanalytics.products.common_v1.CampaignIdentifier)get_store().find_element_user(PRESCREENDATE$16, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.CampaignIdentifier)get_store().add_element_user(PRESCREENDATE$16);
                }
                target.set(prescreenDate);
            }
        }
        
        /**
         * Unsets the "PrescreenDate" element
         */
        public void unsetPrescreenDate()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PRESCREENDATE$16, 0);
            }
        }
        
        /**
         * Gets the "IDScore" element
         */
        public java.lang.String getIDScore()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDSCORE$18, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "IDScore" element
         */
        public com.idanalytics.products.idscore.result.IDScoreDocument.IDScore xgetIDScore()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.IDScoreDocument.IDScore target = null;
                target = (com.idanalytics.products.idscore.result.IDScoreDocument.IDScore)get_store().find_element_user(IDSCORE$18, 0);
                return target;
            }
        }
        
        /**
         * True if has "IDScore" element
         */
        public boolean isSetIDScore()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(IDSCORE$18) != 0;
            }
        }
        
        /**
         * Sets the "IDScore" element
         */
        public void setIDScore(java.lang.String idScore)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDSCORE$18, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDSCORE$18);
                }
                target.setStringValue(idScore);
            }
        }
        
        /**
         * Sets (as xml) the "IDScore" element
         */
        public void xsetIDScore(com.idanalytics.products.idscore.result.IDScoreDocument.IDScore idScore)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.IDScoreDocument.IDScore target = null;
                target = (com.idanalytics.products.idscore.result.IDScoreDocument.IDScore)get_store().find_element_user(IDSCORE$18, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result.IDScoreDocument.IDScore)get_store().add_element_user(IDSCORE$18);
                }
                target.set(idScore);
            }
        }
        
        /**
         * Unsets the "IDScore" element
         */
        public void unsetIDScore()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(IDSCORE$18, 0);
            }
        }
        
        /**
         * Gets the "ScoreBandIndicator" element
         */
        public com.idanalytics.products.idscore.result.ScoreBandIndicatorDocument.ScoreBandIndicator.Enum getScoreBandIndicator()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SCOREBANDINDICATOR$20, 0);
                if (target == null)
                {
                    return null;
                }
                return (com.idanalytics.products.idscore.result.ScoreBandIndicatorDocument.ScoreBandIndicator.Enum)target.getEnumValue();
            }
        }
        
        /**
         * Gets (as xml) the "ScoreBandIndicator" element
         */
        public com.idanalytics.products.idscore.result.ScoreBandIndicatorDocument.ScoreBandIndicator xgetScoreBandIndicator()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.ScoreBandIndicatorDocument.ScoreBandIndicator target = null;
                target = (com.idanalytics.products.idscore.result.ScoreBandIndicatorDocument.ScoreBandIndicator)get_store().find_element_user(SCOREBANDINDICATOR$20, 0);
                return target;
            }
        }
        
        /**
         * True if has "ScoreBandIndicator" element
         */
        public boolean isSetScoreBandIndicator()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(SCOREBANDINDICATOR$20) != 0;
            }
        }
        
        /**
         * Sets the "ScoreBandIndicator" element
         */
        public void setScoreBandIndicator(com.idanalytics.products.idscore.result.ScoreBandIndicatorDocument.ScoreBandIndicator.Enum scoreBandIndicator)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SCOREBANDINDICATOR$20, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SCOREBANDINDICATOR$20);
                }
                target.setEnumValue(scoreBandIndicator);
            }
        }
        
        /**
         * Sets (as xml) the "ScoreBandIndicator" element
         */
        public void xsetScoreBandIndicator(com.idanalytics.products.idscore.result.ScoreBandIndicatorDocument.ScoreBandIndicator scoreBandIndicator)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.ScoreBandIndicatorDocument.ScoreBandIndicator target = null;
                target = (com.idanalytics.products.idscore.result.ScoreBandIndicatorDocument.ScoreBandIndicator)get_store().find_element_user(SCOREBANDINDICATOR$20, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result.ScoreBandIndicatorDocument.ScoreBandIndicator)get_store().add_element_user(SCOREBANDINDICATOR$20);
                }
                target.set(scoreBandIndicator);
            }
        }
        
        /**
         * Unsets the "ScoreBandIndicator" element
         */
        public void unsetScoreBandIndicator()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(SCOREBANDINDICATOR$20, 0);
            }
        }
        
        /**
         * Gets the "ScoreDeltaIndicator" element
         */
        public com.idanalytics.products.idscore.result.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator.Enum getScoreDeltaIndicator()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SCOREDELTAINDICATOR$22, 0);
                if (target == null)
                {
                    return null;
                }
                return (com.idanalytics.products.idscore.result.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator.Enum)target.getEnumValue();
            }
        }
        
        /**
         * Gets (as xml) the "ScoreDeltaIndicator" element
         */
        public com.idanalytics.products.idscore.result.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator xgetScoreDeltaIndicator()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator target = null;
                target = (com.idanalytics.products.idscore.result.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator)get_store().find_element_user(SCOREDELTAINDICATOR$22, 0);
                return target;
            }
        }
        
        /**
         * True if has "ScoreDeltaIndicator" element
         */
        public boolean isSetScoreDeltaIndicator()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(SCOREDELTAINDICATOR$22) != 0;
            }
        }
        
        /**
         * Sets the "ScoreDeltaIndicator" element
         */
        public void setScoreDeltaIndicator(com.idanalytics.products.idscore.result.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator.Enum scoreDeltaIndicator)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SCOREDELTAINDICATOR$22, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SCOREDELTAINDICATOR$22);
                }
                target.setEnumValue(scoreDeltaIndicator);
            }
        }
        
        /**
         * Sets (as xml) the "ScoreDeltaIndicator" element
         */
        public void xsetScoreDeltaIndicator(com.idanalytics.products.idscore.result.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator scoreDeltaIndicator)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator target = null;
                target = (com.idanalytics.products.idscore.result.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator)get_store().find_element_user(SCOREDELTAINDICATOR$22, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result.ScoreDeltaIndicatorDocument.ScoreDeltaIndicator)get_store().add_element_user(SCOREDELTAINDICATOR$22);
                }
                target.set(scoreDeltaIndicator);
            }
        }
        
        /**
         * Unsets the "ScoreDeltaIndicator" element
         */
        public void unsetScoreDeltaIndicator()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(SCOREDELTAINDICATOR$22, 0);
            }
        }
        
        /**
         * Gets the "IDScoreResultCode1" element
         */
        public java.lang.String getIDScoreResultCode1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDSCORERESULTCODE1$24, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "IDScoreResultCode1" element
         */
        public com.idanalytics.products.idscore.result.IDScoreResultCode1Document.IDScoreResultCode1 xgetIDScoreResultCode1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.IDScoreResultCode1Document.IDScoreResultCode1 target = null;
                target = (com.idanalytics.products.idscore.result.IDScoreResultCode1Document.IDScoreResultCode1)get_store().find_element_user(IDSCORERESULTCODE1$24, 0);
                return target;
            }
        }
        
        /**
         * True if has "IDScoreResultCode1" element
         */
        public boolean isSetIDScoreResultCode1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(IDSCORERESULTCODE1$24) != 0;
            }
        }
        
        /**
         * Sets the "IDScoreResultCode1" element
         */
        public void setIDScoreResultCode1(java.lang.String idScoreResultCode1)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDSCORERESULTCODE1$24, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDSCORERESULTCODE1$24);
                }
                target.setStringValue(idScoreResultCode1);
            }
        }
        
        /**
         * Sets (as xml) the "IDScoreResultCode1" element
         */
        public void xsetIDScoreResultCode1(com.idanalytics.products.idscore.result.IDScoreResultCode1Document.IDScoreResultCode1 idScoreResultCode1)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.IDScoreResultCode1Document.IDScoreResultCode1 target = null;
                target = (com.idanalytics.products.idscore.result.IDScoreResultCode1Document.IDScoreResultCode1)get_store().find_element_user(IDSCORERESULTCODE1$24, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result.IDScoreResultCode1Document.IDScoreResultCode1)get_store().add_element_user(IDSCORERESULTCODE1$24);
                }
                target.set(idScoreResultCode1);
            }
        }
        
        /**
         * Unsets the "IDScoreResultCode1" element
         */
        public void unsetIDScoreResultCode1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(IDSCORERESULTCODE1$24, 0);
            }
        }
        
        /**
         * Gets the "IDScoreResultCode2" element
         */
        public java.lang.String getIDScoreResultCode2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDSCORERESULTCODE2$26, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "IDScoreResultCode2" element
         */
        public com.idanalytics.products.idscore.result.IDScoreResultCode2Document.IDScoreResultCode2 xgetIDScoreResultCode2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.IDScoreResultCode2Document.IDScoreResultCode2 target = null;
                target = (com.idanalytics.products.idscore.result.IDScoreResultCode2Document.IDScoreResultCode2)get_store().find_element_user(IDSCORERESULTCODE2$26, 0);
                return target;
            }
        }
        
        /**
         * True if has "IDScoreResultCode2" element
         */
        public boolean isSetIDScoreResultCode2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(IDSCORERESULTCODE2$26) != 0;
            }
        }
        
        /**
         * Sets the "IDScoreResultCode2" element
         */
        public void setIDScoreResultCode2(java.lang.String idScoreResultCode2)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDSCORERESULTCODE2$26, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDSCORERESULTCODE2$26);
                }
                target.setStringValue(idScoreResultCode2);
            }
        }
        
        /**
         * Sets (as xml) the "IDScoreResultCode2" element
         */
        public void xsetIDScoreResultCode2(com.idanalytics.products.idscore.result.IDScoreResultCode2Document.IDScoreResultCode2 idScoreResultCode2)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.IDScoreResultCode2Document.IDScoreResultCode2 target = null;
                target = (com.idanalytics.products.idscore.result.IDScoreResultCode2Document.IDScoreResultCode2)get_store().find_element_user(IDSCORERESULTCODE2$26, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result.IDScoreResultCode2Document.IDScoreResultCode2)get_store().add_element_user(IDSCORERESULTCODE2$26);
                }
                target.set(idScoreResultCode2);
            }
        }
        
        /**
         * Unsets the "IDScoreResultCode2" element
         */
        public void unsetIDScoreResultCode2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(IDSCORERESULTCODE2$26, 0);
            }
        }
        
        /**
         * Gets the "IDScoreResultCode3" element
         */
        public java.lang.String getIDScoreResultCode3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDSCORERESULTCODE3$28, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "IDScoreResultCode3" element
         */
        public com.idanalytics.products.idscore.result.IDScoreResultCode3Document.IDScoreResultCode3 xgetIDScoreResultCode3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.IDScoreResultCode3Document.IDScoreResultCode3 target = null;
                target = (com.idanalytics.products.idscore.result.IDScoreResultCode3Document.IDScoreResultCode3)get_store().find_element_user(IDSCORERESULTCODE3$28, 0);
                return target;
            }
        }
        
        /**
         * True if has "IDScoreResultCode3" element
         */
        public boolean isSetIDScoreResultCode3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(IDSCORERESULTCODE3$28) != 0;
            }
        }
        
        /**
         * Sets the "IDScoreResultCode3" element
         */
        public void setIDScoreResultCode3(java.lang.String idScoreResultCode3)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDSCORERESULTCODE3$28, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDSCORERESULTCODE3$28);
                }
                target.setStringValue(idScoreResultCode3);
            }
        }
        
        /**
         * Sets (as xml) the "IDScoreResultCode3" element
         */
        public void xsetIDScoreResultCode3(com.idanalytics.products.idscore.result.IDScoreResultCode3Document.IDScoreResultCode3 idScoreResultCode3)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.IDScoreResultCode3Document.IDScoreResultCode3 target = null;
                target = (com.idanalytics.products.idscore.result.IDScoreResultCode3Document.IDScoreResultCode3)get_store().find_element_user(IDSCORERESULTCODE3$28, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result.IDScoreResultCode3Document.IDScoreResultCode3)get_store().add_element_user(IDSCORERESULTCODE3$28);
                }
                target.set(idScoreResultCode3);
            }
        }
        
        /**
         * Unsets the "IDScoreResultCode3" element
         */
        public void unsetIDScoreResultCode3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(IDSCORERESULTCODE3$28, 0);
            }
        }
        
        /**
         * Gets the "IDScoreResultCode4" element
         */
        public java.lang.String getIDScoreResultCode4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDSCORERESULTCODE4$30, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "IDScoreResultCode4" element
         */
        public com.idanalytics.products.idscore.result.IDScoreResultCode4Document.IDScoreResultCode4 xgetIDScoreResultCode4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.IDScoreResultCode4Document.IDScoreResultCode4 target = null;
                target = (com.idanalytics.products.idscore.result.IDScoreResultCode4Document.IDScoreResultCode4)get_store().find_element_user(IDSCORERESULTCODE4$30, 0);
                return target;
            }
        }
        
        /**
         * True if has "IDScoreResultCode4" element
         */
        public boolean isSetIDScoreResultCode4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(IDSCORERESULTCODE4$30) != 0;
            }
        }
        
        /**
         * Sets the "IDScoreResultCode4" element
         */
        public void setIDScoreResultCode4(java.lang.String idScoreResultCode4)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDSCORERESULTCODE4$30, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDSCORERESULTCODE4$30);
                }
                target.setStringValue(idScoreResultCode4);
            }
        }
        
        /**
         * Sets (as xml) the "IDScoreResultCode4" element
         */
        public void xsetIDScoreResultCode4(com.idanalytics.products.idscore.result.IDScoreResultCode4Document.IDScoreResultCode4 idScoreResultCode4)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.IDScoreResultCode4Document.IDScoreResultCode4 target = null;
                target = (com.idanalytics.products.idscore.result.IDScoreResultCode4Document.IDScoreResultCode4)get_store().find_element_user(IDSCORERESULTCODE4$30, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result.IDScoreResultCode4Document.IDScoreResultCode4)get_store().add_element_user(IDSCORERESULTCODE4$30);
                }
                target.set(idScoreResultCode4);
            }
        }
        
        /**
         * Unsets the "IDScoreResultCode4" element
         */
        public void unsetIDScoreResultCode4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(IDSCORERESULTCODE4$30, 0);
            }
        }
        
        /**
         * Gets the "IDScoreResultCode5" element
         */
        public java.lang.String getIDScoreResultCode5()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDSCORERESULTCODE5$32, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "IDScoreResultCode5" element
         */
        public com.idanalytics.products.idscore.result.IDScoreResultCode5Document.IDScoreResultCode5 xgetIDScoreResultCode5()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.IDScoreResultCode5Document.IDScoreResultCode5 target = null;
                target = (com.idanalytics.products.idscore.result.IDScoreResultCode5Document.IDScoreResultCode5)get_store().find_element_user(IDSCORERESULTCODE5$32, 0);
                return target;
            }
        }
        
        /**
         * True if has "IDScoreResultCode5" element
         */
        public boolean isSetIDScoreResultCode5()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(IDSCORERESULTCODE5$32) != 0;
            }
        }
        
        /**
         * Sets the "IDScoreResultCode5" element
         */
        public void setIDScoreResultCode5(java.lang.String idScoreResultCode5)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDSCORERESULTCODE5$32, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDSCORERESULTCODE5$32);
                }
                target.setStringValue(idScoreResultCode5);
            }
        }
        
        /**
         * Sets (as xml) the "IDScoreResultCode5" element
         */
        public void xsetIDScoreResultCode5(com.idanalytics.products.idscore.result.IDScoreResultCode5Document.IDScoreResultCode5 idScoreResultCode5)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.IDScoreResultCode5Document.IDScoreResultCode5 target = null;
                target = (com.idanalytics.products.idscore.result.IDScoreResultCode5Document.IDScoreResultCode5)get_store().find_element_user(IDSCORERESULTCODE5$32, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result.IDScoreResultCode5Document.IDScoreResultCode5)get_store().add_element_user(IDSCORERESULTCODE5$32);
                }
                target.set(idScoreResultCode5);
            }
        }
        
        /**
         * Unsets the "IDScoreResultCode5" element
         */
        public void unsetIDScoreResultCode5()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(IDSCORERESULTCODE5$32, 0);
            }
        }
        
        /**
         * Gets the "IDScoreResultCode6" element
         */
        public java.lang.String getIDScoreResultCode6()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDSCORERESULTCODE6$34, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "IDScoreResultCode6" element
         */
        public com.idanalytics.products.idscore.result.IDScoreResultCode6Document.IDScoreResultCode6 xgetIDScoreResultCode6()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.IDScoreResultCode6Document.IDScoreResultCode6 target = null;
                target = (com.idanalytics.products.idscore.result.IDScoreResultCode6Document.IDScoreResultCode6)get_store().find_element_user(IDSCORERESULTCODE6$34, 0);
                return target;
            }
        }
        
        /**
         * True if has "IDScoreResultCode6" element
         */
        public boolean isSetIDScoreResultCode6()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(IDSCORERESULTCODE6$34) != 0;
            }
        }
        
        /**
         * Sets the "IDScoreResultCode6" element
         */
        public void setIDScoreResultCode6(java.lang.String idScoreResultCode6)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDSCORERESULTCODE6$34, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDSCORERESULTCODE6$34);
                }
                target.setStringValue(idScoreResultCode6);
            }
        }
        
        /**
         * Sets (as xml) the "IDScoreResultCode6" element
         */
        public void xsetIDScoreResultCode6(com.idanalytics.products.idscore.result.IDScoreResultCode6Document.IDScoreResultCode6 idScoreResultCode6)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.IDScoreResultCode6Document.IDScoreResultCode6 target = null;
                target = (com.idanalytics.products.idscore.result.IDScoreResultCode6Document.IDScoreResultCode6)get_store().find_element_user(IDSCORERESULTCODE6$34, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result.IDScoreResultCode6Document.IDScoreResultCode6)get_store().add_element_user(IDSCORERESULTCODE6$34);
                }
                target.set(idScoreResultCode6);
            }
        }
        
        /**
         * Unsets the "IDScoreResultCode6" element
         */
        public void unsetIDScoreResultCode6()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(IDSCORERESULTCODE6$34, 0);
            }
        }
        
        /**
         * Gets the "ResultCode1" element
         */
        public com.idanalytics.products.idscore.result.ResultCodeType getResultCode1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.ResultCodeType target = null;
                target = (com.idanalytics.products.idscore.result.ResultCodeType)get_store().find_element_user(RESULTCODE1$36, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "ResultCode1" element
         */
        public boolean isSetResultCode1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(RESULTCODE1$36) != 0;
            }
        }
        
        /**
         * Sets the "ResultCode1" element
         */
        public void setResultCode1(com.idanalytics.products.idscore.result.ResultCodeType resultCode1)
        {
            generatedSetterHelperImpl(resultCode1, RESULTCODE1$36, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "ResultCode1" element
         */
        public com.idanalytics.products.idscore.result.ResultCodeType addNewResultCode1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.ResultCodeType target = null;
                target = (com.idanalytics.products.idscore.result.ResultCodeType)get_store().add_element_user(RESULTCODE1$36);
                return target;
            }
        }
        
        /**
         * Unsets the "ResultCode1" element
         */
        public void unsetResultCode1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(RESULTCODE1$36, 0);
            }
        }
        
        /**
         * Gets the "ResultCode2" element
         */
        public com.idanalytics.products.idscore.result.ResultCodeType getResultCode2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.ResultCodeType target = null;
                target = (com.idanalytics.products.idscore.result.ResultCodeType)get_store().find_element_user(RESULTCODE2$38, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "ResultCode2" element
         */
        public boolean isSetResultCode2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(RESULTCODE2$38) != 0;
            }
        }
        
        /**
         * Sets the "ResultCode2" element
         */
        public void setResultCode2(com.idanalytics.products.idscore.result.ResultCodeType resultCode2)
        {
            generatedSetterHelperImpl(resultCode2, RESULTCODE2$38, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "ResultCode2" element
         */
        public com.idanalytics.products.idscore.result.ResultCodeType addNewResultCode2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.ResultCodeType target = null;
                target = (com.idanalytics.products.idscore.result.ResultCodeType)get_store().add_element_user(RESULTCODE2$38);
                return target;
            }
        }
        
        /**
         * Unsets the "ResultCode2" element
         */
        public void unsetResultCode2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(RESULTCODE2$38, 0);
            }
        }
        
        /**
         * Gets the "ResultCode3" element
         */
        public com.idanalytics.products.idscore.result.ResultCodeType getResultCode3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.ResultCodeType target = null;
                target = (com.idanalytics.products.idscore.result.ResultCodeType)get_store().find_element_user(RESULTCODE3$40, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "ResultCode3" element
         */
        public boolean isSetResultCode3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(RESULTCODE3$40) != 0;
            }
        }
        
        /**
         * Sets the "ResultCode3" element
         */
        public void setResultCode3(com.idanalytics.products.idscore.result.ResultCodeType resultCode3)
        {
            generatedSetterHelperImpl(resultCode3, RESULTCODE3$40, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "ResultCode3" element
         */
        public com.idanalytics.products.idscore.result.ResultCodeType addNewResultCode3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.ResultCodeType target = null;
                target = (com.idanalytics.products.idscore.result.ResultCodeType)get_store().add_element_user(RESULTCODE3$40);
                return target;
            }
        }
        
        /**
         * Unsets the "ResultCode3" element
         */
        public void unsetResultCode3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(RESULTCODE3$40, 0);
            }
        }
        
        /**
         * Gets the "ResultCode4" element
         */
        public com.idanalytics.products.idscore.result.ResultCodeType getResultCode4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.ResultCodeType target = null;
                target = (com.idanalytics.products.idscore.result.ResultCodeType)get_store().find_element_user(RESULTCODE4$42, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "ResultCode4" element
         */
        public boolean isSetResultCode4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(RESULTCODE4$42) != 0;
            }
        }
        
        /**
         * Sets the "ResultCode4" element
         */
        public void setResultCode4(com.idanalytics.products.idscore.result.ResultCodeType resultCode4)
        {
            generatedSetterHelperImpl(resultCode4, RESULTCODE4$42, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "ResultCode4" element
         */
        public com.idanalytics.products.idscore.result.ResultCodeType addNewResultCode4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.ResultCodeType target = null;
                target = (com.idanalytics.products.idscore.result.ResultCodeType)get_store().add_element_user(RESULTCODE4$42);
                return target;
            }
        }
        
        /**
         * Unsets the "ResultCode4" element
         */
        public void unsetResultCode4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(RESULTCODE4$42, 0);
            }
        }
        
        /**
         * Gets the "ResultCode5" element
         */
        public com.idanalytics.products.idscore.result.ResultCodeType getResultCode5()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.ResultCodeType target = null;
                target = (com.idanalytics.products.idscore.result.ResultCodeType)get_store().find_element_user(RESULTCODE5$44, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "ResultCode5" element
         */
        public boolean isSetResultCode5()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(RESULTCODE5$44) != 0;
            }
        }
        
        /**
         * Sets the "ResultCode5" element
         */
        public void setResultCode5(com.idanalytics.products.idscore.result.ResultCodeType resultCode5)
        {
            generatedSetterHelperImpl(resultCode5, RESULTCODE5$44, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "ResultCode5" element
         */
        public com.idanalytics.products.idscore.result.ResultCodeType addNewResultCode5()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.ResultCodeType target = null;
                target = (com.idanalytics.products.idscore.result.ResultCodeType)get_store().add_element_user(RESULTCODE5$44);
                return target;
            }
        }
        
        /**
         * Unsets the "ResultCode5" element
         */
        public void unsetResultCode5()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(RESULTCODE5$44, 0);
            }
        }
        
        /**
         * Gets the "ResultCode6" element
         */
        public com.idanalytics.products.idscore.result.ResultCodeType getResultCode6()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.ResultCodeType target = null;
                target = (com.idanalytics.products.idscore.result.ResultCodeType)get_store().find_element_user(RESULTCODE6$46, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "ResultCode6" element
         */
        public boolean isSetResultCode6()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(RESULTCODE6$46) != 0;
            }
        }
        
        /**
         * Sets the "ResultCode6" element
         */
        public void setResultCode6(com.idanalytics.products.idscore.result.ResultCodeType resultCode6)
        {
            generatedSetterHelperImpl(resultCode6, RESULTCODE6$46, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "ResultCode6" element
         */
        public com.idanalytics.products.idscore.result.ResultCodeType addNewResultCode6()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.ResultCodeType target = null;
                target = (com.idanalytics.products.idscore.result.ResultCodeType)get_store().add_element_user(RESULTCODE6$46);
                return target;
            }
        }
        
        /**
         * Unsets the "ResultCode6" element
         */
        public void unsetResultCode6()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(RESULTCODE6$46, 0);
            }
        }
        
        /**
         * Gets the "ConsumerStatements" element
         */
        public com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.ConsumerStatements getConsumerStatements()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.ConsumerStatements target = null;
                target = (com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.ConsumerStatements)get_store().find_element_user(CONSUMERSTATEMENTS$48, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "ConsumerStatements" element
         */
        public boolean isSetConsumerStatements()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(CONSUMERSTATEMENTS$48) != 0;
            }
        }
        
        /**
         * Sets the "ConsumerStatements" element
         */
        public void setConsumerStatements(com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.ConsumerStatements consumerStatements)
        {
            generatedSetterHelperImpl(consumerStatements, CONSUMERSTATEMENTS$48, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "ConsumerStatements" element
         */
        public com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.ConsumerStatements addNewConsumerStatements()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.ConsumerStatements target = null;
                target = (com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.ConsumerStatements)get_store().add_element_user(CONSUMERSTATEMENTS$48);
                return target;
            }
        }
        
        /**
         * Unsets the "ConsumerStatements" element
         */
        public void unsetConsumerStatements()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(CONSUMERSTATEMENTS$48, 0);
            }
        }
        
        /**
         * Gets the "Messages" element
         */
        public com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.Messages getMessages()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.Messages target = null;
                target = (com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.Messages)get_store().find_element_user(MESSAGES$50, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "Messages" element
         */
        public boolean isSetMessages()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(MESSAGES$50) != 0;
            }
        }
        
        /**
         * Sets the "Messages" element
         */
        public void setMessages(com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.Messages messages)
        {
            generatedSetterHelperImpl(messages, MESSAGES$50, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "Messages" element
         */
        public com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.Messages addNewMessages()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.Messages target = null;
                target = (com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.Messages)get_store().add_element_user(MESSAGES$50);
                return target;
            }
        }
        
        /**
         * Unsets the "Messages" element
         */
        public void unsetMessages()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(MESSAGES$50, 0);
            }
        }
        
        /**
         * Gets the "Signals" element
         */
        public com.idanalytics.products.idscore.result.SignalsDocument.Signals getSignals()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.SignalsDocument.Signals target = null;
                target = (com.idanalytics.products.idscore.result.SignalsDocument.Signals)get_store().find_element_user(SIGNALS$52, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "Signals" element
         */
        public boolean isSetSignals()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(SIGNALS$52) != 0;
            }
        }
        
        /**
         * Sets the "Signals" element
         */
        public void setSignals(com.idanalytics.products.idscore.result.SignalsDocument.Signals signals)
        {
            generatedSetterHelperImpl(signals, SIGNALS$52, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "Signals" element
         */
        public com.idanalytics.products.idscore.result.SignalsDocument.Signals addNewSignals()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.SignalsDocument.Signals target = null;
                target = (com.idanalytics.products.idscore.result.SignalsDocument.Signals)get_store().add_element_user(SIGNALS$52);
                return target;
            }
        }
        
        /**
         * Unsets the "Signals" element
         */
        public void unsetSignals()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(SIGNALS$52, 0);
            }
        }
        
        /**
         * Gets the "PassThru1" element
         */
        public java.lang.String getPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU1$54, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru1" element
         */
        public com.idanalytics.products.idscore.result.PassThru1Document.PassThru1 xgetPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.PassThru1Document.PassThru1 target = null;
                target = (com.idanalytics.products.idscore.result.PassThru1Document.PassThru1)get_store().find_element_user(PASSTHRU1$54, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru1" element
         */
        public boolean isSetPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU1$54) != 0;
            }
        }
        
        /**
         * Sets the "PassThru1" element
         */
        public void setPassThru1(java.lang.String passThru1)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU1$54, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU1$54);
                }
                target.setStringValue(passThru1);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru1" element
         */
        public void xsetPassThru1(com.idanalytics.products.idscore.result.PassThru1Document.PassThru1 passThru1)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.PassThru1Document.PassThru1 target = null;
                target = (com.idanalytics.products.idscore.result.PassThru1Document.PassThru1)get_store().find_element_user(PASSTHRU1$54, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result.PassThru1Document.PassThru1)get_store().add_element_user(PASSTHRU1$54);
                }
                target.set(passThru1);
            }
        }
        
        /**
         * Unsets the "PassThru1" element
         */
        public void unsetPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU1$54, 0);
            }
        }
        
        /**
         * Gets the "PassThru2" element
         */
        public java.lang.String getPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU2$56, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru2" element
         */
        public com.idanalytics.products.idscore.result.PassThru2Document.PassThru2 xgetPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.PassThru2Document.PassThru2 target = null;
                target = (com.idanalytics.products.idscore.result.PassThru2Document.PassThru2)get_store().find_element_user(PASSTHRU2$56, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru2" element
         */
        public boolean isSetPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU2$56) != 0;
            }
        }
        
        /**
         * Sets the "PassThru2" element
         */
        public void setPassThru2(java.lang.String passThru2)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU2$56, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU2$56);
                }
                target.setStringValue(passThru2);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru2" element
         */
        public void xsetPassThru2(com.idanalytics.products.idscore.result.PassThru2Document.PassThru2 passThru2)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.PassThru2Document.PassThru2 target = null;
                target = (com.idanalytics.products.idscore.result.PassThru2Document.PassThru2)get_store().find_element_user(PASSTHRU2$56, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result.PassThru2Document.PassThru2)get_store().add_element_user(PASSTHRU2$56);
                }
                target.set(passThru2);
            }
        }
        
        /**
         * Unsets the "PassThru2" element
         */
        public void unsetPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU2$56, 0);
            }
        }
        
        /**
         * Gets the "PassThru3" element
         */
        public java.lang.String getPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU3$58, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru3" element
         */
        public com.idanalytics.products.idscore.result.PassThru3Document.PassThru3 xgetPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.PassThru3Document.PassThru3 target = null;
                target = (com.idanalytics.products.idscore.result.PassThru3Document.PassThru3)get_store().find_element_user(PASSTHRU3$58, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru3" element
         */
        public boolean isSetPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU3$58) != 0;
            }
        }
        
        /**
         * Sets the "PassThru3" element
         */
        public void setPassThru3(java.lang.String passThru3)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU3$58, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU3$58);
                }
                target.setStringValue(passThru3);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru3" element
         */
        public void xsetPassThru3(com.idanalytics.products.idscore.result.PassThru3Document.PassThru3 passThru3)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.PassThru3Document.PassThru3 target = null;
                target = (com.idanalytics.products.idscore.result.PassThru3Document.PassThru3)get_store().find_element_user(PASSTHRU3$58, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result.PassThru3Document.PassThru3)get_store().add_element_user(PASSTHRU3$58);
                }
                target.set(passThru3);
            }
        }
        
        /**
         * Unsets the "PassThru3" element
         */
        public void unsetPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU3$58, 0);
            }
        }
        
        /**
         * Gets the "PassThru4" element
         */
        public java.lang.String getPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU4$60, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru4" element
         */
        public com.idanalytics.products.idscore.result.PassThru4Document.PassThru4 xgetPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.PassThru4Document.PassThru4 target = null;
                target = (com.idanalytics.products.idscore.result.PassThru4Document.PassThru4)get_store().find_element_user(PASSTHRU4$60, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru4" element
         */
        public boolean isSetPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU4$60) != 0;
            }
        }
        
        /**
         * Sets the "PassThru4" element
         */
        public void setPassThru4(java.lang.String passThru4)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU4$60, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU4$60);
                }
                target.setStringValue(passThru4);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru4" element
         */
        public void xsetPassThru4(com.idanalytics.products.idscore.result.PassThru4Document.PassThru4 passThru4)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.PassThru4Document.PassThru4 target = null;
                target = (com.idanalytics.products.idscore.result.PassThru4Document.PassThru4)get_store().find_element_user(PASSTHRU4$60, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.idscore.result.PassThru4Document.PassThru4)get_store().add_element_user(PASSTHRU4$60);
                }
                target.set(passThru4);
            }
        }
        
        /**
         * Unsets the "PassThru4" element
         */
        public void unsetPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU4$60, 0);
            }
        }
        
        /**
         * Gets the "Indicators" element
         */
        public com.idanalytics.products.idscore.result.IndicatorsDocument.Indicators getIndicators()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.IndicatorsDocument.Indicators target = null;
                target = (com.idanalytics.products.idscore.result.IndicatorsDocument.Indicators)get_store().find_element_user(INDICATORS$62, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "Indicators" element
         */
        public boolean isSetIndicators()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(INDICATORS$62) != 0;
            }
        }
        
        /**
         * Sets the "Indicators" element
         */
        public void setIndicators(com.idanalytics.products.idscore.result.IndicatorsDocument.Indicators indicators)
        {
            generatedSetterHelperImpl(indicators, INDICATORS$62, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "Indicators" element
         */
        public com.idanalytics.products.idscore.result.IndicatorsDocument.Indicators addNewIndicators()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.IndicatorsDocument.Indicators target = null;
                target = (com.idanalytics.products.idscore.result.IndicatorsDocument.Indicators)get_store().add_element_user(INDICATORS$62);
                return target;
            }
        }
        
        /**
         * Unsets the "Indicators" element
         */
        public void unsetIndicators()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(INDICATORS$62, 0);
            }
        }
        
        /**
         * Gets the "IDAInternal" element
         */
        public com.idanalytics.products.idscore.result.IDAInternalDocument.IDAInternal getIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.IDAInternalDocument.IDAInternal target = null;
                target = (com.idanalytics.products.idscore.result.IDAInternalDocument.IDAInternal)get_store().find_element_user(IDAINTERNAL$64, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "IDAInternal" element
         */
        public boolean isSetIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(IDAINTERNAL$64) != 0;
            }
        }
        
        /**
         * Sets the "IDAInternal" element
         */
        public void setIDAInternal(com.idanalytics.products.idscore.result.IDAInternalDocument.IDAInternal idaInternal)
        {
            generatedSetterHelperImpl(idaInternal, IDAINTERNAL$64, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "IDAInternal" element
         */
        public com.idanalytics.products.idscore.result.IDAInternalDocument.IDAInternal addNewIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.IDAInternalDocument.IDAInternal target = null;
                target = (com.idanalytics.products.idscore.result.IDAInternalDocument.IDAInternal)get_store().add_element_user(IDAINTERNAL$64);
                return target;
            }
        }
        
        /**
         * Unsets the "IDAInternal" element
         */
        public void unsetIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(IDAINTERNAL$64, 0);
            }
        }
        
        /**
         * Gets the "PreviousEvents" element
         */
        public com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.PreviousEvents getPreviousEvents()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.PreviousEvents target = null;
                target = (com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.PreviousEvents)get_store().find_element_user(PREVIOUSEVENTS$66, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "PreviousEvents" element
         */
        public boolean isSetPreviousEvents()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PREVIOUSEVENTS$66) != 0;
            }
        }
        
        /**
         * Sets the "PreviousEvents" element
         */
        public void setPreviousEvents(com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.PreviousEvents previousEvents)
        {
            generatedSetterHelperImpl(previousEvents, PREVIOUSEVENTS$66, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "PreviousEvents" element
         */
        public com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.PreviousEvents addNewPreviousEvents()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.PreviousEvents target = null;
                target = (com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.PreviousEvents)get_store().add_element_user(PREVIOUSEVENTS$66);
                return target;
            }
        }
        
        /**
         * Unsets the "PreviousEvents" element
         */
        public void unsetPreviousEvents()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PREVIOUSEVENTS$66, 0);
            }
        }
        
        /**
         * Gets the "schemaVersion" attribute
         */
        public java.math.BigDecimal getSchemaVersion()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(SCHEMAVERSION$68);
                if (target == null)
                {
                    return null;
                }
                return target.getBigDecimalValue();
            }
        }
        
        /**
         * Gets (as xml) the "schemaVersion" attribute
         */
        public org.apache.xmlbeans.XmlDecimal xgetSchemaVersion()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlDecimal target = null;
                target = (org.apache.xmlbeans.XmlDecimal)get_store().find_attribute_user(SCHEMAVERSION$68);
                return target;
            }
        }
        
        /**
         * True if has "schemaVersion" attribute
         */
        public boolean isSetSchemaVersion()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().find_attribute_user(SCHEMAVERSION$68) != null;
            }
        }
        
        /**
         * Sets the "schemaVersion" attribute
         */
        public void setSchemaVersion(java.math.BigDecimal schemaVersion)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(SCHEMAVERSION$68);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(SCHEMAVERSION$68);
                }
                target.setBigDecimalValue(schemaVersion);
            }
        }
        
        /**
         * Sets (as xml) the "schemaVersion" attribute
         */
        public void xsetSchemaVersion(org.apache.xmlbeans.XmlDecimal schemaVersion)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlDecimal target = null;
                target = (org.apache.xmlbeans.XmlDecimal)get_store().find_attribute_user(SCHEMAVERSION$68);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlDecimal)get_store().add_attribute_user(SCHEMAVERSION$68);
                }
                target.set(schemaVersion);
            }
        }
        
        /**
         * Unsets the "schemaVersion" attribute
         */
        public void unsetSchemaVersion()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_attribute(SCHEMAVERSION$68);
            }
        }
        /**
         * An XML ConsumerStatements(@http://idanalytics.com/products/idscore/result).
         *
         * This is a complex type.
         */
        public static class ConsumerStatementsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.ConsumerStatements
        {
            private static final long serialVersionUID = 1L;
            
            public ConsumerStatementsImpl(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType);
            }
            
            private static final javax.xml.namespace.QName CONSUMERSTATEMENT$0 = 
                new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "ConsumerStatement");
            
            
            /**
             * Gets array of all "ConsumerStatement" elements
             */
            public com.idanalytics.products.idscore.result.ConsumerStatementType[] getConsumerStatementArray()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    java.util.List targetList = new java.util.ArrayList();
                    get_store().find_all_element_users(CONSUMERSTATEMENT$0, targetList);
                    com.idanalytics.products.idscore.result.ConsumerStatementType[] result = new com.idanalytics.products.idscore.result.ConsumerStatementType[targetList.size()];
                    targetList.toArray(result);
                    return result;
                }
            }
            
            /**
             * Gets ith "ConsumerStatement" element
             */
            public com.idanalytics.products.idscore.result.ConsumerStatementType getConsumerStatementArray(int i)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    com.idanalytics.products.idscore.result.ConsumerStatementType target = null;
                    target = (com.idanalytics.products.idscore.result.ConsumerStatementType)get_store().find_element_user(CONSUMERSTATEMENT$0, i);
                    if (target == null)
                    {
                      throw new IndexOutOfBoundsException();
                    }
                    return target;
                }
            }
            
            /**
             * Returns number of "ConsumerStatement" element
             */
            public int sizeOfConsumerStatementArray()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    return get_store().count_elements(CONSUMERSTATEMENT$0);
                }
            }
            
            /**
             * Sets array of all "ConsumerStatement" element  WARNING: This method is not atomicaly synchronized.
             */
            public void setConsumerStatementArray(com.idanalytics.products.idscore.result.ConsumerStatementType[] consumerStatementArray)
            {
                check_orphaned();
                arraySetterHelper(consumerStatementArray, CONSUMERSTATEMENT$0);
            }
            
            /**
             * Sets ith "ConsumerStatement" element
             */
            public void setConsumerStatementArray(int i, com.idanalytics.products.idscore.result.ConsumerStatementType consumerStatement)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    com.idanalytics.products.idscore.result.ConsumerStatementType target = null;
                    target = (com.idanalytics.products.idscore.result.ConsumerStatementType)get_store().find_element_user(CONSUMERSTATEMENT$0, i);
                    if (target == null)
                    {
                      throw new IndexOutOfBoundsException();
                    }
                    target.set(consumerStatement);
                }
            }
            
            /**
             * Inserts and returns a new empty value (as xml) as the ith "ConsumerStatement" element
             */
            public com.idanalytics.products.idscore.result.ConsumerStatementType insertNewConsumerStatement(int i)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    com.idanalytics.products.idscore.result.ConsumerStatementType target = null;
                    target = (com.idanalytics.products.idscore.result.ConsumerStatementType)get_store().insert_element_user(CONSUMERSTATEMENT$0, i);
                    return target;
                }
            }
            
            /**
             * Appends and returns a new empty value (as xml) as the last "ConsumerStatement" element
             */
            public com.idanalytics.products.idscore.result.ConsumerStatementType addNewConsumerStatement()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    com.idanalytics.products.idscore.result.ConsumerStatementType target = null;
                    target = (com.idanalytics.products.idscore.result.ConsumerStatementType)get_store().add_element_user(CONSUMERSTATEMENT$0);
                    return target;
                }
            }
            
            /**
             * Removes the ith "ConsumerStatement" element
             */
            public void removeConsumerStatement(int i)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    get_store().remove_element(CONSUMERSTATEMENT$0, i);
                }
            }
        }
        /**
         * An XML Messages(@http://idanalytics.com/products/idscore/result).
         *
         * This is a complex type.
         */
        public static class MessagesImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.Messages
        {
            private static final long serialVersionUID = 1L;
            
            public MessagesImpl(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType);
            }
            
            private static final javax.xml.namespace.QName MESSAGE$0 = 
                new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "Message");
            
            
            /**
             * Gets array of all "Message" elements
             */
            public com.idanalytics.products.idscore.result.MessageType[] getMessageArray()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    java.util.List targetList = new java.util.ArrayList();
                    get_store().find_all_element_users(MESSAGE$0, targetList);
                    com.idanalytics.products.idscore.result.MessageType[] result = new com.idanalytics.products.idscore.result.MessageType[targetList.size()];
                    targetList.toArray(result);
                    return result;
                }
            }
            
            /**
             * Gets ith "Message" element
             */
            public com.idanalytics.products.idscore.result.MessageType getMessageArray(int i)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    com.idanalytics.products.idscore.result.MessageType target = null;
                    target = (com.idanalytics.products.idscore.result.MessageType)get_store().find_element_user(MESSAGE$0, i);
                    if (target == null)
                    {
                      throw new IndexOutOfBoundsException();
                    }
                    return target;
                }
            }
            
            /**
             * Returns number of "Message" element
             */
            public int sizeOfMessageArray()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    return get_store().count_elements(MESSAGE$0);
                }
            }
            
            /**
             * Sets array of all "Message" element  WARNING: This method is not atomicaly synchronized.
             */
            public void setMessageArray(com.idanalytics.products.idscore.result.MessageType[] messageArray)
            {
                check_orphaned();
                arraySetterHelper(messageArray, MESSAGE$0);
            }
            
            /**
             * Sets ith "Message" element
             */
            public void setMessageArray(int i, com.idanalytics.products.idscore.result.MessageType message)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    com.idanalytics.products.idscore.result.MessageType target = null;
                    target = (com.idanalytics.products.idscore.result.MessageType)get_store().find_element_user(MESSAGE$0, i);
                    if (target == null)
                    {
                      throw new IndexOutOfBoundsException();
                    }
                    target.set(message);
                }
            }
            
            /**
             * Inserts and returns a new empty value (as xml) as the ith "Message" element
             */
            public com.idanalytics.products.idscore.result.MessageType insertNewMessage(int i)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    com.idanalytics.products.idscore.result.MessageType target = null;
                    target = (com.idanalytics.products.idscore.result.MessageType)get_store().insert_element_user(MESSAGE$0, i);
                    return target;
                }
            }
            
            /**
             * Appends and returns a new empty value (as xml) as the last "Message" element
             */
            public com.idanalytics.products.idscore.result.MessageType addNewMessage()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    com.idanalytics.products.idscore.result.MessageType target = null;
                    target = (com.idanalytics.products.idscore.result.MessageType)get_store().add_element_user(MESSAGE$0);
                    return target;
                }
            }
            
            /**
             * Removes the ith "Message" element
             */
            public void removeMessage(int i)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    get_store().remove_element(MESSAGE$0, i);
                }
            }
        }
        /**
         * An XML PreviousEvents(@http://idanalytics.com/products/idscore/result).
         *
         * This is a complex type.
         */
        public static class PreviousEventsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord.PreviousEvents
        {
            private static final long serialVersionUID = 1L;
            
            public PreviousEventsImpl(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType);
            }
            
            private static final javax.xml.namespace.QName PREVIOUSEVENT$0 = 
                new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result", "PreviousEvent");
            
            
            /**
             * Gets array of all "PreviousEvent" elements
             */
            public com.idanalytics.products.idscore.result.IdentityEvent[] getPreviousEventArray()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    java.util.List targetList = new java.util.ArrayList();
                    get_store().find_all_element_users(PREVIOUSEVENT$0, targetList);
                    com.idanalytics.products.idscore.result.IdentityEvent[] result = new com.idanalytics.products.idscore.result.IdentityEvent[targetList.size()];
                    targetList.toArray(result);
                    return result;
                }
            }
            
            /**
             * Gets ith "PreviousEvent" element
             */
            public com.idanalytics.products.idscore.result.IdentityEvent getPreviousEventArray(int i)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    com.idanalytics.products.idscore.result.IdentityEvent target = null;
                    target = (com.idanalytics.products.idscore.result.IdentityEvent)get_store().find_element_user(PREVIOUSEVENT$0, i);
                    if (target == null)
                    {
                      throw new IndexOutOfBoundsException();
                    }
                    return target;
                }
            }
            
            /**
             * Returns number of "PreviousEvent" element
             */
            public int sizeOfPreviousEventArray()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    return get_store().count_elements(PREVIOUSEVENT$0);
                }
            }
            
            /**
             * Sets array of all "PreviousEvent" element  WARNING: This method is not atomicaly synchronized.
             */
            public void setPreviousEventArray(com.idanalytics.products.idscore.result.IdentityEvent[] previousEventArray)
            {
                check_orphaned();
                arraySetterHelper(previousEventArray, PREVIOUSEVENT$0);
            }
            
            /**
             * Sets ith "PreviousEvent" element
             */
            public void setPreviousEventArray(int i, com.idanalytics.products.idscore.result.IdentityEvent previousEvent)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    com.idanalytics.products.idscore.result.IdentityEvent target = null;
                    target = (com.idanalytics.products.idscore.result.IdentityEvent)get_store().find_element_user(PREVIOUSEVENT$0, i);
                    if (target == null)
                    {
                      throw new IndexOutOfBoundsException();
                    }
                    target.set(previousEvent);
                }
            }
            
            /**
             * Inserts and returns a new empty value (as xml) as the ith "PreviousEvent" element
             */
            public com.idanalytics.products.idscore.result.IdentityEvent insertNewPreviousEvent(int i)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    com.idanalytics.products.idscore.result.IdentityEvent target = null;
                    target = (com.idanalytics.products.idscore.result.IdentityEvent)get_store().insert_element_user(PREVIOUSEVENT$0, i);
                    return target;
                }
            }
            
            /**
             * Appends and returns a new empty value (as xml) as the last "PreviousEvent" element
             */
            public com.idanalytics.products.idscore.result.IdentityEvent addNewPreviousEvent()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    com.idanalytics.products.idscore.result.IdentityEvent target = null;
                    target = (com.idanalytics.products.idscore.result.IdentityEvent)get_store().add_element_user(PREVIOUSEVENT$0);
                    return target;
                }
            }
            
            /**
             * Removes the ith "PreviousEvent" element
             */
            public void removePreviousEvent(int i)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    get_store().remove_element(PREVIOUSEVENT$0, i);
                }
            }
        }
    }
}
