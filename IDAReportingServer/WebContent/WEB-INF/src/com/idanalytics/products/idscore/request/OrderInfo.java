/*
 * XML Type:  OrderInfo
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.OrderInfo
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request;


/**
 * An XML OrderInfo(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public interface OrderInfo extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(OrderInfo.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("orderinfo21c2type");
    
    /**
     * Gets the "MerchantID" element
     */
    java.lang.String getMerchantID();
    
    /**
     * Gets (as xml) the "MerchantID" element
     */
    com.idanalytics.products.idscore.request.MerchantIDDocument.MerchantID xgetMerchantID();
    
    /**
     * Sets the "MerchantID" element
     */
    void setMerchantID(java.lang.String merchantID);
    
    /**
     * Sets (as xml) the "MerchantID" element
     */
    void xsetMerchantID(com.idanalytics.products.idscore.request.MerchantIDDocument.MerchantID merchantID);
    
    /**
     * Gets the "OrderDate" element
     */
    java.util.Calendar getOrderDate();
    
    /**
     * Gets (as xml) the "OrderDate" element
     */
    org.apache.xmlbeans.XmlDateTime xgetOrderDate();
    
    /**
     * Sets the "OrderDate" element
     */
    void setOrderDate(java.util.Calendar orderDate);
    
    /**
     * Sets (as xml) the "OrderDate" element
     */
    void xsetOrderDate(org.apache.xmlbeans.XmlDateTime orderDate);
    
    /**
     * Gets the "OrderNumber" element
     */
    java.lang.String getOrderNumber();
    
    /**
     * Gets (as xml) the "OrderNumber" element
     */
    com.idanalytics.products.idscore.request.OrderNumberDocument.OrderNumber xgetOrderNumber();
    
    /**
     * True if has "OrderNumber" element
     */
    boolean isSetOrderNumber();
    
    /**
     * Sets the "OrderNumber" element
     */
    void setOrderNumber(java.lang.String orderNumber);
    
    /**
     * Sets (as xml) the "OrderNumber" element
     */
    void xsetOrderNumber(com.idanalytics.products.idscore.request.OrderNumberDocument.OrderNumber orderNumber);
    
    /**
     * Unsets the "OrderNumber" element
     */
    void unsetOrderNumber();
    
    /**
     * Gets the "OrderAmount" element
     */
    java.math.BigDecimal getOrderAmount();
    
    /**
     * Gets (as xml) the "OrderAmount" element
     */
    com.idanalytics.products.idscore.request.OrderAmountDocument.OrderAmount xgetOrderAmount();
    
    /**
     * Sets the "OrderAmount" element
     */
    void setOrderAmount(java.math.BigDecimal orderAmount);
    
    /**
     * Sets (as xml) the "OrderAmount" element
     */
    void xsetOrderAmount(com.idanalytics.products.idscore.request.OrderAmountDocument.OrderAmount orderAmount);
    
    /**
     * Gets the "OrderTax" element
     */
    java.lang.Object getOrderTax();
    
    /**
     * Gets (as xml) the "OrderTax" element
     */
    com.idanalytics.products.common_v1.OptionalDecimal xgetOrderTax();
    
    /**
     * True if has "OrderTax" element
     */
    boolean isSetOrderTax();
    
    /**
     * Sets the "OrderTax" element
     */
    void setOrderTax(java.lang.Object orderTax);
    
    /**
     * Sets (as xml) the "OrderTax" element
     */
    void xsetOrderTax(com.idanalytics.products.common_v1.OptionalDecimal orderTax);
    
    /**
     * Unsets the "OrderTax" element
     */
    void unsetOrderTax();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.idscore.request.OrderInfo newInstance() {
          return (com.idanalytics.products.idscore.request.OrderInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.idscore.request.OrderInfo newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.idscore.request.OrderInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.idscore.request.OrderInfo parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.OrderInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.idscore.request.OrderInfo parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.OrderInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.idscore.request.OrderInfo parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.OrderInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.idscore.request.OrderInfo parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.OrderInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.idscore.request.OrderInfo parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.OrderInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.idscore.request.OrderInfo parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.OrderInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.idscore.request.OrderInfo parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.OrderInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.idscore.request.OrderInfo parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.OrderInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.idscore.request.OrderInfo parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.OrderInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.idscore.request.OrderInfo parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.OrderInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.idscore.request.OrderInfo parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.OrderInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.idscore.request.OrderInfo parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.OrderInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.idscore.request.OrderInfo parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.OrderInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.idscore.request.OrderInfo parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.OrderInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.request.OrderInfo parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.request.OrderInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.request.OrderInfo parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.request.OrderInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
