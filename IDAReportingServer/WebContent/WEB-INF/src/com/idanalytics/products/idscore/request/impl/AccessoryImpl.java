/*
 * XML Type:  Accessory
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.Accessory
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML Accessory(@http://idanalytics.com/products/idscore/request).
 *
 * This is a union type. Instances are of one of the following types:
 *     com.idanalytics.products.idscore.request.Accessory$Member
 *     com.idanalytics.products.idscore.request.Accessory$Member2
 */
public class AccessoryImpl extends org.apache.xmlbeans.impl.values.XmlUnionImpl implements com.idanalytics.products.idscore.request.Accessory, com.idanalytics.products.idscore.request.Accessory.Member, com.idanalytics.products.idscore.request.Accessory.Member2
{
    private static final long serialVersionUID = 1L;
    
    public AccessoryImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected AccessoryImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
    /**
     * An anonymous inner XML type.
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.Accessory$Member.
     */
    public static class MemberImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.request.Accessory.Member
    {
        private static final long serialVersionUID = 1L;
        
        public MemberImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected MemberImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
    /**
     * An anonymous inner XML type.
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.request.Accessory$Member2.
     */
    public static class MemberImpl2 extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.idanalytics.products.idscore.request.Accessory.Member2
    {
        private static final long serialVersionUID = 1L;
        
        public MemberImpl2(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected MemberImpl2(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
