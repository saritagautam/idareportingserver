/*
 * XML Type:  Application
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.Application
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request;


/**
 * An XML Application(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public interface Application extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Application.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("application5c36type");
    
    /**
     * Gets the "Channel" element
     */
    java.lang.String getChannel();
    
    /**
     * Gets (as xml) the "Channel" element
     */
    com.idanalytics.products.idscore.request.ChannelDocument.Channel xgetChannel();
    
    /**
     * Sets the "Channel" element
     */
    void setChannel(java.lang.String channel);
    
    /**
     * Sets (as xml) the "Channel" element
     */
    void xsetChannel(com.idanalytics.products.idscore.request.ChannelDocument.Channel channel);
    
    /**
     * Gets the "AcquisitionMethod" element
     */
    java.lang.String getAcquisitionMethod();
    
    /**
     * Gets (as xml) the "AcquisitionMethod" element
     */
    com.idanalytics.products.idscore.request.AcquisitionMethodDocument.AcquisitionMethod xgetAcquisitionMethod();
    
    /**
     * True if has "AcquisitionMethod" element
     */
    boolean isSetAcquisitionMethod();
    
    /**
     * Sets the "AcquisitionMethod" element
     */
    void setAcquisitionMethod(java.lang.String acquisitionMethod);
    
    /**
     * Sets (as xml) the "AcquisitionMethod" element
     */
    void xsetAcquisitionMethod(com.idanalytics.products.idscore.request.AcquisitionMethodDocument.AcquisitionMethod acquisitionMethod);
    
    /**
     * Unsets the "AcquisitionMethod" element
     */
    void unsetAcquisitionMethod();
    
    /**
     * Gets the "AgentLoc" element
     */
    java.lang.String getAgentLoc();
    
    /**
     * Gets (as xml) the "AgentLoc" element
     */
    com.idanalytics.products.idscore.request.AgentLocDocument.AgentLoc xgetAgentLoc();
    
    /**
     * True if has "AgentLoc" element
     */
    boolean isSetAgentLoc();
    
    /**
     * Sets the "AgentLoc" element
     */
    void setAgentLoc(java.lang.String agentLoc);
    
    /**
     * Sets (as xml) the "AgentLoc" element
     */
    void xsetAgentLoc(com.idanalytics.products.idscore.request.AgentLocDocument.AgentLoc agentLoc);
    
    /**
     * Unsets the "AgentLoc" element
     */
    void unsetAgentLoc();
    
    /**
     * Gets the "AgentID" element
     */
    java.lang.String getAgentID();
    
    /**
     * Gets (as xml) the "AgentID" element
     */
    com.idanalytics.products.idscore.request.AgentIDDocument.AgentID xgetAgentID();
    
    /**
     * True if has "AgentID" element
     */
    boolean isSetAgentID();
    
    /**
     * Sets the "AgentID" element
     */
    void setAgentID(java.lang.String agentID);
    
    /**
     * Sets (as xml) the "AgentID" element
     */
    void xsetAgentID(com.idanalytics.products.idscore.request.AgentIDDocument.AgentID agentID);
    
    /**
     * Unsets the "AgentID" element
     */
    void unsetAgentID();
    
    /**
     * Gets the "SourceIP" element
     */
    java.lang.String getSourceIP();
    
    /**
     * Gets (as xml) the "SourceIP" element
     */
    com.idanalytics.products.idscore.request.SourceIPDocument.SourceIP xgetSourceIP();
    
    /**
     * True if has "SourceIP" element
     */
    boolean isSetSourceIP();
    
    /**
     * Sets the "SourceIP" element
     */
    void setSourceIP(java.lang.String sourceIP);
    
    /**
     * Sets (as xml) the "SourceIP" element
     */
    void xsetSourceIP(com.idanalytics.products.idscore.request.SourceIPDocument.SourceIP sourceIP);
    
    /**
     * Unsets the "SourceIP" element
     */
    void unsetSourceIP();
    
    /**
     * Gets the "DeviceID" element
     */
    java.lang.String getDeviceID();
    
    /**
     * Gets (as xml) the "DeviceID" element
     */
    com.idanalytics.products.idscore.request.DeviceIDDocument.DeviceID xgetDeviceID();
    
    /**
     * True if has "DeviceID" element
     */
    boolean isSetDeviceID();
    
    /**
     * Sets the "DeviceID" element
     */
    void setDeviceID(java.lang.String deviceID);
    
    /**
     * Sets (as xml) the "DeviceID" element
     */
    void xsetDeviceID(com.idanalytics.products.idscore.request.DeviceIDDocument.DeviceID deviceID);
    
    /**
     * Unsets the "DeviceID" element
     */
    void unsetDeviceID();
    
    /**
     * Gets the "EncodedDeviceInfo" element
     */
    java.lang.String getEncodedDeviceInfo();
    
    /**
     * Gets (as xml) the "EncodedDeviceInfo" element
     */
    com.idanalytics.products.idscore.request.EncodedDeviceInfoDocument.EncodedDeviceInfo xgetEncodedDeviceInfo();
    
    /**
     * True if has "EncodedDeviceInfo" element
     */
    boolean isSetEncodedDeviceInfo();
    
    /**
     * Sets the "EncodedDeviceInfo" element
     */
    void setEncodedDeviceInfo(java.lang.String encodedDeviceInfo);
    
    /**
     * Sets (as xml) the "EncodedDeviceInfo" element
     */
    void xsetEncodedDeviceInfo(com.idanalytics.products.idscore.request.EncodedDeviceInfoDocument.EncodedDeviceInfo encodedDeviceInfo);
    
    /**
     * Unsets the "EncodedDeviceInfo" element
     */
    void unsetEncodedDeviceInfo();
    
    /**
     * Gets the "DeviceIDSource" element
     */
    java.lang.String getDeviceIDSource();
    
    /**
     * Gets (as xml) the "DeviceIDSource" element
     */
    com.idanalytics.products.idscore.request.DeviceIDSourceDocument.DeviceIDSource xgetDeviceIDSource();
    
    /**
     * True if has "DeviceIDSource" element
     */
    boolean isSetDeviceIDSource();
    
    /**
     * Sets the "DeviceIDSource" element
     */
    void setDeviceIDSource(java.lang.String deviceIDSource);
    
    /**
     * Sets (as xml) the "DeviceIDSource" element
     */
    void xsetDeviceIDSource(com.idanalytics.products.idscore.request.DeviceIDSourceDocument.DeviceIDSource deviceIDSource);
    
    /**
     * Unsets the "DeviceIDSource" element
     */
    void unsetDeviceIDSource();
    
    /**
     * Gets the "PrimaryDecisionCode" element
     */
    java.lang.String getPrimaryDecisionCode();
    
    /**
     * Gets (as xml) the "PrimaryDecisionCode" element
     */
    com.idanalytics.products.idscore.request.PrimaryDecisionCodeDocument.PrimaryDecisionCode xgetPrimaryDecisionCode();
    
    /**
     * True if has "PrimaryDecisionCode" element
     */
    boolean isSetPrimaryDecisionCode();
    
    /**
     * Sets the "PrimaryDecisionCode" element
     */
    void setPrimaryDecisionCode(java.lang.String primaryDecisionCode);
    
    /**
     * Sets (as xml) the "PrimaryDecisionCode" element
     */
    void xsetPrimaryDecisionCode(com.idanalytics.products.idscore.request.PrimaryDecisionCodeDocument.PrimaryDecisionCode primaryDecisionCode);
    
    /**
     * Unsets the "PrimaryDecisionCode" element
     */
    void unsetPrimaryDecisionCode();
    
    /**
     * Gets the "SecondaryDecisionCode" element
     */
    java.lang.String getSecondaryDecisionCode();
    
    /**
     * Gets (as xml) the "SecondaryDecisionCode" element
     */
    com.idanalytics.products.idscore.request.SecondaryDecisionCodeDocument.SecondaryDecisionCode xgetSecondaryDecisionCode();
    
    /**
     * True if has "SecondaryDecisionCode" element
     */
    boolean isSetSecondaryDecisionCode();
    
    /**
     * Sets the "SecondaryDecisionCode" element
     */
    void setSecondaryDecisionCode(java.lang.String secondaryDecisionCode);
    
    /**
     * Sets (as xml) the "SecondaryDecisionCode" element
     */
    void xsetSecondaryDecisionCode(com.idanalytics.products.idscore.request.SecondaryDecisionCodeDocument.SecondaryDecisionCode secondaryDecisionCode);
    
    /**
     * Unsets the "SecondaryDecisionCode" element
     */
    void unsetSecondaryDecisionCode();
    
    /**
     * Gets the "PrimaryPortfolio" element
     */
    java.lang.String getPrimaryPortfolio();
    
    /**
     * Gets (as xml) the "PrimaryPortfolio" element
     */
    com.idanalytics.products.idscore.request.PrimaryPortfolioDocument.PrimaryPortfolio xgetPrimaryPortfolio();
    
    /**
     * True if has "PrimaryPortfolio" element
     */
    boolean isSetPrimaryPortfolio();
    
    /**
     * Sets the "PrimaryPortfolio" element
     */
    void setPrimaryPortfolio(java.lang.String primaryPortfolio);
    
    /**
     * Sets (as xml) the "PrimaryPortfolio" element
     */
    void xsetPrimaryPortfolio(com.idanalytics.products.idscore.request.PrimaryPortfolioDocument.PrimaryPortfolio primaryPortfolio);
    
    /**
     * Unsets the "PrimaryPortfolio" element
     */
    void unsetPrimaryPortfolio();
    
    /**
     * Gets the "SecondaryPortfolio" element
     */
    java.lang.String getSecondaryPortfolio();
    
    /**
     * Gets (as xml) the "SecondaryPortfolio" element
     */
    com.idanalytics.products.idscore.request.SecondaryPortfolioDocument.SecondaryPortfolio xgetSecondaryPortfolio();
    
    /**
     * True if has "SecondaryPortfolio" element
     */
    boolean isSetSecondaryPortfolio();
    
    /**
     * Sets the "SecondaryPortfolio" element
     */
    void setSecondaryPortfolio(java.lang.String secondaryPortfolio);
    
    /**
     * Sets (as xml) the "SecondaryPortfolio" element
     */
    void xsetSecondaryPortfolio(com.idanalytics.products.idscore.request.SecondaryPortfolioDocument.SecondaryPortfolio secondaryPortfolio);
    
    /**
     * Unsets the "SecondaryPortfolio" element
     */
    void unsetSecondaryPortfolio();
    
    /**
     * Gets the "ConfirmedFraud" element
     */
    com.idanalytics.products.idscore.request.ConfirmedFraudDocument.ConfirmedFraud.Enum getConfirmedFraud();
    
    /**
     * Gets (as xml) the "ConfirmedFraud" element
     */
    com.idanalytics.products.idscore.request.ConfirmedFraudDocument.ConfirmedFraud xgetConfirmedFraud();
    
    /**
     * True if has "ConfirmedFraud" element
     */
    boolean isSetConfirmedFraud();
    
    /**
     * Sets the "ConfirmedFraud" element
     */
    void setConfirmedFraud(com.idanalytics.products.idscore.request.ConfirmedFraudDocument.ConfirmedFraud.Enum confirmedFraud);
    
    /**
     * Sets (as xml) the "ConfirmedFraud" element
     */
    void xsetConfirmedFraud(com.idanalytics.products.idscore.request.ConfirmedFraudDocument.ConfirmedFraud confirmedFraud);
    
    /**
     * Unsets the "ConfirmedFraud" element
     */
    void unsetConfirmedFraud();
    
    /**
     * Gets the "PrimaryFraudCode" element
     */
    java.lang.String getPrimaryFraudCode();
    
    /**
     * Gets (as xml) the "PrimaryFraudCode" element
     */
    com.idanalytics.products.idscore.request.PrimaryFraudCodeDocument.PrimaryFraudCode xgetPrimaryFraudCode();
    
    /**
     * True if has "PrimaryFraudCode" element
     */
    boolean isSetPrimaryFraudCode();
    
    /**
     * Sets the "PrimaryFraudCode" element
     */
    void setPrimaryFraudCode(java.lang.String primaryFraudCode);
    
    /**
     * Sets (as xml) the "PrimaryFraudCode" element
     */
    void xsetPrimaryFraudCode(com.idanalytics.products.idscore.request.PrimaryFraudCodeDocument.PrimaryFraudCode primaryFraudCode);
    
    /**
     * Unsets the "PrimaryFraudCode" element
     */
    void unsetPrimaryFraudCode();
    
    /**
     * Gets the "SecondaryFraudCode" element
     */
    java.lang.String getSecondaryFraudCode();
    
    /**
     * Gets (as xml) the "SecondaryFraudCode" element
     */
    com.idanalytics.products.idscore.request.SecondaryFraudCodeDocument.SecondaryFraudCode xgetSecondaryFraudCode();
    
    /**
     * True if has "SecondaryFraudCode" element
     */
    boolean isSetSecondaryFraudCode();
    
    /**
     * Sets the "SecondaryFraudCode" element
     */
    void setSecondaryFraudCode(java.lang.String secondaryFraudCode);
    
    /**
     * Sets (as xml) the "SecondaryFraudCode" element
     */
    void xsetSecondaryFraudCode(com.idanalytics.products.idscore.request.SecondaryFraudCodeDocument.SecondaryFraudCode secondaryFraudCode);
    
    /**
     * Unsets the "SecondaryFraudCode" element
     */
    void unsetSecondaryFraudCode();
    
    /**
     * Gets the "CARetailFlag" element
     */
    com.idanalytics.products.idscore.request.CARetailFlagDocument.CARetailFlag.Enum getCARetailFlag();
    
    /**
     * Gets (as xml) the "CARetailFlag" element
     */
    com.idanalytics.products.idscore.request.CARetailFlagDocument.CARetailFlag xgetCARetailFlag();
    
    /**
     * True if has "CARetailFlag" element
     */
    boolean isSetCARetailFlag();
    
    /**
     * Sets the "CARetailFlag" element
     */
    void setCARetailFlag(com.idanalytics.products.idscore.request.CARetailFlagDocument.CARetailFlag.Enum caRetailFlag);
    
    /**
     * Sets (as xml) the "CARetailFlag" element
     */
    void xsetCARetailFlag(com.idanalytics.products.idscore.request.CARetailFlagDocument.CARetailFlag caRetailFlag);
    
    /**
     * Unsets the "CARetailFlag" element
     */
    void unsetCARetailFlag();
    
    /**
     * Gets the "PrimaryIncome" element
     */
    java.lang.Object getPrimaryIncome();
    
    /**
     * Gets (as xml) the "PrimaryIncome" element
     */
    com.idanalytics.products.idscore.request.Income xgetPrimaryIncome();
    
    /**
     * True if has "PrimaryIncome" element
     */
    boolean isSetPrimaryIncome();
    
    /**
     * Sets the "PrimaryIncome" element
     */
    void setPrimaryIncome(java.lang.Object primaryIncome);
    
    /**
     * Sets (as xml) the "PrimaryIncome" element
     */
    void xsetPrimaryIncome(com.idanalytics.products.idscore.request.Income primaryIncome);
    
    /**
     * Unsets the "PrimaryIncome" element
     */
    void unsetPrimaryIncome();
    
    /**
     * Gets the "OtherIncome" element
     */
    java.lang.Object getOtherIncome();
    
    /**
     * Gets (as xml) the "OtherIncome" element
     */
    com.idanalytics.products.idscore.request.Income xgetOtherIncome();
    
    /**
     * True if has "OtherIncome" element
     */
    boolean isSetOtherIncome();
    
    /**
     * Sets the "OtherIncome" element
     */
    void setOtherIncome(java.lang.Object otherIncome);
    
    /**
     * Sets (as xml) the "OtherIncome" element
     */
    void xsetOtherIncome(com.idanalytics.products.idscore.request.Income otherIncome);
    
    /**
     * Unsets the "OtherIncome" element
     */
    void unsetOtherIncome();
    
    /**
     * Gets the "InferredIncome" element
     */
    java.lang.Object getInferredIncome();
    
    /**
     * Gets (as xml) the "InferredIncome" element
     */
    com.idanalytics.products.idscore.request.Income xgetInferredIncome();
    
    /**
     * True if has "InferredIncome" element
     */
    boolean isSetInferredIncome();
    
    /**
     * Sets the "InferredIncome" element
     */
    void setInferredIncome(java.lang.Object inferredIncome);
    
    /**
     * Sets (as xml) the "InferredIncome" element
     */
    void xsetInferredIncome(com.idanalytics.products.idscore.request.Income inferredIncome);
    
    /**
     * Unsets the "InferredIncome" element
     */
    void unsetInferredIncome();
    
    /**
     * Gets the "InferredIncomeInd" element
     */
    com.idanalytics.products.idscore.request.Bool.Enum getInferredIncomeInd();
    
    /**
     * Gets (as xml) the "InferredIncomeInd" element
     */
    com.idanalytics.products.idscore.request.Bool xgetInferredIncomeInd();
    
    /**
     * True if has "InferredIncomeInd" element
     */
    boolean isSetInferredIncomeInd();
    
    /**
     * Sets the "InferredIncomeInd" element
     */
    void setInferredIncomeInd(com.idanalytics.products.idscore.request.Bool.Enum inferredIncomeInd);
    
    /**
     * Sets (as xml) the "InferredIncomeInd" element
     */
    void xsetInferredIncomeInd(com.idanalytics.products.idscore.request.Bool inferredIncomeInd);
    
    /**
     * Unsets the "InferredIncomeInd" element
     */
    void unsetInferredIncomeInd();
    
    /**
     * Gets the "RecommendedCreditLine" element
     */
    java.lang.Object getRecommendedCreditLine();
    
    /**
     * Gets (as xml) the "RecommendedCreditLine" element
     */
    com.idanalytics.products.idscore.request.Income xgetRecommendedCreditLine();
    
    /**
     * True if has "RecommendedCreditLine" element
     */
    boolean isSetRecommendedCreditLine();
    
    /**
     * Sets the "RecommendedCreditLine" element
     */
    void setRecommendedCreditLine(java.lang.Object recommendedCreditLine);
    
    /**
     * Sets (as xml) the "RecommendedCreditLine" element
     */
    void xsetRecommendedCreditLine(com.idanalytics.products.idscore.request.Income recommendedCreditLine);
    
    /**
     * Unsets the "RecommendedCreditLine" element
     */
    void unsetRecommendedCreditLine();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.idscore.request.Application newInstance() {
          return (com.idanalytics.products.idscore.request.Application) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.idscore.request.Application newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.idscore.request.Application) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.idscore.request.Application parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.Application) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.idscore.request.Application parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.Application) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.idscore.request.Application parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.Application) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.idscore.request.Application parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.Application) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.idscore.request.Application parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.Application) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.idscore.request.Application parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.Application) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.idscore.request.Application parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.Application) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.idscore.request.Application parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.Application) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.idscore.request.Application parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.Application) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.idscore.request.Application parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.Application) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.idscore.request.Application parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.Application) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.idscore.request.Application parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.Application) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.idscore.request.Application parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.Application) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.idscore.request.Application parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.Application) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.request.Application parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.request.Application) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.request.Application parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.request.Application) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
