/*
 * An XML document type.
 * Localname: ConsumerScoreRequest
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * A document containing one ConsumerScoreRequest(@http://idanalytics.com/products/idscore/request) element.
 *
 * This is a complex type.
 */
public class ConsumerScoreRequestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument
{
    private static final long serialVersionUID = 1L;
    
    public ConsumerScoreRequestDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CONSUMERSCOREREQUEST$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ConsumerScoreRequest");
    
    
    /**
     * Gets the "ConsumerScoreRequest" element
     */
    public com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument.ConsumerScoreRequest getConsumerScoreRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument.ConsumerScoreRequest target = null;
            target = (com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument.ConsumerScoreRequest)get_store().find_element_user(CONSUMERSCOREREQUEST$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "ConsumerScoreRequest" element
     */
    public void setConsumerScoreRequest(com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument.ConsumerScoreRequest consumerScoreRequest)
    {
        generatedSetterHelperImpl(consumerScoreRequest, CONSUMERSCOREREQUEST$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "ConsumerScoreRequest" element
     */
    public com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument.ConsumerScoreRequest addNewConsumerScoreRequest()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument.ConsumerScoreRequest target = null;
            target = (com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument.ConsumerScoreRequest)get_store().add_element_user(CONSUMERSCOREREQUEST$0);
            return target;
        }
    }
    /**
     * An XML ConsumerScoreRequest(@http://idanalytics.com/products/idscore/request).
     *
     * This is a complex type.
     */
    public static class ConsumerScoreRequestImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.ConsumerScoreRequestDocument.ConsumerScoreRequest
    {
        private static final long serialVersionUID = 1L;
        
        public ConsumerScoreRequestImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName CONSUMERHEADER$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ConsumerHeader");
        private static final javax.xml.namespace.QName IDENTITY$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Identity");
        private static final javax.xml.namespace.QName SURVEYRESULTS$4 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "SurveyResults");
        private static final javax.xml.namespace.QName IDAINTERNAL$6 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "IDAInternal");
        private static final javax.xml.namespace.QName EXTERNALDATA$8 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "ExternalData");
        private static final javax.xml.namespace.QName SCHEMAVERSION$10 = 
            new javax.xml.namespace.QName("", "schemaVersion");
        
        
        /**
         * Gets the "ConsumerHeader" element
         */
        public com.idanalytics.products.idscore.request.ConsumerHeaderDocument.ConsumerHeader getConsumerHeader()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.ConsumerHeaderDocument.ConsumerHeader target = null;
                target = (com.idanalytics.products.idscore.request.ConsumerHeaderDocument.ConsumerHeader)get_store().find_element_user(CONSUMERHEADER$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * Sets the "ConsumerHeader" element
         */
        public void setConsumerHeader(com.idanalytics.products.idscore.request.ConsumerHeaderDocument.ConsumerHeader consumerHeader)
        {
            generatedSetterHelperImpl(consumerHeader, CONSUMERHEADER$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "ConsumerHeader" element
         */
        public com.idanalytics.products.idscore.request.ConsumerHeaderDocument.ConsumerHeader addNewConsumerHeader()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.ConsumerHeaderDocument.ConsumerHeader target = null;
                target = (com.idanalytics.products.idscore.request.ConsumerHeaderDocument.ConsumerHeader)get_store().add_element_user(CONSUMERHEADER$0);
                return target;
            }
        }
        
        /**
         * Gets the "Identity" element
         */
        public com.idanalytics.products.idscore.request.Identity getIdentity()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.Identity target = null;
                target = (com.idanalytics.products.idscore.request.Identity)get_store().find_element_user(IDENTITY$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * Sets the "Identity" element
         */
        public void setIdentity(com.idanalytics.products.idscore.request.Identity identity)
        {
            generatedSetterHelperImpl(identity, IDENTITY$2, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "Identity" element
         */
        public com.idanalytics.products.idscore.request.Identity addNewIdentity()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.Identity target = null;
                target = (com.idanalytics.products.idscore.request.Identity)get_store().add_element_user(IDENTITY$2);
                return target;
            }
        }
        
        /**
         * Gets the "SurveyResults" element
         */
        public com.idanalytics.products.idscore.request.SurveyResults getSurveyResults()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.SurveyResults target = null;
                target = (com.idanalytics.products.idscore.request.SurveyResults)get_store().find_element_user(SURVEYRESULTS$4, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "SurveyResults" element
         */
        public boolean isSetSurveyResults()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(SURVEYRESULTS$4) != 0;
            }
        }
        
        /**
         * Sets the "SurveyResults" element
         */
        public void setSurveyResults(com.idanalytics.products.idscore.request.SurveyResults surveyResults)
        {
            generatedSetterHelperImpl(surveyResults, SURVEYRESULTS$4, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "SurveyResults" element
         */
        public com.idanalytics.products.idscore.request.SurveyResults addNewSurveyResults()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.SurveyResults target = null;
                target = (com.idanalytics.products.idscore.request.SurveyResults)get_store().add_element_user(SURVEYRESULTS$4);
                return target;
            }
        }
        
        /**
         * Unsets the "SurveyResults" element
         */
        public void unsetSurveyResults()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(SURVEYRESULTS$4, 0);
            }
        }
        
        /**
         * Gets the "IDAInternal" element
         */
        public com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal getIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal target = null;
                target = (com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal)get_store().find_element_user(IDAINTERNAL$6, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "IDAInternal" element
         */
        public boolean isSetIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(IDAINTERNAL$6) != 0;
            }
        }
        
        /**
         * Sets the "IDAInternal" element
         */
        public void setIDAInternal(com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal idaInternal)
        {
            generatedSetterHelperImpl(idaInternal, IDAINTERNAL$6, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "IDAInternal" element
         */
        public com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal addNewIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal target = null;
                target = (com.idanalytics.products.idscore.request.IDAInternalDocument.IDAInternal)get_store().add_element_user(IDAINTERNAL$6);
                return target;
            }
        }
        
        /**
         * Unsets the "IDAInternal" element
         */
        public void unsetIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(IDAINTERNAL$6, 0);
            }
        }
        
        /**
         * Gets the "ExternalData" element
         */
        public com.idanalytics.products.idscore.request.ExternalData getExternalData()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.ExternalData target = null;
                target = (com.idanalytics.products.idscore.request.ExternalData)get_store().find_element_user(EXTERNALDATA$8, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "ExternalData" element
         */
        public boolean isSetExternalData()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(EXTERNALDATA$8) != 0;
            }
        }
        
        /**
         * Sets the "ExternalData" element
         */
        public void setExternalData(com.idanalytics.products.idscore.request.ExternalData externalData)
        {
            generatedSetterHelperImpl(externalData, EXTERNALDATA$8, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "ExternalData" element
         */
        public com.idanalytics.products.idscore.request.ExternalData addNewExternalData()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.idscore.request.ExternalData target = null;
                target = (com.idanalytics.products.idscore.request.ExternalData)get_store().add_element_user(EXTERNALDATA$8);
                return target;
            }
        }
        
        /**
         * Unsets the "ExternalData" element
         */
        public void unsetExternalData()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(EXTERNALDATA$8, 0);
            }
        }
        
        /**
         * Gets the "schemaVersion" attribute
         */
        public java.math.BigDecimal getSchemaVersion()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(SCHEMAVERSION$10);
                if (target == null)
                {
                    return null;
                }
                return target.getBigDecimalValue();
            }
        }
        
        /**
         * Gets (as xml) the "schemaVersion" attribute
         */
        public org.apache.xmlbeans.XmlDecimal xgetSchemaVersion()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlDecimal target = null;
                target = (org.apache.xmlbeans.XmlDecimal)get_store().find_attribute_user(SCHEMAVERSION$10);
                return target;
            }
        }
        
        /**
         * True if has "schemaVersion" attribute
         */
        public boolean isSetSchemaVersion()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().find_attribute_user(SCHEMAVERSION$10) != null;
            }
        }
        
        /**
         * Sets the "schemaVersion" attribute
         */
        public void setSchemaVersion(java.math.BigDecimal schemaVersion)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(SCHEMAVERSION$10);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(SCHEMAVERSION$10);
                }
                target.setBigDecimalValue(schemaVersion);
            }
        }
        
        /**
         * Sets (as xml) the "schemaVersion" attribute
         */
        public void xsetSchemaVersion(org.apache.xmlbeans.XmlDecimal schemaVersion)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlDecimal target = null;
                target = (org.apache.xmlbeans.XmlDecimal)get_store().find_attribute_user(SCHEMAVERSION$10);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlDecimal)get_store().add_attribute_user(SCHEMAVERSION$10);
                }
                target.set(schemaVersion);
            }
        }
        
        /**
         * Unsets the "schemaVersion" attribute
         */
        public void unsetSchemaVersion()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_attribute(SCHEMAVERSION$10);
            }
        }
    }
}
