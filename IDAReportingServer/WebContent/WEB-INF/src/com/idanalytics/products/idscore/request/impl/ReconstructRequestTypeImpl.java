/*
 * XML Type:  ReconstructRequest_type
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.ReconstructRequestType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request.impl;
/**
 * An XML ReconstructRequest_type(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public class ReconstructRequestTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.request.ReconstructRequestType
{
    private static final long serialVersionUID = 1L;
    
    public ReconstructRequestTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IDANUMBER$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "IDANumber");
    private static final javax.xml.namespace.QName SSN5$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "SSN5");
    private static final javax.xml.namespace.QName SSN9$4 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "SSN9");
    private static final javax.xml.namespace.QName ADDRESS$6 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Address");
    private static final javax.xml.namespace.QName DOB$8 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "DOB");
    private static final javax.xml.namespace.QName EMAIL$10 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Email");
    private static final javax.xml.namespace.QName PHONE$12 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/request", "Phone");
    
    
    /**
     * Gets the "IDANumber" element
     */
    public boolean getIDANumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDANUMBER$0, 0);
            if (target == null)
            {
                return false;
            }
            return target.getBooleanValue();
        }
    }
    
    /**
     * Gets (as xml) the "IDANumber" element
     */
    public org.apache.xmlbeans.XmlBoolean xgetIDANumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(IDANUMBER$0, 0);
            return target;
        }
    }
    
    /**
     * True if has "IDANumber" element
     */
    public boolean isSetIDANumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(IDANUMBER$0) != 0;
        }
    }
    
    /**
     * Sets the "IDANumber" element
     */
    public void setIDANumber(boolean idaNumber)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDANUMBER$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDANUMBER$0);
            }
            target.setBooleanValue(idaNumber);
        }
    }
    
    /**
     * Sets (as xml) the "IDANumber" element
     */
    public void xsetIDANumber(org.apache.xmlbeans.XmlBoolean idaNumber)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(IDANUMBER$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlBoolean)get_store().add_element_user(IDANUMBER$0);
            }
            target.set(idaNumber);
        }
    }
    
    /**
     * Unsets the "IDANumber" element
     */
    public void unsetIDANumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(IDANUMBER$0, 0);
        }
    }
    
    /**
     * Gets the "SSN5" element
     */
    public boolean getSSN5()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SSN5$2, 0);
            if (target == null)
            {
                return false;
            }
            return target.getBooleanValue();
        }
    }
    
    /**
     * Gets (as xml) the "SSN5" element
     */
    public org.apache.xmlbeans.XmlBoolean xgetSSN5()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(SSN5$2, 0);
            return target;
        }
    }
    
    /**
     * True if has "SSN5" element
     */
    public boolean isSetSSN5()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SSN5$2) != 0;
        }
    }
    
    /**
     * Sets the "SSN5" element
     */
    public void setSSN5(boolean ssn5)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SSN5$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SSN5$2);
            }
            target.setBooleanValue(ssn5);
        }
    }
    
    /**
     * Sets (as xml) the "SSN5" element
     */
    public void xsetSSN5(org.apache.xmlbeans.XmlBoolean ssn5)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(SSN5$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlBoolean)get_store().add_element_user(SSN5$2);
            }
            target.set(ssn5);
        }
    }
    
    /**
     * Unsets the "SSN5" element
     */
    public void unsetSSN5()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SSN5$2, 0);
        }
    }
    
    /**
     * Gets the "SSN9" element
     */
    public boolean getSSN9()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SSN9$4, 0);
            if (target == null)
            {
                return false;
            }
            return target.getBooleanValue();
        }
    }
    
    /**
     * Gets (as xml) the "SSN9" element
     */
    public org.apache.xmlbeans.XmlBoolean xgetSSN9()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(SSN9$4, 0);
            return target;
        }
    }
    
    /**
     * True if has "SSN9" element
     */
    public boolean isSetSSN9()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SSN9$4) != 0;
        }
    }
    
    /**
     * Sets the "SSN9" element
     */
    public void setSSN9(boolean ssn9)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SSN9$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SSN9$4);
            }
            target.setBooleanValue(ssn9);
        }
    }
    
    /**
     * Sets (as xml) the "SSN9" element
     */
    public void xsetSSN9(org.apache.xmlbeans.XmlBoolean ssn9)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(SSN9$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlBoolean)get_store().add_element_user(SSN9$4);
            }
            target.set(ssn9);
        }
    }
    
    /**
     * Unsets the "SSN9" element
     */
    public void unsetSSN9()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SSN9$4, 0);
        }
    }
    
    /**
     * Gets the "Address" element
     */
    public boolean getAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ADDRESS$6, 0);
            if (target == null)
            {
                return false;
            }
            return target.getBooleanValue();
        }
    }
    
    /**
     * Gets (as xml) the "Address" element
     */
    public org.apache.xmlbeans.XmlBoolean xgetAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(ADDRESS$6, 0);
            return target;
        }
    }
    
    /**
     * True if has "Address" element
     */
    public boolean isSetAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ADDRESS$6) != 0;
        }
    }
    
    /**
     * Sets the "Address" element
     */
    public void setAddress(boolean address)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ADDRESS$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ADDRESS$6);
            }
            target.setBooleanValue(address);
        }
    }
    
    /**
     * Sets (as xml) the "Address" element
     */
    public void xsetAddress(org.apache.xmlbeans.XmlBoolean address)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(ADDRESS$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlBoolean)get_store().add_element_user(ADDRESS$6);
            }
            target.set(address);
        }
    }
    
    /**
     * Unsets the "Address" element
     */
    public void unsetAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ADDRESS$6, 0);
        }
    }
    
    /**
     * Gets the "DOB" element
     */
    public boolean getDOB()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DOB$8, 0);
            if (target == null)
            {
                return false;
            }
            return target.getBooleanValue();
        }
    }
    
    /**
     * Gets (as xml) the "DOB" element
     */
    public org.apache.xmlbeans.XmlBoolean xgetDOB()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(DOB$8, 0);
            return target;
        }
    }
    
    /**
     * True if has "DOB" element
     */
    public boolean isSetDOB()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(DOB$8) != 0;
        }
    }
    
    /**
     * Sets the "DOB" element
     */
    public void setDOB(boolean dob)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DOB$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DOB$8);
            }
            target.setBooleanValue(dob);
        }
    }
    
    /**
     * Sets (as xml) the "DOB" element
     */
    public void xsetDOB(org.apache.xmlbeans.XmlBoolean dob)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(DOB$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlBoolean)get_store().add_element_user(DOB$8);
            }
            target.set(dob);
        }
    }
    
    /**
     * Unsets the "DOB" element
     */
    public void unsetDOB()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(DOB$8, 0);
        }
    }
    
    /**
     * Gets the "Email" element
     */
    public boolean getEmail()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EMAIL$10, 0);
            if (target == null)
            {
                return false;
            }
            return target.getBooleanValue();
        }
    }
    
    /**
     * Gets (as xml) the "Email" element
     */
    public org.apache.xmlbeans.XmlBoolean xgetEmail()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(EMAIL$10, 0);
            return target;
        }
    }
    
    /**
     * True if has "Email" element
     */
    public boolean isSetEmail()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(EMAIL$10) != 0;
        }
    }
    
    /**
     * Sets the "Email" element
     */
    public void setEmail(boolean email)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EMAIL$10, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(EMAIL$10);
            }
            target.setBooleanValue(email);
        }
    }
    
    /**
     * Sets (as xml) the "Email" element
     */
    public void xsetEmail(org.apache.xmlbeans.XmlBoolean email)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(EMAIL$10, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlBoolean)get_store().add_element_user(EMAIL$10);
            }
            target.set(email);
        }
    }
    
    /**
     * Unsets the "Email" element
     */
    public void unsetEmail()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(EMAIL$10, 0);
        }
    }
    
    /**
     * Gets the "Phone" element
     */
    public boolean getPhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PHONE$12, 0);
            if (target == null)
            {
                return false;
            }
            return target.getBooleanValue();
        }
    }
    
    /**
     * Gets (as xml) the "Phone" element
     */
    public org.apache.xmlbeans.XmlBoolean xgetPhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(PHONE$12, 0);
            return target;
        }
    }
    
    /**
     * True if has "Phone" element
     */
    public boolean isSetPhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PHONE$12) != 0;
        }
    }
    
    /**
     * Sets the "Phone" element
     */
    public void setPhone(boolean phone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PHONE$12, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PHONE$12);
            }
            target.setBooleanValue(phone);
        }
    }
    
    /**
     * Sets (as xml) the "Phone" element
     */
    public void xsetPhone(org.apache.xmlbeans.XmlBoolean phone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(PHONE$12, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlBoolean)get_store().add_element_user(PHONE$12);
            }
            target.set(phone);
        }
    }
    
    /**
     * Unsets the "Phone" element
     */
    public void unsetPhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PHONE$12, 0);
        }
    }
}
