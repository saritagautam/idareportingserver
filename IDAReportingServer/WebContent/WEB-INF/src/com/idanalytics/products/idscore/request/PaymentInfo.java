/*
 * XML Type:  PaymentInfo
 * Namespace: http://idanalytics.com/products/idscore/request
 * Java type: com.idanalytics.products.idscore.request.PaymentInfo
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.request;


/**
 * An XML PaymentInfo(@http://idanalytics.com/products/idscore/request).
 *
 * This is a complex type.
 */
public interface PaymentInfo extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(PaymentInfo.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("paymentinfo56batype");
    
    /**
     * Gets the "PaymentType" element
     */
    java.lang.String getPaymentType();
    
    /**
     * Gets (as xml) the "PaymentType" element
     */
    com.idanalytics.products.common_v1.NormalizedString xgetPaymentType();
    
    /**
     * Sets the "PaymentType" element
     */
    void setPaymentType(java.lang.String paymentType);
    
    /**
     * Sets (as xml) the "PaymentType" element
     */
    void xsetPaymentType(com.idanalytics.products.common_v1.NormalizedString paymentType);
    
    /**
     * Gets the "PaymentAmount" element
     */
    java.lang.Object getPaymentAmount();
    
    /**
     * Gets (as xml) the "PaymentAmount" element
     */
    com.idanalytics.products.common_v1.OptionalDecimal xgetPaymentAmount();
    
    /**
     * Sets the "PaymentAmount" element
     */
    void setPaymentAmount(java.lang.Object paymentAmount);
    
    /**
     * Sets (as xml) the "PaymentAmount" element
     */
    void xsetPaymentAmount(com.idanalytics.products.common_v1.OptionalDecimal paymentAmount);
    
    /**
     * Gets the "PaymentCardInfo" element
     */
    com.idanalytics.products.idscore.request.CardInfo getPaymentCardInfo();
    
    /**
     * True if has "PaymentCardInfo" element
     */
    boolean isSetPaymentCardInfo();
    
    /**
     * Sets the "PaymentCardInfo" element
     */
    void setPaymentCardInfo(com.idanalytics.products.idscore.request.CardInfo paymentCardInfo);
    
    /**
     * Appends and returns a new empty "PaymentCardInfo" element
     */
    com.idanalytics.products.idscore.request.CardInfo addNewPaymentCardInfo();
    
    /**
     * Unsets the "PaymentCardInfo" element
     */
    void unsetPaymentCardInfo();
    
    /**
     * Gets the "PaymentGeneralInfo" element
     */
    com.idanalytics.products.idscore.request.GenericPayment getPaymentGeneralInfo();
    
    /**
     * True if has "PaymentGeneralInfo" element
     */
    boolean isSetPaymentGeneralInfo();
    
    /**
     * Sets the "PaymentGeneralInfo" element
     */
    void setPaymentGeneralInfo(com.idanalytics.products.idscore.request.GenericPayment paymentGeneralInfo);
    
    /**
     * Appends and returns a new empty "PaymentGeneralInfo" element
     */
    com.idanalytics.products.idscore.request.GenericPayment addNewPaymentGeneralInfo();
    
    /**
     * Unsets the "PaymentGeneralInfo" element
     */
    void unsetPaymentGeneralInfo();
    
    /**
     * Gets the "PaymentDate" element
     */
    java.lang.Object getPaymentDate();
    
    /**
     * Gets (as xml) the "PaymentDate" element
     */
    com.idanalytics.products.common_v1.OptionalDate xgetPaymentDate();
    
    /**
     * Sets the "PaymentDate" element
     */
    void setPaymentDate(java.lang.Object paymentDate);
    
    /**
     * Sets (as xml) the "PaymentDate" element
     */
    void xsetPaymentDate(com.idanalytics.products.common_v1.OptionalDate paymentDate);
    
    /**
     * Gets the "AuthorizationDateTime" element
     */
    java.lang.Object getAuthorizationDateTime();
    
    /**
     * Gets (as xml) the "AuthorizationDateTime" element
     */
    com.idanalytics.products.common_v1.OptionalDateTime xgetAuthorizationDateTime();
    
    /**
     * True if has "AuthorizationDateTime" element
     */
    boolean isSetAuthorizationDateTime();
    
    /**
     * Sets the "AuthorizationDateTime" element
     */
    void setAuthorizationDateTime(java.lang.Object authorizationDateTime);
    
    /**
     * Sets (as xml) the "AuthorizationDateTime" element
     */
    void xsetAuthorizationDateTime(com.idanalytics.products.common_v1.OptionalDateTime authorizationDateTime);
    
    /**
     * Unsets the "AuthorizationDateTime" element
     */
    void unsetAuthorizationDateTime();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.idscore.request.PaymentInfo newInstance() {
          return (com.idanalytics.products.idscore.request.PaymentInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.idscore.request.PaymentInfo newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.idscore.request.PaymentInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.idscore.request.PaymentInfo parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.PaymentInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.idscore.request.PaymentInfo parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.PaymentInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.idscore.request.PaymentInfo parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.PaymentInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.idscore.request.PaymentInfo parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.PaymentInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.idscore.request.PaymentInfo parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.PaymentInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.idscore.request.PaymentInfo parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.PaymentInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.idscore.request.PaymentInfo parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.PaymentInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.idscore.request.PaymentInfo parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.PaymentInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.idscore.request.PaymentInfo parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.PaymentInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.idscore.request.PaymentInfo parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.idscore.request.PaymentInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.idscore.request.PaymentInfo parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.PaymentInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.idscore.request.PaymentInfo parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.PaymentInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.idscore.request.PaymentInfo parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.PaymentInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.idscore.request.PaymentInfo parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.idscore.request.PaymentInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.request.PaymentInfo parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.request.PaymentInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.idscore.request.PaymentInfo parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.idscore.request.PaymentInfo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
