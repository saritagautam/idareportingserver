/*
 * XML Type:  ConsumerStatementType
 * Namespace: http://idanalytics.com/products/idscore/result.v31
 * Java type: com.idanalytics.products.idscore.result_v31.ConsumerStatementType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.idscore.result_v31.impl;
/**
 * An XML ConsumerStatementType(@http://idanalytics.com/products/idscore/result.v31).
 *
 * This is a complex type.
 */
public class ConsumerStatementTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.idscore.result_v31.ConsumerStatementType
{
    private static final long serialVersionUID = 1L;
    
    public ConsumerStatementTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName STATEMENTDATE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result.v31", "StatementDate");
    private static final javax.xml.namespace.QName STATEMENTTYPE$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result.v31", "StatementType");
    private static final javax.xml.namespace.QName STATEMENTSUBTYPECODE$4 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result.v31", "StatementSubtypeCode");
    private static final javax.xml.namespace.QName STATEMENT$6 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/idscore/result.v31", "Statement");
    
    
    /**
     * Gets the "StatementDate" element
     */
    public java.lang.String getStatementDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STATEMENTDATE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "StatementDate" element
     */
    public com.idanalytics.products.idscore.result_v31.ConsumerStatementType.StatementDate xgetStatementDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result_v31.ConsumerStatementType.StatementDate target = null;
            target = (com.idanalytics.products.idscore.result_v31.ConsumerStatementType.StatementDate)get_store().find_element_user(STATEMENTDATE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "StatementDate" element
     */
    public void setStatementDate(java.lang.String statementDate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STATEMENTDATE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(STATEMENTDATE$0);
            }
            target.setStringValue(statementDate);
        }
    }
    
    /**
     * Sets (as xml) the "StatementDate" element
     */
    public void xsetStatementDate(com.idanalytics.products.idscore.result_v31.ConsumerStatementType.StatementDate statementDate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result_v31.ConsumerStatementType.StatementDate target = null;
            target = (com.idanalytics.products.idscore.result_v31.ConsumerStatementType.StatementDate)get_store().find_element_user(STATEMENTDATE$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.result_v31.ConsumerStatementType.StatementDate)get_store().add_element_user(STATEMENTDATE$0);
            }
            target.set(statementDate);
        }
    }
    
    /**
     * Gets the "StatementType" element
     */
    public java.lang.String getStatementType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STATEMENTTYPE$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "StatementType" element
     */
    public com.idanalytics.products.idscore.result_v31.ConsumerStatementType.StatementType xgetStatementType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result_v31.ConsumerStatementType.StatementType target = null;
            target = (com.idanalytics.products.idscore.result_v31.ConsumerStatementType.StatementType)get_store().find_element_user(STATEMENTTYPE$2, 0);
            return target;
        }
    }
    
    /**
     * Sets the "StatementType" element
     */
    public void setStatementType(java.lang.String statementType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STATEMENTTYPE$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(STATEMENTTYPE$2);
            }
            target.setStringValue(statementType);
        }
    }
    
    /**
     * Sets (as xml) the "StatementType" element
     */
    public void xsetStatementType(com.idanalytics.products.idscore.result_v31.ConsumerStatementType.StatementType statementType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result_v31.ConsumerStatementType.StatementType target = null;
            target = (com.idanalytics.products.idscore.result_v31.ConsumerStatementType.StatementType)get_store().find_element_user(STATEMENTTYPE$2, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.result_v31.ConsumerStatementType.StatementType)get_store().add_element_user(STATEMENTTYPE$2);
            }
            target.set(statementType);
        }
    }
    
    /**
     * Gets the "StatementSubtypeCode" element
     */
    public java.lang.String getStatementSubtypeCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STATEMENTSUBTYPECODE$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "StatementSubtypeCode" element
     */
    public com.idanalytics.products.idscore.result_v31.ConsumerStatementType.StatementSubtypeCode xgetStatementSubtypeCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result_v31.ConsumerStatementType.StatementSubtypeCode target = null;
            target = (com.idanalytics.products.idscore.result_v31.ConsumerStatementType.StatementSubtypeCode)get_store().find_element_user(STATEMENTSUBTYPECODE$4, 0);
            return target;
        }
    }
    
    /**
     * Sets the "StatementSubtypeCode" element
     */
    public void setStatementSubtypeCode(java.lang.String statementSubtypeCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STATEMENTSUBTYPECODE$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(STATEMENTSUBTYPECODE$4);
            }
            target.setStringValue(statementSubtypeCode);
        }
    }
    
    /**
     * Sets (as xml) the "StatementSubtypeCode" element
     */
    public void xsetStatementSubtypeCode(com.idanalytics.products.idscore.result_v31.ConsumerStatementType.StatementSubtypeCode statementSubtypeCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result_v31.ConsumerStatementType.StatementSubtypeCode target = null;
            target = (com.idanalytics.products.idscore.result_v31.ConsumerStatementType.StatementSubtypeCode)get_store().find_element_user(STATEMENTSUBTYPECODE$4, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.result_v31.ConsumerStatementType.StatementSubtypeCode)get_store().add_element_user(STATEMENTSUBTYPECODE$4);
            }
            target.set(statementSubtypeCode);
        }
    }
    
    /**
     * Gets the "Statement" element
     */
    public java.lang.String getStatement()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STATEMENT$6, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Statement" element
     */
    public com.idanalytics.products.idscore.result_v31.ConsumerStatementType.Statement xgetStatement()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result_v31.ConsumerStatementType.Statement target = null;
            target = (com.idanalytics.products.idscore.result_v31.ConsumerStatementType.Statement)get_store().find_element_user(STATEMENT$6, 0);
            return target;
        }
    }
    
    /**
     * Sets the "Statement" element
     */
    public void setStatement(java.lang.String statement)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STATEMENT$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(STATEMENT$6);
            }
            target.setStringValue(statement);
        }
    }
    
    /**
     * Sets (as xml) the "Statement" element
     */
    public void xsetStatement(com.idanalytics.products.idscore.result_v31.ConsumerStatementType.Statement statement)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.idscore.result_v31.ConsumerStatementType.Statement target = null;
            target = (com.idanalytics.products.idscore.result_v31.ConsumerStatementType.Statement)get_store().find_element_user(STATEMENT$6, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.idscore.result_v31.ConsumerStatementType.Statement)get_store().add_element_user(STATEMENT$6);
            }
            target.set(statement);
        }
    }
    /**
     * An XML StatementDate(@http://idanalytics.com/products/idscore/result.v31).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result_v31.ConsumerStatementType$StatementDate.
     */
    public static class StatementDateImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.result_v31.ConsumerStatementType.StatementDate
    {
        private static final long serialVersionUID = 1L;
        
        public StatementDateImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected StatementDateImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
    /**
     * An XML StatementType(@http://idanalytics.com/products/idscore/result.v31).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result_v31.ConsumerStatementType$StatementType.
     */
    public static class StatementTypeImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.result_v31.ConsumerStatementType.StatementType
    {
        private static final long serialVersionUID = 1L;
        
        public StatementTypeImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected StatementTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
    /**
     * An XML StatementSubtypeCode(@http://idanalytics.com/products/idscore/result.v31).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result_v31.ConsumerStatementType$StatementSubtypeCode.
     */
    public static class StatementSubtypeCodeImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.result_v31.ConsumerStatementType.StatementSubtypeCode
    {
        private static final long serialVersionUID = 1L;
        
        public StatementSubtypeCodeImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected StatementSubtypeCodeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
    /**
     * An XML Statement(@http://idanalytics.com/products/idscore/result.v31).
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.idscore.result_v31.ConsumerStatementType$Statement.
     */
    public static class StatementImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.idscore.result_v31.ConsumerStatementType.Statement
    {
        private static final long serialVersionUID = 1L;
        
        public StatementImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected StatementImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
