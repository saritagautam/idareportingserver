/*
 * XML Type:  IDASequence
 * Namespace: http://idanalytics.com/products/common.v1
 * Java type: com.idanalytics.products.common_v1.IDASequence
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.common_v1.impl;
/**
 * An XML IDASequence(@http://idanalytics.com/products/common.v1).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.common_v1.IDASequence.
 */
public class IDASequenceImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.common_v1.IDASequence
{
    private static final long serialVersionUID = 1L;
    
    public IDASequenceImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected IDASequenceImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
