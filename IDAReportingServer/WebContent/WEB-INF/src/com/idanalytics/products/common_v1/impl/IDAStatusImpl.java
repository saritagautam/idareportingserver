/*
 * XML Type:  IDAStatus
 * Namespace: http://idanalytics.com/products/common.v1
 * Java type: com.idanalytics.products.common_v1.IDAStatus
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.common_v1.impl;
/**
 * An XML IDAStatus(@http://idanalytics.com/products/common.v1).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.common_v1.IDAStatus.
 */
public class IDAStatusImpl extends org.apache.xmlbeans.impl.values.JavaIntHolderEx implements com.idanalytics.products.common_v1.IDAStatus
{
    private static final long serialVersionUID = 1L;
    
    public IDAStatusImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected IDAStatusImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
