/*
 * XML Type:  NormalizedString
 * Namespace: http://idanalytics.com/products/common.v1
 * Java type: com.idanalytics.products.common_v1.NormalizedString
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.common_v1.impl;
/**
 * An XML NormalizedString(@http://idanalytics.com/products/common.v1).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.common_v1.NormalizedString.
 */
public class NormalizedStringImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.common_v1.NormalizedString
{
    private static final long serialVersionUID = 1L;
    
    public NormalizedStringImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected NormalizedStringImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
