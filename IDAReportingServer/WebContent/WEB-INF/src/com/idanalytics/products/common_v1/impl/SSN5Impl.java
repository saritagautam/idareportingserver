/*
 * XML Type:  SSN5
 * Namespace: http://idanalytics.com/products/common.v1
 * Java type: com.idanalytics.products.common_v1.SSN5
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.common_v1.impl;
/**
 * An XML SSN5(@http://idanalytics.com/products/common.v1).
 *
 * This is a union type. Instances are of one of the following types:
 *     com.idanalytics.products.common_v1.SSN5$Member
 *     com.idanalytics.products.common_v1.SSN5$Member2
 */
public class SSN5Impl extends org.apache.xmlbeans.impl.values.XmlUnionImpl implements com.idanalytics.products.common_v1.SSN5, com.idanalytics.products.common_v1.SSN5.Member, com.idanalytics.products.common_v1.SSN5.Member2
{
    private static final long serialVersionUID = 1L;
    
    public SSN5Impl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected SSN5Impl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
    /**
     * An anonymous inner XML type.
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.common_v1.SSN5$Member.
     */
    public static class MemberImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.common_v1.SSN5.Member
    {
        private static final long serialVersionUID = 1L;
        
        public MemberImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected MemberImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
    /**
     * An anonymous inner XML type.
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.common_v1.SSN5$Member2.
     */
    public static class MemberImpl2 extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.common_v1.SSN5.Member2
    {
        private static final long serialVersionUID = 1L;
        
        public MemberImpl2(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected MemberImpl2(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
