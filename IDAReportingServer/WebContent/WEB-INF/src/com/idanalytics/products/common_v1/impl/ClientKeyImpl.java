/*
 * XML Type:  ClientKey
 * Namespace: http://idanalytics.com/products/common.v1
 * Java type: com.idanalytics.products.common_v1.ClientKey
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.common_v1.impl;
/**
 * An XML ClientKey(@http://idanalytics.com/products/common.v1).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.common_v1.ClientKey.
 */
public class ClientKeyImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.common_v1.ClientKey
{
    private static final long serialVersionUID = 1L;
    
    public ClientKeyImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected ClientKeyImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
