/*
 * XML Type:  ReasonCode
 * Namespace: http://idanalytics.com/products/common.v1
 * Java type: com.idanalytics.products.common_v1.ReasonCode
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.common_v1.impl;
/**
 * An XML ReasonCode(@http://idanalytics.com/products/common.v1).
 *
 * This is a union type. Instances are of one of the following types:
 *     com.idanalytics.products.common_v1.ReasonCode$Member
 *     com.idanalytics.products.common_v1.ReasonCode$Member2
 */
public class ReasonCodeImpl extends org.apache.xmlbeans.impl.values.XmlUnionImpl implements com.idanalytics.products.common_v1.ReasonCode, com.idanalytics.products.common_v1.ReasonCode.Member, com.idanalytics.products.common_v1.ReasonCode.Member2
{
    private static final long serialVersionUID = 1L;
    
    public ReasonCodeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected ReasonCodeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
    /**
     * An anonymous inner XML type.
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.common_v1.ReasonCode$Member.
     */
    public static class MemberImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.common_v1.ReasonCode.Member
    {
        private static final long serialVersionUID = 1L;
        
        public MemberImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected MemberImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
    /**
     * An anonymous inner XML type.
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.common_v1.ReasonCode$Member2.
     */
    public static class MemberImpl2 extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.common_v1.ReasonCode.Member2
    {
        private static final long serialVersionUID = 1L;
        
        public MemberImpl2(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected MemberImpl2(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
