/*
 * XML Type:  DateType
 * Namespace: http://idanalytics.com/products/common.v1
 * Java type: com.idanalytics.products.common_v1.DateType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.common_v1.impl;
/**
 * An XML DateType(@http://idanalytics.com/products/common.v1).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.common_v1.DateType.
 */
public class DateTypeImpl extends org.apache.xmlbeans.impl.values.JavaGDateHolderEx implements com.idanalytics.products.common_v1.DateType
{
    private static final long serialVersionUID = 1L;
    
    public DateTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected DateTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
