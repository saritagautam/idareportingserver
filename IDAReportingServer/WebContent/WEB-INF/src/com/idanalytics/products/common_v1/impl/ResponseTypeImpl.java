/*
 * XML Type:  ResponseType
 * Namespace: http://idanalytics.com/products/common.v1
 * Java type: com.idanalytics.products.common_v1.ResponseType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.common_v1.impl;
/**
 * An XML ResponseType(@http://idanalytics.com/products/common.v1).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.common_v1.ResponseType.
 */
public class ResponseTypeImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.idanalytics.products.common_v1.ResponseType
{
    private static final long serialVersionUID = 1L;
    
    public ResponseTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected ResponseTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
