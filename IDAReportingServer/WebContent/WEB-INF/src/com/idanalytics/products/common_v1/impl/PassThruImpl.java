/*
 * XML Type:  PassThru
 * Namespace: http://idanalytics.com/products/common.v1
 * Java type: com.idanalytics.products.common_v1.PassThru
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.common_v1.impl;
/**
 * An XML PassThru(@http://idanalytics.com/products/common.v1).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.common_v1.PassThru.
 */
public class PassThruImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.common_v1.PassThru
{
    private static final long serialVersionUID = 1L;
    
    public PassThruImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected PassThruImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
