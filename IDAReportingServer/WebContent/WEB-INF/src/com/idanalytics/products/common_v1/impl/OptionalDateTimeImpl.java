/*
 * XML Type:  OptionalDateTime
 * Namespace: http://idanalytics.com/products/common.v1
 * Java type: com.idanalytics.products.common_v1.OptionalDateTime
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.common_v1.impl;
/**
 * An XML OptionalDateTime(@http://idanalytics.com/products/common.v1).
 *
 * This is a union type. Instances are of one of the following types:
 *     com.idanalytics.products.common_v1.OptionalDateTime$Member
 *     com.idanalytics.products.common_v1.OptionalDateTime$Member2
 */
public class OptionalDateTimeImpl extends org.apache.xmlbeans.impl.values.XmlUnionImpl implements com.idanalytics.products.common_v1.OptionalDateTime, com.idanalytics.products.common_v1.OptionalDateTime.Member, com.idanalytics.products.common_v1.OptionalDateTime.Member2
{
    private static final long serialVersionUID = 1L;
    
    public OptionalDateTimeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected OptionalDateTimeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
    /**
     * An anonymous inner XML type.
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.common_v1.OptionalDateTime$Member.
     */
    public static class MemberImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.common_v1.OptionalDateTime.Member
    {
        private static final long serialVersionUID = 1L;
        
        public MemberImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected MemberImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
    /**
     * An anonymous inner XML type.
     *
     * This is an atomic type that is a restriction of com.idanalytics.products.common_v1.OptionalDateTime$Member2.
     */
    public static class MemberImpl2 extends org.apache.xmlbeans.impl.values.JavaGDateHolderEx implements com.idanalytics.products.common_v1.OptionalDateTime.Member2
    {
        private static final long serialVersionUID = 1L;
        
        public MemberImpl2(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected MemberImpl2(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
