/*
 * XML Type:  ReconstructResponse_type
 * Namespace: http://idanalytics.com/products/resolve360/result
 * Java type: com.idanalytics.products.resolve360.result.ReconstructResponseType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.resolve360.result.impl;
/**
 * An XML ReconstructResponse_type(@http://idanalytics.com/products/resolve360/result).
 *
 * This is a complex type.
 */
public class ReconstructResponseTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.resolve360.result.ReconstructResponseType
{
    private static final long serialVersionUID = 1L;
    
    public ReconstructResponseTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IDANUMBER$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/resolve360/result", "IDANumber");
    private static final javax.xml.namespace.QName IDANUMBERRESULTCODE$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/resolve360/result", "IDANumberResultCode");
    private static final javax.xml.namespace.QName SSN5$4 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/resolve360/result", "SSN5");
    private static final javax.xml.namespace.QName SSN9$6 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/resolve360/result", "SSN9");
    private static final javax.xml.namespace.QName SSN5RESULTCODE$8 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/resolve360/result", "SSN5ResultCode");
    private static final javax.xml.namespace.QName SSN9RESULTCODE$10 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/resolve360/result", "SSN9ResultCode");
    private static final javax.xml.namespace.QName ADDRESS$12 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/resolve360/result", "Address");
    private static final javax.xml.namespace.QName ZIP$14 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/resolve360/result", "Zip");
    private static final javax.xml.namespace.QName CITY$16 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/resolve360/result", "City");
    private static final javax.xml.namespace.QName STATE$18 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/resolve360/result", "State");
    private static final javax.xml.namespace.QName ADDRESSRESULTCODE$20 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/resolve360/result", "AddressResultCode");
    private static final javax.xml.namespace.QName DOB$22 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/resolve360/result", "DOB");
    private static final javax.xml.namespace.QName DOBRESULTCODE$24 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/resolve360/result", "DOBResultCode");
    private static final javax.xml.namespace.QName PHONE$26 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/resolve360/result", "Phone");
    private static final javax.xml.namespace.QName PHONERESULTCODE$28 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/resolve360/result", "PhoneResultCode");
    private static final javax.xml.namespace.QName EMAIL$30 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/resolve360/result", "Email");
    private static final javax.xml.namespace.QName EMAILRESULTCODE$32 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/resolve360/result", "EmailResultCode");
    
    
    /**
     * Gets the "IDANumber" element
     */
    public java.lang.String getIDANumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDANUMBER$0, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IDANumber" element
     */
    public com.idanalytics.products.resolve360.result.IDANumber xgetIDANumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.resolve360.result.IDANumber target = null;
            target = (com.idanalytics.products.resolve360.result.IDANumber)get_store().find_element_user(IDANUMBER$0, 0);
            return target;
        }
    }
    
    /**
     * True if has "IDANumber" element
     */
    public boolean isSetIDANumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(IDANUMBER$0) != 0;
        }
    }
    
    /**
     * Sets the "IDANumber" element
     */
    public void setIDANumber(java.lang.String idaNumber)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDANUMBER$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDANUMBER$0);
            }
            target.setStringValue(idaNumber);
        }
    }
    
    /**
     * Sets (as xml) the "IDANumber" element
     */
    public void xsetIDANumber(com.idanalytics.products.resolve360.result.IDANumber idaNumber)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.resolve360.result.IDANumber target = null;
            target = (com.idanalytics.products.resolve360.result.IDANumber)get_store().find_element_user(IDANUMBER$0, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.resolve360.result.IDANumber)get_store().add_element_user(IDANUMBER$0);
            }
            target.set(idaNumber);
        }
    }
    
    /**
     * Unsets the "IDANumber" element
     */
    public void unsetIDANumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(IDANUMBER$0, 0);
        }
    }
    
    /**
     * Gets the "IDANumberResultCode" element
     */
    public java.lang.String getIDANumberResultCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDANUMBERRESULTCODE$2, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "IDANumberResultCode" element
     */
    public com.idanalytics.products.common_v1.ReasonCode xgetIDANumberResultCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.ReasonCode target = null;
            target = (com.idanalytics.products.common_v1.ReasonCode)get_store().find_element_user(IDANUMBERRESULTCODE$2, 0);
            return target;
        }
    }
    
    /**
     * True if has "IDANumberResultCode" element
     */
    public boolean isSetIDANumberResultCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(IDANUMBERRESULTCODE$2) != 0;
        }
    }
    
    /**
     * Sets the "IDANumberResultCode" element
     */
    public void setIDANumberResultCode(java.lang.String idaNumberResultCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDANUMBERRESULTCODE$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDANUMBERRESULTCODE$2);
            }
            target.setStringValue(idaNumberResultCode);
        }
    }
    
    /**
     * Sets (as xml) the "IDANumberResultCode" element
     */
    public void xsetIDANumberResultCode(com.idanalytics.products.common_v1.ReasonCode idaNumberResultCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.ReasonCode target = null;
            target = (com.idanalytics.products.common_v1.ReasonCode)get_store().find_element_user(IDANUMBERRESULTCODE$2, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.ReasonCode)get_store().add_element_user(IDANUMBERRESULTCODE$2);
            }
            target.set(idaNumberResultCode);
        }
    }
    
    /**
     * Unsets the "IDANumberResultCode" element
     */
    public void unsetIDANumberResultCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(IDANUMBERRESULTCODE$2, 0);
        }
    }
    
    /**
     * Gets the "SSN5" element
     */
    public java.lang.String getSSN5()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SSN5$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "SSN5" element
     */
    public com.idanalytics.products.common_v1.SSN5 xgetSSN5()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.SSN5 target = null;
            target = (com.idanalytics.products.common_v1.SSN5)get_store().find_element_user(SSN5$4, 0);
            return target;
        }
    }
    
    /**
     * True if has "SSN5" element
     */
    public boolean isSetSSN5()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SSN5$4) != 0;
        }
    }
    
    /**
     * Sets the "SSN5" element
     */
    public void setSSN5(java.lang.String ssn5)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SSN5$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SSN5$4);
            }
            target.setStringValue(ssn5);
        }
    }
    
    /**
     * Sets (as xml) the "SSN5" element
     */
    public void xsetSSN5(com.idanalytics.products.common_v1.SSN5 ssn5)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.SSN5 target = null;
            target = (com.idanalytics.products.common_v1.SSN5)get_store().find_element_user(SSN5$4, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.SSN5)get_store().add_element_user(SSN5$4);
            }
            target.set(ssn5);
        }
    }
    
    /**
     * Unsets the "SSN5" element
     */
    public void unsetSSN5()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SSN5$4, 0);
        }
    }
    
    /**
     * Gets the "SSN9" element
     */
    public java.lang.String getSSN9()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SSN9$6, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "SSN9" element
     */
    public com.idanalytics.products.common_v1.SSNType xgetSSN9()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.SSNType target = null;
            target = (com.idanalytics.products.common_v1.SSNType)get_store().find_element_user(SSN9$6, 0);
            return target;
        }
    }
    
    /**
     * True if has "SSN9" element
     */
    public boolean isSetSSN9()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SSN9$6) != 0;
        }
    }
    
    /**
     * Sets the "SSN9" element
     */
    public void setSSN9(java.lang.String ssn9)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SSN9$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SSN9$6);
            }
            target.setStringValue(ssn9);
        }
    }
    
    /**
     * Sets (as xml) the "SSN9" element
     */
    public void xsetSSN9(com.idanalytics.products.common_v1.SSNType ssn9)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.SSNType target = null;
            target = (com.idanalytics.products.common_v1.SSNType)get_store().find_element_user(SSN9$6, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.SSNType)get_store().add_element_user(SSN9$6);
            }
            target.set(ssn9);
        }
    }
    
    /**
     * Unsets the "SSN9" element
     */
    public void unsetSSN9()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SSN9$6, 0);
        }
    }
    
    /**
     * Gets the "SSN5ResultCode" element
     */
    public java.lang.String getSSN5ResultCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SSN5RESULTCODE$8, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "SSN5ResultCode" element
     */
    public com.idanalytics.products.common_v1.ReasonCode xgetSSN5ResultCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.ReasonCode target = null;
            target = (com.idanalytics.products.common_v1.ReasonCode)get_store().find_element_user(SSN5RESULTCODE$8, 0);
            return target;
        }
    }
    
    /**
     * True if has "SSN5ResultCode" element
     */
    public boolean isSetSSN5ResultCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SSN5RESULTCODE$8) != 0;
        }
    }
    
    /**
     * Sets the "SSN5ResultCode" element
     */
    public void setSSN5ResultCode(java.lang.String ssn5ResultCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SSN5RESULTCODE$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SSN5RESULTCODE$8);
            }
            target.setStringValue(ssn5ResultCode);
        }
    }
    
    /**
     * Sets (as xml) the "SSN5ResultCode" element
     */
    public void xsetSSN5ResultCode(com.idanalytics.products.common_v1.ReasonCode ssn5ResultCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.ReasonCode target = null;
            target = (com.idanalytics.products.common_v1.ReasonCode)get_store().find_element_user(SSN5RESULTCODE$8, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.ReasonCode)get_store().add_element_user(SSN5RESULTCODE$8);
            }
            target.set(ssn5ResultCode);
        }
    }
    
    /**
     * Unsets the "SSN5ResultCode" element
     */
    public void unsetSSN5ResultCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SSN5RESULTCODE$8, 0);
        }
    }
    
    /**
     * Gets the "SSN9ResultCode" element
     */
    public java.lang.String getSSN9ResultCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SSN9RESULTCODE$10, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "SSN9ResultCode" element
     */
    public com.idanalytics.products.common_v1.ReasonCode xgetSSN9ResultCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.ReasonCode target = null;
            target = (com.idanalytics.products.common_v1.ReasonCode)get_store().find_element_user(SSN9RESULTCODE$10, 0);
            return target;
        }
    }
    
    /**
     * True if has "SSN9ResultCode" element
     */
    public boolean isSetSSN9ResultCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SSN9RESULTCODE$10) != 0;
        }
    }
    
    /**
     * Sets the "SSN9ResultCode" element
     */
    public void setSSN9ResultCode(java.lang.String ssn9ResultCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SSN9RESULTCODE$10, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SSN9RESULTCODE$10);
            }
            target.setStringValue(ssn9ResultCode);
        }
    }
    
    /**
     * Sets (as xml) the "SSN9ResultCode" element
     */
    public void xsetSSN9ResultCode(com.idanalytics.products.common_v1.ReasonCode ssn9ResultCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.ReasonCode target = null;
            target = (com.idanalytics.products.common_v1.ReasonCode)get_store().find_element_user(SSN9RESULTCODE$10, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.ReasonCode)get_store().add_element_user(SSN9RESULTCODE$10);
            }
            target.set(ssn9ResultCode);
        }
    }
    
    /**
     * Unsets the "SSN9ResultCode" element
     */
    public void unsetSSN9ResultCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SSN9RESULTCODE$10, 0);
        }
    }
    
    /**
     * Gets the "Address" element
     */
    public java.lang.String getAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ADDRESS$12, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Address" element
     */
    public com.idanalytics.products.common_v1.AddressType xgetAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.AddressType target = null;
            target = (com.idanalytics.products.common_v1.AddressType)get_store().find_element_user(ADDRESS$12, 0);
            return target;
        }
    }
    
    /**
     * True if has "Address" element
     */
    public boolean isSetAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ADDRESS$12) != 0;
        }
    }
    
    /**
     * Sets the "Address" element
     */
    public void setAddress(java.lang.String address)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ADDRESS$12, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ADDRESS$12);
            }
            target.setStringValue(address);
        }
    }
    
    /**
     * Sets (as xml) the "Address" element
     */
    public void xsetAddress(com.idanalytics.products.common_v1.AddressType address)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.AddressType target = null;
            target = (com.idanalytics.products.common_v1.AddressType)get_store().find_element_user(ADDRESS$12, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.AddressType)get_store().add_element_user(ADDRESS$12);
            }
            target.set(address);
        }
    }
    
    /**
     * Unsets the "Address" element
     */
    public void unsetAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ADDRESS$12, 0);
        }
    }
    
    /**
     * Gets the "Zip" element
     */
    public java.lang.String getZip()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ZIP$14, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Zip" element
     */
    public com.idanalytics.products.common_v1.ZipType xgetZip()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.ZipType target = null;
            target = (com.idanalytics.products.common_v1.ZipType)get_store().find_element_user(ZIP$14, 0);
            return target;
        }
    }
    
    /**
     * True if has "Zip" element
     */
    public boolean isSetZip()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ZIP$14) != 0;
        }
    }
    
    /**
     * Sets the "Zip" element
     */
    public void setZip(java.lang.String zip)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ZIP$14, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ZIP$14);
            }
            target.setStringValue(zip);
        }
    }
    
    /**
     * Sets (as xml) the "Zip" element
     */
    public void xsetZip(com.idanalytics.products.common_v1.ZipType zip)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.ZipType target = null;
            target = (com.idanalytics.products.common_v1.ZipType)get_store().find_element_user(ZIP$14, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.ZipType)get_store().add_element_user(ZIP$14);
            }
            target.set(zip);
        }
    }
    
    /**
     * Unsets the "Zip" element
     */
    public void unsetZip()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ZIP$14, 0);
        }
    }
    
    /**
     * Gets the "City" element
     */
    public java.lang.String getCity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CITY$16, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "City" element
     */
    public com.idanalytics.products.common_v1.CityType xgetCity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.CityType target = null;
            target = (com.idanalytics.products.common_v1.CityType)get_store().find_element_user(CITY$16, 0);
            return target;
        }
    }
    
    /**
     * True if has "City" element
     */
    public boolean isSetCity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CITY$16) != 0;
        }
    }
    
    /**
     * Sets the "City" element
     */
    public void setCity(java.lang.String city)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CITY$16, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CITY$16);
            }
            target.setStringValue(city);
        }
    }
    
    /**
     * Sets (as xml) the "City" element
     */
    public void xsetCity(com.idanalytics.products.common_v1.CityType city)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.CityType target = null;
            target = (com.idanalytics.products.common_v1.CityType)get_store().find_element_user(CITY$16, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.CityType)get_store().add_element_user(CITY$16);
            }
            target.set(city);
        }
    }
    
    /**
     * Unsets the "City" element
     */
    public void unsetCity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CITY$16, 0);
        }
    }
    
    /**
     * Gets the "State" element
     */
    public java.lang.String getState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STATE$18, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "State" element
     */
    public com.idanalytics.products.common_v1.StateType xgetState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.StateType target = null;
            target = (com.idanalytics.products.common_v1.StateType)get_store().find_element_user(STATE$18, 0);
            return target;
        }
    }
    
    /**
     * True if has "State" element
     */
    public boolean isSetState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(STATE$18) != 0;
        }
    }
    
    /**
     * Sets the "State" element
     */
    public void setState(java.lang.String state)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STATE$18, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(STATE$18);
            }
            target.setStringValue(state);
        }
    }
    
    /**
     * Sets (as xml) the "State" element
     */
    public void xsetState(com.idanalytics.products.common_v1.StateType state)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.StateType target = null;
            target = (com.idanalytics.products.common_v1.StateType)get_store().find_element_user(STATE$18, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.StateType)get_store().add_element_user(STATE$18);
            }
            target.set(state);
        }
    }
    
    /**
     * Unsets the "State" element
     */
    public void unsetState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(STATE$18, 0);
        }
    }
    
    /**
     * Gets the "AddressResultCode" element
     */
    public java.lang.String getAddressResultCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ADDRESSRESULTCODE$20, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "AddressResultCode" element
     */
    public com.idanalytics.products.common_v1.ReasonCode xgetAddressResultCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.ReasonCode target = null;
            target = (com.idanalytics.products.common_v1.ReasonCode)get_store().find_element_user(ADDRESSRESULTCODE$20, 0);
            return target;
        }
    }
    
    /**
     * True if has "AddressResultCode" element
     */
    public boolean isSetAddressResultCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ADDRESSRESULTCODE$20) != 0;
        }
    }
    
    /**
     * Sets the "AddressResultCode" element
     */
    public void setAddressResultCode(java.lang.String addressResultCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ADDRESSRESULTCODE$20, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ADDRESSRESULTCODE$20);
            }
            target.setStringValue(addressResultCode);
        }
    }
    
    /**
     * Sets (as xml) the "AddressResultCode" element
     */
    public void xsetAddressResultCode(com.idanalytics.products.common_v1.ReasonCode addressResultCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.ReasonCode target = null;
            target = (com.idanalytics.products.common_v1.ReasonCode)get_store().find_element_user(ADDRESSRESULTCODE$20, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.ReasonCode)get_store().add_element_user(ADDRESSRESULTCODE$20);
            }
            target.set(addressResultCode);
        }
    }
    
    /**
     * Unsets the "AddressResultCode" element
     */
    public void unsetAddressResultCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ADDRESSRESULTCODE$20, 0);
        }
    }
    
    /**
     * Gets the "DOB" element
     */
    public java.lang.String getDOB()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DOB$22, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "DOB" element
     */
    public com.idanalytics.products.resolve360.result.DOBType xgetDOB()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.resolve360.result.DOBType target = null;
            target = (com.idanalytics.products.resolve360.result.DOBType)get_store().find_element_user(DOB$22, 0);
            return target;
        }
    }
    
    /**
     * True if has "DOB" element
     */
    public boolean isSetDOB()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(DOB$22) != 0;
        }
    }
    
    /**
     * Sets the "DOB" element
     */
    public void setDOB(java.lang.String dob)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DOB$22, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DOB$22);
            }
            target.setStringValue(dob);
        }
    }
    
    /**
     * Sets (as xml) the "DOB" element
     */
    public void xsetDOB(com.idanalytics.products.resolve360.result.DOBType dob)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.resolve360.result.DOBType target = null;
            target = (com.idanalytics.products.resolve360.result.DOBType)get_store().find_element_user(DOB$22, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.resolve360.result.DOBType)get_store().add_element_user(DOB$22);
            }
            target.set(dob);
        }
    }
    
    /**
     * Unsets the "DOB" element
     */
    public void unsetDOB()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(DOB$22, 0);
        }
    }
    
    /**
     * Gets the "DOBResultCode" element
     */
    public java.lang.String getDOBResultCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DOBRESULTCODE$24, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "DOBResultCode" element
     */
    public com.idanalytics.products.common_v1.ReasonCode xgetDOBResultCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.ReasonCode target = null;
            target = (com.idanalytics.products.common_v1.ReasonCode)get_store().find_element_user(DOBRESULTCODE$24, 0);
            return target;
        }
    }
    
    /**
     * True if has "DOBResultCode" element
     */
    public boolean isSetDOBResultCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(DOBRESULTCODE$24) != 0;
        }
    }
    
    /**
     * Sets the "DOBResultCode" element
     */
    public void setDOBResultCode(java.lang.String dobResultCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DOBRESULTCODE$24, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DOBRESULTCODE$24);
            }
            target.setStringValue(dobResultCode);
        }
    }
    
    /**
     * Sets (as xml) the "DOBResultCode" element
     */
    public void xsetDOBResultCode(com.idanalytics.products.common_v1.ReasonCode dobResultCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.ReasonCode target = null;
            target = (com.idanalytics.products.common_v1.ReasonCode)get_store().find_element_user(DOBRESULTCODE$24, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.ReasonCode)get_store().add_element_user(DOBRESULTCODE$24);
            }
            target.set(dobResultCode);
        }
    }
    
    /**
     * Unsets the "DOBResultCode" element
     */
    public void unsetDOBResultCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(DOBRESULTCODE$24, 0);
        }
    }
    
    /**
     * Gets the "Phone" element
     */
    public java.lang.String getPhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PHONE$26, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Phone" element
     */
    public com.idanalytics.products.resolve360.result.PhoneType xgetPhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.resolve360.result.PhoneType target = null;
            target = (com.idanalytics.products.resolve360.result.PhoneType)get_store().find_element_user(PHONE$26, 0);
            return target;
        }
    }
    
    /**
     * True if has "Phone" element
     */
    public boolean isSetPhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PHONE$26) != 0;
        }
    }
    
    /**
     * Sets the "Phone" element
     */
    public void setPhone(java.lang.String phone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PHONE$26, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PHONE$26);
            }
            target.setStringValue(phone);
        }
    }
    
    /**
     * Sets (as xml) the "Phone" element
     */
    public void xsetPhone(com.idanalytics.products.resolve360.result.PhoneType phone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.resolve360.result.PhoneType target = null;
            target = (com.idanalytics.products.resolve360.result.PhoneType)get_store().find_element_user(PHONE$26, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.resolve360.result.PhoneType)get_store().add_element_user(PHONE$26);
            }
            target.set(phone);
        }
    }
    
    /**
     * Unsets the "Phone" element
     */
    public void unsetPhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PHONE$26, 0);
        }
    }
    
    /**
     * Gets the "PhoneResultCode" element
     */
    public java.lang.String getPhoneResultCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PHONERESULTCODE$28, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "PhoneResultCode" element
     */
    public com.idanalytics.products.common_v1.ReasonCode xgetPhoneResultCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.ReasonCode target = null;
            target = (com.idanalytics.products.common_v1.ReasonCode)get_store().find_element_user(PHONERESULTCODE$28, 0);
            return target;
        }
    }
    
    /**
     * True if has "PhoneResultCode" element
     */
    public boolean isSetPhoneResultCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PHONERESULTCODE$28) != 0;
        }
    }
    
    /**
     * Sets the "PhoneResultCode" element
     */
    public void setPhoneResultCode(java.lang.String phoneResultCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PHONERESULTCODE$28, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PHONERESULTCODE$28);
            }
            target.setStringValue(phoneResultCode);
        }
    }
    
    /**
     * Sets (as xml) the "PhoneResultCode" element
     */
    public void xsetPhoneResultCode(com.idanalytics.products.common_v1.ReasonCode phoneResultCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.ReasonCode target = null;
            target = (com.idanalytics.products.common_v1.ReasonCode)get_store().find_element_user(PHONERESULTCODE$28, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.ReasonCode)get_store().add_element_user(PHONERESULTCODE$28);
            }
            target.set(phoneResultCode);
        }
    }
    
    /**
     * Unsets the "PhoneResultCode" element
     */
    public void unsetPhoneResultCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PHONERESULTCODE$28, 0);
        }
    }
    
    /**
     * Gets the "Email" element
     */
    public java.lang.String getEmail()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EMAIL$30, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "Email" element
     */
    public com.idanalytics.products.resolve360.result.EmailType xgetEmail()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.resolve360.result.EmailType target = null;
            target = (com.idanalytics.products.resolve360.result.EmailType)get_store().find_element_user(EMAIL$30, 0);
            return target;
        }
    }
    
    /**
     * True if has "Email" element
     */
    public boolean isSetEmail()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(EMAIL$30) != 0;
        }
    }
    
    /**
     * Sets the "Email" element
     */
    public void setEmail(java.lang.String email)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EMAIL$30, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(EMAIL$30);
            }
            target.setStringValue(email);
        }
    }
    
    /**
     * Sets (as xml) the "Email" element
     */
    public void xsetEmail(com.idanalytics.products.resolve360.result.EmailType email)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.resolve360.result.EmailType target = null;
            target = (com.idanalytics.products.resolve360.result.EmailType)get_store().find_element_user(EMAIL$30, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.resolve360.result.EmailType)get_store().add_element_user(EMAIL$30);
            }
            target.set(email);
        }
    }
    
    /**
     * Unsets the "Email" element
     */
    public void unsetEmail()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(EMAIL$30, 0);
        }
    }
    
    /**
     * Gets the "EmailResultCode" element
     */
    public java.lang.String getEmailResultCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EMAILRESULTCODE$32, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "EmailResultCode" element
     */
    public com.idanalytics.products.common_v1.ReasonCode xgetEmailResultCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.ReasonCode target = null;
            target = (com.idanalytics.products.common_v1.ReasonCode)get_store().find_element_user(EMAILRESULTCODE$32, 0);
            return target;
        }
    }
    
    /**
     * True if has "EmailResultCode" element
     */
    public boolean isSetEmailResultCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(EMAILRESULTCODE$32) != 0;
        }
    }
    
    /**
     * Sets the "EmailResultCode" element
     */
    public void setEmailResultCode(java.lang.String emailResultCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(EMAILRESULTCODE$32, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(EMAILRESULTCODE$32);
            }
            target.setStringValue(emailResultCode);
        }
    }
    
    /**
     * Sets (as xml) the "EmailResultCode" element
     */
    public void xsetEmailResultCode(com.idanalytics.products.common_v1.ReasonCode emailResultCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.common_v1.ReasonCode target = null;
            target = (com.idanalytics.products.common_v1.ReasonCode)get_store().find_element_user(EMAILRESULTCODE$32, 0);
            if (target == null)
            {
                target = (com.idanalytics.products.common_v1.ReasonCode)get_store().add_element_user(EMAILRESULTCODE$32);
            }
            target.set(emailResultCode);
        }
    }
    
    /**
     * Unsets the "EmailResultCode" element
     */
    public void unsetEmailResultCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(EMAILRESULTCODE$32, 0);
        }
    }
}
