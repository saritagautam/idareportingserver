/*
 * An XML document type.
 * Localname: Resolve360Response
 * Namespace: http://idanalytics.com/products/resolve360/result
 * Java type: com.idanalytics.products.resolve360.result.Resolve360ResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.resolve360.result.impl;
/**
 * A document containing one Resolve360Response(@http://idanalytics.com/products/resolve360/result) element.
 *
 * This is a complex type.
 */
public class Resolve360ResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.resolve360.result.Resolve360ResponseDocument
{
    private static final long serialVersionUID = 1L;
    
    public Resolve360ResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName RESOLVE360RESPONSE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/products/resolve360/result", "Resolve360Response");
    
    
    /**
     * Gets the "Resolve360Response" element
     */
    public com.idanalytics.products.resolve360.result.Resolve360ResponseDocument.Resolve360Response getResolve360Response()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.resolve360.result.Resolve360ResponseDocument.Resolve360Response target = null;
            target = (com.idanalytics.products.resolve360.result.Resolve360ResponseDocument.Resolve360Response)get_store().find_element_user(RESOLVE360RESPONSE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "Resolve360Response" element
     */
    public void setResolve360Response(com.idanalytics.products.resolve360.result.Resolve360ResponseDocument.Resolve360Response resolve360Response)
    {
        generatedSetterHelperImpl(resolve360Response, RESOLVE360RESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "Resolve360Response" element
     */
    public com.idanalytics.products.resolve360.result.Resolve360ResponseDocument.Resolve360Response addNewResolve360Response()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.products.resolve360.result.Resolve360ResponseDocument.Resolve360Response target = null;
            target = (com.idanalytics.products.resolve360.result.Resolve360ResponseDocument.Resolve360Response)get_store().add_element_user(RESOLVE360RESPONSE$0);
            return target;
        }
    }
    /**
     * An XML Resolve360Response(@http://idanalytics.com/products/resolve360/result).
     *
     * This is a complex type.
     */
    public static class Resolve360ResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.products.resolve360.result.Resolve360ResponseDocument.Resolve360Response
    {
        private static final long serialVersionUID = 1L;
        
        public Resolve360ResponseImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName IDASTATUS$0 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/resolve360/result", "IDAStatus");
        private static final javax.xml.namespace.QName APPID$2 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/resolve360/result", "AppID");
        private static final javax.xml.namespace.QName DESIGNATION$4 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/resolve360/result", "Designation");
        private static final javax.xml.namespace.QName IDASEQUENCE$6 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/resolve360/result", "IDASequence");
        private static final javax.xml.namespace.QName IDATIMESTAMP$8 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/resolve360/result", "IDATimeStamp");
        private static final javax.xml.namespace.QName RECONSTRUCTRESPONSE$10 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/resolve360/result", "ReconstructResponse");
        private static final javax.xml.namespace.QName PASSTHRU1$12 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/resolve360/result", "PassThru1");
        private static final javax.xml.namespace.QName PASSTHRU2$14 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/resolve360/result", "PassThru2");
        private static final javax.xml.namespace.QName PASSTHRU3$16 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/resolve360/result", "PassThru3");
        private static final javax.xml.namespace.QName PASSTHRU4$18 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/resolve360/result", "PassThru4");
        private static final javax.xml.namespace.QName IDAINTERNAL$20 = 
            new javax.xml.namespace.QName("http://idanalytics.com/products/resolve360/result", "IDAInternal");
        
        
        /**
         * Gets the "IDAStatus" element
         */
        public int getIDAStatus()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASTATUS$0, 0);
                if (target == null)
                {
                    return 0;
                }
                return target.getIntValue();
            }
        }
        
        /**
         * Gets (as xml) the "IDAStatus" element
         */
        public com.idanalytics.products.common_v1.IDAStatus xgetIDAStatus()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDAStatus target = null;
                target = (com.idanalytics.products.common_v1.IDAStatus)get_store().find_element_user(IDASTATUS$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "IDAStatus" element
         */
        public void setIDAStatus(int idaStatus)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASTATUS$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDASTATUS$0);
                }
                target.setIntValue(idaStatus);
            }
        }
        
        /**
         * Sets (as xml) the "IDAStatus" element
         */
        public void xsetIDAStatus(com.idanalytics.products.common_v1.IDAStatus idaStatus)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDAStatus target = null;
                target = (com.idanalytics.products.common_v1.IDAStatus)get_store().find_element_user(IDASTATUS$0, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.IDAStatus)get_store().add_element_user(IDASTATUS$0);
                }
                target.set(idaStatus);
            }
        }
        
        /**
         * Gets the "AppID" element
         */
        public java.lang.String getAppID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(APPID$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "AppID" element
         */
        public com.idanalytics.products.common_v1.AppID xgetAppID()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.AppID target = null;
                target = (com.idanalytics.products.common_v1.AppID)get_store().find_element_user(APPID$2, 0);
                return target;
            }
        }
        
        /**
         * Sets the "AppID" element
         */
        public void setAppID(java.lang.String appID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(APPID$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(APPID$2);
                }
                target.setStringValue(appID);
            }
        }
        
        /**
         * Sets (as xml) the "AppID" element
         */
        public void xsetAppID(com.idanalytics.products.common_v1.AppID appID)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.AppID target = null;
                target = (com.idanalytics.products.common_v1.AppID)get_store().find_element_user(APPID$2, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.AppID)get_store().add_element_user(APPID$2);
                }
                target.set(appID);
            }
        }
        
        /**
         * Gets the "Designation" element
         */
        public com.idanalytics.products.common_v1.Designation.Enum getDesignation()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESIGNATION$4, 0);
                if (target == null)
                {
                    return null;
                }
                return (com.idanalytics.products.common_v1.Designation.Enum)target.getEnumValue();
            }
        }
        
        /**
         * Gets (as xml) the "Designation" element
         */
        public com.idanalytics.products.common_v1.Designation xgetDesignation()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.Designation target = null;
                target = (com.idanalytics.products.common_v1.Designation)get_store().find_element_user(DESIGNATION$4, 0);
                return target;
            }
        }
        
        /**
         * True if has "Designation" element
         */
        public boolean isSetDesignation()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(DESIGNATION$4) != 0;
            }
        }
        
        /**
         * Sets the "Designation" element
         */
        public void setDesignation(com.idanalytics.products.common_v1.Designation.Enum designation)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DESIGNATION$4, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DESIGNATION$4);
                }
                target.setEnumValue(designation);
            }
        }
        
        /**
         * Sets (as xml) the "Designation" element
         */
        public void xsetDesignation(com.idanalytics.products.common_v1.Designation designation)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.Designation target = null;
                target = (com.idanalytics.products.common_v1.Designation)get_store().find_element_user(DESIGNATION$4, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.Designation)get_store().add_element_user(DESIGNATION$4);
                }
                target.set(designation);
            }
        }
        
        /**
         * Unsets the "Designation" element
         */
        public void unsetDesignation()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(DESIGNATION$4, 0);
            }
        }
        
        /**
         * Gets the "IDASequence" element
         */
        public java.lang.String getIDASequence()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASEQUENCE$6, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "IDASequence" element
         */
        public com.idanalytics.products.common_v1.IDASequence xgetIDASequence()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDASequence target = null;
                target = (com.idanalytics.products.common_v1.IDASequence)get_store().find_element_user(IDASEQUENCE$6, 0);
                return target;
            }
        }
        
        /**
         * Sets the "IDASequence" element
         */
        public void setIDASequence(java.lang.String idaSequence)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDASEQUENCE$6, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDASEQUENCE$6);
                }
                target.setStringValue(idaSequence);
            }
        }
        
        /**
         * Sets (as xml) the "IDASequence" element
         */
        public void xsetIDASequence(com.idanalytics.products.common_v1.IDASequence idaSequence)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDASequence target = null;
                target = (com.idanalytics.products.common_v1.IDASequence)get_store().find_element_user(IDASEQUENCE$6, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.IDASequence)get_store().add_element_user(IDASEQUENCE$6);
                }
                target.set(idaSequence);
            }
        }
        
        /**
         * Gets the "IDATimeStamp" element
         */
        public java.util.Calendar getIDATimeStamp()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDATIMESTAMP$8, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getCalendarValue();
            }
        }
        
        /**
         * Gets (as xml) the "IDATimeStamp" element
         */
        public com.idanalytics.products.common_v1.IDATimeStamp xgetIDATimeStamp()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDATimeStamp target = null;
                target = (com.idanalytics.products.common_v1.IDATimeStamp)get_store().find_element_user(IDATIMESTAMP$8, 0);
                return target;
            }
        }
        
        /**
         * Sets the "IDATimeStamp" element
         */
        public void setIDATimeStamp(java.util.Calendar idaTimeStamp)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(IDATIMESTAMP$8, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(IDATIMESTAMP$8);
                }
                target.setCalendarValue(idaTimeStamp);
            }
        }
        
        /**
         * Sets (as xml) the "IDATimeStamp" element
         */
        public void xsetIDATimeStamp(com.idanalytics.products.common_v1.IDATimeStamp idaTimeStamp)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.IDATimeStamp target = null;
                target = (com.idanalytics.products.common_v1.IDATimeStamp)get_store().find_element_user(IDATIMESTAMP$8, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.IDATimeStamp)get_store().add_element_user(IDATIMESTAMP$8);
                }
                target.set(idaTimeStamp);
            }
        }
        
        /**
         * Gets the "ReconstructResponse" element
         */
        public com.idanalytics.products.resolve360.result.ReconstructResponseType getReconstructResponse()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.resolve360.result.ReconstructResponseType target = null;
                target = (com.idanalytics.products.resolve360.result.ReconstructResponseType)get_store().find_element_user(RECONSTRUCTRESPONSE$10, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "ReconstructResponse" element
         */
        public boolean isSetReconstructResponse()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(RECONSTRUCTRESPONSE$10) != 0;
            }
        }
        
        /**
         * Sets the "ReconstructResponse" element
         */
        public void setReconstructResponse(com.idanalytics.products.resolve360.result.ReconstructResponseType reconstructResponse)
        {
            generatedSetterHelperImpl(reconstructResponse, RECONSTRUCTRESPONSE$10, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "ReconstructResponse" element
         */
        public com.idanalytics.products.resolve360.result.ReconstructResponseType addNewReconstructResponse()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.resolve360.result.ReconstructResponseType target = null;
                target = (com.idanalytics.products.resolve360.result.ReconstructResponseType)get_store().add_element_user(RECONSTRUCTRESPONSE$10);
                return target;
            }
        }
        
        /**
         * Unsets the "ReconstructResponse" element
         */
        public void unsetReconstructResponse()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(RECONSTRUCTRESPONSE$10, 0);
            }
        }
        
        /**
         * Gets the "PassThru1" element
         */
        public java.lang.String getPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU1$12, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru1" element
         */
        public com.idanalytics.products.common_v1.PassThru xgetPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU1$12, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru1" element
         */
        public boolean isSetPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU1$12) != 0;
            }
        }
        
        /**
         * Sets the "PassThru1" element
         */
        public void setPassThru1(java.lang.String passThru1)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU1$12, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU1$12);
                }
                target.setStringValue(passThru1);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru1" element
         */
        public void xsetPassThru1(com.idanalytics.products.common_v1.PassThru passThru1)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU1$12, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.PassThru)get_store().add_element_user(PASSTHRU1$12);
                }
                target.set(passThru1);
            }
        }
        
        /**
         * Unsets the "PassThru1" element
         */
        public void unsetPassThru1()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU1$12, 0);
            }
        }
        
        /**
         * Gets the "PassThru2" element
         */
        public java.lang.String getPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU2$14, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru2" element
         */
        public com.idanalytics.products.common_v1.PassThru xgetPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU2$14, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru2" element
         */
        public boolean isSetPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU2$14) != 0;
            }
        }
        
        /**
         * Sets the "PassThru2" element
         */
        public void setPassThru2(java.lang.String passThru2)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU2$14, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU2$14);
                }
                target.setStringValue(passThru2);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru2" element
         */
        public void xsetPassThru2(com.idanalytics.products.common_v1.PassThru passThru2)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU2$14, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.PassThru)get_store().add_element_user(PASSTHRU2$14);
                }
                target.set(passThru2);
            }
        }
        
        /**
         * Unsets the "PassThru2" element
         */
        public void unsetPassThru2()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU2$14, 0);
            }
        }
        
        /**
         * Gets the "PassThru3" element
         */
        public java.lang.String getPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU3$16, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru3" element
         */
        public com.idanalytics.products.common_v1.PassThru xgetPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU3$16, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru3" element
         */
        public boolean isSetPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU3$16) != 0;
            }
        }
        
        /**
         * Sets the "PassThru3" element
         */
        public void setPassThru3(java.lang.String passThru3)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU3$16, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU3$16);
                }
                target.setStringValue(passThru3);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru3" element
         */
        public void xsetPassThru3(com.idanalytics.products.common_v1.PassThru passThru3)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU3$16, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.PassThru)get_store().add_element_user(PASSTHRU3$16);
                }
                target.set(passThru3);
            }
        }
        
        /**
         * Unsets the "PassThru3" element
         */
        public void unsetPassThru3()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU3$16, 0);
            }
        }
        
        /**
         * Gets the "PassThru4" element
         */
        public java.lang.String getPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU4$18, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "PassThru4" element
         */
        public com.idanalytics.products.common_v1.PassThru xgetPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU4$18, 0);
                return target;
            }
        }
        
        /**
         * True if has "PassThru4" element
         */
        public boolean isSetPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(PASSTHRU4$18) != 0;
            }
        }
        
        /**
         * Sets the "PassThru4" element
         */
        public void setPassThru4(java.lang.String passThru4)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(PASSTHRU4$18, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(PASSTHRU4$18);
                }
                target.setStringValue(passThru4);
            }
        }
        
        /**
         * Sets (as xml) the "PassThru4" element
         */
        public void xsetPassThru4(com.idanalytics.products.common_v1.PassThru passThru4)
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.common_v1.PassThru target = null;
                target = (com.idanalytics.products.common_v1.PassThru)get_store().find_element_user(PASSTHRU4$18, 0);
                if (target == null)
                {
                    target = (com.idanalytics.products.common_v1.PassThru)get_store().add_element_user(PASSTHRU4$18);
                }
                target.set(passThru4);
            }
        }
        
        /**
         * Unsets the "PassThru4" element
         */
        public void unsetPassThru4()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(PASSTHRU4$18, 0);
            }
        }
        
        /**
         * Gets the "IDAInternal" element
         */
        public com.idanalytics.products.resolve360.result.IDAInternalDocument.IDAInternal getIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.resolve360.result.IDAInternalDocument.IDAInternal target = null;
                target = (com.idanalytics.products.resolve360.result.IDAInternalDocument.IDAInternal)get_store().find_element_user(IDAINTERNAL$20, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "IDAInternal" element
         */
        public boolean isSetIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(IDAINTERNAL$20) != 0;
            }
        }
        
        /**
         * Sets the "IDAInternal" element
         */
        public void setIDAInternal(com.idanalytics.products.resolve360.result.IDAInternalDocument.IDAInternal idaInternal)
        {
            generatedSetterHelperImpl(idaInternal, IDAINTERNAL$20, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
        }
        
        /**
         * Appends and returns a new empty "IDAInternal" element
         */
        public com.idanalytics.products.resolve360.result.IDAInternalDocument.IDAInternal addNewIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                com.idanalytics.products.resolve360.result.IDAInternalDocument.IDAInternal target = null;
                target = (com.idanalytics.products.resolve360.result.IDAInternalDocument.IDAInternal)get_store().add_element_user(IDAINTERNAL$20);
                return target;
            }
        }
        
        /**
         * Unsets the "IDAInternal" element
         */
        public void unsetIDAInternal()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(IDAINTERNAL$20, 0);
            }
        }
    }
}
