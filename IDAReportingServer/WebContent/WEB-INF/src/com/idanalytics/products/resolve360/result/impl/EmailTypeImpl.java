/*
 * XML Type:  EmailType
 * Namespace: http://idanalytics.com/products/resolve360/result
 * Java type: com.idanalytics.products.resolve360.result.EmailType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.resolve360.result.impl;
/**
 * An XML EmailType(@http://idanalytics.com/products/resolve360/result).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.resolve360.result.EmailType.
 */
public class EmailTypeImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.resolve360.result.EmailType
{
    private static final long serialVersionUID = 1L;
    
    public EmailTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected EmailTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
