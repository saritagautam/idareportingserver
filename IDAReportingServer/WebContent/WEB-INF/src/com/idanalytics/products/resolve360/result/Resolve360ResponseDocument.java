/*
 * An XML document type.
 * Localname: Resolve360Response
 * Namespace: http://idanalytics.com/products/resolve360/result
 * Java type: com.idanalytics.products.resolve360.result.Resolve360ResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.resolve360.result;


/**
 * A document containing one Resolve360Response(@http://idanalytics.com/products/resolve360/result) element.
 *
 * This is a complex type.
 */
public interface Resolve360ResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Resolve360ResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("resolve360responsed71edoctype");
    
    /**
     * Gets the "Resolve360Response" element
     */
    com.idanalytics.products.resolve360.result.Resolve360ResponseDocument.Resolve360Response getResolve360Response();
    
    /**
     * Sets the "Resolve360Response" element
     */
    void setResolve360Response(com.idanalytics.products.resolve360.result.Resolve360ResponseDocument.Resolve360Response resolve360Response);
    
    /**
     * Appends and returns a new empty "Resolve360Response" element
     */
    com.idanalytics.products.resolve360.result.Resolve360ResponseDocument.Resolve360Response addNewResolve360Response();
    
    /**
     * An XML Resolve360Response(@http://idanalytics.com/products/resolve360/result).
     *
     * This is a complex type.
     */
    public interface Resolve360Response extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Resolve360Response.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("resolve360response8e80elemtype");
        
        /**
         * Gets the "IDAStatus" element
         */
        int getIDAStatus();
        
        /**
         * Gets (as xml) the "IDAStatus" element
         */
        com.idanalytics.products.common_v1.IDAStatus xgetIDAStatus();
        
        /**
         * Sets the "IDAStatus" element
         */
        void setIDAStatus(int idaStatus);
        
        /**
         * Sets (as xml) the "IDAStatus" element
         */
        void xsetIDAStatus(com.idanalytics.products.common_v1.IDAStatus idaStatus);
        
        /**
         * Gets the "AppID" element
         */
        java.lang.String getAppID();
        
        /**
         * Gets (as xml) the "AppID" element
         */
        com.idanalytics.products.common_v1.AppID xgetAppID();
        
        /**
         * Sets the "AppID" element
         */
        void setAppID(java.lang.String appID);
        
        /**
         * Sets (as xml) the "AppID" element
         */
        void xsetAppID(com.idanalytics.products.common_v1.AppID appID);
        
        /**
         * Gets the "Designation" element
         */
        com.idanalytics.products.common_v1.Designation.Enum getDesignation();
        
        /**
         * Gets (as xml) the "Designation" element
         */
        com.idanalytics.products.common_v1.Designation xgetDesignation();
        
        /**
         * True if has "Designation" element
         */
        boolean isSetDesignation();
        
        /**
         * Sets the "Designation" element
         */
        void setDesignation(com.idanalytics.products.common_v1.Designation.Enum designation);
        
        /**
         * Sets (as xml) the "Designation" element
         */
        void xsetDesignation(com.idanalytics.products.common_v1.Designation designation);
        
        /**
         * Unsets the "Designation" element
         */
        void unsetDesignation();
        
        /**
         * Gets the "IDASequence" element
         */
        java.lang.String getIDASequence();
        
        /**
         * Gets (as xml) the "IDASequence" element
         */
        com.idanalytics.products.common_v1.IDASequence xgetIDASequence();
        
        /**
         * Sets the "IDASequence" element
         */
        void setIDASequence(java.lang.String idaSequence);
        
        /**
         * Sets (as xml) the "IDASequence" element
         */
        void xsetIDASequence(com.idanalytics.products.common_v1.IDASequence idaSequence);
        
        /**
         * Gets the "IDATimeStamp" element
         */
        java.util.Calendar getIDATimeStamp();
        
        /**
         * Gets (as xml) the "IDATimeStamp" element
         */
        com.idanalytics.products.common_v1.IDATimeStamp xgetIDATimeStamp();
        
        /**
         * Sets the "IDATimeStamp" element
         */
        void setIDATimeStamp(java.util.Calendar idaTimeStamp);
        
        /**
         * Sets (as xml) the "IDATimeStamp" element
         */
        void xsetIDATimeStamp(com.idanalytics.products.common_v1.IDATimeStamp idaTimeStamp);
        
        /**
         * Gets the "ReconstructResponse" element
         */
        com.idanalytics.products.resolve360.result.ReconstructResponseType getReconstructResponse();
        
        /**
         * True if has "ReconstructResponse" element
         */
        boolean isSetReconstructResponse();
        
        /**
         * Sets the "ReconstructResponse" element
         */
        void setReconstructResponse(com.idanalytics.products.resolve360.result.ReconstructResponseType reconstructResponse);
        
        /**
         * Appends and returns a new empty "ReconstructResponse" element
         */
        com.idanalytics.products.resolve360.result.ReconstructResponseType addNewReconstructResponse();
        
        /**
         * Unsets the "ReconstructResponse" element
         */
        void unsetReconstructResponse();
        
        /**
         * Gets the "PassThru1" element
         */
        java.lang.String getPassThru1();
        
        /**
         * Gets (as xml) the "PassThru1" element
         */
        com.idanalytics.products.common_v1.PassThru xgetPassThru1();
        
        /**
         * True if has "PassThru1" element
         */
        boolean isSetPassThru1();
        
        /**
         * Sets the "PassThru1" element
         */
        void setPassThru1(java.lang.String passThru1);
        
        /**
         * Sets (as xml) the "PassThru1" element
         */
        void xsetPassThru1(com.idanalytics.products.common_v1.PassThru passThru1);
        
        /**
         * Unsets the "PassThru1" element
         */
        void unsetPassThru1();
        
        /**
         * Gets the "PassThru2" element
         */
        java.lang.String getPassThru2();
        
        /**
         * Gets (as xml) the "PassThru2" element
         */
        com.idanalytics.products.common_v1.PassThru xgetPassThru2();
        
        /**
         * True if has "PassThru2" element
         */
        boolean isSetPassThru2();
        
        /**
         * Sets the "PassThru2" element
         */
        void setPassThru2(java.lang.String passThru2);
        
        /**
         * Sets (as xml) the "PassThru2" element
         */
        void xsetPassThru2(com.idanalytics.products.common_v1.PassThru passThru2);
        
        /**
         * Unsets the "PassThru2" element
         */
        void unsetPassThru2();
        
        /**
         * Gets the "PassThru3" element
         */
        java.lang.String getPassThru3();
        
        /**
         * Gets (as xml) the "PassThru3" element
         */
        com.idanalytics.products.common_v1.PassThru xgetPassThru3();
        
        /**
         * True if has "PassThru3" element
         */
        boolean isSetPassThru3();
        
        /**
         * Sets the "PassThru3" element
         */
        void setPassThru3(java.lang.String passThru3);
        
        /**
         * Sets (as xml) the "PassThru3" element
         */
        void xsetPassThru3(com.idanalytics.products.common_v1.PassThru passThru3);
        
        /**
         * Unsets the "PassThru3" element
         */
        void unsetPassThru3();
        
        /**
         * Gets the "PassThru4" element
         */
        java.lang.String getPassThru4();
        
        /**
         * Gets (as xml) the "PassThru4" element
         */
        com.idanalytics.products.common_v1.PassThru xgetPassThru4();
        
        /**
         * True if has "PassThru4" element
         */
        boolean isSetPassThru4();
        
        /**
         * Sets the "PassThru4" element
         */
        void setPassThru4(java.lang.String passThru4);
        
        /**
         * Sets (as xml) the "PassThru4" element
         */
        void xsetPassThru4(com.idanalytics.products.common_v1.PassThru passThru4);
        
        /**
         * Unsets the "PassThru4" element
         */
        void unsetPassThru4();
        
        /**
         * Gets the "IDAInternal" element
         */
        com.idanalytics.products.resolve360.result.IDAInternalDocument.IDAInternal getIDAInternal();
        
        /**
         * True if has "IDAInternal" element
         */
        boolean isSetIDAInternal();
        
        /**
         * Sets the "IDAInternal" element
         */
        void setIDAInternal(com.idanalytics.products.resolve360.result.IDAInternalDocument.IDAInternal idaInternal);
        
        /**
         * Appends and returns a new empty "IDAInternal" element
         */
        com.idanalytics.products.resolve360.result.IDAInternalDocument.IDAInternal addNewIDAInternal();
        
        /**
         * Unsets the "IDAInternal" element
         */
        void unsetIDAInternal();
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static com.idanalytics.products.resolve360.result.Resolve360ResponseDocument.Resolve360Response newInstance() {
              return (com.idanalytics.products.resolve360.result.Resolve360ResponseDocument.Resolve360Response) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static com.idanalytics.products.resolve360.result.Resolve360ResponseDocument.Resolve360Response newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (com.idanalytics.products.resolve360.result.Resolve360ResponseDocument.Resolve360Response) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.resolve360.result.Resolve360ResponseDocument newInstance() {
          return (com.idanalytics.products.resolve360.result.Resolve360ResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.resolve360.result.Resolve360ResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.resolve360.result.Resolve360ResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.resolve360.result.Resolve360ResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.resolve360.result.Resolve360ResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.resolve360.result.Resolve360ResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.resolve360.result.Resolve360ResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.resolve360.result.Resolve360ResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.resolve360.result.Resolve360ResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.resolve360.result.Resolve360ResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.resolve360.result.Resolve360ResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.resolve360.result.Resolve360ResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.resolve360.result.Resolve360ResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.resolve360.result.Resolve360ResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.resolve360.result.Resolve360ResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.resolve360.result.Resolve360ResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.resolve360.result.Resolve360ResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.resolve360.result.Resolve360ResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.resolve360.result.Resolve360ResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.resolve360.result.Resolve360ResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.resolve360.result.Resolve360ResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.resolve360.result.Resolve360ResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.resolve360.result.Resolve360ResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.resolve360.result.Resolve360ResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.resolve360.result.Resolve360ResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.resolve360.result.Resolve360ResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.resolve360.result.Resolve360ResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.resolve360.result.Resolve360ResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.resolve360.result.Resolve360ResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.resolve360.result.Resolve360ResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.resolve360.result.Resolve360ResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.resolve360.result.Resolve360ResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.resolve360.result.Resolve360ResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.resolve360.result.Resolve360ResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.resolve360.result.Resolve360ResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
