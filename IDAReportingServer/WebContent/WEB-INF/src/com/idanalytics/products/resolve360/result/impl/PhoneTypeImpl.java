/*
 * XML Type:  PhoneType
 * Namespace: http://idanalytics.com/products/resolve360/result
 * Java type: com.idanalytics.products.resolve360.result.PhoneType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.resolve360.result.impl;
/**
 * An XML PhoneType(@http://idanalytics.com/products/resolve360/result).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.resolve360.result.PhoneType.
 */
public class PhoneTypeImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.resolve360.result.PhoneType
{
    private static final long serialVersionUID = 1L;
    
    public PhoneTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected PhoneTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
