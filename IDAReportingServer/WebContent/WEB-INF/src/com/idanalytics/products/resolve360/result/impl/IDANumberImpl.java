/*
 * XML Type:  IDANumber
 * Namespace: http://idanalytics.com/products/resolve360/result
 * Java type: com.idanalytics.products.resolve360.result.IDANumber
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.resolve360.result.impl;
/**
 * An XML IDANumber(@http://idanalytics.com/products/resolve360/result).
 *
 * This is an atomic type that is a restriction of com.idanalytics.products.resolve360.result.IDANumber.
 */
public class IDANumberImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements com.idanalytics.products.resolve360.result.IDANumber
{
    private static final long serialVersionUID = 1L;
    
    public IDANumberImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected IDANumberImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
