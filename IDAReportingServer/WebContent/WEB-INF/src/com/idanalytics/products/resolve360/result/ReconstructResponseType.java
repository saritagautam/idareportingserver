/*
 * XML Type:  ReconstructResponse_type
 * Namespace: http://idanalytics.com/products/resolve360/result
 * Java type: com.idanalytics.products.resolve360.result.ReconstructResponseType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.products.resolve360.result;


/**
 * An XML ReconstructResponse_type(@http://idanalytics.com/products/resolve360/result).
 *
 * This is a complex type.
 */
public interface ReconstructResponseType extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ReconstructResponseType.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("reconstructresponsetype1d80type");
    
    /**
     * Gets the "IDANumber" element
     */
    java.lang.String getIDANumber();
    
    /**
     * Gets (as xml) the "IDANumber" element
     */
    com.idanalytics.products.resolve360.result.IDANumber xgetIDANumber();
    
    /**
     * True if has "IDANumber" element
     */
    boolean isSetIDANumber();
    
    /**
     * Sets the "IDANumber" element
     */
    void setIDANumber(java.lang.String idaNumber);
    
    /**
     * Sets (as xml) the "IDANumber" element
     */
    void xsetIDANumber(com.idanalytics.products.resolve360.result.IDANumber idaNumber);
    
    /**
     * Unsets the "IDANumber" element
     */
    void unsetIDANumber();
    
    /**
     * Gets the "IDANumberResultCode" element
     */
    java.lang.String getIDANumberResultCode();
    
    /**
     * Gets (as xml) the "IDANumberResultCode" element
     */
    com.idanalytics.products.common_v1.ReasonCode xgetIDANumberResultCode();
    
    /**
     * True if has "IDANumberResultCode" element
     */
    boolean isSetIDANumberResultCode();
    
    /**
     * Sets the "IDANumberResultCode" element
     */
    void setIDANumberResultCode(java.lang.String idaNumberResultCode);
    
    /**
     * Sets (as xml) the "IDANumberResultCode" element
     */
    void xsetIDANumberResultCode(com.idanalytics.products.common_v1.ReasonCode idaNumberResultCode);
    
    /**
     * Unsets the "IDANumberResultCode" element
     */
    void unsetIDANumberResultCode();
    
    /**
     * Gets the "SSN5" element
     */
    java.lang.String getSSN5();
    
    /**
     * Gets (as xml) the "SSN5" element
     */
    com.idanalytics.products.common_v1.SSN5 xgetSSN5();
    
    /**
     * True if has "SSN5" element
     */
    boolean isSetSSN5();
    
    /**
     * Sets the "SSN5" element
     */
    void setSSN5(java.lang.String ssn5);
    
    /**
     * Sets (as xml) the "SSN5" element
     */
    void xsetSSN5(com.idanalytics.products.common_v1.SSN5 ssn5);
    
    /**
     * Unsets the "SSN5" element
     */
    void unsetSSN5();
    
    /**
     * Gets the "SSN9" element
     */
    java.lang.String getSSN9();
    
    /**
     * Gets (as xml) the "SSN9" element
     */
    com.idanalytics.products.common_v1.SSNType xgetSSN9();
    
    /**
     * True if has "SSN9" element
     */
    boolean isSetSSN9();
    
    /**
     * Sets the "SSN9" element
     */
    void setSSN9(java.lang.String ssn9);
    
    /**
     * Sets (as xml) the "SSN9" element
     */
    void xsetSSN9(com.idanalytics.products.common_v1.SSNType ssn9);
    
    /**
     * Unsets the "SSN9" element
     */
    void unsetSSN9();
    
    /**
     * Gets the "SSN5ResultCode" element
     */
    java.lang.String getSSN5ResultCode();
    
    /**
     * Gets (as xml) the "SSN5ResultCode" element
     */
    com.idanalytics.products.common_v1.ReasonCode xgetSSN5ResultCode();
    
    /**
     * True if has "SSN5ResultCode" element
     */
    boolean isSetSSN5ResultCode();
    
    /**
     * Sets the "SSN5ResultCode" element
     */
    void setSSN5ResultCode(java.lang.String ssn5ResultCode);
    
    /**
     * Sets (as xml) the "SSN5ResultCode" element
     */
    void xsetSSN5ResultCode(com.idanalytics.products.common_v1.ReasonCode ssn5ResultCode);
    
    /**
     * Unsets the "SSN5ResultCode" element
     */
    void unsetSSN5ResultCode();
    
    /**
     * Gets the "SSN9ResultCode" element
     */
    java.lang.String getSSN9ResultCode();
    
    /**
     * Gets (as xml) the "SSN9ResultCode" element
     */
    com.idanalytics.products.common_v1.ReasonCode xgetSSN9ResultCode();
    
    /**
     * True if has "SSN9ResultCode" element
     */
    boolean isSetSSN9ResultCode();
    
    /**
     * Sets the "SSN9ResultCode" element
     */
    void setSSN9ResultCode(java.lang.String ssn9ResultCode);
    
    /**
     * Sets (as xml) the "SSN9ResultCode" element
     */
    void xsetSSN9ResultCode(com.idanalytics.products.common_v1.ReasonCode ssn9ResultCode);
    
    /**
     * Unsets the "SSN9ResultCode" element
     */
    void unsetSSN9ResultCode();
    
    /**
     * Gets the "Address" element
     */
    java.lang.String getAddress();
    
    /**
     * Gets (as xml) the "Address" element
     */
    com.idanalytics.products.common_v1.AddressType xgetAddress();
    
    /**
     * True if has "Address" element
     */
    boolean isSetAddress();
    
    /**
     * Sets the "Address" element
     */
    void setAddress(java.lang.String address);
    
    /**
     * Sets (as xml) the "Address" element
     */
    void xsetAddress(com.idanalytics.products.common_v1.AddressType address);
    
    /**
     * Unsets the "Address" element
     */
    void unsetAddress();
    
    /**
     * Gets the "Zip" element
     */
    java.lang.String getZip();
    
    /**
     * Gets (as xml) the "Zip" element
     */
    com.idanalytics.products.common_v1.ZipType xgetZip();
    
    /**
     * True if has "Zip" element
     */
    boolean isSetZip();
    
    /**
     * Sets the "Zip" element
     */
    void setZip(java.lang.String zip);
    
    /**
     * Sets (as xml) the "Zip" element
     */
    void xsetZip(com.idanalytics.products.common_v1.ZipType zip);
    
    /**
     * Unsets the "Zip" element
     */
    void unsetZip();
    
    /**
     * Gets the "City" element
     */
    java.lang.String getCity();
    
    /**
     * Gets (as xml) the "City" element
     */
    com.idanalytics.products.common_v1.CityType xgetCity();
    
    /**
     * True if has "City" element
     */
    boolean isSetCity();
    
    /**
     * Sets the "City" element
     */
    void setCity(java.lang.String city);
    
    /**
     * Sets (as xml) the "City" element
     */
    void xsetCity(com.idanalytics.products.common_v1.CityType city);
    
    /**
     * Unsets the "City" element
     */
    void unsetCity();
    
    /**
     * Gets the "State" element
     */
    java.lang.String getState();
    
    /**
     * Gets (as xml) the "State" element
     */
    com.idanalytics.products.common_v1.StateType xgetState();
    
    /**
     * True if has "State" element
     */
    boolean isSetState();
    
    /**
     * Sets the "State" element
     */
    void setState(java.lang.String state);
    
    /**
     * Sets (as xml) the "State" element
     */
    void xsetState(com.idanalytics.products.common_v1.StateType state);
    
    /**
     * Unsets the "State" element
     */
    void unsetState();
    
    /**
     * Gets the "AddressResultCode" element
     */
    java.lang.String getAddressResultCode();
    
    /**
     * Gets (as xml) the "AddressResultCode" element
     */
    com.idanalytics.products.common_v1.ReasonCode xgetAddressResultCode();
    
    /**
     * True if has "AddressResultCode" element
     */
    boolean isSetAddressResultCode();
    
    /**
     * Sets the "AddressResultCode" element
     */
    void setAddressResultCode(java.lang.String addressResultCode);
    
    /**
     * Sets (as xml) the "AddressResultCode" element
     */
    void xsetAddressResultCode(com.idanalytics.products.common_v1.ReasonCode addressResultCode);
    
    /**
     * Unsets the "AddressResultCode" element
     */
    void unsetAddressResultCode();
    
    /**
     * Gets the "DOB" element
     */
    java.lang.String getDOB();
    
    /**
     * Gets (as xml) the "DOB" element
     */
    com.idanalytics.products.resolve360.result.DOBType xgetDOB();
    
    /**
     * True if has "DOB" element
     */
    boolean isSetDOB();
    
    /**
     * Sets the "DOB" element
     */
    void setDOB(java.lang.String dob);
    
    /**
     * Sets (as xml) the "DOB" element
     */
    void xsetDOB(com.idanalytics.products.resolve360.result.DOBType dob);
    
    /**
     * Unsets the "DOB" element
     */
    void unsetDOB();
    
    /**
     * Gets the "DOBResultCode" element
     */
    java.lang.String getDOBResultCode();
    
    /**
     * Gets (as xml) the "DOBResultCode" element
     */
    com.idanalytics.products.common_v1.ReasonCode xgetDOBResultCode();
    
    /**
     * True if has "DOBResultCode" element
     */
    boolean isSetDOBResultCode();
    
    /**
     * Sets the "DOBResultCode" element
     */
    void setDOBResultCode(java.lang.String dobResultCode);
    
    /**
     * Sets (as xml) the "DOBResultCode" element
     */
    void xsetDOBResultCode(com.idanalytics.products.common_v1.ReasonCode dobResultCode);
    
    /**
     * Unsets the "DOBResultCode" element
     */
    void unsetDOBResultCode();
    
    /**
     * Gets the "Phone" element
     */
    java.lang.String getPhone();
    
    /**
     * Gets (as xml) the "Phone" element
     */
    com.idanalytics.products.resolve360.result.PhoneType xgetPhone();
    
    /**
     * True if has "Phone" element
     */
    boolean isSetPhone();
    
    /**
     * Sets the "Phone" element
     */
    void setPhone(java.lang.String phone);
    
    /**
     * Sets (as xml) the "Phone" element
     */
    void xsetPhone(com.idanalytics.products.resolve360.result.PhoneType phone);
    
    /**
     * Unsets the "Phone" element
     */
    void unsetPhone();
    
    /**
     * Gets the "PhoneResultCode" element
     */
    java.lang.String getPhoneResultCode();
    
    /**
     * Gets (as xml) the "PhoneResultCode" element
     */
    com.idanalytics.products.common_v1.ReasonCode xgetPhoneResultCode();
    
    /**
     * True if has "PhoneResultCode" element
     */
    boolean isSetPhoneResultCode();
    
    /**
     * Sets the "PhoneResultCode" element
     */
    void setPhoneResultCode(java.lang.String phoneResultCode);
    
    /**
     * Sets (as xml) the "PhoneResultCode" element
     */
    void xsetPhoneResultCode(com.idanalytics.products.common_v1.ReasonCode phoneResultCode);
    
    /**
     * Unsets the "PhoneResultCode" element
     */
    void unsetPhoneResultCode();
    
    /**
     * Gets the "Email" element
     */
    java.lang.String getEmail();
    
    /**
     * Gets (as xml) the "Email" element
     */
    com.idanalytics.products.resolve360.result.EmailType xgetEmail();
    
    /**
     * True if has "Email" element
     */
    boolean isSetEmail();
    
    /**
     * Sets the "Email" element
     */
    void setEmail(java.lang.String email);
    
    /**
     * Sets (as xml) the "Email" element
     */
    void xsetEmail(com.idanalytics.products.resolve360.result.EmailType email);
    
    /**
     * Unsets the "Email" element
     */
    void unsetEmail();
    
    /**
     * Gets the "EmailResultCode" element
     */
    java.lang.String getEmailResultCode();
    
    /**
     * Gets (as xml) the "EmailResultCode" element
     */
    com.idanalytics.products.common_v1.ReasonCode xgetEmailResultCode();
    
    /**
     * True if has "EmailResultCode" element
     */
    boolean isSetEmailResultCode();
    
    /**
     * Sets the "EmailResultCode" element
     */
    void setEmailResultCode(java.lang.String emailResultCode);
    
    /**
     * Sets (as xml) the "EmailResultCode" element
     */
    void xsetEmailResultCode(com.idanalytics.products.common_v1.ReasonCode emailResultCode);
    
    /**
     * Unsets the "EmailResultCode" element
     */
    void unsetEmailResultCode();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.products.resolve360.result.ReconstructResponseType newInstance() {
          return (com.idanalytics.products.resolve360.result.ReconstructResponseType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.products.resolve360.result.ReconstructResponseType newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.products.resolve360.result.ReconstructResponseType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.products.resolve360.result.ReconstructResponseType parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.resolve360.result.ReconstructResponseType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.products.resolve360.result.ReconstructResponseType parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.resolve360.result.ReconstructResponseType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.products.resolve360.result.ReconstructResponseType parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.resolve360.result.ReconstructResponseType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.products.resolve360.result.ReconstructResponseType parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.resolve360.result.ReconstructResponseType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.products.resolve360.result.ReconstructResponseType parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.resolve360.result.ReconstructResponseType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.products.resolve360.result.ReconstructResponseType parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.resolve360.result.ReconstructResponseType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.products.resolve360.result.ReconstructResponseType parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.resolve360.result.ReconstructResponseType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.products.resolve360.result.ReconstructResponseType parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.resolve360.result.ReconstructResponseType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.products.resolve360.result.ReconstructResponseType parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.resolve360.result.ReconstructResponseType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.products.resolve360.result.ReconstructResponseType parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.products.resolve360.result.ReconstructResponseType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.products.resolve360.result.ReconstructResponseType parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.resolve360.result.ReconstructResponseType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.products.resolve360.result.ReconstructResponseType parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.resolve360.result.ReconstructResponseType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.products.resolve360.result.ReconstructResponseType parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.resolve360.result.ReconstructResponseType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.products.resolve360.result.ReconstructResponseType parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.products.resolve360.result.ReconstructResponseType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.resolve360.result.ReconstructResponseType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.resolve360.result.ReconstructResponseType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.products.resolve360.result.ReconstructResponseType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.products.resolve360.result.ReconstructResponseType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
