/*
 * XML Type:  ErrorType
 * Namespace: http://idanalytics.com/error/api
 * Java type: com.idanalytics.error.api.ErrorType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.error.api.impl;
/**
 * An XML ErrorType(@http://idanalytics.com/error/api).
 *
 * This is a complex type.
 */
public class ErrorTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.error.api.ErrorType
{
    private static final long serialVersionUID = 1L;
    
    public ErrorTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CODE$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/error/api", "Code");
    private static final javax.xml.namespace.QName MESSAGE$2 = 
        new javax.xml.namespace.QName("http://idanalytics.com/error/api", "Message");
    
    
    /**
     * Gets array of all "Code" elements
     */
    public com.idanalytics.error.api.ErrorCode.Enum[] getCodeArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(CODE$0, targetList);
            com.idanalytics.error.api.ErrorCode.Enum[] result = new com.idanalytics.error.api.ErrorCode.Enum[targetList.size()];
            for (int i = 0, len = targetList.size() ; i < len ; i++)
                result[i] = (com.idanalytics.error.api.ErrorCode.Enum)((org.apache.xmlbeans.SimpleValue)targetList.get(i)).getEnumValue();
            return result;
        }
    }
    
    /**
     * Gets ith "Code" element
     */
    public com.idanalytics.error.api.ErrorCode.Enum getCodeArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CODE$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return (com.idanalytics.error.api.ErrorCode.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) array of all "Code" elements
     */
    public com.idanalytics.error.api.ErrorCode[] xgetCodeArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(CODE$0, targetList);
            com.idanalytics.error.api.ErrorCode[] result = new com.idanalytics.error.api.ErrorCode[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets (as xml) ith "Code" element
     */
    public com.idanalytics.error.api.ErrorCode xgetCodeArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.error.api.ErrorCode target = null;
            target = (com.idanalytics.error.api.ErrorCode)get_store().find_element_user(CODE$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "Code" element
     */
    public int sizeOfCodeArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CODE$0);
        }
    }
    
    /**
     * Sets array of all "Code" element
     */
    public void setCodeArray(com.idanalytics.error.api.ErrorCode.Enum[] codeArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(codeArray, CODE$0);
        }
    }
    
    /**
     * Sets ith "Code" element
     */
    public void setCodeArray(int i, com.idanalytics.error.api.ErrorCode.Enum code)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(CODE$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.setEnumValue(code);
        }
    }
    
    /**
     * Sets (as xml) array of all "Code" element
     */
    public void xsetCodeArray(com.idanalytics.error.api.ErrorCode[]codeArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(codeArray, CODE$0);
        }
    }
    
    /**
     * Sets (as xml) ith "Code" element
     */
    public void xsetCodeArray(int i, com.idanalytics.error.api.ErrorCode code)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.error.api.ErrorCode target = null;
            target = (com.idanalytics.error.api.ErrorCode)get_store().find_element_user(CODE$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(code);
        }
    }
    
    /**
     * Inserts the value as the ith "Code" element
     */
    public void insertCode(int i, com.idanalytics.error.api.ErrorCode.Enum code)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = 
                (org.apache.xmlbeans.SimpleValue)get_store().insert_element_user(CODE$0, i);
            target.setEnumValue(code);
        }
    }
    
    /**
     * Appends the value as the last "Code" element
     */
    public void addCode(com.idanalytics.error.api.ErrorCode.Enum code)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(CODE$0);
            target.setEnumValue(code);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "Code" element
     */
    public com.idanalytics.error.api.ErrorCode insertNewCode(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.error.api.ErrorCode target = null;
            target = (com.idanalytics.error.api.ErrorCode)get_store().insert_element_user(CODE$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "Code" element
     */
    public com.idanalytics.error.api.ErrorCode addNewCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.error.api.ErrorCode target = null;
            target = (com.idanalytics.error.api.ErrorCode)get_store().add_element_user(CODE$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "Code" element
     */
    public void removeCode(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CODE$0, i);
        }
    }
    
    /**
     * Gets array of all "Message" elements
     */
    public java.lang.String[] getMessageArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(MESSAGE$2, targetList);
            java.lang.String[] result = new java.lang.String[targetList.size()];
            for (int i = 0, len = targetList.size() ; i < len ; i++)
                result[i] = ((org.apache.xmlbeans.SimpleValue)targetList.get(i)).getStringValue();
            return result;
        }
    }
    
    /**
     * Gets ith "Message" element
     */
    public java.lang.String getMessageArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MESSAGE$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) array of all "Message" elements
     */
    public org.apache.xmlbeans.XmlString[] xgetMessageArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(MESSAGE$2, targetList);
            org.apache.xmlbeans.XmlString[] result = new org.apache.xmlbeans.XmlString[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets (as xml) ith "Message" element
     */
    public org.apache.xmlbeans.XmlString xgetMessageArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(MESSAGE$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "Message" element
     */
    public int sizeOfMessageArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(MESSAGE$2);
        }
    }
    
    /**
     * Sets array of all "Message" element
     */
    public void setMessageArray(java.lang.String[] messageArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(messageArray, MESSAGE$2);
        }
    }
    
    /**
     * Sets ith "Message" element
     */
    public void setMessageArray(int i, java.lang.String message)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MESSAGE$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.setStringValue(message);
        }
    }
    
    /**
     * Sets (as xml) array of all "Message" element
     */
    public void xsetMessageArray(org.apache.xmlbeans.XmlString[]messageArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(messageArray, MESSAGE$2);
        }
    }
    
    /**
     * Sets (as xml) ith "Message" element
     */
    public void xsetMessageArray(int i, org.apache.xmlbeans.XmlString message)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(MESSAGE$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(message);
        }
    }
    
    /**
     * Inserts the value as the ith "Message" element
     */
    public void insertMessage(int i, java.lang.String message)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = 
                (org.apache.xmlbeans.SimpleValue)get_store().insert_element_user(MESSAGE$2, i);
            target.setStringValue(message);
        }
    }
    
    /**
     * Appends the value as the last "Message" element
     */
    public void addMessage(java.lang.String message)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(MESSAGE$2);
            target.setStringValue(message);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "Message" element
     */
    public org.apache.xmlbeans.XmlString insertNewMessage(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().insert_element_user(MESSAGE$2, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "Message" element
     */
    public org.apache.xmlbeans.XmlString addNewMessage()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(MESSAGE$2);
            return target;
        }
    }
    
    /**
     * Removes the ith "Message" element
     */
    public void removeMessage(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(MESSAGE$2, i);
        }
    }
}
