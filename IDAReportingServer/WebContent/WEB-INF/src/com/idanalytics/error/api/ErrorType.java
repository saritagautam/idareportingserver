/*
 * XML Type:  ErrorType
 * Namespace: http://idanalytics.com/error/api
 * Java type: com.idanalytics.error.api.ErrorType
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.error.api;


/**
 * An XML ErrorType(@http://idanalytics.com/error/api).
 *
 * This is a complex type.
 */
public interface ErrorType extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ErrorType.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s5651DC4401F467E84A2F49B47D833C2C").resolveHandle("errortype7b37type");
    
    /**
     * Gets array of all "Code" elements
     */
    com.idanalytics.error.api.ErrorCode.Enum[] getCodeArray();
    
    /**
     * Gets ith "Code" element
     */
    com.idanalytics.error.api.ErrorCode.Enum getCodeArray(int i);
    
    /**
     * Gets (as xml) array of all "Code" elements
     */
    com.idanalytics.error.api.ErrorCode[] xgetCodeArray();
    
    /**
     * Gets (as xml) ith "Code" element
     */
    com.idanalytics.error.api.ErrorCode xgetCodeArray(int i);
    
    /**
     * Returns number of "Code" element
     */
    int sizeOfCodeArray();
    
    /**
     * Sets array of all "Code" element
     */
    void setCodeArray(com.idanalytics.error.api.ErrorCode.Enum[] codeArray);
    
    /**
     * Sets ith "Code" element
     */
    void setCodeArray(int i, com.idanalytics.error.api.ErrorCode.Enum code);
    
    /**
     * Sets (as xml) array of all "Code" element
     */
    void xsetCodeArray(com.idanalytics.error.api.ErrorCode[] codeArray);
    
    /**
     * Sets (as xml) ith "Code" element
     */
    void xsetCodeArray(int i, com.idanalytics.error.api.ErrorCode code);
    
    /**
     * Inserts the value as the ith "Code" element
     */
    void insertCode(int i, com.idanalytics.error.api.ErrorCode.Enum code);
    
    /**
     * Appends the value as the last "Code" element
     */
    void addCode(com.idanalytics.error.api.ErrorCode.Enum code);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "Code" element
     */
    com.idanalytics.error.api.ErrorCode insertNewCode(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "Code" element
     */
    com.idanalytics.error.api.ErrorCode addNewCode();
    
    /**
     * Removes the ith "Code" element
     */
    void removeCode(int i);
    
    /**
     * Gets array of all "Message" elements
     */
    java.lang.String[] getMessageArray();
    
    /**
     * Gets ith "Message" element
     */
    java.lang.String getMessageArray(int i);
    
    /**
     * Gets (as xml) array of all "Message" elements
     */
    org.apache.xmlbeans.XmlString[] xgetMessageArray();
    
    /**
     * Gets (as xml) ith "Message" element
     */
    org.apache.xmlbeans.XmlString xgetMessageArray(int i);
    
    /**
     * Returns number of "Message" element
     */
    int sizeOfMessageArray();
    
    /**
     * Sets array of all "Message" element
     */
    void setMessageArray(java.lang.String[] messageArray);
    
    /**
     * Sets ith "Message" element
     */
    void setMessageArray(int i, java.lang.String message);
    
    /**
     * Sets (as xml) array of all "Message" element
     */
    void xsetMessageArray(org.apache.xmlbeans.XmlString[] messageArray);
    
    /**
     * Sets (as xml) ith "Message" element
     */
    void xsetMessageArray(int i, org.apache.xmlbeans.XmlString message);
    
    /**
     * Inserts the value as the ith "Message" element
     */
    void insertMessage(int i, java.lang.String message);
    
    /**
     * Appends the value as the last "Message" element
     */
    void addMessage(java.lang.String message);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "Message" element
     */
    org.apache.xmlbeans.XmlString insertNewMessage(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "Message" element
     */
    org.apache.xmlbeans.XmlString addNewMessage();
    
    /**
     * Removes the ith "Message" element
     */
    void removeMessage(int i);
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static com.idanalytics.error.api.ErrorType newInstance() {
          return (com.idanalytics.error.api.ErrorType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static com.idanalytics.error.api.ErrorType newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (com.idanalytics.error.api.ErrorType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static com.idanalytics.error.api.ErrorType parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.error.api.ErrorType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static com.idanalytics.error.api.ErrorType parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.error.api.ErrorType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static com.idanalytics.error.api.ErrorType parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.error.api.ErrorType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static com.idanalytics.error.api.ErrorType parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.error.api.ErrorType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static com.idanalytics.error.api.ErrorType parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.error.api.ErrorType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static com.idanalytics.error.api.ErrorType parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.error.api.ErrorType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static com.idanalytics.error.api.ErrorType parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.error.api.ErrorType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static com.idanalytics.error.api.ErrorType parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.error.api.ErrorType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static com.idanalytics.error.api.ErrorType parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.error.api.ErrorType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static com.idanalytics.error.api.ErrorType parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (com.idanalytics.error.api.ErrorType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static com.idanalytics.error.api.ErrorType parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.error.api.ErrorType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static com.idanalytics.error.api.ErrorType parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.error.api.ErrorType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static com.idanalytics.error.api.ErrorType parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.error.api.ErrorType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static com.idanalytics.error.api.ErrorType parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (com.idanalytics.error.api.ErrorType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.error.api.ErrorType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.error.api.ErrorType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static com.idanalytics.error.api.ErrorType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (com.idanalytics.error.api.ErrorType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
