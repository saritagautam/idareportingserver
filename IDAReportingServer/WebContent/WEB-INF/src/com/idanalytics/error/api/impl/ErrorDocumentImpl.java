/*
 * An XML document type.
 * Localname: Error
 * Namespace: http://idanalytics.com/error/api
 * Java type: com.idanalytics.error.api.ErrorDocument
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.error.api.impl;
/**
 * A document containing one Error(@http://idanalytics.com/error/api) element.
 *
 * This is a complex type.
 */
public class ErrorDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements com.idanalytics.error.api.ErrorDocument
{
    private static final long serialVersionUID = 1L;
    
    public ErrorDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ERROR$0 = 
        new javax.xml.namespace.QName("http://idanalytics.com/error/api", "Error");
    
    
    /**
     * Gets the "Error" element
     */
    public com.idanalytics.error.api.ErrorType getError()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.error.api.ErrorType target = null;
            target = (com.idanalytics.error.api.ErrorType)get_store().find_element_user(ERROR$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "Error" element
     */
    public void setError(com.idanalytics.error.api.ErrorType error)
    {
        generatedSetterHelperImpl(error, ERROR$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
    }
    
    /**
     * Appends and returns a new empty "Error" element
     */
    public com.idanalytics.error.api.ErrorType addNewError()
    {
        synchronized (monitor())
        {
            check_orphaned();
            com.idanalytics.error.api.ErrorType target = null;
            target = (com.idanalytics.error.api.ErrorType)get_store().add_element_user(ERROR$0);
            return target;
        }
    }
}
