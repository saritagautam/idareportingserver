/*
 * XML Type:  ErrorCode
 * Namespace: http://idanalytics.com/error/api
 * Java type: com.idanalytics.error.api.ErrorCode
 *
 * Automatically generated - do not modify.
 */
package com.idanalytics.error.api.impl;
/**
 * An XML ErrorCode(@http://idanalytics.com/error/api).
 *
 * This is an atomic type that is a restriction of com.idanalytics.error.api.ErrorCode.
 */
public class ErrorCodeImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements com.idanalytics.error.api.ErrorCode
{
    private static final long serialVersionUID = 1L;
    
    public ErrorCodeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected ErrorCodeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
