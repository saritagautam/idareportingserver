import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;

import org.apache.axis2.AxisFault;
import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlOptions;

import com.idanalytics.core.api.BodyType;
import com.idanalytics.core.api.CommandDocument;
import com.idanalytics.core.api.CommandType;
import com.idanalytics.core.api.CredentialsType;
import com.idanalytics.core.api.ItemType;
import com.idanalytics.core.api.ResponseDocument;
import com.idanalytics.products.idscore.request.Address;
import com.idanalytics.products.idscore.request.Application;
import com.idanalytics.products.idscore.request.DesignationDocument;
import com.idanalytics.products.idscore.request.Employment;
import com.idanalytics.products.idscore.request.EnrollmentRequestDocument;
import com.idanalytics.products.idscore.request.IDScoreRequestDocument;
import com.idanalytics.products.idscore.request.Identity;
import com.idanalytics.products.idscore.request.Origination;
import com.idanalytics.products.idscore.request.ParsedAddress;
import com.idanalytics.products.idscore.request.ConsumerHeaderDocument.ConsumerHeader;
import com.idanalytics.products.idscore.request.EnrollmentRequestDocument.EnrollmentRequest;
import com.idanalytics.products.idscore.request.IDScoreRequestDocument.IDScoreRequest;
import com.idanalytics.products.idscore.result.GroupDocument.Group;
import com.idanalytics.products.idscore.result.IndicatorDocument.Indicator;
import com.idanalytics.products.idscore.result.IndicatorsDocument.Indicators;
import com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord;
import com.idanalytics.products.myidmonitoring.AlertEvent;
import com.idanalytics.products.myidmonitoring.AlertRequestDocument;
import com.idanalytics.products.myidmonitoring.NotificationRequestDocument;
import com.idanalytics.products.myidmonitoring.AlertRequestDocument.AlertRequest;
import com.idanalytics.products.myidmonitoring.AlertsDocument.Alerts;
import com.idanalytics.products.myidmonitoring.EnrollmentStateDocument.EnrollmentState;
import com.idanalytics.products.myidmonitoring.NotificationRequestDocument.NotificationRequest;
import com.idanalytics.products.myidmonitoring.NotificationsDocument.Notifications;
import com.idanalytics.webservice.definitions.IDSPServiceStub;
public class TestAxisClient {
    private static final String USAGE = "To execute a command from a file: TestAxisClient URL fileName \n" +
    		"To run a demo of several request types: TestAxisClient URL clientID userName password";
    private static final String empty="";
    private String clientID;
    private String userID;
    private String password;
    
    /*
	 * This method demonstrates reading a file that already
	 * contains the entire CommandDocument construct in XML format.
	 */
    private CommandDocument constructCommandFromFile(String fileName) throws Exception {
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        InputStream xmlStream = new FileInputStream(fileName);
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(xmlStream);

        return CommandDocument.Factory.parse(reader,
            new XmlOptions().setLoadLineNumbers());
    }

    public static void main(String[] args) {
        System.out.println("TestAxisClient...");
        /*args[0] = "https://uat2.idanalytics.com/webservice/services/IDSPService";
        args[1] = "LendingPoint";
        args[2] = "LendingPoint/T1";
        args[3] = "vSApx*!4";*/
       /* if (args.length != 2 && args.length != 4) {
            System.out.println(USAGE);
            System.exit(-1);
        }*/
    
        TestAxisClient tac = new TestAxisClient();
        IDSPServiceStub stub = null;
        
		try {
			stub = new IDSPServiceStub("https://uat2.idanalytics.com/webservice/services/IDSPService" );
		} catch (AxisFault e1) {
			e1.printStackTrace();
		}
        
        if (true){
        	System.out.println("Running Demo...");
        	tac.clientID = "LendingPoint";
        	tac.userID = "LendingPoint/T1";
        	tac.password = "vSApx*!4";
        	tac.runDemo(stub);
        }
        else{
        	System.out.println("Executing command from input file...");
	        try {	                        
	            CommandDocument cd = tac.constructCommandFromFile("LendingPoint" );
	            System.out.println("Command: " + cd.toString());
	            System.out.println("Calling stub...");
	            ResponseDocument rd = stub.executeCommand(cd);
	                    	
	    	    // Get the item type array (depending on the request type there may be multiple responses)
	    	    ItemType[] items = rd.getResponse().getBody().getItemArray();
	                
	    	    System.out.println("Printing response keys...");
	            for (int i = 0; i < items.length; i++){
	            	ItemType item = items[i];
	            	System.out.println(" " + item.getKey());	            	
	            }
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
        }
    }
    
    private void runDemo(IDSPServiceStub stub ){
    	/**
         * To demonstrate various requests and responses, we'll create an array of request types.   
         * We'll then iterate through these types, construct the appropriate command, execute the solution, 
         * and finally, print out some response values for each request.  
         */
        String[] requestsToExecute = { "idScoreRiskRequest", "enrollment", "alertRequest", "notificationRequest"};
    	        
        try {
	        for (String requestKey : requestsToExecute){
	        	System.out.println("\nExecuting " + requestKey);
	        	CommandDocument commandDoc = buildCommand(requestKey);
	        	ResponseDocument responseDoc = stub.executeCommand(commandDoc);	        	
	        	printResults(responseDoc);	        	
	        }
        }
        catch (RemoteException e) {
			e.printStackTrace();
		}       
    }

    /**
     * This method demonstrates the creation of a CommandDocument object and the populating of its 
     * associated elements based on the request type. To help you draw an association 
     * from XML to code, you may find it helpful to cross-reference the sample SOAP requests 
     * that are packaged with the Developer's Kit.
     **/    	
    private CommandDocument buildCommand(String key){
    	BigDecimal threePointO = java.math.BigDecimal.valueOf(3.0);
    		
    	// all solutions require a command object which encapsulates the request.
    	CommandDocument commandDoc = (CommandDocument) CommandDocument.Factory.newInstance();
    	CommandType command = commandDoc.addNewCommand();
    	
    	command.setClient(clientID);
    	
	    CredentialsType creds = command.addNewCredentials();
	    creds.setUsername(userID);
	    creds.setPassword(password);
	    command.setCredentials(creds);
	    
	    // setup the BodyType and ItemType
        // refer to the schemas & Developer's Guide for request types for each solution                
	    BodyType body = command.addNewBody();	    
	    ItemType item = body.addNewItem();
	    
	    if (key.equals("idScoreRiskRequest")){
	    	command.setSolution("Standard/IDScoreRisk");
	    	IDScoreRequestDocument scoreRequestDoc = IDScoreRequestDocument.Factory.newInstance();	    	
	    	IDScoreRequest idScoreRequest = scoreRequestDoc.addNewIDScoreRequest();	    	
	    	idScoreRequest.setSchemaVersion(threePointO);
	    	
	    	Origination origination = idScoreRequest.addNewOrigination();
	    	buildOrigination(origination);
	    
	    	Identity identity = idScoreRequest.addNewIdentity();
	    	buildIdentity(identity);
	    	
	    	Application app = idScoreRequest.addNewApplication();
	    	buildApplication(app);
	    	item.set(scoreRequestDoc);	    	
	    }
	    else if (key.equals("enrollment")){
	    	command.setSolution("Standard/MyIDMonitoringAndAlerts");
	    	EnrollmentRequestDocument enrollmentRequestDoc = EnrollmentRequestDocument.Factory.newInstance();
	    	EnrollmentRequest enrollmentRequest = enrollmentRequestDoc.addNewEnrollmentRequest();
	    	enrollmentRequest.setSchemaVersion(threePointO);
	    	ConsumerHeader consumerHeader = enrollmentRequest.addNewConsumerHeader();
	    	consumerHeader.setRequestType("ADD");
	    	consumerHeader.setConsumerID("consumer123");
	    	consumerHeader.setIPAddress("127.0.0.1");
	    	consumerHeader.setPassThru1("Setting passthru 1 ");
	    	Identity identity = enrollmentRequest.addNewIdentity();
	    	buildIdentity(identity);
	    	item.set(enrollmentRequestDoc);
	    }
	    else if (key.equals("alertRequest")){
	    	command.setSolution("Standard/MyIDMonitoringAndAlerts");
	    	AlertRequestDocument alertRequestDoc = AlertRequestDocument.Factory.newInstance();
	    	AlertRequest alertRequest = alertRequestDoc.addNewAlertRequest();
	    	item.set(alertRequestDoc);
	    }
	    else if (key.equals("notificationRequest")){
	    	command.setSolution("Standard/MyIDMonitoringAndAlerts");
	    	NotificationRequestDocument notificationRequestDoc = NotificationRequestDocument.Factory.newInstance();
	    	NotificationRequest notificationRequest = notificationRequestDoc.addNewNotificationRequest();
	    	item.set(notificationRequestDoc);
	    }
	    
	    item.setKey(key);	    
	    return commandDoc;
    }
   
    /**
     * This method demonstrates the creation of an Application object.
     **/
    private Application buildApplication(Application app){
    	app.setChannel(empty);
        app.setAcquisitionMethod(empty);
        app.setAgentLoc(empty);
        app.setSourceIP(empty);
        app.setPrimaryDecisionCode(empty);
        app.setSecondaryDecisionCode(empty);
        app.setPrimaryPortfolio(empty);
        app.setSecondaryPortfolio(empty);
        app.setSecondaryFraudCode(empty);
        app.setPrimaryIncome(empty);
        app.setInferredIncome(empty);
        app.setRecommendedCreditLine(empty);
        return app;
    }
    
    /**
     * This method demonstrates the creation and assembly of an Identity object.  
     * Refer to the schema for information on required values.             
     **/
    private Identity buildIdentity(Identity identity){    	
    	identity.setSSN("123456789");
        identity.setTitle("Mr");
        identity.setFirstName("Frederick");
        identity.setMiddleName("Joe");
        identity.setLastName("Smith");
        identity.setSuffix("Sr");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
        identity.setDOB(df.format(System.currentTimeMillis()));
        Address addr = identity.addNewAddress();
        addr.setLine1("One Programmer Way");        
        addr.setLine2("Software Ave");
        identity.setCity("San Diego");
        identity.setState("CA");
        identity.setZip("92021");        
        identity.setHomePhone("6195551212");
        identity.setMobilePhone(empty);
        identity.setWorkPhone(empty);
        identity.setEmail(empty);
        identity.setIDType(empty);
        identity.setIDOrigin(empty);
        identity.setIDNumber(empty);        
        Employment emp = identity.addNewEmployment();
        emp.setEmployerType(empty);
        emp.setEmploymentType(empty);
        emp.setName(empty);
        ParsedAddress padd = emp.addNewParsedAddress();
        padd.setStreetDir(empty);
        padd.setStreet(empty);
        padd.setStreetNumber(empty);
        padd.setStreetType(empty);
        padd.setAptNumber(empty);
        padd.setAptNumber(empty);
        padd.setRuralRoute(empty);
        emp.setCity(empty);
        emp.setState(empty);
        emp.setZip(empty);
        emp.setTimeAtEmployer(empty);
        emp.setStartDate(empty);
        emp.setTitle(empty);
        emp.setSalary(empty);
        emp.setPrevEmployerType(empty);        
        emp.setPrevEmploymentType(empty);
        emp.setPrevName(empty);        
        ParsedAddress parprev = emp.addNewParsedPrevAddress();
        parprev.setStreetDir(empty);
        parprev.setStreet(empty);
        parprev.setStreetNumber(empty);
        parprev.setStreetType(empty);        
        parprev.setAptNumber(empty);
        parprev.setPOBox(empty);
        parprev.setRuralRoute(empty);
        emp.setPrevCity(empty);
        emp.setPrevState(empty);
        emp.setPrevZip(empty);
        emp.setPrevTimeAtEmployer(empty);
        emp.setPrevStartDate(empty);
        emp.setPrevTitle(empty);
        emp.setPrevSalary(empty);
        
        return identity;
    }
    
    /**
     * This method demonstrates the creation of an Origination object.
     * Refer to the developer's guide for information on RequestType.
     **/
    private Origination buildOrigination(Origination origination){
		origination.setRequestType("N");  
		origination.setApplicationDate(java.util.Calendar.getInstance());
		origination.setAppID(String.valueOf(new java.util.Random().nextLong()));
		origination.setDesignation(DesignationDocument.Designation.A_1);		
		return origination;
    }
    
    /**
     * This method iterates over the ItemType elements from the Body (because that's where all the 
     * interesting stuff is) and then prints out certain values. Based on the key, we'll print 
     * attributes that are associated to the object type.
     **/
    private void printResults(ResponseDocument responseDoc){
    	ItemType[] items = responseDoc.getResponse().getBody().getItemArray();
    	        	
    	for (ItemType item: items){
    		System.out.println("\n" + item.getKey());
    		// Getting a cursor will allow us to extract the proper object later.
        	XmlCursor cursor = item.newCursor();
        	if (item.getKey().equals("idScoreRiskResponse"))
            {
        		if (cursor.toFirstChild()){
	                OutputRecord outputRecord = (OutputRecord) cursor.getObject();
	                System.out.println("** Printing idScoreRiskResponse **");
	                System.out.println("IDASequence: " + outputRecord.getIDASequence());
	                System.out.println("IDAStatus: " + outputRecord.getIDAStatus());
	                System.out.println("IDATimeStamp: " + outputRecord.getIDATimeStamp());
	                System.out.println("IDScore: " + outputRecord.getIDScore());
	                System.out.println("IDScoreResultCode1: " + outputRecord.getIDScoreResultCode1());
	                System.out.println("PassThru1: " + outputRecord.getPassThru1());
        		}
            }
        	else if (item.getKey().equals("myIDScoreResponse")){    			
    			if (cursor.toFirstChild()) {
    				OutputRecord outputRecord = (OutputRecord) cursor.getObject();
    				System.out.println("Consumer ID: " + outputRecord.getConsumerID());
    				System.out.println("Score: " + outputRecord.getIDScore());
    				Indicators indicators = outputRecord.getIndicators();    				
    				Group[] groupsArray = indicators.getGroupArray();
    				for (Group group: groupsArray){
    					System.out.println("Group :" + group.getName());
    					Indicator[] indicatorArray = group.getIndicatorArray();
    					for (Indicator indicator: indicatorArray){
    						System.out.println("  Indicator Name: " + indicator.getName());
    						System.out.println("  Indicator Value: " + indicator.getStringValue());
    					}
    				}    				
    			}   			
    		}
        	else if (item.getKey().equals("enrollmentState"))
            {
        		if (cursor.toFirstChild()) {
	                EnrollmentState enrollmentState = (EnrollmentState) cursor.getObject();
	                System.out.println("** Printing EnrollmentState **");
	                System.out.println("Consumer ID: " + enrollmentState.getConsumerID());
	                System.out.println("Previously Enrolled: " + enrollmentState.getPreviouslyEnrolled());
	                System.out.println("Enrollment Date: " + enrollmentState.getEnrollmentDate());
        		}
            }
        	else if (item.getKey().equals("notifications"))
            {
        		if (cursor.toFirstChild()){
	        		System.out.println("** Printing Notifications **");
	                Notifications notifications = (Notifications) cursor.getObject();
	                System.out.println("Sequence: " + notifications.getSequence());
	                AlertEvent[] alertEventArray = notifications.getNotificationArray();	                
	                for (AlertEvent alertEvent : alertEventArray)
	                {	                    
	                    System.out.println("Affiliate: " + alertEvent.getAffiliate());
	                    System.out.println("Consumer ID: " + alertEvent.getConsumerID());
	                    System.out.println("ID Score: " + alertEvent.getIDScore());
	                    System.out.println("** Printing IndicatorGroups **");
	                    com.idanalytics.products.myidmonitoring.IndicatorsDocument.Indicators indicators = alertEvent.getIndicators();
	                    com.idanalytics.products.myidmonitoring.GroupDocument.Group[] groupArray = indicators.getGroupArray();
	                    for (com.idanalytics.products.myidmonitoring.GroupDocument.Group group: groupArray)
	                    {    
	                        System.out.println("Indicator Group: " + group.getName());
	                        com.idanalytics.products.myidmonitoring.IndicatorDocument.Indicator[] indicatorArray = group.getIndicatorArray();                        
	                        System.out.println("** Printing Indicators **");
	                        for (com.idanalytics.products.myidmonitoring.IndicatorDocument.Indicator indicator: indicatorArray)
	                        {                        	
	                            System.out.println(indicator.getName());
	                            System.out.println(indicator.getStringValue());
	                        }
	                    }
	                }
        		}
            }
        	else if (item.getKey().equals("alerts"))
            {
        		if (cursor.toFirstChild()){
	        		System.out.println("** Printing Alerts **");
	                Alerts alerts = (Alerts) cursor.getObject();
	                System.out.println("Sequence: " + alerts.getSequence());
	                AlertEvent[] alertEventArray = alerts.getAlertArray();
	                for (AlertEvent alertEvent : alertEventArray)
	                {
	                    System.out.println("Affiliate: " + alertEvent.getAffiliate());
	                    System.out.println("Consumer ID: " + alertEvent.getConsumerID());
	                    System.out.println("ID Score: " + alertEvent.getIDScore());
	                    System.out.println("** Printing IndicatorGroups **");
	                    com.idanalytics.products.myidmonitoring.IndicatorsDocument.Indicators indicators = alertEvent.getIndicators();
	                    com.idanalytics.products.myidmonitoring.GroupDocument.Group[] groupArray = indicators.getGroupArray();
	                    for (com.idanalytics.products.myidmonitoring.GroupDocument.Group group: groupArray)
	                    {
	                    	System.out.println("Indicator Group: " + group.getName());
	                        com.idanalytics.products.myidmonitoring.IndicatorDocument.Indicator[] indicatorArray = group.getIndicatorArray();	                        
	                        for (com.idanalytics.products.myidmonitoring.IndicatorDocument.Indicator indicator: indicatorArray)
	                        {                        	
	                            System.out.println(indicator.getName());
	                            System.out.println(indicator.getStringValue());
	                        }
	                    }
	                }
        		}
            }
        	else if (item.getKey().equals("error")){ 
        		System.out.println(item.toString());
        	}
    	}    	
    }
}