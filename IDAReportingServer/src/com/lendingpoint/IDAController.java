package com.lendingpoint;
import javax.ws.rs.core.MediaType;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;

import org.apache.axis2.AxisFault;
import org.apache.xmlbeans.XmlCursor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.idanalytics.core.api.BodyType;
import com.idanalytics.core.api.CommandDocument;
import com.idanalytics.core.api.CommandType;
import com.idanalytics.core.api.CredentialsType;
import com.idanalytics.core.api.ItemType;
import com.idanalytics.core.api.ProductSelectionType;
import com.idanalytics.core.api.ProductType;
import com.idanalytics.core.api.ResponseDocument;
import com.idanalytics.error.api.ErrorType;
import com.idanalytics.products.idscore.request.Address;
import com.idanalytics.products.idscore.request.Application;
import com.idanalytics.products.idscore.request.DesignationDocument;
import com.idanalytics.products.idscore.request.Employment;
import com.idanalytics.products.idscore.request.IDScoreRequestDocument;
import com.idanalytics.products.idscore.request.ParsedAddress;
import com.idanalytics.products.idscore.request.IDScoreRequestDocument.IDScoreRequest;
import com.idanalytics.products.idscore.request.Identity;
import com.idanalytics.products.idscore.request.Origination;
import com.idanalytics.products.idscore.result.GroupDocument.Group;
import com.idanalytics.products.idscore.result.IndicatorDocument.Indicator;
import com.idanalytics.products.idscore.result.IndicatorsDocument.Indicators;
import com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord;
import com.idanalytics.webservice.definitions.IDSPServiceStub;

@Controller
@RequestMapping("/idaProxyServer")
public class IDAController {
	
	@Autowired
	IDAServiceProcessor idaServiceProcessor;
	
    Logger logger = LoggerFactory.getLogger(IDAController.class);
    
    @RequestMapping(value = "/getIdaReponse", method = RequestMethod.POST,consumes = "application/json")
	@ResponseBody
	public String postapplication(@RequestBody String requestbody) {
    	return idaServiceProcessor.postIdaRequest(requestbody);
	}
	
	
}
