package com.lendingpoint;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.axis2.AxisFault;
import org.apache.xmlbeans.XmlCursor;
import org.springframework.beans.factory.annotation.Value;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.idanalytics.core.api.BodyType;
import com.idanalytics.core.api.CommandDocument;
import com.idanalytics.core.api.CommandType;
import com.idanalytics.core.api.CredentialsType;
import com.idanalytics.core.api.ItemType;
import com.idanalytics.core.api.ProductSelectionType;
import com.idanalytics.core.api.ProductType;
import com.idanalytics.core.api.ResponseDocument;
import com.idanalytics.products.idscore.request.Address;
import com.idanalytics.products.idscore.request.Application;
import com.idanalytics.products.idscore.request.DesignationDocument;
import com.idanalytics.products.idscore.request.Employment;
import com.idanalytics.products.idscore.request.IDScoreRequestDocument;
import com.idanalytics.products.idscore.request.Identity;
import com.idanalytics.products.idscore.request.Origination;
import com.idanalytics.products.idscore.request.ParsedAddress;
import com.idanalytics.products.idscore.request.IDScoreRequestDocument.IDScoreRequest;
import com.idanalytics.products.idscore.result.GroupDocument.Group;
import com.idanalytics.products.idscore.result.IndicatorDocument.Indicator;
import com.idanalytics.products.idscore.result.IndicatorsDocument.Indicators;
import com.idanalytics.products.idscore.result.OutputRecordDocument.OutputRecord;
import com.idanalytics.webservice.definitions.IDSPServiceStub;

public class IDAServiceProcessor {

	@Value("${ida.client.id}")
    private String CLIENT_ID;
	@Value("${ida.user.id}")
	private String USER_ID;
	@Value("${ida.password}")
	private String PASS_WORD;
	@Value("${ida.stub.url}")
	private String STUB_URL;
	
	private static final String empty="";
	
	public String postIdaRequest(String requestbody){
		ApplicationData[] appDataList;
		ObjectMapper objectMapper = new ObjectMapper();
		ResponseDocument responseDoc = null;
		List<HashMap<String, String>> indicatorMapList = new ArrayList<HashMap<String,String>>();
		HashMap<String,String> indicatorMap = new HashMap<String,String>();
		String indicatorMapstring = null;
    	try {	
    		appDataList = (ApplicationData[]) objectMapper.readValue(requestbody, ApplicationData[].class);
    		System.out.println("appDataList size :: "+appDataList.length);
    		IDSPServiceStub stub = null;
			stub = new IDSPServiceStub(this.STUB_URL);
			CommandDocument commandDoc = null;
			SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
        	for(ApplicationData appData : appDataList){
        		appData.setProcessedDate(sf.format(new Date()));
        		commandDoc = buildCommand(appData);
        		
        		responseDoc = stub.executeCommand(commandDoc);	        	
            	indicatorMap = getIdaIndicators(responseDoc);	
            	indicatorMap.put("indicatorDetails", objectMapper.writeValueAsString(indicatorMap));
            	indicatorMap.put("appData", objectMapper.writeValueAsString(appData));
            	indicatorMapList.add(indicatorMap);
        	}
        	System.out.println("indicatorMapList size :: " + indicatorMapList.size());
			indicatorMapstring = objectMapper.writeValueAsString(indicatorMapList);
			
		} catch (AxisFault e1) {
			e1.printStackTrace();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return indicatorMapstring;
	}
	
	private CommandDocument buildCommand(ApplicationData appData){
    	BigDecimal threePointO = java.math.BigDecimal.valueOf(3.0);
    		
    	// all solutions require a command object which encapsulates the request.
    	CommandDocument commandDoc = (CommandDocument) CommandDocument.Factory.newInstance();
    	CommandType command = commandDoc.addNewCommand();
    	
    	command.setClient(this.CLIENT_ID);
    	
	    CredentialsType creds = command.addNewCredentials();
	    creds.setUsername(this.USER_ID);
	    creds.setPassword(this.PASS_WORD);
	    command.setCredentials(creds);
	    
	    // setup the BodyType and ItemType
        // refer to the schemas & Developer's Guide for request types for each solution                
	    BodyType body = command.addNewBody();	    
	    ItemType item = body.addNewItem();
	    command.setSolution("Standard/MultiProduct");
    	ProductSelectionType productSelectionType = ProductSelectionType.Factory.newInstance();
    	ProductType productType = productSelectionType.addNewProduct();
    	productType.setName("OLNAttributesGLBA");
    	productType.setProductID("OLNAG1.0");
    	command.setProductSelection(productSelectionType);
    	
    	IDScoreRequestDocument scoreRequestDoc = IDScoreRequestDocument.Factory.newInstance();	    	
    	IDScoreRequest idScoreRequest = scoreRequestDoc.addNewIDScoreRequest();	    	
    	idScoreRequest.setSchemaVersion(threePointO);
    	//idScoreRequest.add
    	Origination origination = idScoreRequest.addNewOrigination();
    	buildOrigination(origination);
    	
    	Identity identity = idScoreRequest.addNewIdentity();
    	buildIdentity(appData,identity);
    	
    	Application app = idScoreRequest.addNewApplication();
    	buildApplication(app);
    	item.set(scoreRequestDoc);
	    
	    item.setKey("standardRequest");	    
	    return commandDoc;
    }
	
	private Origination buildOrigination(Origination origination){
		origination.setRequestType("N");  
		origination.setApplicationDate(java.util.Calendar.getInstance());
		origination.setAppID(String.valueOf(new java.util.Random().nextLong()));
		origination.setDesignation(DesignationDocument.Designation.A_1);	
		origination.setEventType("CAN");
		origination.setIndustryType("IN");
		return origination;
    }
	
	private Application buildApplication(Application app){
    	app.setChannel(empty);
        app.setAcquisitionMethod(empty);
        app.setAgentLoc(empty);
        app.setSourceIP(empty);
        app.setPrimaryDecisionCode(empty);
        app.setSecondaryDecisionCode(empty);
        app.setPrimaryPortfolio(empty);
        app.setSecondaryPortfolio(empty);
        app.setSecondaryFraudCode(empty);
        app.setPrimaryIncome(empty);
        app.setInferredIncome(empty);
        app.setRecommendedCreditLine(empty);
        return app;
    }
	
	private HashMap<String,String> getIdaIndicators(ResponseDocument responseDoc){
		HashMap<String,String> indicatorMap = new HashMap<String,String>();
		ItemType[] items = responseDoc.getResponse().getBody().getItemArray();
    	for (ItemType item: items){
    		XmlCursor cursor = item.newCursor();
    		if (item.getKey().equals("error")){ 
    			indicatorMap.put("error", item.toString());
        	}
    		else{    			
    			if (cursor.toFirstChild()) {
    				OutputRecord outputRecord = (OutputRecord) cursor.getObject();
    				Indicators indicators = outputRecord.getIndicators();    				
    				Group[] groupsArray = indicators.getGroupArray();
    				for (Group group: groupsArray){
    					Indicator[] indicatorArray = group.getIndicatorArray();
    					for (Indicator indicator: indicatorArray){
    						indicatorMap.put(indicator.getName(), indicator.getStringValue());
    						
    					}
    				}    				
    			}   			
    		}
    	}    
    	return indicatorMap;
    }
	
	private Identity buildIdentity(ApplicationData appData,Identity identity){    	
    	identity.setSSN(appData.getSsn());
        identity.setTitle(appData.getTitle());
        identity.setFirstName(appData.getFirstName());
        identity.setMiddleName(empty);
        identity.setLastName(appData.getLastName());
        identity.setSuffix(empty);
       /* if(appData.getDob()!= null){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
        identity.setDOB(df.format(appData.getDob()));
        }*/
        identity.setDOB(appData.getDob());
        Address addr = identity.addNewAddress();
        addr.setLine1(appData.getAddress());        
        addr.setLine2(empty);
        identity.setCity(appData.getCity());
        identity.setState(appData.getState());
        identity.setZip(appData.getZip());        
        identity.setHomePhone(empty);
        identity.setMobilePhone(empty);
        identity.setWorkPhone(appData.getMobilePhone());
        identity.setEmail(appData.getEmail());
        identity.setIDType(empty);
        identity.setIDOrigin(empty);
        identity.setIDNumber(empty);        
        Employment emp = identity.addNewEmployment();
        emp.setEmployerType(empty);
        emp.setEmploymentType(empty);
        emp.setName(empty);
        ParsedAddress padd = emp.addNewParsedAddress();
        padd.setStreetDir(empty);
        padd.setStreet(empty);
        padd.setStreetNumber(empty);
        padd.setStreetType(empty);
        padd.setAptNumber(empty);
        padd.setAptNumber(empty);
        padd.setRuralRoute(empty);
        emp.setCity(empty);
        emp.setState(empty);
        emp.setZip(empty);
        emp.setTimeAtEmployer(empty);
        emp.setStartDate(empty);
        emp.setTitle(empty);
        emp.setSalary(empty);
        emp.setPrevEmployerType(empty);        
        emp.setPrevEmploymentType(empty);
        emp.setPrevName(empty);        
        ParsedAddress parprev = emp.addNewParsedPrevAddress();
        parprev.setStreetDir(empty);
        parprev.setStreet(empty);
        parprev.setStreetNumber(empty);
        parprev.setStreetType(empty);        
        parprev.setAptNumber(empty);
        parprev.setPOBox(empty);
        parprev.setRuralRoute(empty);
        emp.setPrevCity(empty);
        emp.setPrevState(empty);
        emp.setPrevZip(empty);
        emp.setPrevTimeAtEmployer(empty);
        emp.setPrevStartDate(empty);
        emp.setPrevTitle(empty);
        emp.setPrevSalary(empty);
        
        return identity;
    }
}
