package com.lendingpoint;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

public class TestJDBC {
		
	 public static void main(String[] args) throws SQLException {
	        SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
	        dataSource.setDriver(new com.mysql.jdbc.Driver());
	        /*dataSource.setUrl("jdbc:mysql://localhost:3306/SalesForce");
	        dataSource.setUsername("root");
	        dataSource.setPassword("root");*/
	        dataSource.setUrl("jdbc:mysql://mysqlqa.ce5qbgeken3n.us-east-1.rds.amazonaws.com:3306/SalesForce");
	        dataSource.setUsername("testAdmin");
	        dataSource.setPassword("testAdminPassword");
	         
	         
	        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
	 
	        String sqlSelect = "SELECT app.Id,"
	        		+ "app.name,"
	        		+ "con.genesis__SSN__c,"
	        		+ "con.title,"
	        		+ "con.firstName,"
	        		+ "con.lastName,"
	        		+ "con.birthdate,"
	        		+ "con.mailingStreet,"
	        		+ "con.mailingCity,"
	        		+ "con.postal_code__c,"
	        		+ "con.phone,"
	        		+ "con.email "
	        		+ "FROM genesis__Applications__c app "
	        		+ "Inner Join Contact con on con.ID=app.genesis__Contact__c "
	        		+ "where app.genesis__Status__c  = 'Credit Qualified' limit 10";
	        List<ApplicationData> listapp = jdbcTemplate.query(sqlSelect, new RowMapper<ApplicationData>() {
	 
	            public ApplicationData mapRow(ResultSet result, int rowNum) throws SQLException {
	            	ApplicationData app = new ApplicationData();
	            	app.setAppId(result.getString("firstName"));
	            	app.setSsn(result.getString("genesis__SSN__c"));
	            	app.setTitle(result.getString("title"));
	            	app.setFirstName(result.getString("firstName"));
	            	app.setLastName(result.getString("lastName"));
	            	app.setDob(result.getString("birthdate"));
	            	app.setAddress(result.getString("mailingStreet"));
	            	app.setCity(result.getString("mailingCity"));
	            	app.setZip(result.getString("postal_code__c"));
	            	app.setMobilePhone(result.getString("phone"));
	            	app.setEmail(result.getString("email"));
	            	//app.setName(result.getString("FirstName"));
	            	//app.setLoanAmount(result.getDouble("genesis__Loan_Amount__c"));
	            	/*app.setEmail(result.getString("email"));
	            	app.setAddress(result.getString("address"));
	            	app.setPhone(result.getString("telephone"));*/
	                 
	                return app;
	            }
	             
	        });
	         
	        for (ApplicationData aContact : listapp) {
	            System.out.println(aContact);
	        }
	         
	        
	    }
}
